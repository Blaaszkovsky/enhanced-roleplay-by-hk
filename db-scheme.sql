-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 17 Cze 2024, 21:56
-- Wersja serwera: 8.0.35-0ubuntu0.20.04.1
-- Wersja PHP: 7.4.3-4ubuntu2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `gs_archolos`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `account`
--

CREATE TABLE `account` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `playerUID` varchar(255) NOT NULL,
  `playerUIDAlt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ip` varchar(200) NOT NULL,
  `resetCode` varchar(200) NOT NULL,
  `confirmationCode` varchar(200) NOT NULL,
  `isConfirmed` int NOT NULL DEFAULT '0',
  `createdAt` varchar(255) NOT NULL,
  `updatedAt` varchar(255) NOT NULL,
  `online` int NOT NULL DEFAULT '0',
  `nextPnToCharacter` int NOT NULL DEFAULT '0',
  `skinsIds` varchar(255) NOT NULL DEFAULT '',
  `auth_key` varchar(255) NOT NULL DEFAULT '',
  `hkPoints` int NOT NULL DEFAULT '0',
  `isNeedPasswordReroll` int NOT NULL DEFAULT '0',
  `isNeedAcceptPolicy` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `accountwhitelist`
--

CREATE TABLE `accountwhitelist` (
  `id` int NOT NULL,
  `mac` varchar(200) NOT NULL,
  `used` int NOT NULL DEFAULT '0',
  `active` int NOT NULL DEFAULT '1',
  `code` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `actiondraw`
--

CREATE TABLE `actiondraw` (
  `id` int NOT NULL,
  `userId` int NOT NULL,
  `expireAt` int NOT NULL,
  `text` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `world` varchar(200) NOT NULL,
  `x` int NOT NULL,
  `y` int NOT NULL,
  `z` int NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `admin`
--

CREATE TABLE `admin` (
  `id` int NOT NULL,
  `role` int NOT NULL DEFAULT '0',
  `accountId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adminasset`
--

CREATE TABLE `adminasset` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `instance` varchar(200) NOT NULL DEFAULT 'PC_HERO',
  `str` int NOT NULL DEFAULT '0',
  `dex` int NOT NULL DEFAULT '0',
  `maxHp` int NOT NULL DEFAULT '0',
  `maxMana` int NOT NULL DEFAULT '0',
  `oneh` int NOT NULL DEFAULT '0',
  `twoh` int NOT NULL DEFAULT '0',
  `bow` int NOT NULL DEFAULT '0',
  `cbow` int NOT NULL DEFAULT '0',
  `fatness` int NOT NULL DEFAULT '0',
  `height` int NOT NULL DEFAULT '0',
  `inteligence` int NOT NULL,
  `magicLvl` int NOT NULL,
  `bodyModel` varchar(200) NOT NULL,
  `bodyTxt` int NOT NULL,
  `headModel` varchar(200) NOT NULL,
  `headTxt` int NOT NULL,
  `scaleX` float NOT NULL DEFAULT '1',
  `scaleY` float NOT NULL DEFAULT '1',
  `scaleZ` float NOT NULL DEFAULT '1',
  `items` text NOT NULL,
  `effects` text NOT NULL,
  `createdAt` varchar(200) NOT NULL,
  `createdBy` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adminlog`
--

CREATE TABLE `adminlog` (
  `id` int NOT NULL,
  `adminId` int NOT NULL,
  `log` text NOT NULL,
  `playerId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `all_items`
--

CREATE TABLE `all_items` (
  `id` int NOT NULL,
  `instance` varchar(128) CHARACTER SET cp1250 COLLATE cp1250_bin NOT NULL,
  `name` varchar(255) CHARACTER SET cp1250 COLLATE cp1250_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_bin;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ban`
--

CREATE TABLE `ban` (
  `id` int NOT NULL,
  `accountId` int NOT NULL,
  `name` varchar(250) NOT NULL,
  `serial` varchar(250) NOT NULL,
  `playerUID` varchar(255) NOT NULL,
  `mac` varchar(250) NOT NULL,
  `reason` text NOT NULL,
  `timeout` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `board`
--

CREATE TABLE `board` (
  `id` int NOT NULL,
  `boardId` int NOT NULL,
  `text` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `world` varchar(200) NOT NULL,
  `x` int NOT NULL,
  `y` int NOT NULL,
  `expiredAt` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `buff`
--

CREATE TABLE `buff` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `scheme` varchar(200) NOT NULL,
  `counter` int NOT NULL,
  `intervalsec` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `building`
--

CREATE TABLE `building` (
  `id` int NOT NULL,
  `scheme` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `author` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `date` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `world` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `progress` int NOT NULL DEFAULT '1',
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `rotX` float NOT NULL,
  `rotY` float NOT NULL,
  `rotZ` float NOT NULL,
  `targetX` float NOT NULL,
  `targetY` float NOT NULL,
  `targetZ` float NOT NULL,
  `targetRotX` float NOT NULL,
  `targetRotY` float NOT NULL,
  `targetRotZ` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chatlog`
--

CREATE TABLE `chatlog` (
  `id` int NOT NULL,
  `log` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `playerId` int NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_bin;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `corner`
--

CREATE TABLE `corner` (
  `id` int NOT NULL,
  `name` varchar(128) NOT NULL,
  `status` int NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `angle` float NOT NULL,
  `moneyMin` int NOT NULL,
  `moneyMax` int NOT NULL,
  `animation` varchar(255) NOT NULL,
  `itemToTrade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `craft`
--

CREATE TABLE `craft` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `caller` int NOT NULL,
  `animation` varchar(255) NOT NULL,
  `staminaCost` float NOT NULL,
  `skillRequired` varchar(255) NOT NULL,
  `skillValue` varchar(255) NOT NULL,
  `fractionIds` varchar(64) NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `createdAt` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `craftitems`
--

CREATE TABLE `craftitems` (
  `id` int NOT NULL,
  `instance` varchar(255) NOT NULL,
  `amount` int NOT NULL,
  `craftId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `craftreward`
--

CREATE TABLE `craftreward` (
  `id` int NOT NULL,
  `craftId` int NOT NULL,
  `instance` varchar(255) NOT NULL,
  `amount` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `disconnects`
--

CREATE TABLE `disconnects` (
  `id` int NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `playerName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `accountId` int NOT NULL,
  `disconnectTime` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `draws`
--

CREATE TABLE `draws` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `r` int NOT NULL,
  `g` int NOT NULL,
  `b` int NOT NULL,
  `distance` int NOT NULL,
  `world` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fishing`
--

CREATE TABLE `fishing` (
  `id` int NOT NULL,
  `scheme` varchar(255) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `slots` int NOT NULL,
  `rotationX` float NOT NULL,
  `rotationY` float NOT NULL,
  `rotationZ` float NOT NULL,
  `world` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'ARCHOLOS.ZEN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `forest`
--

CREATE TABLE `forest` (
  `id` int NOT NULL,
  `scheme` varchar(255) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `growth` int NOT NULL,
  `rotationX` float NOT NULL,
  `rotationY` float NOT NULL,
  `rotationZ` float NOT NULL,
  `world` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'ARCHOLOS.ZEN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fraction`
--

CREATE TABLE `fraction` (
  `id` int NOT NULL,
  `createdBy` varchar(200) NOT NULL,
  `updatedBy` varchar(200) NOT NULL,
  `createdAt` varchar(200) NOT NULL,
  `updatedAt` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `colorR` int NOT NULL,
  `colorG` int NOT NULL,
  `colorB` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fractionmember`
--

CREATE TABLE `fractionmember` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `fractionId` int NOT NULL,
  `permissions` text NOT NULL,
  `createdAt` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fractionpermission`
--

CREATE TABLE `fractionpermission` (
  `id` int NOT NULL,
  `fractionId` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` int NOT NULL,
  `labels` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fractionterritory`
--

CREATE TABLE `fractionterritory` (
  `id` int NOT NULL,
  `fractionId` int NOT NULL,
  `world` varchar(255) NOT NULL,
  `positions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `groundherb`
--

CREATE TABLE `groundherb` (
  `id` int NOT NULL,
  `scheme` varchar(200) NOT NULL,
  `rangeDistance` int NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `herb`
--

CREATE TABLE `herb` (
  `id` int NOT NULL,
  `scheme` varchar(255) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `stamina` int NOT NULL,
  `status` int NOT NULL,
  `growth` int NOT NULL,
  `rotation` int NOT NULL,
  `userId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `house`
--

CREATE TABLE `house` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `position` text NOT NULL,
  `minY` float NOT NULL,
  `maxY` float NOT NULL,
  `world` varchar(200) NOT NULL,
  `ownerId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `housedoor`
--

CREATE TABLE `housedoor` (
  `id` int NOT NULL,
  `houseId` int NOT NULL,
  `positionX` float NOT NULL,
  `positionY` float NOT NULL,
  `positionZ` float NOT NULL,
  `rotationX` float NOT NULL,
  `rotationY` float NOT NULL,
  `rotationZ` float NOT NULL,
  `targetPositionX` float NOT NULL,
  `targetPositionY` float NOT NULL,
  `targetPositionZ` float NOT NULL,
  `targetRotationX` float NOT NULL,
  `targetRotationY` float NOT NULL,
  `targetRotationZ` float NOT NULL,
  `keyId` int NOT NULL,
  `visual` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `item`
--

CREATE TABLE `item` (
  `id` int NOT NULL,
  `instance` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `resistance` int NOT NULL,
  `guid` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `amount` int NOT NULL DEFAULT '1',
  `stacked` tinyint NOT NULL DEFAULT '0',
  `equipped` tinyint NOT NULL DEFAULT '0',
  `description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `itemrelation`
--

CREATE TABLE `itemrelation` (
  `id` int NOT NULL,
  `objTypeId` int NOT NULL,
  `objId` int NOT NULL,
  `itemId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `keybinds`
--

CREATE TABLE `keybinds` (
  `idunq` int NOT NULL,
  `id` int NOT NULL,
  `keyId` int NOT NULL,
  `anim` varchar(255) CHARACTER SET cp1250 COLLATE cp1250_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mine`
--

CREATE TABLE `mine` (
  `id` int NOT NULL,
  `scheme` varchar(255) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `slots` int NOT NULL,
  `rotationX` float NOT NULL,
  `rotationY` float NOT NULL,
  `rotationZ` float NOT NULL,
  `world` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'ARCHOLOS.ZEN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mute`
--

CREATE TABLE `mute` (
  `id` int NOT NULL,
  `serial` varchar(255) NOT NULL,
  `mac` varchar(255) NOT NULL,
  `status` int NOT NULL,
  `timeout` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `note`
--

CREATE TABLE `note` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `world` varchar(255) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `status` int NOT NULL,
  `playerId` int NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `player`
--

CREATE TABLE `player` (
  `id` int NOT NULL,
  `accountId` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `bodyModel` varchar(200) NOT NULL,
  `bodyTxt` int NOT NULL,
  `headModel` varchar(200) NOT NULL,
  `headTxt` int NOT NULL,
  `personalCard` text NOT NULL,
  `personalNotes` text NOT NULL,
  `walking` int NOT NULL,
  `fatness` int NOT NULL,
  `speech` int NOT NULL,
  `unconscious` int NOT NULL DEFAULT '0',
  `serial` varchar(200) NOT NULL,
  `str` int NOT NULL DEFAULT '10',
  `dex` int NOT NULL DEFAULT '10',
  `health` int NOT NULL DEFAULT '0',
  `maxHealth` int NOT NULL DEFAULT '0',
  `mana` int NOT NULL DEFAULT '0',
  `maxMana` int NOT NULL DEFAULT '0',
  `oneH` int NOT NULL DEFAULT '0',
  `twoH` int NOT NULL DEFAULT '0',
  `bow` int NOT NULL DEFAULT '0',
  `cbow` int NOT NULL DEFAULT '0',
  `inteligence` int NOT NULL DEFAULT '0',
  `positionX` float NOT NULL DEFAULT '0',
  `positionY` float NOT NULL DEFAULT '0',
  `positionZ` float NOT NULL DEFAULT '0',
  `world` varchar(200) DEFAULT NULL,
  `angle` int NOT NULL DEFAULT '0',
  `deaths` int NOT NULL DEFAULT '0',
  `kills` int NOT NULL DEFAULT '0',
  `stamina` float NOT NULL DEFAULT '0',
  `hunger` float NOT NULL DEFAULT '100',
  `endurance` int NOT NULL DEFAULT '0',
  `statusId` int NOT NULL DEFAULT '0',
  `respawnLeft` int NOT NULL DEFAULT '0',
  `lastPnDay` int NOT NULL DEFAULT '0',
  `magicLvl` int NOT NULL DEFAULT '0',
  `experience` int NOT NULL DEFAULT '0',
  `level` int NOT NULL DEFAULT '0',
  `learnPoints` int NOT NULL DEFAULT '0',
  `minutesInGame` int NOT NULL DEFAULT '0',
  `minutesCounter` int NOT NULL,
  `pointsToday` int NOT NULL,
  `staminaTodayRecover` int NOT NULL,
  `armor` varchar(255) DEFAULT NULL,
  `ranged` varchar(255) DEFAULT NULL,
  `melee` varchar(255) DEFAULT NULL,
  `fractionId` int NOT NULL DEFAULT '-1',
  `fractionRoleId` int NOT NULL DEFAULT '-1',
  `online` int NOT NULL DEFAULT '0',
  `createdAt` varchar(255) NOT NULL,
  `updatedAt` varchar(255) NOT NULL,
  `scaleX` float NOT NULL DEFAULT '1',
  `scaleY` float NOT NULL DEFAULT '1',
  `scaleZ` float NOT NULL DEFAULT '1',
  `instance` varchar(16) NOT NULL DEFAULT 'PC_HERO',
  `stamineRecoveryByFood` float NOT NULL DEFAULT '0',
  `stamineRecoveryByInteraction` float NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT 'Brak'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playerdialog`
--

CREATE TABLE `playerdialog` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `timestamp` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playereffect`
--

CREATE TABLE `playereffect` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `effectId` int NOT NULL,
  `refreshTime` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playeritem`
--

CREATE TABLE `playeritem` (
  `id` int NOT NULL,
  `instance` varchar(200) NOT NULL,
  `amount` int NOT NULL,
  `equipped` int NOT NULL,
  `playerId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playerlog`
--

CREATE TABLE `playerlog` (
  `id` int NOT NULL,
  `log` text NOT NULL,
  `playerId` int NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playerrelation`
--

CREATE TABLE `playerrelation` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `relationPlayerId` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `playerskill`
--

CREATE TABLE `playerskill` (
  `id` int NOT NULL,
  `playerId` int NOT NULL,
  `skill_0` int NOT NULL DEFAULT '0',
  `skill_1` int NOT NULL DEFAULT '0',
  `skill_2` int NOT NULL DEFAULT '0',
  `skill_3` int NOT NULL DEFAULT '0',
  `skill_4` int NOT NULL DEFAULT '0',
  `skill_5` int NOT NULL DEFAULT '0',
  `skill_6` int NOT NULL DEFAULT '0',
  `skill_7` int NOT NULL DEFAULT '0',
  `skill_8` int NOT NULL DEFAULT '0',
  `skill_9` int NOT NULL DEFAULT '0',
  `skill_10` int NOT NULL DEFAULT '0',
  `skill_11` int NOT NULL DEFAULT '0',
  `skill_12` int NOT NULL DEFAULT '0',
  `skill_13` int NOT NULL DEFAULT '0',
  `skill_14` int NOT NULL DEFAULT '0',
  `skill_15` int NOT NULL DEFAULT '0',
  `skill_16` int NOT NULL DEFAULT '0',
  `skill_17` int NOT NULL DEFAULT '0',
  `skill_18` int NOT NULL DEFAULT '0',
  `skill_19` int NOT NULL DEFAULT '0',
  `skill_20` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sqlcommands`
--

CREATE TABLE `sqlcommands` (
  `id` int NOT NULL,
  `type` int NOT NULL,
  `playerId` int NOT NULL,
  `value` int DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `executor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `system`
--

CREATE TABLE `system` (
  `id` int NOT NULL,
  `type` int NOT NULL,
  `value` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `test`
--

CREATE TABLE `test` (
  `xd` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `verified`
--

CREATE TABLE `verified` (
  `id` int NOT NULL,
  `id_discord` bigint NOT NULL,
  `account_name` varchar(128) NOT NULL,
  `code` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vob`
--

CREATE TABLE `vob` (
  `id` int NOT NULL,
  `hpMode` int NOT NULL,
  `world` varchar(200) NOT NULL,
  `hp` int NOT NULL,
  `hpMax` int NOT NULL,
  `triggerId` int NOT NULL,
  `triggerAnimation` int NOT NULL,
  `triggerType` int NOT NULL,
  `triggerTime` int NOT NULL DEFAULT '0',
  `movable` int NOT NULL,
  `alpha` float NOT NULL,
  `dynamic` int NOT NULL,
  `weight` int NOT NULL,
  `visual` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `animationId` int NOT NULL,
  `animationTime` int NOT NULL,
  `positionX` float NOT NULL,
  `positionY` float NOT NULL,
  `positionZ` float NOT NULL,
  `rotationX` float NOT NULL,
  `rotationY` float NOT NULL,
  `rotationZ` float NOT NULL,
  `targetPositionX` float NOT NULL DEFAULT '0',
  `targetPositionY` float NOT NULL DEFAULT '0',
  `targetPositionZ` float DEFAULT '0',
  `targetRotationX` float NOT NULL DEFAULT '0',
  `targetRotationY` float NOT NULL DEFAULT '0',
  `targetRotationZ` float NOT NULL DEFAULT '0',
  `items` text NOT NULL,
  `keyId` int NOT NULL DEFAULT '-1',
  `opened` int NOT NULL DEFAULT '0',
  `type` int NOT NULL,
  `guid` varchar(255) NOT NULL DEFAULT '',
  `complexity` int NOT NULL DEFAULT '3',
  `createdBy` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `voblog`
--

CREATE TABLE `voblog` (
  `id` int NOT NULL,
  `type` varchar(100) NOT NULL,
  `playerId` int NOT NULL,
  `vobId` int NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `date` datetime NOT NULL,
  `info` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `work`
--

CREATE TABLE `work` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `staminaCost` float NOT NULL,
  `seconds` int NOT NULL,
  `secondsMultiplier` int NOT NULL,
  `staminaCostMultiplier` int NOT NULL,
  `createdAt` varchar(255) NOT NULL,
  `createdBy` varchar(255) NOT NULL,
  `skillRequired` int NOT NULL,
  `skillValue` int NOT NULL,
  `animation` varchar(200) NOT NULL,
  `caller` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `workreward`
--

CREATE TABLE `workreward` (
  `id` int NOT NULL,
  `workId` int NOT NULL,
  `instance` varchar(200) NOT NULL,
  `amount` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `serial` (`serial`);

--
-- Indeksy dla tabeli `accountwhitelist`
--
ALTER TABLE `accountwhitelist`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `actiondraw`
--
ALTER TABLE `actiondraw`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `adminAccountRelation` (`accountId`);

--
-- Indeksy dla tabeli `adminasset`
--
ALTER TABLE `adminasset`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `adminlog`
--
ALTER TABLE `adminlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adminId` (`adminId`);

--
-- Indeksy dla tabeli `all_items`
--
ALTER TABLE `all_items`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ban`
--
ALTER TABLE `ban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accountId` (`accountId`);

--
-- Indeksy dla tabeli `board`
--
ALTER TABLE `board`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `buff`
--
ALTER TABLE `buff`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `chatlog`
--
ALTER TABLE `chatlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `corner`
--
ALTER TABLE `corner`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `craft`
--
ALTER TABLE `craft`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `craftitems`
--
ALTER TABLE `craftitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftId` (`craftId`);

--
-- Indeksy dla tabeli `craftreward`
--
ALTER TABLE `craftreward`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftId` (`craftId`);

--
-- Indeksy dla tabeli `disconnects`
--
ALTER TABLE `disconnects`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `draws`
--
ALTER TABLE `draws`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `fishing`
--
ALTER TABLE `fishing`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `forest`
--
ALTER TABLE `forest`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `fraction`
--
ALTER TABLE `fraction`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `fractionmember`
--
ALTER TABLE `fractionmember`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`),
  ADD KEY `fractionId` (`fractionId`);

--
-- Indeksy dla tabeli `fractionpermission`
--
ALTER TABLE `fractionpermission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fractionId` (`fractionId`);

--
-- Indeksy dla tabeli `fractionterritory`
--
ALTER TABLE `fractionterritory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fractionId` (`fractionId`);

--
-- Indeksy dla tabeli `groundherb`
--
ALTER TABLE `groundherb`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `herb`
--
ALTER TABLE `herb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indeksy dla tabeli `house`
--
ALTER TABLE `house`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `housedoor`
--
ALTER TABLE `housedoor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `houseId` (`houseId`);

--
-- Indeksy dla tabeli `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `itemrelation`
--
ALTER TABLE `itemrelation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `objTypeId` (`objTypeId`),
  ADD KEY `objId` (`objId`),
  ADD KEY `itemId` (`itemId`);

--
-- Indeksy dla tabeli `keybinds`
--
ALTER TABLE `keybinds`
  ADD PRIMARY KEY (`idunq`),
  ADD KEY `id` (`id`);

--
-- Indeksy dla tabeli `mine`
--
ALTER TABLE `mine`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `mute`
--
ALTER TABLE `mute`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accountId` (`accountId`);

--
-- Indeksy dla tabeli `playerdialog`
--
ALTER TABLE `playerdialog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `playereffect`
--
ALTER TABLE `playereffect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `playeritem`
--
ALTER TABLE `playeritem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `playerlog`
--
ALTER TABLE `playerlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `playerrelation`
--
ALTER TABLE `playerrelation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`),
  ADD KEY `relationPlayerId` (`relationPlayerId`);

--
-- Indeksy dla tabeli `playerskill`
--
ALTER TABLE `playerskill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `sqlcommands`
--
ALTER TABLE `sqlcommands`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `verified`
--
ALTER TABLE `verified`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `vob`
--
ALTER TABLE `vob`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `voblog`
--
ALTER TABLE `voblog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`);

--
-- Indeksy dla tabeli `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `workreward`
--
ALTER TABLE `workreward`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workId` (`workId`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `account`
--
ALTER TABLE `account`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `accountwhitelist`
--
ALTER TABLE `accountwhitelist`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `actiondraw`
--
ALTER TABLE `actiondraw`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `adminasset`
--
ALTER TABLE `adminasset`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `adminlog`
--
ALTER TABLE `adminlog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `all_items`
--
ALTER TABLE `all_items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ban`
--
ALTER TABLE `ban`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `board`
--
ALTER TABLE `board`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `buff`
--
ALTER TABLE `buff`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `building`
--
ALTER TABLE `building`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `chatlog`
--
ALTER TABLE `chatlog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `corner`
--
ALTER TABLE `corner`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `craft`
--
ALTER TABLE `craft`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `craftitems`
--
ALTER TABLE `craftitems`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `craftreward`
--
ALTER TABLE `craftreward`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `disconnects`
--
ALTER TABLE `disconnects`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `draws`
--
ALTER TABLE `draws`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fishing`
--
ALTER TABLE `fishing`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `forest`
--
ALTER TABLE `forest`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fraction`
--
ALTER TABLE `fraction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fractionmember`
--
ALTER TABLE `fractionmember`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fractionpermission`
--
ALTER TABLE `fractionpermission`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fractionterritory`
--
ALTER TABLE `fractionterritory`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `groundherb`
--
ALTER TABLE `groundherb`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `herb`
--
ALTER TABLE `herb`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `house`
--
ALTER TABLE `house`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `housedoor`
--
ALTER TABLE `housedoor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `item`
--
ALTER TABLE `item`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `itemrelation`
--
ALTER TABLE `itemrelation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `keybinds`
--
ALTER TABLE `keybinds`
  MODIFY `idunq` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `mine`
--
ALTER TABLE `mine`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `mute`
--
ALTER TABLE `mute`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `note`
--
ALTER TABLE `note`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `player`
--
ALTER TABLE `player`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playerdialog`
--
ALTER TABLE `playerdialog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playereffect`
--
ALTER TABLE `playereffect`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playeritem`
--
ALTER TABLE `playeritem`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playerlog`
--
ALTER TABLE `playerlog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playerrelation`
--
ALTER TABLE `playerrelation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `playerskill`
--
ALTER TABLE `playerskill`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `sqlcommands`
--
ALTER TABLE `sqlcommands`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `system`
--
ALTER TABLE `system`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `verified`
--
ALTER TABLE `verified`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `vob`
--
ALTER TABLE `vob`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `voblog`
--
ALTER TABLE `voblog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `work`
--
ALTER TABLE `work`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `workreward`
--
ALTER TABLE `workreward`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `adminAccountRelation` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `adminlog`
--
ALTER TABLE `adminlog`
  ADD CONSTRAINT `adminLogRelation` FOREIGN KEY (`adminId`) REFERENCES `account` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `ban`
--
ALTER TABLE `ban`
  ADD CONSTRAINT `accountBanRelation` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`);

--
-- Ograniczenia dla tabeli `chatlog`
--
ALTER TABLE `chatlog`
  ADD CONSTRAINT `chatLogRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `craftitems`
--
ALTER TABLE `craftitems`
  ADD CONSTRAINT `craftItemsRelation` FOREIGN KEY (`craftId`) REFERENCES `craft` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `craftreward`
--
ALTER TABLE `craftreward`
  ADD CONSTRAINT `craftRewardRelation` FOREIGN KEY (`craftId`) REFERENCES `craft` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `fractionmember`
--
ALTER TABLE `fractionmember`
  ADD CONSTRAINT `playerFractionMemberRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `fractionpermission`
--
ALTER TABLE `fractionpermission`
  ADD CONSTRAINT `fractionRelation` FOREIGN KEY (`fractionId`) REFERENCES `fraction` (`id`);

--
-- Ograniczenia dla tabeli `fractionterritory`
--
ALTER TABLE `fractionterritory`
  ADD CONSTRAINT `fractionTerritoryRelation` FOREIGN KEY (`fractionId`) REFERENCES `fraction` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `herb`
--
ALTER TABLE `herb`
  ADD CONSTRAINT `herbUserRelation` FOREIGN KEY (`userId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `housedoor`
--
ALTER TABLE `housedoor`
  ADD CONSTRAINT `houseDoorRelation` FOREIGN KEY (`houseId`) REFERENCES `house` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `itemrelation`
--
ALTER TABLE `itemrelation`
  ADD CONSTRAINT `itemRelation` FOREIGN KEY (`itemId`) REFERENCES `item` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `accountRelation` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`);

--
-- Ograniczenia dla tabeli `playerdialog`
--
ALTER TABLE `playerdialog`
  ADD CONSTRAINT `playerDialogRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `playereffect`
--
ALTER TABLE `playereffect`
  ADD CONSTRAINT `playerEffectRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `playeritem`
--
ALTER TABLE `playeritem`
  ADD CONSTRAINT `playerItemsRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `playerlog`
--
ALTER TABLE `playerlog`
  ADD CONSTRAINT `playerLogRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `playerrelation`
--
ALTER TABLE `playerrelation`
  ADD CONSTRAINT `playerRelationedRelationRelation` FOREIGN KEY (`relationPlayerId`) REFERENCES `player` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `playerRelationsRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `playerskill`
--
ALTER TABLE `playerskill`
  ADD CONSTRAINT `playerSkillsRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `voblog`
--
ALTER TABLE `voblog`
  ADD CONSTRAINT `vobLogRelation` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `workreward`
--
ALTER TABLE `workreward`
  ADD CONSTRAINT `workRewardsRelation` FOREIGN KEY (`workId`) REFERENCES `work` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
