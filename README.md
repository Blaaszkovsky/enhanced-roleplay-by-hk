# Enhanced RolePlay by HKGroup

An improved version of the script https://gitlab.com/PMalin/hkgroup by several people. 

## Configuration
    1. Configure your database by import db-scheme with phpmyadmin. 
    2. Set in `server/mysql/connector.nut` line 169 with your data.
    3. Set items, crafts, npc, monsters and other in resources. 

## Others
- Admin VobLists - server/vob/client-scripts/vobList.nut
- Board, jobs and other systems in server/config.nut
- Edit code - https://gothicmultiplayerteam.gitlab.io/docs/0.2.1/
- Set discord activity icon and title - server/player/client-scripts/playerController.nut:129
- Set welcome message on chat - server/chat/server-scripts/chatPlayer.nut




## License

For open source projects, use as you wish. 