item.Scheme("ItRu_SDRP_ARTEFAKT_01", {
	name = "Amulet Ognistego J�zyka",
	description = "Niech Gniew Innosa wypali z�o tego �wiata!",
	value = 500,
	flag = ITEMCAT_RUNE,
	dmgType = DAMAGE_MAGIC,
	dmg = 70,
	material = 1,
	requirement = [{
			type = PlayerAttributes.Mana,
			value = 15
		},
		{
			type = PlayerAttributes.Int,
			value = 30
		},
	],
	weight = 0.2
});