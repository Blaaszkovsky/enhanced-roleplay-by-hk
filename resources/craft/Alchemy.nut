preRegisterCrafts("NAME_OF_CRAFT", {
	animation = "NONE",
	caller = CraftCaller.Alchemy, //caller from config.nut
	staminaCost = 5,
	skillRequired = PlayerSkill.Alchemy, //playerskill from config.nut
	skillValue = 1, //playerskill required points
	items = [
		["ITFO_WATER", 1], //items required to craft
		["ITCR_SDRP_DROP_15", 1],
		["ITCR_SDRP_DROP_60", 1],
	],
	rewards = [
		["ITUS_SDRP_TOOL_39", 5], //complete craft item
	],
})