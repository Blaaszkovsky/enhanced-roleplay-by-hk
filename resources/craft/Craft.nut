
local preRegisterCrafts = {};

function preRegisterCrafts(name, tab) {
    preRegisterCrafts[name] <- tab;
}

addEventHandler("onInit", function () {
    local adedAny = false;
    foreach(name, tab in preRegisterCrafts)
    {
        name = String.dePolonizer(name);

        local findName = false;

        foreach(craft in CraftController.crafts)
        {
            if(craft.name.toupper() == name.toupper()) {
                findName = true;
                break;
            }
        }

        // foreach(item in tab.items) {
        //     if(getItemScheme(item[0]) == null)
        //         throw("Brak przedmiotu w rejestrze "+item[0]);
        // }

        // foreach(item in tab.rewards) {
        //     if(item[0].find("3DS") || item[0].find("ASC") || item[0].find("MDS"))
        //         continue;

        //     if(getItemScheme(item[0]) == null)
        //         throw("Brak przedmiotu w rejestrze "+item[0]);
        // }

        if(findName)
            continue;

        local dateObject = date();
        local dateFormat = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        local tabSkillRequired = "";

        if("skillRequired" in tab) {
            tabSkillRequired = tab.skillRequired;
            if(typeof(tab.skillRequired) == "array") {
                tabSkillRequired = "";
                foreach(value in tab.skillRequired)
                    tabSkillRequired += value+",";

                if(tabSkillRequired.len() > 0)
                    tabSkillRequired = tabSkillRequired.slice(0, -1);
            }
        }

        local tabSkillValue = "";
        if("skillValue" in tab) {
            tabSkillValue = tab.skillValue;
            if(typeof(tab.skillRequired) == "array") {
                tabSkillValue = "";
                foreach(value in tab.skillValue)
                    tabSkillValue += value+",";

                if(tabSkillValue.len() > 0)
                    tabSkillValue = tabSkillValue.slice(0, -1);
            }
        }

        local fractionIds = "";
        if("fractionIds" in tab) {
            fractionIds = tab.fractionIds;
            if(typeof(tab.fractionIds) == "array") {
                fractionIds = "";
                foreach(value in tab.fractionIds) {
                    fractionIds += value+",";
                }

                if(fractionIds.len() > 0)
                    fractionIds = fractionIds.slice(0, -1);
            }
        }

        Query().insertInto(Craft.CraftTable, ["createdBy", "createdAt", "name", "animation", "caller", "staminaCost", "skillRequired", "skillValue", "fractionIds"
        ], [
            "'System'", "'"+dateFormat+"'", "'"+name+"'", "'"+tab.animation+"'", tab.caller, tab.staminaCost, "'"+tabSkillRequired+"'", "'"+tabSkillValue+"'", "'" + fractionIds + "'"
        ]).execute();

        local itemCraft = Query().select().from(Craft.CraftTable).orderBy("id DESC").one();

        foreach(item in tab.items)
            Query().insertInto(Craft.CraftItemsTable, ["craftId", "instance", "amount"], [itemCraft["id"], "'"+item[0].toupper()+"'", item[1]]).execute();

        foreach(item in tab.rewards)
            Query().insertInto(Craft.CraftRewardsTable, ["craftId", "instance", "amount"], [itemCraft["id"], "'"+item[0].toupper()+"'", item[1]]).execute();

        adedAny = true;
    }

    if(adedAny)
        exit();
})
