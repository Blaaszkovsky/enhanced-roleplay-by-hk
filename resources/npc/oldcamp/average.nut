local npc = BotHuman("Podr�nik Miejski", "OLDCAMP"), dialog = null;

npc.melee = Items.id("ITMW_SDRP_1H_SWD_81")
npc.armor = Items.id("ITAR_ARX_H2")
npc.weapon = [1000,1000,0,0]
npc.hp = 50000;
npc.hpMax = 50000;
npc.addHomePosition(-97998,168.4,-117674, 30,"S_LGUARD");
npc.visual = { bodyModel = "Hum_Body_Naked0", bodyTxt = 40, headModel = "Hum_Head_Bald", headTxt = 1311 }

dialog = npc.addDialog("Podroznik1_DIALOG", BotDialogType.Normal, "Kim jeste�?", ["Mog� przeprowadzi� Ci� przez najbardziej niebezpieczne tereny, to kosztuje 1 grosz."]);

dialog = npc.addDialog("Podroznik1_DIALOG", BotDialogType.Normal, "Zabierz mnie do kopalni. Trzymaj, oto monety.", ["Zbieraj si�"]);
dialog.addRequirement(BotDialogRequirement(BotRequirementType.ItemPosession, "ITMI_OLDCOIN", 1));
dialog.addCaller(BotDialogCaller(BotCallerType.RemoveItem, "ITMI_OLDCOIN", 1));
dialog.addCaller(BotDialogCaller(BotCallerType.CloseDialog));
dialog.addCaller(BotDialogCaller(BotCallerType.Notification, "Zap�aci�e� za podr�"));
dialog.addCaller(BotDialogCaller(BotCallerType.RunToPosition, -66453,193.438,-138954, 344.399));
dialog.addCaller(BotDialogCaller(BotCallerType.CloseDialog));

dialog = npc.addDialog("Podroznik1_DIALOG", BotDialogType.Last, "Ruszajmy", ["Ta, trzymaj si�!"]);