
BotSchemes <- {
    "Szczur": { 
        name = "Szczur",
        instance = "giant_rat",
        str = 5,
        dex = 5,
		scale = {x=0.6,y=0.6,z=0.6},
        hp = 100,
        respawnTime = 14400,
        drop = "ITCR_SDRP_DROP_37",
    },
}

function addMob(name,x,y,z,angle, world = getServerWorld()) {
    local scheme = BotSchemes[name];

    if("friendly" in scheme)
        local bot = BotMonster(scheme.name, "", "HUMAN");
    else
        local bot = BotMonster(scheme.name, "", "BEAST");

    bot.world = world
    bot.setPosition(x,y,z);
    bot.spawnPosition = {x = x, y = y, z = z}
    bot.angle = angle
    bot.instance = scheme.instance;
    bot.hpMax = scheme.hp;
    bot.hp = scheme.hp;
    bot.str = scheme.str;
    bot.dex = scheme.dex;
    bot.respawnTime = scheme.respawnTime;



    if("scale" in scheme)
        bot.scale = scheme.scale;

    if("melee" in scheme)
        bot.melee = Items.id(scheme.melee);

    if("instantDrop" in scheme)
        bot.addInstantDrop(scheme.instantDrop);

    if("drop" in scheme)
        bot.addDrop(scheme.drop);

    return bot;
}