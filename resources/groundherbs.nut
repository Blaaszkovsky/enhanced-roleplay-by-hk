

local preRegisterGroundHerbs = {};

function preRegisterGroundHerb(name, x,y,z, range) {
    if(!(name in preRegisterGroundHerbs))
        preRegisterGroundHerbs[name] <- [];

    preRegisterGroundHerbs[name].push({x = x, y = y, z = z, range = range});
}

addEventHandler("onInit", function () {
    setTimer(addOrAbortGroundHerbs, 5000, 1);
})

function addOrAbortGroundHerbs() {
    local existingGroundHerbs = getGroundHerbs(), needToAbort = false;

    foreach(nameOfIndex, indexes in preRegisterGroundHerbs) {
        foreach(v,k in indexes) {
            local exist = false;

            foreach(herb in existingGroundHerbs) {
                if(herb.position.x == k.x && herb.position.y == k.y && herb.position.z == k.z) {
                    exist = true;
                    break;
                }
            }

            if(exist == false) {
                Query().insertInto(GroundHerb.GroundHerbTable, ["scheme", "rangeDistance", "x", "y", "z"], ["'"+nameOfIndex+"'", k.range, k.x, k.y, k.z]).execute();
                needToAbort = true;
            }
        }
    }

    if(needToAbort)
        exit();
}