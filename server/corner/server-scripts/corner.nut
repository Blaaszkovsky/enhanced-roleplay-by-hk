class Corner {
    static CornerTable = "corner";

    id = -1;
    position = null;

    name = -1;
    status = -1;

    money = null;
    animation = "";
    itemToTrade = "";
    timeToNewResp = 0;

    constructor(_id) {
        id = _id;

        position = {
            x = 0,
            y = 0,
            z = 0,
            angle = 0,
        };

        name = 0;
        status = 0;
        money = {
            min = 0,
            max = 0,
        }
        animation = "S_LGUARD";
        itemToTrade = "";
    }


    function addBotToWorld(id) {
        local object = BotHuman(getBotScheme(id).name, "CORNER");
        local dialog = null;
        local moneyToGive = randomGiveMoney(getBotScheme(id).moneyMin, getBotScheme(id).moneyMax);
        local itemToTrade = getBotScheme(id).itemToTrade;

        object.isTemporary = true;
        object.setPosition(getBotScheme(id).x,getBotScheme(id).y,getBotScheme(id).z);
        object.setAngle(getBotScheme(id).angle);
        object.str = 50;
        object.dex = 200;
        object.hp = 2500;
        object.hpMax = 2500;
        object.armor = Items.id("ITAR_SDRP_CLH_17");
        object.melee = Items.id("ITMW_1H_BAU_MACE");

        for(local i = 0; i <= getMaxSlots(); i ++) {
            if(isPlayerSpawned(i))
                callEvent("onPlayerChangeCell", i, getPlayer(i).cell, getPlayer(i).cell);
        }

        dialog = object.addDialog(getBotScheme(id).name + "_" + id + "_rozmowa", BotDialogType.Normal, "Kim jeste�?", ["Nie interesuj si�, spadaj st�d."]);
        dialog.addCaller(BotDialogCaller(BotCallerType.CloseDialog));

        dialog = object.addDialog(getBotScheme(id).name + "_" + id + "_handel", BotDialogType.Normal, "Masz tu dzia�k� narkotyku: " + getItemName(itemToTrade), ["Dawaj to i si� nie znamy."]);
        dialog.addRequirement(BotDialogRequirement(BotRequirementType.ItemPosession, itemToTrade, 1));
        dialog.addCaller(BotDialogCaller(BotCallerType.RemoveItem, itemToTrade, 1));
        dialog.addCaller(BotDialogCaller(BotCallerType.AddItem, "ITMI_OLDCOIN", moneyToGive));
        dialog.addCaller(BotDialogCaller(BotCallerType.CloseDialog));
        dialog.addCaller(BotDialogCaller(BotCallerType.Notification, "Otrzyma�e� w transakcji: " + moneyToGive + " groszy"));
        dialog.addCaller(BotDialogCaller(BotCallerType.CloseDialog));
        dialog.addRequirement(BotDialogRequirement(BotRequirementType.Time, getBotScheme(id).name + "_" + id + "_handel", 1800));
    }


    function randomGiveMoney(min, max) {
        if(min > max) {
            local temp = min;
            min = max;
            max = temp;
        }

        return rand() % (max - min + 1) + min;
    }

    function removeBotFromWorld() {
        foreach(bot in getBots()) {
            if(bot.group == "CORNER") {
                bot.destroyBot();
            }
        }
    }

    function getBotScheme(id) {
        return Query().select().from(Corner.CornerTable).where(["id = " + id]).one();
    }


    static function onMinuteCheckBot() {
        local realTime = date();
        local realHour = format("%02d",realTime.hour) + ":" + format("%02d",realTime.min);
        local tempIdxTable = [];

        if(realHour == "20:59" || realHour == "21:59" || realHour == "22:59" || realHour == "23:59") {
            removeBotFromWorld();
        }

        if(realHour == "20:00" || realHour == "21:00" || realHour == "22:00" || realHour == "23:00") {
            foreach(idx, _ in cornerController.corners) {
                tempIdxTable.push(idx);
            }

            local randomBotIndex = rand() % tempIdxTable.len();
            foreach(idx, botIdx in tempIdxTable) {
                if(idx == randomBotIndex) {
                    addBotToWorld(botIdx);
                }
            }
        }

        
    }
}

setTimer(Corner.onMinuteCheckBot.bindenv(Corner), 1000 * 60, 0);
