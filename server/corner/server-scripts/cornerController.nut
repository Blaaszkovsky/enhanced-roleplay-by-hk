class cornerController {

    static corners = {};

    static function onInit() {

        local cornersInDatabase = Query().select().from(Corner.CornerTable).all();
        foreach(cornerObject in cornersInDatabase)
        {
            local newCorner = Corner(cornerObject.id.tointeger())

            newCorner.name = cornerObject.name;
            newCorner.status = cornerObject.status.tointeger();
            newCorner.position = {x = cornerObject.x.tofloat(), y = cornerObject.y.tofloat(), z = cornerObject.z.tofloat(), angle = cornerObject.angle.tofloat()},
            newCorner.money = {min = cornerObject.moneyMin, max = cornerObject.moneyMax}
            newCorner.animation = cornerObject.animation,
            newCorner.itemToTrade = cornerObject.itemToTrade,

            corners[newCorner.id] <- newCorner;
        }

        addBotsToTable()

        print("We have " + corners.len() + " corner bot.");
    }

    static function addBotsToTable() {
        local cornersInDatabase = Query().select().from(Corner.CornerTable).all();
        foreach(cornerBot in Config["CornerSystem"]) {

            local findBot = false;

            foreach(cornerDatabase in cornersInDatabase) {
                if(cornerDatabase.x == cornerBot.position.x) {
                    findBot = true;
                    break;
                }
            }

            if(findBot)
                continue

            Query().insertInto(Corner.CornerTable, 
                ["name", "status", "x", "y", "z", "angle", "moneyMin", "moneyMax", "animation", "itemToTrade"], 
                ["'"+cornerBot.name+"'", 0, cornerBot.position.x, cornerBot.position.y, cornerBot.position.z, cornerBot.position.angle, cornerBot.money.min, cornerBot.money.max, "'"+cornerBot.animation+"'", "'"+cornerBot.itemToTrade+"'"]
            ).execute();
        }
    }
}


addEventHandler("onInit", cornerController.onInit.bindenv(cornerController))