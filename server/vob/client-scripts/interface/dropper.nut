
VobUIDropper <- {
    startPosition = null,
    keyHandlerRegister = null,
    operationVob = null,

    lines = [],
    draws = [],

    hide = function()
    {
        lines = [];
        draws = [];
        operationVob = null;
        BaseGUI.hide();
        ActiveGui = null;

        removeEventHandler("onKey", keyHandlerRegister)
    }

    show = function(_operationVob)
    {
        local pos = getPlayerPosition(heroId);

        startPosition = pos;
        operationVob = _operationVob;

        draws = [
            Draw(anx(Resolution.x - 300), any(Resolution.y/2 - 200), "W/S/A/D/Q/E - poruszanie."),
            Draw(anx(Resolution.x - 300), any(Resolution.y/2 - 170), "Z/X - rotacja lewo / prawo."),
            Draw(anx(Resolution.x - 300), any(Resolution.y/2 - 140), "M/N - rotacja g�ra / d�."),
            Draw(anx(Resolution.x - 300), any(Resolution.y/2 - 100), "Enter zatwierd�."),
        ]

        foreach(draw in draws)
        {
            draw.font = "FONT_OLD_20_WHITE_HI.TGA";
			draw.setScale(0.5, 0.5);
            draw.visible = true;
        }

        operationVob.loseMovable();

        lines = [];

        local degreePerButton = 360/120;
        local points = [];

        for(local i = 0; i <= 120; i ++)
        {
            local pos = getPlayerPosition(heroId);
            local rotation = degreePerButton * i;

            pos.x = pos.x + (sin(rotation * 3.14 / 180.0) * 300);
            pos.z = pos.z + (cos(rotation * 3.14 / 180.0) * 300);

            points.append({x = pos.x, z = pos.z});
        }

        for(local i = 0; i < points.len() - 2; i = i + 2)
        {
            local pos = getPlayerPosition(heroId);
            local our = points[i];
            local next = points[i + 1];
            lines.append(Line3d(our.x, pos.y, our.z, next.x, pos.y, next.z));
        }

        foreach(line in lines)
        {
            line.setColor(0, 200, 100);
            line.visible = true;
        }

        ActiveGui = true;
        BaseGUI.show();

        keyHandlerRegister = VobUIDropper.onKey.bindenv(VobUIDropper)
        addEventHandler("onKey", keyHandlerRegister)
    }

    onKey = function(key)
    {
        if(operationVob == null)
            return;

        local position = operationVob.position;
        local rotation = operationVob.rotation;
        switch(key)
        {
            case KEY_Z:
                rotation.x = rotation.x - 10;
            break;
            case KEY_X:
                rotation.x = rotation.x + 10;
            break;
            case KEY_M:
                rotation.y = rotation.y - 10;
            break;
            case KEY_N:
                rotation.y = rotation.y + 10;
            break;
            case KEY_A:
                position.x = position.x - 20;
            break;
            case KEY_D:
                position.x = position.x + 20;
            break;
            case KEY_S:
                position.z = position.z - 20;
            break;
            case KEY_W:
                position.z = position.z + 20;
            break;
            case KEY_Q:
                position.y = position.y - 20;
            break;
            case KEY_E:
                position.y = position.y + 20;
            break;
            case KEY_RETURN:
                operationVob.dropped = true;
                operationVob.position = operationVob.object.getPosition();
                operationVob.rotation = operationVob.object.getRotation();
                VobPacket(operationVob).setMovable();
                hide();
                return;
            break;
        }

        if(getDistance3d(startPosition.x, startPosition.y, startPosition.z, position.x, position.y, position.z) > 300)
        {
            foreach(line in lines)
                line.setColor(255, 0, 100);

            return;
        }

        foreach(line in lines)
            line.setColor(0, 200, 100);

        operationVob.position = position;
        operationVob.rotation = rotation;

        operationVob.object.setPosition(position.x, position.y, position.z);
        operationVob.object.setRotation(rotation.x, rotation.y, rotation.z);

        if(position.y > (getPlayerPosition(heroId).y + 100))
            operationVob.object.floor();
    }
}
