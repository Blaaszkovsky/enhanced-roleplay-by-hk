
VobUIOpener <-
{
    _vob = null
    bucket = null

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 200), anx(400), any(430), "SG_BOX.TGA", null, false)
        result.uis <- [];
        result.renders <- [];
        result.buttons <- [];
        result.draw <- Draw(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 230), "");

        result.slider <- GUI.ScrollBar(anx(Resolution.x/2 + 150), any(Resolution.y/2 - 180), anx(35), any(390), "SG_SCROLL.TGA", "SG_BUTTON.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical);

        result.bindForScrollbar <- Bind.onChange(result.slider, function(value) {
            VobUIOpener.refresh();
        }, PlayerGUI.VobOpener);

        return result;
    }

    refresh = function()
    {
        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        bucket.buttons.clear();
        bucket.renders.clear();

        local _items = Hero.inventory.getItems();
        local passedItems = 0;
        local marginVisible = bucket.slider.getValue() * 3;

        local x = 0;
        local y = 0;

        bucket.draw.text = "Wymagany klucz: "+_vob.guid;
        bucket.draw.visible = true;

        foreach(_item in _items) {
            passedItems = passedItems + 1;

            if(marginVisible > passedItems)
                continue;

            local butt = GUI.Button(anx(15 + (x * 105)), any(15 + (y * 105)), anx(100), any(100), "WHITE.TGA", "", bucket.window);
            local render = ItemRender(anx(Resolution.x/2 - 200) + anx(15 + (x * 105)), any(Resolution.y/2 - 200) + any(15 + (y * 105)), anx(100), any(100), _item.instance);
            butt.attribute = _item;

            if(_item.guid == _vob.guid)
                butt.setColor(3,40,50);
            else if(_item.guid != "" && _item.guid != "-1")
                butt.setColor(20,10,3);
            else
                butt.setColor(0,0,0);

            butt.bind(EventType.Click, function(element) {
                VobUIOpener.selectItem(element.attribute);
            });

            bucket.buttons.push(butt);
            bucket.renders.push(render);

            x = x + 1;
            if(x >= 3) {
                x = 0;
                y = y + 1
            }

            if(y == 4)
                break;
        }

        foreach(butt in bucket.buttons)
            butt.setVisible(true);

        foreach(butt in bucket.renders) {
            butt.visible = true;
            butt.top();
        }
    }

    selectItem = function(_item) {
        bucket.uis.clear();

        if("usebutton" in bucket) {
            bucket.usebutton.setVisible(false);
            bucket.usebutton.remove();
        }

        local cursorPosition = getCursorPosition();
        local draw = Draw(cursorPosition.x + anx(15), cursorPosition.y + any(15), _item.name +"\n\n"+_item.getFullStringDescription());
        bucket.uis.push(draw);

        local texture = Texture(cursorPosition.x, cursorPosition.y, draw.width + anx(30), draw.height + any(90), "BLACK.TGA");
        bucket.uis.push(texture);
        texture.visible = true;
        draw.visible = true;
        draw.top();

        if(_item.guid == _vob.guid) {
            local bbutton = GUI.Button(cursorPosition.x + anx(15), cursorPosition.y + draw.height + any(30), draw.width, any(45), "BLACK.TGA", "U�yj");

            bbutton.bind(EventType.Click, function(element) {
                VobController.cachedOpeners[VobUIOpener._vob.id] <- getTickCount() + (1000 * 120);
                VobUIOpener.hide();
                addPlayerNotification(heroId, "U�yto klucza, mo�esz otworzy�.");
            });

            bbutton.setVisible(true);
            bucket.usebutton <- bbutton;
        }else if(_item.instance == "ITKE_LOCKPICK" && Hero.skills[PlayerSkill.Thief] >= 200) {
            local bbutton = GUI.Button(cursorPosition.x + anx(15), cursorPosition.y + draw.height + any(30), draw.width, any(45), "BLACK.TGA", "U�yj");

            bbutton.bind(EventType.Click, function(element) {
                VobUIThief._vob = VobUIOpener._vob;
                VobUIOpener.hide();
                VobUIThief.show();
                addPlayerNotification(heroId, "Pr�bujesz otworzy� wytrychem.");
            });

            bbutton.setVisible(true);
            bucket.usebutton <- bbutton;
        }
    }

    show = function(operationVob)
    {
        _vob = operationVob;
        bucket = prepareWindow();
        refresh();

        bucket.window.setVisible(true);
        bucket.slider.setVisible(true);

        BaseGUI.show();
        ActiveGui = PlayerGUI.VobOpener;
    }

    hide = function()
    {
        BaseGUI.hide();
        ActiveGui = null;

        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);
        bucket.window.destroyAll();
        bucket.slider.destroy();
        bucket = null;

        playAni(heroId,"T_IGET_2_STAND");

        _vob = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.VobOpener)
            hide();
    }

    onRemove = function(vobId)
    {
        if(_vob == null)
            return;

        if(_vob.id == vobId)
            hide();
    }

    onRender = function()
    {
        if(ActiveGui != PlayerGUI.VobOpener)
            return;

        playAni(heroId,"S_BOOK_S1");
    }
}

addEventHandler("onVobRemove", VobUIOpener.onRemove.bindenv(VobUIOpener));
addEventHandler("onForceCloseGUI", VobUIOpener.onForceCloseGUI.bindenv(VobUIOpener));
addEventHandler("onRender", VobUIOpener.onRender.bindenv(VobUIOpener));
Bind.addKey(KEY_ESCAPE, VobUIOpener.hide.bindenv(VobUIOpener), PlayerGUI.VobOpener)
