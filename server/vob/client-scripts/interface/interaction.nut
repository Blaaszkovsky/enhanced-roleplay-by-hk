VobUIInteraction <- {
    operationVob = null,

    show = function(_vob)
    {
        operationVob = _vob;

        operationVob.inventory.ui = item.Bucket.UI(operationVob.inventory);
        operationVob.inventory.ui.setConnection(Hero.inventory);
        operationVob.inventory.ui.setTitle("Skrzynia");
        operationVob.inventory.ui.setVisible(true, ItemGUISide.Left);

        Hero.inventory.ui.setTitle("Ekwipunek");
        Hero.inventory.ui.setConnection(operationVob.inventory);
        Hero.inventory.ui.setVisible(true, ItemGUISide.Right);

        BaseGUI.show();
        ActiveGui = PlayerGUI.VobInteraction;
    }

    hide = function()
    {
        BaseGUI.hide();
        ActiveGui = null;

        operationVob.inventory.ui.setVisible(false);
        operationVob.inventory.ui = null
        Hero.inventory.ui.setVisible(false);

        VobPacket(operationVob).setOperation();
        operationVob = null;
        playAni(heroId,"T_IGET_2_STAND");
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.VobInteraction)
            hide();
    }

    onRemove = function(vobId)
    {
        if(operationVob == null)
            return;

        if(operationVob.id == vobId)
            hide();
    }

    onRender = function()
    {
        if(ActiveGui != PlayerGUI.VobInteraction)
            return;

        playAni(heroId,"S_CHESTBIG_S1");
    }
}

addEventHandler("onVobRemove", VobUIInteraction.onRemove.bindenv(VobUIInteraction));
addEventHandler("onRender", VobUIInteraction.onRender.bindenv(VobUIInteraction));
addEventHandler("onForceCloseGUI", VobUIInteraction.onForceCloseGUI.bindenv(VobUIInteraction));
Bind.addKey(KEY_ESCAPE, VobUIInteraction.hide.bindenv(VobUIInteraction), PlayerGUI.VobInteraction)

/*
VobUIInteraction <- {
    bucket = {},
    operationVob = null,

    init = function()
    {
        bucket.extra <- [];

        bucket.window <- GUI.Window(anx(Resolution.x/2 - 100), any(Resolution.y - 200), anx(200), any(80), "SG_BOX.TGA");
        GUI.Draw(50, 7900, "Aby zobaczy� opis przedmiotu kliknij prawym przyciskiem myszy na jego slot.", bucket.window);
        bucket.inventory <- Equipment("Ekwipunek");
        bucket.container <- Equipment("Skrzynia");

        bucket.inventory.onLeftClickObject = function(obj) {
            VobUIInteraction.bucket.extra.clear();
            if(isKeyPressed(KEY_LSHIFT)) {
                if(VobUIInteraction.bucket.inventory.hasItem(obj.instance) > 30)
                    VobUIInteraction.operationVob.addItem(obj.instance, 30);
                else
                    VobUIInteraction.operationVob.addItem(obj.instance, VobUIInteraction.bucket.inventory.hasItem(obj.instance));
            }else
                VobUIInteraction.operationVob.addItem(obj.instance, 1);
        }

        bucket.container.onLeftClickObject = function(obj) {
            VobUIInteraction.bucket.extra.clear();
            if(isKeyPressed(KEY_LSHIFT))
                if(VobUIInteraction.operationVob.items[obj.instance] > 30)
                    VobUIInteraction.operationVob.deleteItem(obj.instance, 30);
                else
                    VobUIInteraction.operationVob.deleteItem(obj.instance, VobUIInteraction.operationVob.items[obj.instance]);
            else
                VobUIInteraction.operationVob.deleteItem(obj.instance, 1);
        };

        bucket.inventory.onRightClickObject = function(obj) {
            VobUIInteraction.showDescription(obj.instance);
        }

        bucket.container.onRightClickObject = function(obj) {
            VobUIInteraction.showDescription(obj.instance);
        }

        bucket.exitButton <- GUI.Button(anx(10), any(10), anx(180), any(60), "SG_BUTTON.TGA", "Zamknij", bucket.window);
        bucket.exitButton.bind(EventType.Click, function(element) {
            VobUIInteraction.hide();
        });


        bucket.weightEquipment <- GUI.Draw(255, 255, "Waga", bucket.window);
        bucket.weightEquipment.setColor(225,225,225);

        if(getSetting("OtherCustomFont")) {
            bucket.weightEquipment.setFont("FONT_OLD_20_WHITE_HI.TGA");
            bucket.weightEquipment.setScale(0.6, 0.6);
        }

        bucket.weightContainer <- GUI.Draw(255, 255, "Waga", bucket.window);
        bucket.weightContainer.setColor(225,225,225);

        if(getSetting("OtherCustomFont")) {
            bucket.weightContainer.setFont("FONT_OLD_20_WHITE_HI.TGA");
            bucket.weightContainer.setScale(0.6, 0.6);
        }

        bucket.inventory.left = 50;
        bucket.inventory.top = Resolution.y/2 - 256;

        bucket.container.left = Resolution.x - 562;
        bucket.container.top = Resolution.y/2 - 256;
    }

    refresh = function()
    {
        if(ActiveGui != PlayerGUI.VobInteraction)
            return;

        bucket.inventory.setBucket(getEq());

        local vobItems = [];
        foreach(instance, amount in operationVob.items)
            vobItems.append({instance = instance, amount = amount});

        bucket.container.setBucket(vobItems);

        bucket.weightEquipment.setText("Waga: "+getInventoryWeight()+"/"+getMaxInventoryWeight());
        bucket.weightContainer.setText("Waga: "+operationVob.itemsWeight()+"/"+operationVob.weight);

        bucket.inventory.showItemsWithIndex();
        bucket.container.showItemsWithIndex();
    }

    show = function(vob)
    {
        operationVob = vob;

        bucket.inventory.setBucket(getEq());

        local vobItems = [];
        foreach(instance, amount in vob.items)
            vobItems.append({instance = instance, amount = amount});

        bucket.container.setBucket(vobItems);

        bucket.weightEquipment.setText("Waga: "+getInventoryWeight()+"/"+getMaxInventoryWeight());
        bucket.weightContainer.setText("Waga: "+operationVob.itemsWeight()+"/"+operationVob.weight);

        bucket.weightEquipment.setPosition(anx(100), any(Resolution.y/2 + 340));
        bucket.weightContainer.setPosition(anx(Resolution.x - 512), any(Resolution.y/2 + 340));

        bucket.window.setVisible(true);
        bucket.inventory.setVisible(true);
        bucket.container.setVisible(true);
        bucket.extra.clear();

        BaseGUI.show();
        ActiveGui = PlayerGUI.VobInteraction;
    }

    hide = function()
    {
        bucket.window.setVisible(false);
        bucket.inventory.setVisible(false);
        bucket.container.setVisible(false);
        bucket.extra.clear();

        VobPacket(operationVob).setOperation();
        operationVob = null;
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"T_IGET_2_STAND");
    }

    showDescription = function(instance)
    {
        local cursor = getCursorPosition();
        local item = getItem(instance);
        if(item == null)
            return;

        bucket.extra = [
            Draw(anx(Resolution.x - 200), any(Resolution.y/2 - 220), item.name+"\n\n"+item.getFullStringDescription()),
            Texture(anx(0), any(0), anx(0), any(0), "SG_BOX.TGA")
        ]

        if(getSetting("OtherCustomFont")) {
            bucket.extra[0].font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.extra[0].setScale(0.45, 0.45);
        }

        bucket.extra[0].setPosition(cursor.x + 50, cursor.y + 50)
        bucket.extra[0].visible = true;
        bucket.extra[1].setPosition(cursor.x, cursor.y);
        bucket.extra[1].setSize(bucket.extra[0].width + 100, bucket.extra[0].height + 100);
        bucket.extra[1].visible = true;
        bucket.extra[0].top();
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.VobInteraction)
            hide();
    }

    onRemove = function(vobId)
    {
        if(operationVob == null)
            return;

        if(operationVob.id == vobId)
            hide();
    }

    onRender = function()
    {
        if(ActiveGui != PlayerGUI.VobInteraction)
            return;

        playAni(heroId,"S_CHESTBIG_S1");
    }
}


*/