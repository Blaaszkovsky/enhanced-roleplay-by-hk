
class VobPacket
{
    packet = null;
    object = null;

    constructor(vobObject)
    {
        packet = ExtendedPacket(Packets.Vob);
        object = vobObject;
    }

    function hit() {
        packet.writeUInt8(VobPackets.Health)
        packet.writeInt16(object.id);
        packet.send();
    }

    function remove() {
        packet.writeUInt8(VobPackets.Remove);
        packet.writeInt16(object.id);
        packet.send();
    }

    function setMovable() {
        packet.writeUInt8(VobPackets.Movable)
        packet.writeInt16(object.id);
        packet.send();
    }

    function setOperation() {
        packet.writeUInt8(VobPackets.Operation)
        packet.writeInt16(object.id);
        packet.send();
    }

    function rebuild() {
        packet.writeUInt8(VobPackets.Rebuild)
        packet.writeInt16(object.id);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.send();
    }

    function doorInteraction() {
        packet.writeUInt8(VobPackets.DoorInteraction)
        packet.writeInt16(object.id);
        packet.send();
    }

    function wrongVobComplexity() {
        packet.writeUInt8(VobPackets.Complexity);
        packet.writeInt16(object.id);
        packet.writeBool(false);
        packet.send();
    }
}

