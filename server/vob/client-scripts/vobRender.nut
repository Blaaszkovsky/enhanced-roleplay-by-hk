
VobRender <- {
    cache = {},

    control = function() {
        local hPosition = getPlayerPosition(heroId);

        foreach(vobId, _ in cache) {
            local vob = getVob(vobId);
            if(vob == null) {
                cache.rawdelete(vobId);
                return;
            }

            if(getPositionDifference(hPosition, vob.position) > 1500) {
                cache.rawdelete(vobId);
                return;
            }
        }

        foreach(vobId, vob in VobController.vobs) {
            if(vobId in cache)
                continue;

            if(getPositionDifference(hPosition, vob.position) <= 1500) {
                cache[vobId] <- true;
                vob.refreshPosition();
            }
        }
    }
}