
class VobController
{
    static OPERATION_MOVABLE = 1;
    static OPERATION_VOURNABLE = 2;
    static OPERATION_CONTAINER = 3;
    static OPERATION_DOOR = 4;
    static OPERATION_SWITCH = 5;

    static vobs = {};
    static operationOnVobs = {};
    static movable = {};
    static cachedOpeners = {};

    static function onPlayerChangeAnim(anim) {
        local animAcceptedAsHit = [
            "S_FISTATTACK",
            "S_FISTATTACKMOVE",
            "T_1HATTACKL",
            "T_1HATTACKR",
            "S_1HATTACK",
            "T_2HATTACKL",
            "T_2HATTACKR",
            "S_2HATTACK",
        ]

        if(animAcceptedAsHit.find(anim) != null)
        {
            foreach(vobId, modes in operationOnVobs)
            {
                if(modes.find(OPERATION_VOURNABLE) == null)
                    continue;

                if(!(vobId in vobs))
                {
                    operationOnVobs.rawdelete(vobId);
                    continue;
                }
                if(getPositionDifference(getPlayerPosition(heroId),getVob(vobId).position) < 300)
                {
                    VobPacket(vobs[vobId]).hit();
                    break;
                }
            }
        }
    }

    static function onDamage(struct)
    {
        if(Hero.movingVob == false)
            return;

        foreach(vobId, playerMovable in movable)
            if(playerMovable == heroId)
                VobPacket(vobs[vobId]).setMovable();
    }

    static function onRender() {
        foreach(vobId, playerMovable in movable)
            if(isPlayerStreamed(playerMovable))
                vobs[vobId].moveWithPlayer(playerMovable);

        foreach(vobId, vob in vobs)
            if(vob.changingState != null)
                vob.moveWithChangingState();
    }

    static function onKey(key) {
        if (chatInputIsOpen())
		    return

        if (isConsoleOpen())
            return

        if(ActiveGui != null)
            return;

        if(getPlayerWeaponMode(heroId) != 0)
            return;

        switch(key)
        {
            case KEY_RETURN:
                if(Hero.movingVob == false)
                    return;

                foreach(vobId, playerMovable in movable) {
                    if(playerMovable == heroId) {
                        VobUIDropper.show(vobs[vobId]);
                        return;
                    }
                }
            break;
            case KEY_X:
                if(Hero.movingVob)
                    return;

                local time = date();
                if(time.hour >= 3 && time.hour < 10) {
                    addPlayerNotification(heroId, "Godziny w ktorych zablokowali�my przenoszenie 3:00-10:00");
                    return;
                }

                foreach(vobId, modes in operationOnVobs)
                {
                    if(modes.find(OPERATION_MOVABLE) == null)
                        continue;

                    if(!(vobId in vobs))
                    {
                        operationOnVobs.rawdelete(vobId);
                        continue;
                    }

                    if(getVob(vobId).isPlayerBeforeVob(heroId))
                    {
                        local vob = vobs[vobId];

                        if(vob.movable == VobMovable.Active && vob.movablePlayer == -1)
                        {
                            playAni(heroId,"T_PLUNDER");

                            setTimer(function(obj) {
                                VobPacket(obj).setMovable();
                            }, 500, 1, vob);
                        }

                        break;
                    }
                }
            break;
            case KEY_LCONTROL:
                foreach(vobId, modes in operationOnVobs)
                {
                    if(modes.find(OPERATION_CONTAINER) == null && modes.find(OPERATION_DOOR) && modes.find(OPERATION_SWITCH))
                        continue;

                    if(!(vobId in vobs))
                    {
                        operationOnVobs.rawdelete(vobId);
                        continue;
                    }

                    local vob = vobs[vobId];

                    if(vob.type == VobType.Static || vob.type == VobType.Interaction)
                        continue;

                    if(getPositionDifference(getPlayerPosition(heroId),vob.position) < vob.getDistanceToInteract())
                    {
                        if(vob.isBed()) {
                            useClosestMob(heroId, "BEDHIGH", 1);
                            continue;
                        }

                        foreach(vobId, opener in cachedOpeners) {
                            if(opener < getTickCount())
                                cachedOpeners.rawdelete(vobId);
                        }

                        if(vob.guid != null && vob.guid != "") {
                            if(!(vob.id in cachedOpeners))
                            {
                                VobUIOpener.show(vob);
                                return;
                            }
                        }

                        if(vob.type == VobType.Door) {
                            VobPacket(vob).doorInteraction();
                            vob.playInteractionAnimation();
                        } else if(vob.type == VobType.Switch) {
                            VobPacket(vob).doorInteraction();
                        } else {
                            VobPacket(vob).setOperation();
                            playAni(heroId,"S_CHESTBIG_S1");
                        }
                        break;
                    }
                }
            break;
        }
    }

    static function onVobInteract(vobId) {
        if(!(vobId in operationOnVobs))
            return;

        local vob = vobs[vobId];
        local modes = operationOnVobs[vobId];

        if(vob.type == VobType.Static || vob.type == VobType.Interaction)
            return;

        foreach(vobId, opener in cachedOpeners) {
            if(opener < getTickCount())
                cachedOpeners.rawdelete(vobId);
        }

        if(vob.guid != null && vob.guid != "") {
            if(!(vob.id in cachedOpeners))
            {
                VobUIOpener.show(vob);
                return;
            }
        }

        if(vob.type == VobType.Door) {
            VobPacket(vob).doorInteraction();
            vob.playInteractionAnimation();
        } else if(vob.type == VobType.Switch) {
            VobPacket(vob).doorInteraction();
        } else {
            VobPacket(vob).setOperation();
            playAni(heroId,"S_CHESTBIG_S1");
        }
    }

    static function onSecond() {
        VobRender.control();
    }

    static function toggleDraws(visible)
    {
        foreach(vob in vobs)
        {
            if (vob.world != getWorld())
                continue

            if (vob.hpMode == VobHealth.Vulnerable)
                vob.draw.visible = visible
        }
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case VobPackets.Create:
                local id = packet.readInt16();
                local type = packet.readInt16();
                local dynamic = packet.readBool();
                local name = packet.readString();
                local visual = packet.readString();
                local movable = packet.readInt16();
                local movablePlayer = packet.readInt16();
                local trigger = packet.readInt16();
                local triggerType = packet.readInt16();
                local triggerAnimation = packet.readInt16();
                local triggerTime = packet.readInt16();
                local hpMode = packet.readInt16();
                local hp = packet.readInt32();
                local hpMax = packet.readInt32();
                local opened = packet.readInt16();
                local alpha = packet.readFloat();
                local position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local targetPosition = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local targetRotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local weight = packet.readFloat();
                local guid = packet.readString();
                local complexity = packet.readUInt8();
                local operationPlayer = packet.readInt16();
                local createdBy = packet.readString();
                local world = packet.readString();

                local vob = GameVob(id);
                vob.position = position;
                vob.rotation = rotation;
                vob.targetPosition = targetPosition;
                vob.targetRotation = targetRotation;
                vob.trigger = trigger;
                vob.triggerType = triggerType;
                vob.triggerAnimation = triggerAnimation;
                vob.triggerTime = triggerTime;
                vob.alpha = alpha;
                vob.hpMax = hpMax;
                vob.hp = hp;
                vob.hpMode = hpMode;
                vob.movablePlayer = movablePlayer;
                vob.movable = movable;
                vob.visual = visual;
                vob.name = name;
                vob.opened = opened;
                vob.dynamic = dynamic;
                vob.type = type;
                vob.weight = weight;
                vob.guid = guid;
                vob.complexity = complexity;
                vob.operationPlayer = operationPlayer;
                vob.createdBy = createdBy;
                vob.world = world
                vob.createObject();

                vobs[id] <- vob;

                if(vob.hpMode == VobHealth.Vulnerable)
                {
                    if(!(id in operationOnVobs))
                        operationOnVobs[id] <- [OPERATION_VOURNABLE];
                    else
                        operationOnVobs[id].append(OPERATION_VOURNABLE);
                }

                if(vob.movable == VobMovable.Active)
                {
                    if(!(id in operationOnVobs))
                        operationOnVobs[id] <- [OPERATION_MOVABLE];
                    else
                        operationOnVobs[id].append(OPERATION_MOVABLE);
                }

                if(vob.type == VobType.Container)
                {
                    if(!(id in operationOnVobs))
                        operationOnVobs[id] <- [OPERATION_CONTAINER];
                    else
                        operationOnVobs[id].append(OPERATION_CONTAINER);
                }

                if(vob.type == VobType.Door)
                {
                    if(!(id in operationOnVobs))
                        operationOnVobs[id] <- [OPERATION_DOOR];
                    else
                        operationOnVobs[id].append(OPERATION_DOOR);
                }

                if(vob.type == VobType.Switch)
                {
                    if(!(id in operationOnVobs))
                        operationOnVobs[id] <- [OPERATION_SWITCH];
                    else
                        operationOnVobs[id].append(OPERATION_SWITCH);
                }

                callEvent("onVobAdd", vob.id);
            break;
            case VobPackets.Rebuild:
                local id = packet.readInt16();
                local position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}

                vobs[id].position = position;
                vobs[id].rotation = rotation;

                if(id in VobController.movable)
                    VobController.movable.rawdelete(id);

                vobs[id].createObject();
            break;
            case VobPackets.Remove:
                local id = packet.readInt16();
                vobs[id].onRemove();
                vobs.rawdelete(id);

                callEvent("onVobRemove", id);
            break;
            case VobPackets.Position:
                local id = packet.readInt16();
                vobs[id].setPosition(packet.readFloat(), packet.readFloat(), packet.readFloat())
            break;
            case VobPackets.Rotation:
                local id = packet.readInt16();
                vobs[id].setRoation(packet.readFloat(), packet.readFloat(), packet.readFloat())
            break;
            case VobPackets.Movable:
                local id = packet.readInt16();
                vobs[id].setMovable(packet.readInt16());
            break;
            case VobPackets.ChangeMovable:
                local id = packet.readInt16();
                vobs[id].movable = packet.readInt16();
            break;
            case VobPackets.Health:
                local id = packet.readInt16();
                vobs[id].setHealth(packet.readInt32());
            break;
            case VobPackets.NewGuid:
                local id = packet.readInt16();
                vobs[id].setGuid(packet.readString());
            break;
            case VobPackets.Operation:
                local id = packet.readInt16();
                local mode = packet.readBool();
                local vob = vobs[id];
                local itemBucketId = packet.readInt32();

                if(itemBucketId != -1)
                    vob.inventory = item.Controller.createBucket(itemBucketId);

                vob.setOperation(mode);
            break;
            case VobPackets.DoorInteraction:
                local id = packet.readInt16();
                local openedValue = packet.readInt16();
                vobs[id].setOpened(openedValue);
            break;
            case VobPackets.Complexity:
                local id = packet.readInt16();
                vobs[id].complexity = packet.readUInt8();
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Vob, VobController.onPacket.bindenv(VobController));
addEventHandler("onPlayerChangeAnim", VobController.onPlayerChangeAnim.bindenv(VobController));
addEventHandler("onVobInteract", VobController.onVobInteract.bindenv(VobController));
addEventHandler("onRender", VobController.onRender.bindenv(VobController));
addEventHandler("onKey", VobController.onKey.bindenv(VobController));
addEventHandler("onSecond", VobController.onSecond.bindenv(VobController));
addEventHandler("onDamage", VobController.onDamage.bindenv(VobController));

function getVob(id) {
    if(id in VobController.vobs)
        return VobController.vobs[id];

    return null;
}
getVobs <- @() VobController.vobs;

