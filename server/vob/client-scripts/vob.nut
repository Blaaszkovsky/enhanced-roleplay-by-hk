
class GameVob
{
    id = -1
    type = -1

    hpMode = -1
    hp = -1
    hpMax = -1

    opened = -1
    changingState = null;

    weight = -1
    inventory = -1
    operationPlayer = -1
    guid = -1

    movable = -1
    movablePlayer = -1

    alpha = -1
    dynamic = false
    visual = ""
    name = ""
    complexity = ""

    position = null
    rotation = null

    trigger = -1
    triggerType = -1
    triggerAnimation = -1
    triggerTime = -1

    targetPosition = null
    targetRotation = null

    object = null;
    draw = null;

    dropped = false;
    createdBy = "";

    world = ""

    helpers = null;

    constructor(_id)
    {
        id = _id
        type = VobType.Static;

        hpMode = VobHealth.Static;
        hp = -1
        hpMax = -1
        opened = 0

        movable = VobMovable.Static;
        movablePlayer = -1

        alpha = 1.0
        dynamic = false;
        visual = "";
        name = "";
        complexity = 6;

        changingState = null;

        weight = 0.0
        inventory = null
        operationPlayer = -1
        guid = ""

        position = {x = 0, y = 0, z = 0}
        rotation = {x = 0, y = 0, z = 0}

        targetPosition = {x = 0, y = 0, z = 0}
        targetRotation = {x = 0, y = 0, z = 0}

        trigger = 0
        triggerType = 0
        triggerAnimation = 0
        triggerTime = 0

        object = null;
        draw = null;
        world = ""
        dropped = false;
        createdBy = "";

        helpers = [];
    }

    // Create object according to object model
    function createObject() {
        switch(type)
        {
            case VobType.Static:
                object = Vob(visual);
            break;
            case VobType.StaticName:
                object = Mob(visual);
                object.name = name;
            break;
            case VobType.Interaction:
                if(isBed())
                    object = MobDoor(visual);
                else
                    object = MobInter(visual);

                if(name != "")
                    object.name = name;
            break;
            case VobType.Switch:
                object = MobInter(visual);

                if(name != "")
                    object.name = name;
            break;
            case VobType.Container:
                object = MobInter(visual);

                if(name != "")
                    object.name = name;
            break;
            case VobType.Movable:
                object = MobInter(visual);

                if(name != "")
                    object.name = name;
            break;
            case VobType.Door:
                object = Mob(visual);

                if(name != "")
                    object.name = name;
            break;
        }

        if(type == VobType.Door && opened == 100) {
            object.setPosition(targetPosition.x, targetPosition.y, targetPosition.z);
            object.setRotation(targetRotation.x, targetRotation.y, targetRotation.z);
        } else {
            object.setPosition(position.x, position.y, position.z);
            object.setRotation(rotation.x, rotation.y, rotation.z);
        }

        object.cdDynamic = !dynamic;
        object.cdStatic = !dynamic;
        object.visualAlpha = alpha;

        if(hpMode == VobHealth.Vulnerable)
        {
            draw = Draw3d(position.x, position.y + 107, position.z);
            draw.insertText(hp+"/"+hpMax);
            draw.setColor(255,255,255);

            if (world == getWorld())
                draw.visible = true;

            draw.distance = 800;

            if(hpMax > 10)
            {
                if(abs(hpMax/3) > hp)
                    draw.setColor(200,0,0);
            }else
                draw.setColor(200,0,0);
        }

        if (world == getWorld())
            object.addToWorld();

        callEvent("onVobCreate", this);
    }

    function onRemove() {
        object.setPosition(0,-5000,0);
    }

    // Set position / set rotation
    function setPosition(x,y,z) {
        position = {x = x, y = y, z = z}
        object.setPosition(position.x, position.y, position.z);
    }

    function setRotation(x,y,z) {
        rotation = {x = x, y = y, z = z}
        object.setRotation(rotation.x, rotation.y, rotation.z);
    }

    // Movable functions
    function loseMovable() {
        local pos = getPlayerPosition(heroId);
        local angle = getPlayerAngle(heroId);

        pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 50);
        pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 50);

        position.x = pos.x;
        position.z = pos.z;
        rotation.y = angle;

        object.floor();
        VobController.movable.rawdelete(id);
    }

    function setMovable(movableId) {
        local oldMovable = movablePlayer;
        movablePlayer = movableId;

        if(movableId == -1) {
            if(oldMovable != heroId)
                return;

            Hero.movingVob = false;

            playAni(heroId,"T_PLUNDER");
            if(dropped == false)
            {
                local pos = getPlayerPosition(heroId);
                local angle = getPlayerAngle(heroId);

                pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 50);
                pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 50);

                position.x = pos.x;
                position.z = pos.z;
                rotation.y = angle;

                object.floor();

                local objectPosY = object.getPosition().y;
                position.y = objectPosY;
            }

            VobPacket(this).rebuild();

            VobController.movable.rawdelete(id);
        }else{
            VobController.movable[id] <- movableId;

            if(movableId == heroId)
                Hero.movingVob = true;

            object.cdDynamic = false;
            object.cdStatic = false;
            dropped = false;
        }
    }

    function moveWithPlayer(playerId) {
        local pos = getPlayerPosition(playerId);
        local angle = getPlayerAngle(playerId);

        pos.y = pos.y + 40.5;
        pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 50);
        pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 50);

        if(draw != null)
            draw.setWorldPosition(pos.x, pos.y+105.5, pos.z);

        object.setPosition(pos.x, pos.y, pos.z);
        object.setRotation(rotation.x, angle, rotation.z);
    }

    // Hit operation
    function setHealth(value) {
        hp = value;

        draw = Draw3d(position.x, position.y + 107, position.z);
        draw.insertText(hp+"/"+hpMax);
        draw.setColor(255,255,255);
        draw.visible = true;
        draw.distance = 800;

        if(hpMax > 10)
        {
            if(abs(hpMax/3) > hp)
                draw.setColor(200,0,0);
        }else
            draw.setColor(200,0,0);
    }

    // Door functions
    function playInteractionAnimation() {
        switch(triggerAnimation) {
            case VobTriggerAnimation.Door:
                if(opened == 0)
                    playAni(heroId,"T_DOOR_FRONT_S0_2_S1");
                else
                    playAni(heroId,"T_DOOR_FRONT_S1_2_S0");

                setTimer(function() {
                    stopAni(heroId);
                }, 500, 1);
            break;
        }
    }

    function setDoorState(value) {
        opened = value;

        if(opened == 0) {
            local pos = clone targetPosition;
            local rot = clone targetRotation;

            object.cdDynamic = false;
            object.cdStatic = false;

            if(getPositionDifference(getPlayerPosition(heroId), pos) < 4000)
            {
                changingState = {
                    time = triggerTime,
                    last = getTickCount(),
                    offsetPosition = {x = -(pos.x - position.x)/triggerTime, y = -(pos.y - position.y)/triggerTime, z = -(pos.z - position.z)/triggerTime},
                    offsetRotation = {x = -(rot.x - rotation.x)/triggerTime, y = -(rot.y - rotation.y)/triggerTime, z = -(rot.z - rotation.z)/triggerTime},
                }
            }else{
                changingState = null;

                object.setPosition(position.x, position.y, position.z);
                object.setRotation(rotation.x, rotation.y, rotation.z);
                object.cdDynamic = !dynamic;
                object.cdStatic = !dynamic;
            }
        } else {
            local pos = clone targetPosition;
            local rot = clone targetRotation;

            object.cdDynamic = false;
            object.cdStatic = false;

            if(getPositionDifference(getPlayerPosition(heroId), pos) < 4000)
            {
                changingState = {
                    time = triggerTime,
                    last = getTickCount(),
                    offsetPosition = {x = (pos.x - position.x)/triggerTime, y = (pos.y - position.y)/triggerTime, z = (pos.z - position.z)/triggerTime},
                    offsetRotation = {x = (rot.x - rotation.x)/triggerTime, y = (rot.y - rotation.y)/triggerTime, z = (rot.z - rotation.z)/triggerTime},
                }
            }else{
                changingState = null;

                object.setPosition(targetPosition.x, targetPosition.y, targetPosition.z);
                object.setRotation(targetRotation.x, targetRotation.y, targetRotation.z);
                object.cdDynamic = !dynamic;
                object.cdStatic = !dynamic;
            }
        }
    }

    function moveWithChangingState() {
        local difference = getTickCount() - changingState.last;
        changingState.last = getTickCount();

        changingState.time -= difference;

        local pos = object.getPosition();
        local rot = object.getRotation();
        object.setPosition(pos.x + changingState.offsetPosition.x * difference,pos.y + changingState.offsetPosition.y * difference,pos.z + changingState.offsetPosition.z * difference)
        object.setRotation(rot.x + changingState.offsetRotation.x * difference,rot.y + changingState.offsetRotation.y * difference,rot.z + changingState.offsetRotation.z * difference)

        if(changingState.time <= 0) {
            if(opened == 0) {
                object.setPosition(position.x, position.y, position.z);
                object.setRotation(rotation.x, rotation.y, rotation.z);
            }else{
                object.setPosition(targetPosition.x, targetPosition.y, targetPosition.z);
                object.setRotation(targetRotation.x, targetRotation.y, targetRotation.z);
            }

            object.cdDynamic = !dynamic;
            object.cdStatic = !dynamic;
            changingState = null;
        }
    }

    // Container functions
    function setOperation(mode) {
        if(mode) {
            operationPlayer = heroId;
            VobUIInteraction.show(this);
        } else {
            if(operationPlayer != heroId)
                return;

            operationPlayer = -1;
        }
    }

    function setOpened(value) {
        switch(type)
        {
            case VobType.Door:
                setDoorState(value);
            break;
        }
    }

    function refreshPosition() {
        if(movablePlayer != -1)
            return;

        if(changingState != null)
            return;

        object.cdDynamic = false;
        object.cdStatic = false;

        if(opened == 0) {
            object.setPosition(position.x, position.y, position.z);
            object.setRotation(rotation.x, rotation.y, rotation.z);
        }else{
            object.setPosition(targetPosition.x, targetPosition.y, targetPosition.z);
            object.setRotation(targetRotation.x, targetRotation.y, targetRotation.z);
        }

        object.cdDynamic = !dynamic;
        object.cdStatic = !dynamic;
    }

    function isPlayerBeforeVob(playerId) {
        local angle = rotation.y;

        local findInTwistedVobs = Config["TwistedVobs"].find(visual);
        if(findInTwistedVobs == null)
        {
            angle = rotation.y + 180;
            if(angle > 360)
                angle = angle - 360;
        }

        local targetPosition = {x = position.x, y = position.y, z = position.z}
        targetPosition.x = position.x + (sin(angle * 3.14 / 180.0) * 150);
        targetPosition.z = position.z + (cos(angle * 3.14 / 180.0) * 150);
        local heroAngle = getPlayerAngle(playerId);

        angle = rotation.y + 180;
        if(angle > 360)
            angle = angle - 360;

        local findInTwistedVobs = Config["TwistedVobs"].find(visual);
        if(findInTwistedVobs == null)
            angle = rotation.y;

        if(!(heroAngle >= angle - 95 && heroAngle <= angle + 95))
            return false;

        local heroPos = getPlayerPosition(playerId);
        return getDistance3d(heroPos.x, heroPos.y, heroPos.z, targetPosition.x, targetPosition.y, targetPosition.z) < 120;
    }

    function getDistanceToInteract() {
        if(type == VobType.Door)
            return 280;

        return 165;
    }

    function isBed() {
        if(Config["BedVobs"].find(visual) != null)
            return true;

        return false;
    }

    function setGuid(newguid) {
        guid = newguid;
    }
}

addEventHandler("onInit", function()
{
    addEventHandler("onWorldChange", function(world)
    {
        foreach(vob in VobController.vobs)
        {
            if (vob.world != world)
            {
                vob.object.removeFromWorld()

                if(vob.hpMode == VobHealth.Vulnerable)
                    vob.draw.visible = false
            }
        }
    })

    addEventHandler("onWorldEnter", function(world)
    {
        foreach(vob in VobController.vobs)
        {
            if (vob.world == world)
            {
                vob.object.addToWorld()

                if(vob.hpMode == VobHealth.Vulnerable)
                    vob.draw.visible = true
            }
        }
    })
})
