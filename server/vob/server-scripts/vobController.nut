
class VobController
{
    static vobs = {};
    static movablePlayers = {};
    static operationPlayers = {};

    static function onInit() {
        local vobsInDatabase = Query().select().from(GameVob.VobTable).all();

        foreach(_index,vob in vobsInDatabase)
        {
            local newVob = GameVob(vob["id"].tointeger())

            newVob.trigger = vob.triggerId.tointeger();
            newVob.triggerType = vob.triggerType.tointeger();
            newVob.triggerAnimation = vob.triggerAnimation.tointeger();
            newVob.triggerTime = vob.triggerTime.tointeger();

            newVob.hpMode = vob.hpMode.tointeger();
            newVob.hp = vob.hp.tointeger();
            newVob.hpMax = vob.hpMax.tointeger();

            newVob.opened = vob.opened.tointeger();
            newVob.movable = vob.movable.tointeger();

            newVob.alpha = vob.alpha.tofloat();
            newVob.dynamic = vob.dynamic.tointeger() == 1 ? true : false;
            newVob.visual = vob.visual;
            newVob.name = vob.name;

            newVob.type = vob.type.tointeger();
            newVob.weight = vob.weight.tofloat();
            newVob.complexity = vob.complexity.tointeger();
            newVob.guid = vob.guid;
            newVob.world = vob.world
            newVob.createdBy = vob.createdBy == null ? "" : vob.createdBy;

            newVob.position = {x = vob.positionX.tofloat(), y = vob.positionY.tofloat(), z = vob.positionZ.tofloat()}
            newVob.rotation = {x = vob.rotationX.tofloat(), y = vob.rotationY.tofloat(), z = vob.rotationZ.tofloat()}

            if(newVob.type == VobType.Container) {
                newVob.inventory = item.Controller.createBucket(ItemRelationType.Vob, newVob.id);
                newVob.inventory.maxWeight = newVob.weight;
            }

            newVob.targetPosition = {x = vob.targetPositionX.tofloat(), y = vob.targetPositionY.tofloat(), z = vob.targetPositionZ.tofloat()}
            newVob.targetRotation = {x = vob.targetRotationX.tofloat(), y = vob.targetRotationY.tofloat(), z = vob.targetRotationZ.tofloat()}

            vobs[newVob.id] <- newVob;

            vob.animationId = vob.animationId.tointeger();
            if(vob.animationId != 0)
                System.Effect.add(newVob.id, SystemEffectTarget.Vob, vob.animationId, vob.animationTime.tointeger());
        }

        goVobGenerator(vobs);

        print("We have " + vobs.len() + " registered vobs.");
    }

    static function onPlayerJoin(playerId)
    {
        foreach(vob in vobs)
            VobPacket(vob).sendCreate(playerId);
    }

    static function onPlayerDestroyedVob(playerId, vobId) {
        if(!(vobId in vobs))
            return;

        local vob = vobs[vobId];

        local message = "niszczy obiekt";
        if(vob.name != "")
            message = "niszczy "+vob.name;

        local object = Config["ChatMethod"][ChatMethods.Action];
        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] "+message+"");

        if(vob.inventory != null)
        {
            foreach(itemId in vob.inventory.ids)
            {
                local pos = vob.position;
                local _item = getItem(itemId);

                local itemGround = ItemsGround.spawn(Items.id(_item.instance), _item.amount, pos.x - 75 + rand() % 150, pos.y, pos.z - 75 + rand() % 150, getPlayerWorld(playerId));
                GarbageCollector.add(GarbageCollectorType.ItemGround, itemGround.id, 600);
                ItemsWorldRegister.add(getPlayerWorld(playerId), itemGround.id, itemId);
            }

            item.Controller.removeBucket(vob.inventory.id);
            vob.inventory = null;
        }

        Query().deleteFrom(GameVob.VobTable).where(["id = "+vobId]).execute();

        VobPacket(vobs[vobId]).remove();
        vobs.rawdelete(vobId);
    }

    static function onPlayerDisconnect(playerId, reason) {
        foreach(vob in vobs) {
            if(vob.operationPlayer == playerId || vob.movablePlayer == playerId)
                vob.onPlayerExit(playerId);
        }
    }

    static function onPacket(playerId, packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case VobPackets.Create:
                local typ = packet.readUInt8();
                local hpMode = packet.readUInt8();
                local movementVob = packet.readUInt8();
                local healthValueVob = packet.readInt32();
                local trigger = packet.readInt16();
                local triggerType = packet.readInt16();
                local triggerAnimation = packet.readInt16();
                local triggerTime = packet.readInt16();
                local guid = packet.readString();
                local keyConfigurationValueVob = packet.readUInt8();
                local animationValueVob = packet.readInt16();
                local animationTime = packet.readInt16();
                local weightValueVob = packet.readFloat();
                local isDynamic = packet.readBool() ? 1 : 0;
                local name = packet.readString();
                local visual = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();
                local targetPositionX = packet.readFloat();
                local targetPositionY = packet.readFloat();
                local targetPositionZ = packet.readFloat();
                local targetRotationX = packet.readFloat();
                local targetRotationY = packet.readFloat();
                local targetRotationZ = packet.readFloat();

                if(guid == "Klucz" || guid == "Guid")
                    guid = "";

                Query().insertInto(GameVob.VobTable, ["animationId", "animationTime", "triggerId", "triggerType", "triggerAnimation", "triggerTime", "hpMode", "world", "hp", "hpMax", "movable", "alpha", "dynamic", "weight", "visual", "name",
                    "positionX", "positionY", "positionZ", "rotationX", "rotationY", "rotationZ","targetPositionX", "targetPositionY",
                    "targetPositionZ", "targetRotationX", "targetRotationY", "targetRotationZ", "items", "guid", "complexity", "type", "createdBy"],
                [
                    animationValueVob, animationTime, trigger, triggerType, triggerAnimation, triggerTime, hpMode, "'"+getPlayerWorld(playerId)+"'", healthValueVob, healthValueVob,
                    movementVob, 1.0, isDynamic, weightValueVob, "'"+visual+"'", "'"+name+"'", positionX, positionY, positionZ, rotationX, rotationY, rotationZ,
                    targetPositionX, targetPositionY, targetPositionZ, targetRotationX, targetRotationY, targetRotationZ, "''", "'"+guid+"'", keyConfigurationValueVob, typ, "'"+getAccount(playerId).username+"'"
                ]).execute();

                local vob = Query().select().from(GameVob.VobTable).where(["visual = '"+visual+"'"]).orderBy("id DESC").one();
                if(vob == null)
                    return;

                local newVob = GameVob(vob["id"].tointeger())

                newVob.trigger = vob.triggerId.tointeger();
                newVob.triggerType = vob.triggerType.tointeger();
                newVob.triggerAnimation = vob.triggerAnimation.tointeger();
                newVob.triggerTime = vob.triggerTime.tointeger();

                newVob.hpMode = vob.hpMode.tointeger();
                newVob.hp = vob.hp.tointeger();
                newVob.hpMax = vob.hpMax.tointeger();
                newVob.opened = vob.opened.tointeger();

                newVob.movable = vob.movable.tointeger();

                newVob.alpha = vob.alpha.tofloat();
                newVob.dynamic = vob.dynamic.tointeger() == 1 ? true : false;
                newVob.visual = vob.visual;
                newVob.name = vob.name;

                newVob.type = vob.type.tointeger();
                newVob.weight = vob.weight.tofloat();
                newVob.guid = vob.guid;
                newVob.complexity = vob.complexity.tointeger();
                newVob.createdBy = vob.createdBy;
                newVob.world = getPlayerWorld(playerId)

                if(newVob.type == VobType.Container) {
                    newVob.inventory = item.Controller.createBucket(ItemRelationType.Vob, newVob.id);
                    newVob.inventory.maxWeight = newVob.weight;
                }

                newVob.position = {x = vob.positionX.tofloat(), y = vob.positionY.tofloat(), z = vob.positionZ.tofloat()}
                newVob.rotation = {x = vob.rotationX.tofloat(), y = vob.rotationY.tofloat(), z = vob.rotationZ.tofloat()}

                newVob.targetPosition = {x = vob.targetPositionX.tofloat(), y = vob.targetPositionY.tofloat(), z = vob.targetPositionZ.tofloat()}
                newVob.targetRotation = {x = vob.targetRotationX.tofloat(), y = vob.targetRotationY.tofloat(), z = vob.targetRotationZ.tofloat()}

                vobs[newVob.id] <- newVob;

                vob.animationId = vob.animationId.tointeger();
                if(vob.animationId != 0)
                    System.Effect.add(newVob.id, SystemEffectTarget.Vob, vob.animationId, vob.animationTime.tointeger());

                VobPacket(newVob).sendCreateToAll();
                getAdmin(playerId).saveLog("Dodal voba visual: " + visual + " name: " + name + " guid: " + guid, -1);
            break;
            case VobPackets.Remove:
                local vobId = packet.readInt16();
                if(!(vobId in vobs))
                    return;

                local vob = vobs[vobId];
                if(vob.inventory != null)
                {
                    item.Controller.removeBucket(vob.inventory.id);
                    vob.inventory = null;
                }

                getAdmin(playerId).saveLog("Usunal voba vobId: " + vobId + " instance: " + vob.visual + " guid: " + vob.guid + " voba stworzyl: " + vob.createdBy, -1);

                Query().deleteFrom(GameVob.VobTable).where(["id = "+vobId]).execute();
                VobPacket(vob).remove();
                vobs.rawdelete(vobId);
                callEvent("onVobDestroy", vobId);
            break;
            case VobPackets.Movable:
                local id = packet.readInt16();
                if(id in vobs)
                    vobs[id].setMovable(playerId);
                VobLog(playerId,id).log("move");
            break;
            case VobPackets.Health:
                local id = packet.readInt16();
                if(id in vobs)
                    vobs[id].hit(playerId);
            break;
            case VobPackets.Rebuild:
                local id = packet.readInt16();
                if(id in vobs)
                    vobs[id].rebuild(packet.readFloat(),packet.readFloat(),packet.readFloat(),packet.readFloat(),packet.readFloat(),packet.readFloat());
                VobLog(playerId,id).log("rebuild");
            break;
            case VobPackets.Operation:
                local id = packet.readInt16();
                if(id in vobs)
                    vobs[id].setOperation(playerId);
            break;
            case VobPackets.DoorInteraction:
                local id = packet.readInt16();
                if(!(id in vobs))
                    return;

                local vobObject = vobs[id];
                switch(vobObject.type)
                {
                    case VobType.Door:
                        vobObject.doorInteraction(playerId);
                    break;
                    case VobType.Switch:
                        foreach(vob in vobs)
                        {
                            if(vob.trigger == id)
                                vob.doorInteraction(playerId);
                        }
                    break;
                }
            break;
            case VobPackets.Complexity:
                local result = packet.readBool();
                if(result == false)
                    removeItem(playerId, "ITKE_LOCKPICK", 1);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Vob, VobController.onPacket.bindenv(VobController));
addEventHandler("onInit", VobController.onInit.bindenv(VobController));
addEventHandler("onPlayerJoin", VobController.onPlayerJoin.bindenv(VobController));
addEventHandler("onPlayerDisconnect", VobController.onPlayerDisconnect.bindenv(VobController));