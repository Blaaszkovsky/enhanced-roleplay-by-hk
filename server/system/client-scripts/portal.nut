
System.Portal <- {

    portals = {}

    add = function(portalId, from, to) {
        portals[portalId] <- {
            from = {x = from.x, y = from.y, z = from.z},
            to = {x = to.x, y = to.y, z = to.z},
            element = Vob("ITMI_FOCUS.3DS"),
        }

        portals[portalId].element.setPosition(from.x, from.y, from.z);
        portals[portalId].element.floor();
        portals[portalId].element.addToWorld();
        addEffectVob(portals[portalId].element.ptr, "spellFX_HealShrine");
    }

    onSecond = function() {
        foreach(portal in portals) {
            if(getPositionDifference(getPlayerPosition(heroId), portal.from) < 200){
                setPlayerPosition(heroId, portal.to.x, portal.to.y, portal.to.z);
                addEffect(heroId, "VOB_MAGICBURN");
            }
        }

        foreach(portal in portals)
            addEffectVob(portal.element.ptr, "spellFX_HealShrine");
    }

    remove = function(id) {
        if(!(id in portals))
            return;

        portals.rawdelete(id);
    }
}

addEventHandler("onSecond", System.Portal.onSecond.bindenv(System.Portal))

