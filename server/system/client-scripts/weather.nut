
System.Weather <- {
    cache = null

    initialize = function(_cache) {
        cache = _cache;

        if(cache.weather == SystemWeatherType.SunnyDay && cache.cloudsColor.r == 255 && cache.cloudsColor.g == 255 && cache.cloudsColor.b == 255)
            return;

        switch(cache.weather) {
            case SystemWeatherType.SunnyDay:
                Sky.windScale = 0.5;
                Sky.renderLightning = false;
            break;
            case SystemWeatherType.Snow:
                Sky.weather = WEATHER_SNOW;
                Sky.windScale = 1.2;
                Sky.renderLightning = true;
            break;
            case SystemWeatherType.Rain:
                Sky.weather = WEATHER_RAIN;
                Sky.windScale = 1.2;
                Sky.renderLightning = true;
            break;
        }

        Sky.setCloudsColor(cache.cloudsColor.r, cache.cloudsColor.g, cache.cloudsColor.b);
        Sky.setLightingColor(cache.cloudsColor.r, cache.cloudsColor.g, cache.cloudsColor.b);
        Sky.setFogColor(cache.cloudsColor.r, cache.cloudsColor.g, cache.cloudsColor.b, 255);
        Sky.setPlanetSize(PLANET_SUN, cache.sunSize);
        Sky.setPlanetColor(PLANET_SUN, cache.sunColor.r, cache.sunColor.g, cache.sunColor.b, 255);
        Sky.setPlanetSize(PLANET_MOON, cache.moonSize);
        Sky.setPlanetColor(PLANET_MOON, cache.moonColor.r, cache.moonColor.g, cache.moonColor.b, 255);
    }
}
