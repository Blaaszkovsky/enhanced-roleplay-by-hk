
_getTime <- getTime;

System.TimeGame <- {

    hour = 0,
    min = 0,
    day = 0,
    timerSystem = null,

    cache = {},

    onInit = function(_day,_hour,_min) {
        day = _day;
        hour = _hour;
        min = _min;

        if(timerSystem != null)
            killTimer(timerSystem);

        generateCache();
        
        timerSystem = setTimer(function() {
            System.TimeGame.plus();
        }, Config["MinuteSeconds"] * 1000, 0);
    }

    generateCache = function() {
        local __year = floor(day/Config["Calendar"].yearDays);
        local __dayOfTheYear = day - (Config["Calendar"].yearDays * __year);

        local __day = 0, __daysInMonth = 0, __month = 0, __dayOfMonth = 0, __f = false, __season = "", __firstDayOfMonth = 0;
        foreach(season in Config["Calendar"]["Seasons"]) {
            if(__f)
                break;

            foreach(daysInMonth in Config["Calendar"][season].months) {
                __firstDayOfMonth = __day;
                __dayOfMonth = abs(__day - __dayOfTheYear);
                __day = __day + daysInMonth;
                __month = __month + 1;
                __daysInMonth = daysInMonth;
                if(__day > __dayOfTheYear) {
                    __season = season;
                    __f = true;
                    break;
                }
            }
        }

        cache = {
            season = __season,
            year = __year,
            firstDayOfMonth = __firstDayOfMonth,
            dayOfTheYear = __dayOfTheYear,
            month = __month,
            daysInMonth = __daysInMonth,
            dayOfMonth = __dayOfMonth,
            dayOfWeek = day % 7,
        }
    }

    plus = function() {
        min = min + 1;

        if(min >= 60) {
            hour = hour + 1;
            min = 0;
        }

        if(hour >= 24) {
            hour = 0;
            min = 0;
            day = day + 1;
            generateCache();
        }
    }
}

function getTime() {
    return {hour = System.TimeGame.hour, min = System.TimeGame.min, day = System.TimeGame.day};
}
