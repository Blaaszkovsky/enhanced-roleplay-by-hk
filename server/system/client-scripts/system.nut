
System <- {

    onInit = function() {
        enable_WeaponTrail(true);
        enable_NicknameId(false);

        setTimer(function() {

            if(fileMd5("Data/union.vdf") != null)
                Account.Packet.kick("Wykryto Uniona. Prosimy o wyłączenie.");

            if(fileMd5("System/Union.patch") != null)
                Account.Packet.kick("Wykryto Uniona. Prosimy o wyłączenie.");
        }, 1500, 1);
    }

    getSystemPacket = function(packet) {
        local packetId = packet.readUInt8();
        if(packetId != PacketSystem)
            return;

        local type = packet.readUInt8();
        switch(type)
        {
            case SystemPackets.Time:
                local day = packet.readInt16();
                local hour = packet.readInt16();
                local min = packet.readInt16();

                System.TimeGame.onInit(day,hour,min);
            break;
            case SystemPackets.RemovePortal:
                local portalId = packet.readInt16();
                System.Portal.remove(portalId);
            break;
            case SystemPackets.AddPortal:
                local portalId = packet.readInt16();
                local from = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                local to = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                System.Portal.add(portalId,from,to);
            break;
            case SystemPackets.RemoveEffect:
                local effectId = packet.readInt32();
                System.Effect.remove(effectId);
            break;
            case SystemPackets.AddEffect:
                local id = packet.readInt32();
                local effectId = packet.readInt16();
                local objId = packet.readInt16();
                local objTypeId = packet.readInt16();
                local refreshTime = packet.readInt16();
                System.Effect.add(id, effectId, objId, objTypeId, refreshTime);
            break;
            case SystemPackets.AddMount:
                local mountPlayer = packet.readInt16();
                local mountType = packet.readInt16();
                local armor = packet.readInt16();
                local melee = packet.readInt16();
                local ranged = packet.readInt16();
                local head = packet.readString();
                local face = packet.readInt16();
                local body = packet.readString();
                local skin = packet.readInt16();

                System.Mount.add(mountPlayer, mountType, armor, melee, ranged, head, face, body, skin);
            break;
            case SystemPackets.RemoveMount:
                local mountPlayer = packet.readInt16();

                System.Mount.remove(mountPlayer);
            break;
            case SystemPackets.Bound:
                local playerId = packet.readInt16();
                local active = packet.readBool();

                if(active == false)
                    System.Bound.remove(playerId);
                else
                    System.Bound.add(playerId);
            break;
            case SystemPackets.Weather:
                local cache = {};

                cache.weather <- packet.readInt16();
                cache.cloudsColor <- {r = packet.readInt16(), g = packet.readInt16(), b = packet.readInt16()};
                cache.sunSize <- packet.readFloat();
                cache.sunColor <- {r = packet.readInt16(), g = packet.readInt16(), b = packet.readInt16()};
                cache.moonSize <- packet.readFloat();
                cache.moonColor <- {r = packet.readInt16(), g = packet.readInt16(), b = packet.readInt16()};

                System.Weather.initialize(cache);
            break;
            case SystemPackets.AddMusic:
                local id = packet.readInt32();
                local name = packet.readString();
                local sound = packet.readString();
                local pos = { x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat() }
                local range = packet.readInt16();
                local volume = packet.readInt16();
                System.Music.add(id, name, sound, pos.x, pos.y, pos.z, range, volume)
            break;
            case SystemPackets.RemoveMusic:
                local id = packet.readInt32();
                System.Music.stop(id);
            break;
            case SystemPackets.HideMusic:
                System.Music.hide();
            break;
            case SystemPackets.ShowMusic:
                System.Music.show();
            break;
        }
    }
}

addEventHandler("onInit", System.onInit.bindenv(System));
addEventHandler("onPacket", System.getSystemPacket.bindenv(System));
