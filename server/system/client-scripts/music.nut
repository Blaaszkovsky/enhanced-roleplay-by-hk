
System.Music <- {
    localDraws = [],
    musics = {},
    drawsVisible = true

    function add(id, name, soundName, x, y, z, range, volume) {
        local vob = Vob("PUSTY.3DS");
        vob.setPosition(x,y,z);
        vob.addToWorld();

        local obj = Sound3d(soundName)
        obj.looping = true
        obj.volume = 0.1 * volume;
        obj.radius = range

        obj.stop()
		obj.setTargetVob(vob)
		obj.play()

        musics[id] <- {
            name = name,
            vob = vob,
            music = obj,
        }
    }

    function hide(){
        drawsVisible = false
    }

    function show(){
        drawsVisible = true
    }

    function stop(id) {
        if(id in musics) {
            musics[id].music.stop();
            musics.rawdelete(id);
        }
    }

    render = function() {
        local objects = [];

        local heroPos = getPlayerPosition(heroId);
        foreach(id, m in musics) {
            local posDiff = getPositionDifference(heroPos, m.vob.getPosition());
            if(posDiff < 2000) {
                local obj = {};
                obj.position <- m.vob.getPosition();
                obj.name <- id+ "| - " + m.name;
                obj.posDifference <- posDiff;
                objects.push(obj);
            }
        }
        localDraws.clear();

        foreach(obj in objects) {
            local draw = Draw(0, 0, obj.name);

            local project = _Camera.project(obj.position.x, obj.position.y + 109.2, obj.position.z);
            if(project == null)
                continue;

            draw.font = "FONT_OLD_20_WHITE_HI.TGA";
            local scaleI = 0.2 + 0.3 * (((2000.0 - obj.posDifference)/2000.0) * 1.0);

            draw.setScale(scaleI, scaleI);
            draw.setPosition(anx(project.x), any(project.y));

            draw.visible = drawsVisible;
            localDraws.push(draw);
        }
    }
}

addEventHandler ("onRender", System.Music.render.bindenv(System.Music));