
System.Mount <- {

    mounts = {}
    mountNpcs = {}

    add = function(mountPlayer, mountType, armor, melee, ranged, head, face, body, skin) {
        if(mountType == -1) {
            remove(mountPlayer);
            return;
        }

        mounts[mountPlayer] <- {
            typeId = mountType,
            armor = armor,
            melee = melee,
            ranged = ranged,
            head = head,
            face = face,
            body = body,
            skin = skin,
            spawned = false,
        };

        mountNpcs[mountPlayer] <- createNpc(getPlayerRealName(mountPlayer));
    }

    function spawnMount(playerId) {
        mounts[playerId].spawned = true;
        local element = mountNpcs[playerId];

        spawnNpc(element);
        setPlayerInstance(element, "PC_HERO");

        setPlayerMaxHealth(element,1000);
        setPlayerHealth(element,1000);

        local position = getPlayerPosition(playerId);
        setPlayerAngle(element, getPlayerAngle(playerId));
        setPlayerPosition(element, position.x, position.y, position.z);

        if(mounts[playerId].melee != -1)
            equipItem(element, mounts[playerId].melee);

        if(mounts[playerId].ranged != -1)
            equipItem(element, mounts[playerId].ranged);

        if(mounts[playerId].armor != -1)
            equipItem(element, mounts[playerId].armor);

        setPlayerVisual(element, mounts[playerId].body, mounts[playerId].skin, mounts[playerId].head, mounts[playerId].face);
        setPlayerCollision(element, false);
    }

    function unSpawnMount(playerId) {
        mounts[playerId].spawned = false;
        unspawnNpc(mountNpcs[playerId]);
    }

    remove = function(playerId) {
        if(playerId in mounts) {
            destroyNpc(mountNpcs[playerId]);
            mountNpcs.rawdelete(playerId);
            mounts.rawdelete(playerId);
        }
    }

    getNpcs = function() {
        local ids = {};
        foreach(playerId, mount in mountNpcs)
            ids[mount] <- playerId;

        return ids;
    }

    onSecond = function() {
        local heroPos = getPlayerPosition(heroId);
        foreach(playerId, mount in mounts)
        {
            local pos = getPlayerPosition(playerId);
            local diff = getPositionDifference(heroPos, pos);
            if(diff > 3000 && mount.spawned)
                unSpawnMount(playerId);
            else if(diff <= 3000 && mount.spawned == false)
                spawnMount(playerId);
        }
    }

    onRender = function() {
        foreach(player, bot in mountNpcs)
        {
            if(mounts[player].spawned == false)
                return;

            local pos = getPlayerPosition(player);

            if(pos == null)
                continue;

            switch(mounts[player].typeId) {
                case 0:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    setPlayerPosition(bot, pos.x, pos.y + 23, pos.z);
                break;
                case 1:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    setPlayerPosition(bot, pos.x, pos.y + 23, pos.z);
                break;
                case 2:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    setPlayerPosition(bot, pos.x, pos.y + 23, pos.z);
                break;
                case 3:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 20);
                    setPlayerPosition(bot, pos.x, pos.y + 33, pos.z);
                break;
                case 4:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 26);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 26);
                    setPlayerPosition(bot, pos.x, pos.y + 27, pos.z);
                break;
                case 5:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 15);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 15);
                    setPlayerPosition(bot, pos.x, pos.y + 54, pos.z);
                break;
                case 6:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 42);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 42);
                    setPlayerPosition(bot, pos.x, pos.y + 24, pos.z);
                break;
                case 7:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 72);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 72);
                    setPlayerPosition(bot, pos.x, pos.y + 184, pos.z);
                break;
                case 8:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 12);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 12);
                    setPlayerPosition(bot, pos.x, pos.y + 12, pos.z);
                break;
                case 9:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 62);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 62);
                    setPlayerPosition(bot, pos.x, pos.y + 194, pos.z);
                break;
                case 10:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    setPlayerPosition(bot, pos.x, pos.y + 23, pos.z);
                break;
                case 11:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 35);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 35);
                    setPlayerPosition(bot, pos.x, pos.y + 99, pos.z);
                break;
                case 12:
                    pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 30);
                    setPlayerPosition(bot, pos.x, pos.y + 66, pos.z);
                break;				
            }

            if(getPlayerAni(bot) != "S_HORSESIT")
                playAni(bot, "S_HORSESIT");

            setPlayerAngle(bot, getPlayerAngle(player))
        }
    }
}

addEventHandler("onRender", System.Mount.onRender.bindenv(System.Mount));
addEventHandler("onSecond", System.Mount.onSecond.bindenv(System.Mount));