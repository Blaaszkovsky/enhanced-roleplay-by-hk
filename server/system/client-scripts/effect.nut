
System.Effect <- {

    effects = {}

    add = function(id, effectId, objId, objTypeId, refreshTime) {
        effects[id] <- {
            objId = objId,
            effectId = effectId,
            objTypeId = objTypeId,
            refreshTime = refreshTime,
            refreshTimeLeft = 0,
        }

        giveEffect(effects[id]);
    }

    function giveEffect(object) {
        switch(object.objTypeId) {
            case SystemEffectTarget.Player:
                if(isPlayerCreated(object.objId))
                    addEffect(object.objId, System_Effects[object.effectId.tostring()])
            break;
            case SystemEffectTarget.Vob:
                if(!(object.objId in VobController.vobs))
                    return;

                local vob = getVob(object.objId);
                if(vob.object == null)
                    return;

                addEffectVob(vob.object.ptr, System_Effects[object.effectId.tostring()]);
            break;
            case SystemEffectTarget.Npc:
                local bots = getBots();
                if(!(object.objId in bots))
                    return;

                local npcObject = bots[object.objId];
                if(isPlayerCreated(npcObject.element))
                    addEffect(npcObject.element, System_Effects[object.effectId.tostring()])
            break;
        }
    }

    onPlayerCreate = function(playerId) {
        foreach(effectId, effect in effects)
            if(effect.objId == playerId && effect.objTypeId == SystemEffectTarget.Player)
                giveEffect(effect);
    }

    onVobCreate = function(objectVob) {
        foreach(effectId, effect in effects)
            if(effect.objId == objectVob.id && effect.objTypeId == SystemEffectTarget.Vob)
                giveEffect(effect);
    }

    onNpcSpawn = function(npcObject) {
        foreach(effectId, effect in effects)
            if(effect.objId == npcObject.id && effect.objTypeId == SystemEffectTarget.Npc)
                giveEffect(effect);
    }

    onSecond = function() {
        foreach(effect in effects) {
            if(effect.refreshTime == 0)
                continue;

            effect.refreshTimeLeft = effect.refreshTimeLeft + 1;
            if(effect.refreshTimeLeft >= effect.refreshTime)
            {
                effect.refreshTimeLeft = 0;
                giveEffect(effect);
            }
        }
    }

    remove = function(id) {
        if(!(id in effects))
            return;

        local effect = effects[id];

        switch(effect.objTypeId)
        {
            case SystemEffectTarget.Player:
                removeEffect(effect.objId, System_Effects[effect.effectId.tostring()]);
            break;
            case SystemEffectTarget.Npc:
                local npcObject = bots[effect.objId];
                if(isPlayerCreated(npcObject.element))
                    removeEffect(npcObject.element, System_Effects[effect.effectId.tostring()]);
            break;
            case SystemEffectTarget.Vob:
                local vob = getVob(effect.objId);
                if(vob == null)
                    return;

                removeEffectVob(vob.object.ptr, System_Effects[effect.effectId.tostring()]);
            break;
        }

        effects.rawdelete(id);
    }
}

addEventHandler("onSecond", System.Effect.onSecond.bindenv(System.Effect))
addEventHandler("onPlayerCreate", System.Effect.onPlayerCreate.bindenv(System.Effect))
addEventHandler("onVobCreate", System.Effect.onVobCreate.bindenv(System.Effect))
addEventHandler("onNpcSpawn", System.Effect.onNpcSpawn.bindenv(System.Effect))

function addEffectVob(vobPtr, effect) {

}