
System <- {};
System.tableName <- "`system`";

function System::updateRecord(type, value) {
    Query().update(System.tableName, ["value"], ["'"+value+"'"]).where(["type = "+type]).execute();
}

function System::readRecord(type, defaultValue) {
    local record = Query().select("value").from(System.tableName).where(["type = "+type]).one();
    if(record)
        return record["value"];
    else
        Query().insertInto(System.tableName, ["type", "value"], [type, "'"+defaultValue+"'"]).execute();

    return defaultValue;
}