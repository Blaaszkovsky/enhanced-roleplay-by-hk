
System.Effect <- {
    effects = {},
    freeId = 0,

    add = function(objId, objTypeId, effectId, refreshTime) {
        effects[freeId] <- {
            objId = objId,
            effectId = effectId,
            objTypeId = objTypeId,
            refreshTime = refreshTime,
        }

        for(local i = 0; i < getMaxSlots(); i ++)
            if(isPlayerConnected(i))
                sendCreate(i, freeId, effectId, objId, objTypeId, refreshTime);

        freeId = freeId + 1;
    }

    remove = function(id) {
        if(!(id in effects))
            return;

        effects.rawdelete(id);
        for(local i = 0; i < getMaxSlots(); i ++)
            if(isPlayerConnected(i))
                sendRemove(i, id);
    }

    getForPlayer = function(playerId) {
        local effects = [];
        foreach(effectId, effect in effects) {
            if(effect.objId == playerId && effect.objTypeId == SystemEffectTarget.Player) {
                effects.push({effectId = effect.effectId, refreshTime = effect.refreshTime})
            }
        }
        return effects;
    }

    onPlayerJoin = function(playerId) {
        foreach(effectId, effect in effects)
            sendCreate(playerId, effectId, effect.effectId, effect.objId, effect.objTypeId, effect.refreshTime)
    }

    onPlayerDisconnect = function(playerId, reason) {
        foreach(_index, effect in effects) {
            if(effect.objId == playerId && effect.objTypeId == SystemEffectTarget.Player) {
                remove(_index);
            }
        }
    }

    onVobDestroy = function(vobId) {
        foreach(_index, effect in effects) {
            if(effect.objId == vobId && effect.objTypeId == SystemEffectTarget.Vob) {
                remove(_index);
            }
        }
    }

    onNpcEffectRemove = function(objId, effectId) {
        foreach(_index, effect in effects) {
            if(effect.objId == objId && effect.objTypeId == SystemEffectTarget.Npc && effect.effectId == effectId) {
                remove(_index);
            }
        }
    }

    onPlayerEffectRemove = function(objId, effectId) {
        foreach(_index, effect in effects) {
            if(effect.objId == objId && effect.objTypeId == SystemEffectTarget.Player && effect.effectId == effectId) {
                remove(_index);
            }
        }
    }

    sendRemove = function(pid, id) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.RemoveEffect);
        packet.writeInt32(id);
        packet.send(pid, RELIABLE)
    }

    sendCreate = function(pid, id, effectId, objId, objTypeId, refreshTime) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.AddEffect);
        packet.writeInt32(id);
        packet.writeInt16(effectId);
        packet.writeInt16(objId);
        packet.writeInt16(objTypeId);
        packet.writeInt16(refreshTime);
        packet.send(pid, RELIABLE)
    }
}

addEventHandler("onPlayerJoin", System.Effect.onPlayerJoin.bindenv(System.Effect))
addEventHandler("onPlayerDisconnect", System.Effect.onPlayerDisconnect.bindenv(System.Effect))
addEventHandler("onVobDestroy", System.Effect.onVobDestroy.bindenv(System.Effect))
