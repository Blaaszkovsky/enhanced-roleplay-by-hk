enum SQLCommandType {
    Kick,
    Ban,
    GiveItem,
    RemoveItem,
    SetSkill,
    SetStat,
    AddEffect,
    RemoveEffect,
}

System.SQLCommands <- {
    table = "sqlcommands",

    onInit = function() {
        local sqlCommands = Query().select().from(System.SQLCommands.table).all();

        if(sqlCommands.len() > 0) {
            foreach(sqlCommand in sqlCommands)
                executeCommand(sqlCommand);

            Query().deleteFrom(System.SQLCommands.table).execute();
        }
    }

    executeCommand = function(obj) {
        switch(obj.type) {
            case SQLCommandType.Kick:
                local id = obj.playerId;
                local reason = obj.name;

                if(isPlayerConnected(id) == false)
                    return;

                kick(id, reason);

                sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� kicka przez %s", getPlayerName(id), obj.executor));
                sendMessageToAll(255, 80, 0, ChatType.ALL + format("Pow�d: %s", reason));
            break;
            case SQLCommandType.Ban:
                local id = obj.playerId;
                local reason = obj.name;

                if(isPlayerConnected(id) == false)
                    return;

                ban(id, -1, reason);

                sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� bana przez %s", getPlayerName(id), obj.executor));
                sendMessageToAll(255, 80, 0, ChatType.ALL + format("Pow�d: %s", reason));
            break;
            case SQLCommandType.GiveItem:
                local id = obj.playerId;
                local instance = obj.name;
                local amount = obj.value;

                if(isPlayerConnected(id) == false)
                    return;

                giveItem(id, instance, amount);
                getPlayer(id).save();
                sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s dosta� %s w ilo�ci %d od %s", getPlayerName(id), instance, amount, obj.executor));
            break;
            case SQLCommandType.RemoveItem:
                local id = obj.playerId;
                local instance = obj.name;
                local amount = obj.value;

                if(isPlayerConnected(id) == false)
                    return;

                removeItem(id, instance, amount);
                getPlayer(id).save();
                sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s zabrano %s w ilo�ci %d od %s", getPlayerName(id), instance, amount, obj.executor));
            break;
            case SQLCommandType.SetStat:
                local id = obj.playerId;
                local typ = obj.name.tointeger();
                local value = obj.value;

                if(isPlayerConnected(id) == false)
                    return;

                switch(typ)
                {
                    case PlayerAttributes.Str: setPlayerStrength(id, value); break;
                    case PlayerAttributes.Dex: setPlayerDexterity(id, value); break;
                    case PlayerAttributes.Int: setPlayerInteligence(id, value); break;
                    case PlayerAttributes.Hp: setPlayerMaxHealth(id, value); setPlayerHealth(id, value); break;
                    case PlayerAttributes.Mana: setPlayerMana(id, value); setPlayerMaxMana(id, value); break;
                    case PlayerAttributes.OneH: setPlayerSkillWeapon(id, 0, value); break;
                    case PlayerAttributes.TwoH: setPlayerSkillWeapon(id, 1, value); break;
                    case PlayerAttributes.Bow: setPlayerSkillWeapon(id, 2, value); break;
                    case PlayerAttributes.Cbow: setPlayerSkillWeapon(id, 3, value); break;
                    case PlayerAttributes.MagicLvl: setPlayerMagicLevel(id, value); break;
                    case PlayerAttributes.Stamina: setPlayerStamina(id, value/1.0); break;
                    case PlayerAttributes.LearnPoints: setPlayerLearnPoints(id, value); break;
                    case PlayerAttributes.Hunger: setPlayerHunger(id, value/1.0); break;
                }

                getPlayer(id).save();
                sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"zmieni� "+obj.executor + " ci " + Config["PlayerAttributes"][typ]+" na " + value);
            break;
            case SQLCommandType.SetSkill:
                local id = obj.playerId;
                local typ = obj.name.tointeger();
                local value = obj.value;

                if(isPlayerConnected(id) == false)
                    return;

                setPlayerSkill(id, typ, value);
                getPlayer(id).save();
                sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"zmieni� "+obj.executor + " ci " + Config["PlayerSkill"][typ]+" na " + value);
            break;
            case SQLCommandType.AddEffect:
                local id = obj.playerId;
                local effectId = obj.name.tointeger();
                local refreshTime = obj.value;

                if(isPlayerConnected(id) == false)
                    return;

                getPlayer(id).effectModule.addEffect(effectId, refreshTime);
            break;
            case SQLCommandType.RemoveEffect:
                local id = obj.playerId;
                local effectId = obj.name.tointeger();

                if(isPlayerConnected(id) == false)
                    return;

                getPlayer(id).effectModule.removeEffect(effectId);
            break;
        }
    }

    read = function() {
        local sqlCommands = Query().select().from(System.SQLCommands.table).all();

        if(sqlCommands.len() > 0) {
            foreach(sqlCommand in sqlCommands)
                executeCommand(sqlCommand);

            Query().deleteFrom(System.SQLCommands.table).execute();
        }
    }
}

addEventHandler ("onSecond", System.SQLCommands.read.bindenv(System.SQLCommands));
addEventHandler ("onInit", System.SQLCommands.onInit.bindenv(System.SQLCommands));