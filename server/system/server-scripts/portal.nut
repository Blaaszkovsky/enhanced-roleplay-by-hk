
System.Portal <- {

    portals = {}
    freeId = 0,

    add = function(from, to, seconds) {
        portals[freeId] <- {
            from = {x = from.x, y = from.y, z = from.z},
            to = {x = to.x, y = to.y, z = to.z},
            seconds = seconds,
        }

        for(local i = 0; i < getMaxSlots(); i ++)
            if(isPlayerConnected(i))
                sendCreate(i, freeId, from, to);

        freeId = freeId + 1;
    }

    remove = function(id) {
        if(!(id in portals))
            return;

        portals.rawdelete(id);
        for(local i = 0; i < getMaxSlots(); i ++)
            if(isPlayerConnected(i))
                sendRemove(i, id);
    }

    onPlayerJoin = function(pid) {
        foreach(portalId, portal in portals)
            sendCreate(pid, portalId, portal.from, portal.to);
    }

    function onSecond() {
        foreach(portalId, portal in portals) {
            portal.seconds -= 1;

            if(portal.seconds <= 0)
                remove(portalId);
        }
    }

    sendRemove = function(pid, id) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.RemovePortal);
        packet.writeInt16(id);
        packet.send(pid, RELIABLE)
    }

    sendCreate = function(pid, id, from, to) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.AddPortal);
        packet.writeInt16(id);
        packet.writeFloat(from.x);
        packet.writeFloat(from.y);
        packet.writeFloat(from.z);
        packet.writeFloat(to.x);
        packet.writeFloat(to.y);
        packet.writeFloat(to.z);
        packet.send(pid, RELIABLE)
    }
}

addEventHandler("onPlayerJoin", System.Portal.onPlayerJoin.bindenv(System.Portal))
addEventHandler("onSecond", System.Portal.onSecond.bindenv(System.Portal))