
System.Timer <- {
    value = 0

    plus = function()
    {
        callEvent("onSecond");
        value = value + 1;

        if(value % 10 == 0)
            callEvent("onTenSeconds");

        if(value % 5 == 0)
            callEvent("onFiveSeconds");

        if(value >= 60)
        {
            value = 0;
            callEvent("onMinute");
        }
    }
}

setTimer(System.Timer.plus.bindenv(System.Timer), 1000, 0);