System.Music <- {
    musics = {},
    nextId = 0,

    function add(name, soundName, x, y, z, range, volume, seconds) {
        local id = nextId;
        nextId = nextId + 1;

        musics[id] <- {
            name = name,
            sound = soundName,
            position = {x = x, y = y, z = z},
            range = range,
            volume = volume,
            seconds = seconds,
        }

        local m = musics[id];
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.AddMusic);
        packet.writeInt32(id);
        packet.writeString(m.name);
        packet.writeString(m.sound);
        packet.writeFloat(m.position.x);
        packet.writeFloat(m.position.y);
        packet.writeFloat(m.position.z);
        packet.writeInt16(m.range);
        packet.writeInt16(m.volume);
        packet.sendToAll(RELIABLE);
    }

    function remove(id) {
        if(id in musics)
            musics.rawdelete(id);

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.RemoveMusic);
        packet.writeInt32(id);
        packet.sendToAll(RELIABLE);
    }

    function hide(){
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.HideMusic);
        packet.sendToAll(RELIABLE);
    }

    function show(){
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.ShowMusic);
        packet.sendToAll(RELIABLE); 
    }

    onSecond = function() {
        foreach(id,m in musics) {
            if(m.seconds <= 0) {
                remove(id);
                return;
            }

            musics[id].seconds--;
        }
    }

    onPlayerJoin = function(playerId) {
        foreach(id,m in musics) {
            local packet = Packet();
            packet.writeUInt8(PacketSystem);
            packet.writeUInt8(SystemPackets.AddMusic);
            packet.writeInt32(id);
            packet.writeString(m.name);
            packet.writeString(m.sound);
            packet.writeFloat(m.position.x);
            packet.writeFloat(m.position.y);
            packet.writeFloat(m.position.z);
            packet.writeInt16(m.range);
            packet.writeInt16(m.volume);
            packet.send(playerId, RELIABLE);
        }
    }
}

addEventHandler("onSecond", System.Music.onSecond.bindenv(System.Music));
addEventHandler("onPlayerJoin", System.Music.onPlayerJoin.bindenv(System.Music));
