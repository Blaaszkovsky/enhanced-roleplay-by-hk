
System.BlockDeathMatch <- {
    active = false,
    minutes = -1,
    players = [],

    onInit = function() {
        for(local i = 0; i < getMaxSlots(); i ++)
            players.push(0);
    }

    set = function (minute) {
        minutes = minute;

        active = true;
    }

    remove = function() {
        active = false;
    }

    plus = function() {
        if(active == false)
            return;

        foreach(playerId, minutePlayer in players)
        {
            if(minutePlayer == 0)
                continue;

            minutePlayer = minutePlayer - 1;
        }

        minutes = minutes - 1;

        if(minutes <= 0)
        {
            active = false;
            minutes = 0;
        }
    }
}

function isDmBlockedForPlayer(playerId) {
    if(System.BlockDeathMatch.active)
        return true;

    return System.BlockDeathMatch.players[playerId] != 0;
}

addEventHandler("onInit", System.BlockDeathMatch.onInit.bindenv(System.BlockDeathMatch))
addEventHandler("onMinute", System.BlockDeathMatch.plus.bindenv(System.BlockDeathMatch))

/*
addEvent("onSecond");
addEvent("onMinute");
addEvent("onTenSeconds");
*/
