
//local fileObject = file("stack/bugreport.txt", "a+");

local stackInfo = {
    "bug": [],
    "info": []
    "botpos": [],
};

function stack(pattern, info) {
    stackInfo[pattern].push(time()+" - "+info);

    if(stackInfo[pattern].len() > 100) {
        local fileObject = file("database/stack/"+pattern+".txt", "a+");

        foreach(text in stackInfo[pattern])
            fileObject.write(text+"\n");

        fileObject.close();
        stackInfo[pattern].clear();
    }
}