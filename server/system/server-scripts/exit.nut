System.Exit <- {

    function kickAllPlayer() {
        for(local i = 0; i < getMaxSlots(); i ++) {
            if(isPlayerConnected(i)) {
                getPlayer(i).save();
                kick(i, "Restart serwera, zapraszamy za chwil�.");
            }
        };

    }
}

addEventHandler("onExit", System.Exit.kickAllPlayer.bindenv(System.Exit));
