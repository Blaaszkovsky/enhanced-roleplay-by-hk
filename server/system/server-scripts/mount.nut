
System.Mount <- {

    mounts = {}

    add = function(playerId, typeId) {
        if(playerId in mounts) {
            remove(playerId);
            return
        }

        if(typeId == -1) {
            remove(playerId);
            return;
        }

        local visual = getPlayerVisual(playerId);
        mounts[playerId] <- {
            typeId = typeId,
            armor = getPlayerArmor(playerId),
            melee = getPlayerMeleeWeapon(playerId),
            ranged = getPlayerRangedWeapon(playerId),
            head = visual.headModel,
            face = visual.headTxt,
            body = visual.bodyModel,
            skin = visual.bodyTxt,
        };

        switch(typeId) {
            case 0:setPlayerInstance(playerId, "SCAVENGER");break;
            case 1:setPlayerInstance(playerId, "WOLF");break;
            case 2:setPlayerInstance(playerId, "WARG");break;
            case 3:setPlayerInstance(playerId, "SHADOWBEAST");break;
            case 4:setPlayerInstance(playerId, "SHEEP");break;
            case 5:setPlayerInstance(playerId, "GOBBO_GREEN"); setPlayerScale(playerId, 1.7, 1.7, 1.7); break;
            case 6:setPlayerInstance(playerId, "WARAN");break;
            case 7:setPlayerInstance(playerId, "TROLL");break;
            case 8:setPlayerInstance(playerId, "BLOODFLY"); setPlayerScale(playerId, 1.5, 1.5, 1.5); break;
            case 9:setPlayerInstance(playerId, "SWAMPSHARK");break;
            case 10:setPlayerInstance(playerId, "HRS");break;
            case 11:setPlayerInstance(playerId, "FROG_ROAM_01");break;
            case 12:setPlayerInstance(playerId, "BLACKBEAR");break;			
        }

        sendCreate(-1, playerId, mounts[playerId]);
    }

    remove = function(playerId) {
        if(playerId in mounts) {
            if(isPlayerConnected(playerId)) {
                setPlayerInstance(playerId, "PC_HERO");
                respawnPlayer(playerId);
            }

            mounts.rawdelete(playerId);
            sendRemove(playerId);
        }
    }

    onPlayerJoin = function(playerId) {
        foreach(mountPlayer, mountObject in mounts)
            sendCreate(playerId, mountPlayer, mountObject);
    }

    onPlayerDisconnect = function(playerId, reason) {
        foreach(mountPlayer, mountObject in mounts) {
            if(mountPlayer == playerId) {
                mounts.rawdelete(mountPlayer);
                sendRemove(mountPlayer);
                return;
            }
        }
    }

    sendRemove = function(playerId) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.RemoveMount);
        packet.writeInt16(playerId);
        packet.sendToAll(RELIABLE);
    }

    sendCreate = function(playerId, mountPlayer, mountObject) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.AddMount);
        packet.writeInt16(mountPlayer);
        packet.writeInt16(mountObject.typeId);
        packet.writeInt16(mountObject.armor);
        packet.writeInt16(mountObject.melee);
        packet.writeInt16(mountObject.ranged);
        packet.writeString(mountObject.head);
        packet.writeInt16(mountObject.face);
        packet.writeString(mountObject.body);
        packet.writeInt16(mountObject.skin);
        if(playerId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(playerId, RELIABLE)
    }
}


addEventHandler("onPlayerJoin", System.Mount.onPlayerJoin.bindenv(System.Mount))
addEventHandler("onPlayerDisconnect", System.Mount.onPlayerDisconnect.bindenv(System.Mount))