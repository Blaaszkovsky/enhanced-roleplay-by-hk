
System.Torch <- {
    players = array(getMaxSlots(), false)
    timers = array(getMaxSlots(), 0)

    change = function(playerId) {
        players[playerId] = !players[playerId];

        if (players[playerId] == true)
            timers[playerId] = 900
        else
        {
            timers[playerId] = 0;

            foreach(_item in getPlayer(playerId).inventory.getItems())
            {
                if(_item.instance == "ITLSTORCH")
                {
                    _item.equipped = false
                    _item.sendUpdate(playerId)

                    _giveItem(playerId, Items.id(_item.instance), 1)
                    _unequipItem(playerId, Items.id(_item.instance))
                    removeItem(playerId, _item.instance, 1);
                    break
                }
            }
        }
    }

    onPlayerChangeWeaponMode = function(playerId, oldWeaponMode, newWeaponMode) {
        if(players[playerId] == false)
            return;

        change(playerId)
    }

    onPlayerHit = function(playerId, killerId, dmg) {
        if(players[playerId] == false)
            return;

        change(playerId)
    }

    onSecond = function(){
        foreach(index, player in players){
            if (player == true){
                timers[index] = timers[index]-1;
                if (timers[index] == 0){
                    addPlayerNotification(index, "Pochodnia si� wypali�a");
                    change(index);
                }
            }
        }
    }
}

addEventHandler("onPlayerHit", System.Torch.onPlayerHit.bindenv(System.Torch))
addEventHandler("onPlayerChangeWeaponMode", System.Torch.onPlayerChangeWeaponMode.bindenv(System.Torch))
addEventHandler("onSecond", System.Torch.onSecond.bindenv(System.Torch))
