System.Save <- {

    function autoSave() {
        local counter = PerformanceCounter()
        counter.start()
        for(local i = 0; i < getMaxSlots(); i ++) {
            if(isPlayerConnected(i)) {
                getPlayer(i).save();
            }
        };
        counter.stop()
        stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of autoSave player iteration.");
    }
}

// setTimer(function() {
//     System.Save.autoSave();
// }, 60000 * 15, 0);

