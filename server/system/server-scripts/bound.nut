
System.Bound <- {
    players = [],

    add = function(playerId) {
        players.push(playerId);
        getPlayer(playerId).boundModule.set(true);

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Bound);
        packet.writeInt16(playerId);
        packet.writeBool(true);
        packet.sendToAll(RELIABLE)
    }

    remove = function(playerId) {
        local rFind = players.find(playerId);
        if(rFind == null)
            return;

        players.remove(rFind);
        getPlayer(playerId).boundModule.set(false);

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Bound);
        packet.writeInt16(playerId);
        packet.writeBool(false);
        packet.sendToAll(RELIABLE)
    }

    onPlayerJoin = function(playerId) {
        foreach(_, player in players) {
            local packet = Packet();
            packet.writeUInt8(PacketSystem);
            packet.writeUInt8(SystemPackets.Bound);
            packet.writeInt16(player);
            packet.writeBool(true);
            packet.send(playerId, RELIABLE)
        }
    }

    onPlayerDisconnect = function(playerId, reason) {
        local rFind = players.find(playerId);
        if(rFind == null)
            return;

        players.remove(rFind);

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Bound);
        packet.writeInt16(playerId);
        packet.writeBool(false);
        packet.sendToAll(RELIABLE)
    }
}

addEventHandler("onPlayerJoin", System.Bound.onPlayerJoin.bindenv(System.Bound))
addEventHandler("onPlayerDisconnect", System.Bound.onPlayerDisconnect.bindenv(System.Bound))