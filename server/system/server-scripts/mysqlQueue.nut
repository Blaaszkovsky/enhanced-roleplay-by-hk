
mysqlQueue <- {
    store = []
    timer = null

    function add(text) {
        if(timer == null)
            timer = setTimer(mysqlQueue.reset, 5000, 1);

        store.push(text);
    }

    function reset() {
        mysqlQueue.timer = null;

        local str = "";
        foreach(item in mysqlQueue.store)
            str += item;

        mysqlQueue.store.clear();
        DB.query(str);
    }
}