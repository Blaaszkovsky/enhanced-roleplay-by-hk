
class ActionDrawController
{
    static actionDraws = {};
    
    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case ActionDrawPackets.Create:
                local id = packet.readInt32();
                local text = packet.readString();
                local user = packet.readString();
                local active = packet.readInt8();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();

                local obj = ActionDraw(id);
                obj.text = text;
                obj.user = user;
                obj.active = active;
                obj.position = {x = positionX, y = positionY, z = positionZ};
                obj.addToWorld();

                actionDraws[id] <- obj;
            break;
            case ActionDrawPackets.Delete:
                local id = packet.readInt32();
                if(!(actionDraws.rawin(id)))
                    return;

                getActionDraw(id).remove();
                actionDraws.rawdelete(id);
            break;
            case ActionDrawPackets.Activate:
                local id = packet.readInt32();
                if(!(actionDraws.rawin(id)))
                    return;

                local obj = getActionDraw(id);
                obj.active = 1;
                obj.updateDraw();
            break;
        }
    }
}

getActionDraw <- @(id) ActionDrawController.actionDraws[id];
getActionDraws <- @() ActionDrawController.actionDraws;

RegisterPacketReceiver(Packets.ActionDraw, ActionDrawController.onPacket.bindenv(ActionDrawController));
