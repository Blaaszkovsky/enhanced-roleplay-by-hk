local list = MyGUI.SelectList(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 300), anx(800), any(640))
local exitButton = GUI.Button(anx(Resolution.x/2 - 400) + anx(775), any(Resolution.y/2 - 325), anx(50), any(50), "SG_CHECKBOX.TGA", "X");

local bindExit = Bind.addClick(exitButton, function() {
    hideActionDrawsMenu();
}, PlayerGUI.ActionDraws);

function showActionDrawsMenu() {      
    local actionDraws = getActionDraws();

    if(actionDraws.len() > 0)
    {  
        textSetFont("FONT_OLD_10_WHITE_HI.TGA");
        local buttonWidth = 750;

        foreach(id, actionDraw in actionDraws) {
            local text = actionDraw.id+" - ("+actionDraw.user+")"+(actionDraw.active?"(A)":"(N)")+" - "+actionDraw.text;
            local widthCalc = 0, lastLetter = 0;

            foreach(i, letter in text) {
                if(buttonWidth > widthCalc) {
                    widthCalc += letterWidthPx(letter.tochar()) + letterDistancePx();
                    lastLetter = i;
                }
            }

            list.addOption(id, text.slice(0, lastLetter+1));
        }

        BaseGUI.show();
        ActiveGui = PlayerGUI.ActionDraws;
        list.setVisible(true);
        exitButton.setVisible(true);
    }
    else 
        Chat.addMessage(ChatType.ADMIN, 255, 0, 0, "Brak danych do wy�wietlenia!")
}

function hideActionDrawsMenu() {
    list.clear();
    BaseGUI.hide();
    ActiveGui = null;
    list.setVisible(false);
    exitButton.setVisible(false);
}

addCommandAdmin("drawlist", function (params) {
    showActionDrawsMenu();
}, "Komenda wy�wietlaj�ca list� aktywnych i nieaktywnych draw�w akcji", PlayerRole.Mod);

Bind.addKey(KEY_ESCAPE, hideActionDrawsMenu, PlayerGUI.ActionDraws)