
class ActionDraw 
{
    id = -1

    position = null

    text = null;
    user = null;

    active = 0;
    
    draw = null

    constructor(_id)
    {
        this.id = _id

        this.position = {x = 0,y = 0,z = 0}
        
        this.text = null;
        this.user = null;
             
        this.active = 0;

        this.draw = null
    }

    function addToWorld() {
        draw = WorldInterface.Draw3d(position.x,position.y + 67,position.z);
        draw.addLine(text);
        draw.addLine("DrawID: "+id);
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        //draw.setLineScale(1, 0.65, 0.65);
        draw.distance = 900;
        updateDraw();
    }

    function updateDraw() {
        if(active) draw.visible = true;
    }

    function remove()  {
        draw.remove();
    }
}