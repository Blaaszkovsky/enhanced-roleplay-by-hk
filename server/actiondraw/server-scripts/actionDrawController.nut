
class ActionDrawController
{
    static actionDraws = {};

    static function getPlayerDraws(playerId) {
        local idsOfActionDraws = [], baseId = getPlayer(playerId).id;
        foreach(actionDraw in actionDraws) {
            if(actionDraw.userId == baseId)
                idsOfActionDraws.push(actionDraw.id);
        }
        return idsOfActionDraws;
    }

    static function createActionDraw(playerId, text) {
        local playerObj = getPlayer(playerId);
        local playerDraws = getPlayerDraws(playerId);

        if(playerDraws.len() == 2)
        {
            deleteActionDraw(playerDraws[0] > playerDraws[1] ? playerDraws[1] : playerDraws[0]);
        }

        local pos = getPlayerPosition(playerId);
        local world = getPlayerWorld(playerId);

        local expireAt = time()+43200; //24h in unix

        playerObj.name = mysql_escape_string(playerObj.name);
        text = mysql_escape_string(text);

        Query().insertInto(ActionDraw.ActionDrawTable, ["expireAt", "text", "userId", "userName", "world", "x", "y", "z", "active"
        ], [
            expireAt, "'"+text+"'", playerObj.id, "'"+playerObj.name+"'", "'"+world+"'", pos.x, pos.y, pos.z, 0
        ]).execute();

        local lastId = Query().select("LAST_INSERT_ID() AS LAST").from(ActionDraw.ActionDrawTable).one().LAST;
        //local lastId = actionDraws[actionDraws.len()-1].id+1;

        local newActionDraw = ActionDraw(lastId.tointeger());

        newActionDraw.user = playerObj.name;
        newActionDraw.userId = playerObj.id;
        newActionDraw.text = text;
        newActionDraw.active = 0;
        newActionDraw.expireAt = expireAt;
        newActionDraw.world = world;
        newActionDraw.position = {x = pos.x, y = pos.y, z = pos.z}

        actionDraws[newActionDraw.id] <- newActionDraw;

            ActionDrawPacket(newActionDraw).create();

        return lastId;
    }

    static function deleteActionDraw(id) {
        if(id in actionDraws)
        {
            local obj = actionDraws[id];

            ActionDrawPacket(obj).deleteActionDraw();

            Query().deleteFrom(ActionDraw.ActionDrawTable).where(["id = "+id]).execute();
            actionDraws.rawdelete(id);

            return true;
        }

        return false;
    }

    static function activateActionDraw(id)
    {
        if(id in actionDraws)
        {
            local obj = actionDraws[id];

            if(!obj.active)
            {
                obj.active = 1;

                Query().update(ActionDraw.ActionDrawTable, ["active"], [ 1 ]).where(["id = "+id]).execute();
                ActionDrawPacket(obj).activateActionDraw();

                return true;
            }
        }

        return false;
    }

    static function onInit() {
        local actionDrawInDatabase = Query().select().from(ActionDraw.ActionDrawTable).all();

        foreach(actionDrawObject in actionDrawInDatabase)
        {
            local newActionDrawObject = ActionDraw(actionDrawObject.id.tointeger())

            newActionDrawObject.user = actionDrawObject.userName;
            newActionDrawObject.userId = actionDrawObject.userId;
            newActionDrawObject.text = actionDrawObject.text;
            newActionDrawObject.active = actionDrawObject.active;
            newActionDrawObject.expireAt = actionDrawObject.expireAt;
            newActionDrawObject.world = actionDrawObject.world;
            newActionDrawObject.position = {x = actionDrawObject.x.tointeger(), y = actionDrawObject.y.tointeger(), z = actionDrawObject.z.tointeger()}

            actionDraws[newActionDrawObject.id] <- newActionDrawObject;
        }
 
        print("We have loaded " + actionDrawInDatabase.len() + " action draws.");
    }

    static function onMinute() {
        local idsOfActionDraws = [], cTime = time();

        foreach(actionDraw in actionDraws)
        {
            if(actionDraw.expireAt < cTime)
            {
                local drawId = actionDraw.id;

                actionDraws.rawdelete(drawId);
                Query().deleteFrom(ActionDraw.ActionDrawTable).where(["id = "+drawId]).execute();

                idsOfActionDraws.push(drawId);
            }
        }

        if(idsOfActionDraws.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.ActionDraw);
        packet.writeUInt8(ActionDrawPackets.Delete);
        packet.writeInt16(idsOfActionDraws.len());
        foreach(obj in idsOfActionDraws) {
            packet.writeInt32(obj);
        }
        packet.sendToAll(RELIABLE);
    }

    static function onPlayerJoin(playerId) {
        foreach(actionDraw in actionDraws)
            ActionDrawPacket(actionDraw).create();
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case ActionDrawPackets.Delete:
                onTryToDeleteActionBar(playerId, packet.readInt32());
            break;
        }
    }
}

getActionDraws <- @() ActionDrawController.actionDraws;

addEventHandler("onInit", ActionDrawController.onInit.bindenv(ActionDrawController));
addEventHandler("onMinute", ActionDrawController.onMinute.bindenv(ActionDrawController))
addEventHandler("onPlayerJoin", ActionDrawController.onPlayerJoin.bindenv(ActionDrawController))
RegisterPacketReceiver(Packets.ActionDraw, ActionDrawController.onPacket.bindenv(ActionDrawController));