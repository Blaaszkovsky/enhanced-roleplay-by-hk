
local function getModifier(x)
{
    switch (true) 
    {
        case (x >= 60):
            return 1.6276
        case (x >= 30):
            return 1.3711
        default:
            return 1.1613
    }
}

DMGCalculation <- {};

DMGCalculation.calculateInventoryProtection <- function(obj)
{
    return obj.getProtection();
}

DMGCalculation.calculateDamage <- function (obj, preyObj)
{
    local weaponMode = obj.getWeaponMode();

    if (!("isTemporary" in obj))
    {
        switch (weaponMode)
        {
            case WEAPONMODE_BOW:
            if (hasPlayerItem(obj.id, "ITRW_ARROW") < 1)
                return 0
            break;
            case WEAPONMODE_CBOW:
            if (hasPlayerItem(obj.id, "ITRW_BOLT") < 1)
                return 0
            break;
        }
    }

    local damage = 0, itemUsedToGiveDmg = -1, dmgType = DAMAGE_BLUNT;

    local protections = DMGCalculation.calculateInventoryProtection(preyObj);

    switch (weaponMode)
	{
		case WEAPONMODE_FIST:
            itemUsedToGiveDmg = null;
		break;

		case WEAPONMODE_1HS:
		case WEAPONMODE_2HS:
			itemUsedToGiveDmg = obj.getMeleeWeapon();
		break;

		case WEAPONMODE_BOW:
		case WEAPONMODE_CBOW:
			itemUsedToGiveDmg = obj.getRangedWeapon();
		break;

		case WEAPONMODE_MAG:
			itemUsedToGiveDmg = obj.getMagicWeapon();
		break;
	}

    if(itemUsedToGiveDmg == null || itemUsedToGiveDmg == -1) {
        if(obj.str > 10) {
            local dmg = abs(obj.str/2);
            local protectionForDamage = protections[DAMAGE_BLUNT];
            protectionForDamage = 100 - protectionForDamage;

            dmg = (dmg * protectionForDamage)/100;
            dmg = abs(dmg);
            if(dmg < 5)
                dmg = 5;

            return dmg;
        }

        return 5;
    }

    if(itemUsedToGiveDmg.getRequirement()) {
        foreach(_requirement in itemUsedToGiveDmg.getRequirement()) {
            if(_requirement.type == PlayerAttributes.Mana)
                continue;

            damage += obj.getAttribute(_requirement.type)/2;
        }
    }

    dmgType = itemUsedToGiveDmg.getDmgType();
    local protectionForDamage = protections[dmgType];
    if(protectionForDamage > 100)
        protectionForDamage = 100;

    protectionForDamage = 100 - protectionForDamage;
    damage += itemUsedToGiveDmg.getDamage();
    damage = (damage * protectionForDamage)/100;

    switch (weaponMode)
	{
		case WEAPONMODE_1HS:
            damage = damage * (1 + obj.weapon[0] * 0.0075);
        break;
		case WEAPONMODE_2HS:
            damage = damage * (1 + obj.weapon[1] * 0.0075);
            damage = damage * getModifier(obj.weapon[1]);
		break;
		case WEAPONMODE_BOW:
            damage = damage * (1 + obj.weapon[2] * 0.0075);
            damage = damage * getModifier(obj.weapon[2]);
        break;
		case WEAPONMODE_CBOW:
            damage = damage * (1 + obj.weapon[3] * 0.0075);
            damage = damage * getModifier(obj.weapon[3]);
        break;
	}

    preyObj.resistanceCallback();


    if(itemUsedToGiveDmg.resistance > 0 && itemUsedToGiveDmg.resistance != -1) {
        itemUsedToGiveDmg.resistance = itemUsedToGiveDmg.resistance - 1;
        itemUsedToGiveDmg.sendResistance(obj.id);
    }

    return abs(damage * 0.7);
}
