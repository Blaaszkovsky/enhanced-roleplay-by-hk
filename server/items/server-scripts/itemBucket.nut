local localIdBucket = 0;

class item.Bucket {
    static dbTable = "itemrelation";

    id = -1

    ids = null
    observers = null

    objTypeId = -1
    objId = -1

    maxWeight = 0.0;
    weight = 0.0;

    block = false

    constructor(_objTypeId, _objId) {
        localIdBucket = localIdBucket + 1;

        objTypeId = _objTypeId
        objId = _objId

        id = localIdBucket;

        block = false

        maxWeight = 0.0;
        weight = 0.0;

        ids = []
        observers = []

        local relationsInDatabase = Query().select().from(item.Bucket.dbTable).where(["objTypeId = "+objTypeId, "objId = "+objId]).all();
        foreach(_itemRelation in relationsInDatabase)
            ids.push(_itemRelation.itemId.tointeger());
    }

    function save() {
        Query().deleteFrom(item.Bucket.dbTable).where(["objTypeId = "+objTypeId, "objId = "+objId]).execute();

        if(ids.len() > 0) {
            local arr = [];
            foreach(itemId in ids) {
                local itemObj = getItem(itemId);

                if(itemObj == null)
                    DB.query("DELETE FROM "+item.Object.dbTable+" WHERE id = "+itemId+";");
                else {
                    local stackedLabel = itemObj.stacked ? 1 : 0;
                    local equippedLabel = itemObj.equipped ? 1 : 0;
                    local description = null;

                    DB.query("DELETE FROM "+item.Object.dbTable+" WHERE id = "+itemId+";");
                    arr.append([itemObj.id, "'"+itemObj.instance+"'", "'"+itemObj.name+"'", itemObj.resistance, "'"+itemObj.guid+"'", itemObj.amount, stackedLabel, "'"+description+"'", equippedLabel]);
                }
            }

            Query().insertInto(item.Object.dbTable, ["id", "instance", "name", "resistance", "guid", "amount", "stacked", "description", "equipped"], arr).execute();

            arr = [];
            foreach(itemId in ids)
                arr.append([objTypeId, objId, itemId]);

            
            Query().insertInto(item.Bucket.dbTable, ["objTypeId", "objId", "itemId"], arr).execute();
        }
    }

    function _addItem(itemId) {
        ids.push(itemId);
    }

    function _removeItem(itemId) {
        local index = ids.find(itemId);

        if(index != null)
            ids.remove(index);
    }

    function addItem(itemId) {
        local _item = getItem(itemId);
        if(_item == null)
            return null;

        if(_item.stacked == false) {
            _addItem(itemId);
            item.Packet(this).addItem(itemId);
        }
        else
        {
            foreach(_itemInBucket in getItems()) {
                if(_itemInBucket.instance != _item.instance)
                    continue;

                _itemInBucket.amount = _itemInBucket.amount + _item.amount;
                _itemInBucket.save();

                item.Controller.removeItem(itemId);
                item.Packet(this).resend();
                return;
            }

            _addItem(itemId);
            item.Packet(this).addItem(itemId);
        }
    }

    function removeItem(itemId) {
        _removeItem(itemId);
        item.Packet(this).removeItem(itemId);
    }

    function open(playerId) {
        item.Packet(this).send(playerId);
        observers.push(playerId);
    }

    function close(playerId) {
        local index = observers.find(playerId);

        if(index != null)
            observers.remove(index);
    }

    function hasItemId(_itemId) {
        local index = ids.find(_itemId);
        if(index != null)
            return true;

        return false;
    }

    function hasItem(_item) {
        return hasItemId(_item.id);
    }

    function getItems() {
        local returnTab = [];
        foreach(itemId in ids) {
            local _item = getItem(itemId);
            if(_item == null)
            {
                try {
                    removeItem(itemId);
                    item.Packet(this).removeItem(itemId);
                }catch(error){}
            }else{
                returnTab.push(_item);
            }
        }

        return returnTab;
    }
}