isNeedGenerateItemsXML <- false

class item.Scheme
{
    instance = ""
    flag = -1

    value = -1
    material = -1
    weight = -1
    resistance = 0

    description = ""
    name = ""

    dmg = null;
    dmgType = null;

    resistanceDebuf = 0;
    repairmentTool = 0;

    bonus = null;
    protection = null;
    requirement = null;
    restore = null;

    stacked = false;
    effect = null;

    buffApply = null
    buffRemove = null

    summon = null

    constructor(instance, properties)
    {
        instance = instance.toupper();
        this.instance = instance;
        this.flag = properties.flag;

        this.name = properties.name;
        this.description = "description" in properties ? properties.description : "";

        this.material = properties.material;
        this.value = properties.value;
        this.weight = 0.01;
        this.resistance = 0;
        this.resistanceDebuf = 0;
        this.repairmentTool = 0;

        this.dmg = null;
        this.dmgType = null;

        this.bonus = null;
        this.protection = [];
        this.requirement = null;
        this.restore = null;

        this.stacked = false;

        if(this.flag == ITEMCAT_RUNE || this.flag == ITEMCAT_SCROLL)
            this.stacked = true;

        this.effect = null;

        this.summon = null;

        if("bonus" in properties)
            this.bonus = properties.bonus;

        if("weight" in properties)
            this.weight = properties.weight/1.0;

        if("dmg" in properties)
            this.dmg = properties.dmg;

        if("dmgType" in properties)
            this.dmgType = properties.dmgType;

        if("protection" in properties)
            this.protection = properties.protection;

        if("requirement" in properties)
            this.requirement = properties.requirement;

        if("restore" in properties)
            this.restore = properties.restore;

        if("stacked" in properties)
            this.stacked = properties.stacked;

        if("resistance" in properties)
            this.resistance = properties.resistance;

        if("resistanceDebuf" in properties)
            this.resistanceDebuf = properties.resistanceDebuf;

        if("repairmentTool" in properties)
            this.repairmentTool = properties.repairmentTool;

        if("effect" in properties)
            this.effect = properties.effect;

        if("buffApply" in properties)
            this.buffApply = properties.buffApply;

        if("buffRemove" in properties)
            this.buffRemove = properties.buffRemove;

        if("summon" in properties)
            this.summon = properties.summon;

        if (instance in item.schemes)
        print(instance)
            item.schemes[instance] <- this;

        if (Items.id(instance) == -1)
            isNeedGenerateItemsXML = true
    }


    /**
     * get requirement for item
     */
    function getRequirement() {
        return requirement;
    }

    /**
     * Return dmg type of weapon
     */
    function getDmgType() {
        return dmgType;
    }

    /**
     * Return dmg
     */
    function getDamage() {
        return dmg;
    }

    function sendResistance(_) {};

    static function isBowWeapon(weaponId) {
        local instance = Items.name(weaponId);

        if(instance in item.schemes)
            return item.schemes[instance].flag == ITEMCAT_BOW;

        return false;
    }

    static function isOneHWeapon(weaponId) {
        local instance = Items.name(weaponId);

        if(instance in item.schemes)
            return item.schemes[instance].flag == ITEMCAT_ONEH;

        return false;
    }
}

function getItemScheme(instance) {
    instance = instance.toupper();

    if(instance in item.schemes)
        return item.schemes[instance];

    return null
}

function getItemName(instance) {
    return getItemScheme(instance).name;
}