
class item.Controller
{
    static items = {}
    static buckets = {}

    static function onInit() {
        DB.queryGet("DELETE FROM `item` WHERE `id` NOT IN (SELECT `itemId` FROM `itemrelation`);");
        local itemsInDatabase = Query().select().from(item.Object.dbTable).all();

        foreach(_item in itemsInDatabase) {
            item.nextId = _item.id;

            local obj = item.Object(item.nextId, _item.instance);
            obj.name = _item.name;
            obj.resistance = _item.resistance;
            obj.guid = _item.guid;
            obj.amount = _item.amount;
            obj.stacked = _item.stacked == 1 ? true : false;
            obj.description = _item.description;
            obj.equipped = _item.equipped == 1 ? true : false;

            item.Controller.items[item.nextId] <- obj;
        }

        item.nextId = item.nextId + 1;
    }

    static function onPacket(playerId, packet) {
        local identfier = packet.readUInt8();
        if(identfier != item.packetId)
            return;

        local typ = packet.readUInt8();

        switch(typ)
        {
            case ItemPackets.AddItemBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local itemId = packet.readUInt32();
                local bucket = buckets[bucketId];

                if(bucket.block)
                    return;

                local _item = getItem(itemId);

                if(bucket instanceof PlayerInventory) {
                    _giveItem(bucket.playerId, Items.id(_item.instance), _item.amount);
                }

                bucket.addItem(itemId);

                addPlayerLog(playerId, "Doda� do objTypeId: " + bucket.objTypeId + " objId: " + bucket.objId + " " + _item.instance + " x" + _item.amount)
            break;
            case ItemPackets.RemoveItemBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local bucket = buckets[bucketId];
                if(bucket.block)
                    return;

                local itemId = packet.readUInt32();
                local _item = getItem(itemId);

                if(bucket instanceof PlayerInventory) {
                    if(_item.equipped == true)
                        unequipItem(bucket.playerId, _item);
                }

                addPlayerLog(playerId, "Wyciagnal z objTypeId: " + bucket.objTypeId + " objId: " + bucket.objId + " " + _item.instance + " x" + _item.amount)
                bucket.removeItem(itemId);
            break;
            case ItemPackets.ExchangeItemBucket:
                local bucketOldId = packet.readUInt16();
                local bucketNewId = packet.readUInt16();
                if(!(bucketOldId in buckets))
                    return;

                if(!(bucketNewId in buckets))
                    return;

                if(buckets[bucketOldId].block || buckets[bucketNewId].block)
                    return;

                local itemId = packet.readUInt32();
                local amount = packet.readUInt32();

                local itemObject = getItem(itemId);
                if(itemObject.stacked == false)
                    return;

                if(itemObject.amount <= amount)
                    return;

                if (amount <= 0)
                    return;

                itemObject.amount = itemObject.amount - amount;

                local newItem = createItem(itemObject.instance);
                newItem.stacked = true;
                newItem.amount = amount;
                newItem.name = itemObject.name;
                newItem.guid = itemObject.guid;
                newItem.description = itemObject.description;
                newItem.resistance = itemObject.resistance;
                newItem.equipped = false;

                newItem.save();
                itemObject.save();

                buckets[bucketNewId].addItem(newItem.id);
                item.Packet(buckets[bucketOldId]).resend();
                item.Packet(buckets[bucketNewId]).resend();

                addPlayerLog(playerId, "Przerzuci� do objTypeId: " + buckets[bucketNewId].objTypeId + " objId: " + buckets[bucketNewId].objId + " " + itemObject.instance + " x" + amount)
            break;
            case ItemPackets.AdminCreateItem:
                local instance = packet.readString();
                local name = packet.readString();
                local description = packet.readString();
                local guid = packet.readString();
                local amount = packet.readUInt32();
                local stacked = packet.readBool();
                local resistance = packet.readInt16();
                local target = packet.readInt16();

                local player = getPlayer(target);
                if(player == null)
                    return;

                if(player.loggIn == false)
                    return;

                local _item = createItem(instance, amount);
                _item.name = name;
                _item.description = description;
                _item.guid = guid;
                _item.stacked = stacked;
                _item.resistance = resistance;
                player.inventory.addItem(_item.id);

                getAdmin(playerId).saveLog("Stworzyl item: " + instance + " x" + amount + " guid: " + guid, playerId);
            break;
        }
    }

    static function createBucket(objTypeId, objId) {
        local obj = item.Bucket(objTypeId, objId);
        buckets[obj.id] <- obj.weakref();
        return obj;
    }

    static function removeBucket(bucketId) {
        if(bucketId in buckets)
            buckets.rawdelete(bucketId);
    }

    static function deleteCorespondingItemBucket(itemId) {
        foreach(_bucket in buckets) {
            if(_bucket == null)
                continue;
            if(_bucket.hasItemId(itemId)) {
                if(_bucket instanceof PlayerInventory)
                    unequipItem(_bucket.playerId, getItem(itemId));

                _bucket.removeItem(itemId);
                return;
            }
        }
    }

    static function updateCorespondingItemBucket(itemId) {
        foreach(_bucket in buckets) {
            if(_bucket.hasItemId(itemId)) {
                item.Packet(_bucket).resend();
                return;
            }
        }
    }

    static function removeItem(itemId) {
        if(itemId in item.Controller.items) {
            local _item = getItem(itemId);
            _item.remove();
            item.Controller.items.rawdelete(itemId);
        }
    }
}

addEventHandler ("onInit", item.Controller.onInit.bindenv(item.Controller));
addEventHandler ("onPacket", item.Controller.onPacket.bindenv(item.Controller));

function getItem(id) {
    if(id in item.Controller.items)
        return item.Controller.items[id];

    return null;
}

function createItem(instance, amount = 1) {
    local obj = item.Object(item.nextId, instance);
    obj.amount = amount;
    obj.save();

    item.Controller.items[item.nextId] <- obj;
    item.nextId = item.nextId + 1;

    return obj;
}

