
class item.Object {
    static dbTable = "item";

    id = -1
    flag = -1
    instance = ""

    name = ""
    guid = ""
    description = ""

    amount = 0
    resistance = 0
    stacked = false

    equipped = false

    constructor(_id, _instance) {
        _instance = _instance.toupper();

        this.id = _id;
        this.instance = _instance;

        local scheme = getScheme();

        this.flag = scheme.flag;

        this.name = scheme.name;
        this.guid = "";
        this.description = null;

        this.amount = 1
        this.resistance = scheme.resistance;
        this.stacked = scheme.stacked;
        this.equipped = false;
    }

    function save() {
        local stackedLabel = stacked ? 1 : 0;
        local equippedLabel = equipped ? 1 : 0;

        name = mysql_escape_string(name);

        description = null;

        DB.query("DELETE FROM "+item.Object.dbTable+" WHERE id = "+id+";");

        Query().insertInto(item.Object.dbTable, ["id", "instance", "name", "resistance", "guid", "amount", "stacked", "description", "equipped"
        ], [
            id, "'"+instance+"'", "'"+name+"'", resistance, "'"+guid+"'", amount, stackedLabel, description, equippedLabel
        ]).execute();
    }

    /**
     * remove item from database
     */
    function remove() {
        DB.query("DELETE FROM "+item.Object.dbTable+" WHERE id = "+id+";");
    }

    /**
     * send update packet to player
     */
    function sendUpdate(playerId) {
        local packet = Packet();
        packet.writeUInt8(item.packetId);
        packet.writeUInt8(ItemPackets.UpdateItem);
        packet.writeUInt32(id);
        packet.writeUInt32(amount);
        packet.writeInt16(resistance);
        packet.writeString(name);
        packet.writeString(description);
        packet.writeString(guid);
        packet.writeBool(equipped);
        packet.send(playerId, RELIABLE);
    }

    /**
     * Return item protection
     */
    function getProtection() {
        local schemeProtection = getScheme().protection;
        if(resistance == -1 || resistance > 0)
            return schemeProtection;

        local debuff = getScheme().resistanceDebuf;
        local newProt = [];
        foreach(_t, prot in schemeProtection) {
            local _prot = prot;
            _prot = _prot - debuff;
            if(_prot < 0)
                _prot = 0;

            newProt.push(_prot);
        }

        return newProt;
    }

    /**
     * get requirement for item
     */
    function getRequirement() {
        return getScheme().requirement;
    }

    /**
     * Return scheme from items.nut
     */
    function getScheme()
    {
        return item.schemes[instance];
    }

    /**
     * Return dmg type of weapon
     */
    function getDmgType() {
        return getScheme().dmgType;
    }

    /**
     * Return dmg
     */
    function getDamage() {
        local schemeDmg = getScheme().dmg;
        if(resistance == -1 || resistance > 0)
            return schemeDmg;

        local debuff = getScheme().resistanceDebuf;
        schemeDmg = schemeDmg - debuff;
        if(schemeDmg <= 1)
            schemeDmg = 1;

        return schemeDmg;
    }

    /**
     * Send resistance
     */
    function sendResistance(playerId) {
        local packet = Packet();
        packet.writeUInt8(item.packetId);
        packet.writeUInt8(ItemPackets.Resistance);
        packet.writeUInt32(id);
        packet.writeInt16(resistance);
        packet.send(playerId, RELIABLE);
    }
}