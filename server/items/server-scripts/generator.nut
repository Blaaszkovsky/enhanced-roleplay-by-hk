
function rewritePlayerEquipment() {
    local items = Query().select().from(Player.itemsTable).all();

    if(items.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(v,itemDB in items) {
        local scheme = getItemScheme(itemDB.instance);
        if(scheme == null)
            continue;

        itemDB.instance = itemDB.instance.toupper();

        if(scheme.stacked) {
            local obj = createItem(itemDB.instance, itemDB.amount);
            obj.equipped = itemDB.equipped;
            obj.save();
            Query().insertInto(item.Bucket.dbTable, ["objTypeId", "objId", "itemId"], [ItemRelationType.Player, itemDB.playerId, obj.id]).execute();
        }else{
            for(local i = 0; i < itemDB.amount; i++) {
                local obj = createItem(itemDB.instance, 1);
                obj.save();
                Query().insertInto(item.Bucket.dbTable, ["objTypeId", "objId", "itemId"], [ItemRelationType.Player, itemDB.playerId, obj.id]).execute();
            }
        }
        print("--- item generator ("+v+"/"+items.len()+") ---");
        Query().deleteFrom(Player.itemsTable).where(["id = "+itemDB.id]).execute();
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of item regenerator.");
    exit();
}

function rewriteVobsEquipment() {
    local vobs = Query().select().from(GameVob.VobTable).where(["items<>''"]).all();

    if(vobs.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(iIterator, vob in vobs) {
        vob.items = split(vob.items, ",");
        local itemsInVob = {};

        foreach(params in vob.items)
        {
            local args = sscanf("ds", params);
            if (args)
                itemsInVob[args[1]] <- args[0];
        }

        foreach(instance,amount in itemsInVob) {
            local scheme = getItemScheme(instance);
            if(scheme == null)
                continue;

            instance = instance.toupper();

            if(scheme.stacked) {
                local obj = createItem(instance, amount);
                obj.equipped = false;
                obj.save();
                Query().insertInto(item.Bucket.dbTable, ["objTypeId", "objId", "itemId"], [ItemRelationType.Vob, vob.id, obj.id]).execute();
            }else{
                for(local i = 0; i < amount; i++) {
                    local obj = createItem(instance, 1);
                    obj.save();
                    Query().insertInto(item.Bucket.dbTable, ["objTypeId", "objId", "itemId"], [ItemRelationType.Vob, vob.id, obj.id]).execute();
                }
            }
            print("--- vob to new items generator ("+iIterator+"/"+vobs.len()+") ---");
            DB.queryGet("UPDATE "+GameVob.VobTable+" SET items = '' WHERE id = "+vob.id+";");
        }
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of item regenerator.");
    exit();
}

function rewritePlayerCards()
{
    local players = Query().select().from(Player.dbTable).where(["personalCard<>''"]).all();

    if(players.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(iIterator, _player in players) {
        local rId = _player.id;
        local personalCard = split(_player.personalCard, "&");

        local wfile = file("database/cards/"+rId, "w");
        foreach(value in personalCard)
            wfile.write(value+"\n");

        wfile.close();
        print("--- player cards generator ("+iIterator+"/"+players.len()+") ---");
        DB.queryGet("UPDATE "+Player.dbTable+" SET personalCard = '' WHERE id = "+rId+";");
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of cards regenerator.");
    exit();
}

function rewritePlayerNotes()
{
    local players = Query().select().from(Player.dbTable).where(["personalNotes<>''"]).all();

    if(players.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(iIterator, _player in players) {
        local rId = _player.id;
        local personalNote = split(_player.personalNotes, "&");

        local wfile = file("database/notes/"+rId, "w");
        foreach(value in personalNote)
            wfile.write(value+"\n");

        wfile.close();
        print("--- player notes generator ("+iIterator+"/"+players.len()+") ---");
        DB.queryGet("UPDATE "+Player.dbTable+" SET personalNotes = '' WHERE id = "+rId+";");
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of notes regenerator.");
    exit();
}

function rewriteSpecialKeys() {
    local vobs = Query().select().from(GameVob.VobTable).where(["keyId <> -1"]).all();

    if(vobs.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(iIterator, vob in vobs) {
        print("--- special keys generator ("+iIterator+"/"+vobs.len()+") ---");
        DB.queryGet("UPDATE "+GameVob.VobTable+" SET keyId = -1, guid = '"+("KLUCZ_"+vob.keyId)+"' WHERE id = "+vob.id+";");
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of item regenerator.");
    exit();
}

function rewriteSpecialKeysToItems() {
    local items = Query().select().from(item.Object.dbTable).where(["guid = '' AND instance LIKE 'ITKE_SDRP_DOOR_%'"]).all();

    if(items.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(iIterator,_item in items) {
        print("--- special keys generator ("+iIterator+"/"+items.len()+") ---");
        local instance = _item.instance.slice(13);
        DB.queryGet("UPDATE "+item.Object.dbTable+" SET guid = '"+("KLUCZ_"+instance)+"' WHERE id = "+_item.id+";");
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of item regenerator.");
    exit();
}

function rewritePlayerKeySettings() {
    local accounts = Query().select("id").from(Account.dbTable).all();

    if(accounts.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(accountId in accounts) {
        local rfile = io.file("database/keys/"+accountId.id, "r");
        if (rfile.isOpen == false)
        {
            rfile.close();
            rfile = io.file("database/keys/"+accountId.id, "w");
            rfile.write("\n");
        }
        rfile.close();
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of account key regenerator.");
}

function rewritePlayerKeySettings() {
    local accounts = Query().select("id").from(Account.dbTable).all();

    if(accounts.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(accountId in accounts) {
        local rfile = io.file("database/keys/"+accountId.id, "r");
        if (rfile.isOpen == false)
        {
            rfile.close();
            rfile = io.file("database/keys/"+accountId.id, "w");
            rfile.write("\n");
        }
        rfile.close();
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of account key regenerator.");
}

function rewriteMySQLPasswordAndEmails() {
    local accounts = Query().select().from(Account.dbTable).all();

    if(accounts.len() == 0)
        return;

    local counter = PerformanceCounter()
    counter.start()

    foreach(account in accounts) {
        if(account.password.find("_") != null)
            return;

        local passwd = "_"+md5(Account.saltPassword + account.password);
        DB.queryGet("UPDATE "+Account.dbTable+" SET password = '"+passwd+"' WHERE id = "+account.id+";");
    }

    counter.stop()
    stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of account password regenerator.");
}

function generateItemsXML()
{
    if (isNeedGenerateItemsXML)
    {
        local file = io.file("items.xml", "w")

        file.write("<items>\n")

        foreach(instance, _ in item.schemes)
            file.write("\t<item><instance>" + instance + "</instance></item>\n")

        file.write("</items>")

        file.close()
    }
}

addEventHandler ("onInit", function () {
    generateItemsXML()
    rewritePlayerEquipment();
    rewriteVobsEquipment();
    rewritePlayerCards();
    rewritePlayerNotes();
    rewriteSpecialKeys();
    rewriteSpecialKeysToItems();
    rewritePlayerKeySettings();
    rewriteMySQLPasswordAndEmails();
});

