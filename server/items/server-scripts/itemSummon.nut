class item.Summon
{
    name = ""
    str = 0
    dex = 0
    hp = 0
    count = 1

    constructor(properties)
    {
        name = "";

        str = 0;
        dex = 0;
        hp = 0;

        count = 1;

        if("name" in properties)
            this.name = properties.name;

        if("str" in properties)
            this.str = properties.str;

        if("dex" in properties)
            this.dex = properties.dex;

        if("hp" in properties)
            this.hp = properties.hp;

        if("count" in properties)
            this.count = properties.count;
    }

    function getName()
    {
        if(typeof(this.name) == "array")
        {
            local index = irand(this.name.len() - 1)
            return this.name[index];
        }

        return this.name;
    }
}