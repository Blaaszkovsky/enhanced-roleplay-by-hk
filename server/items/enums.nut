
enum ItemPackets {
    SendBucket,
    AddItemBucket,
    RemoveItemBucket,
    ExchangeItemBucket,
    UpdateItem,
    AdminCreateItem,
    Resistance
}

enum ItemRelationType {
    Player,
    Vob,
    Trade,
    Ground,
}

enum ItemGUISide {
    Left,
    Right,
}

enum ItemGUIPosition {
    Top,
    Bottom,
}

enum ItemGUISize {
    Full,
    Half,
}