class item.Bucket {
    id = -1

    ids = null

    weight = 0.0
    maxWeight = 0.0
    ui = null

    constructor(_id) {
        id = _id;

        ids = []

        weight = 0.0
        maxWeight = 0.0

        ui = null
    }

    function onRefresh() {
        weight = calculateWeight();

        if(ui != null)
            ui.refresh();
    }

    function calculateWeight() {
        local value = 0.0;
        foreach(itemId in ids) {
            local _item = getItem(itemId);
            if(_item.flag != ITEMCAT_BAG)
                value += _item.getWeight();
        }

        return value;
    }

    function addItem(itemId) {
        ids.push(itemId);
    }

    function removeItem(itemId) {
        local index = ids.find(itemId);

        if(index != null)
            ids.remove(index);
    }

    function hasItemId(_itemId) {
        local index = ids.find(_itemId);
        if(index != null)
            return true;

        return false;
    }

    function hasItem(_item) {
        return hasItemId(_item.id);
    }

    function getItems() {
        local returnTab = [];
        foreach(itemId in ids)
            returnTab.push(getItem(itemId));

        return returnTab;
    }

    function rechangeItem(_item, amount, connectionBucket) {
        if(_item.amount < amount)
            return;

        if(connectionBucket.maxWeight != 0.0) {
            if((_item.getWeight() + connectionBucket.weight) > connectionBucket.maxWeight)
                return;
        }

        if(_item.amount == amount)
        {
            item.Packet(this).removeItem(_item.id);
            item.Packet(connectionBucket).addItem(_item.id);
            return;
        }

        item.Packet(this).rechangeItem(_item.id, amount, connectionBucket.id);
    }

    function isBlocked() {
        if(ui != null)
            return ui.blocked;

        return false;
    }
}