
DuplicatingInterface <- {
    bucket = null,
    type = -1,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 200), anx(400), any(430), "SG_BOX.TGA");

        result.uis <- [];
        result.renders <- [];
        result.buttons <- [];
        result.draw <- Draw(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 230), "");

        result.slider <- GUI.ScrollBar(anx(Resolution.x/2 + 150), any(Resolution.y/2 - 180), anx(35), any(390), "SG_SCROLL.TGA", "SG_BUTTON.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical);

        result.bindForScrollbar <- Bind.onChange(result.slider, function(value) {
            DuplicatingInterface.refresh();
        }, PlayerGUI.Duplicating);

        result.backgroundOperation <- Texture(anx(Resolution.x/2 - 200), any(Resolution.y/2 + 240), anx(400), any(150), "SG_BOX.TGA");
        result.goWithDuplicating <- GUI.Button(anx(Resolution.x/2 - 200) + anx(25), any(Resolution.y/2 + 265), anx(150), any(50), "SG_BUTTON.TGA", "Dorob");
        result.goWithClosing <- GUI.Button(anx(Resolution.x/2 - 200) + anx(225), any(Resolution.y/2 + 265), anx(150), any(50), "SG_BUTTON.TGA", "Zamknij");
        result.drawWithDuplicating <- Draw(anx(Resolution.x/2 - 175), any(Resolution.y/2 + 340), "");

        result.goWithDuplicating.bind(EventType.Click, function(element) {
            PlayerPacket().duplicating(element.attribute.id);
        });

        result.goWithClosing.bind(EventType.Click, function(element) {
            DuplicatingInterface.bucket.backgroundOperation.visible = false;
            DuplicatingInterface.bucket.drawWithDuplicating.visible = false;
            DuplicatingInterface.bucket.goWithDuplicating.setVisible(false);
            DuplicatingInterface.bucket.goWithClosing.setVisible(false);
        });

        return result;
    }

    refresh = function()
    {
        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        bucket.buttons.clear();
        bucket.renders.clear();
        bucket.uis.clear();

        local _items = Hero.inventory.getItems();
        local passedItems = 0;
        local marginVisible = bucket.slider.getValue() * 3;

        local x = 0;
        local y = 0;

        bucket.draw.text = "Dorabianie kluczy";
        bucket.draw.visible = true;

        foreach(_item in _items) {
            if(!((type == InteractionVob.Carpentry && (_item.instance.toupper().find("ITKE_") != null))))
                continue;

            passedItems = passedItems + 1;

            if(marginVisible > passedItems)
                continue;

            local butt = GUI.Button(anx(15 + (x * 105)), any(15 + (y * 105)), anx(100), any(100), "SG_BOX.TGA", "", bucket.window);
            local render = ItemRender(anx(Resolution.x/2 - 200) + anx(15 + (x * 105)), any(Resolution.y/2 - 200) + any(15 + (y * 105)), anx(100), any(100), _item.instance);
            butt.attribute = _item;
            local background = Texture(anx(Resolution.x/2 - 200) + anx(15 + (x * 105)), any(Resolution.y/2 - 200) + any(15 + (y * 105)), anx(100), any(100), "SG_BOX.TGA")


            butt.bind(EventType.Click, function(element) {
                DuplicatingInterface.selectItem(element.attribute);
            });

            bucket.buttons.push(butt);
            bucket.renders.push(render);
            bucket.uis.push(background);

            x = x + 1;
            if(x >= 3) {
                x = 0;
                y = y + 1
            }

            if(y == 4)
                break;
        }

        foreach(butt in bucket.buttons)
            butt.setVisible(true);

        foreach(ui in bucket.uis) {
            ui.visible = true;
            ui.top();
        }

        foreach(butt in bucket.renders) {
            butt.visible = true;
            butt.top();
        }
    }

    selectItem = function(_item) {
        DuplicatingInterface.bucket.backgroundOperation.visible = true;
        DuplicatingInterface.bucket.drawWithDuplicating.visible = true;
        DuplicatingInterface.bucket.goWithDuplicating.setVisible(true);
        DuplicatingInterface.bucket.goWithClosing.setVisible(true);
        DuplicatingInterface.bucket.goWithDuplicating.attribute = _item;

        DuplicatingInterface.bucket.drawWithDuplicating.text = _item.name + "\n"+_item.guid;
    }

    show = function(interactionType) {
        type = interactionType;
        bucket = prepareWindow();

        BaseGUI.show();
        ActiveGui = PlayerGUI.Duplicating;
        BlackScreen(1000);

        refresh();

        bucket.window.setVisible(true);
        bucket.slider.setVisible(true);
    }

    hide = function() {
        ActiveGui = null;
        BaseGUI.hide();

        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);
        bucket.window.destroyAll();
        bucket.slider.destroy();
        bucket = null;

        useClosestMob(heroId, "BENCH", -1);
    }

    onInteractLostMob = function() {
        if(ActiveGui == PlayerGUI.Duplicating)
            hide();
    }
}

Bind.addKey(KEY_ESCAPE, DuplicatingInterface.hide.bindenv(DuplicatingInterface), PlayerGUI.Duplicating)
addEventHandler("onForceCloseGUI", DuplicatingInterface.onInteractLostMob.bindenv(DuplicatingInterface));