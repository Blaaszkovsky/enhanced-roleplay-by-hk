
class item.Packet
{
    packet = null
    bucket = null

    constructor(_bucket)
    {
        packet = Packet();
        packet.writeUInt8(item.packetId);

        bucket = _bucket
    }

    function addItem(itemId) {
        packet.writeUInt8(ItemPackets.AddItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeUInt32(itemId);
        packet.send(RELIABLE);
    }

    function removeItem(itemId) {
        packet.writeUInt8(ItemPackets.RemoveItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeUInt32(itemId);
        packet.send(RELIABLE);
    }

    function rechangeItem(itemId, amount, bucketId) {
        packet.writeUInt8(ItemPackets.ExchangeItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeUInt16(bucketId);
        packet.writeUInt32(itemId);
        packet.writeUInt32(amount);
        packet.send(RELIABLE);
    }
}