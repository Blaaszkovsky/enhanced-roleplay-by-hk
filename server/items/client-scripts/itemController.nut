
class item.Controller
{
    static items = {}
    static buckets = {}

    static function onPacket(packet) {
        local identfier = packet.readUInt8();
        if(identfier != item.packetId)
            return;

        local typ = packet.readUInt8();

        switch(typ)
        {
            case ItemPackets.SendBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local bucket = buckets[bucketId];
                foreach(itemId in bucket.ids)
                    destroyItem(itemId);

                local maxWeight = packet.readFloat();
                bucket.maxWeight = maxWeight;
                bucket.ids.clear();

                local itemsInside = packet.readUInt16();
                for(local i = 0; i < itemsInside; i++) {
                    local itemId = packet.readUInt32();
                    local instance = packet.readString();
                    local name = packet.readString();
                    local description = packet.readString();
                    local guid = packet.readString();
                    local amount = packet.readUInt32();
                    local resistance = packet.readUInt16();
                    local stacked = packet.readBool();
                    local equipped = packet.readBool();

                    local itemObject = item.Object(itemId, instance);
                    itemObject.id = itemId;
                    itemObject.name = name;
                    itemObject.description = description;
                    itemObject.guid = guid;
                    itemObject.amount = amount;
                    itemObject.resistance = resistance;
                    itemObject.stacked = stacked;
                    itemObject.equipped = equipped;
                    bucket.ids.push(itemId);

                    if(itemId in item.Controller.items)
                        item.Controller.items.rawdelete(itemId);

                    item.Controller.items[itemId] <- itemObject;
                }

                bucket.onRefresh();
            break;
            case ItemPackets.ExchangeItemBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local bucket = buckets[bucketId];
                foreach(itemId in bucket.ids)
                    destroyItem(itemId);

                local maxWeight = packet.readFloat();
                bucket.maxWeight = maxWeight;
                bucket.ids.clear();

                local itemsInside = packet.readUInt16();
                for(local i = 0; i < itemsInside; i++) {
                    local itemId = packet.readUInt32();
                    local instance = packet.readString();
                    local name = packet.readString();
                    local description = packet.readString();
                    local guid = packet.readString();
                    local amount = packet.readUInt32();
                    local resistance = packet.readUInt16();
                    local stacked = packet.readBool();
                    local equipped = packet.readBool();

                    local itemObject = item.Object(itemId, instance);
                    itemObject.id = itemId;
                    itemObject.name = name;
                    itemObject.description = description;
                    itemObject.guid = guid;
                    itemObject.amount = amount;
                    itemObject.resistance = resistance;
                    itemObject.stacked = stacked;
                    itemObject.equipped = equipped;
                    bucket.ids.push(itemId);

                    if(itemId in item.Controller.items)
                        item.Controller.items.rawdelete(itemId);

                    item.Controller.items[itemId] <- itemObject;
                }

                bucket.onRefresh();
            break;
            case ItemPackets.AddItemBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local itemId = packet.readUInt32();
                local bucket = buckets[bucketId];

                local instance = packet.readString();
                local name = packet.readString();
                local description = packet.readString();
                local guid = packet.readString();
                local amount = packet.readUInt32();
                local resistance = packet.readUInt16();
                local stacked = packet.readBool();
                local equipped = packet.readBool();

                if(itemId in item.Controller.items)
                    item.Controller.items.rawdelete(itemId);

                local itemObject = item.Object(itemId, instance);
                itemObject.id = itemId;
                itemObject.name = name;
                itemObject.description = description;
                itemObject.guid = guid;
                itemObject.amount = amount;
                itemObject.resistance = resistance;
                itemObject.stacked = stacked;
                itemObject.equipped = equipped;

                item.Controller.items[itemId] <- itemObject;

                bucket.addItem(itemId);
                bucket.onRefresh();
            break;
            case ItemPackets.RemoveItemBucket:
                local bucketId = packet.readUInt16();
                if(!(bucketId in buckets))
                    return;

                local itemId = packet.readUInt32();
                local bucket = buckets[bucketId];
                bucket.removeItem(itemId);
                print("usuwam "+itemId);
                bucket.onRefresh();
            break;
            case ItemPackets.UpdateItem:
                local itemId = packet.readUInt32();
                local amount = packet.readUInt32();
                local resistance = packet.readInt16();
                local name = packet.readString();
                local description = packet.readString();
                local guid = packet.readString();
                local equipped = packet.readBool();

                local itemObj = item.Controller.items[itemId];
                itemObj.amount = amount;
                itemObj.name = name;
                itemObj.description = description;
                itemObj.guid = guid;
                itemObj.resistance = resistance;
                itemObj.equipped = equipped;
            break;
            case ItemPackets.Resistance:
                local itemId = packet.readUInt32();
                local resistance = packet.readInt16();

                local itemObj = item.Controller.items[itemId];
                itemObj.resistance = resistance;
            break;
        }
    }

    static function createBucket(bucketId) {
        local obj = item.Bucket(bucketId);
        buckets[obj.id] <- obj.weakref();
        return obj;
    }
}

addEventHandler ("onPacket", item.Controller.onPacket.bindenv(item.Controller));

function getItem(id) {
    if(id in item.Controller.items)
        return item.Controller.items[id];

    return null;
}

function destroyItem(itemId) {
    item.Controller.items.rawdelete(itemId);
}
