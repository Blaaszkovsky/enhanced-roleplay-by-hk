
class item.Object {
    id = -1
    instance = ""

    name = ""
    guid = ""
    description = ""

    amount = 0
    resistance = 0
    stacked = false
    equipped = false

    flag = -1

    constructor(_id, _instance) {
        _instance = _instance.toupper();

        this.id = _id;
        this.instance = _instance;

        local scheme = getScheme();

        this.flag = scheme.flag;

        this.name = scheme.name;
        this.guid = "";
        this.description = scheme.description;

        this.amount = 1
        this.resistance = scheme.resistance;
        this.stacked = scheme.stacked;
        this.equipped = false;
    }

    /**
     * Return weight of item
     */
    function getWeight() {
        return getScheme().weight * amount;
    }

    /**
     * Return scheme from items.nut
     */
    function getScheme() {
        return item.schemes[instance];
    }

    /**
     * Return item protection
     */
    function getProtection() {
        local schemeProtection = getScheme().protection;
        if(resistance == -1 || resistance > 0)
            return schemeProtection;

        local debuff = getScheme().resistanceDebuf;
        local newProt = [];
        foreach(_t, prot in schemeProtection) {
            local _prot = prot;
            _prot = _prot - debuff;
            if(_prot < 0)
                _prot = 0;

            newProt[_t] = _prot;
        }

        return newProt;
    }

    /**
     * get requirement for item
     */
    function getRequirement() {
        return getScheme().requirement;
    }

    /**
     * Return dmg type of weapon
     */
    function getDmgType() {
        return getScheme().dmgType;
    }

    /**
     * Return dmg
     */
    function getDamage() {
        return getScheme().dmg;
    }

    /**
     * Returns description
     */
    function getFullStringDescription()
    {
        local scheme = getScheme();
        if(description != null)
            local string = description+"\n";
        else
            local string = "";
        switch(scheme.flag)
        {
            case ITEMCAT_ARMOR:
            case ITEMCAT_MASK:
            case ITEMCAT_HELMET:
            case ITEMCAT_SHIELD:
                foreach(damageId, label in Config["DamageLabel"])
                {
                    if(scheme.protection.len() > damageId)
                        string += "Ochrona "+label+": "+scheme.protection[damageId]+"\n";
                }
            break;
            case ITEMCAT_ONEH: case ITEMCAT_TWOH: case ITEMCAT_BOW: case ITEMCAT_CBOW: case ITEMCAT_RUNE: case ITEMCAT_SCROLL:
                string += "Typ obra�e�: "+Config["DamageLabel"][scheme.dmgType]+"\n";
                string += "Obra�enia "+scheme.dmg+"\n";

                if (scheme.flag == ITEMCAT_ONEH || scheme.flag == ITEMCAT_TWOH)
                {
                    local daedalusInstance = Daedalus.instance(instance)
                    string += "Zasi�g: " + daedalusInstance.range + "\n"
                    if(scheme.flag == ITEMCAT_TWOH)
                    string += "Bro� trzymana w dw�ch r�kach - lepszy modyfikator obra�e�\n"
                }
                else if(scheme.flag == ITEMCAT_BOW || scheme.flag == ITEMCAT_CBOW)
                {
                    string += "Bro� miotaj�ca - lepszy modyfikator obra�e�\n"
                }
            break;
            case ITEMCAT_RING: case ITEMCAT_BELT:
                foreach(damageId, label in Config["DamageLabel"])
                {
                    if(scheme.protection.len() > damageId)
                        string += "Ochrona "+label+": "+scheme.protection[damageId]+"\n";
                }
            break;
        }

        if(scheme.bonus) {
            string += "\n\nBonusy\n";
            foreach(_bonus in scheme.bonus)
                string += "Bonus do "+Config["PlayerAttributes"][_bonus.type]+": "+_bonus.value+"\n";
        }

        if(scheme.requirement) {
            string += "\n\nWymagania\n";
            foreach(_requirement in scheme.requirement)
                string += Config["PlayerAttributes"][_requirement.type]+": "+_requirement.value+"\n";
        }

        if(scheme.restore) {
            string += "\n\nPrzywraca\n";
            foreach(_restore in scheme.restore)
                string += Config["PlayerAttributes"][_restore.type]+": "+_restore.value+"\n";
        }

        if(scheme.resistance != -1)
            string += "\n\nWytrzyma�o��: "+resistance+"/"+scheme.resistance+"\n";

        if(scheme.buffApply) {
            string += "\n\nDzia�ania w czasie\n"
            local buffType = null;
            local buffAttrId = null;
            local buffSkillId = null;
            local buffTime = null;
            local buffInterval = null;
            local buffValue = null;

            if(typeof(scheme.buffApply) == "array") {
                foreach(_buffApply in scheme.buffApply) {
                    buffType = Config["Buffs"][_buffApply].type;
                    buffTime = Config["Buffs"][_buffApply].time;
                    buffInterval = Config["Buffs"][_buffApply].interval;
                    buffValue = Config["Buffs"][_buffApply].value;
    
                    if(buffType == BuffType.Attribute) {
                        string += "Dodaje " + buffValue + " " + Config["PlayerAttributes"][Config["Buffs"][_buffApply].attributeId] + " na " + buffTime / 60 + " minut \n"
                    } else if(buffType == BuffType.WeightLimit) {
                        string += "Dodaje " + buffValue + " ud�wigu na " + buffTime / 60 + " minut \n"
                    } else if(buffType == BuffType.Skill) {
                        string += "Dodaje " + buffValue + " punkty w profesje " + Config["PlayerSkill"][Config["Buffs"][_buffApply].skillId ] + " na " + buffTime / 60 + " minut \n"
                    } else if(buffType == BuffType.EnduranceRegeneration) {
                        string += "Regeneruje " + buffValue + " wytzyma�o�ci co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    } else if(buffType == BuffType.StaminaRegeneration) {
                        string += "Regeneruje " + buffValue + " staminy/e co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    } else if(buffType == BuffType.HealthReduce) {
                        string += "Redukuje " + buffValue + " punkt�w �ycia co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    } else if(buffType == BuffType.ManaReduce) {
                        string += "Redukuje " + buffValue + " punkt�w many co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    } else if(buffType == BuffType.HealthRegeneration) {
                        string += "Regeneruje " + buffValue + " punkt�w �ycia co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    } else if(buffType == BuffType.ManaRegeneration) {
                        string += "Regeneruje " + buffValue + " punkt�w many co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                    }
                }
            } else {
                buffType = Config["Buffs"][scheme.buffApply].type;
                buffTime = Config["Buffs"][scheme.buffApply].time;
                buffInterval = Config["Buffs"][scheme.buffApply].interval;
                buffValue = Config["Buffs"][scheme.buffApply].value;

                if(buffType == BuffType.Attribute) {
                    string += "Dodaje " + buffValue + " " + Config["PlayerAttributes"][Config["Buffs"][scheme.buffApply].attributeId] + " na " + buffTime / 60 + " minut \n"
                } else if(buffType == BuffType.WeightLimit) {
                    string += "Dodaje " + buffValue + " ud�wigu na " + buffTime / 60 + " minut \n"
                } else if(buffType == BuffType.Skill) {
                    string += "Dodaje " + buffValue + " punkty w profesje " + Config["PlayerSkill"][Config["Buffs"][scheme.buffApply].skillId] + " na " + buffTime / 60 + " minut \n"
                } else if(buffType == BuffType.EnduranceRegeneration) {
                    string += "Regeneruje " + buffValue + " wytzyma�o�ci co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                } else if(buffType == BuffType.StaminaRegeneration) {
                    string += "Regeneruje " + buffValue + " staminy/e co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                } else if(buffType == BuffType.HealthReduce) {
                    string += "Redukuje " + buffValue + " punkt�w �ycia co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                } else if(buffType == BuffType.ManaReduce) {
                    string += "Redukuje " + buffValue + " punkt�w many co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                } else if(buffType == BuffType.HealthRegeneration) {
                    string += "Regeneruje " + buffValue + " punkt�w �ycia co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                } else if(buffType == BuffType.ManaRegeneration) {
                    string += "Regeneruje " + buffValue + " punkt�w many co " + buffInterval + " sekund(e) przez " + buffTime + " sekund \n"
                }
            }
        }

        if(guid == "")
            string += "\n\nWaga:"+scheme.weight+"\nWarto��:"+scheme.value;
        else
            string += "\n\nIdentyfikator:"+guid+"\nWaga:"+scheme.weight+"\nWarto��:"+scheme.value;

        return string;
    }

}