class BarrierController
{
    static function getDistanceToLine(barrierLineX1, barrierLineZ1, barrierLineX2, barrierLineZ2, x, z)
    {
        local px= barrierLineX2 - barrierLineX1;
        local py= barrierLineZ2 - barrierLineZ1;

        local temp = (px * px) + (py * py);
        local u = ((x - barrierLineX1) * px + (z - barrierLineZ1) * py) / (temp);

        if(u > 1) u = 1; else if(u < 0) u = 0;

        local dx = (barrierLineX1 + u * px) - x;
        local dy = (barrierLineZ1 + u * py) - z;

        local dist = sqrt(dx * dx + dy * dy);

        return dist;
    }

    static function checkDistance(x, z) {
        local BARRIER_ZONE = Config["Barrier"];

        local points = BARRIER_ZONE.len();
        local lastPoint = points - 1;

        local distance = 100000;

        for(local i = 0; i < points; i++) {
            if(i != lastPoint) {
                local newDistance = getDistanceToLine(BARRIER_ZONE[i][0], BARRIER_ZONE[i][1], BARRIER_ZONE[i + 1][0], BARRIER_ZONE[i + 1][1], x, z);
                if(distance > newDistance) {
                    distance = newDistance;
                }
            } else {
                 local newDistance = getDistanceToLine(BARRIER_ZONE[i][0], BARRIER_ZONE[i][1], BARRIER_ZONE[0][0], BARRIER_ZONE[0][1], x, z);
                if(distance > newDistance) {
                    distance = newDistance;
                }
            }
        }

        if(distance <= 650) {
            if(distance <= 300) {
                return 2;
            }
            return 1;
        }

        return 0;
    }

    static function onSecond()
    {
        for(local id = 0; id < getMaxSlots(); id++)
        {
            local player = getPlayer(id);

            if(player.loggIn)
            {
                if(!isPlayerDead(id))
                {
                    if (getPlayerWorld(id) != "NEWCOLONY.ZEN")
                        continue

                    local position = getPlayerPosition(id);
                    local warningLevel = checkDistance(position.x, position.z);

                    if(warningLevel)
                    {
                        if(warningLevel == 1 && player.barrierWarningLevel != 1)
                        {
                            addPlayerNotification(id, "Zbli�asz si� do bariery! Zawr�� p�ki mo�esz!");

                            player.barrierWarningLevel = 1;
                        }
                        else if(warningLevel == 2 && player.barrierWarningLevel != 2)
                        {
                            addPlayerNotification(id, "Biada tym, kt�rzy pr�bowali przekroczy� barier�, masz jednak szcz�cie zosta�e� jedynie odrzucony do ty�u oraz oszo�omiony!");

                            for(local observerId = 0; observerId < getMaxSlots(); observerId++)
                            {
                                local observerPosition = getPlayerPosition(observerId);

                                if(getDistance2d(observerPosition.x, observerPosition.z, position.x, position.z) <= 1500)
                                {
                                    PlayerPacket(observerId).oneTimeEffect(observerId, 70)
                                }
                            }

                            player.barrierWarningLevel = 2;

                            setPlayerHealth(id, 0);
                        }
                    }
                    else
                    {
                        if(player.barrierWarningLevel)
                        {
                            addPlayerNotification(id, "Opu�ci�e� stref� zagro�enia! Mo�esz czu� si� bezpieczny!");
                            player.barrierWarningLevel = 0;
                        }
                    }
                }
                else
                {
                    if(player.barrierWarningLevel != 0)
                    {
                        player.barrierWarningLevel = 0;
                    }
                }
            }
        }
    }
}

addEventHandler("onSecond", BarrierController.onSecond.bindenv(BarrierController))