
local commands = {};
local rolesName = {}

function addCommandAdmin(command, callback, description = "", role = PlayerRole.Admin) {
    commands[command] <- {callback = callback, description = description, role = role};
}

addEventHandler("onPlayerCommand", function (playerId, command, params) {
    local player = getAdmin(playerId);
    if(player.role == PlayerRole.User)
        return;

    if(player.active == false)
        return;

    if(command in commands) {
        if(player.role >= commands[command].role) {
            commands[command].callback.call(this, playerId, params)
            // player.saveLog("komenda "+command+" z parametrami "+params);
        }
    }
})

function getAllAdminCommands() {
    return commands;
}

function getRoleName(roleId) {
    return rolesName[roleId]
}

foreach(name, idx in getconsttable().PlayerRole)
    rolesName[idx] <- name