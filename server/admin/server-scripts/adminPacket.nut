
class AdminPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Admin);
    }

    function sendPlayerItems(adminId, items) {
        packet.writeUInt8(AdminPackets.GetItems);
        packet.writeUInt16(items.len());
        foreach(itemObj in items) {
            packet.writeUInt32(itemObj.id);
            packet.writeString(itemObj.instance);
            packet.writeString(itemObj.name);
            packet.writeString(itemObj.description);
            packet.writeString(itemObj.guid);
            packet.writeInt32(itemObj.amount);
            packet.writeInt16(itemObj.resistance);
            packet.writeBool(itemObj.stacked);
            packet.writeBool(itemObj.equipped);
        }
        packet.send(adminId, RELIABLE);
    }

    function setRole(adminId, role) {
        packet.writeUInt8(AdminPackets.Role)
        packet.writeInt16(role);
        packet.send(adminId, RELIABLE);
    }

    function sendPlayerRegister(adminId, playerId, characterName, accountName, characterId) {
        packet.writeUInt8(AdminPackets.PlayerRegister)
        packet.writeInt16(playerId);
        packet.writeString(characterName);
        packet.writeString(accountName);
        packet.writeInt16(characterId);
        packet.send(adminId, RELIABLE);
    }

    function sendPlayerUnRegister(adminId, playerId) {
        packet.writeUInt8(AdminPackets.PlayerUnRegister)
        packet.writeInt16(playerId);
        packet.send(adminId, RELIABLE);
    }

    function sendExitGame(playerId) {
        packet.writeUInt8(AdminPackets.ExitGame)
        packet.writeInt16(playerId);
        packet.send(playerId, RELIABLE);
    }

    function sendPlayer(adminId, playerId) {
        local playerObject = getPlayer(playerId);

        if(playerObject.rId == -1)
            return;

        local position = getPlayerPosition(playerId);

        packet.writeUInt8(AdminPackets.GetPlayers)
        packet.writeInt16(playerId);
        packet.writeInt16(playerObject.rId);
        packet.writeString(playerObject.name);
        packet.writeString(getAccount(playerId).username);
        packet.writeBool(playerObject.penaltyModule.active);
        packet.writeFloat(position.x);
        packet.writeFloat(position.y);
        packet.writeFloat(position.z);
        packet.writeInt16(getPlayerAngle(playerId));
        packet.writeInt16(playerObject.fatness);
        packet.writeInt16(playerObject.speech);
        packet.writeInt16(playerObject.walking);
        packet.writeFloat(playerObject.stamina);
        packet.writeInt16(playerObject.respawnLeft);
        packet.writeInt16(playerObject.str);
        packet.writeInt16(playerObject.dex);
        packet.writeInt16(playerObject.inteligence);
        packet.writeInt32(playerObject.hp);
        packet.writeInt32(playerObject.hpMax);
        packet.writeInt32(playerObject.mana);
        packet.writeInt32(playerObject.maxMana);
        packet.writeInt16(playerObject.magicLvl);
        packet.writeInt16(playerObject.learnPoints);
        packet.writeInt16(playerObject.skills[0]);
        packet.writeInt16(playerObject.skills[1]);
        packet.writeInt16(playerObject.skills[2]);
        packet.writeInt16(playerObject.skills[3]);
        packet.writeInt16(playerObject.skills[4]);
        packet.writeInt16(playerObject.skills[5]);
        packet.writeInt16(playerObject.skills[6]);
        packet.writeInt16(playerObject.skills[7]);
        packet.writeInt16(playerObject.skills[8]);
        packet.writeInt16(playerObject.skills[9]);
        packet.writeInt16(playerObject.skills[10]);
        packet.writeInt16(playerObject.weapon[0]);
        packet.writeInt16(playerObject.weapon[1]);
        packet.writeInt16(playerObject.weapon[2]);
        packet.writeInt16(playerObject.weapon[3]);

        packet.writeInt16(playerObject.inventory.getItems().len());

        foreach(v,k in playerObject.inventory.getItems()) {
            packet.writeString(k.instance);
            packet.writeInt16(k.amount);
        }

        packet.writeInt16(playerObject.effectModule.effects.len());

        foreach(v,k in playerObject.effectModule.effects)
        {
            local obj = System.Effect.effects[v];
            packet.writeInt16(obj.effectId);
            packet.writeInt16(obj.refreshTime);
        }

        packet.writeInt16(playerObject.logModule.cache.len());

        foreach(v,k in playerObject.logModule.cache)
        {
            packet.writeString(k.content);
            packet.writeString(k.dateObject);
        }

        packet.send(adminId, RELIABLE);
    }

    function sendBot(adminId, botObject) {
        packet.writeUInt8(AdminPackets.GetBots)
        packet.writeInt16(botObject.id);
        packet.writeInt16(botObject.effects.len());

        foreach(v,k in botObject.effects)
        {
            local obj = System.Effect.effects[v];
            packet.writeInt16(obj.effectId);
            packet.writeInt16(obj.refreshTime);
        }

        packet.writeBool(botObject.isTemporary);
        packet.writeBool(botObject.isSaved);
        packet.writeString(botObject.group);
        packet.writeString(botObject.name);
        packet.writeString(botObject.instance);
        packet.writeFloat(botObject.position.x);
        packet.writeFloat(botObject.position.y);
        packet.writeFloat(botObject.position.z);
        packet.writeInt16(botObject.angle);
        packet.writeInt16(botObject.str);
        packet.writeInt16(botObject.dex);
        packet.writeInt32(botObject.hp);
        packet.writeInt32(botObject.hpMax);
        packet.writeFloat(botObject.scale.x);
        packet.writeFloat(botObject.scale.y);
        packet.writeFloat(botObject.scale.z);
        packet.writeString(botObject.animation);
        packet.writeInt16(botObject.weapon[0]);
        packet.writeInt16(botObject.weapon[1]);
        packet.writeInt16(botObject.weapon[2]);
        packet.writeInt16(botObject.weapon[3]);
        packet.writeInt16(botObject.respawnTime);
        packet.writeInt16(botObject.melee);
        packet.writeInt16(botObject.armor);
        packet.writeInt16(botObject.ranged);
        packet.writeInt16(botObject.helmet);
        packet.writeInt16(botObject.shield);
        packet.writeInt16(botObject.magic);
        packet.writeString(botObject.visual.bodyModel);
        packet.writeInt16(botObject.visual.bodyTxt);
        packet.writeString(botObject.visual.headModel);
        packet.writeInt16(botObject.visual.headTxt);
        packet.writeString(botObject.world)
        packet.send(adminId, RELIABLE);
    }

    function sendAsset(adminId, asset) {
        packet.writeUInt8(AdminPackets.GetAssets)
        packet.writeInt16(asset.id);
        packet.writeString(asset.name);
        packet.writeString(asset.instance);
        packet.writeInt16(asset.str);
        packet.writeInt16(asset.dex);
        packet.writeInt32(asset.maxHp);
        packet.writeInt32(asset.maxMana);
        packet.writeFloat(asset.scale.x);
        packet.writeFloat(asset.scale.y);
        packet.writeFloat(asset.scale.z);
        packet.writeInt16(asset.oneh);
        packet.writeInt16(asset.twoh);
        packet.writeInt16(asset.bow);
        packet.writeInt16(asset.cbow);
        packet.writeInt16(asset.fatness);
        packet.writeInt16(asset.inteligence);
        packet.writeInt16(asset.magicLvl);
        packet.writeString(asset.bodyModel);
        packet.writeInt16(asset.bodyTxt);
        packet.writeString(asset.headModel);
        packet.writeInt16(asset.headTxt);
        packet.writeInt16(asset.effects.len());

        foreach(v,k in asset.effects) {
            packet.writeInt16(k.effectId);
            packet.writeInt16(k.refreshTime);
        }

        packet.writeInt16(asset.items.len());

        foreach(v,k in asset.items) {
            packet.writeString(k.instance);
            packet.writeInt16(k.amount);
        }

        packet.send(adminId, RELIABLE);
    }

    function sendAsset(adminId, asset) {
        packet.writeUInt8(AdminPackets.GetAssets)
        packet.writeInt16(asset.id);
        packet.writeString(asset.name);
        packet.writeString(asset.instance);
        packet.writeInt16(asset.str);
        packet.writeInt16(asset.dex);
        packet.writeInt32(asset.maxHp);
        packet.writeInt32(asset.maxMana);
        packet.writeFloat(asset.scale.x);
        packet.writeFloat(asset.scale.y);
        packet.writeFloat(asset.scale.z);
        packet.writeInt16(asset.oneh);
        packet.writeInt16(asset.twoh);
        packet.writeInt16(asset.bow);
        packet.writeInt16(asset.cbow);
        packet.writeInt16(asset.fatness);
        packet.writeInt16(asset.inteligence);
        packet.writeInt16(asset.magicLvl);
        packet.writeString(asset.bodyModel);
        packet.writeInt16(asset.bodyTxt);
        packet.writeString(asset.headModel);
        packet.writeInt16(asset.headTxt);
        packet.writeInt16(asset.effects.len());

        foreach(v,k in asset.effects) {
            packet.writeInt16(k.effectId);
            packet.writeInt16(k.refreshTime);
        }

        packet.writeInt16(asset.items.len());

        foreach(v,k in asset.items) {
            packet.writeString(k.instance);
            packet.writeInt16(k.amount);
        }

        packet.send(adminId, RELIABLE);
    }

    function sendQuestions(adminId, questions) {
        packet.writeUInt8(AdminPackets.Question)
        packet.writeInt16(questions.len());
        foreach(question in questions) {
            packet.writeInt16(question.id);
            packet.writeString(question.question);
            local lenghtAnswers = question.answers.len();
            packet.writeInt16(lenghtAnswers);
            for(local i = 0; i < lenghtAnswers; i++) {
                packet.writeString(question.answers[i][0]);
            }
        }
        packet.send(adminId, RELIABLE);
    }

    function sendAdminConfirmation(adminId) {
        packet.writeUInt8(AdminPackets.ConfirmRole);
        packet.writeBool(true);
        packet.send(adminId, RELIABLE);
    }
}
