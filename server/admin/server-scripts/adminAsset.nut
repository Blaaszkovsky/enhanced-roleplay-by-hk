
class AdminAsset
{
    static AssetTable = "adminasset";

    constructor(_id) {
        id = _id
        instance = ""
        name = ""
        str = -1
        dex = -1
        maxHp = -1
        maxMana = -1
        oneh = -1
        twoh = -1
        bow = -1
        cbow = -1
        fatness = -1
        inteligence = -1
        magicLvl = -1
        bodyModel = ""
        bodyTxt = -1
        headModel = ""
        headTxt = -1
        scale = {x = 1.0, y = 1.0, z = 1.0}
        items = []
        effects = []
        createdAt = ""
        createdBy = ""
    }

    function transfer(playerId) {
        setPlayerInstance(playerId, instance);

        foreach(v in effects)
            getPlayer(playerId).effectModule.addEffect(v.effectId, v.refreshTime);

        setPlayerInteligence(playerId, inteligence);
        setPlayerStrength(playerId, str);
        setPlayerDexterity(playerId, dex);
        setPlayerMaxHealth(playerId, maxHp);
        setPlayerHealth(playerId, maxHp);
        setPlayerMana(playerId, maxMana);
        setPlayerMaxMana(playerId, maxMana);
        setPlayerMagicLevel(playerId, magicLvl);
        setPlayerScale(playerId, scale.x, scale.y, scale.z);
        setPlayerVisual(playerId, bodyModel, bodyTxt, headModel, headTxt);
        setPlayerSkillWeapon(playerId, 0, oneh);
        setPlayerSkillWeapon(playerId, 1, twoh);
        setPlayerSkillWeapon(playerId, 2, bow);
        setPlayerSkillWeapon(playerId, 3, cbow);
        setPlayerFatness(playerId, fatness);

        foreach(v in items) {
            giveItem(playerId, v.instance, v.amount);
            if(v.equipped == 1)
                equipItem(playerId, v.instance);
        }
    }

    id = -1
    instance = ""
    name = ""
    str = -1
    dex = -1
    maxHp = -1
    maxMana = -1
    oneh = -1
    twoh = -1
    bow = -1
    cbow = -1
    fatness = -1
    inteligence = -1
    magicLvl = -1
    bodyModel = ""
    bodyTxt = -1
    headModel = ""
    headTxt = -1
    scale = null
    items = null
    effects = null
    createdAt = ""
    createdBy = ""
}