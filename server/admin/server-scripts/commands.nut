
addCommandAdmin("ahelp", function (playerId, params) {
    local player = getAdmin(playerId);
    sendMessageToPlayer(playerId, 255, 255,255, ChatType.ADMIN+"POMOC (ranga numer: " + player.role + ")");

    foreach(command, value in getAllAdminCommands())
        if(player.role >= value.role)
            sendMessageToPlayer(playerId, 255, 255,255, ChatType.ADMIN+command+": "+value.description);

    sendMessageToPlayer(playerId, 255, 255,255, ChatType.ADMIN+"---------------------------");
}, "Pomoc dotycz�ca istniej�cych komend.", PlayerRole.Narrator);


addCommandAdmin("giveskill", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /giveskill id <typ> <wartosc>");

        local i = 0;
        local text = "";
        foreach(index, value in Config["PlayerSkill"]) {
            text += "("+index+"-"+value+"), ";
            i = i + 1;
            if(i == 3) {
                sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+""+text);
                i = 0;
                text = "";
            }
        }

        if(i > 0)
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+" "+text);
        return;
    }

    local id = args[0];
    local typ = args[1];
    local value = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if(!(typ in Config["PlayerSkill"]))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje skill o tym Id.");
        return;
    }

    if(value < 0 || value > 9)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Warto�� w przedziale 0-9");
        return;
    }

    setPlayerSkill(id, typ, value);
    getAdmin(playerId).saveLog("Uzyta komenda giveskill " + id + " " + typ + " " + value, id);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"zmieni�e� "+getPlayerName(id) + " " + Config["PlayerSkill"][typ]+" na " + value);
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"zmieni� "+getPlayerName(playerId) + " ci " + Config["PlayerSkill"][typ]+" na " + value);
}, "Komenda ustawia skille gracza. Sk�adnia /giveskill <id> <typ> <wartosc 0 - 9>", PlayerRole.GameMasterPlus);

addCommandAdmin("checkskill", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /checkskill id");
        return;
    }

    local id = args[0];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+getPlayerName(id));

    local i = 0;
    local text = "";
    foreach(index, value in Config["PlayerSkill"]) {
        text += ""+value+"-"+getPlayerSkill(id, index)+", ";
        i = i + 1;
        if(i == 2) {
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+""+text);
            i = 0;
            text = "";
        }
    }

    if(i > 0)
        sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+" "+text);

    getAdmin(playerId).saveLog("Uzyta komenda checkskill " + id, id);

}, "Komenda sprawdza skille gracza. Sk�adnia /checkskill <id>", PlayerRole.SupportPlus);


addCommandAdmin("ignoring", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /ignoring id");
        return;
    }

    local id = args[0];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    getPlayer(id).ignoring = !getPlayer(id).ignoring
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+" "+ (getPlayer(id).ignoring ? ("W�aczy�e� ignorowanie graczowi: " + getPlayerName(id)) : ("Wy�aczy�e� ignorowanie graczowi: " + getPlayerName(id))));

}, "Komenda ustawia ignorowanie przez mob�w gracza. Sk�adnia /ignoring <id>", PlayerRole.SupportPlus);


addCommandAdmin("givestat", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /givestat id <typ> <wartosc>");

        local i = 0;
        local text = "";
        foreach(index, value in Config["PlayerAttributes"]) {
            text += "("+index+"-"+value+"), ";
            i = i + 1;
            if(i == 3) {
                sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+""+text);
                i = 0;
                text = "";
            }
        }

        if(i > 0)
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+" "+text);
        return;
    }

    local id = args[0];
    local typ = args[1];
    local value = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    switch(typ)
    {
        case PlayerAttributes.Str: setPlayerStrength(id, value); break;
        case PlayerAttributes.Dex: setPlayerDexterity(id, value); break;
        case PlayerAttributes.Int: setPlayerInteligence(id, value); break;
        case PlayerAttributes.Hp: setPlayerMaxHealth(id, value); setPlayerHealth(id, value); break;
        case PlayerAttributes.Mana: setPlayerMana(id, value); setPlayerMaxMana(id, value); break;
        case PlayerAttributes.OneH: setPlayerSkillWeapon(id, 0, value); break;
        case PlayerAttributes.TwoH: setPlayerSkillWeapon(id, 1, value); break;
        case PlayerAttributes.Bow: setPlayerSkillWeapon(id, 2, value); break;
        case PlayerAttributes.Cbow: setPlayerSkillWeapon(id, 3, value); break;
        case PlayerAttributes.MagicLvl: setPlayerMagicLevel(id, value); break;
        case PlayerAttributes.Stamina: setPlayerStamina(id, value/1.0); break;
        case PlayerAttributes.LearnPoints: setPlayerLearnPoints(id, value); break;
        case PlayerAttributes.Hunger: setPlayerHunger(id, value/1.0); break;
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"zmieni�e� "+getPlayerName(id) + " " + Config["PlayerAttributes"][typ]+" na " + value);
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"zmieni� "+getPlayerName(playerId) + " ci " + Config["PlayerAttributes"][typ]+" na " + value);
    getAdmin(playerId).saveLog("Uzyta komenda givestat " + id + " " + typ + " " + value, id);

}, "Komenda ustawia staty gracza. Sk�adnia /givestat <id> <typ> <wartosc>", PlayerRole.GameMasterPlus);

addCommandAdmin("addstat", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /addstat id <typ> <wartosc>");

        local i = 0;
        local text = "";
        foreach(index, value in Config["PlayerAttributes"]) {
            text += "("+index+"-"+value+"), ";
            i = i + 1;
            if(i == 3) {
                sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+""+text);
                i = 0;
                text = "";
            }
        }

        if(i > 0)
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+" "+text);
        return;
    }

    local id = args[0];
    local typ = args[1];
    local value = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    switch(typ)
    {
        case PlayerAttributes.Str: setPlayerStrength(id, getPlayerStrength(id) + value); break;
        case PlayerAttributes.Dex: setPlayerDexterity(id, getPlayerDexterity(id) + value); break;
        case PlayerAttributes.Int: setPlayerInteligence(id, getPlayerInteligence(id) + value); break;
        case PlayerAttributes.Hp: setPlayerMaxHealth(id, getPlayerMaxHealth(id) + value); setPlayerHealth(id, getPlayerHealth(id) + value); break;
        case PlayerAttributes.Mana: setPlayerMana(id, getPlayerMana(id) + value); setPlayerMaxMana(id, getPlayerMaxMana(id) + value); break;
        case PlayerAttributes.OneH: setPlayerSkillWeapon(id, 0, getPlayerSkillWeapon(id, 0) + value); break;
        case PlayerAttributes.TwoH: setPlayerSkillWeapon(id, 1, getPlayerSkillWeapon(id, 1) + value); break;
        case PlayerAttributes.Bow: setPlayerSkillWeapon(id, 2, getPlayerSkillWeapon(id, 2) + value); break;
        case PlayerAttributes.Cbow: setPlayerSkillWeapon(id, 3, getPlayerSkillWeapon(id, 3) + value); break;
        case PlayerAttributes.MagicLvl: setPlayerMagicLevel(id, getPlayerMagicLevel(id) + value); break;
        case PlayerAttributes.Stamina: setPlayerStamina(id, getPlayerStamina(id) + value/1.0); break;
        case PlayerAttributes.LearnPoints: setPlayerLearnPoints(id, getPlayerLearnPoints(id) + value); break;
        case PlayerAttributes.Hunger: setPlayerHunger(id, getPlayerHunger(id) + value/1.0); break;
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Doda�e� "+getPlayerName(id) + " " + Config["PlayerAttributes"][typ]+" + " + value);
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"doda� "+getPlayerName(playerId) + " ci " + Config["PlayerAttributes"][typ]+" + " + value);
    getAdmin(playerId).saveLog("Uzyta komenda addstat " + id + " " + typ + " " + value, id);

}, "Komenda ustawia staty gracza. Sk�adnia /addstat <id> <typ> <wartosc>", PlayerRole.GameMasterPlus);

addCommandAdmin("addskill", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /addskill id <typ> <wartosc>");

        local i = 0;
        local text = "";
        foreach(index, value in Config["PlayerSkill"]) {
            text += "("+index+"-"+value+"), ";
            i = i + 1;
            if(i == 3) {
                sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+""+text);
                i = 0;
                text = "";
            }
        }

        if(i > 0)
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+" "+text);
        return;
    }

    local id = args[0];
    local typ = args[1];
    local value = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if(!(typ in Config["PlayerSkill"]))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje skill o tym Id.");
        return;
    }

    if((getPlayerSkill(id, typ) + value) < 0 || (getPlayerSkill(id, typ) + value) > 9)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Warto�� w przedziale 0-9");
        return;
    }

    setPlayerSkill(id, typ, getPlayerSkill(id, typ) + value);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"doda�e� "+getPlayerName(id) + " " + Config["PlayerSkill"][typ]+" + " + value);
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"doda� "+getPlayerName(playerId) + " ci " + Config["PlayerSkill"][typ]+" + " + value);
    getAdmin(playerId).saveLog("Uzyta komenda addskill " + id + " " + typ + " " + value, id);

}, "Komenda ustawia skill gracza. Sk�adnia /addskill <id> <typ> <wartosc>", PlayerRole.GameMasterPlus);

addCommandAdmin("checkstat", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /checkstat id");
        return;
    }

    local id = args[0];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+getPlayerName(id) + " - konto "+getAccount(id).username);
    sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+"si�a "+getPlayerStrength(id) + ", zr�czno�� "+getPlayerDexterity(id) + ", inteligencja "+getPlayerInteligence(id));
    sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+"hp "+getPlayerHealth(id) + "/"+getPlayerMaxHealth(id) + ", mana "+getPlayerMana(id) +"/"+getPlayerMaxMana(id));
    sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+"1h "+getPlayerSkillWeapon(id, 0) + ", 2h "+getPlayerSkillWeapon(id, 1) + ", �uki "+getPlayerSkillWeapon(id, 2) + ", kusze "+getPlayerSkillWeapon(id, 3));
    sendMessageToPlayer(playerId, 255, 255, 255, ChatType.ADMIN+"kr�g "+getPlayerMagicLevel(id) + ", punkty nauki "+getPlayerLearnPoints(id) + ", stamina "+getPlayerStamina(id));
    getAdmin(playerId).saveLog("Uzyta komenda checkstat " + id, id);
}, "Komenda sprawdza staty gracza. Sk�adnia /checkstat <id>", PlayerRole.SupportPlus);

addCommandAdmin("ban", function (playerId, params) {
    local args = sscanf("dds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /ban id minuty powod");
        return;
    }

    local id = args[0];
    local minutes = args[1];
    local reason = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    ban(id, minutes, reason);

    sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� bana przez %s", getPlayerName(id), getPlayerName(playerId)));
    sendMessageToAll(255, 80, 0, ChatType.ALL + format("Pow�d: %s", reason));
    getAdmin(playerId).saveLog("Uzyta komenda ban " + id + " " + minutes + " " + reason, id);
}, "Komenda banuj�ca graczy. Sk�adnia /ban <id> <minuty> <powod>", PlayerRole.SupportPlus);


addCommandAdmin("lasts", function(playerId, params) {

    local lastUsers = Query().select().from("disconnects").orderBy("id DESC LIMIT 10").all();
    local i = 1;

    foreach(lastUser in lastUsers) {
        if(i < 10) {
            sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN + " " + i + ". " + lastUser.username + " (gracz: " + lastUser.playerName + ", accountId: " + lastUser.accountId + ") - czas: " + lastUser.disconnectTime);
            i++;
        }
    }
}, "Zwraca ostatnie 10 wyj�� z serwera", PlayerRole.Support);

addCommandAdmin("ban_offline", function (playerId, params) {
    local args = sscanf("dds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /ban_offline id minuty powod");
        return;
    }

    local id = args[0];
    local minutes = args[1];
    local reason = args[2];

    local accountObj = Query().select().from(Account.dbTable).where(["id = "+id]).one();
    if (accountObj == null)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local timeout = 0;
    if(minutes != 0)
        timeout = time() + ( minutes * 60);

    AdminController.bans[id] <- {
        name = accountObj.username,
        timeout = timeout,
        reason = reason + " (" + minutes + " min. )",
        mac = "fc-aa-14-ff-fd-f9",
        serial = accountObj.serial,
        playerUID = accountObj.playerUID
    };

    updateBanSystem();

    sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� bana przez %s", accountObj.username, getPlayerName(playerId)));
    sendMessageToAll(255, 80, 0, ChatType.ALL + format("Pow�d: %s", reason));
    getAdmin(playerId).saveLog("Uzyta komenda ban_offline " + id + " " + minutes + " " + reason, id);

}, "Komenda banuj�ca graczy. Sk�adnia /ban_offline <id> <minuty> <powod>", PlayerRole.SupportPlus);

addCommandAdmin("blockdm", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /blockdm id(-1 dla wszystkich) minuty");
        return;
    }

    local id = args[0];
    local minutes = args[1];

    if(id == -1) {
        sendMessageToAll(255, 80, 0, ChatType.ALL + format("Blokada na DM na: %d minut (%s)", minutes, getPlayerName(playerId)));
        System.BlockDeathMatch.set(minutes);
    }
    else {
        if (!isPlayerConnected(id)) {
            sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
            return;
        }

        System.BlockDeathMatch.players[id] = minutes;
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+getPlayerName(id) + " otrzyma� blokad� na DM. Na czas "+minutes+" minut.");
        sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+""+getPlayerName(playerId) + " da� ci blokad� na DM. Na czas "+minutes+" minut.");
    }
    getAdmin(playerId).saveLog("Uzyta komenda blockdm " + id + " " + minutes, id);

}, "Komenda blokuj�ca dm dla graczy. Sk�adnia /blockdm <id(-1 dla wszystkich)> <minuty>", PlayerRole.SupportPlus);

addCommandAdmin("mute", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /mute id minuty typ( 0 - ic, 1 - ooc, 2 - oba )");
        return;
    }

    local id = args[0];
    local minutes = args[1];
    local type = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    mute(id, minutes, type);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+getPlayerName(id) + " otrzyma� mute. Na czas "+minutes+" minut.");
    sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� mute od %s na %d minut/y.", getPlayerName(id), getPlayerName(playerId), minutes));
    getAdmin(playerId).saveLog("Uzyta komenda mute " + id + " " + minutes + " " + type, id);
}, "Komenda mutuj�ca graczy. Sk�adnia /mute <id> <minuty> <typ( 0 - ic, 1 - ooc, 2 - oba )>", PlayerRole.SupportPlus);

addCommandAdmin("unmute", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /unmute id");
        return;
    }

    local id = args[0];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    unmute(id);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+getPlayerName(id) + " otrzyma� unmute.");
    sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� unmute od %s.", getPlayerName(id), getPlayerName(playerId)));
    getAdmin(playerId).saveLog("Uzyta komenda unmute " + id, id);

}, "Komenda odmutuje danego gracza. Sk�adnia /unmute <id>", PlayerRole.GameMaster);

addCommandAdmin("kill", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /kill id");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))  {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    setPlayerHealth(id, 0);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Zabi�e� gracza nr. "+id);
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"Zosta�e� zabity przez gracza nr. "+playerId);
    getAdmin(playerId).saveLog("Uzyta komenda kill " + id, id);

}, "Komenda zabijaj�ca graczy. Sk�adnia /kill <id>", PlayerRole.Helper);

addCommandAdmin("goto", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /goto <id>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))  {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local position = getPlayerPosition(id);
    setPlayerPosition(playerId, position.x, position.y, position.z);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Przenios�e� si� do gracza nr."+id);
    getAdmin(playerId).saveLog("Uzyta komenda goto " + id, id);

}, "Komenda teleportuj�ca do gracza. Sk�adnia /goto <id>", PlayerRole.Helper);

addCommandAdmin("kick", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /kick id powod");
        return;
    }

    local id = args[0];
    local reason = args[1];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    kick(id, reason);

    sendMessageToAll(255, 80, 0, ChatType.ALL + format("%s dosta� kicka przez %s", getPlayerName(id), getPlayerName(playerId)));
    sendMessageToAll(255, 80, 0, ChatType.ALL + format("Pow�d: %s", reason));
    getAdmin(playerId).saveLog("Uzyta komenda kick " + id + " " + reason, id);

}, "Komenda kickuj�ca graczy. Sk�adnia /kick <id> <powod>", PlayerRole.Support);

addCommandAdmin("kickall", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /kickall <powod>");
        return;
    }

    for(local i = 0; i < getMaxSlots(); i++)
    {
        if (isPlayerConnected(i) && i != playerId)
        {
            kick(i, args[0]);
        }
    }
    getAdmin(playerId).saveLog("Uzyta komenda kickall", -1);
}, "Komenda kickuj�ca wszystkich graczy. Sk�adnia /kickall <powod>", PlayerRole.Mod);

addCommandAdmin("exit_server", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /exit_server <powod>");
        return;
    }

    for(local i = 0; i < getMaxSlots(); i++)
    {
        if (isPlayerConnected(i))
        {
            kick(i, args[0]);
        }
    } setTimer(exit, 3000, 1);
    getAdmin(playerId).saveLog("Uzyta komenda exit_server ", -1);
}, "Komenda kickuj�ca wszystkich graczy oraz wy��czaj�ca serwer. Sk�adnia /exit_server <powod>", PlayerRole.Admin);

addCommandAdmin("ck", function(playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /ck <id_gracza> <powod>");
        return;
    }

    local ckId = args[0];
    local reason = args[1];

    if (!isPlayerConnected(ckId))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local player = getPlayer(ckId);

    if(player.ck())
    {
        sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s otrzyma� CK od %s", getPlayerName(ckId), getPlayerName(playerId)));
        kick(ckId, reason);
    }
    getAdmin(playerId).saveLog("Uzyta komenda ck " + ckId + " " + reason, ckId);
}, "Komenda ck'uj�ca posta� gracza. Sk�adnia /ck <id_gracza> <powod>", PlayerRole.GameMaster);

addCommandAdmin("giveitem", function (playerId, params) {
    local args = sscanf("dsd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /giveitem id instance amount");
        return;
    }

    local id = args[0];
    local instance = args[1].toupper();
    local amount = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if (getItemScheme(instance) == null)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje ten item w rejestrze.");
        return;
    }

    giveItem(id, instance, amount);

    sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s dosta� %s w ilo�ci %d od %s", getPlayerName(id), instance, amount, getPlayerName(playerId)));
    getAdmin(playerId).saveLog("Uzyta komenda giveitem " + id + " - " + instance + " w ilosci " + amount, id);

}, "Komenda daj�ca item graczowi. Sk�adnia /giveitem <id> <instance> <amount>", PlayerRole.GameMasterPlus);

addCommandAdmin("create_key", function (playerId, params) {
    local args = sscanf("dss", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /create_key id key_name guid");
        return;
    }

    local id = args[0];
    local name = args[1];
    local guid = args[2];

    if(name == null) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Podaj nazw� klucza");
        return;
    }

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if (guid == null)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie poda�e� GUID klucza.");
        return;
    }

    local pObj = getPlayer(args[0]);

    local _item = createItem("ITKE_SDRP_DOOR_01", 1);
    _item.name = String.replace(name, "_", " ");
    _item.guid = guid;
    _item.save();
    pObj.inventory.addItem(_item.id);

    sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s dosta� klucz guid: %s od %s", getPlayerName(id), guid, getPlayerName(playerId)));
    getAdmin(playerId).saveLog("Uzyta komenda create_key " + id + " - GUID: " + guid, id);

}, "Komenda daj�ca klucz graczowi. Sk�adnia /create_key id key_name guid amount", PlayerRole.SupportPlus);

addCommandAdmin("takeitem", function (playerId, params) {
    local args = sscanf("dsd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /takeitem id instance amount");
        return;
    }

    local id = args[0];
    local instance = args[1].toupper();
    local amount = args[2];

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if (getItemScheme(instance) == null)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje ten item w rejestrze.");
        return;
    }

    removeItem(id, instance, amount);

    sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s odebra� %s w ilo�ci %d od %s", getPlayerName(id), instance, amount, getPlayerName(playerId)));
    getAdmin(playerId).saveLog("Uzyta komenda takeitem " + id + " - " + instance + " w ilosci " + amount, id);

}, "Komenda zabieraj�ca item graczowi. Sk�adnia /takeitem <id> <instance> <amount>", PlayerRole.GameMasterPlus);

addCommandAdmin("time", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /time godziny minuty");
        return;
    }

    local hours = args[0];
    local minutes = args[1];

    setTime(hours,minutes);

    sendMessageToAll(255, 80, 0, ChatType.ADMIN+"Zmieni� czas "+getPlayerName(playerId) + " na "+hours + ":"+minutes);
}, "Komenda zmieniaj�ca czas gry. Sk�adnia /time godziny minuty", PlayerRole.Admin);

addCommandAdmin("vanish", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /vanish <id> <1 - 0>");
        return;
    }

    local value = args[1], invisibledPlayerId = args[0];
    local invisible = value == 1 ? true : false
    setPlayerInvisible(invisibledPlayerId, invisible)
    PlayerPacket(invisibledPlayerId).setInvisible(invisible);
    sendMessageToPlayer(invisibledPlayerId, 255, 80, 0, ChatType.OOC+"Zmieni� ci niewidzialno�� "+getPlayerName(playerId));
    sendMessageToPlayer(playerId, 255, 80, 0, ChatType.ADMIN+"Zmieni�e� niewidzialno�� "+getPlayerName(invisibledPlayerId));
    getAdmin(playerId).saveLog("Uzyta komenda vanish " + invisibledPlayerId + " - " + value, invisibledPlayerId);

}, "Komenda zmieniaj�ca widzialno�� postaci.", PlayerRole.Helper);

addCommandAdmin("scale", function (playerId, params) {
    local args = sscanf("dfff", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /scale playerid x y z");
        return;
    }

    local scaleId = args[0];
    if (!isPlayerConnected(scaleId))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local scale = {x = args[1], y = args[2], z = args[3]};

    local player = getPlayer(args[0]);
    player.scaleX = args[1];
    player.scaleY = args[2];
    player.scaleZ = args[3];

    setPlayerScale(scaleId,scale.x,scale.y,scale.z);

    sendMessageToPlayer(scaleId, 255, 80, 0, ChatType.OOC+"Zmieni� ci skalowanie "+getPlayerName(playerId));
    sendMessageToPlayer(playerId, 255, 80, 0, ChatType.ADMIN+"Zmieni�e� skalowanie "+getPlayerName(scaleId));
    getAdmin(playerId).saveLog("Uzyta komenda scale " + args[1] + " " + args[2] + " " + args[3], args[0]);
}, "Komenda zmieniaj�ca scale postaci. Sk�adnia /scale playerid x y z", PlayerRole.Helper);

addCommandAdmin("instance", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /instance id instance");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local player = getPlayer(id);

    local instance = args[1].toupper();

    player.instance = instance;

    setPlayerInstance(id,instance);

    if(instance == "PC_HERO")
    {
        foreach(v in player.inventory)
        {
            _giveItem(id, Items.id(v.instance), v.amount);
            if(v.equipped == 1)
                _equipItem(id, Items.id(v.instance));
        }

        _setPlayerStrength(id, player.str);
        _setPlayerDexterity(id, player.dex);
        _setPlayerMaxHealth(id, player.hpMax);
        setPlayerHealth(id, player.hpMax);
        setPlayerMana(id, player.maxMana);
        _setPlayerMaxMana(id, player.maxMana);
        _setPlayerMagicLevel(id, player.magicLvl);
        _setPlayerVisual(id, player.visual.bodyModel, player.visual.bodyTxt, player.visual.headModel, player.visual.headTxt);
        PlayerPacket(id).setExperience(player.exp);
        PlayerPacket(id).setNextLevelExperience(player.nextLevelExp);
        PlayerPacket(id).setLevel(player.level);
        PlayerPacket(id).setLearnPoints(player.learnPoints);
        _setPlayerSkillWeapon(id, 0, player.weapon[0]);
        _setPlayerSkillWeapon(id, 1, player.weapon[1]);
        _setPlayerSkillWeapon(id, 2, player.weapon[2]);
        _setPlayerSkillWeapon(id, 3, player.weapon[3]);
    }

    sendMessageToPlayer(id, 255, 80, 0, ChatType.OOC+"Zmieni� ci instance "+getPlayerName(playerId));
    sendMessageToPlayer(playerId, 255, 80, 0, ChatType.ADMIN+"Zmieni�e� instance "+getPlayerName(id));
    getAdmin(playerId).saveLog("Uzyta komenda instance " + instance, id);

}, "Komenda zmieniaj�ca instancje postaci. Sk�adnia /instance id instance", PlayerRole.Helper);

addCommandAdmin("color", function (playerId, params) {
    local args = sscanf("dddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /color r g b");
        return;
    }

    local colorId = args[0];
    if (!isPlayerConnected(colorId))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local color = {r = args[1], g = args[2], b = args[3]};

    setPlayerColor(colorId,color.r,color.g,color.b);

    sendMessageToPlayer(colorId, color.r,color.g,color.b, ChatType.OOC+"Zmieni� ci kolor nicku "+getPlayerName(playerId));
    sendMessageToPlayer(playerId, color.r,color.g,color.b, ChatType.ADMIN+"Zmieni�e� kolor nicku "+getPlayerName(colorId));
    getAdmin(playerId).saveLog("Uzyta komenda color", colorId);

}, "Komenda zmieniaj�ca kolor nicku postaci. Sk�adnia /color playerid r g b", PlayerRole.Mod);

addCommandAdmin("portal", function (playerId, params) {
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/portal_list - lista aktywnych portali.");
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/portal_remove - usu� portal.");
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/portal_add - dodaj portal.");
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/portal_change - zmie� aktywny portal.");
}, "Komenda pomocy odno�nie teleportacji. Sk�adnia /portal", PlayerRole.SupportPlus);

addCommandAdmin("portal_add", function (playerId, params) {
    local args = sscanf("fffd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /portal_add x, y, z time");
        return;
    }

    local x = args[0];
    local y = args[1];
    local z = args[2];
    local time = args[3];

    System.Portal.add(getPlayerPosition(playerId), {x = x, y = y, z = z}, time);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Doda�e� teleport.");
}, "Komenda tworzaca portal. Sk�adnia /portal_add x, y, z time", PlayerRole.SupportPlus);

addCommandAdmin("portal_remove", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /portal_remove id");
        return;
    }

    local id = args[0];
    System.Portal.remove(id);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usun��e� teleport.");
}, "Komenda usuwaj�ca portal. Sk�adnia /portal_remove id", PlayerRole.SupportPlus);

addCommandAdmin("portal_change", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /portal_change <id> <time>");
        return;
    }

    local id = args[0];
    if(id in System.Portal.portals)
        System.Portal.portals[id].seconds = args[1];

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Zmieni�e� teleport.");
}, "Komenda zmieniaj�ca czas portalu. Sk�adnia /portal_change id <time>", PlayerRole.SupportPlus);

addCommandAdmin("portal_list", function (playerId, params) {

    foreach(portalId, portal in System.Portal.portals)
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Portal Id:"+portalId+" - "+portal.seconds);

}, "Komenda pokazuj�ca list� portali. Sk�adnia /portal_list", PlayerRole.SupportPlus);

addCommandAdmin("tp", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /tp <id> <toid>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local toid = args[1];
    if (!isPlayerConnected(toid))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local pos = getPlayerPosition(toid);
    setPlayerPosition(id, pos.x + 50, pos.y, pos.z);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Teleportowa�e� "+getPlayerName(id) +" do "+getPlayerName(toid));
    getAdmin(playerId).saveLog("Uzyta komenda tp" , id);

}, "Komenda zmieniaj�ca pozycj� gracza. Sk�adnia /tp id toid", PlayerRole.Helper);

addCommandAdmin("bring", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adni. Prawid�owa sk�adnia to: /bring <id_0 id_1 id_2 (...)>!");
        return;
    }

    local tp_list = split(args[0],",; ");
    local pos = getPlayerPosition(playerId);

    local text = "";
    local r = regexp(@"^\d+$");

    foreach(id in tp_list) {
        if(r.match(id)) {
            id = id.tointeger();
            if(isPlayerConnected(id)) {
                setPlayerPosition(id, pos.x + 50, pos.y, pos.z);
                text += getAccount(id).username+"(nick OOC) ";
            }
        }
        else
            sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"To nie jest id gracza - "+id+"!");
    }

    if(text!="") sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Przyzwa�e� do siebie nast�puj�cych graczy: "+text+"!");
    else
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Wszystkie ID, kt�re poda�e� by�y b��dne!");

    getAdmin(playerId).saveLog("Uzyta komenda bring", -1);
}, "Komenda przyzywaj�ca graczy. Sk�adnia to: /bring <id_0 id_1 id_2 (...)>", PlayerRole.Helper);

addCommandAdmin("heal", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d s�adniowy. Prawid�owa sk�adnia to: /heal <id> <procent_do_ustawienia>!");
        return;
    }

    local id = args[0];
    local healPercent = args[1];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    setPlayerHealth(id, getPlayerMaxHealth(id) * healPercent / 100);

    setPlayerUnconscious(id, 0);

    sendMessageToPlayer(id, 91, 152, 30, ChatType.OOC+"Administrator zmieni� Ci HP na ("+healPercent+"% twojego max HP) "+getAccount(playerId).username+"("+playerId+")(nick OOC)!");
    sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Zmieni�e� HP gracza na ("+healPercent+"% jego max HP) "+getAccount(id).username+"(nick OOC)!");
    getAdmin(playerId).saveLog("Uzyta komenda heal ("+healPercent+")", id);
}, "Komenda ustawiajaca HP gracza. Sk�adnia to: /heal <id> <procent_do_uleczenia>", PlayerRole.Helper);

addCommandAdmin("mana", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d s�adniowy. Prawid�owa sk�adnia to: /mana <id> <procent_do_uleczenia>!");
        return;
    }

    local id = args[0];
    local manaPercent = args[1];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    setPlayerMana(id, getPlayerMaxMana(id) * manaPercent / 100);

    sendMessageToPlayer(id, 91, 152, 30, ChatType.OOC+"Administrator zmieni� Ci man� na ("+manaPercent+"% twojej max many) "+getAccount(playerId).username+"("+playerId+")(nick OOC)!");
    sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Zmieni�e� Man� gracza na ("+manaPercent+"% jego max many) "+getAccount(id).username+"(nick OOC)!");
    getAdmin(playerId).saveLog("Uzyta komenda mana ("+manaPercent+")", id);
}, "Komenda ustawiaj�ca man� gracza. Sk�adnia to: /mana <id> <procent_do_uleczenia>", PlayerRole.Helper);

addCommandAdmin("healdistance", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /healdistance <distance>!");
        return;
    }

    local distance = args[0];

    if(distance == -1)
    {
        for(local i = 0; i < getMaxSlots(); i++) {
            setPlayerHealth(i, getPlayerMaxHealth(i));
            setPlayerMana(i, getPlayerMaxMana(i));
            setPlayerUnconscious(i, 0);
            sendMessageToPlayer(i, 91, 152, 30, ChatType.OOC+"Zosta�e� uzdrowiony przez administratora "+getAccount(playerId).username+"("+playerId+")");
        }
    }
    else
    {
        local position = getPlayerPosition(playerId);
        for(local i = 0; i < getMaxSlots(); i++) {
            local pos = getPlayerPosition(i);
            if(getDistance3d(position.x, position.y, position.z, pos.x, pos.y, pos.z) < distance) {
                setPlayerHealth(i, getPlayerMaxHealth(i));
                setPlayerMana(i, getPlayerMaxMana(i));
                setPlayerUnconscious(i, 0);
                sendMessageToPlayer(i, 91, 152, 30, ChatType.OOC+"Zosta�e� uzdrowiony przez administratora "+getAccount(playerId).username+"("+playerId+")");
            }
        }
    }

    sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Uzdrowi�e� wszystkich graczy w promieniu: "+distance+"!");
    getAdmin(playerId).saveLog("Uzyta komenda healdistance " + distance , -1);

}, "Komenda uzdrawiaj�ca graczy w okre�lonym promieniu. Sk�adnia to: /healdistance <distance>!", PlayerRole.SupportPlus);

addCommandAdmin("poke", function (playerId, params) {
    local args = sscanf("dds", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /poke <id> <dmg> <pow�d>!");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    local dmg = args[1];
    if (dmg <= 0) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Nie mo�esz zada� ujemnych b�dz zerowych obra�e�!");
        return;
    }

    local calc = getPlayerHealth(id)-dmg;
    if (calc < 2) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Komenda poke nie s�u�y do zabijania graczy, u�yj komendy kill!");
        return;
    }

    setPlayerHealth(id, calc);

    sendMessageToPlayer(id, 152, 30, 30, ChatType.OOC+"Zosta�e� uderzony przez administratora "+getAccount(playerId).username+"("+playerId+")(nick OOC)! za "+dmg+" obra�e�! Pow�d to: "+args[2]);
    sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Uderzy�e� gracza "+getAccount(id).username+"(nick OOC)!");

    getAdmin(playerId).saveLog("Uzyta komenda poke dmg: " + dmg + " z powodu " + args[2], id);
}, "Komenda szturchaj�ca gracza. Sk�adnia to: /poke <id> <dmg> <pow�d>", PlayerRole.SupportPlus);

addCommandAdmin("warn", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /warn <id> ><tresc>!");
        return;
    }

    local id = args[0], text = args[1];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }
    local idName = getAccount(id).username;

    PlayerPacket(id).addNotification(getAccount(playerId).username+" przyzna� Ci ostrze�enie o tre�ci: "+text);

    sendMessageToAll(152, 30, 30, ChatType.OOC+"Gracz "+idName+"("+id+")(nick OOC) otrzyma� ostrze�enie o tre�ci: "+text+"!");
    sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Ostrzeg�e� gracza "+idName+"(nick OOC)!");
    sendMessageToPlayer(id, 152, 30, 30, ChatType.OOC+"Dosta�e� ostrze�enie "+text);

    getAdmin(playerId).saveLog("Uzyta komenda poke: " + text, id);
}, "Komenda generuje komunikat o ostrze�eniu dla gracza. Sk�adnia to: /warn <id> <tre��>!", PlayerRole.Support);

addCommandAdmin("cheer", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /cheer <id> <tre��>!");
        return;
    }

    local id = args[0], text = args[1];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }
    local reason = args[1];

    local idName = getAccount(id).username;

    PlayerPacket(id).addNotification(getAccount(playerId).username+" przyzna� Ci pochwa�� o tre�ci: "+text);

    sendMessageToAll(91, 152, 30, ChatType.OOC+"Gracz "+idName+"("+id+")(nick OOC) otrzyma� pochwa�� o tre�ci: "+text+"!");
    sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Pochwali�e� gracza "+idName+"(nick OOC)!");

    getAdmin(playerId).saveLog("Uzyta komenda cheer: " + reason, id);

}, "Komenda generuje komunikat o pochwale dla gracza. Sk�adnia to: /cheer <id> <tre��>!", PlayerRole.Support);

addCommandAdmin("godmode", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /godmode <id>!");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    local idName = getAccount(id).username;

    local player = getPlayer(id);
    local playerGodMode = getPlayer(id).isGodModeEnabled();

    if(playerGodMode)
    {
        sendMessageToPlayer(id, 152, 30, 30, ChatType.OOC+"Godmode zosta� Ci odebrany przez "+getAccount(playerId).username+"("+playerId+")!");
        sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Odebra�e� graczowi "+idName+"(nick OOC) godmode!");
    }
    else
    {
        sendMessageToPlayer(id, 91, 152, 30, ChatType.OOC+"Otrzyma�e� godmode od "+getAccount(playerId).username+"("+playerId+")!");
        sendMessageToPlayer(playerId, 91, 152, 30, ChatType.ADMIN+"Da�e� graczowi "+idName+"(nick OOC) godmode!");
    }

    getPlayer(id).setGodMode(!playerGodMode);

    getAdmin(playerId).saveLog("Uzyta komenda godmode", id);

}, "Komenda nadaje godmode, kt�ry znika po relogu. Sk�adnia to: /godmode <id>!", PlayerRole.SupportPlus);


addCommandAdmin("unban", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /unban <id>");
        return;
    }

    local id = args[0];
    if(!(id in AdminController.bans))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje ban gracza o tym id.");
        return;
    }

    unban(id);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Odbanowa�e� "+id);

    getAdmin(playerId).saveLog("Uzyta komenda unban", id);
}, "Komenda usuwaj�ca ban na�o�ony na graczu. Sk�adnia /unban <id>", PlayerRole.GameMasterPlus);

addCommandAdmin("banlist", function (playerId, params) {
    foreach(banId, ban in AdminController.bans)
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Ban Id:"+banId+" - "+ban.name);

}, "Komenda pokazuj�ca osoby zbanowane. Sk�adnia /banlist", PlayerRole.SupportPlus);

addCommandAdmin("mount", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /mount <id gracza> <id potwora>");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"0 - �cierwojad 1 - wilk 2 - warg 3 - cieniostw�r");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"4 - owca 5 - du�y goblin 6 - jaszczur");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"7 - troll 8 - krwiopijca 9 - w�� b�otny");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"10 - ko�, 11 - ork kobita, -1 - usu�");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local type = args[1];
    System.Mount.add(id, type);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Da�e� "+getPlayerName(id) +" mounta.");
    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"Dosta�e� mounta od "+getPlayerName(playerId));

    getAdmin(playerId).saveLog("Uzyta komenda mount typ: " + type, id);
}, "Komenda ustawiaj�ca mount'a dla gracza. Sk�adnia /mount <id gracza> <id potwora>", PlayerRole.GameMaster);

addCommandAdmin("seepw", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /seepw <id>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if(getAdmin(playerId).hasPlayerInPwTable(id) == false)
        getAdmin(playerId).seePwTable.push(id);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Ogl�dasz pw gracza nr. "+id);
    getAdmin(playerId).saveLog("Uzyta komenda seepw", id);

}, "Komenda sprawdzaj�ca pw gracza. Sk�adnia /seepw <id gracza>", PlayerRole.GameMaster);


addCommandAdmin("unseepw", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /unseepw <id>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if(getAdmin(playerId).hasPlayerInPwTable(id))
        getAdmin(playerId).seePwTable.remove(getAdmin(playerId).seePwTable.find(id));

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie ogl�dasz pw gracza nr. "+id);
    getAdmin(playerId).saveLog("Uzyta komenda unseepw", id);
}, "Komenda ustawiaj�ca koniec sprawdzania pw dla gracza. Sk�adnia /unseepw <id gracza>", PlayerRole.GameMaster);

addCommandAdmin("seeallpw", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /seeallpw  <range (-1 dla braku)>");
        return;
    }

    local range = args[0];
    if(range == -1)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"U�yj komendy /unseeallpw");
        return
    }

    getAdmin(playerId).seePw = true;
    getAdmin(playerId).seePwRange = range;

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Ogl�dasz pw graczy.");
    getAdmin(playerId).saveLog("Uzyta komenda seeallpw na dystans: " + range, -1);

}, "Komenda sprawdzaj�ca pw graczy. Sk�adnia /seeallpw <range>", PlayerRole.GameMaster);

addCommandAdmin("unseeallpw", function (playerId, params) {
    getAdmin(playerId).seePw = false;
    getAdmin(playerId).seePwRange = null;

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie ogl�dasz pw.");
    getAdmin(playerId).saveLog("Uzyta komenda unseeallpw ", -1);
}, "Komenda ustawiaj�ca koniec sprawdzania pw. Sk�adnia /unseeallpw", PlayerRole.GameMaster);

addCommandAdmin("pos", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /pos <nazwa moba>");
        return;
    }
    local pos = getPlayerPosition(playerId);
    local nazwamoba = args[0];
  sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Pozycja "+pos.x+","+pos.y+","+pos.z+" obrot "+getPlayerAngle(playerId)+" dla "+nazwamoba+"");
    print("Pozycja "+pos.x+","+pos.y+","+pos.z+" obrot "+getPlayerAngle(playerId)+" dla "+nazwamoba+"");
    local myfile = io.file("pos.nut", "a");
    myfile.write("addMob(\""+nazwamoba+"\", "+pos.x+", "+pos.y+", "+pos.z+", "+getPlayerAngle(playerId)+");\n");
    myfile.close();
}, "Komenda pokazuj�ca pozycj�. Sk�adnia /pos <nazwa moba>", PlayerRole.Helper);

addCommandAdmin("giverole", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /giverole <id gracza> <role 0 - user, 1 - patron, 2 - budowniczy, 3 - narrator, 4 - helper, 5 - support, 6 - supportPlus, 7 - gamemaster, 8 - gamemasterPlus, 9 - moderator (gmPRO), 10 - admin>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local role = args[1];
    if(role < 0 || role > 10) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje rola o tym id.");
        return;
    }

    getAdmin(id).role = role;

    if(role != PlayerRole.User && role != PlayerRole.Patreon && role != PlayerRole.Narrator && role != PlayerRole.Builder) {
        foreach(account in getAccounts()) {
            local playerObject = getPlayer(account.id);
            AdminPacket().sendPlayerRegister(id, account.id, playerObject.name, account.username, playerObject.rId)
        }
    }

    getAdmin(id).save();
    AdminPacket().setRole(id, role);
    getAdmin(id).afterRoleChange();

    sendMessageToPlayer(id, 255, 0, 0, ChatType.OOC+"Dosta�e� rol� "+role + " od gracza nr."+playerId);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nada�e� graczowi nr."+id+" rol� "+role);
    getAdmin(playerId).saveLog("Uzyta komenda giverole " + role, id);

}, "Komenda ustawiaj�ca rol� gracza. Sk�adnia /giverole <id gracza> <role 0 - user, 1 - patron, 2 - budowniczy, 3 - narrator, 4 - helper, 5 - support, 6 - supportPlus, 7 - gamemaster, 8 - gamemasterPlus, 9 - moderator (gmPRO), 10 - admin>", PlayerRole.Admin);

addCommandAdmin("killnpc", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /killnpc <id bota>");
        return;
    }

    local bot = getBot(args[0]);
    if(bot == null) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje bot o tym id.");
        return;
    }

    bot.playAnimation("S_DEAD");
    bot.setHealth(0);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Zabi�e� "+bot.name);
    getAdmin(playerId).saveLog("Uzyta komenda killnpc id: " + args[0], -1);

}, "Komenda zabijaj�ca npc. Sk�adnia /killnpc <id bota>", PlayerRole.SupportPlus);

addCommandAdmin("playani", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /playani <anim name>");
        return;
    }

    playAni(playerId, args[0])

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+" odpali�e� animacj�" + args[0]);
}, "Komenda odpalaj�ca wybran� animacj�. Sk�adnia /playani <anim name>", PlayerRole.Helper);

addCommandAdmin("addeffect", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /addeffect <id gracza> <id animacji> <refresh time>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local effectId = args[1];
    local refreshTime = args[2];

    getPlayer(id).effectModule.addEffect(effectId, refreshTime);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nada�e� efekt " + effectId + " graczowi nr."+id);
    getAdmin(playerId).saveLog("Uzyta komenda addeffect id: " + effectId, id);
}, "Komenda daj�ca efekt graczowi. Sk�adnia /addeffect <id gracza> <id animacji> <refresh time>", PlayerRole.SupportPlus);

addCommandAdmin("resendnames", function (playerId, params) {
    foreach(account in getAccounts()) {
        local playerObject = getPlayer(account.id);
        AdminPacket().sendPlayerRegister(playerId, account.id, playerObject.name, account.username, playerObject.rId)
    }

}, "Ponownie wysy�a dane do ciebie odno�nie graczy.", PlayerRole.Helper);

addCommandAdmin("removeeffect", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /removeeffect <id gracza> <id animacji>!");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    local effectId = args[1];

    if(getPlayer(id).effectModule.removeEffect(effectId))
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usune efekt " + effectId + " graczowi nr."+id);
    else
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Ten gracz nie ma na sobie efektu o podanym ID!");

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usun��e� efekt " + effectId + " graczowi nr."+id);
    getAdmin(playerId).saveLog("Uzyta komenda removeeffect id: " + effectId, id);

}, "Komenda zabieraj�ca efekt z gracza. Sk�adnia to: /removeeffect <id gracza> <id animacji>", PlayerRole.SupportPlus);

addCommandAdmin("removeeffects", function (playerId, params) { //
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adniowy. Prawid�owa sk�adnia to: /removeeffecs <id gracza>!");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Gracz o ID: "+id+" nie jest po��czony z serwerem!");
        return;
    }

    getPlayer(id).effectModule.removeAllEffects();
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usun��e� wszystkie efekty graczowi "+getAccount(id).username+"(nick OOC)!");
    getAdmin(playerId).saveLog("Uzyta komenda removeeffects", id);
}, "Komenda usuwaj�ca wszystkie efekty z gracza. Sk�adnia to: /removeeffects <id gracza>", PlayerRole.SupportPlus);

addCommandAdmin("giveasset", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /giveasset <id gracza> <id assetu>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local assetId = args[1];

    if(!(assetId in AdminController.assets))
        return;

    AdminController.givePlayerAsset(id, assetId);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Da�e� asset " + AdminController.assets[assetId].name + " graczowi nr."+id);
    getAdmin(playerId).saveLog("Uzyta komenda giveasset o id:" + assetId, id);
}, "Komenda daj�ca asset graczowi. Sk�adnia /giveasset <id gracza> <id assetu>", PlayerRole.Mod);

addCommandAdmin("removeasset", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /removeasset <id gracza>");
        return;
    }

    local id = args[0];
    if (!isPlayerConnected(id)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    AdminController.revertPlayerAsset(id);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usun��e� asset graczowi nr."+id);
    getAdmin(playerId).saveLog("Uzyta komenda removeassets", id);

}, "Komenda usuwaj�ca asset graczowi. Sk�adnia /removeasset <id gracza>", PlayerRole.Mod);

addCommandAdmin("assets", function (playerId, params) {
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Assety:");
    foreach(asset in AdminController.assets)
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Asset id:"+asset.id+" - "+asset.name);
}, "Komenda pokazuj�ca assety graczowi. Sk�adnia /assets", PlayerRole.Mod);

addCommandAdmin("weather_sun", function (playerId, params) {
    local args = sscanf("fddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /weather_sun <size> <r> <g> <b>");
        return;
    }

    System.Weather.cache.sunSize = args[0];
    System.Weather.cache.sunColor = {r = args[1], g = args[2], b = args[3]};
    System.Weather.updateWeather();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono konfiguracj� pogody.");
}, "Komenda pozwalaj�ca ustawi� rozmiar i kolor s�o�ca.", PlayerRole.GameMasterPlus);

addCommandAdmin("weather_moon", function (playerId, params) {
    local args = sscanf("fddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /weather_moon <size> <r> <g> <b>");
        return;
    }

    System.Weather.cache.moonSize = args[0];
    System.Weather.cache.moonColor = {r = args[1], g = args[2], b = args[3]};
    System.Weather.updateWeather();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono konfiguracj� pogody.");
}, "Komenda pozwalaj�ca ustawi� rozmiar i kolor ksi�yca.", PlayerRole.GameMasterPlus);

addCommandAdmin("weather_cloud", function (playerId, params) {
    local args = sscanf("ddd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /weather_cloud <r> <g> <b>");
        return;
    }

    System.Weather.cache.cloudsColor = {r = args[0], g = args[1], b = args[2]};
    System.Weather.updateWeather();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono konfiguracj� pogody.");
}, "Komenda pozwalaj�ca ustawi� kolor pogody.", PlayerRole.GameMasterPlus);


addCommandAdmin("weather", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /weather <0 - s�o�ce, 1 - �nieg, 2 - deszcz>");
        return;
    }

    if(args[0] < 0 || args[0] > 2)
        return;

    System.Weather.cache.weather = args[0];
    System.Weather.updateWeather();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono konfiguracj� pogody.");
}, "Komenda pozwalaj�ca ustawi� kolor pogody.", PlayerRole.GameMasterPlus);

addCommandAdmin("gdo", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.OOC+"Wpisz /gdo <dystans> <text>");
        return;
    }

    if(args[0] < 0 || args[0] > 52352)
        return;

    local distance = args[0];

    distanceChat(playerId, ChatType.IC, distance, 192, 166, 122, args[1], false)
    getAdmin(playerId).saveLog("Uzyta komenda gdo o tresci: " + args[1], -1);
}, "Komenda pozwalaj�ca chatowa� DO z troch� wi�kszym dystansem.", PlayerRole.Narrator);

addCommandAdmin("pw_do", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.OOC+"Wpisz /pw_do <id> <text>");
        return;
    }

    local sendId = args[0];
    if(isPlayerConnected(sendId) == false) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Gracz nie jest po��czony.");
        return;
    }
    local text = args[1];

    sendMessageToPlayer(playerId, 192, 166, 122, ChatType.IC+"Wiadomo�� do "+getPlayerRealName(playerId, sendId) + "(" + sendId + ")"+ " : "+text);
    sendMessageToPlayer(sendId, 192, 166, 122, ChatType.IC+text);
    getAdmin(playerId).saveLog("Uzyta komenda pw_do o tresci: " + text, sendId);
}, "Komenda pozwalaj�ca chatowa� DO do konkretnego gracza.", PlayerRole.Narrator);


addCommandAdmin("buff", function (playerId, params) {
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/buff_player <id> - lista buf�w dla gracza");
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/buff_add <playerId> <buff name> - daj buff dla gracza");
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"/buff_remove <playerId> <buff name> - usun buff od gracza");
}, "Komenda pokazuj�ca list� komend odno�nie buf�w.", PlayerRole.SupportPlus);

addCommandAdmin("buff_player", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /buff_player id");
        return;
    }

    local argId = args[0];
    if (!isPlayerConnected(argId)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Bufy "+getPlayerName(argId)+":");
    foreach(buff in BuffController.buffs[argId]) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+""+buff.scheme+" : "+buff.counter);
    }
    getAdmin(playerId).saveLog("Uzyta komenda buff_player", argId);

}, "Komenda pokazuj�ca buffy gracza. Sk�adnia /buff_player id", PlayerRole.SupportPlus);

addCommandAdmin("buff_add", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /buff_add id schemat");
        return;
    }

    local argId = args[0];
    if (!isPlayerConnected(argId)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local scheme = args[1];
    BuffController.givePlayerBuff(argId, scheme);
    sendMessageToPlayer(argId, 255, 0, 0, ChatType.OOC+"Dosta�e� buff o nazwie "+scheme);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Da�e� buff o nazwie "+scheme+" graczowi o id "+argId);
    getAdmin(playerId).saveLog("Uzyta komenda buff_add " + scheme, argId);

}, "Komenda dodaj�ca buff dla gracza. Sk�adnia /buff_add id schemat", PlayerRole.SupportPlus);

addCommandAdmin("buff_remove", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /buff_remove id schemat");
        return;
    }

    local argId = args[0];
    if (!isPlayerConnected(argId)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    local scheme = args[1];
    BuffController.removePlayerBuff(argId, scheme);
    sendMessageToPlayer(argId, 255, 0, 0, ChatType.OOC+"Usun��e� buff o nazwie "+scheme);
    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usun��e� buff o nazwie "+scheme+" graczowi o id "+argId);
    getAdmin(playerId).saveLog("Uzyta komenda buff_remove " + scheme, argId);

}, "Komenda usuwaj�ca buff dla gracza. Sk�adnia /buff_remove id schemat", PlayerRole.SupportPlus);
addCommandAdmin("reset", function(playerId, params) { //reset
    local args = sscanf("ds", params);
    if (!args)  {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adni. Wpisz /reset id Tak");
        return;
    }

    local argId = args[0];
    if (!isPlayerConnected(argId)) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    if(args[1] != "Tak")
        return;

    local pObject = getPlayer(argId);
    if(System.Reset.exist(pObject.rId)) {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Na tej postaci zrobiono ju� reset.");
        return;
    }

    System.Reset.add(pObject.rId);
    local moreSystemStr = "Zrobil reset ";
    local learnPoints = pObject.learnPoints;

    local str = pObject.str - 10;
    if(str > 0) {
        if(str > 20) {
            local rest = str - 20;
            str = str - 20;
            learnPoints = learnPoints + (rest * 5);
        }

        learnPoints = learnPoints + (str * 3);
    }

    moreSystemStr += " po sile: "+learnPoints;

    local dex = pObject.dex - 10;
    if(dex > 0) {
        if(dex > 20) {
            local rest = dex - 20;
            dex = dex - 20;
            learnPoints = learnPoints + (rest * 5);
        }

        learnPoints = learnPoints + (dex * 3);
    }

    moreSystemStr += " po zrecz: "+learnPoints;

    local inteligence = pObject.inteligence - 10;
    if(inteligence > 0) {
        if(inteligence > 20) {
            local rest = inteligence - 20;
            inteligence = inteligence - 20;
            learnPoints = learnPoints + (rest * 5);
        }

        learnPoints = learnPoints + (inteligence * 3);
    }

    moreSystemStr += " po inteligen: "+learnPoints;

    local hp = pObject.hpMax - 100;
    if(hp > 0) {
        learnPoints = learnPoints + abs(hp/2);
    }

    moreSystemStr += " po hp: "+learnPoints;

    local mana = pObject.maxMana - 100;
    if(mana > 0) {
        learnPoints = learnPoints + abs(mana/2);
    }

    moreSystemStr += " po mana: "+learnPoints;

    foreach(weaponValue in pObject.weapon) {
        learnPoints = learnPoints + abs(weaponValue * 2);
    }

    moreSystemStr += " po 1h,2h,bow,cbow: "+learnPoints;

    foreach(skill in pObject.skills) {
        if(skill == 0)
            continue;

        if(skill == 1) {
            learnPoints = learnPoints + 6;
        } else if(skill == 2) {
            learnPoints = learnPoints + 66;
        }else {
            learnPoints = learnPoints + 166;
        }
    }

    moreSystemStr += " po skillu: "+learnPoints;
    addPlayerLog(argId, moreSystemStr);
    setPlayerStrength(argId, 10);
    setPlayerDexterity(argId, 10);
    setPlayerInteligence(argId, 10);
    setPlayerMaxHealth(argId, 100)
    setPlayerHealth(argId, 100)
    setPlayerMagicLevel(argId, 0);
    setPlayerMaxMana(argId, 10);
    setPlayerMana(argId, 10);
    setPlayerSkillWeapon(argId, 0, 0);
    setPlayerSkillWeapon(argId, 1, 0);
    setPlayerSkillWeapon(argId, 2, 0);
    setPlayerSkillWeapon(argId, 3, 0);
    foreach(index, value in Config["PlayerSkill"])
        setPlayerSkill(argId, index, 0);

    setPlayerLearnPoints(argId, learnPoints);
    sendMessageToPlayer(argId, 152, 30, 30, ChatType.OOC+"Dokonano resetu. Twoje punkty postaci: "+learnPoints);
    sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Dokonano resetu. Liczba punkt�w postaci: "+learnPoints);

    getAdmin(playerId).saveLog("Uzyta komenda reset ", argId);

} "Komenda resetuj�ca punkty nauki gracza",PlayerRole.Mod)

addCommandAdmin("spawn",function(playerId,params){
    local args = sscanf("ssddd", params);

    if (!args)  {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"B��d sk�adni. Wpisz /spawn <nazwa> <instance> <hp> <si�a> <ilosc>");
        return;
    }

    local name = args[0];
    local instance = args[1];


    if(instance == "PC_HERO"){
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.ADMIN+"Komenda s�u�y tylko do tworzenia bot�w potwor�w");
        return;
    }

    for(local i = 0; i < args[4]; i++) {
        local position = getPlayerPosition(playerId);
        local digital = irand(10);
        if(digital < 3) {
            position.x = position.x + rand() % 300;
            position.z = position.z + rand() % 300;
        }else if(digital < 7) {
            position.x = position.x + rand() % 300;
            position.z = position.z - rand() % 300;
        }else {
            position.x = position.x - rand() % 300;
            position.z = position.z - rand() % 300;
        }

        local schemeObject = null
        foreach(scheme in BotSchemes)
        {
            if (scheme.instance.tolower() == instance.tolower())
            {
                schemeObject = scheme;
                break
            }
        }

        local object = BotMonster(name);
        object.group = "";
        object.instance = instance;
        object.isTemporary = true;
        object.isSaved = true;
        object.world = getPlayerWorld(playerId)
        object.position = clone position;
        object.setPosition(object.position.x,object.position.y,object.position.z);
        object.setAngle(getPlayerAngle(playerId));
        object.str = args[3];
        object.dex = 10;
        object.hp = args[2];
        object.hpMax = 50;
        object.scale = {x = 1.0, y = 1.0, z = 1.0};
        object.animation = "";
        object.weapon =  [0,0,0,0];
        object.respawnTime = 10;
        object.melee = -1;
        object.armor = -1;
        object.ranged = -1;
        object.helmet = -1;
        object.shield = -1;
        object.magic = -1;
        object.visual = {bodyModel = "Hum_Body_Naked0", bodyTxt = -1, headModel = "Hum_Head_FatBald", headTxt = -1};

        if (schemeObject != null)
        {
            if ("melee" in schemeObject)
                object.melee = Items.id(schemeObject.melee);
        }

        AdminPacket().sendBot(playerId, object);

        for(local i = 0; i <= getMaxSlots(); i ++) {
            if(isPlayerSpawned(i))
                callEvent("onPlayerChangeCell", i, getPlayer(i).cell, getPlayer(i).cell);
        }
    }

    PlayerPacket(playerId).addNotification("Uda�o si� doda� npc.");
    getAdmin(playerId).saveLog("Doda� "+args[4]+" NPC "+name +"("+instance+")", playerId);

} "Komenda spawnuj�ca bota", PlayerRole.SupportPlus)

addCommand("admin_confirm", function (playerId, params) {
    local args = sscanf("s", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /admin_confirm haslo");
        return;
    }

    foreach(_user, _password in Config["AdminConfirms"]) {
        if(_password == args[0])
        {
            local fileObject = file("database/admin-pass.txt", "a+");
            fileObject.write(getPlayerName(playerId) + " || " + getPlayerSerial(playerId) + " || " + time() + " || " + getAccount(playerId).rId + " " + args[0] + " \n")
            fileObject.close();

            getAdmin(playerId).active = true;
            AdminPacket().sendAdminConfirmation(playerId);
            sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Konto zosta�o aktywowane.");
            return;
        }
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+" Nie ma takiego has�a.");
});

addCommandAdmin("bot_delete_range", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /bot_delete_range range");
        return;
    }

    local pos = getPlayerPosition(playerId);
    foreach(_bot in getBots()) {
        if(_bot.positionDifference(pos) < args[0])
            _bot.destroyBot();
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usuni�to boty w sferze "+args[0]);
    getAdmin(playerId).saveLog("Uzyta komenda bot_delete_range dist: " + args[0], -1);

}, "Kasuje wszystkie NPC w podanym zasi�gu", PlayerRole.Admin);

addCommandAdmin("monster_delete_range", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /monster_delete_range range");
        return;
    }

    local pos = getPlayerPosition(playerId);
    foreach(_bot in getBots()) {
        if(_bot instanceof BotMonster) {
            if(_bot.positionDifference(pos) < args[0])
                _bot.destroyBot();
        }
    }

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Usuni�to bestie w sferze "+args[0]);
    getAdmin(playerId).saveLog("Uzyta komenda monster_delete_range dist: " + args[0], -1);

}, "Kasuje wszystkie Monstery w podanym zasi�gu", PlayerRole.Admin);

addCommandAdmin("set_unconscious", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /set_unconscious <id> <seconds>");
        return;
    }

    setPlayerUnconscious(args[0], args[1]);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+" Da�e� BW na "+args[1]+" sekund graczowi o id "+args[0]);
    sendMessageToPlayer(args[0], 255, 0, 0, ChatType.OOC+"Dosta�e� "+args[1]+" sekund BW.");
}, "Komenda dajaca bw /set_unconscious <id> <seconds>", PlayerRole.GameMaster);

//Komenda /world wymaga dodania pobocznych swiatow do resources/cell.nut aby mozna bylo si� tam teleportowac oraz wymaga mapy w addonie z modelami i teksturami
addCommandAdmin("world", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /world <id> <destination>");
        return;
    }

    local id = args[0];
    local path = args[1];
    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    setPlayerWorld(id, path);

    sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Teleportowa�e� "+getPlayerName(id) +" do innego swiata");
    getAdmin(playerId).saveLog("Uzyta komenda world" , id);

}, "Komenda zmieniaj�ca �wiat gracza. Sk�adnia /world id swiat", PlayerRole.GameMasterPlus);

//Ujawnia stalker�w w zadanym dystansie
addCommandAdmin("klauzula", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /klauzula <distance>");
        return;
    }

    local pos = getPlayerPosition(playerId)

    foreach(player in getPlayers())
    {
        if (player.id == playerId)
            continue

        local posTarget = getPlayerPosition(player.id)

        if (getPlayerInvisible(player.id) && getDistance3d(pos.x, pos.y, pos.z, posTarget.x, posTarget.y, posTarget.z) <= args[0])
        {
            setPlayerInvisible(player.id, false)
            sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Sciagnieto niewidzialnosc " + player.name);
        }
    }
}, "Komenda rozwojowa, nie dotyka�.", PlayerRole.Admin);