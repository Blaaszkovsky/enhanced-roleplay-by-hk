
local loadedMutes = [];

function mute(pid, minutes, option)
{
    local timeout = 0;
    if(minutes != 0)
        timeout = time() + ( minutes * 60);

    ChatController.addMute(pid, option, timeout);
    MuteList.Add(getAccount(pid).username, getPlayerMacAddr(pid), getPlayerSerial(pid), option, timeout);
}

class MuteList
{
    dbTable = "mute";

    function load()
    {
        foreach(data in Query().select().from(MuteList.dbTable).all())
            MuteList.Add(data["name"], data["mac"], data["serial"], data["status"].tointeger(), data["timeout"].tointeger());

        print(loadedMutes.len() + " - amount of given mutes.");
    }

    function save()
    {
        Query().deleteFrom(MuteList.dbTable).execute();

        if(loadedMutes.len() > 0)
        {
            local arr = [];
            foreach(item in loadedMutes)
                arr.append(["'"+item.name+"'", "'"+item.serial+"'", "'"+item.mac+"'", item.option, item.expiration]);

            Query().insertInto(MuteList.dbTable, ["name", "serial", "mac", "status", "timeout"], arr).execute();
        }
    }

    function Add(name, mac, serial, option, expiration)
    {
        loadedMutes.append({ name = name, mac = mac, serial = serial, option = option, expiration = expiration });
        MuteList.save();
    }

    function RemoveByName(name)
    {
        foreach(c, b in loadedMutes)
        {
            if(b.name == name)
                loadedMutes.remove(c);
        }
    }

    function isPersonMuted(obj, pid)
    {
        if(obj.expiration != 0 && obj.expiration < time())
            return false;

        if(obj.mac == getPlayerMacAddr(pid) || obj.serial == getPlayerSerial(pid))
            return true;

        return false;
    }

    function checkMuteList(pid)
    {
        foreach(mute in loadedMutes)
        {
            if(MuteList.isPersonMuted(mute, pid)) {
                ChatController.addMute(pid, mute.option, mute.expiration);
                return true;
            }
        }
        return false;
    }
}

addEventHandler("onInit", MuteList.load.bindenv(MuteList));