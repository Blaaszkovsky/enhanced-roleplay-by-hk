New table
/createportal x y z time	Tworzy portal przenoszacy kazdego kto w niego wejdzie badz wejdzie w interakcje przez okreslony czas
/door	stawia drzwi
/deldoor	usuwa drzwi
/seepw id	Na chacie dostajesz ka�� wiadomo�� PW wys�an� i dostarczon� od/do jakiego� gracza pod danym ID.
/unseepw id	Przerywa podgl�danie PW gracza
/pos	Pokazuje pozycje x y z
Lista komend
/play id link	Odtwarza muzyke z linku do yt (przyczepione do gracza)
/stopplay id	stopuje dzwiek
/play music range	Gra na danym zasi�gu muzyk�, kt�ra jest w addonie
/stopplay music	stopuje dzwiek
Lista komend
/giverole admin/mod/vip	Nadaje rang� administratora/moderatora
/givepn id	Nadaje PN graczowi (ograniczenie liczby, ale nie dla Szakala)
/giveitem id name	Daje graczowi przedmiot
/givestat id name number	Nadaje statystyke graczowi z dan� warto�ci�
/giveability id name	Nadaje umiej�tno�� graczowi
Lista komend
/tp id1 id2	Teleportuje gracza o jednym id do drugiego
/bring id	Teleportuje gracza do siebie
/bring group id id id	Teleportuje kilku graczy do siebie
/goto id	Teleportuje ciebie do kogo�
Lista komend
/heal id amount	Leczy gracza
/kill id	zabija gracza
/unbw id	Podnosi gracza z bw
/healdistance distance Amount	Leczy graczy o dan� warto�� na dany obszar
/killdistance distance	Zabija graczy na dany obszar
/unbwdistance distance	podnosi graczy z bw na danym obszarze
/stamina id amount	Dodanie graczowi staminy, je�li co� si� zbuguje, lub dla admina.
/mana id amount	Tak jak /stamina czy /heal tylko z man�
Lista komend
/draw text	Tworzy drawa z id, kt�rego tekst jest w #(...)#
/editdraw	Pojawia si� menu, w kt�rym edytujesz opcje drawa
Lista komend
/checkability	Sprawdza umiej�tno�ci gracza
/checkname id	Sprawdza imie postaci gracza
/checkstat id	sprawdza statystyki gracza
/log id	Pokazuje historie wiadomosci gracza
/topmoney	Pokazyje graczy z najwieksza iloscia zlota/bry�ek
/checkchest id	Saprawdza zawarto�� skrzyni gracza
/checkeq id	Pokazuje EQ gracza
/pwall	Pokazuje pw wszystkich graczy na serwerze
/blacklist	Pokazuje liczb� zbanowanych graczy
/ahelp	pokazuje liste komend
/wb	w��cza world builder
Lista komend
/spawnnpc	Respi NPC
/delnpc id	Usuwa NPC
/editnpc id	Edytuje NPC
New table
/saveped name	Zapisuje wyglad postaci wraz z nickiem i przedmiotami i statystki
/loadped name	Wczytuje wyglad postaci wraz ze zmiana nicku i przedmiotami i statystki
/resetped	Wraca twoje wszystkie statystki do domy�lne postaci
/fck id	Zabija posta� gracza
/change id mobid	Zmienia gracza w moba lub npc z singla
/god	W��cza God Mode
/poke id reason	zadaje obrazenia graczowi i wysyla wiadomosc
Lista komend
/scale id scale	Zmienia wielko�� postaci
/vanish id	Stajesz si� niewidzialny
/namecolor id color	Zmienia kolor nicku gracza
/pfx id name time	Nadaje efekt pfx na gracza na okreslony czas
/weather name	zmienia pogode
/freeze id	unieruchamia gracza
/freezegroup id id id
/freezedistance amount
/spectate id	Kamera przenosi sie na danego gracza
/freecam	Wolna kamera
/restart delaytime	restartuje server za iles minut, wysy�a wiadomo�� na czatach
St� My�liwego