
ItemsBuilder <- {
    bucket = null,
    itemId = -1,

    prepareWindow = function() {
        local result = {};

        result.previewWindow <- GUI.Window(anx(50), any(Resolution.y/2 - 200), anx(240), any(280), "SG_BOX.TGA");
        result.createButton <- GUI.Button(anx(20), any(20), anx(200), any(60), "SG_BUTTON.TGA", "Stw�rz", result.previewWindow);
        result.playerItemsButton <- GUI.Button(anx(20), any(100), anx(200), any(60), "SG_BUTTON.TGA", "Itemy Gracza", result.previewWindow);
        result.exitButton <- GUI.Button(anx(20), any(180), anx(200), any(60), "SG_BUTTON.TGA", "Wyjd�", result.previewWindow);

        result.createButton.bind(EventType.Click, function(element) {
            ItemsBuilder.createItem();
        });

        result.playerItemsButton.bind(EventType.Click, function(element) {
            ItemsBuilder.showPlayerList();
        });

        result.exitButton.bind(EventType.Click, function(element) {
            ItemsBuilder.hide();
        });

        result.playerWindow <- GUI.Window(anx(Resolution.x - 500), any(Resolution.y/2 - 400), anx(460), any(200), "SG_BOX.TGA");
        result.playerItemsWindow <- GUI.Window(anx(Resolution.x - 500), any(Resolution.y/2 - 150), anx(460), any(580), "SG_BOX.TGA");
        result.createWindow <- GUI.Window(anx(Resolution.x - 500), any(Resolution.y/2 - 400), anx(460), any(800), "SG_BOX.TGA");

        result.ui <- {};
        result.uiGUI <- {};

        result.store <- {};

        return result;
    }

    createItem = function() {
        foreach(item in bucket.uiGUI) {
            item.setVisible(false);
            item.destroy();
        }

        bucket.uiGUI.clear();
        bucket.ui.clear();

        bucket.ui = {};

        bucket.playerWindow.setVisible(false);
        bucket.playerItemsWindow.setVisible(false);
        bucket.createWindow.setVisible(true);

        local contentSecondary = [];

        for(local i = 0; i < getMaxSlots(); i ++)
        {
            contentSecondary.push({
                id = i,
                name = "("+i+") "+getPlayerAdminStatistics(i).characterName
            });
        }

        bucket.uiGUI = {
            instance = GUI.Input(anx(20), any(20), anx(420), any(50), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Instance", 2, bucket.createWindow),
            infoDraw = GUI.Draw(anx(20), any(73), "", bucket.createWindow),
            name = GUI.Input(anx(20), any(100), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2, bucket.createWindow),
            description = GUI.Input(anx(20), any(180), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Opis", 2, bucket.createWindow),
            guid = GUI.Input(anx(20), any(260), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Guid", 2, bucket.createWindow),
            amount = GUI.Input(anx(20), any(340), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Ilo��", 2, bucket.createWindow),
            resistance = GUI.Input(anx(20), any(500), anx(210), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Odporno��", 2, bucket.createWindow),
            repetition = GUI.Input(anx(230), any(500), anx(210), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Powt�rze�", 2, bucket.createWindow),
            stacked = GUI.CheckBox(anx(20), any(420), anx(40), any(40), "SG_CHECKBOX.TGA", "SG_CHECKBOX_CHECKED.TGA", bucket.createWindow),
            draw = GUI.Draw(anx(80),any(420),"Stackowany item (X\n - ilo�� jeden >= 1)", bucket.createWindow),
            target = MyGUI.DropDown(anx(Resolution.x - 480), any(Resolution.y/2 + 180), anx(420), any(60), "Wywo�anie", contentSecondary, "Dla kogo..", heroId),
            butt = GUI.Button(anx(20), any(700), anx(420), any(60), "SG_BUTTON.TGA", "Utw�rz", bucket.createWindow),
            error = GUI.Draw(anx(20), any(650), "", bucket.createWindow),
        }

        bucket.uiGUI.error.setColor(220,0,0);

        foreach(item in bucket.uiGUI)
            item.setVisible(true);

        bucket.uiGUI.butt.bind(EventType.Click, function(element) {
            ItemsBuilder.createItemSend();
        });
    }

    createItemSend = function() {
        local instance = bucket.uiGUI.instance.getText();
        local scheme = getItemScheme(instance);
        if(scheme == null)
        {
            bucket.uiGUI.error.setText("Brak schematu o tym instance");
            return;
        }

        local amount = 1;
        try { amount = bucket.uiGUI.amount.draw.getText().tointeger(); }catch(error) {}

        local resistance = -1;
        try { resistance = bucket.uiGUI.resistance.draw.getText().tointeger(); }catch(error) {}

        local repetition = 1;
        try { repetition = bucket.uiGUI.repetition.draw.getText().tointeger(); }catch(error) {}

        local stacked = true;
        stacked = bucket.uiGUI.stacked.getChecked();

        if(stacked == false)
            amount = 1;

        local target = bucket.uiGUI.target.getValue();
        local guid = bucket.uiGUI.guid.getText();
        if(guid == "" || guid == null || guid == "Guid")
            guid = "";

        local itemsCommand = true;
        if(adminPlayer.role == PlayerRole.SupportPlus || adminPlayer.role == PlayerRole.GameMaster) {
            itemsCommand = false;
        }

        if(itemsCommand == false) {
            if(instance.find("ITKE_") != null || instance.find("itke_") != null) {
                for(local i = 0; i < repetition; i++) {
                    local packet = Packet();
                    packet.writeUInt8(item.packetId);
                    packet.writeUInt8(5);
                    packet.writeString(bucket.uiGUI.instance.getText());
                    packet.writeString(bucket.uiGUI.name.getText());
                    packet.writeString(bucket.uiGUI.description.getText());
                    packet.writeString(guid);
                    packet.writeUInt32(amount);
                    packet.writeBool(stacked);
                    packet.writeInt16(resistance);
                    packet.writeInt16(target);
                    packet.send(RELIABLE);
                }
            }
        } else {
            for(local i = 0; i < repetition; i++) {
                local packet = Packet();
                packet.writeUInt8(item.packetId);
                packet.writeUInt8(5);
                packet.writeString(bucket.uiGUI.instance.getText());
                packet.writeString(bucket.uiGUI.name.getText());
                packet.writeString(bucket.uiGUI.description.getText());
                packet.writeString(guid);
                packet.writeUInt32(amount);
                packet.writeBool(stacked);
                packet.writeInt16(resistance);
                packet.writeInt16(target);
                packet.send(RELIABLE);
            }
        }
        foreach(item in bucket.uiGUI) {
            item.setVisible(false);
            item.destroy();
        }

        bucket.uiGUI.clear();
        bucket.ui.clear();

        bucket.ui = {};

        bucket.createWindow.setVisible(false);
    }

    onRender = function() {
        if(!("itemRender" in bucket.ui))
            return;

        bucket.ui.itemRender.rotX = bucket.ui.itemRender.rotX + 2;
        bucket.ui.itemRender.rotZ = bucket.ui.itemRender.rotZ + 1;
    }

    refreshInstance = function(_inpt) {
        if(!("instance" in bucket.uiGUI))
            return;

        if(bucket.uiGUI.instance != _inpt)
            return;

        local instance = bucket.uiGUI.instance.getText();
        local scheme = getItemScheme(instance);
        if(scheme == null)
        {
            bucket.uiGUI.infoDraw.setText("Brak schematu o tym instance");
            bucket.uiGUI.infoDraw.setColor(222,0,0);
            return;
        }

        local itemsCommand = true;
        if(adminPlayer.role == PlayerRole.SupportPlus || adminPlayer.role == PlayerRole.GameMaster) {
            itemsCommand = false;
        }

        if(itemsCommand == false) {
            if(instance.find("ITKE_") != null || instance.find("itke_") != null) {
                bucket.ui.itemRender <- ItemRender(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 200), anx(400), any(400), instance);
                bucket.ui.itemRender.visible = true;

                bucket.uiGUI.infoDraw.setText("Znaleziono");
                bucket.uiGUI.infoDraw.setColor(0,222,0);
                bucket.uiGUI.stacked.setChecked(scheme.stacked);
                bucket.uiGUI.name.setText(scheme.name);
                bucket.uiGUI.description.setText(scheme.description);
                bucket.uiGUI.resistance.setText(scheme.resistance.tostring());
            }
        } else {
            bucket.ui.itemRender <- ItemRender(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 200), anx(400), any(400), instance);
            bucket.ui.itemRender.visible = true;
            bucket.uiGUI.infoDraw.setText("Znaleziono");
            bucket.uiGUI.infoDraw.setColor(0,222,0);
            bucket.uiGUI.stacked.setChecked(scheme.stacked);
            bucket.uiGUI.name.setText(scheme.name);
            bucket.uiGUI.description.setText(scheme.description);
            bucket.uiGUI.resistance.setText(scheme.resistance.tostring());
        }


        if(scheme.stacked == false)
            bucket.uiGUI.amount.setText("1");
    }

    showPlayerList = function() {
        foreach(item in bucket.uiGUI) {
            item.setVisible(false);
            item.destroy();
        }

        bucket.uiGUI.clear();
        bucket.ui.clear();

        bucket.ui = {};

        bucket.playerWindow.setVisible(true);
        bucket.playerItemsWindow.setVisible(false);
        bucket.createWindow.setVisible(false);

        local contentSecondary = [];

        for(local i = 0; i < getMaxSlots(); i ++)
        {
            contentSecondary.push({
                id = i,
                name = "("+i+") "+getPlayerAdminStatistics(i).characterName
            });
        }

        bucket.uiGUI = {
            target = MyGUI.DropDown(anx(Resolution.x - 480), any(Resolution.y/2 - 380), anx(420), any(60), "Gracz", contentSecondary, "Gracz..", heroId),
            butt = GUI.Button(anx(20), any(100), anx(420), any(60), "SG_BUTTON.TGA", "Zobacz", bucket.playerWindow),
        }

        foreach(item in bucket.uiGUI)
            item.setVisible(true);

        bucket.uiGUI.butt.bind(EventType.Click, function(element) {
            local playerId = ItemsBuilder.bucket.uiGUI.target.getValue();
            AdminPacket().sendPlayerItems(playerId);
        });
    }

    showPlayerItems = function(_items) {
        bucket.store = _items;

        foreach(item in bucket.uiGUI) {
            item.setVisible(false);
            item.destroy();
        }

        bucket.uiGUI.clear();
        bucket.ui.clear();

        bucket.ui = {};

        bucket.playerWindow.setVisible(true);
        bucket.playerItemsWindow.setVisible(true);
        bucket.createWindow.setVisible(false);

        local contentSecondary = [];

        for(local i = 0; i < getMaxSlots(); i ++)
        {
            contentSecondary.push({
                id = i,
                name = "("+i+") "+getPlayerAdminStatistics(i).characterName
            });
        }

        itemId = -1;

        bucket.uiGUI = {
            target = MyGUI.DropDown(anx(Resolution.x - 480), any(Resolution.y/2 - 380), anx(420), any(60), "Gracz", contentSecondary, "Gracz..", heroId),
            butt = GUI.Button(anx(20), any(100), anx(420), any(60), "SG_BUTTON.TGA", "Zobacz", bucket.playerWindow),
            name = GUI.Input(anx(20), any(20), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2, bucket.playerItemsWindow),
            description = GUI.Input(anx(20), any(100), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Opis", 2, bucket.playerItemsWindow),
            guid = GUI.Input(anx(20), any(180), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Guid", 2, bucket.playerItemsWindow),
            amount = GUI.Input(anx(20), any(260), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Ilo��", 2, bucket.playerItemsWindow),
            resistance = GUI.Input(anx(20), any(340), anx(420), any(60), "SG_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Odporno��", 2, bucket.playerItemsWindow),
            update = GUI.Button(anx(20), any(420), anx(200), any(60), "SG_BUTTON.TGA", "Aktualizuj", bucket.playerItemsWindow),
            remove = GUI.Button(anx(240), any(420), anx(200), any(60), "SG_BUTTON.TGA", "Usu�", bucket.playerItemsWindow),
            list = MyGUI.SelectList(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(440))
        }

        bucket.uiGUI.list.onClick = function(_itemId) { ItemsBuilder.showPlayerSpecificItem(_itemId); }

        foreach(_itemId, _item in bucket.store)
            bucket.uiGUI.list.addOption(_itemId, _item.name + "-"+_item.amount);

        foreach(item in bucket.uiGUI)
            item.setVisible(true);

        bucket.uiGUI.update.bind(EventType.Click, function(element) {
            ItemsBuilder.updateItemSend();
        });

        bucket.uiGUI.remove.bind(EventType.Click, function(element) {
            ItemsBuilder.deleteItemSend();
        });

        bucket.uiGUI.butt.bind(EventType.Click, function(element) {
            local playerId = ItemsBuilder.bucket.uiGUI.target.getValue();
            AdminPacket().sendPlayerItems(playerId);
        });
    }

    updateItemSend = function() {
        if(itemId == -1 || itemId == null)
            return;

        local amount = 1;
        try { amount = bucket.uiGUI.amount.draw.getText().tointeger(); }catch(error) {}

        local resistance = -1;
        try { resistance = bucket.uiGUI.resistance.draw.getText().tointeger(); }catch(error) {}

        local guid = bucket.uiGUI.guid.getText();
        if(guid == "" || guid == null || guid == "Guid")
            guid = "";

        AdminPacket().updateItem(
            itemId,
            bucket.uiGUI.name.getText(),
            bucket.uiGUI.description.getText(),
            guid,
            amount,
            resistance
        );

        bucket.store[itemId].name = bucket.uiGUI.name.getText();
        bucket.store[itemId].description = bucket.uiGUI.description.getText();
        bucket.store[itemId].guid = guid;
        bucket.store[itemId].amount = amount;
        bucket.store[itemId].resistance = resistance;
    }

    deleteItemSend = function() {
        if(itemId == -1 || itemId == null)
            return;

        bucket.uiGUI.list.setVisible(false);
        bucket.uiGUI.list.destroy();

        bucket.uiGUI.list = MyGUI.SelectList(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 200), anx(600), any(440))
        bucket.uiGUI.list.onClick = function(_itemId) { ItemsBuilder.showPlayerSpecificItem(_itemId); }

        foreach(_itemId, _item in bucket.store)
            bucket.uiGUI.list.addOption(_itemId, _item.name + "-"+_item.amount);

        bucket.uiGUI.list.setVisible(true);

        AdminPacket().deleteItem(itemId);
    }

    showPlayerSpecificItem = function(_itemId) {
        itemId = _itemId;
        local _item = bucket.store[_itemId];

        bucket.ui = {};
        bucket.ui.itemRender <- ItemRender(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 300), anx(400), any(400), _item.instance);
        bucket.ui.itemRender.visible = true;

        bucket.uiGUI.name.setText(_item.name);
        bucket.uiGUI.description.setText(_item.description);
        bucket.uiGUI.guid.setText(_item.guid);
        bucket.uiGUI.amount.setText(_item.amount.tostring());
        bucket.uiGUI.resistance.setText(_item.resistance.tostring());
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        bucket.previewWindow.setVisible(true);

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderItems;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.previewWindow.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        foreach(item in bucket.uiGUI) {
            item.setVisible(false);
            item.destroy();
        }

        bucket.uiGUI.clear();
        bucket.ui.clear();
        bucket.store.clear();

        bucket.playerWindow.setVisible(false);
        bucket.createWindow.setVisible(false);
        bucket.playerItemsWindow.setVisible(false);
    }
}

Bind.addKey(KEY_ESCAPE, function() { ItemsBuilder.hide(); }, PlayerGUI.ServerBuilderItems)

addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderItems)
        ItemsBuilder.onRender();
})


addEventHandler("GUI.onInputInsertLetter", function(inpt, letter) {
    if(ActiveGui == PlayerGUI.ServerBuilderItems)
        ItemsBuilder.refreshInstance(inpt);
});

addEventHandler("GUI.onInputRemoveLetter", function(inpt, letter) {
    if(ActiveGui == PlayerGUI.ServerBuilderItems)
        ItemsBuilder.refreshInstance(inpt);
});

