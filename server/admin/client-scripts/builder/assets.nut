
AssetsBuilder <- {
    bucket = null,

    bucketAsset = null,
    activeKeys = [],
    list = {},

    prepareWindow = function() {
        local result = {};

        result.previewWindow <- GUI.Window(anx(Resolution.x - 400), any(Resolution.y/2 - 400), anx(350), any(780), "BLACK.TGA");
        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600))
        result.addButton <- GUI.Button(anx(40), any(Resolution.y/2 + 350), anx(200), any(60), "SG_BUTTON.TGA", "Dodaj");
        result.removeButton <- GUI.Button(anx(40), any(850), anx(200), any(60), "SG_BUTTON.TGA", "Usu�",result.previewWindow);

        result.addButton.bind(EventType.Click, function(element) {
            AssetsBuilder.bucket.previewWindow.setVisible(false);
            AssetsBuilder.bucket.list.setVisible(false);
            AssetsBuilder.bucket.addButton.setVisible(false);
            AssetsBuilder.bucket.nameInput.setVisible(true);
            AssetsBuilder.bucket.saveButton.setVisible(true);
        });

        result.removeButton.bind(EventType.Click, function(element) {
            local id = AssetsBuilder.bucketAsset.id;
            AdminPacket().deleteAsset(id);
            AssetsBuilder.hide();
        });

        result.internals <- [];
        result.nameInput <- GUI.Input(anx(Resolution.x/2 - 150), any(Resolution.y/2 - 150), anx(300), any(55), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa u�ytkownika", 2);
        result.saveButton <- GUI.Button(anx(Resolution.x/2 - 150), any(Resolution.y/2 - 40), anx(300), any(60), "SG_BUTTON.TGA", "Zapisz");

        result.saveButton.bind(EventType.Click, function(element) {
            AdminPacket().addAsset(AssetsBuilder.bucket.nameInput.getText());
            AssetsBuilder.hide();
        });

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(true);
        bucket.addButton.setVisible(true);

        bucket.list.onClick = function(id) {
            AssetsBuilder.preview(id);
        }

        list.clear();

        //AdminPacket().turnOnInvisibility();
        AdminPacket().getAllAssets();

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderAssets;
    }

    addAsset = function(id, object) {
        list[id] <- object;
        refreshList();

        return list[id];
    }

    refreshList = function() {
        bucket.list.clear();

        foreach(itemId, item in list)
            bucket.list.addOption(itemId, item.name+"("+itemId+")");
    }

    preview = function(id) {
        local object = AssetsBuilder.list[id];

        AssetsBuilder.bucketAsset = object;
        AssetsBuilder.bucket.previewWindow.setVisible(true);

        foreach(internal in bucket.internals) {
            internal.setVisible(false);
            internal.destroy();
        }
        bucket.internals.clear();

        local text = [
            "[#FFFFFF]nazwa [#FF0000]"+bucketAsset.name,
            "[#FFFFFF]instancja [#FF0000]"+bucketAsset.instance,
            "[#FF0000]====================",
            "[#FFFFFF]si�a [#FF0000]"+bucketAsset.str + " [#FFFFFF]zr�czno�� [#FF0000]"+bucketAsset.dex,
            "[#FFFFFF]hp [#FF0000]"+bucketAsset.hp+" [#FFFFFF]mana [#FF0000]"+bucketAsset.mana,
            "[#FFFFFF]1h [#FF0000]"+bucketAsset.oneh+" [#FFFFFF]2h [#FF0000]"+bucketAsset.twoh,
            "[#FFFFFF]�uk [#FF0000]"+bucketAsset.bow+" [#FFFFFF]kusza [#FF0000]"+bucketAsset.cbow,
            "[#FFFFFF]grubo�� [#FF0000]"+bucketAsset.fatness+ "[#FFFFFF]kr�g magiczny [#FF0000]"+bucketAsset.magicLvl,
            "[#FFFFFF]inteligencja [#FF0000]"+bucketAsset.inteligence+ "[#FF0000]====================",
        ]

        foreach(item in bucketAsset.inventory)
            text.push("[#FFFFFF]kod [#FF0000]"+item.instance+ " [#FFFFFF]nazwa [#FF0000]"+item.amount);

        local str = "";
        foreach(_text in text)
            str += _text+"\n";

        bucket.internals.append(GUI.Draw(anx(Resolution.x - 370), any(Resolution.y/2 - 370), str));

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.nameInput.setVisible(false);
        bucket.saveButton.setVisible(false);

        //AdminPacket().turnOffInvisibility();

        foreach(internal in bucket.internals) {
            internal.setVisible(false);
            internal.destroy();
        }
        bucket.internals.clear();

        BaseGUI.hide();
        ActiveGui = null;
    }
}

Bind.addKey(KEY_ESCAPE, function() { AssetsBuilder.hide(); }, PlayerGUI.ServerBuilderAssets)
