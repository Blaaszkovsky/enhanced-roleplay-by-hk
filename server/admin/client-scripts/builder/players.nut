
PlayersBuilder <- {
    bucket = null,
    bucketPlayer = null,
    activeTab = -1,

    startPosition = null,
    list = {},

    prepareWindow = function() {
        local result = {};

        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600));

        result.previewWindow <- GUI.Window(anx(Resolution.x - 400), any(100), anx(370), any(680), "SG_BOX.TGA");
        result.settingsButton <- GUI.Button(anx(10), any(10), anx(110), any(60), "SG_BUTTON.TGA", "Og�lne", result.previewWindow);
        result.characterButton <- GUI.Button(anx(120), any(10), anx(110), any(60), "SG_BUTTON.TGA", "Przedmioty", result.previewWindow);
        result.relationsButton <- GUI.Button(anx(230), any(10), anx(110), any(60), "SG_BUTTON.TGA", "Logi", result.previewWindow);

        result.settingsButton.bind(EventType.Click, function(element) {PlayersBuilder.onSwitch(0);});
        result.characterButton.bind(EventType.Click, function(element) {PlayersBuilder.onSwitch(1);});
        result.relationsButton.bind(EventType.Click, function(element) {PlayersBuilder.onSwitch(2);});

        result.internals <- [];

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        list.clear();
        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(true);

        startPosition = getPlayerPosition(heroId);

        bucket.list.onClick = function(id) {
            PlayersBuilder.preview(id);
        }

        //AdminPacket().turnOnInvisibility();
        AdminPacket().getAllPlayers();

        setTimer(function() {
            PlayersBuilder.refreshList();
        }, 2500, 3);

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderPlayer;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(false);

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);
        //AdminPacket().turnOffInvisibility();

        BaseGUI.hide();
        destroyInternals();
        ActiveGui = null;
    }

    function destroyInternals() {
        foreach(internal in bucket.internals) {
            internal.setVisible(false);
            internal.destroy();
        }
        bucket.internals.clear();
    }

    preview = function(id) {
        local object = PlayersBuilder.list[id];
        Camera.setBeforePosFromTop(400, object.position.x, object.position.y, object.position.z);
        setPlayerPosition(heroId, object.position.x + 120, object.position.y + 50, object.position.z)

        PlayersBuilder.bucketPlayer = object;

        PlayersBuilder.bucket.previewWindow.setVisible(true);
        PlayersBuilder.onSwitch(0);
    }

    refreshList = function() {
        bucket.list.clear();

        foreach(itemId, item in list)
            bucket.list.addOption(itemId, itemId + "."+ item.name+ "("+item.account+") ");
    }

    addPlayer = function(id, object, refresh = true) {
        list[id] <- object;

        object.id <- id;

        if(refresh)
            refreshList();

        return list[id];
    }

    showSettingsPage = function() {
        local startPosition = {x = anx(Resolution.x - 400), y = any(100) }

        local text = [
            "[#FFFFFF]nazwa [#FF0000]"+bucketPlayer.name,
            "[#FFFFFF]konto [#FF0000]"+bucketPlayer.account,
            "[#FF0000]====================",
            "[#FFFFFF]si�a [#FF0000]"+bucketPlayer.str + " [#FFFFFF]zr�czno�� [#FF0000]"+bucketPlayer.dex,
            "[#FFFFFF]hp [#FF0000]"+bucketPlayer.hp+"/"+bucketPlayer.hpMax+" [#FFFFFF]mana [#FF0000]"+bucketPlayer.mana+"/"+bucketPlayer.maxMana,
            "[#FFFFFF]1h [#FF0000]"+bucketPlayer.weapon[0]+" [#FFFFFF]2h [#FF0000]"+bucketPlayer.weapon[1],
            "[#FFFFFF]�uk [#FF0000]"+bucketPlayer.weapon[2]+" [#FFFFFF]kusza [#FF0000]"+bucketPlayer.weapon[3],
            "[#FFFFFF]grubo�� [#FF0000]"+bucketPlayer.fatness+" [#FFFFFF]g�os [#FF0000]"+bucketPlayer.speech,
            "[#FFFFFF]stamina [#FF0000]"+bucketPlayer.stamina+ " [#FFFFFF]inteligencja [#FF0000]"+bucketPlayer.inteligence,
            "[#FFFFFF]kr�g magiczny [#FF0000]"+bucketPlayer.magicLvl,
            "[#FFFFFF]punkty nauki [#FF0000]"+bucketPlayer.learnPoints,
            "[#FF0000]====================",
        ]

        for(local i = 0; i < bucketPlayer.skills.len()-1; i = i + 2)
            text.push("[#FFFFFF]"+Config["PlayerSkill"][i]+" [#FF0000]"+bucketPlayer.skills[i]+ " [#FFFFFF]"+Config["PlayerSkill"][i+1]+" [#FF0000]"+bucketPlayer.skills[i+1]);

        local str = "";
        foreach(_text in text)
            str += _text+"\n";

        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(110), str));

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    showRelationPage = function() {
        local startPosition = {x = anx(Resolution.x - 400), y = any(100) }

        bucket.internals.append(GUI.List(startPosition.x + anx(10),startPosition.y + any(110), anx(350), any(500), "DLG_CONVERSATION.TGA", "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA"));

        foreach(_log in bucketPlayer.logs)
            bucket.internals[0].addRow("[#FF0000]"+_log.dateObject + " - [#FFFFFF]"+_log.content)

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    showStatisticPage = function() {
        local startPosition = {x = anx(Resolution.x - 400), y = any(100) }

        bucket.internals.append(GUI.List(startPosition.x + anx(10),startPosition.y + any(110), anx(350), any(250), "DLG_CONVERSATION.TGA", "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA"));
        bucket.internals.append(GUI.List(startPosition.x + anx(10),startPosition.y + any(370), anx(350), any(250), "DLG_CONVERSATION.TGA", "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA"));

        foreach(_item in bucketPlayer.inventory)
            bucket.internals[0].addRow("[#FF0000]"+_item.instance + " - [#FFFFFF]"+_item.amount)

        foreach(_effect in bucketPlayer.effects)
            bucket.internals[1].addRow("[#FF0000]"+_effect.effectId + " - [#FFFFFF]"+_effect.refreshTime)

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    onSwitch = function(tab) {
        destroyInternals();

        activeTab = tab;
        if(1 == activeTab)
            showStatisticPage();
        else if(2 == activeTab)
            showRelationPage();
        else
            showSettingsPage();
    }

    onRender = function() {
        if(bucketPlayer == null)
            return;

        local heroPos = getPlayerPosition(bucketPlayer.id);
        Camera.setBeforePosFromTop(400, heroPos.x, heroPos.y, heroPos.z);
        setPlayerPosition(heroId, heroPos.x + 200, heroPos.y, heroPos.z);
    }
}

Bind.addKey(KEY_ESCAPE, function() { PlayersBuilder.hide(); }, PlayerGUI.ServerBuilderPlayer)

addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderBots)
        PlayersBuilder.onRender();
})
