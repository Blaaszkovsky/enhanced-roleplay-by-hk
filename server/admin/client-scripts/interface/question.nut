

QuestionsGUI <- {
    questions = null,
    bucket = {},
    activeQuestionId = null,
    lastAnswer = 0,

    show = function(_questions) {
        BaseGUI.show();
        ActiveGui = PlayerGUI.Questions;

        questions = _questions;
        BlackScreen(1000);
        //AdminPacket().turnOnInvisibility();

        preview(0);
    }

    hide = function()  {
        BaseGUI.hide();
        ActiveGui = null;

        foreach(item in bucket) {
            if(item instanceof GUI.Button) {
                item.setVisible(false);
                item.destroy();
            }
        }
        bucket.clear();

        //AdminPacket().turnOffInvisibility();
    }

    preview = function(id) {
        foreach(item in bucket) {
            if(item instanceof GUI.Button) {
                item.setVisible(false);
                item.destroy();
            }
        }
        bucket.clear();

        activeQuestionId = id;
        local obj = questions[id];
        bucket.draw <- Draw(anx(30), 2000, obj.question);
        bucket.draw.visible = true;
        lastAnswer = getTickCount() + 1000;
        foreach(_ind, answer in obj.answers) {
            local button = GUI.Button(anx(30),3200 + any(100 * _ind),anx(Resolution.x - 60), any(85), "DLG_CONVERSATION.TGA", "");
            bucket["draw_"+_ind] <- Draw(anx(60),3200 + any(100 * _ind) + any(20), answer);
            bucket["draw_"+_ind].visible = true;
            button.attribute = _ind;
            button.bind(EventType.Click, function(element) {
                QuestionsGUI.activeAnswer(element.attribute);
            });
            bucket["button_"+_ind] <- button;
            button.setVisible(true);
            bucket["draw_"+_ind].top();
        }
    }

    activeAnswer = function(answerId) {
        if(lastAnswer > getTickCount())
            return;

        AdminPacket().sendAnswer(questions[activeQuestionId].id, answerId);

        questions.remove(0);

        if(questions.len() == 0)
            hide();
        else
            preview(0);
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Questions)
            hide();
    }
}

addEventHandler("onForceCloseGUI", QuestionsGUI.onForceCloseGUI.bindenv(QuestionsGUI));
