
addCommandAdmin("playani", function (params) {
    local args = sscanf("s", params);
    if (!args)
    {
        Chat.addMessage(ChatType.ADMIN, 255, 255, 255, "/playani <animacja>")
        return;
    }

    setTimer(function () {
        playAni(heroId, args[0]);
    }, 500, 1);
}, "Komenda wykonywuj�ca dan� animacje /playani <animacja>.");

addCommandAdmin("freecam", function (params) { 
    setTimer(function(){enableFreeCam(!isFreeCamEnabled())}, 1000, 1);
}, "Komenda wykonywuj�ca dan� animacje /freecam", PlayerRole.Patreon);

