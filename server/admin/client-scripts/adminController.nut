
class AdminController
{
    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case AdminPackets.GetItems:
                local amount = packet.readUInt16();
                local items = {};

                for(local i = 0; i < amount; i++) {
                    local itemId = packet.readUInt32();
                    local instance = packet.readString();
                    local name = packet.readString();
                    local description = packet.readString();
                    local guid = packet.readString();
                    local amount = packet.readInt32();
                    local resistance = packet.readInt16();
                    local stacked = packet.readBool();
                    local equipped = packet.readBool();

                    items[itemId] <- {
                        itemId = itemId,
                        instance = instance,
                        name = name,
                        description = description,
                        guid = guid,
                        amount = amount,
                        resistance = resistance,
                        stacked = stacked,
                        equipped = equipped
                    }
                }

                ItemsBuilder.showPlayerItems(items);
            break;
            case AdminPackets.PlayerRegister:
                local playerId = packet.readInt16();
                local characterName = packet.readString();
                local accountName = packet.readString();
                local characterId = packet.readInt16();

                setPlayerAdminStatistics(playerId, {
                    account = accountName,
                    realId = characterId,
                    characterName = characterName,
                });
            break;
            case AdminPackets.Role:
                adminPlayer.setRole(packet.readInt16());
            break;
            case AdminPackets.GetBots:
                local botId = packet.readInt16();
                local effectsLen = packet.readInt16();
                local effects = [];

                for(local i = 0; i < effectsLen; i ++)
                    effects.append({effectId = packet.readInt16(), refreshTime = packet.readInt16()})

                BotsBuilder.addBot(botId, {
                    isTemporary = packet.readBool(),
                    isSaved = packet.readBool(),
                    group = packet.readString(),
                    name = packet.readString(),
                    instance = packet.readString(),
                    position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()},
                    angle = packet.readInt16(),
                    str = packet.readInt16(),
                    dex = packet.readInt16(),
                    hp = packet.readInt32(),
                    hpMax = packet.readInt32(),
                    scale = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()},
                    animation = packet.readString(),
                    weapon = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()],
                    respawnTime = packet.readInt16(),
                    melee = packet.readInt16(),
                    armor = packet.readInt16(),
                    ranged = packet.readInt16(),
                    helmet = packet.readInt16(),
                    shield = packet.readInt16(),
                    magic = packet.readInt16(),
                    bodyModel = packet.readString(),
                    bodyTxt = packet.readInt16(),
                    headModel = packet.readString(),
                    headTxt = packet.readInt16(),
                    world = packet.readString(),
                    effects = effects,
                });
            break;
            case AdminPackets.GetPlayers:
                local playerId = packet.readInt16();
                local object = PlayersBuilder.addPlayer(playerId, {
                    rId = packet.readInt16(),
                    name = packet.readString(),
                    account = packet.readString(),
                    isPenalty = packet.readBool(),
                    position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()},
                    angle = packet.readInt16(),
                    fatness = packet.readInt16(),
                    speech = packet.readInt16(),
                    walking = packet.readInt16(),
                    stamina = packet.readFloat(),
                    hunger = packet.readFloat(),
                    respawnLeft = packet.readInt16(),
                    str = packet.readInt16(),
                    dex = packet.readInt16(),
                    inteligence = packet.readInt16(),
                    hp = packet.readInt32(),
                    hpMax = packet.readInt32(),
                    mana = packet.readInt32(),
                    maxMana = packet.readInt32(),
                    magicLvl = packet.readInt16(),
                    learnPoints = packet.readInt16(),
                    skills = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()],
                    weapon = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()],
                    inventory = [],
                    effects = [],
                    logs = [],
                }, false);

                local inventoryLen = packet.readInt16();
                for(local i = 0; i < inventoryLen; i ++)
                    object.inventory.push({instance = packet.readString(), amount = packet.readInt32()});

                local effectsLen = packet.readInt16();
                for(local i = 0; i < effectsLen; i ++)
                    object.effects.push({effectId = packet.readInt16(), refreshTime = packet.readInt16()});

                local logsLen = packet.readInt16();
                for(local i = 0; i < logsLen; i ++)
                    object.logs.push({content = packet.readString(), dateObject = packet.readString()});
            break;
            case AdminPackets.GetAssets:
                local assetId = packet.readInt16();
                local object = AssetsBuilder.addAsset(assetId, {
                    id = assetId,
                    name = packet.readString(),
                    instance = packet.readString(),
                    str = packet.readInt16(),
                    dex = packet.readInt16(),
                    hp = packet.readInt32(),
                    mana = packet.readInt32(),
                    scale = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()},
                    oneh = packet.readInt16(),
                    twoh = packet.readInt16(),
                    bow = packet.readInt16(),
                    cbow = packet.readInt16(),
                    fatness = packet.readInt16(),
                    inteligence = packet.readInt16(),
                    magicLvl = packet.readInt16(),
                    bodyModel = packet.readString(),
                    bodyTxt = packet.readInt16(),
                    headModel = packet.readString(),
                    headTxt = packet.readInt16(),
                    inventory = [],
                    effects = [],
                    isTemporary = true,
                });

                local effectsLen = packet.readInt16();
                for(local i = 0; i < effectsLen; i ++)
                    object.effects.push({effectId = packet.readInt16(), refreshTime = packet.readInt16()});

                local inventoryLen = packet.readInt16();
                for(local i = 0; i < inventoryLen; i ++)
                    object.inventory.push({instance = packet.readString(), amount = packet.readInt32()});
            break;
            case AdminPackets.Question:
                local questionsAmount = packet.readInt16();
                local questions = [];
                for(local i = 0; i < questionsAmount; i++) {
                    local obj = {
                        id = packet.readInt16(),
                        question = packet.readString(),
                        answers = [],
                    }

                    local answersAmount = packet.readInt16();
                    for(local i = 0; i < answersAmount; i ++)
                        obj.answers.push(packet.readString());

                    questions.push(obj);
                }

                QuestionsGUI.show(questions);
            break;
            case AdminPackets.ExitGame:
                exitGame();
            break;
            case AdminPackets.GetFractionMembers:
                local members = packet.readInt16();
                for(local i = 0; i < members; i++)
                    ServerBuilderFractions.addMember(packet.readInt32(), packet.readInt16(), packet.readString());
            break;
            case AdminPackets.ConfirmRole:
                adminPlayer.active = packet.readBool();
            break;
        }
    }

    static function onCommand(cmd, param) {
        if(!adminPlayer.canCommand())
            return;

        if(cmd == "quit" || cmd == "q")
            exitGame();

        if(cmd == "localdraws")
            enableLocalDrawsSystem(!getEnableLocalDrawsSystem());


        // if(cmd == "testx1") {
        //     Sky.weather = WEATHER_SNOW;
        //     Sky.renderLightning = true;
        //     Sky.windScale = 1.5;
        //     Sky.setFogColor(200, 0, 0, 200);
        //     Sky.setCloudsColor(200, 0, 0);
        //     Sky.setLightingColor(200, 0, 0);
        //     Sky.setPlanetSize(PLANET_SUN, 20.0);
        //     Sky.setPlanetColor(PLANET_SUN, 255, 0, 0, 200);
        // }

        // if(cmd == "light") {
        //     Sky.weatherWeight = 0.7;
        //     Sky.windScale = true;
        //     Sky.renderLightning = true;
        // }

        // if(cmd == "darksky") {
        //     Sky.darkSky = true;
        // }
    }
}

addEventHandler("onCommand", AdminController.onCommand.bindenv(AdminController));
RegisterPacketReceiver(Packets.Admin, AdminController.onPacket.bindenv(AdminController));
