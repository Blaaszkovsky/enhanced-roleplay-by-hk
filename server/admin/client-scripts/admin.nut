local adminPlayersStatistics = {};

for(local i = 0; i < getMaxSlots(); i ++)
{
    adminPlayersStatistics[i] <- {
        account = "Konto",
        realId = -1,
        characterName = "Name"
    }
}

function getPlayerAdminStatistics(playerId) {
    return adminPlayersStatistics[playerId];
}

function setPlayerAdminStatistics(playerId, obj) {
    return adminPlayersStatistics[playerId] = obj;
}

local commands = {};

function addCommandAdmin(command, callback, description = "", role = PlayerRole.Admin) {
    commands[command] <- {callback = callback, description = description, role = role};
}

addEventHandler("onCommand", function (command, params) {
    if(adminPlayer.role == PlayerRole.User)
        return;

    if(command in commands)
        if(adminPlayer.role >= commands[command].role)
            commands[command].callback.call(this, params)
})

function getAllAdminCommands() {
    return commands;
}

addEventHandler("onKey", function(key) {
    if(adminPlayer.canJump() == false)
        return;

    if (chatInputIsOpen())
        return

    if (isConsoleOpen())
        return

    if(ActiveGui != null)
        return;

    if(key == KEY_K)
    {
        local position = getPlayerPosition(heroId);
        local angle = getPlayerAngle(heroId);
        position.x = position.x + (sin(angle * 3.14 / 180.0) * 300);
        position.z = position.z + (cos(angle * 3.14 / 180.0) * 300);
        position.y = position.y + 10;
        setPlayerPosition(heroId, position.x, position.y, position.z);
    }

    if(key == KEY_F8)
    {
        stopAni(heroId);
        setPlayerHealth(heroId, getPlayerMaxHealth(heroId))
        local position = getPlayerPosition(heroId);
        local angle = getPlayerAngle(heroId);
        position.x = position.x + (sin(angle * 3.14 / 180.0) * 500);
        position.z = position.z + (cos(angle * 3.14 / 180.0) * 500);
        position.y = position.y + 200;
        setPlayerPosition(heroId, position.x, position.y, position.z);
    }
})

// addEventHandler ("onInit", function () {
//     local tab = {};
//     tab[KEY_ESCAPE] <- "ESC";
//     tab[KEY_1] <- "1";
//     tab[KEY_2] <- "2";
//     tab[KEY_3] <- "3";
//     tab[KEY_4] <- "4";
//     tab[KEY_5] <- "5";
//     tab[KEY_6] <- "6";
//     tab[KEY_7] <- "7";
//     tab[KEY_8] <- "8";
//     tab[KEY_9] <- "9";
//     tab[KEY_0] <- "0";
//     tab[KEY_MINUS] <- "-";
//     tab[KEY_EQUALS] <- "=";
//     tab[KEY_BACK] <- "BACKSPACE";
//     tab[KEY_TAB] <- "TAB";
//     tab[KEY_Q] <- "Q";
//     tab[KEY_W] <- "W";
//     tab[KEY_E] <- "E";
//     tab[KEY_R] <- "R";
//     tab[KEY_T] <- "T";
//     tab[KEY_Y] <- "Y";
//     tab[KEY_U] <- "U";
//     tab[KEY_I] <- "I";
//     tab[KEY_O] <- "O";
//     tab[KEY_P] <- "P";
//     tab[KEY_LBRACKET] <- "[";
//     tab[KEY_RBRACKET] <- "]";
//     tab[KEY_RETURN] <- "ENTER";
//     tab[KEY_LCONTROL] <- "L_CTRL";
//     tab[KEY_A] <- "A";
//     tab[KEY_S] <- "S";
//     tab[KEY_D] <- "D";
//     tab[KEY_F] <- "F";
//     tab[KEY_G] <- "G";
//     tab[KEY_H] <- "H";
//     tab[KEY_J] <- "J";
//     tab[KEY_K] <- "K";
//     tab[KEY_L] <- "L";
//     tab[KEY_SEMICOLON] <- ";";
//     tab[KEY_APOSTROPHE] <- "'";
//     tab[KEY_TILDE] <- "~";
//     tab[KEY_LSHIFT] <- "L_SHIFT";
//     tab[KEY_BACKSLASH] <- "\\";
//     tab[KEY_Z] <- "Z";
//     tab[KEY_X] <- "X";
//     tab[KEY_C] <- "C";
//     tab[KEY_V] <- "V";
//     tab[KEY_B] <- "B";
//     tab[KEY_N] <- "N";
//     tab[KEY_M] <- "M";
//     tab[KEY_COMMA] <- "'";
//     tab[KEY_PERIOD] <- ".";
//     tab[KEY_SLASH] <- "/";
//     tab[KEY_RSHIFT] <- "R_SHIFT";
//     tab[KEY_MULTIPLY] <- "*";
//     tab[KEY_LMENU] <- "L_ALT";
//     tab[KEY_SPACE] <- "SPACE";
//     tab[KEY_CAPITAL] <- "CAPSLOCK";
//     tab[KEY_F1] <- "F1";
//     tab[KEY_F2] <- "F2";
//     tab[KEY_F3] <- "F3";
//     tab[KEY_F4] <- "F4";
//     tab[KEY_F5] <- "F5";
//     tab[KEY_F6] <- "F6";
//     tab[KEY_F7] <- "F7";
//     tab[KEY_F8] <- "F8";
//     tab[KEY_F9] <- "F9";
//     tab[KEY_F10] <- "F10";
//     tab[KEY_NUMLOCK] <- "NUMLOCK";
//     tab[KEY_SCROLL] <- "SCROLLLOCK";
//     tab[KEY_NUMPAD7] <- "NUMPAD7";
//     tab[KEY_NUMPAD8] <- "NUMPAD8";
//     tab[KEY_NUMPAD9] <- "NUMPAD9";
//     tab[KEY_SUBTRACT] <- "-";
//     tab[KEY_NUMPAD4] <- "NUMPAD4";
//     tab[KEY_NUMPAD5] <- "NUMPAD5";
//     tab[KEY_NUMPAD6] <- "NUMPAD6";
//     tab[KEY_ADD] <- "+";
//     tab[KEY_NUMPAD1] <- "NUMPAD1";
//     tab[KEY_NUMPAD2] <- "NUMPAD2";
//     tab[KEY_NUMPAD3] <- "NUMPAD3";
//     tab[KEY_NUMPAD0] <- "NUMPAD0";
//     tab[KEY_DECIMAL] <- "NUM .";
//     tab[KEY_F11] <- "F11";
//     tab[KEY_F12] <- "F12";
//     tab[KEY_NUMPADENTER] <- "NUM ENTER";
//     tab[KEY_RCONTROL] <- "R_CTRL";
//     tab[KEY_DIVIDE] <- "/";
//     tab[KEY_RMENU] <- "R_ALT";
//     tab[KEY_PAUSE] <- "Pause/Break";
//     tab[KEY_HOME] <- "Home";
//     tab[KEY_UP] <- "ARROW UP";
//     tab[KEY_PRIOR] <- "Page Up";
//     tab[KEY_LEFT] <- "ARROW LEFT";
//     tab[KEY_RIGHT] <- "ARROW RIGHT";
//     tab[KEY_END] <- "END";
//     tab[KEY_DOWN] <- "ARROW DOWN";
//     tab[KEY_NEXT] <- "Page Down";
//     tab[KEY_INSERT] <- "INSERT";
//     tab[KEY_DELETE] <- "DELETE";
//     tab[KEY_LWIN] <- "L_WINDOWS";
//     tab[KEY_LWIN] <- "R_WINDOWS";
//     tab[KEY_POWER] <- "POWER";
//     tab[KEY_SLEEP] <- "SLEEP";
//     tab[KEY_WAKE] <- "WAKE";

//     local _tab = [];

//     foreach(keyId, val in tab) {
//         _tab.push(keyId +" = "+ val);
//     }

//     AdminPacket().saveToLogTable(_tab);
// });