
class ac.Packet {
    packet = null;

    constructor() {
        packet = ExtendedPacket(Packets.AntyCheat);
    }

    function itemsChecksum(amount) {
        packet.writeUInt8(AntyCheatPackets.ItemsChecksum)
        packet.writeInt16(amount);
        packet.send(RELIABLE);
    }

    function health(amount) {
        packet.writeUInt8(AntyCheatPackets.Health)
        packet.writeInt16(amount);
        packet.send(RELIABLE);
    }

    function stamina(amount) {
        packet.writeUInt8(AntyCheatPackets.Stamina)
        packet.writeFloat(amount);
        packet.send(RELIABLE);
    }
}