
class ac.StaminaModule extends ac.Module {
    function onTenSeconds() {
        local stamina = getPlayerStamina(heroId);
        ac.Packet().stamina(stamina);
    }
}