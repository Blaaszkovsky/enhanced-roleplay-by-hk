
class ac.Module {
    type = null;
    value = null;

    constructor(_type) {
        type = _type;
        value = "";
    }

    function getValue() { return value; }
    function onSecond() {}
    function onTenSeconds() {}
}