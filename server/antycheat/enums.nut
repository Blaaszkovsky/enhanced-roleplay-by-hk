ac <- {};

enum AntyCheatPackets {
    ItemsChecksum,
    StatisticsChecksum,
    Health,
    Stamina,
    Teleportation,
}

enum AntyCheatModule {
    Stamina,
    Health,
    Teleportation,
    StatisticsChecksum,
    ItemsChecksum,
}