
class ac.Controller {
    static objects = {};

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            objects[i] <- ac.Object(i);
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ) {
            case AntyCheatPackets.ItemsChecksum:
                objects[playerId].checkItems(packet.readInt16());
            break;
            case AntyCheatPackets.Health:
                objects[playerId].checkHealth(packet.readInt16());
            break;
            case AntyCheatPackets.Stamina:
                objects[playerId].checkStamina(packet.readFloat());
            break;
        }
    }

    static function onPlayerChangeInventory(playerId) {
        objects[playerId].flushItemsCache();
    }
}

addEventHandler("onInit", ac.Controller.onInit.bindenv(ac.Controller));
addEventHandler("onPlayerChangeInventory", ac.Controller.onPlayerChangeInventory.bindenv(ac.Controller));
RegisterPacketReceiver(Packets.AntyCheat, ac.Controller.onPacket.bindenv(ac.Controller));