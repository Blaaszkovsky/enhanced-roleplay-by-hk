
class MinePacket
{
    packet = null;
    object = null;

    constructor(mineObject)
    {
        packet = ExtendedPacket(Packets.Mine);
        object = mineObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(MinePackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.scheme);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeInt16(object.slots);
        packet.writeString(object.world)

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function deleteMine() {
        packet.writeUInt8(MinePackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}