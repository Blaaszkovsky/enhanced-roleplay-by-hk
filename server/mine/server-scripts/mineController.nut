
class MineController
{
    static mines = {};

    static function onTryToHarvestMine(playerId, id) {
        if(!(id != mines))
            return;

        local obj = mines[id];
        local scheme = obj.getScheme();

        if(obj.slots <= 0) {
            addPlayerNotification(playerId, "Tu niema nic!");
            return;
        }

        if(getPlayerStamina(playerId) < 15.0) {
            addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
            return;
        }

        setPlayerStamina(playerId, getPlayerStamina(playerId) - 15.0);

        obj.slots -= 1;
        local outcome = scheme.outcome;

        local possibilitiesArray = [];
        foreach(index, _item in outcome) {
            if(getPlayerSkill(playerId, PlayerSkill.Miner) == _item.skill && _item.chance != -1) {
                for(local i = 0; i < _item.chance; i ++) {
                    possibilitiesArray.push({instance = _item.instance, amount = _item.amount});
                }
            }
        }

        local givedItem = false;
        if(possibilitiesArray.len() != 0) {
            local _item = possibilitiesArray[irand(possibilitiesArray.len() - 1)];
            giveItem(playerId, _item.instance, _item.amount);
            if(_item.amount == 0)
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Nie uda�o si� wykopa� z��.");
            else
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Uda�o si� uzyska� "+getItemName(_item.instance)+" w ilo�ci "+_item.amount+".");

            addPlayerLog(playerId, "Udalo sie uzyskac z kopania w kopalni "+_item.instance+" w ilosci "+_item.amount);
            givedItem = true;
        }

        if(givedItem == false) {
            foreach(_item in outcome) {
                if(_item.chance == -1) {
                    giveItem(playerId, _item.instance, _item.amount);
                    sendMessageToPlayer(playerId, 255, 255, 255, ChatType.IC+"Uda�o si� uzyska� "+getItemName(_item.instance)+" w ilo�ci "+_item.amount+".");
                    addPlayerLog(playerId, "Udalo sie uzyskac z kopania w kopalni "+_item.instance+" w ilosci "+_item.amount);
                    givedItem = true;
                    break;
                }
            }
        }
        local pObject = getPlayer(playerId);
        foreach(_item in pObject.inventory.getItems()) {
            if(_item.instance == "ITMW_SDRP_2H_OTH_06" || _item.instance == "itmw_SDRP2_2h_oth_03") {
                if(_item.resistance > 0) {
                    _item.resistance = _item.resistance - 1;
                    _item.sendUpdate(playerId);
                    break;
                }
            }
        }

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Mine);
        packet.writeUInt8(MinePackets.Update);
        packet.writeInt16(1);
        packet.writeInt32(obj.id);
        packet.writeInt16(obj.slots);
        packet.sendToAll(RELIABLE);
    }

    static function onInit() {
        local minesInDatabase = Query().select().from(Mine.MineTable).all();

        foreach(minesObject in minesInDatabase)
        {
            local newMine = Mine(minesObject.id.tointeger())

            newMine.scheme = minesObject.scheme;
            newMine.slots = minesObject.slots.tointeger();
            newMine.rotation = {x = minesObject.rotationX.tofloat(), y = minesObject.rotationY.tofloat(), z = minesObject.rotationZ.tofloat()}
            newMine.position = {x = minesObject.x.tofloat(), y = minesObject.y.tofloat(), z = minesObject.z.tofloat()}
            newMine.world = minesObject.world

            mines[newMine.id] <- newMine;
        }

        print("We have " + mines.len() + " existing mines.");
    }

    static function onPlayerJoin(playerId) {
        foreach(mine in mines)
            MinePacket(mine).create(playerId);
    }

    static function onSecond() {
        local idsOfMines = [];

        foreach(mineId, mine in mines)
        {
            if(mine.onSecond())
                idsOfMines.push({id = mineId, slots = mine.slots});
        }

        if(idsOfMines.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Mine);
        packet.writeUInt8(MinePackets.Update);
        packet.writeInt16(idsOfMines.len());
        foreach(obj in idsOfMines) {
            packet.writeInt32(obj.id);
            packet.writeInt16(obj.slots);
        }
        packet.sendToAll(RELIABLE);
    }

    function getNearMine(playerId) {
        foreach(mineId, mine in mines)
            if(getPositionDifference(pos, mine.position) < 300)
                return mineId;

        return null;
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case MinePackets.Slots:
                onTryToHarvestMine(playerId, packet.readInt32());
            break;
            case MinePackets.Create:
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();

                Query().insertInto(Mine.MineTable, ["x", "y", "z", "rotationX", "rotationY", "rotationZ", "slots", "scheme"], [
                    positionX, positionY, positionZ, rotationX, rotationY, rotationZ, Config["MineSystem"][scheme].slots, "'"+scheme+"'"
                ]).execute();

                local minesObject = Query().select().from(Mine.MineTable).orderBy("id DESC").one();
                if(minesObject.scheme != scheme)
                    return;

                local newMine = Mine(minesObject.id.tointeger())

                newMine.scheme = minesObject.scheme;
                newMine.slots = minesObject.slots.tointeger();
                newMine.rotation = {x = minesObject.rotationX.tofloat(), y = minesObject.rotationY.tofloat(), z = minesObject.rotationZ.tofloat()}
                newMine.position = {x = minesObject.x.tofloat(), y = minesObject.y.tofloat(), z = minesObject.z.tofloat()}

                newMine.world = getPlayerWorld(playerId)

                mines[newMine.id] <- newMine;
                MinePacket(newMine).create(-1);
            break;
            case MinePackets.Delete:
                local mineId = packet.readInt16();
                if(!(mineId in mines))
                    return;

                MinePacket(mines[mineId]).deleteMine();
                mines.rawdelete(mineId);
                Query().deleteFrom(Mine.MineTable).where(["id = "+mineId]).execute();
            break;
        }
    }
}

addEventHandler("onInit", MineController.onInit.bindenv(MineController));
addEventHandler("onPlayerJoin", MineController.onPlayerJoin.bindenv(MineController))
addEventHandler("onSecond", MineController.onSecond.bindenv(MineController))
RegisterPacketReceiver(Packets.Mine, MineController.onPacket.bindenv(MineController));
