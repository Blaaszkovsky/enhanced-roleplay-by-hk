
class MinePacket
{
    packet = null;
    object = null;

    constructor(mineObject)
    {
        packet = ExtendedPacket(Packets.Mine);
        object = mineObject;
    }

    function deleteMine() {
        packet.writeUInt8(MinePackets.Delete)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function takeMine() {
        packet.writeUInt8(MinePackets.Slots)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }
}