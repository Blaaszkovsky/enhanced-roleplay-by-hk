
class MineController
{
    static mines = {};

    static function interactWithMine(_mineId) {
        if (eggInstance != null)
            return

        local pos = getPlayerPosition(heroId);
        local mineId = getNearMine(pos);
        if(mineId == null || mineId != _mineId)
            return;

        if(hasItemThatIsNotDestroyed("ITMW_SDRP_2H_OTH_06") == false && hasItemThatIsNotDestroyed("itmw_SDRP2_2h_oth_03") == false) {
            addPlayerNotification(heroId, "Nie masz kilofu.");
            return;
        }

        // if(!((getPlayerMeleeWeapon(heroId) == Items.id("ITMW_2H_AXE_L_01") || getPlayerMeleeWeapon(heroId) == Items.id("ITMW_SDRP_2H_OTH_06")) && getPlayerWeaponMode(heroId) == WEAPONMODE_2HS)) {
        //     addPlayerNotification(heroId, "Wyci�gnij kilof.");
        //     return;
        // }

        local mine = mines[mineId];
        if(mine.slots <= 0) {
            addPlayerNotification(heroId, "Brak zasobu.");
            return;
        }

        local eggTimer = EggTimer(5000);
        BaseGUI.show();
        eggTimer.onEnd = function(eleobjectment) {
            BaseGUI.hide();
        }

        unequipMeleeWeapon(heroId);
        MinePacket(mine).takeMine();
    }

    static function getNearMine(pos)
    {
        foreach(mineId, mine in mines)
            if(getPositionDifference(pos, mine.position) < 300)
                return mineId;

        return null;
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case MinePackets.Create:
                local id = packet.readInt32();
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();
                local slots = packet.readInt16();
                local world = packet.readString();

                local obj = Mine(id);
                obj.scheme = scheme;
                obj.position = {x = positionX, y = positionY, z = positionZ};
                obj.rotation = {x = rotationX, y = rotationY, z = rotationZ};
                obj.slots = slots;
                obj.world = world;

                if (obj.world == getWorld())
                    obj.addToWorld();

                mines[id] <- obj;
                callEvent("onMineAdd", id);
            break;
            case MinePackets.Delete:
                local id = packet.readInt32();
                if(!(id in mines))
                    return;

                getMine(id).remove();
                mines.rawdelete(id);
                callEvent("onMineRemove", id);
            break;
            case MinePackets.Update:
                local length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    mines[packet.readInt32()].update(packet.readInt16())
            break;
        }
    }
}

getMine <- @(id) MineController.mines[id];
getMines <- @() MineController.mines;

RegisterPacketReceiver(Packets.Mine, MineController.onPacket.bindenv(MineController));
