
class Mine
{
    id = -1
    position = null
    rotation = -1

    scheme = -1
    slots = -1

    draw = null
    observer = null
    vob = null

    world = ""

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        rotation = {
            x = 0,
            y = 0,
            z = 0
        }
        slots = 0

        draw = null
        vob = null

        world = ""
    }

    function addToWorld() {
        draw = WorldInterface.Draw3d(position.x,position.y + 67,position.z);
        draw.addLine(scheme);
        draw.addLine("Ilo��: "+slots+"/"+getScheme().slots);
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        draw.distance = 600;
        draw.setScale(0.6, 0.6);
        draw.visible = true;
        updateDrawText();

        vob = MobInter(getScheme().vob);
        vob.setPosition(position.x, position.y,position.z);
        vob.setRotation(rotation.x,rotation.y,rotation.z);
        vob.cdDynamic = true;
        vob.cdStatic = true;
        vob.addToWorld();
        vob.floor();
        update(slots);
    }

    function updateDrawText() {
        draw.setLineText(0, scheme);
        draw.setLineText(1, "Ilo��: "+slots+"/"+getScheme().slots);
    }

    function update(_slots) {
        slots = _slots;
        updateDrawText();
    }

    function remove()  {
        draw.remove();
    }

    function getScheme() {
        return Config["MineSystem"][scheme];
    }

    static function onWorldChange(world)
    {
        foreach(mine in MineController.mines)
        {
            if (mine.world != world)
            {
                mine.remove()
                mine.vob.removeFromWorld()
            }
        }
    }

    static function onWorldEnter(world)
    {
        foreach(mine in MineController.mines)
        {
            if (mine.world == world)
                mine.addToWorld()
        }
    }
}


addEventHandler("onWorldEnter", Mine.onWorldEnter.bindenv(Forest));
addEventHandler("onWorldChange", Mine.onWorldChange.bindenv(Forest));