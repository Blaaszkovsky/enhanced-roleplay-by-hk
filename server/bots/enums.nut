enum BotTargetType
{
    Human,
    Npc,
}

enum BotCombatStatus
{
    Wait,
    Warn,
    Run,
    Attack,
}

enum BotPackets
{
    Destroy,
    Spawn,
    Unspawn,
    Streamer,
    Animation,
    Angle,
    Attack,
    AttackBot,
    AttackPlayer,
    WeaponMode,
    Health,
    Mana,
    Lvl,
    Scale,
    Fatness,
    Synchronization,
    Interaction,
    Dialogs,
    CallDialog,
    StartDialog,
    CloseDialog,
    InteractDeadBody,
    Taken,
    Direction,
    ResetDirection,
    JumpForward,
    MoveForward,
}

enum BotCallerType
{
    Custom,
    AddItem,
    RemoveItem,
    CloseDialog,
    AddEnemy,
    RemoveEnemy,
    Distance,
    RunToPosition,
    WalkToPosition,
    Notification,
}

enum BotRequirementType
{
    OtherDialog,
    OtherDialogNotExist,
    OneOfDialogs,
    ItemPosession,
    Fraction,
    FractionNotExist,
    Custom,
    Once,
    AttributePlayer,
    SkillPlayer
    Time,
}

enum BotDialogType
{
    Quick,
    Normal,
    Once,
    Last,
    Forced,
    ForcedClose,
    ForcedMedium,
}

enum BotDirectionMode
{
    Run,
    Walk,
}

const ATTACK_SWORD_LEFT = 1;
const ATTACK_SWORD_RIGHT = 2;
const ATTACK_FRONT = 0;
const ATTACK_SWORD_FRONT = 3;