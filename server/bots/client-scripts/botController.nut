local tick = getTickCount() + 100;

class BotController
{
    static function makeSynchronization() {
        local bots = [];
        local botsRegister = getBots();

        foreach(botId, bool in BotPlayer.streamed)
        {
            if(!(botId in botsRegister))
                continue;

            if (getWorld() != botsRegister[botId].world)
                continue

            local bot = botsRegister[botId];
            local positionRealNPC = getPlayerPosition(bot.element);
            if(isPlayerStreamed(bot.element) == false)
                continue;

            if(bot.positionDifference(positionRealNPC) > 10.0)
                bots.append({id = bot.id, x = positionRealNPC.x, y = positionRealNPC.y, z = positionRealNPC.z});
        }

        if(bots.len() > 0)
            BotPacket.sendSynchronization(bots);
    }

    static function onRender() {
        if(tick < getTickCount()) {
            makeSynchronization();
            tick = getTickCount() + 300;
        }
    }

    static function onInteractNpc(botId) {
        local bot = getBot(botId);
        if(bot.hp == 0) {
            playAni(heroId,"T_PLUNDER");
            BotPacket.onInteractWithDeadBody(botId);
        } else {
            BotPacket.interactBot(botId);
            BotDialog.start(botId);
        }
    }

    static function onPacket(packet)
    {
        local typeId = packet.readUInt8();
        if(typeId != PacketBots)
            return;

        switch(packet.readUInt8())
        {
            case BotPackets.Destroy:
                local botId = packet.readInt16();

                if(botId in getBots())
                    getBots()[botId].destroyBot();

                BotAnimations.remove(botId);
            break;
            case BotPackets.Spawn:
                local botId = packet.readInt16();
                local isTakken = packet.readBool();
                local name = packet.readString();

                local bot = Bot(botId, name);

                bot.isTakken = isTakken;
                bot.group = packet.readString();
                bot.instance = packet.readString();
                bot.animation = packet.readString();
                bot.position.x = packet.readFloat();
                bot.position.y = packet.readFloat();
                bot.position.z = packet.readFloat();
                bot.scale.x = packet.readFloat();
                bot.scale.y = packet.readFloat();
                bot.scale.z = packet.readFloat();
                bot.angle = packet.readFloat();
                bot.fatness = packet.readFloat();
                bot.hp = packet.readInt32();
                bot.hpMax = packet.readInt32();
                bot.mana = packet.readInt32();
                bot.manaMax = packet.readInt32();
                bot.weapon = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()];
                bot.str = packet.readInt16();
                bot.dex = packet.readInt16();
                bot.lvl = packet.readInt16();
                bot.magicLvl = packet.readInt16();
                bot.weaponMode = packet.readInt16();
                bot.melee = packet.readInt16();
                bot.ranged = packet.readInt16();
                bot.armor = packet.readInt16();
                bot.helmet = packet.readInt16();
                bot.shield = packet.readInt16();
                bot.magic = packet.readInt16();
                bot.visual.bodyModel = packet.readString();
                bot.visual.bodyTxt = packet.readInt16();
                bot.visual.headModel = packet.readString();
                bot.visual.headTxt = packet.readInt16();

                local x = packet.readFloat()
                local y = packet.readFloat()
                local z = packet.readFloat()
                local distance = packet.readFloat()
                local model = packet.readUInt8()
                bot.world = packet.readString()
                bot.spawn();
                BotAnimations.add(bot.id, bot.animation);
                bot.setDirection(x, y, z, distance, model);
            break;

            case BotPackets.Taken:
                local botId = packet.readInt16();
                local isTakken = packet.readBool();

                if(botId in getBots())
                    getBots()[botId].setTakken(isTakken);
            break;

            case BotPackets.Unspawn:
                local botId = packet.readInt16();

                if(botId in getBots())
                    getBots()[botId].destroyBot();

                BotAnimations.remove(botId);
            break;

            case BotPackets.Attack:
                local botId = packet.readInt16();
                local playerId = packet.readInt16();
                local typeAttack = packet.readUInt8();

                if(botId in getBots())
                    getBots()[botId].attackPlayerId(playerId, typeAttack);
            break;

            case BotPackets.Streamer:
                local botId = packet.readInt16();
                local spawnerId = packet.readInt16();

                if(spawnerId == heroId)
                    BotPlayer.add(botId);
                else
                    BotPlayer.remove(botId);
            break;

            case BotPackets.Animation:
                local botId = packet.readInt16();
                local animation = packet.readString();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].playAnimation(animation);
            break;

            case BotPackets.WeaponMode:
                local botId = packet.readInt16();
                local weaponMode = packet.readInt16();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].setWeaponMode(weaponMode);
            break;

            case BotPackets.Angle:
                local botId = packet.readInt16();
                local angle = packet.readInt16();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].setAngle(angle);
            break;

            case BotPackets.Health:
                local botId = packet.readInt16();
                local health = packet.readInt32();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].setHealth(health);
            break;

            case BotPackets.Mana:
                local botId = packet.readInt16();
                local mana = packet.readInt32();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].setMana(mana);
            break;

            case BotPackets.Synchronization:
                local botId = packet.readInt16();
                local bots = getBots();

                local posX = packet.readFloat();
                local posY = packet.readFloat();
                local posZ = packet.readFloat();

                if(botId in bots)
                    bots[botId].setPosition(posX, posY, posZ);
            break;

            case BotPackets.Direction:
                local botId = packet.readInt16();
                local bots = getBots();

                local posX = packet.readFloat();
                local posY = packet.readFloat();
                local posZ = packet.readFloat();
                local distance = packet.readFloat();
                local mode = packet.readUInt8();

                if(botId in bots)
                    bots[botId].setDirection(posX, posY, posZ, distance, mode);
            break;

            case BotPackets.ResetDirection:
                local botId = packet.readInt16();
                local bots = getBots();

                if(botId in bots)
                    bots[botId].resetDirection();
            break;

            case BotPackets.Dialogs:
                BotDialog.clear();

                local botId = packet.readInt16();
                local dialogLabelsLen = packet.readInt16();

                for(local i = 0; i < dialogLabelsLen; i ++)
                {
                    local iddialog = packet.readInt16();
                    local type = packet.readUInt8();
                    local label = packet.readString();
                    local textLen = packet.readInt16();
                    local texts = [];
                    for(local i = 0; i < textLen; i ++)
                        texts.append(packet.readString());

                    BotDialog.addItem(BotDialogItem(iddialog,type,label,texts));
                }

                BotDialog.updateUI();
            break;

            case BotPackets.StartDialog:
                local botId = packet.readInt16();
                local bots = getBots();

                if(botId in bots)
                    BotDialog.start(botId);
            break;

            case BotPackets.CloseDialog:
                BotDialog.hide();
            break;
        }
    }
}

addEventHandler("onPacket", BotController.onPacket.bindenv(BotController));
addEventHandler("onRender", BotController.onRender.bindenv(BotController));
addEventHandler("onInteractNpc", BotController.onInteractNpc.bindenv(BotController));