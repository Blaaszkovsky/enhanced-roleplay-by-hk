
local tick = getTickCount() + 50;
local jumpCheck = getTickCount() + 500;
local directionCheck = getTickCount() + 100;

BotAnimations <- {
    objects = {},
    jumps = {},
    direction = {},

    add = function(botId, animation)
    {
        BotAnimations.objects[botId] <- animation;

        if(botId in BotPlayer.streamed) {
            if(animation == "S_FISTRUNL" || animation == "S_1HRUNL" || animation == "S_2HRUNL" || animation == "S_RUNL") {
                BotAnimations.jumps[botId] <- {
                    element = getBot(botId).element,
                    position = getBot(botId).position,
                    check = 0,
                    checkHits = 0,
                }
            }
        }
    }

    remove = function(botId)
    {
        BotAnimations.objects.rawdelete(botId);

        if(botId in BotAnimations.jumps)
            BotAnimations.jumps.rawdelete(botId);

        if(botId in BotAnimations.direction)
            BotAnimations.direction.rawdelete(botId);
    }

    change = function(botId, animation)
    {
        BotAnimations.objects[botId] = animation;

        if(botId in BotPlayer.streamed) {
            if(animation == "S_FISTRUNL" || animation == "S_1HRUNL" || animation == "S_2HRUNL" || animation == "S_RUNL") {
                BotAnimations.jumps[botId] <- {
                    element = getBot(botId).element,
                    position = getBot(botId).position,
                    check = 0,
                    checkHits = 0,
                }
            }
            else {
                if(botId in BotAnimations.jumps)
                    BotAnimations.jumps.rawdelete(botId);
            }
        }
    }

    update = function()
    {
        foreach(botId, animation in BotAnimations.objects)
        {
            local element = getBots()[botId].element;
            local currAnimation = getPlayerAni(element)

            switch(animation)
            {
                case "T_WARN": case "S_FISTRUNL": case "S_1HRUNL": case "S_2HRUNL": case "S_LGUARD": case "S_HGUARD": case "S_BENCH_S0": case "S_BENCH_S1": case "T_SEARCH": case "T_AMBIENT_01": case "S_LEAN": case "S_BSSHARP_S1": case "T_PLUNDER": case "T_AMBIENT_SIT_01": case "T_POTION_RANDOM_1": case "T_ROZGRZEWKA": case "S_POMPKI_S1": case "S_DYBY":
                    if(currAnimation != animation) {
                        playAni(element, animation);
                    }
                break;
                case "STOP":
                    if(currAnimation != "S_RUN" && currAnimation != "S_FISTRUN")
                        stopAni(element);
                break;
                case "DIALOG":
                    playGesticulation(element);
                break;
            }
        }
    }

    jumpPossibility = function()
    {
        foreach(botId, obj in BotAnimations.jumps)
        {
            local posReal = getPlayerPosition(obj.element);
            if(getDistance3d(posReal.x, posReal.y, posReal.z, obj.position.x, obj.position.y, obj.position.z) > 10.0) {
                obj.position = posReal;
                obj.check = 0;
            }else
                obj.check += 1;


            if(obj.check >= 5)
            {
                BotPacket.jumpForward(botId);
                BotAnimations.jumps.rawdelete(botId);
                jumpCheck = getTickCount() + 10;
                break;
            }
        }
    }

    directionControl = function(botId, elementId, x, y, z, distance, mode)
    {
        if(botId in BotAnimations.direction)
            BotAnimations.direction.rawdelete(botId);

        BotAnimations.direction[botId] <- {
            elementId = elementId,
            x = x,
            y = y,
            z = z,
            distance = distance,
            mode = mode,
        }
    }

    directionControlRemove = function(botId)
    {
        if(botId in BotAnimations.direction)
            BotAnimations.direction.rawdelete(botId);
    }

    directionControlOnRender = function()
    {
        foreach(botId, botData in direction) {
            local pos = getPlayerPosition(botData.elementId);
            if(getDistance3d(pos.x, pos.y, pos.z, botData.x, botData.y, botData.z) < botData.distance) {
                getBot(botId).playAnimation("STOP");
                direction.rawdelete(botId);
                BotPacket.sendResetDirection(botId);
                return;
            }

            local angle = getVectorAngle(pos.x,pos.z,botData.x,botData.z);
            setPlayerAngle(botData.elementId, angle);
        }
    }
}

function checkMoveForwardChecker(pid, kid)
{
    foreach(botId, obj in BotAnimations.jumps)
    {
        if(pid == heroId && obj.element == kid)
            obj.checkHits += 1;

        if(obj.checkHits > 4) {
            BotPacket.moveForward(botId, pid);
            BotAnimations.jumps.rawdelete(botId);
        }
    }
}

addEventHandler("onRender", function() {
    if(tick < getTickCount()) {
        BotAnimations.update();
        tick = getTickCount() + 1000;
    }

    if(jumpCheck < getTickCount()) {
        BotAnimations.jumpPossibility();
        jumpCheck = getTickCount() + 500;
    }

    if(directionCheck < getTickCount()) {
        BotAnimations.directionControlOnRender();
        directionCheck = getTickCount() + 50;
    }
})