
BotDialog <- {
    botObjectId = -1
    nameNpc = ""

    dialogs = []
    isOpened = false

    addItem = function(obj) {
        dialogs.push(obj);
    }

    clear = function()
    {
        dialogs.clear();
    }

    start = function(botId)
    {
        local bots = getBots();
        if(!(botId in bots))
            return;

        clear();

        botObjectId = botId;
        local botObj = bots[botObjectId];
        nameNpc = botObj.name;

        BlackScreen(500);
        BotDialogInterface.start();

        setTimer(BotDialog.check, 500, 0);

        Camera.setBeforePlayer(botObj.element, 240)
    }

    hide = function()
    {
        clear();
        isOpened = false;
        BotDialogInterface.hide();
    }

    check = function()
    {
        if(BotDialog.dialogs.len() == 0)
            BotDialog.addItem(BotDialogItem(-1, BotDialogType.Quick, "", ["Nie mam ci nic do powiedzenia."]));
    }

    openDialog = function (dialog) {
        if(!isOpened) {
            BotDialogInterface.openDialog(dialog);
            isOpened = true;
        }
    }

    updateUI = function()
    {
        if(ActiveGui != PlayerGUI.BotDialog)
            return;

        isOpened = false;

        if(dialogs.len() == 0)
            BotDialog.addItem(BotDialogItem(-1, BotDialogType.Quick, "", ["Nie mam ci nic do powiedzenia."]));

        if(dialogs[0].type == BotDialogType.Quick)
        {
            if(dialogs.len() == 1)
                BotDialog.openDialog(BotDialog.dialogs[0]);
            else {
                local randN = irand(dialogs.len()) - 1;
                if(randN == -1)
                    randN = 0;

                BotDialog.openDialog(BotDialog.dialogs[randN]);
            }
            return;
        }

        BotDialogInterface.clear();

        BotDialogInterface.textures.push(Texture(0, 0, 8200, 600, "BLACK.TGA"));
        BotDialogInterface.textures.push(Texture(0, 7600, 8200, 600, "BLACK.TGA"));

        foreach(index, dialog in dialogs)
        {
            local draw = GUI.Draw(anx(200), any(Resolution.y/2 - 100 + index * 40), dialog.label);

            if(getSetting("DialogCustomFont")) {
                draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
                draw.setScale(0.5, 0.5);
            }else {
                draw.setScale(1.1, 1.1);
            }

            BotDialogInterface.draws.append(draw);
        }

        foreach(text in BotDialogInterface.textures)
            text.visible = true;

        foreach(draw in BotDialogInterface.draws)
            draw.setVisible(true);
    }

    onMouseClick = function(element) {
        foreach(index, text in BotDialogInterface.draws) {
            if(text == element) {
                if(index < BotDialog.dialogs.len())
                    BotDialog.openDialog(BotDialog.dialogs[index])

                return;
            }
        }
    }

    onMouseIn = function(element) {
        foreach(index, text in BotDialogInterface.draws) {
            if(text == element) {
                element.setColor(249, 55, 14);
            }
        }
    }

    onMouseOut = function(element) {
        foreach(index, text in BotDialogInterface.draws) {
            if(text == element) {
                element.setColor(255, 255, 255);
            }
        }
    }
}

BotDialogInterface <- {
    textures = []
    draws = []

    openedDialog = null

    queueCurrent = 0
    queueEnd = 0
    queueActive = false
    queueTimer = null

    start = function() {
        BaseGUI.show();
        ActiveGui = PlayerGUI.BotDialog;
    }

    hide = function() {
        clear();

        if(queueTimer != null)
            killTimer(queueTimer);

        queueTimer = null;
        BaseGUI.hide();
        ActiveGui = null;
        queueActive = false;

    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.BotDialog)
            hide();
    }

    openDialog = function (dialog) {
        clear();

        openedDialog = dialog;
        BotDialogInterface.queueStart();
    }

    endDialog = function() {
        switch(openedDialog.type)
        {
            case BotDialogType.Last:case BotDialogType.Quick:
                return BotDialog.hide();
            break;
        }

        BotPacket.callDialog(BotDialog.botObjectId,openedDialog.id);
        queueActive = false;
    }

    queueStart = function() {
        queueCurrent = 0
        queueEnd = openedDialog.text.len() - 1;
        queueActive = true

        BotDialogInterface.queueContent();
    }

    queueContent = function() {
        BotDialogInterface.clear();

        local botObj = getBots()[BotDialog.botObjectId];
        playGesticulation(botObj.element);

        BotDialogInterface.textures.push(Texture(0, 0, 8200, 600, "BLACK.TGA"));
        BotDialogInterface.textures.push(Texture(0, 7600, 8200, 600, "BLACK.TGA"));

        local arr = split(openedDialog.text[queueCurrent], "\n");
        foreach(_item, _value in arr)
        {
            local draw = GUI.Draw(0, 0, _value);
            if(getSetting("DialogCustomFont")) {
                draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
                draw.setScale(0.4, 0.4);
            }
            draw.setPosition(anx(Resolution.x/2) - draw.getSizePx().width/2, 800 + _item * any(40));
            draws.append(draw);
        }

        foreach(draw in BotDialogInterface.draws)
            draw.setVisible(true);

        foreach(texture in BotDialogInterface.textures)
            texture.visible = true;

        queueTimer = setTimer(function() {
            BotDialogInterface.queueContinue();
        }, 1000 * 10, 1);
    }

    queueContinue = function() {
        queueCurrent = queueCurrent + 1
        if(queueTimer != null)
            killTimer(queueTimer);

        queueTimer = null;

        if(queueCurrent > queueEnd)
            BotDialogInterface.endDialog();
        else
            BotDialogInterface.queueContent();
    }

    onKey = function(key) {
        if(BotDialogInterface.queueActive == false)
            return;

        if(key == KEY_SPACE)
            BotDialogInterface.queueContinue();
    }

    clear = function() {
        foreach(draw in draws)
            draw.destroy();

        textures.clear();
        draws.clear();
    }
}

class BotDialogItem
{
    id = -1
    type = -1
    label = ""
    text = ""

    constructor(id, type, label, text)
    {
        this.id = id;
        this.type = type;
        this.label = label;
        this.text = text;
    }
}

addEventHandler("GUI.onClick", BotDialog.onMouseClick)
addEventHandler("GUI.onMouseOut", BotDialog.onMouseOut)
addEventHandler("GUI.onMouseIn", BotDialog.onMouseIn)
addEventHandler("onKey", BotDialogInterface.onKey)
addEventHandler("onForceCloseGUI", BotDialogInterface.onForceCloseGUI.bindenv(Inventory));