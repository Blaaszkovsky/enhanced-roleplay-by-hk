
local factions = {};
local nextId = -1;

class Faction
{
    id = -1

    allies = null
    enemies = null

    constructor()
    {
        nextId = nextId + 1;

        this.id = nextId;

        this.enemies = [];
        this.allies = [];

        factions[id] <- this;
    }

    function addAlly(faction) {
        this.allies.push(faction.id);
    }

    function removeAlly(faction) {
        local lenght = this.allies.len()
        this.allies[faction.id] = this.allies[lenght - 1]
        this.allies.pop();
    }

    function addEnemy(faction) {
        this.enemies.push(faction.id);
    }

    function removeEnemy(faction) {
        local lenght = this.enemies.len()
        this.enemies[faction.id] = this.enemies[lenght - 1]
        this.enemies.pop();
    }

    function searchForEnemies(factionsToCompare) {
        foreach(comparedFactionId in factionsToCompare)
        {
            if(factions[comparedFactionId].enemies.find(id) != null)
                return true;
        }

        return false;
    }

    function searchForAllies(factionsToCompare) {
        foreach(comparedFactionId in factionsToCompare)
        {
            if(factions[comparedFactionId].allies.find(id) != null)
                return true;
        }

        return false;
    }
}

function getFactions() {
    return factions;
}