GridRegistered <- {};

class Grid
{
    cellSize = -1;
    world = -1;

    cells = null;

    constructor(cellSize)
    {
        this.cellSize = cellSize;
        this.world = -1;

        this.cells = {};
    }

    function ajustBotToCell(cellIndex, botId) {
        if(!(cellIndex in cells))
            cells[cellIndex] <- {};

        cells[cellIndex][botId] <- true;
    }

    function removeBotFromCell(cellIndex, botId) {
        if(cellIndex == null)
            return;

        if(!(cellIndex in cells))
            cells[cellIndex] <- {};

        if(botId in cells[cellIndex])
            cells[cellIndex].rawdelete(botId);
    }

    function nearestChunk(index, radius = 3)
    {
        local pos = split(index, ",")
        local chunk = {
            x = pos[0].tointeger(),
            z = pos[1].tointeger(),
        }
        local chunks = [];

        for (local x = chunk.x - radius, endX = chunk.x + radius; x <= endX; ++x)
            for (local z = chunk.z - radius, endZ = chunk.z + radius; z <= endZ; ++z)
                chunks.append(x + "," + z);

        return chunks;
    }

    function nearestBots(index, radius = 3) {
        local value = {};

        foreach(chunk in nearestChunk(index, radius))
        {
            foreach(botId, bool in get(chunk))
                value[botId] <- bool;
        }

        return value;
    }

    function getIndex(x,z) {
        return (x / cellSize).tointeger() + "," + (z / cellSize).tointeger()
    }

    function get(index)
    {
        if(index in cells)
            return cells[index];

        return cells[index] <- {};
    }
}

