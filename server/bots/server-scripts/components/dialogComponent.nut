local nextId = 0

class BotDialogComponent extends BotComponent
{
    dialogs = [];

    constructor()
    {
        dialogs = [];

        events = {
            "onPlayerInteract": "onPlayerInteract",
            "onPlayerEnterCloseRange": "onPlayerEnterCloseRange",
            "onPlayerEnterVeryCloseRange": "onPlayerEnterVeryCloseRange",
            "onPlayerEnterMediumCloseRange": "onPlayerEnterMediumCloseRange",
        }
    }

    function add(object) {
        dialogs.append(object);
    }

    function onPlayerEnterCloseRange(playerId)
    {
        local forced = [];
        foreach(dialog in dialogs)
        {
            if(dialog.type == BotDialogType.Forced)
                if(dialog.doRequirements(playerId, parent))
                    forced.push(dialog);
        }

        if(forced.len() > 0)
        {
            parent.packetManager.startDialog(playerId);
            parent.packetManager.sendDialogs(playerId, forced);
        }
    }

    function onPlayerEnterVeryCloseRange(playerId)
    {
        local forced = [];
        foreach(dialog in dialogs)
        {
            if(dialog.type == BotDialogType.ForcedClose)
                if(dialog.doRequirements(playerId, parent))
                    forced.push(dialog);
        }

        if(forced.len() > 0)
        {
            parent.packetManager.startDialog(playerId);
            parent.packetManager.sendDialogs(playerId, forced);
        }
    }

    function onPlayerEnterMediumCloseRange(playerId)
    {
        local forced = [];
        foreach(dialog in dialogs)
        {
            if(dialog.type == BotDialogType.ForcedMedium)
                if(dialog.doRequirements(playerId, parent))
                    forced.push(dialog);
        }

        if(forced.len() > 0)
        {
            parent.packetManager.startDialog(playerId);
            parent.packetManager.sendDialogs(playerId, forced);
        }
    }

    function onPlayerCallDialog(playerId, dialogId)
    {
        foreach(dialog in dialogs)
        {
            if(dialog.id == dialogId && !dialog.callersExecuted)
            {
                if(dialog.type == BotDialogType.Last || dialog.type == BotDialogType.Quick)
                    return;

                getBotPlayer(playerId).addDialog(dialog.identifier)
                dialog.doCallers(playerId,parent);
                break;
            }
        }

        local related = getRelatedDialogs(playerId);
        parent.packetManager.sendDialogs(playerId, related);
    }

    function onPlayerInteract(playerId) {
        if(parent.hp == 0)
            return;

        local related = getRelatedDialogs(playerId);
        parent.packetManager.sendDialogs(playerId, related);
    }

    function getRelatedDialogs(playerId)
    {
        local dialogsOperationed = [];

        foreach(dialog in dialogs)
        {
            if(dialog.type == BotDialogType.Once) {
                if(getBotPlayer(playerId).hasDialog(dialog.identifier))
                    continue;
            }

            if(dialog.doRequirements(playerId, parent))
                dialogsOperationed.append(dialog);
        }

        return dialogsOperationed;
    }
}

class BotDialog
{
    id = -1
    identifier = ""
    type = -1
    label = ""
    dePolonizedLabel = ""
    text = ""

    requirements = null
    callers = null
    callersExecuted = false

    constructor(identifier, type, label, text)
    {
        this.id = nextId;
        this.identifier = identifier;
        this.type = type;
        this.label = label;
        this.dePolonizedLabel = String.dePolonizer(label);
        this.text = text;

        this.requirements = [];
        this.callers = [];
        this.callersExecuted = false

        nextId = nextId + 1;
    }

    function doCallers(playerId, bot) {
        this.callersExecuted = true
        foreach(caller in callers)
            caller.go(playerId, bot, this);
        this.callersExecuted = false
    }

    function doRequirements(playerId, bot) {
        local possibleToAdd = true;
        foreach(requirement in requirements) {
            if(requirement.go(playerId, bot, this) == false)
                possibleToAdd = false;
        }

        return possibleToAdd;
    }

    function addRequirement(requirement) {
        requirements.append(requirement);
        return requirement;
    }

    function addCaller(caller) {
        callers.append(caller);
        return caller;
    }
}

class BotDialogRequirement
{
    type = -1

    value = null
    valueSecond = null
    valueThird = null

    constructor(type, value = null, valueSecond = null, valueThird = null)
    {
        this.type = type
        this.value = value
        this.valueSecond = valueSecond
        this.valueThird = valueThird
    }

    function go(playerId, bot, dialog)
    {
        switch(type)
        {
            case BotRequirementType.OtherDialog:
                return getBotPlayer(playerId).hasDialog(value) != null;
            break;
            case BotRequirementType.OtherDialogNotExist:
                return getBotPlayer(playerId).hasDialog(value) == null;
            break;
            case BotRequirementType.OneOfDialogs:
                if(getBotPlayer(playerId).hasDialog(value))
                    return true;

                if(getBotPlayer(playerId).hasDialog(valueSecond))
                    return true;

                if(getBotPlayer(playerId).hasDialog(valueThird))
                    return true;

                return false;
            break;
            case BotRequirementType.Once:
                return getBotPlayer(playerId).hasDialog(value) == null;
            break;
            case BotRequirementType.Time:
                if(getBotPlayer(playerId).hasDialog(value) == null)
                    return true;

                return time() > (getBotPlayer(playerId).hasDialog(value) + valueSecond);
            break;
            case BotRequirementType.ItemPosession:
                return hasPlayerItem(playerId, value) >= valueSecond;
            break;
            case BotRequirementType.Custom:
                return this.value(playerId, bot);
            break;
            case BotRequirementType.Fraction:
                return getBotPlayer(playerId).hasFaction(value.id);
            break;
            case BotRequirementType.FractionNotExist:
                return !getBotPlayer(playerId).hasFaction(value.id);
            break;
            case BotRequirementType.AttributePlayer:
                return getPlayerAttributeValue(playerId, value) >= valueSecond;
            break;
            case BotRequirementType.SkillPlayer:
                return getPlayerSkill(playerId, value) >= valueSecond;
            break;
        }
    }
}

class BotDialogCaller
{
    type = -1

    value = null
    valueSecond = null
    valueThird = null
    valueFour = null

    constructor(type, value = null, valueSecond = null, valueThird = null, valueFour = null)
    {
        this.type = type

        this.value = value
        this.valueSecond = valueSecond
        this.valueThird = valueThird
        this.valueFour = valueFour
    }

    function go(playerId, bot, dialog)
    {
        switch(type)
        {
            case BotCallerType.AddItem:
                return giveItem(playerId, value, valueSecond);
            break;
            case BotCallerType.RemoveItem:
                return removeItem(playerId, value, valueSecond);
            break;
            case BotCallerType.Custom:
                return this.value(playerId, bot);
            break;
            case BotCallerType.CloseDialog:
                bot.packetManager.closeDialog(playerId);
            break;
            case BotCallerType.RunToPosition:
                PlayerPacket(playerId).forcePositionRearange(false, value, valueSecond, valueThird, valueFour)
            break;
            case BotCallerType.WalkToPosition:
                PlayerPacket(playerId).forcePositionRearange(true, value, valueSecond, valueThird, valueFour)
            break;
            case BotCallerType.Notification:
                PlayerPacket(playerId).addNotification(value)
            break;
            case BotCallerType.AddEnemy:
                bot.onGetDamage(playerId, null, false);
            break;
            case BotCallerType.RemoveEnemy:
                setPlayerHealth(playerId, 0);
            break;
        }
    }
}