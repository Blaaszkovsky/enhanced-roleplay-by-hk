
class BotWanderingComponent extends BotComponent
{
    isWandering = false
    wanderedAway = false

    defaultPosition = null
    destinationPosition = null

    constructor()
    {
        isWandering = false
        wanderedAway = false

        defaultPosition = []
        destinationPosition = []

        events = {
            "noRoutineFinded": "noRoutineFinded",
        }
    }

    function noRoutineFinded() {
        isWandering == false ? wander() : judge();
    }

    function judge() {
        parent.playAnimation("T_STAND_2_EAT");
        parent.playAnimation("STOP");
        isWandering = false;
    }

    function wander() {
        local chance = irand(100);
        if(chance < 30)
            startWander();
    }

    function startWander() {
        local random = irand(4);

        if(wanderedAway == false) {
            wanderedAway = true
            defaultPosition = clone parent.position;
            destinationPosition = clone parent.position;

            switch(random)
            {
                case 2:
                    destinationPosition.z = destinationPosition.z + rand() % 200;
                    destinationPosition.x = destinationPosition.x + rand() % 200;
                break;
                case 3:
                    destinationPosition.z = destinationPosition.z + rand() % 200;
                    destinationPosition.x = destinationPosition.x - rand() % 200;
                break;
                case 1:
                    destinationPosition.z = destinationPosition.z - rand() % 200;
                    destinationPosition.x = destinationPosition.x + rand() % 200;
                break;
                default:
                    destinationPosition.z = destinationPosition.z - rand() % 200;
                    destinationPosition.x = destinationPosition.x - rand() % 200;
                break;
            }
        } else {
            wanderedAway = false;
            destinationPosition = defaultPosition;
        }

        isWandering = true;

        parent.turnIntoPosition(destinationPosition);
        parent.playAnimation("S_FISTWALKL");
    }
}