class BotStreamComponent extends BotComponent
{
    streamerId = -1;

    constructor () {
        streamerId = -1;

        events = {
            "playerEnterRange" : "playerEnterRange",
            "playerExitRange" : "playerExitRange",
        }
    }

    function playerEnterRange(playerId) {
        if(streamerId == -1)
            streamerId = playerId;
        else if(getPlayerPing(streamerId) > getPlayerPing(playerId))
            streamerId = playerId;

        parent.playersInStream.append(playerId);
        parent.moduleCache.streamerId = streamerId;
        parent.packetManager.sendStreamer(streamerId);


        if(parent.nextTick == -1) {
            parent.setTickCount(getTickCount() + 1000);
            parent.callComponent("enterScene")
        }
    }

    function playerExitRange(playerId) {
        if(streamerId == playerId)
            streamerId = -1;

        parent.playersInStream.remove(parent.playersInStream.find(playerId));

        if(parent.playersInStream.len() == 0) {
            parent.setTickCount(-1);
            parent.callComponent("exitScene")
        }else{
            local bestPing = 999, bestId = -1;
            foreach(pid in parent.playersInStream) {
                if(getPlayerPing(pid) < bestPing) {
                    bestPing = getPlayerPing(pid);
                    bestId = pid;
                }
            }

            if(bestId == -1)
                bestId = parent.playersInStream[0];

            streamerId = bestId;
            parent.moduleCache.streamerId = streamerId;
            parent.packetManager.sendStreamer(streamerId);
        }
    }
}