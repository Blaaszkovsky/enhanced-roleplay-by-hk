
class BotFactionComponent extends BotComponent
{
    factions = null;

    constructor()
    {
        factions = [];

        events = {
            "onAddFaction" : "onAddFaction",
            "onRemoveFaction" : "onRemoveFaction",
            "onNearAttack" : "onNearAttack",
        }
    }

    function onNearAttack(playerId, bot) {
        foreach(component in bot.components) {
            if(component instanceof BotOwningComponent){
                return false;
            }     
        }

        local botFactions = [];
        foreach(component in bot.components)
        {
            if(component instanceof BotFactionComponent)
            {
                botFactions = component.factions;
                break;
            }
        }

        local factionsRegistered = getFactions();
        foreach(faction in factions)
        {
            if(botFactions.find(faction) != null)
            {
                parent.callComponent("onAddEnemy", playerId, BotTargetType.Human);
                return;
            }

            local factionObject = factionsRegistered[faction];
            if(factionObject.searchForAllies(botFactions))
            {
                parent.callComponent("onAddEnemy", playerId, BotTargetType.Human);
                return;
            }
        }
    }

    function onAddFaction(factionId) {
        if(factions.find(factionId) == null)
            factions.push(factionId);
    }

    function onRemoveFaction(factionId) {
        if(factions.find(factionId) != null)
            factions.remove(factions.remove(factionId));
    }
}