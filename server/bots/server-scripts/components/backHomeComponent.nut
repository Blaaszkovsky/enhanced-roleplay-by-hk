
local timers = [];

class BotBackHomeComponent extends BotComponent
{
    homePosition = null;
    waypointList = null;
    moveBehaviour = null;
    targetPosition = null;

    constructor(x,y,z,angle,animation)
    {
        events = {
            "afterUpdate": "afterUpdate",
            "onRespawn": "onRespawn",
        }

        waypointList = [];
        homePosition = {x = x, y = y, z = z, angle = angle, animation = animation}
        targetPosition = null
        moveBehaviour = null

        timers.push(this);
    }

    function onRespawn() {
        parent.setPosition(homePosition.x,homePosition.y,homePosition.z);
        parent.setAngle(homePosition.angle);
        parent.playAnimation(homePosition.animation);
    }

    function afterUpdate() {
        local distance = parent.positionDifference(homePosition);

        if(targetPosition != null) {
            local nextPosition = waypointList[0];
            local distanceToNextPosition = parent.simplePositionDifference(nextPosition);
            if(distanceToNextPosition < 200)
            {
                parent.setPosition(nextPosition.x,nextPosition.y,nextPosition.z)
                waypointList.remove(0);

                if(waypointList.len() == 0) {
                    parent.setPosition(homePosition.x,homePosition.y,homePosition.z);
                    parent.setAngle(homePosition.angle);
                    parent.playAnimation("STOP");
                    parent.playAnimation(homePosition.animation);
                    parent.setTickCount(getTickCount() + 500);
                    moveBehaviour = null;
                    targetPosition = null;
                    return;
                }

                targetPosition = waypointList[0];
            }

            moveBehaviour.walk();
            parent.turnIntoPosition(nextPosition);
            parent.setTickCount(getTickCount() + 500);
            return;
        }

        if(distance < 100) {
            parent.callComponent("noRoutineFinded");
            parent.setTickCount(getTickCount() + 2000);
            return;
        }

        if(distance > 400) {
            // local world = parent.world;
            // local botWaypoint = getNearestWaypoint(world, parent.position.x, parent.position.y, parent.position.z)
            // local behaviourWaypoint = getNearestWaypoint(world, homePosition.x, homePosition.y, homePosition.z)

            // waypointList = []

            // if(botWaypoint.name != behaviourWaypoint.name)
            // {
            //     try {
            //         local wayObject = Way(world, botWaypoint.name, behaviourWaypoint.name);
            //         foreach(wayName in wayObject.getWaypoints()) {
            //             waypointList.insert(0, getWaypoint(world, wayName))
            //         }
            //     } catch (exception){}

            //     waypointList.push(homePosition);
            // }else
            //     waypointList.push(homePosition)
            waypointList.push(homePosition)
        } else {
            waypointList.push(homePosition)
        }

        targetPosition = waypointList[0];
        parent.turnIntoPosition(targetPosition);
        parent.setTickCount(getTickCount() + 500);
        moveBehaviour = BotMoveBehaviour(this);
        moveBehaviour.walk();
    }

    function onOfflineUpdate() {
        if(parent.playersInStream.len() > 0)
            return;

        if(targetPosition == null)
            return;

        parent.setPosition(homePosition.x,homePosition.y,homePosition.z);
        parent.setAngle(homePosition.angle);
        parent.playAnimation("STOP");
        parent.playAnimation(homePosition.animation);
        moveBehaviour = null;
        targetPosition = null;
    }
}

setTimer(function() {
    foreach(timer in timers)
        timer.onOfflineUpdate();
}, 1000, 0);
