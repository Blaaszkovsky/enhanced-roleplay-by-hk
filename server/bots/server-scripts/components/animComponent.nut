
class BotAnimComponent extends BotComponent
{
    animationsQueue = null;
    
    constructor()
    {
        events = {
            "changeAnimation" : "stopAnimation",
            "stopAnimation" : "stopAnimation",
            "addAnimation" : "addAnimation",
            "endAnimation" : "endAnimation",
            "doAnimation" : "doAnimation",
        }

        animationsQueue = queue();
    }

    function changeAnimation(anim) {
        stopAnimation();

        animationsQueue.push(anim);
    }

    function stopAnimation() {
        parent.playAnimation("STOP");

        animationsQueue.clear();
    }

    function endAnimation() {
        animationsQueue.pop();

        if(animationsQueue.len() > 0)
            parent.playAnimation(animationsQueue.front());
    }

    function addAnimation(anim) {
        animationsQueue.push(anim);
    }

    function doAnimation() {
        parent.playAnimation(animationsQueue.front());
    }
}