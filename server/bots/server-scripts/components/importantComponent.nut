
class BotImportantComponent extends BotComponent
{
    constructor()
    {
        events = {
            "onUpdate": "onUpdate",
        }
    }

    function onUpdate() {
        if(parent.hp == 0) {
            parent.callComponent("onDeathUpdate");
            return;
        }

        parent.callComponent("onUpdateAfterCombat");
    }
}