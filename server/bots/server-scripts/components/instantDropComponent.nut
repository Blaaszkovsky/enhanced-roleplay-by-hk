
class BotInstantDropComponent extends BotComponent
{
    dropItem = null;

    constructor(instance)
    {
        dropItem = instance;

        events = {
            "onDeath": "onDeath",
        }
    }

    function onDeath(killerId) {
        if(killerId < getMaxSlots()) {
            giveItem(killerId, dropItem, 1);
        }
    }
}