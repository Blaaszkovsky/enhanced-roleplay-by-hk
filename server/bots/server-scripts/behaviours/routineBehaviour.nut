class BotRoutineBehaviour extends BotBehaviour
{
    routineObject = null
    botObject = null
    moveObject = null

    wayObject = null
    currentWaypointList = -1

    constructor(parentComponent, routineObjectToContain, botObjectReference)
    {
        base.constructor(parentComponent);

        this.routineObject = routineObjectToContain;
        this.botObject = botObjectReference;
        this.moveObject = BotMoveBehaviour(parentComponent);
        this.wayObject = null;

        local world = botObject.world;
        local botWaypoint = getNearestWaypoint(world, botObject.position.x, botObject.position.y, botObject.position.z)
        local behaviourWaypoint = getNearestWaypoint(world, routineObject.position.x, routineObject.position.y, routineObject.position.z)

        this.currentWaypointList = []

        if(botWaypoint.name != behaviourWaypoint.name && botObject.simplePositionDifference(routineObject.position) > 300)
        {
            try {
                this.wayObject = Way(world, botWaypoint.name, behaviourWaypoint.name);
                foreach(wayName in wayObject.getWaypoints())
                    currentWaypointList.insert(0, getWaypoint(world, wayName))
            } catch (exception){
                currentWaypointList.push(routineObject.position)
            }
        }else
            currentWaypointList.push(routineObject.position)
    }

    function continueBehaviour() {
        local nextPosition = currentWaypointList[0];
        local distance = botObject.simplePositionDifference(nextPosition);
        if(distance < 100)
        {
            botObject.setPosition(nextPosition.x,nextPosition.y,nextPosition.z)

            currentWaypointList.remove(0);

            if(currentWaypointList.len() == 0)
            {
                botObject.playAnimation(routineObject.animation);
                botObject.setAngle(routineObject.position.angle);

                parentComponent.onBehaviourStopped();
            }
        } else {
            botObject.turnIntoPosition(nextPosition)

            if("walk" in routineObject && routineObject.walk == true)
                moveObject.walk();
            else
                moveObject.run();
        }
    }

    function continueOfflineBehaviour() {
        local nextPosition = currentWaypointList[0];
        local distance = botObject.simplePositionDifference(nextPosition);
        botObject.setPosition(nextPosition.x,nextPosition.y,nextPosition.z)

        currentWaypointList.remove(0);

        if(currentWaypointList.len() == 0)
        {
            botObject.playAnimation(routineObject.animation);
            botObject.setAngle(routineObject.position.angle);

            parentComponent.onBehaviourStopped();
        }
    }
}

