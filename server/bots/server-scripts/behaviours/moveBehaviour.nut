class BotMoveBehaviour extends BotBehaviour
{
    function run() {
        local bot = parentComponent.parent;

        local anim = "S_RUNL";

        switch(bot.weaponMode)
        {
            case WEAPONMODE_FIST: anim = "S_FISTRUNL"; break;
            case WEAPONMODE_1HS: anim = "S_1HRUNL"; break;
            case WEAPONMODE_2HS: anim = "S_2HRUNL"; break;
            case WEAPONMODE_BOW: anim = "S_BOWRUNL"; break;
            case WEAPONMODE_CBOW: anim = "S_CBOWRUNL"; break;
            case WEAPONMODE_MAG: anim = "S_1HRUNL"; break;
        }

        if(bot.animation != anim)
            bot.playAnimation(anim);
    }

    function walk() {
        local bot = parentComponent.parent;

        local anim = "S_WALKL";
        switch(bot.weaponMode)
        {
            case WEAPONMODE_FIST: anim = "S_FISTWALKL"; break;
            case WEAPONMODE_1HS: anim = "S_1HWALKL"; break;
            case WEAPONMODE_2HS: anim = "S_2HWALKL"; break;
            case WEAPONMODE_BOW: anim = "S_BOWWALKL"; break;
            case WEAPONMODE_CBOW: anim = "S_CBOWWALKL"; break;
            case WEAPONMODE_MAG: anim = "S_1HWALKL"; break;
        }

        if(bot.animation != anim)
            bot.playAnimation(anim);
    }

    function warn() {
        local bot = parentComponent.parent;

        local anim = "T_WARN";
        if(bot.animation != anim)
            bot.playAnimation(anim);
    }

    function openWeapon() {
        local bot = parentComponent.parent;
        if(bot.magic != -1)
            bot.setWeaponMode(WEAPONMODE_MAG);
        else if(bot.ranged != -1) {
            if(item.Scheme.isBowWeapon(bot.ranged))
                bot.setWeaponMode(WEAPONMODE_BOW);
            else
                bot.setWeaponMode(WEAPONMODE_CBOW);
        }else if(bot.melee != -1) {
            if(item.Scheme.isOneHWeapon(bot.melee))
                bot.setWeaponMode(WEAPONMODE_1HS);
            else
                bot.setWeaponMode(WEAPONMODE_2HS);
        }else
            bot.setWeaponMode(WEAPONMODE_FIST);
    }

    function checkForMeleeWeapon() {
        local bot = parentComponent.parent;
        if(bot.melee != -1) {
            if(item.Scheme.isOneHWeapon(bot.melee))
                bot.setWeaponMode(WEAPONMODE_1HS);
            else
                bot.setWeaponMode(WEAPONMODE_2HS);
            return true;
        }
        return false;
    }

    function jumpBack() {
        local bot = parentComponent.parent;

        bot.playAnimation("T_FISTPARADEJUMPB");
    }

    function block() {
        local bot = parentComponent.parent;

        switch(bot.weaponMode)
        {
            case WEAPONMODE_1HS:
                bot.playAnimation("T_1HPARADE_0");
            break;
            case WEAPONMODE_2HS:
                bot.playAnimation("T_2HPARADE_0");
            break;
            default:
                bot.playAnimation("T_FISTPARADEJUMPB");
            break;
        }
    }

    function attackPlayer(playerId, typeOponnent, typeAttack = ATTACK_FRONT) {
        local bot = parentComponent.parent;

        switch(bot.weaponMode)
        {
            case WEAPONMODE_MAG:
                bot.playAnimation("T_CBOWRELOAD");
            break;
            case WEAPONMODE_CBOW:
                bot.playAnimation("T_CBOWRELOAD");
            break;
            case WEAPONMODE_BOW:
                bot.playAnimation("T_BOWRELOAD");
            break;
            case WEAPONMODE_1HS:
                switch(typeAttack)
                {
                    case ATTACK_SWORD_LEFT:
                        bot.playAnimation("T_1HATTACKL");
                    break;
                    case ATTACK_SWORD_RIGHT:
                        bot.playAnimation("T_1HATTACKR");
                    break;
                    default:
                        bot.playAnimation("S_1HATTACK");
                    break;
                }
            break;
            case WEAPONMODE_2HS:
                switch(typeAttack)
                {
                    case ATTACK_SWORD_LEFT:
                        bot.playAnimation("T_2HATTACKL");
                    break;
                    case ATTACK_SWORD_RIGHT:
                        bot.playAnimation("T_2HATTACKR");
                    break;
                    default:
                        bot.playAnimation("S_2HATTACK");
                    break;
                }
            break;
            default:
                bot.playAnimation("S_FISTATTACK");
            break;
        }

        if(typeOponnent == BotTargetType.Npc)
            bot.attackBot(playerId);
        else
            bot.attackPlayer(playerId, typeAttack);
    }
}