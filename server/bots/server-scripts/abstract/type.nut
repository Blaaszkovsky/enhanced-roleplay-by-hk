class BotCombatType extends BotComponent
{
    useEyeView = false;
    useSmell = false;

    searchAttackDistance = 1024;
    interactDistance = 2048;

    weaponDrawModeEnable = false;
    maxDistanceChase = 5000;
    maxOtherEnemyAttacks = 3;

    warnEnable = true;
    warnTime = 6000;
    warnMaxDistance = 1500;
    warnMinDistance = 900;

    jumpBackActive = false;
    jumpBackChance = 20;
    jumpBackTime = 400;

    blockActive = false;
    blockChance = 20;
    blockTime = 600;

    attackDelay = 1000;
    attackMinDist = 150;
    attackMaxDist = 240;

    jumpSideActive = false;
    jumpSideChance = 20;
    jumpSideTime = 400;
}