// next Id of Bot
local nextId = 0;

// contains bots
local bots = {};

// contains ticks of bots
local ticks = {};

local CELL_SIZE_WITH_MODE = 2;

class BotBase
{
    id = -1

    name = "";
    instance = "";
    group = "";

    moduleCache = null

    components = null
    nextTick = -1

    str = 10
    dex = 10

    hp = 40
    hpMax = 40
    mana = 10
    manaMax = 40

    position = null
    direction = null
    spawnPosition = null
    scale = null
    fatness = 1.0
    angle = 0

    cell = null
    weapon = null
    effects = null

    lvl = 0
    magicLvl = 0
    weaponMode = 0
    respawnTime = 10;

    // Equipment of bot
    melee = -1
    armor = -1
    ranged = -1
    helmet = -1
    shield = -1
    magic = -1

    // Current animation of bot
    animation = ""

    // This works only with instance PC_HERO
    visual = null

    // Packer manager instance
    packetManager = null

    // Players in stream (array)
    playersInStream = null

    // Statistics is connected to combat parameters
    statistics = null

    // World of bot
    world = "";

    isTakken = false
    isTemporary = false
    isSaved = false
    checkPosition = false

    constructor(name)
    {
        nextId = nextId + 1

        this.name = name
        this.instance = "PC_HERO"
        this.group = ""

        this.moduleCache = {
            "streamerId": -1,
            "ownerId": -1,
            "attackDistance": {
                min = 150,
                max = 230,
            },
            "hitWithoutAttack": 0,
        }

        this.id = nextId
        this.components = []
        this.nextTick = -1

        this.str = 10
        this.dex = 10

        this.hp = 40
        this.hpMax = 40
        this.mana = 10
        this.manaMax = 40

        this.position = {x = 0, y = 0, z = 0}
        this.direction = {x = 0, y = 0, z = 0, distance = 0, mode = 0}
        this.spawnPosition = {x = 0, y = 0, z = 0}
        this.angle = 0

        this.scale = {x = 1.0, y = 1.0, z = 1.0}
        this.fatness = 1.0

        this.animation = ""
        this.cell = null
        this.weapon = [0,0,0,0]
        this.effects = []

        this.lvl = 0
        this.magicLvl = 0
        this.weaponMode = 0
        this.respawnTime = 10

        this.melee = -1
        this.armor = -1
        this.ranged = -1
        this.helmet = -1
        this.shield = -1
        this.magic = -1

        this.visual = {bodyModel = "", bodyTxt = 1, headModel = "", headTxt = 1}

        this.packetManager = BotPacket(this)
        this.playersInStream = []
        this.statistics = BASIC_COMBAT;

        this.world = getServerWorld();

        this.isTemporary = false;
        this.isSaved = true;
        this.isTakken = false;
        this.checkPosition = false;

        bots[id] <- this;
    }

    function respawn() {
        if(isTemporary) {
            destroyBot();
            return;
        }

        isTakken = false;
        packetManager.sendTakken();

        hp = hpMax;
        packetManager.sendHealth();

        mana = manaMax;
        packetManager.sendMana();

        moduleCache.hitWithoutAttack = 0;

        playAnimation("STOP");

        if(spawnPosition.x == 0)
            setPosition(position.x, position.y, position.z);
        else
            setPosition(spawnPosition.x, spawnPosition.y, spawnPosition.z);

        if(position.x == 0 && position.y == 0 && position.z == 0) {
            destroyBot();
            return;
        }

        setAngle(angle);

        callComponent("onRespawn")
    }

    function playAnimation(value) {
        animation = value;
        packetManager.sendAnimation();
    }

    function attackPlayer(playerId, typeAttack) {
        packetManager.sendAttack(playerId, typeAttack);
    }

    function attackBot(botId) {
        bots[botId].onGetDamage(id, BotTargetType.Npc);
    }

    function setTickCount(value) {
        if(value == -1) {
            if(id in ticks)
                ticks.rawdelete(id);
        } else {
            if(id in ticks)
                ticks[id] = value;
            else
                ticks[id] <- value;
        }
    }

    function setPosition(x, y, z) {
        local newCell = GridRegistered[world].getIndex(x,z);

        if(newCell != cell)
        {
            GridRegistered[world].removeBotFromCell(cell, id)

            cell = newCell;
            GridRegistered[world].ajustBotToCell(cell, id)
        }
        position = {x = x, y = y, z = z};
        packetManager.sendSynchronization();

        callComponent("positionChange")
    }

    function addComponent(component) {
        component.idx = this.components.len()
        this.components.push(component)

        component.parent = this;

        return component
    }

    function removeComponent(component) {
        if (component.idx == -1)
            return

        local lenght = this.components.len()
        this.components[components.idx] = this.components[lenght - 1]
        this.components.pop();

        component.idx = -1
    }

    function clearComponents() {
        this.components.clear()
    }

    function callComponent(event, ...) {
        foreach(component in this.components) {
            if(event in component.events) {
                local arr = clone vargv;
                arr.insert(0, component);
                component[event].acall(arr);
            }
        }
    }

    function hasComponent(_component) {
        foreach(component in this.components) {
            if(component instanceof _component)
                return true;
        }
        return false;
    }

    function recognizeType(targetId, targetType)
    {
        if(targetType == BotTargetType.Human)
            return getPlayer(targetId);

        return getBot(targetId);
    }

    function onGetDamage(targetId, targetType = null, giveDamage = true) {
        if(targetId == moduleCache.ownerId){
            local player = getPlayer(targetId);
            local weaponMode = player.getWeaponMode();
            if(weaponMode == WEAPONMODE_FIST){
                hp = 0;
                callComponent("onDeath", targetId, targetType);
            }
            return false;
        }

        if(targetType == null)
            targetType = BotTargetType.Human;

        if(targetType == BotTargetType.Human)
        {
            if(getPlayer(targetId).lastHitDelay > getTickCount()) {
                eventValue(0);
                return false;
            }

            getPlayer(targetId).lastHitDelay = getTickCount() + 220;
        }

        if(giveDamage) {
            local dmg = DMGCalculation.calculateDamage(recognizeType(targetId, targetType), this);
            hp = hp - dmg;
        }

        local counter = 0;
        local botsNearest = GridRegistered[world].nearestBots(cell,CELL_SIZE_WITH_MODE);
        foreach(botId, nbmBool in botsNearest) {
            if(counter >= 3)
                break;

            if(botId != id) {
                local _bot = bots[botId];
                if(_bot.positionDifference(position) > 500)
                    continue;

                local isMine = false;
                if(targetId == _bot.moduleCache.ownerId)
                    isMine = true;

                if(!isMine) {
                    _bot.callComponent("onNearAttack",targetId, this);
                    counter = counter + 1;
                }
            }
        }

        callComponent("onDirectAttack", targetId, targetType);

        if(hp <= 0) {
            hp = 0;
            resetDirection();

            if(targetId < getMaxSlots())
                addPlayerLog(targetId, "Zabi� bota o nazwie " + name);

            callComponent("onDeath", targetId, targetType);
            if(targetType == BotTargetType.Human)
            {
                callEvent("onNpcDead", targetId, this);
            }
            else
            {
                local _bot = getBot(targetId);
                if(_bot.moduleCache.ownerId != -1)
                {
                    callEvent("onNpcDead", _bot.moduleCache.ownerId, this);
                }
            }
        }

        packetManager.sendHealth();
    }

    /**
    * Method to send health value to clients
    * @param int value
    */
    function setHealth(value) {
        hp = value;
        packetManager.sendHealth();
    }

    function setMaxHealth() {
        hp = hpMax;
        packetManager.sendHealth();
    }

    /**
    * Method used to detect interaction
    * @param int player id
    */
    function tryInteract(playerId) {
        if(hasComponent(BotDialogComponent) == false)
            return;

        foreach(component in this.components)
        {
            if(component instanceof BotCombatComponent)
            {
                component.onPlayerBeforeInteract(playerId);
                return;
            }
        }

        callComponent("onPlayerInteract", playerId);
    }

    /**
    * Method used to detect dialog call
    * @param int player id
    * @param int dialog id
    */
    function dialogIdCall(playerId, dialogId) {
        foreach(component in this.components)
        {
            if(component instanceof BotDialogComponent)
            {
                component.onPlayerCallDialog(playerId, dialogId);
                return;
            }
        }
    }

    /**
    * Method gives player item from drop
    *
    * @param playerId int
    */
    function interactWithDeadBody(playerId) {
        // if(hp != 0)
        //     return;

        // if(isTakken != false)
        //     return;

        // foreach(component in this.components)
        // {
        //     if(component instanceof BotDropComponent)
        //     {
        //         component.onSecure(playerId);
        //         isTakken = true;
        //         packetManager.sendTakken();
        //         return;
        //     }
        // }
    }

    /**
    * Method returning all available protection methods
    *
    * @return table of items
    */
    function getProtectionAvailableItems() {
        local table = [];

        if(armor != -1)
            table.append(armor);

        if(helmet != -1)
            table.append(helmet);

        if(shield != -1)
            table.append(shield);

        return table;
    }

    /**
    * Method returning protection from damage in server
    *
    * @return table of protection
    */
    function getProtection() {
        local protection = [0,0,0,0,0,0,0,0,0];

        foreach(itemId in getProtectionAvailableItems())
        {
            if(itemId == null)
                continue;

            local item = getItemScheme(Items.name(itemId).toupper());
            foreach(index, value in item.protection)
                protection[index] += value;
        }

        return protection;
    }

    function resistanceCallback() {}

    /**
    * Method triggers when player get attacked by this bot
    *
    * @param playerId
    */
    function onAttackPlayer(playerId) {
        local pObject = getPlayer(playerId);
        local dmg = DMGCalculation.calculateDamage(this, pObject);

        moduleCache.hitWithoutAttack = 0;
        local health = getPlayerHealth(playerId);
        health = health - dmg;
        if(health <= 0) {
            health = 0;
            //PlayerPacket(playerId).randomlyRespawn();
            if(playerId < getMaxSlots())
                addPlayerLog(playerId, "Zabi� gracza bot o nazwie " + name);
        }

        local botsOwningByPlayers = getBotPlayer(playerId).owningBots;
        foreach(botId in botsOwningByPlayers) {
            local _bot = getBot(botId);
            if(_bot == null)
                continue;

            _bot.callComponent("onDirectAttack", id, BotTargetType.Npc);
        }

        callComponent("onTargetAttack", playerId, BotTargetType.Human);
        setPlayerHealth(playerId, health);
    }

    /**
    * Method turning angle into position only if there's difference
    *
    * @param position - table {x, y, z}
    */
    function turnIntoPosition(pos)
    {
        local _angle = getVectorAngle(position.x,position.z,pos.x,pos.z);
        local angleDiff = abs(_angle - angle);

        if(angleDiff > 10)
            setAngle(_angle);
    }

    /**
    * Get eye area of bot
    *
    * @param range - distance in witch we wanna to create eye area
    * @return position - table {x, y, z}
    */
    function getEyeArea(rangeView)
	{
		local rot = angle;
		local pos = clone position;

		if(rot < 0)
			rot += (ceil(abs(rot)/360.0))*360;
        else
			rot -= (floor(rot/360))*360.0;

		if(rot%90==0)
		{
			if(rot == 0)		pos.z += rangeView;
			else if(rot == 90)	pos.x += rangeView;
			else if(rot == 180)	pos.z -= rangeView;
			else if(rot == 270)	pos.x -= rangeView;
		}
		else
		{
			local mrot = (floor(rot/90))*90;
			local a = cos((rot - mrot) * ( PI / 180 )) * rangeView;
			local b = sin((rot - mrot) * ( PI / 180 )) * rangeView;

			if(rot > 270)		{ pos.x -= a; pos.z += b;}
			else if(rot > 180)	{ pos.x -= b; pos.z -= a;}
			else if(rot > 90)	{ pos.x += a; pos.z -= b;}
			else if(rot > 0)	{ pos.x += b; pos.z += a;}
		}

		return pos;
	}

    /**
    * Jump forward if bot is stucked
    */
    function jumpForward() {
        local currentPosition = clone position;
        local rotationX = angle;

        currentPosition.x = currentPosition.x + (sin(rotationX * 3.14 / 180.0) * 30);
        currentPosition.z = currentPosition.z + (cos(rotationX * 3.14 / 180.0) * 30);

        setPosition(currentPosition.x, currentPosition.y + 120, currentPosition.z);

        if(instance == "PC_HERO")
            playAnimation("T_RUNL_2_JUMP");
        else
            playAnimation("T_FISTRUNL_2_JUMP");
    }

    /**
    * Move forward if bot is stucked simple to bot
    */
    function moveForward(playerId) {
        local currentPosition = getPlayerPosition(playerId);
        setPosition(currentPosition.x + 40, currentPosition.y, currentPosition.z + 120);
        if(instance == "PC_HERO")
            playAnimation("T_RUNL_2_JUMP");
        else
            playAnimation("T_FISTRUNL_2_JUMP");
    }

    /**
    * Move into direction
    *
    * @param x - position x axis
    * @param y - position y axis
    * @param z - position z axis
    */
    function setDirection(x,y,z,distance,mode) {
        direction = {x = x, y = y, z = z, distance = distance, mode = mode}
        packetManager.sendDirection();
    }

    /**
    * Reset direction
    */
    function resetDirection() {
        if(direction.x == 0 && direction.z == 0)
            return;

        direction = {x = 0, y = 0, z = 0, distance = 0, mode = 0}
        packetManager.sendResetDirection();
    }

    /**
    * Reset direction
    */
    function onGetResetDirection() {
        if(direction.x == 0 && direction.z == 0)
            return;

        moduleCache.hitWithoutAttack = 0;

        direction = {x = 0, y = 0, z = 0, distance = 0, mode = 0}
        packetManager.sendResetDirection();
        setTickCount(0);
    }

    /**
    * Set spawn position for bot
    *
    * @param x - position x axis
    * @param y - position y axis
    * @param z - position z axis
    */
    function setSpawnPosition(x,y,z) {
        spawnPosition = {x = x, y = y, z = z}
    }

    /**
    * Add faction too bot register
    *
    * @param faction - BotFaction object
    */
    function addFaction(faction) {
        callComponent("onAddFaction", faction.id);
    }

    /**
    * Remove faction from bot register
    *
    * @param faction - BotFaction object
    */
    function removeFaction(faction) {
        callComponent("onRemoveFaction", faction.id);
    }

    /**
    * Checks 3d difference in position given with param and bot position
    *
    * @param positionToCompare - table {x, y, z}
    * @return distance
    */
    function positionDifference(positionToCompare) {
        return getDistance3d(position.x, position.y, position.z, positionToCompare.x, positionToCompare.y, positionToCompare.z);
    }

    /**
    * Checks 2d difference in position given with param and bot position
    *
    * @param positionToCompare - table {x, y, z}
    * @return distance
    */
    function simplePositionDifference(positionToCompare) {
        return getDistance2d(position.x, position.z, positionToCompare.x, positionToCompare.z);
    }

    /**
    * Method to check wheter bot is targetable
    */
    function isTargetable() {
        return hp > 0;
    }

    /**
    * Method to set scale of bot
    *
    * @param x - player x axis scale
    * @param y - player y axis scale
    * @param z - player z axis scale
    */
    function setScale(x,y,z) {
        scale = {x = x, y = y, z = z};
        packetManager.sendScale();
    }

    /**
    * Method returning scale of bot
    */
    function getScale() {
        return scale;
    }

    /**
    * Method to set fatness of bot
    *
    * @param fatness - player fatness axis scale
    */
    function setFatness(value) {
        if(fatness == value)
            return;

        fatness = value;
        packetManager.sendFatness();
    }

    /**
    * Method returning fatness of bot
    */
    function getFatness() {
        return fatness;
    }

    /**
    * Method to set angle of bot
    *
    * @param angle - player new angle
    */
    function setAngle(value) {
        if(angle == value)
            return;

        angle = value;
        packetManager.sendAngle();
    }

    /**
    * Method returning angle of bot
    */
    function getAngle() {
        return angle;
    }

    /**
    * Method to set weapon mode of bot
    *
    * @param weaponMode - player new weapon mode
    */
    function setWeaponMode(value) {
        if(weaponMode == value)
            return;

        weaponMode = value;
        packetManager.sendWeaponMode();
    }

    /**
    * Method returning weapon mode of bot
    */
    function getWeaponMode() {
        return weaponMode;
    }

    /**
    * Method returning weapon mode melee
    */
    function getMeleeWeapon() {
        return getItemScheme(Items.name(melee).toupper());
    }

    /**
    * Method returning weapon mode ranged
    */
    function getRangedWeapon() {
        return getItemScheme(Items.name(ranged).toupper());
    }

    /**
    * Method returning weapon mode magic
    */
    function getMagicWeapon() {
        return getItemScheme(Items.name(magic).toupper());
    }

    /**
    * Method that adds effect visual with given time to npc
    */
    function addEffect(effectId, refreshTime) {
        effects.push(effectId);
        System.Effect.add(id, SystemEffectTarget.Npc, effectId, refreshTime)
    }

    /**
    * Method that remove effect visual with given time to npc
    */
    function removeEffect(effectId) {
        local index = effects.find(effectId);
        effects.remove(index);
        System.Effect.onNpcEffectRemove(id, effectId);
    }

    /**
    Helper for dmg calculation method
    @param int attribute id
    */
    function getAttribute(attributeId) {
        switch(attributeId) {
            case PlayerAttributes.Str:
                return str;
            break;
            case PlayerAttributes.Dex:
                return dex;
            break;
            case PlayerAttributes.Int:
                return 100;
            break;
            case PlayerAttributes.MagicLvl:
                return magicLvl;
            break;
            case PlayerAttributes.Mana:
                return mana;
            break;
            case PlayerAttributes.Hp:
                return hp;
            break;
            case PlayerAttributes.OneH:
                return weapon[0];
            break;
            case PlayerAttributes.TwoH:
                return weapon[1];
            break;
            case PlayerAttributes.Bow:
                return weapon[2];
            break;
            case PlayerAttributes.Cbow:
                return weapon[3];
            break;
            case PlayerAttributes.Stamina:
                return 0;
            break;
        }
        return 0;
    }

    /**
    * Method to destroy bot
    */
    function destroyBot() {
        // Send destroy
        packetManager.destroyBot();

        // Undo all components
        clearComponents();

        // Remove bot from cell
        GridRegistered[world].removeBotFromCell(cell, id);

        // Remove from ticks
        if(id in ticks)
            ticks.rawdelete(id);

        // Remove bot from players scene
        BotPlayer.removeBotFromScene(id);

        // Destroys from register
        bots.rawdelete(id);
    }
}

/**
* Timer to call every component in bots containing method onUpdate
*/
setTimer(function() {

    foreach(botId, tick in ticks) {
        if(tick <= getTickCount()) {
            if(botId in bots)
                bots[botId].callComponent("onUpdate")
            else {
                ticks.rawdelete(botId);
                break;
            }
        }
    }
}, 250, 0);



// FUNCTIONS
getBots <- @() bots;

function getBot(id) {
    if(id in bots)
        return bots[id];

    return null;
}
