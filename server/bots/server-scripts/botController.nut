
class BotController
{
    static players = [];

    static function onInit() {
        for(local i = 0; i <= getMaxSlots(); i ++)
            players.push(BotPlayer(i));
    }

    static function onPacket(playerId, packet)
    {
        local typeId = packet.readUInt8();
        if(typeId != PacketBots)
            return;

        switch(packet.readUInt8())
        {
            case BotPackets.Synchronization:
                local bots = getBots();
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++)
                {
                    local botId = packet.readInt16();

                    local posX = packet.readFloat();
                    local posY = packet.readFloat();
                    local posZ = packet.readFloat();

                    if(botId in bots) {
                        local bot = bots[botId];
                        local posDiff = bot.positionDifference({x = posX, y = posY, z = posZ});

                        if(posDiff > 500) {
                            /*
                            local text = posDiff + " " + getPlayerName(playerId) + " | " + getPlayerMacAddr(playerId) + " | " + getPlayerSerial(playerId) + " | " + bot.instance;
                            local fileObject = file("database/stack/botpos.txt", "a+");
                            fileObject.write(text+"\n");
                            fileObject.close();
                            */
                        }else {
                            if(bot.moduleCache.streamerId == playerId)
                                bots[botId].setPosition(posX, posY, posZ);
                        }
                    }
                }
            break;
            case BotPackets.AttackPlayer:
                local bots = getBots();
                local id = packet.readInt16();
                local idPlayer = packet.readInt16();

                if(id in bots)
                    bots[id].onGetDamage(idPlayer);
            break;
            case BotPackets.AttackBot:
                local bots = getBots();
                local id = packet.readInt16();
                local idPlayer = packet.readInt16();

                if(id in bots)
                    bots[id].onAttackPlayer(idPlayer);
            break;
            case BotPackets.Interaction:
                local bots = getBots();
                local id = packet.readInt16();
                if(id in bots)
                    bots[id].tryInteract(playerId);
            break;
            case BotPackets.CallDialog:
                local bots = getBots();
                local id = packet.readInt16();
                local idDialog = packet.readInt16();
                if(id in bots)
                    bots[id].dialogIdCall(playerId, idDialog);
            break;
            case BotPackets.InteractDeadBody:
                local bots = getBots();
                local id = packet.readInt16();
                if(id in bots)
                    bots[id].interactWithDeadBody(playerId);
            break;
            case BotPackets.JumpForward:
                local bots = getBots();
                local id = packet.readInt16();
                if(id in bots)
                    bots[id].jumpForward();
            break;
            case BotPackets.MoveForward:
                local bots = getBots();
                local id = packet.readInt16();
                if(id in bots)
                    bots[id].moveForward(packet.readInt16());
            break;
            case BotPackets.ResetDirection:
                local bots = getBots();
                local id = packet.readInt16();
                if(id in bots)
                    bots[id].onGetResetDirection();
            break;
        }
    }

    static function onPlayerChangeCell(playerId, newCell, oldCell)
    {
        local playerBots = players[playerId].visibleBots
        local newBots = GridRegistered[getPlayerWorld(playerId)].nearestBots(newCell)
        local bots = getBots();

        foreach(playerBot, bool in playerBots)
        {
            if(!(playerBot in newBots))
            {
                players[playerId].visibleBots.rawdelete(playerBot);
                bots[playerBot].callComponent("playerExitRange", playerId)
            }
        }

        foreach(addBot, bool in newBots)
        {
            if(!(addBot in playerBots))
            {
                players[playerId].visibleBots[addBot] <- true;
                bots[addBot].callComponent("playerEnterRange", playerId)
            }
        }
    }

    static function onPlayerEnterWorld(playerId, world)
    {
        local playerBots = players[playerId].visibleBots
        local bots = getBots();

        foreach(playerBot, bool in playerBots)
        {
            players[playerId].visibleBots.rawdelete(playerBot);
            bots[playerBot].callComponent("playerExitRange", playerId)
        }
    }

    static function onPlayerChangeWorld(playerId, world)
    {
        local pos = getPlayerPosition(playerId);

        if(world in GridRegistered)
        {
            local newCell = GridRegistered[world].getIndex(pos.x, pos.z);
            local oldCell = getPlayer(playerId).cell;

            getPlayer(playerId).cell = newCell;
            callEvent("onPlayerChangeCell", playerId, newCell, oldCell);
        }
    }

    static function onPlayerDisconnect(playerId, reas) {
        local playerBots = players[playerId].visibleBots
        local bots = getBots();

        foreach(playerBot, bool in playerBots)
        {
            players[playerId].visibleBots.rawdelete(playerBot);
            bots[playerBot].callComponent("playerExitRange", playerId)
        }

        players[playerId].doneDialogs.clear();
    }

    static function onPlayerLoad(playerId) {
        players[playerId].loadDoneDialogs();
    }
}


getBotPlayer <- @(id) BotController.players[id];
getBotPlayers <- @() BotController.players;

addEventHandler("onInit", BotController.onInit.bindenv(BotController));
addEventHandler("onPacket", BotController.onPacket.bindenv(BotController));
addEventHandler("onPlayerDisconnect", BotController.onPlayerDisconnect.bindenv(BotController));
addEventHandler("onPlayerChangeCell", BotController.onPlayerChangeCell.bindenv(BotController));
addEventHandler("onPlayerLoad", BotController.onPlayerLoad.bindenv(BotController));
addEventHandler("onPlayerEnterWorld", BotController.onPlayerEnterWorld.bindenv(BotController));
addEventHandler("onPlayerChangeWorld", BotController.onPlayerChangeWorld.bindenv(BotController));