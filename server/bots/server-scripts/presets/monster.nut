
class BotMonster extends BotBase
{
    routine = null;

    constructor(name, group = "", faction = "BEAST")
    {
        base.constructor(name);

        this.group = group;
        this.name = name;

        addComponent(BotAnimComponent());
        addComponent(BotStreamComponent());
        addComponent(BotSpawnComponent());
        addComponent(BotFactionComponent());
        addComponent(BotWanderingComponent());
        addComponent(BotCombatComponent());

        //add for faster reach
        routine = addComponent(BotRoutineComponent());

        this.statistics = BEAST_COMBAT;

        moduleCache.attackDistance.min = statistics.attackMinDist;
        moduleCache.attackDistance.max = statistics.attackMaxDist;

        //factions
        if(faction == "BEAST")
            this.addFaction(BEAST_FACTION);
        else
            this.addFaction(SHEEP_FACTION);

    }

    function addRoutine(routineObject) {
        return routine.add(routineObject)
    }

    function addInstantDrop(instance) {
        addComponent(BotInstantDropComponent(instance));
    }

    function addDrop(instance) {
        addComponent(BotDropComponent(instance));
    }
}