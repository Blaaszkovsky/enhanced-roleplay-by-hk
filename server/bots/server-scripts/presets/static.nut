
class BotStatic extends BotBase
{
    constructor(name, group = "")
    {
        base.constructor(name);

        this.group = group;
    }
}