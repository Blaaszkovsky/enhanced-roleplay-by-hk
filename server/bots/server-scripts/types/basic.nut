
BASIC_COMBAT <- BotCombatType();

BASIC_COMBAT.useEyeView = false;
BASIC_COMBAT.useSmell = false;

BASIC_COMBAT.weaponDrawModeEnable = true;
BASIC_COMBAT.maxDistanceChase = 5000;
BASIC_COMBAT.maxOtherEnemyAttacks = 3;

BASIC_COMBAT.warnEnable = true;
BASIC_COMBAT.warnTime = 6000;
BASIC_COMBAT.warnMaxDistance = 1500;
BASIC_COMBAT.warnMinDistance = 900;

BASIC_COMBAT.jumpBackActive = true;
BASIC_COMBAT.jumpBackChance = 20;
BASIC_COMBAT.jumpBackTime = 400;

BASIC_COMBAT.blockActive = true;
BASIC_COMBAT.blockChance = 20;
BASIC_COMBAT.blockTime = 600;

BASIC_COMBAT.attackDelay = 1000;
BASIC_COMBAT.attackMinDist = 150;
BASIC_COMBAT.attackMaxDist = 260;

BASIC_COMBAT.jumpSideActive = true;
BASIC_COMBAT.jumpSideChance = 10;
BASIC_COMBAT.jumpSideTime = 500;