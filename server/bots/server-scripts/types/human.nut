
HUMAN_COMBAT <- BotCombatType();

HUMAN_COMBAT.useEyeView = true;
HUMAN_COMBAT.useSmell = false;

HUMAN_COMBAT.weaponDrawModeEnable = true;
HUMAN_COMBAT.maxDistanceChase = 5000;
HUMAN_COMBAT.maxOtherEnemyAttacks = 3;

HUMAN_COMBAT.warnEnable = false;
HUMAN_COMBAT.warnTime = 6000;
HUMAN_COMBAT.warnMaxDistance = 1500;
HUMAN_COMBAT.warnMinDistance = 900;

HUMAN_COMBAT.jumpBackActive = true;
HUMAN_COMBAT.jumpBackChance = 20;
HUMAN_COMBAT.jumpBackTime = 400;

HUMAN_COMBAT.blockActive = true;
HUMAN_COMBAT.blockChance = 20;
HUMAN_COMBAT.blockTime = 600;

HUMAN_COMBAT.attackDelay = 1000;
HUMAN_COMBAT.attackMinDist = 150;
HUMAN_COMBAT.attackMaxDist = 260;

HUMAN_COMBAT.jumpSideActive = true;
HUMAN_COMBAT.jumpSideChance = 10;
HUMAN_COMBAT.jumpSideTime = 500;