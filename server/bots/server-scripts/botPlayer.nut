
class BotPlayer
{
    dialogsTable = "playerdialog";

    id = -1;

    visibleBots = null;
    doneDialogs = null;
    owningBots = null;

    constructor(id)
    {
        this.id = id;

        this.visibleBots = {};
        this.doneDialogs = {};
        this.owningBots = [];
    }

    function addDialog(identifier, timestamp = null)
    {
        if(timestamp == null)
            timestamp = time();

        local rId = getPlayer(id).rId;
        if(identifier in doneDialogs) {
            doneDialogs[identifier] = timestamp;
            Query().update(BotPlayer.dialogsTable, ["timestamp"], [timestamp]).where(["playerId = "+rId+" AND identifier = '"+identifier+"'"]).execute();
        }
        else {
            doneDialogs[identifier] <- timestamp;
            Query().insertInto(BotPlayer.dialogsTable, ["identifier", "timestamp", "playerId"], ["'"+identifier+"'", timestamp, rId]).execute();
        }
    }

    function hasDialog(identifier)
    {
        if(identifier in doneDialogs)
            return doneDialogs[identifier];

        return null;
    }

    function removeDialog(identifier)
    {
        if(identifier in doneDialogs) {
            doneDialogs.rawdelete(identifier);
            Query().deleteFrom(BotPlayer.dialogsTable).where(["identifier = "+identifier+" AND playerId = "+getPlayer(id).rId]).execute();
        }
    }

    function saveDoneDialogs() {
        local rId = getPlayer(id).rId;
        Query().deleteFrom(BotPlayer.dialogsTable).where(["playerId = "+rId]).execute();

        if(doneDialogs.len() > 0)
        {
            local arr = [];

            foreach(group, _group in doneDialogs) {
                foreach(npcname, _npcname in _group) {
                    foreach(label in _npcname) {
                        foreach(timestamp in label)
                            arr.append(["'"+group+"'", "'"+npcname+"'", "'"+label+"'", rId, "'"+timestamp+"'"]);
                    }
                }
            }

            Query().insertInto(BotPlayer.dialogsTable, ["npcgroup", "npcname", "label", "playerId", "timestamp"], arr).execute();

        }
    }

    function loadDoneDialogs() {
        local rId = getPlayer(id).rId;

        foreach(data in Query().select().from(BotPlayer.dialogsTable).where(["playerId = "+rId]).all())
            doneDialogs[data["identifier"]] <- data["timestamp"].tointeger();
    }

    function isFractionAllie(bot) {
        if(!bot.hasComponent(BotFactionComponent))
            return false;

        foreach(_component in bot.components)
            if(_component instanceof BotFactionComponent)
                return _component.isPlayerAllie(id);

        return false;
    }

    function hasFaction(factionObj) {
        local factions = getPlayer(id).getPlayerFactions();
        return factions.find(factionObj) != null;
    }

    static function removeBotFromScene(id) {
        foreach(player in BotController.players) {
            if(id in player.visibleBots)
                player.visibleBots.rawdelete(id);
        }
    }
}

