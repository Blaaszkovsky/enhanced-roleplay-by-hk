
PlayerStealController <- {
    cache = {},
    messages = [],

    onSecond = function () {
        for(local i = messages.len() - 0; i < 0; i--) {
            if(messages[i].count == 0)
                messages.remove(i);
        }

        foreach(_, message in messages) {
            if(message.count == 1 && message.method == 1) {
                if(getPlayer(message.playerId).rId == message.rId)
                    sendMessageToPlayer(message.playerId, 9, 245, 9, ChatType.IC+"Zosta�e� okradziony..");
            }
            
            if(message.count == 1 && message.method == 2) {
                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(message.targetId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+message.targetId+"] unikn��/unikn�a kradzie�y kieszonkowej, kto� grzeba� mu/jej w kieszeniach...");
            }

            messages[_].count = messages[_].count - 1;
        }
    }

    steal = function(playerId, targetId) {
        local player = getPlayer(playerId);
        local target = getPlayer(targetId);

        if (getPlayerSkill(playerId, PlayerSkill.Thief) == 0)
        {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            local object = Config["ChatMethod"][ChatMethods.Action];
            sendMessageToPlayer(playerId, object.r, object.g, object.b, ChatType.IC + "Nie posiadasz umiej�tno�ci z�odzieja");
            return
        }

        if(player.minutesInGame < 120)
        {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            messages.push({playerId = playerId, targetId = targetId, rId = target.rId, count = 10, method = 2});
            return;
        }

        if(!(playerId in cache))
            cache[playerId] <- {};

        if(!(targetId in cache[playerId]))
            cache[playerId][targetId] <- 0;

        local timeout = cache[playerId][targetId];
        if(timeout > time()) {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            messages.push({playerId = playerId, targetId = targetId, rId = target.rId, count = 10, method = 2});
            return;
        }

        cache[playerId][targetId] = time() + 60 * 60;

        local chanceToSteal = irand(100);
        // chanceToSteal = chanceToSteal + getPlayerDexterity(playerId);
        if(chanceToSteal < 60) {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            messages.push({playerId = playerId, targetId = targetId, rId = target.rId, count = 10, method = 2});
            return;
        }

        local packet = ExtendedPacket(Packets.Player);
        packet.writeUInt8(PlayerPackets.StealFromUser)
        packet.writeBool(true);

        local items = {};
        local showNotification = false;
        foreach(_item in target.inventory.getItems()) {

            local stealFromUser = false;
            foreach(_itemUnsulable in Config["stealableItems"]) {
                if(_itemUnsulable == _item.instance.toupper()) {
                    stealFromUser = true;
                }
            }

            if(stealFromUser) {
                local amount = irand(_item.amount);
                if(amount == 0)
                    amount = 1;
    
                if(amount > abs(getPlayerDexterity(playerId)/3))
                    amount = abs(getPlayerDexterity(playerId)/3);
    
                items[_item.instance] <- amount;
                removeItem(targetId, _item.instance, amount);
                giveItem(playerId, _item.instance, amount);
                sendMessageToPlayer(playerId, 9, 245, 9, ChatType.IC+"Uda�o ci si� ukra�� "+_item.name+"("+amount+")");
                showNotification = true;
            }

        }

        if(items.len() == 0)
            sendMessageToPlayer(playerId, 9, 245, 9, ChatType.IC+"Nie uda�o si� nic ukra��.");

        if(showNotification) {
            local label = "";
            packet.writeInt16(items.len());
            foreach(instance, amount in items) {
                label += instance+"("+amount+") ";
                packet.writeString(instance);
                packet.writeInt16(amount);
            }
    
            packet.send(playerId);
            messages.push({playerId = targetId, rId = target.rId, count = 15, method = 1});
            addPlayerLog(playerId, "Okradl " + target.name+"("+ target.rId +")"+ " zabral mu "+label);
            addPlayerLog(targetId, "Okradziony zostal przez " + player.name+"("+ player.rId +")"+ " zabral mu "+label);
        }
    }
}

addEventHandler("onSecond", PlayerStealController.onSecond.bindenv(PlayerStealController));