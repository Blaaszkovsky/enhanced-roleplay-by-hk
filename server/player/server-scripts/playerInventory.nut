
class PlayerInventory extends item.Bucket
{
    melee = -1
    ranged = -1
    armor = -1
    shield = -1
    helmet = -1
    rings = -1
    belt = -1

    playerId = -1

    constructor(_objTypeId, _objId) {
        melee = -1
        ranged = -1
        armor = -1
        shield = -1
        helmet = -1
        rings = -1
        belt = -1

        playerId = -1

        base.constructor(_objTypeId, _objId);
    }

    function clear() {
        melee = -1
        ranged = -1
        armor = -1
        shield = -1
        helmet = -1
        rings = -1
        belt = -1

        ids.clear();
        item.Packet(this).resend();
    }

    function addItem(itemId) {
        local _item = getItem(itemId);
        if(_item == null)
            return null;

        if(_item.stacked == false) {
            _addItem(itemId);
            item.Packet(this).addItem(itemId);
        }
        else
        {
            foreach(_itemInBucket in getItems()) {
                if(_itemInBucket.instance != _item.instance)
                    continue;

                _itemInBucket.amount = _itemInBucket.amount + _item.amount;
                _itemInBucket.save();
                item.Controller.removeItem(itemId);
                item.Packet(this).resend();
                return;
            }

            _addItem(itemId);
            item.Packet(this).addItem(itemId);
        }
    }

    function removeItem(itemId) {
        if(melee == itemId) melee = -1;
        else if(ranged == itemId) ranged = -1;
        else if(armor == itemId) armor = -1;
        else if(shield == itemId) shield = -1;
        else if(helmet == itemId) helmet = -1;
        else if(rings == itemId) rings = -1;
        else if(belt == itemId) belt = -1;

        _removeItem(itemId);
        item.Packet(this).removeItem(itemId);
    }

    function getProtectionAvailableItems() {
        local table = [];

        if(armor != -1) table.append(getItem(armor));
        if(shield != -1) table.append(getItem(shield));
        if(helmet != -1) table.append(getItem(helmet));
        if(rings != -1) table.append(getItem(rings));
        if(belt != -1) table.append(getItem(belt));

        return table;
    }

    function getProtection() {
        local protection = [0,0,0,0,0,0,0,0,0];

        foreach(_item in getProtectionAvailableItems())
        {
            foreach(index, value in _item.getProtection())
                protection[index] += value;
        }

        return protection;
    }

    function callResistance() {
        foreach(_item in getProtectionAvailableItems())
        {
            if(_item.resistance > 0) {
                _item.resistance = itemUsedToGiveDmg.resistance - 1;
                _item.sendResistance(playerId);
            }
        }
    }
}