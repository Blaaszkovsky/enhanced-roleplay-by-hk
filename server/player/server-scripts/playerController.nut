
class PlayerController
{
    static players = [];

    static function onSecond() {
        // local counter = PerformanceCounter()
        // counter.start()

        // foreach(player in players)
        //     if(player.loggIn)
        //         player.onSecond();

        // counter.stop()
        // stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of onSecond player iteration.");
    }

    static function onMinute() {
        local realTime = date();
        local realHour = format("%02d",realTime.hour) + ":" + format("%02d",realTime.min);

        if(realHour == "04:30") {
            for(local i = 0; i <= getMaxSlots(); i++)
                if(isPlayerConnected(i))
                    setPlayerLearnPoints(i, getPlayerLearnPoints(i) + 10)

            DB.queryGet("UPDATE `player` SET `learnPoints` = `learnPoints` + 10");
            // DB.queryGet("UPDATE " + System.tableName + " SET `value` = `value` + 10 WHERE `type` = " + SystemTypes.PlayerLearnPoints);
            sendMessageToAll(115, 31, 31, ChatType.ALL + "Zosta�y nadane codziennie punkty nauki.");
        }
        foreach(player in players)
            if(player.loggIn)
                player.onMinute();
    }

    static function getLoggInPlayers()  {
        return players.filter(function(index, value) {
            return value.loggIn == true;
        });
    }

    static function getAllRealIdsPlayers() {
        return players.map(function(value) {
            if(value.loggIn == true)
                return value.rId;
        });
    }

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            players.push(Player(i));

        DB.query("UPDATE "+Player.dbTable+" SET online = 0;");
    }

    static function onPlayerTeleport(playerId, vobName) {
        kick(playerId, "Nie u�ywaj cheat�w :)");
    }

    static function onPlayerJoin(playerId) {
        if(getPlayerName(playerId) == "Nieznajomy")
            setPlayerName(playerId, "Gracz "+playerId);

        foreach(player in players){

            if(isPlayerConnected(player.id) == false)
                continue;


            local itemId = getPlayerHelmet(player.id)
            if(itemId > -1){
                local instance = Items.name(itemId);
                local _itemScheme = getItemScheme(instance);
                local flag = _itemScheme.flag;
                if(flag == ITEMCAT_MASK){
                    setPlayerMaskedOnce(player.id,true,playerId);
                } else {
                    setPlayerMaskedOnce(player.id,false,playerId);
                }
            } else {
                setPlayerMaskedOnce(player.id,false,playerId);
            }
        }
    }

    static function onExit(playerId, reason) {

        // Check player that exiting is carrying any person
        local carriedPerson = getPlayerCarrying(playerId);
        if(carriedPerson != -1) {
            setPlayerCarried(carriedPerson, -1);
            setPlayerCarrying(playerId, -1);
        }

        // Check player that exiting is carried by any person
        local carryingPerson = getPlayerCarried(playerId);
        if(carryingPerson != -1) {
            setPlayerCarried(playerId, -1);
            setPlayerCarrying(carryingPerson, -1);
        }

        BuffController.onPlayerDisconnect(playerId);
        TradeController.onBeforeDisconnectGame(playerId);

        //OnDisconnect
        players[playerId].save();

        callEvent("onPlayerUnLoad", playerId);
        players[playerId].clear();
    }

    static function onPlayerHit(playerId, killerId, dmg) {

        local isDmgBlocked = isDmBlockedForPlayer(killerId);
        if(isDmgBlocked || getPlayer(playerId).isGodModeEnabled()) {
            eventValue(0);
            return false;
        }

        local pObject = getPlayer(playerId);
        local kObject = getPlayer(killerId);

        if(kObject.loggIn == false || pObject.loggIn == false) {
            eventValue(0);
            return false;
        }

        if(kObject.inventory == null || pObject.inventory == null)
        {
            eventValue(0);
            return false;
        }

        if(kObject.lastHitDelay > getTickCount()) {
            eventValue(0);
            return false;
        }

        // Check player that get hit is carried by any person
        local carriedPerson = getPlayerCarrying(playerId);
        if(carriedPerson != -1) {
            setPlayerCarried(carriedPerson, -1);
            setPlayerCarrying(playerId, -1);
        }

        kObject.lastHitDelay = getTickCount() + 220;
        eventValue(-DMGCalculation.calculateDamage(kObject, pObject));
        if(getPlayerHealth(playerId) > 0)
            pObject.hp = getPlayerHealth(playerId) - DMGCalculation.calculateDamage(kObject, pObject);
    }

    static function onPlayerDead(playerId, killerId) {

        // When player is dead he cant carry anyone
        local carriedPerson = getPlayerCarrying(playerId);
        if(carriedPerson != -1) {
            setPlayerCarried(carriedPerson, -1);
            setPlayerCarrying(playerId, -1);
        }

        BuffController.resetBuffsForPlayer(playerId);
        players[playerId].onDead(killerId);
        
        sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s zabi� %s", "Gracz "+killerId, "gracza "+playerId));
    }

    static function onPlayerUseCheat(playerId, cheatTypeId) {
        kick(playerId, "Nie u�ywaj cheat�w :)");
    }

    static function onPlayerRespawn(playerId) {
        spawnPlayer(playerId);
        respawnPlayer(playerId);

        players[playerId].onRespawn();
        setPlayerHealth(playerId, 5);
        setPlayerMana(playerId, 5);
    }

    static function onAccountLoad(playerId) {
        local accountObject = getAccount(playerId);
        local playerCharacters = Player.getAccountPlayers(accountObject.rId);

        foreach(data in playerCharacters) {
            PlayerPacket(playerId).sendCharacter(data["id"],data["name"],data["bodyModel"],data["bodyTxt"],data["headModel"],data["headTxt"],data["armor"],data["melee"],
            data["ranged"],data["str"],data["dex"],data["health"],data["maxHealth"],data["mana"],data["maxMana"],data["statusId"],data["learnPoints"],
            data["minutesInGame"],data["createdAt"],data["updatedAt"])
        }
    }

    static function onPlayerChangeWeaponMode (playerid, oldWeaponMode, newWeaponMode) {
        // When player change weapon we check is he carry someone
        local carriedPerson = getPlayerCarrying(playerid);
        if(carriedPerson != -1) {
            setPlayerCarried(carriedPerson, -1);
            setPlayerCarrying(playerid, -1);
        }

        local shieldEquiped = getPlayerShield(playerid) != -1;
        setTimer(function () {
            if(newWeaponMode == WEAPONMODE_1HS){
                local skill = getPlayerSkillWeapon(playerid,WEAPON_1H)
                local itemId = getPlayerMeleeWeapon(playerid);
                local instance = Items.name(itemId);
                if(shieldEquiped){
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("Shield_ST4.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("Shield_ST3.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("Shield_ST2.MDS"));
                    }
                } else if (instance.find("RAPIER") != null) {
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_RAPIER_ST3.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_RAPIER_ST2.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_RAPIER_ST1.MDS"));
                    }
                } else {
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_1HST2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_1HST1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS.MDS"));
                    }
                }

            }
            if(newWeaponMode == WEAPONMODE_2HS){
                local skill = getPlayerSkillWeapon(playerid,WEAPON_2H);
                local itemId = getPlayerMeleeWeapon(playerid);
                local instance = Items.name(itemId);
                if(instance.find("STAFF") != null || instance.find("POLE") != null){
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT0.MDS"));
                    }
                } else {
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_2HST2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_2HST1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS.MDS"));
                    }
                }
            }
        },400,1)
    }

    static function onPlayerEquipHelmet(playerid,itemId) {
        if(itemId > -1){
            local instance = Items.name(itemId);
            local _item = getItemScheme(instance.toupper());

            local flag = _item.flag;
            if(flag == ITEMCAT_MASK){
                setPlayerMaskedForAll(playerid,true);
            } else {
                setPlayerMaskedForAll(playerid,false);
            }
        } else {
            setPlayerMaskedForAll(playerid,false);
        }
    }

    static function onPlayerShoot(playerId)
    {
        local rangedWeapon = getPlayerRangedWeapon(playerId);

        if(rangedWeapon == -1)
            return;

        local munitionInstance = ""
        rangedWeapon = getItemScheme(Items.name(rangedWeapon));

        if(rangedWeapon.flag == ITEMCAT_BOW)
            munitionInstance = "ITRW_ARROW"
        else if(rangedWeapon.flag == ITEMCAT_CBOW)
            munitionInstance = "ITRW_BOLT"
        else
        {
            kick(playerId, "onPlayerShoot " + Items.name(rangedWeapon) + " munition error")
            return
        }

        if (hasPlayerItem(playerId, munitionInstance) < 1)
            return

        _giveItem(playerId, Items.id(munitionInstance), 1);
        removeItem(playerId, munitionInstance, 1);
    }

    static function register(playerId, name)
    {
        local playerObject = players[playerId];

        if(playerObject.rId != -1)
            return;

        name = mysql_escape_string(name);

        local alredyExist = Player.findByUsername(name);
        if(alredyExist != null) {
            addPlayerNotification(playerId, "Taki u�ytkownik ju� istnieje.");
            return;
        }

        if (getAdmin(playerId).role < PlayerRole.Helper)
        {
            local charactersAmount = Player.getAccountPlayersBySerial(getPlayerSerial(playerId));
            if(charactersAmount.len() > 1) {
                addPlayerNotification(playerId, "Nowe konto nie zwi�kszy limitu postaci.");
                return;
            }

            charactersAmount = Player.getAccountPlayers(getAccount(playerId).rId);
            if(charactersAmount.len() > 1) {
                addPlayerNotification(playerId, "Masz ju� 2 postacie stworzone.");
                return;
            }
        }

        playerObject.name = name;
        PlayerController.load(playerId, playerObject.register());

        callEvent("onPlayerRegister", playerId);
    }

    static function load(playerId, characterId)
    {
        local playerObject = players[playerId];

        if(playerObject.loggIn)
            return;

        playerObject.load(characterId);
        PlayerPacket(playerId).acceptCharacter();

        callEvent("onPlayerLoad", playerId);
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();
        switch(typ)
        {
            // Statistics
            case PlayerPackets.Scale:
                local playerId = packet.readInt16();
                setPlayerScale(playerId, packet.readFloat(), packet.readFloat(), packet.readFloat())
            break;
            case PlayerPackets.WalkingStyle:
                setPlayerWalkingStyle(packet.readInt16(), packet.readInt16())
            break;
            case PlayerPackets.Visual:
                setPlayerVisual(playerId, packet.readString(), packet.readInt16(), packet.readString(), packet.readInt16())
            break;
            case PlayerPackets.Fatness:
                setPlayerFatness(packet.readInt16(), packet.readInt16());
            break;
            case PlayerPackets.Upgrade:
                upgradePlayerAttribute(playerId, packet.readInt16());
            break;
            case PlayerPackets.SkillUpgrade:
                upgradePlayerSkill(playerId, packet.readInt16());
            break;
            case PlayerPackets.Fraction:
                getPlayer(playerId).sendFractionInfo(packet.readInt16());
            break;
            case PlayerPackets.Unconscious:
                getPlayer(playerId).resetUnconscious();
            break;
            case PlayerPackets.Carrying:
                local carriedPerson = getPlayerCarrying(playerId);
                if(carriedPerson != -1) {
                    setPlayerCarried(carriedPerson, -1);
                    setPlayerCarrying(playerId, -1);
                }
            break;
            case PlayerPackets.FractionInfo:
                local pObj = getPlayer(playerId);
                local membersToSend = {};
                if(pObj.fractionId != -1) {
                    local fractionObject = getFraction(pObj.fractionId);
                    local rank = fractionObject.ranks[pObj.fractionRoleId];
                    if(rank.canManage) {
                        local members = Query().select(["id", "name", "fractionRoleId"]).from(Player.dbTable).where(["fractionId = "+pObj.fractionId]).all()
                        foreach(member in members)
                            membersToSend[member.id] <- {rank = member.fractionRoleId, name = member.name}

                        foreach(_player in getPlayers()) {
                            if(_player.fractionId == pObj.fractionId) {
                                if(_player.rId in membersToSend)
                                    membersToSend.rawdelete(_player.rId);

                                membersToSend[_player.rId] <- {rank = _player.fractionRoleId, name = _player.name}
                            }
                        }
                    }
                }

                PlayerPacket(playerId).getPlayerFractionInfo(pObj.fractionId, pObj.fractionRoleId, membersToSend);
            break;
            case PlayerPackets.FractionManage:
                local playerFraction = getPlayer(packet.readInt16());

                PlayerPacket(playerId).getPlayerManageFractionPlayer(playerFraction.id, playerFraction.fractionId, playerFraction.fractionRoleId)
            break;
            case PlayerPackets.FractionRemove:
                local realPlayerId = packet.readInt32();
                Query().update(Player.dbTable, ["fractionId", "fractionRoleId"], [-1, -1]).where(["id = "+realPlayerId]).execute();
                foreach(player in getPlayers()) {
                    if(player.rId != -1 && player.rId == realPlayerId) {
                        setPlayerFraction(player.id, -1, -1);
                        break;
                    }
                }
            break;
            case PlayerPackets.Lute:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playLute(playerId, soundId, loopMode, volume);
                }
            break;
            case PlayerPackets.Flute:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playFlute(playerId, soundId, loopMode, volume);
                }
            break;				
            case PlayerPackets.Okarina:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playOkarina(playerId, soundId, loopMode, volume);
                }	
            break;				
            case PlayerPackets.Conga:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playConga(playerId, soundId, loopMode, volume);
                }	
            break;				
            case PlayerPackets.Harp:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playHarp(playerId, soundId, loopMode, volume);
                }				
            break;
            // Methods
            case PlayerPackets.UseItem:
                local itemId = packet.readUInt32();
                local itemObject = getItem(itemId);
                if(itemObject == null)
                    return;

                if(itemObject.equipped)
                    unequipItem(playerId, itemObject);
                else
                    equipItem(playerId, itemObject);
            break;
            case PlayerPackets.Speech:
                setPlayerSpeech(playerId, packet.readInt16());
            break;
            case PlayerPackets.ThrowItem:
                local itemId = packet.readUInt32();
                local amount = packet.readInt16();

                playerThrowItem(playerId, itemId, amount);
            break;
            case PlayerPackets.PersonalNotes:
                local tabPersonalNotes = [];
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    tabPersonalNotes.append(str)
                }

                setPlayerPersonalNotes(playerId, tabPersonalNotes);
            break;
            case PlayerPackets.PersonalCard:
                local tabPersonalCard = [];
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    tabPersonalCard.append(str)
                }

                setPlayerPersonalCard(playerId, tabPersonalCard);
            break;
            case PlayerPackets.RealName:
                local targetId = packet.readInt16();
                local name = packet.readString();
                if(targetId == -1)
                    return;

                name = mysql_escape_string(name);
                if(name == null)
                    name = "";

                local playerObject = getPlayer(playerId);
                local playerTargetObject = getPlayer(targetId);
                Query().deleteFrom(Player.relationsTable).where(["playerId = "+playerObject.rId+"", "relationPlayerId = "+playerTargetObject.rId+""]).execute();
                Query().insertInto(Player.relationsTable, ["playerId", "relationPlayerId", "name"], [
                    playerObject.rId, playerTargetObject.rId, "'"+name+"'"
                ]).execute();

                setPlayerRealName(playerId, targetId, name);
            break;
            case PlayerPackets.Ask:
                local askId = packet.readInt16();
                local askType = packet.readInt16();
                local askValue = packet.readString();

                if(getPlayerAskId(askId) != -1 || getPlayerAskId(playerId) != -1) {
                    PlayerPacket(playerId).rejectAsk();
                    return;
                }

                setPlayerAskId(playerId, askId);
                setPlayerAskId(askId, playerId);

                PlayerPacket(askId).sendAsk(playerId, askType, askValue);
            break;
            case PlayerPackets.StealFromUser:
                PlayerStealController.steal(playerId, packet.readInt16())
            break;
            case PlayerPackets.GetDescription:
                local focusId = packet.readInt16();
                local description = getPlayer(focusId).personalCard;

                PlayerPacket(playerId).sendPlayerDescription(focusId, description);
            break;
            case PlayerPackets.AcceptAsk:
                local askType = packet.readInt16();
                if(getPlayerAskId(playerId) == -1)
                    return;

                local askId = getPlayerAskId(playerId);

                PlayerPacket(askId).acceptAsk();
                PlayerPacket(playerId).acceptAsk();

                switch(askType) {
                    case PlayerAskType.Search:
                        PlayerPacket(askId).startSearch();

                        foreach(item in getPlayerItems(playerId))
                            PlayerPacket(askId).runSearch(item.instance, item.amount);

                        PlayerPacket(askId).endSearch();

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Przeszukuje [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.Trade:
                        TradeController.startTrade(playerId,askId);
                    break;
                    case PlayerAskType.Trap:
                        local instance = -1;
                        foreach(item in getPlayerItems(askId)) {
                            if(item.instance == "ITMI_NACPANY") {
                                instance = item.instance;
                                break;
                            }
                        }

                        if(instance == null) {
                            local object = Config["ChatMethod"][ChatMethods.Action];
                            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] nie posiada liny.");
                            return;
                        }

                        System.Bound.add(playerId);
                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Uwi�zi� [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.UnTrap:
                        System.Bound.remove(playerId);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Wypu�ci� [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.Invite:
                        setPlayerFraction(playerId, getPlayerFraction(askId), getFraction(getPlayerFraction(askId)).getFirstRank());
                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Do��czy� do frakcji [PLAYER:"+askId+"]");
                    break;
                    case PlayerAskType.Take:
                        local _player = getPlayer(playerId);
                        local _askPlayer = getPlayer(askId);

                        if(_askPlayer.unconscious != 0 || _askPlayer.carried != -1)
                            return;

                        if(_player.unconscious == 0) {
                            addPlayerNotification(askId, "Osoba, kt�r� chcesz podnie�� nie potrzebuje tego.");
                            return;
                        }

                        if(_player.carried != -1) {
                            addPlayerNotification(askId, "Kto� ju� niesie dan� osob�.");
                            return;
                        }

                        if(_askPlayer.carrying != -1) {
                            addPlayerNotification(askId, "Niesiesz ju� kogo�.");
                            return;
                        }

                        setPlayerCarried(playerId, askId);
                        setPlayerCarrying(askId, playerId);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Podni�s� [PLAYER:"+playerId+"].");
                    break;
                    case PlayerAskType.Heal:
                        local value = -1, instance = -1;
                        foreach(item in getPlayerItems(askId)) {
                            if(item.instance in Config["PlayerHeal"]) {
                                value = Config["PlayerHeal"][item.instance];
                                instance = item.instance;
                                break;
                            }
                        }

                        if(value == -1) {
                            local object = Config["ChatMethod"][ChatMethods.Action];
                            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Nie posiada ma�ci.");
                            return;
                        }

                        removeItem(askId, instance, 1);

                        local hp = getPlayerHealth(playerId);
                        hp = hp + value;

                        if(hp > getPlayerMaxHealth(playerId))
                            hp = getPlayerMaxHealth(playerId);

                        setPlayerHealth(playerId, hp);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Uleczy� [PLAYER:"+playerId+"]");
                    break;
                }

                setPlayerAskId(askId, -1);
                setPlayerAskId(playerId, -1);
            break;
            case PlayerPackets.RejectAsk:
                if(getPlayerAskId(playerId) == -1)
                    return;

                local askId = getPlayerAskId(playerId);

                PlayerPacket(askId).rejectAsk();
                PlayerPacket(playerId).rejectAsk();

                setPlayerAskId(askId, -1);
                setPlayerAskId(playerId, -1);
            break;
            case PlayerPackets.UseSprint:
                getPlayer(playerId).useSprint();
            break;
            // Character packets
            case PlayerPackets.CharacterAdd:
                PlayerController.register(playerId, packet.readString());
            break;
            case PlayerPackets.CharacterAccept:
                PlayerController.load(playerId, packet.readInt16());
            break;
            case PlayerPackets.Recovery:
                local value = packet.readFloat();
                local stamina = getPlayerStamina(playerId);
                local hunger = getPlayerHunger(playerId);
                if(hunger > 50){
                    local player = getPlayer(playerId)
                    if (player.stamineRecoveryByInteraction + value <= Config["MaxStamineRecoveredByInteraction"])
                        player.stamineRecoveryByInteraction += (getPlayerStamina(playerId) + value) - getPlayerStamina(playerId)
                    else
                        return

                    stamina = stamina + value;
                    if(stamina > 100)
                        stamina = 100.00;

                    setPlayerStamina(playerId, stamina);
                }
            break;
            case PlayerPackets.CharacterAccept:
                PlayerController.load(playerId, packet.readInt16());
            break;
            case PlayerPackets.SpellSummon:
                local value = packet.readInt16();
                local targetId = packet.readInt16();
                callEvent("onPlayerSpellSummon", playerId, value, targetId);
            break;
            case PlayerPackets.Print:
                print(packet.readString());
            break;
            case PlayerPackets.DuplicatingKey:
                local _item = getItem(packet.readInt32());

                if(getPlayerSkill(playerId, PlayerSkill.Carpenter) <= 2) {
                    addPlayerNotification(playerId, "Nie jeste� wykszta�cony w tej dziedzinie (T3)");
                    return;
                }

                if(hasPlayerItem(playerId, "ITKE_SDRP_DOOR_100") == 0) {
                    addPlayerNotification(playerId, "Nie posiadsz materialow.");
                    return;
                }

                if(_item.guid == "") {
                    PlayerPacket(playerId).addNotification("Klucz musi posiadac ID");
                    return;
                }

                local _key = createItem("ITKE_SDRP_DOOR_01", 1);
                local pObj = getPlayer(playerId);

                _key.name = _item.name;
                _key.description = _item.description;
                _key.guid = _item.guid;
                _key.save();

                pObj.inventory.addItem(_key.id);
                removeItem(playerId, "ITKE_SDRP_DOOR_100", 1);
                PlayerPacket(playerId).addNotification("Dorobiono klucz: " + _item.guid);
                addPlayerLog(playerId, "Skopiowal klucz z guid " + _item.guid);
            break;

            case PlayerPackets.AnimationBind:
            {
                local keyId = packet.readUInt16()
                local animationName = packet.readString() // Spis animacji jest client-side, soo....

                local player = getPlayer(playerId)

                local text = mysql_real_escape_string(DB.handler, animationName)

                if ((keyId in player.keyBinds))
                {
                    Query().update("keybinds", ["keyId", "anim"], [keyId,"'" + text + "'"]).where(["id = "+player.rId, "keyId = " + keyId]).execute();
                    player.keyBinds[keyId] = text
                }
                else
                {
                    Query().insertInto("keybinds", ["id", "keyId", "anim"], [player.rId, keyId, "'" + text + "'"]).execute()
                    player.keyBinds[keyId] <- text
                }

                break
            }

            case PlayerPackets.AnimationBindDelete:
            {
                local keyId = packet.readUInt16()
                local player = getPlayer(playerId)

                delete player.keyBinds[keyId]
                Query().deleteFrom("keybinds").where(["id = " + player.rId, "keyId = " + keyId]).execute()
                break
            }
        }
    }
}

getPlayer <- @(id) PlayerController.players[id];
getPlayers <- @() PlayerController.players;

addEventHandler("onInit", PlayerController.onInit.bindenv(PlayerController));
addEventHandler("onPlayerJoin", PlayerController.onPlayerJoin.bindenv(PlayerController));
addEventHandler("onPlayerDisconnect", PlayerController.onExit.bindenv(PlayerController));
addEventHandler("onPlayerRespawn", PlayerController.onPlayerRespawn.bindenv(PlayerController));
addEventHandler("onPlayerHit", PlayerController.onPlayerHit.bindenv(PlayerController));
addEventHandler("onPlayerDead", PlayerController.onPlayerDead.bindenv(PlayerController));
addEventHandler("onAccountLoad", PlayerController.onAccountLoad.bindenv(PlayerController));
addEventHandler("onPlayerTeleport", PlayerController.onPlayerTeleport.bindenv(PlayerController));
addEventHandler("onPlayerChangeWeaponMode", PlayerController.onPlayerChangeWeaponMode.bindenv(PlayerController));
addEventHandler("onPlayerEquipHelmet", PlayerController.onPlayerEquipHelmet.bindenv(PlayerController));
addEventHandler("onPlayerUseCheat", PlayerController.onPlayerUseCheat.bindenv(PlayerController));
addEventHandler("onPlayerShoot", PlayerController.onPlayerShoot.bindenv(PlayerController));

RegisterPacketReceiver(Packets.Player, PlayerController.onPacket.bindenv(PlayerController));

setTimer(function() {
    PlayerController.onSecond();
}, 1000, 0);

setTimer(function() {
    PlayerController.onMinute();
}, 60 * 1000, 0);
