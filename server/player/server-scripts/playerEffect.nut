
class PlayerEffect
{
    static dbTable = "playereffect";

    id = -1
    characterId = -1

    effects = [];

    constructor(id, characterId)
    {
        this.id = id;
        this.characterId = characterId;

        this.effects = [];

        read();
    }

    function read() {
        local effectsInDatabase = Query().select().from(PlayerEffect.dbTable).where(["playerId = "+characterId]).all();

        foreach(effect in effectsInDatabase) {
            addEffect(effect.effectId, effect.refreshTime, false)
        }
    }

    function addEffect(effectId, refreshTime, addToDB = true) {
        effects.push(effectId);
        System.Effect.add(id, SystemEffectTarget.Player, effectId, refreshTime);
        if(addToDB)
            Query().insertInto(PlayerEffect.dbTable, ["effectId", "playerId", "refreshTime"], [effectId, characterId, refreshTime]).execute();
    }

    function removeEffect(effectId) {
        local index = effects.find(effectId);
        if(index != null) {
            effects.remove(index);
            System.Effect.onPlayerEffectRemove(id, effectId);
            Query().deleteFrom(PlayerEffect.dbTable).where(["effectId = "+effectId+" AND playerId = "+characterId]).execute();
            return true;
        }
        return false;
    }

    function removeAllEffects() {
        foreach(index, effectId in effects) {
            effects.remove(index);
            System.Effect.onPlayerEffectRemove(id, effectId);
        }

        Query().deleteFrom(PlayerEffect.dbTable).where(["playerId = "+characterId]).execute();
    }
}