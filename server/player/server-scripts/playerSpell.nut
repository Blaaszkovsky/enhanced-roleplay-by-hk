PlayerSpellController <- {
    createBot = function(playerId, instance, str, dex, hp) {
        local object = BotHuman("Zwierzak "+playerId, "HOMEMADE");

        local pos = getPlayerPosition(playerId);
        local digital = irand(10);
        if(digital < 3) {
            pos.x = pos.x + rand() % 300;
            pos.z = pos.z + rand() % 300;
        }else if(digital < 7) {
            pos.x = pos.x + rand() % 300;
            pos.z = pos.z - rand() % 300;
        }else {
            pos.x = pos.x - rand() % 300;
            pos.z = pos.z - rand() % 300;
        }

        object.instance = instance;
        object.isTemporary = true;
        object.isSaved = false;
        object.position = pos;
        object.setPosition(object.position.x,object.position.y,object.position.z);
        object.setAngle(getPlayerAngle(playerId));
        object.str = str;
        object.dex = dex;
        object.hp = hp;
        object.hpMax = hp;
        object.respawnTime = 1;

        for(local i = 0; i <= getMaxSlots(); i ++) {
            if(isPlayerSpawned(i))
                callEvent("onPlayerChangeCell", i, getPlayer(i).cell, getPlayer(i).cell);
        }

        if(instance == "SKELETON")
            object.melee = Items.id("ITMW_2H_SLD_SWORD");

        object.addEffect(45, 1000);
        object.addEffect(61, 3);

        object.addComponent(BotOwningComponent(playerId));
    }

    checkItem = function(playerId, item, instance, machine) {
        foreach(r in item.requirement){
            if(r.type == PlayerAttributes.Mana){
                local currMana = getPlayerMana(playerId);
                currMana = currMana - r.value;
                if(currMana <= 0)
                    currMana = 0;

                setPlayerMana(playerId, currMana);
            }
        }
        if(item.flag == ITEMCAT_SCROLL)
            removeItem(playerId, instance, 1);

        if(machine == false)
            _giveItem(playerId, Items.id(instance), 1);
    }

    onPlayerSpellSummon = function(playerId, spellId, targetId) {
        local instance = Items.name(spellId);
        local item = getItemScheme(instance);
        local summon = item.summon;
        if(summon != null){
            for(local i = 0; i < summon.count; i ++) {
                createBot(playerId, summon.getName(), summon.str, summon.dex, summon.hp);
            }
            checkItem(playerId,item,instance,true)
        } else {
            switch(instance) {
                case "ITRU_HEALOTHER":
                case "ITSC_HEALOTHER":
                    if(targetId == -1 || targetId > getMaxSlots())
                        targetId = playerId;

                    setPlayerHealth(targetId, getPlayerMaxHealth(targetId));
                    PlayerPacket(targetId).oneTimeEffect(targetId, 40);
                    checkItem(playerId,item,instance,true)
                break;
                case "ITRU_FULLHEAL":
                case "ITSC_FULLHEAL":
                    setPlayerHealth(playerId, getPlayerMaxHealth(playerId));
                    PlayerPacket(playerId).oneTimeEffect(playerId, 40);
                    checkItem(playerId,item,instance,true)
                break;
            }
        }
    }

    onPlayerSpellCast = function (playerId, spellId) {
        getPlayer(playerId).magicWeapon = spellId;

        local instance = Items.name(spellId);
        local item = getItemScheme(instance);
        if(item == null)
            kick(playerId, "U�yto nie zarejestrowanego przedmiotu.");
        else {
            checkItem(playerId,item,instance,false)
        }
    }
}

addEventHandler("onPlayerSpellCast", PlayerSpellController.onPlayerSpellCast.bindenv(PlayerSpellController));
addEventHandler("onPlayerSpellSummon", PlayerSpellController.onPlayerSpellSummon.bindenv(PlayerSpellController));
