class Player
{
    // Table names
    dbTable = "player";
    skillsTable = "playerskill";
    itemsTable = "playeritem";
    relationsTable = "playerrelation";

    // Important { Primary keys }
    id = -1;
    rId = -1;

    // {Bool}
    loggIn = false;
    wounded = false;
    sprinting = false;
    drunken = false;

    // {String}
    instance = "";
    cell = "";
    name = "";

    // {Int}
    fatness = -1;
    speech = -1;
    walking = -1;

    fractionId = -1;
    fractionRoleId = -1;

    deaths = 0;
    kills = 0;
    assists = 0;

    statusId = 0;
    stamina = 0;
    hunger = 0;
    respawnLeft = 0;
    lastPnDay = 0;
    minutesInGame = 0;

    minutesCounter = 0;
    pointsToday = 0;
    staminaTodayRecover	= 0;
    stamineRecoveryByFood = 0;
    stamineRecoveryByInteraction = 0;

    str = 0;
    dex = 0;
    hp = 40;
    hpMax = 40;
    mana = 10;
    maxMana = 10;
    magicLvl = 0;
    magicWeapon = -1;
    inteligence = 0;
    endurance = 0;
    askId = -1;

    exp = 0;
    nextLevelExp = 0;
    level = 0;
    learnPoints = 0;

    scaleX = 1.0;
    scaleY = 1.0;
    scaleZ = 1.0;

    lastHitDelay = -1;
    barrierWarningLevel = 0;
    autoSaveNext = 0;
    unconscious = 0;
    carried = -1;
    carrying = -1;
    godMode = false;

    // {Array}
    personalCard = [];
    personalNotes = [];
    realNames = [];
    weapon = [];
    factions = [];
    skills = [];
    bonusProtection = [];
    bonusDamage = [];
    magicSlots = [];

    // {Table}
    animations = {};
    visual = {};

    // {Modules}
    effectModule = null;
    logModule = null;
    boundModule = null;
    penaltyModule = null;

    inventory = null;
    playerTimer = null;
    masked = false;

    lastPWId = -1

    ignoring = false
    ignorepw = false
    description = "Brak";
    keyBinds = null

    constructor(pid)
    {
        // Set primary keys
        id = pid;
        rId = -1;

        // Set bool
        loggIn = false;
        wounded = false;
        sprinting = false;

        ignorepw = false
        ignoring = false
        // Set string
        name = "";
        cell = "0,0";
        instance = "PC_HERO";

        // Set int
        fatness = 0;
        speech = PlayerSpeech.Normal;
        walking = 0;

        fractionId = -1;
        fractionRoleId = -1;

        deaths = 0;
        kills = 0;
        assists = 0;

        stamina = 100.0;
        hunger = 100.0;
        statusId = PlayerStatus.Active;
        respawnLeft = 0;
        lastPnDay = 0;

        minutesInGame = 0;
        minutesCounter = 0;
        pointsToday = 0;
        staminaTodayRecover	= 0;
        stamineRecoveryByFood = 0;
        stamineRecoveryByInteraction = 0;

        lastHitDelay = getTickCount();
        barrierWarningLevel = 0;

        str = 0;
        dex = 0;
        inteligence = 0;
        askId = -1;
        endurance = 100;

        hp = 40;
        hpMax = 40;

        mana = 10;
        maxMana = 10;
        magicLvl = 0;
        magicWeapon = -1;

        exp = 0;
        nextLevelExp = 0;
        level = 0;
        learnPoints = 0;

        scaleX = 1.0;
        scaleY = 1.0;
        scaleZ = 1.0;

        godMode = false;
        lastPWId = -1

        // Set array
        personalCard = [];
        personalNotes = [];
        realNames = [];

        masked = false;

        for(local i = 0; i < getMaxSlots(); i ++)
            realNames.append("")

        skills = []

        for(local i = 0; i < Config["PlayerSkill"].len(); i ++)
            skills.push(0);

        weapon = [0,0,0,0];
        inventory = null;
        factions = [];
        animations = {};
        bonusProtection = [0,0,0,0,0,0,0,0]
        bonusDamage = [0,0,0,0,0,0,0,0]
        magicSlots = [0,0,0,0,0,0,0]

        factions.append(HUMAN_FACTION.id);

        // Set table
        visual = {
            bodyModel = "Hum_Body_Naked0",
            bodyTxt = 1,
            headModel = "Hum_Head_FatBald",
            headTxt = 7,
        }

        effectModule = null;
        logModule = null;
        boundModule = null;
        penaltyModule = null;
        playerTimer = null;
        description = "Brak";
        keyBinds = {}

    }

    /**
    * Register player function
    * Add to databse new object on given username
    * Here u can define default statistics
    */
    function register() {
        local dateObject = date();
        local createdAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        str = 10;
        dex = 10;
        hp = 100;
        hpMax = 100;
        mana = 10;
        maxMana = 10;

        weapon = [0,0,0,0];
        exp = 0;
        level = 0;
        magicLvl = 0;
        inteligence = 10;
        askId = -1;
        learnPoints = 10;
        pointsToday = 0;
        lastPnDay = date().day;

        scaleX = 1.0;
        scaleY = 1.0;
        scaleZ = 1.0;

        minutesCounter = 0;
        stamina = 100.0;
        hunger = 100.00;

        name = mysql_escape_string(name);
        if(name == null)
            name = "";

        Query().insertInto(Player.dbTable, ["accountId", "name", "bodyModel", "bodyTxt", "headModel", "headTxt", "walking", "fatness", "speech", "unconscious", "serial",
            "str", "dex", "health", "maxHealth", "mana", "maxMana", "oneH", "twoH", "bow", "cbow", "positionX", "positionY", "positionZ", "world", "angle",
            "deaths", "kills", "stamina", "statusId", "respawnLeft", "lastPnDay", "magicLvl", "experience", "level", "learnPoints", "inteligence"
            "minutesInGame", "minutesCounter", "pointsToday", "staminaTodayRecover", "armor", "ranged", "melee", "createdAt", "updatedAt", "personalCard", "personalNotes","hunger"
        ], [
            getAccount(id).rId, "'"+name+"'", "'"+visual.bodyModel+"'", visual.bodyTxt, "'"+visual.headModel+"'", visual.headTxt, walking, fatness, speech, unconscious, "'"+getPlayerSerial(id)+"'",
            str, dex, hp, hpMax, mana, maxMana, weapon[0], weapon[1], weapon[2], weapon[3], -88970.9, 617.57, -118394, "'"+getServerWorld()+"'", 59,
            deaths, kills, stamina, statusId, respawnLeft, lastPnDay, magicLvl, exp, level, learnPoints, inteligence,
            minutesInGame, minutesCounter, pointsToday, staminaTodayRecover, "''", "''", "''", "'"+createdAt+"'", "'"+createdAt+"'", "' '", "' '",hunger
        ]).execute();

        local playerObjectId = Query().select().from(Player.dbTable).where(["accountId = "+getAccount(id).rId+""]).orderBy("id DESC").one()["id"];

        Query().insertInto(Player.skillsTable, ["playerId"], [playerObjectId]).execute();

        return playerObjectId;
    }

    function load(characterId) {
        if(characterId == -1) {
            kick(id, "Problem z wczytaniem postaci.");
            return;
        }

        DB.query("UPDATE "+Player.dbTable+" SET online = 1 WHERE id = "+characterId+";");

        local object = Query().select().from(Player.dbTable).where(["id = "+characterId+""]).one();
        local skillsQuery = Query().select().from(Player.skillsTable).where(["playerId = "+characterId]).one();

        foreach(index, value in Config["PlayerSkill"])
            setPlayerSkill(id, index, skillsQuery["skill_"+index].tointeger());

        if(object.world != null)
            if(getPlayerWorld(id) != object.world)
                setPlayerWorld(id, object.world);

        name = object.name;

        setPlayerRealName(id, id, name);

        rId = object.id;

        minutesCounter = object.minutesCounter;
        pointsToday = object.pointsToday;
        staminaTodayRecover = object.staminaTodayRecover;
        stamineRecoveryByFood = object.stamineRecoveryByFood;
        stamineRecoveryByInteraction = object.stamineRecoveryByInteraction;

        deaths = object.deaths;
        kills = object.kills;
        statusId = object.statusId;

        fractionId = object.fractionId;
        fractionRoleId = object.fractionRoleId;

        autoSaveNext = 300;

        setPlayerFraction(id, fractionId, fractionRoleId);

        respawnLeft = object.respawnLeft;
        lastPnDay = object.lastPnDay;
        minutesInGame = object.minutesInGame;

        playerTimer = setTimer(function(_) {
            _.onSecond();
        }, 1000, 0, this);

        foreach(playerObj in PlayerController.getLoggInPlayers())
        {
            if(playerObj.id == id)
                continue;

            local data = Query().select().from(Player.relationsTable).where(["playerId = "+characterId, "relationPlayerId = " + playerObj.rId]).one();
            if(data)
                setPlayerRealName(id, playerObj.id, data["name"]);

            local data = Query().select().from(Player.relationsTable).where(["playerId = "+playerObj.rId, "relationPlayerId = " + characterId]).one();
            if(data)
                setPlayerRealName(playerObj.id, id, data["name"]);
            else
                setPlayerRealName(playerObj.id, id, "");
        }

        personalCard = [];
        personalNotes = [];

        local rfile = io.file("database/cards/"+rId, "r");
        if (rfile.isOpen)
        {
            local result = null;
            do
            {
                result = rfile.read(io_type.LINE);
                if(result != null && result != "")
                    personalCard.push(result);
            }while(result != null)

            rfile.close();
        }

        rfile = io.file("database/notes/"+rId, "r");
        if (rfile.isOpen)
        {
            local result = null;
            do
            {
                result = rfile.read(io_type.LINE);
                if(result != null && result != "")
                    personalNotes.push(result);
            }while(result != null)

            rfile.close();
        }

        if(object.endurance > 100)
            object.endurance = 100;

        setPlayerEndurance(id, object.endurance);
        setPlayerPersonalCard(id, personalCard);
        setPlayerInteligence(id, object.inteligence)
        setPlayerPersonalNotes(id, personalNotes);
        setPlayerInstance(id, object.instance);
        setPlayerWalkingStyle(id, object.walking);
        setPlayerFatness(id, object.fatness);
        setPlayerSpeech(id, object.speech);
        setPlayerUnconscious(id, object.unconscious);
        setPlayerVisual(id, object.bodyModel, object.bodyTxt, object.headModel, object.headTxt)
        setPlayerStrength(id, object.str)
        setPlayerDexterity(id, object.dex)
        setPlayerMaxHealth(id, object.maxHealth)
        setPlayerHealth(id, object.health)
        setPlayerMagicLevel(id, object.magicLvl);
        setPlayerMaxMana(id, object.maxMana);
        setPlayerMana(id, object.mana);
        setPlayerLearnPoints(id, object.learnPoints);
        setPlayerLevel(id, object.level);
        setPlayerExperience(id, object.experience);
        setPlayerNextLevelExperience(id, 500);
        setPlayerSkillWeapon(id, 0, object.oneH);
        setPlayerSkillWeapon(id, 1, object.twoH);
        setPlayerSkillWeapon(id, 2, object.bow);
        setPlayerSkillWeapon(id, 3, object.cbow);
        setPlayerPosition(id, object.positionX, object.positionY, object.positionZ);
        setPlayerAngle(id, object.angle);
        setPlayerTalent(id, TALENT_SNEAK, true);
        setPlayerHunger(id,object.hunger);

        instance = object.instance;

        scaleX = object.scaleX;
        scaleY = object.scaleY;
        scaleZ = object.scaleZ;

        setPlayerScale(id, scaleX, scaleY, scaleZ);

        inventory = PlayerInventory(ItemRelationType.Player, rId);
        inventory.playerId = id;
        item.Controller.buckets[inventory.id] <- inventory.weakref();

        PlayerPacket(id).sendInventoryId(inventory.id);
        inventory.open(id);

        foreach(_item in inventory.getItems()) {
            _giveItem(id, Items.id(_item.instance), _item.amount);
            if(_item.equipped)
                equipItem(id, _item);
        }

        if(lastPnDay != date().day)
        {
            lastPnDay = date().day;
            // setPlayerLearnPoints(id, getPlayerLearnPoints(id) + 3);

            minutesCounter = 0;
            pointsToday = 0;
            staminaTodayRecover = 0;
            stamineRecoveryByFood = 0;
            stamineRecoveryByInteraction = 0;

            stamina = 100.0;
        }

        setPlayerStamina(id, object.stamina);

        spawnPlayer(id);
        setPlayerRespawnTime(id, 1000);

        effectModule = PlayerEffect(id, characterId);
        logModule = PlayerLog(characterId);
        boundModule = PlayerBound(id, characterId);
        penaltyModule = PlayerDeathPenalty(id);
        description = object.description;

        local keyBindsQuery = Query().select().from("keybinds").where(["id = "+characterId]).all()

        foreach(bind in keyBindsQuery)
        {
            keyBinds[bind.keyId] <- bind.anim

            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.AnimationBind)
            packet.writeUInt16(bind.keyId);
            packet.writeString(bind.anim)
            packet.send(id, RELIABLE)
        }

        loggIn = true;

        sendDescriptionsToPlayer(id);
        PlayerPacket(id).setPlayerDescription(id, description);
    }

    function getCurrentPlayerDescription(pId) {
        return getPlayer(pId).description;
    }

    function sendDescriptionsToPlayer(playerId) {
        for(local i=0; i <= getMaxSlots(); i++) {
            if(isPlayerConnected(i)) {
                if(getPlayer(i).id != playerId) {
                    PlayerPacket(playerId).getPlayerDescription(i, getCurrentPlayerDescription(i))
                }
            }
        }
    }

    function save(executeInstantly = true) {
        if(loggIn == false)
            return;

        local position = getPlayerPosition(id);
        local dateObject = date();
        local updatedAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        local armorLabel = "";
        local meleeLabel = "";
        local rangedLabel = "";

        local query = Query().update(Player.dbTable, ["fractionId", "fractionRoleId", "bodyModel", "bodyTxt", "headModel", "headTxt", "walking", "fatness", "speech", "unconscious", "serial",
            "endurance", "str", "dex", "health", "maxHealth", "mana", "maxMana", "oneH", "twoH", "bow", "cbow", "positionX", "positionY", "positionZ", "world", "angle",
            "deaths", "kills", "stamina", "statusId", "respawnLeft", "lastPnDay", "magicLvl", "experience", "level", "learnPoints", "inteligence"
            "minutesInGame", "minutesCounter", "pointsToday", "staminaTodayRecover", "armor", "ranged", "melee", "updatedAt", "personalCard", "personalNotes", "hunger"
            ,"scaleX", "scaleY", "scaleZ", "instance", "stamineRecoveryByFood", "stamineRecoveryByInteraction", "description"
        ], [
            fractionId, fractionRoleId, "'"+visual.bodyModel+"'", visual.bodyTxt, "'"+visual.headModel+"'", visual.headTxt, walking, fatness, speech, unconscious, "'"+getPlayerSerial(id)+"'",
            endurance, str, dex, hp, hpMax, getPlayerMana(id), getPlayerMaxMana(id), weapon[0], weapon[1], weapon[2], weapon[3], position.x, position.y, position.z, "'"+getPlayerWorld(id)+"'", getPlayerAngle(id),
            deaths, kills, stamina, statusId, respawnLeft, lastPnDay, magicLvl, exp, level, learnPoints, inteligence,
            minutesInGame, minutesCounter, pointsToday, staminaTodayRecover, "'"+armorLabel+"'", "'"+rangedLabel+"'", "'"+meleeLabel+"'", "'"+updatedAt+"'", "''", "''",hunger, scaleX, scaleY, scaleZ, "'" + instance + "'",
            stamineRecoveryByFood, stamineRecoveryByInteraction, "'" + description + "'"
		]).where(["id = "+rId]);

        local wfile = file("database/cards/"+rId, "w");
        foreach(value in personalCard)
            wfile.write(value+"\n");

        wfile.close();

        wfile = file("database/notes/"+rId, "w");
        foreach(value in personalNotes)
            wfile.write(value+"\n");

        wfile.close();

        if(executeInstantly) {
            DB.query("UPDATE "+Player.dbTable+" SET online = 0 WHERE id = "+rId+";");
            query.execute();
        }else
            mysqlQueue.add(query.query);

        local labelsSkills = [], valuesSkills = [];

        foreach(index, value in Config["PlayerSkill"]) {
            labelsSkills.append("skill_"+index);
            valuesSkills.append(skills[index]);
        }

        query = Query().update(Player.skillsTable, labelsSkills, valuesSkills).where(["playerId = "+rId]);

        if(executeInstantly)
            query.execute();
        else
            mysqlQueue.add(query.query);

        inventory.save();
    }

    function ck() {
        if(loggIn) {
            statusId = PlayerStatus.Killed;

            // local moreLearnPoints = learnPoints;

            // local copyStr = str - 10;
            // if(copyStr > 0) {
            //     if(copyStr > 30) {
            //         local rest = copyStr - 30;
            //         copyStr = copyStr - 30;
            //         moreLearnPoints = moreLearnPoints + (rest * 5);
            //     }

            //     moreLearnPoints = moreLearnPoints + (copyStr * 3);
            // }

            // local copyDex = dex - 10;
            // if(copyDex > 0) {
            //     if(copyDex > 30) {
            //         local rest = copyDex - 30;
            //         copyDex = copyDex - 30;
            //         moreLearnPoints = moreLearnPoints + (rest * 5);
            //     }

            //     moreLearnPoints = moreLearnPoints + (copyDex * 3);
            // }

            // local copyInteligence = inteligence - 10;
            // if(copyInteligence > 0) {
            //     if(copyInteligence > 30) {
            //         local rest = copyInteligence - 30;
            //         copyInteligence = copyInteligence - 30;
            //         moreLearnPoints = moreLearnPoints + (rest * 5);
            //     }

            //     moreLearnPoints = moreLearnPoints + (copyInteligence * 3);
            // }

            // local copyHp = hpMax - 100;
            // if(copyHp > 0) {
            //     moreLearnPoints = moreLearnPoints + abs(copyHp/2);
            // }

            // local copyMana = maxMana - 100;
            // if(copyMana > 0) {
            //     moreLearnPoints = moreLearnPoints + abs(copyMana/2);
            // }

            // foreach(weaponValue in weapon) {
            //     moreLearnPoints = moreLearnPoints + abs(weaponValue * 2);
            // }

            // foreach(skill in skills) {
            //     if(skill == 0)
            //         continue;

            //     if(skill == 1) {
            //         moreLearnPoints = moreLearnPoints + 6;
            //     } else if(skill == 2) {
            //         moreLearnPoints = moreLearnPoints + 66;
            //     }else {
            //         moreLearnPoints = moreLearnPoints + 166;
            //     }
            // }

            // moreLearnPoints = abs(moreLearnPoints/2);

            // if(minutesInGame < 600)
            //     moreLearnPoints = 0;

            // Account.setNextPnToCharacter(id, moreLearnPoints);

            return true;
        }
        return false;
    }

    /**
    * Get items that can be protecting for player
    *
    * @return protection items array
    */
    function getProtectionAvailableItems() {
        local table = [];

        local ringLeft = getPlayerRing(id, HAND_LEFT);
        if(ringLeft != -1) table.append(ringLeft);

        local ringRight = getPlayerRing(id, HAND_RIGHT);
        if(ringRight != -1) table.append(ringRight);

        local amulet = getPlayerAmulet(id);
        if(amulet != -1) table.append(amulet);

        local belt = getPlayerBelt(id);
        if(belt != -1) table.append(belt);

        return table;
    }

    /**
    * Get items that can be protecting for player
    *
    * @return protection items array
    */
    function getProtection() {
        local protection = [0,0,0,0,0,0,0,0,0];

        foreach(itemId in getProtectionAvailableItems())
        {
            if(itemId == null)
                continue;

            local item = getItemScheme(Items.name(itemId).toupper());
            foreach(index, value in item.protection)
                protection[index] += value;
        }

        foreach(index, value in inventory.getProtection())
            protection[index] += value;

        return protection;
    }

    function resetUnconscious() {
        if(carried != -1) {
            setPlayerCarrying(carried, -1);
            setPlayerCarried(id, -1);
        }

        setPlayerUnconscious(id, 0);
    }

    /**
    * Every second action
    */
    function onSecond() {
        if(unconscious != 0) {
            setPlayerUnconscious(id, getPlayerUnconscious(id) - 1);

            if(unconscious <= 0)
                resetUnconscious();
        }

        autoSaveNext = autoSaveNext - 1;

        if(autoSaveNext <= 0) {
            if(!(id in BuffController.buffs))
                BuffController.recreateBuffForId(id);

            if(BuffController.buffs[id].len() == 0)
                save(false);

            autoSaveNext = 300;
        }

        local pos = getPlayerPosition(id);

        // Giving wounded animation to player
        if(getPlayerHealth(id) < 20 && getPlayerHealth(id) > 0) {
            if(wounded == false && getPlayerWeaponMode(id) == 0) {
                wounded = true;
                applyPlayerOverlay(id, Mds.id("HUMANS_WOUNDED.MDS"));
            }
        } else {
            if(wounded) {
                wounded = false;
                removePlayerOverlay(id, Mds.id("HUMANS_WOUNDED.MDS"));
            }
        }

        // Check player cell in grid system
        local pWorld = getPlayerWorld(id);
        if(pWorld in GridRegistered) {
            local newCell = GridRegistered[pWorld].getIndex(pos.x, pos.z);
            if(newCell != cell) {
                local oldCell = cell;
                cell = newCell;
                callEvent("onPlayerChangeCell", id, cell, oldCell);
            }
        }

        if(sprinting) {
            local offset = 0;
            if(dex < 20)
                offset = 5;
            else if(dex < 50)
                offset = 3;
            else
                offset = 0;

            endurance = endurance - (5 + offset);
            setPlayerEndurance(id, endurance);
            if(endurance < 10)
                useSprint();
        }else {
            local offset = 0;
            if(dex < 20)
                offset = 0;
            else if(dex < 50)
                offset = 1;
            else
                offset = 2;

            endurance = endurance + (1 + offset);
            if(endurance > 100)
                endurance = 100;

            setPlayerEndurance(id, endurance);
        }

        if(isPlayerSpawned(id)) {
            if(inventory.ranged == null || inventory.ranged == -1 || getItem(inventory.ranged).flag != ITEMCAT_DRAGGABLE) {
                foreach(_item in inventory.getItems()) {
                    if(_item.flag == ITEMCAT_DRAGGABLE) {
                        if(getPlayerWeaponMode(id) != 0) {
                            setPlayerWeaponMode(id, 0);
                            addPlayerNotification(id, "Musisz nosi� przedmiot, kt�ry jest do tego przeznaczony.");
                        }

                        equipItem(id, _item);
                        break;
                    }
                }
            }
        }
    }

    /**
    * On player respawn
    */
    function onRespawn() {
        wounded = false;
        sprinting = false;

        foreach(animationId, refreshTime in animations)
            removePlayerOverlay(id, animationId);

        animations.clear();
        PlayerPacket(id).removeAllAnimations();
        setPlayerUnconscious(id, 1800);
    }

    /**
    * On player dead
    */
    function onDead(killerId) {
        wounded = false;
        sprinting = false;

        foreach(animationId, refreshTime in animations)
            removePlayerOverlay(id, animationId);

        animations.clear();
        PlayerPacket(id).removeAllAnimations();
    }

    /**
    * Every minute action
    */
    function onMinute() {

        // Plus one minute in game on statistics
        minutesInGame = minutesInGame + 1;
        minutesCounter = minutesCounter + 1;

        // On every hour
        if(minutesCounter >= 60){
            minutesCounter = 0;

            local giveLp = false;
            local giveNuggets = false;

            if(pointsToday <= Config["MaxDailyNuggets"])
            {
                giveNuggets = true;
                pointsToday = pointsToday + 1;
                // local getCurrentDate = date();

                // if(!(getCurrentDate.hour >= 1 && getCurrentDate.hour <= 8))
                //     giveNuggets = true;
            }

            // if(minutesInGame > 1200){
            //     giveNuggets = false;
            // }

            local message = "";

            // giving nuggets points on every hour
            if(giveNuggets)
                giveItem(id, "ITMI_OLDCOIN", Config["HourlyGivenNuggets"]);

            // giving learn points on every hour
            // if(giveLp)
            //     setPlayerLearnPoints(id, getPlayerLearnPoints(id) + Config["DailyGivenPoints"]);

            // giving message
            if(giveNuggets && giveLp)
                message = "Dosta�e� "+Config["DailyGivenPoints"]+" punkty nauki i "+Config["HourlyGivenNuggets"]+" sztuk z�ota. (" + pointsToday + "/" + Config["MaxDailyLearnPoints"] + ")";
            else if(giveNuggets)
                message = "Dosta�e� "+Config["HourlyGivenNuggets"]+" grosze";
            else if(giveLp)
                message = "Dosta�e� "+Config["DailyGivenPoints"]+" punkty nauki.";

            if(message != "")
                PlayerPacket(id).addNotification(message);
        }

        // Stamina recovery but only 100% on one day.
        if(staminaTodayRecover <= 100 && stamina < 100.0 && hunger > 50.0)
        {
            stamina = stamina + 1.0;
            staminaTodayRecover = staminaTodayRecover + 1;
            if(stamina > 100.0)
                stamina = 100.0;

            setPlayerStamina(id, stamina);
        }

        if(hunger > 0){
            local tempHunger =  hunger;

            if(stamina > 0 && stamina <= 20) {
                tempHunger = hunger - 1.6;
            } else if(stamina > 20 && stamina <= 40) {
                tempHunger = hunger - 1.2;
            } else if(stamina > 40 && stamina <= 60) {
                tempHunger = hunger - 1.0;
            } else if(stamina > 60 && stamina <= 80) {
                tempHunger = hunger - 0.8;
            } else if(stamina > 80 && stamina < 100) {
                tempHunger = hunger - 0.6;
            } else if(stamina == 100) {
                tempHunger = hunger - 0.15;
            }

            if(tempHunger < 0){
                hunger = 0;
            } else {
                hunger = tempHunger;
            }
            setPlayerHunger(id, hunger);
        }

        if(hunger > 75 && stamina > 25) {
            local getPercentHp = getPlayerMaxHealth(id) * 0.25;

            if(getPercentHp > hp) {
                setPlayerHealth(id, hp + 3);
            }
        }
    }

    /**
    * Apply player overlay from client mds
    *
    * @param mds id
    */
    function applyAnimation(mdsId) {
        if(mdsId in animations)
            animations[mdsId] = getTickCount();
        else
            animations[mdsId] <- getTickCount();

        if(mdsId == Mds.id("HUMANS_DRUNKEN.MDS"))
            drunken = true

        PlayerPacket(id).applyAnimation(mdsId);
    }

    /**
    * Removes player overlay from client mds
    *
    * @param mds id
    */
    function removeAnimation(mdsId) {
        if(mdsId in animations)
            animations.rawdelete(mdsId);
        if(mdsId == Mds.id("HUMANS_DRUNKEN.MDS"))
            drunken = false
        PlayerPacket(id).removeAnimation(mdsId);
    }

    /**
    * Here we get distance from one position to player position
    *
    * @return distance int
    */
    function distanceToPosition(positionToCompare) {
        if(rId == -1)
            return 100000;

        local pos = getPlayerPosition(id);
        return getDistance3d(pos.x,pos.y,pos.z,positionToCompare.x,positionToCompare.y,positionToCompare.z);
    }

    /**
    * Here we get distance from one position to player position in y axis
    *
    * @return distance int
    */
    function distanceInYAxis(positionToCompare) {
        if(rId == -1)
            return 100000;

        local pos = getPlayerPosition(id).y;
        return abs(positionToCompare.y - pos);
    }

    /**
    * Here we can get out factions and factions that belongs to our fractions
    *
    * @return array with factions
    */
    function getPlayerFactions()
    {
        local retTab = factions;
        local fractionTable = FractionController.getPlayerFactions(id);

        foreach(idFaction in fractionTable)
            retTab.push(idFaction);

        return retTab;
    }

    /**
    * Sending information about fraction
    * Here we send every information about player in fraction
    */
    function sendFractionInfo(fractionId)
    {
        // get fraction on given id
        local fraction = getFraction(fractionId);

        // object to add packets
        local obj = [];

        // no member in fraction
        if(!fraction.hasMember(rId))
        {
            obj.append({type = PlayerGUIFraction.None, value = ""});
            PlayerPacket(id).sendFractionInfo(obj);
            return;
        }

        // some variables to not send information second time
        local sendWorks = false, sendedWorks = [];
        local sendCraftings = false, sendedCrafts = [];
        local canTrade = false;
        local canInvite = false;
        local sendMembers = false;
        local sendPermissions = false;
        // label with permission
        local permissioLabel = "";

        foreach(permissionId in fraction.members[rId].permissions)
        {
            local permissionObject = fraction.permissions[permissionId];
            switch(permissionObject.type)
            {
                case FractionPermissionType.Invite:
                    canInvite = true;
                break;
                case FractionPermissionType.Leadership:
                    canInvite = true;
                    sendMembers = true;
                    sendPermissions = true;
                break;
                case FractionPermissionType.Working:
                    sendWorks = true;
                    sendedWorks.extend(permissionObject.labels);
                break;
                case FractionPermissionType.Crafting:
                    sendCraftings = true;
                    sendedCrafts.extend(permissionObject.labels);
                break;
            }
            permissioLabel = permissioLabel + permissionObject.name + ",";
        }

        obj.append({type = PlayerGUIFraction.Permissions, value = permissioLabel});

        if(canInvite)
            obj.append({type = PlayerGUIFraction.Invite, value = ""});

        if(canTrade)
            obj.append({type = PlayerGUIFraction.Trade, value = ""});

        if(sendPermissions)
        {
            local permissionLabels = "";
            foreach(permission in fraction.permissions)
                permissionLabels += permission.id+" "+permission.name+",";

            if(permissionLabels.len() > 0)
                permissionLabels.slice(0, -1);

            obj.append({type = PlayerGUIFraction.MemberPermission, value = permissionLabels});
        }

        if(sendMembers)
        {
            local membersLabels = "";
            foreach(memberId, member in fraction.members)
                membersLabels += memberId+" "+Player.getUserNameByPlayerId(memberId)+"-"+member.getPermissionLabels("&")+",";

            if(membersLabels.len() > 0)
                membersLabels.slice(0, -1);

            obj.append({type = PlayerGUIFraction.MemberList, value = membersLabels});
        }

        // send object to player
        PlayerPacket(id).sendFractionInfo(obj);
    }

    /**
    Method use to call sprint mds
    */
    function useSprint() {
        if(sprinting) {
            setTimer(function(player)
            {
                player.sprinting = false;
                removePlayerOverlay(player.id, Mds.id("HUMANS_SPRINT2.MDS"));
            }, 200, 1, getPlayer(id))

            return;
        }

        local rangedIsBag = false;
        if(inventory.ranged != -1 && inventory.ranged != null)
            if(getItem(inventory.ranged).flag == ITEMCAT_DRAGGABLE)
                rangedIsBag = true;

        if(endurance < 15 || wounded || drunken || rangedIsBag || getPlayerHealth(id) < 20)
            return;

        endurance = endurance - 10;
        setPlayerEndurance(id, endurance);
        sprinting = true;
        applyPlayerOverlay(id, Mds.id("HUMANS_SPRINT2.MDS"));
    }

    /**
    Helper for dmg calculation method
    @param int attribute id
    */
    function getAttribute(attributeType) {
        return getPlayerAttributeValue(id, attributeType);
    }

    /**
    * Method returning weapon mode
    *
    * @return weaponMode int
    */

    function getWeaponMode() {
        return getPlayerWeaponMode(id);
    }

    /**
    * Method returning weapon mode melee
    *
    * @return melee weapon int
    */
    function getMeleeWeapon() {
        if(inventory.melee != -1)
            return getItem(inventory.melee);

        return null;
    }

    /**
    * Method returning weapon mode ranged
    *
    * @return ranged weapon int
    */
    function getRangedWeapon() {
        if(inventory.ranged != -1)
            return getItem(inventory.ranged);

        return null;
    }

    /**
    * Method returning weapon mode magic
    *
    * @return magic weapon int
    */
    function getMagicWeapon() {
        return getItemScheme(Items.name(magicWeapon));
    }

    /**
    * Method will call resistance of item
    */
    function resistanceCallback() {
        inventory.callResistance();
    }


    /**
    * Method will call dmg of item
    */
   function dmgCallback() {
        inventory.dmgCallback();
    }


    /**
    * Method check for any other instances (like bot) checking is object targetable
    *
    * @return bool
    */
    function isTargetable() {
        return unconscious == 0 && getPlayerHealth(id) > 0 && isPlayerSpawned(id) && isPlayerConnected(id) && getPlayerInvisible(id) == false;
    }

    /**
    * Method sets god mode
    *
    */

    function setGodMode(value)
    {
        PlayerPacket(id).setGodMode(godMode = value);
    }

    /**
    * Method check if player is killable
    *
    * @return bool
    */

    function isGodModeEnabled() { return godMode; }

    /**
    * Return players instance belonging to account on given id
    *
    * @param accountId int
    * @return Player[] array
    */
    static function getAccountPlayers(accountId) {
        return Query().select().from(Player.dbTable).where(["accountId = "+accountId+" AND statusId = 0"]).all();
    }

    static function getAccountPlayersBySerial(serial) {
        return Query().select().from(Player.dbTable).where(["serial = '"+serial+"' AND statusId = 0"]).all();
    }

    /**
    * Return username for player on given Id
    *
    * @param playerId int
    * @return name string
    */
    static function getUserNameByPlayerId(playerId) {
        return Query().select("name").from(Player.dbTable).where(["id = "+playerId]).one()["name"];
    }

    /**
    * Return player from database on given username
    *
    * @param username string
    * @return Player instance
    */
    static function findByUsername(username) {
        return Query().select().from(Player.dbTable).where(["name = '"+username+"'"]).one();
    }

    /**
    * Reset method to clear state of character
    */
    function reset()
    {
        unspawnPlayer(id);

        removePlayerOverlay(id, Mds.id("HUMANS_SPRINT2.MDS"));
        removePlayerOverlay(id, Mds.id("HUMANS_WOUNDED.MDS"));
        removePlayerOverlay(id, Mds.id("HUMANS_ZWIAZANIE.MDS"));
        removePlayerOverlay(id, Mds.id("HUMANS_CARRYBARREL.MDS"));
    }

    /**
    * Clear player object to default statistics
    */
    function clear()
    {
        rId = -1;

        loggIn = false;
        wounded = false;
        name = "";

        if(playerTimer) {
            killTimer(playerTimer);
            playerTimer = null;
        }

        fatness = 0;
        speech = PlayerSpeech.Normal;
        walking = 0;

        fractionId = -1;
        fractionRoleId = -1;

        personalCard = [];
        personalNotes = [];
        realNames = [];

        for(local i = 0; i < getMaxSlots(); i ++)
            realNames.append("")

        deaths = 0;
        kills = 0;
        assists = 0;

        stamina = 100.0;
        statusId = PlayerStatus.Active;
        respawnLeft = 0;
        lastPnDay = 0;
        minutesInGame = 0;
        inteligence = 0;
        askId = -1;
        endurance = 100;

        minutesCounter = 0;
        pointsToday = 0;
        staminaTodayRecover	= 0;
        stamineRecoveryByFood = 0;
        stamineRecoveryByInteraction = 0;

        unconscious = 0;
        carried = -1;
        carrying = -1;

        str = 0;
        dex = 0;
        hp = 40;
        hpMax = 40;
        ignoring = false
        lastHitDelay = getTickCount();

        instance = "PC_HERO";
        cell = "0,0";

        mana = 10;
        maxMana = 10;
        magicLvl = 0;
        magicWeapon = -1;

        scaleX = 1.0;
        scaleY = 1.0;
        scaleZ = 1.0;

        exp = 0;
        nextLevelExp = 0;
        level = 0;
        learnPoints = 0;

        bonusProtection = [0,0,0,0,0,0,0,0]
        bonusDamage = [0,0,0,0,0,0,0,0]
        magicSlots = [0,0,0,0,0,0,0];

        skills = []

        for(local i = 0; i < Config["PlayerSkill"].len(); i ++)
            skills.push(0);

        godMode = false;

        if(inventory != null)
            item.Controller.removeBucket(inventory.id);

        inventory = null;

        factions.clear();
        animations.clear();
        animations = {};

        factions.append(HUMAN_FACTION.id);
        visual = {
            bodyModel = "Hum_Body_Naked0",
            bodyTxt = 1,
            headModel = "Hum_Head_FatBald",
            headTxt = 7,
        }

        effectModule = null;
        logModule = null;
        boundModule = null;
        penaltyModule = null;

        lastPWId = -1
        ignorepw = false
        keyBinds = {}
    }
}