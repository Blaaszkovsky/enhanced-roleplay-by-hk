
addCommand("report", function(playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /report id wiadomosc");
        return;
    }

    local raportedId = args[0];
    if(isPlayerConnected(raportedId) == false)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Gracz nie jest po��czony.");
        return;
    }

    local ticket = false;
    foreach(adminId, admin in getAdmins()) {
        if(admin.role >= PlayerRole.Helper) {
            sendMessageToPlayer(adminId, 216, 85, 9, ChatType.REPORT+"Raport od "+getAccount(playerId).username+"("+playerId+") na "+getAccount(raportedId).username+"("+raportedId+")");
            sendMessageToPlayer(adminId, 216, 85, 9, ChatType.REPORT+args[1]);
            addPlayerNotification(adminId, "Pojawi� si� nowy report. Od gracza o id "+playerId);
            ticket = true;
        }
    }

    addPlayerLog(playerId, "Wys�a� report: " + args[1]);

    if(ticket == true)
        sendMessageToPlayer(playerId, 216, 85, 9, ChatType.OOC+"Raport odno�nie zdarzenia zosta� przekazany administratorowi.");
    else
        sendMessageToPlayer(playerId, 216, 85, 9, ChatType.OOC+"Nie ma online �adnej osoby, kt�ra mog�aby przyj�� zg�oszenie.");
})

addCommandAdmin("takereport", function(playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /takereport id_gracza");
        return;
    }

    local raportedId = args[0];
    if(isPlayerConnected(raportedId) == false)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Gracz nie jest po��czony.");
        return;
    }

    foreach(adminId, admin in getAdmins()) {
        if(admin.role >= PlayerRole.Helper) {
            sendMessageToPlayer(adminId, 0, 255, 9, ChatType.REPORT + getAccount(playerId).username + "(" + playerId + ") " + "przyja� reporta od " + getAccount(raportedId).username + "(" + raportedId + ")");
        }
    }
}, "Informuje na czacie report�w o przyj�ciu reporta", PlayerRole.Helper)

addCommandAdmin("tonline", function(playerId, params) {
    sendMessageToPlayer(playerId, 255, 255, 0, ChatType.ADMIN + "Osoby online od roli Helper:")

    local consttable = getconsttable()

    foreach(adminId, admin in getAdmins())
    {
        if(admin.role >= PlayerRole.Helper)
            sendMessageToPlayer(playerId, 0, 255, 9, ChatType.ADMIN + getAccount(adminId).username + "(" + adminId + ") " + "rola: " + getRoleName(admin.role));
    }
}, "Informuje na czacie administratorskim kto z ekipy jest online", PlayerRole.Helper)

addCommand("ignorepw", function(playerId, params) {
    getPlayer(playerId).ignorepw = !getPlayer(playerId).ignorepw
    sendMessageToPlayer(playerId, 118, 46, 212, ChatType.OOC+(getPlayer(playerId).ignorepw ? "Zablokowa�e� sobie wiadomo�ci prywatne" : "Odblokowa�e� sobie wiadomo�ci prywatne"));
})

addCommand(["pw", "pm", "w"], function(playerId, params) {
    local args = sscanf("ds", params);
    if (!args)  {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /pw id wiadomo��");
        return;
    }

    local sendId = args[0];
    if(isPlayerConnected(sendId) == false) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Gracz nie jest po��czony.");
        return;
    }

    if (getPlayer(sendId).ignorepw)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Ten gracz ignoruje prywatne wiadomo�ci.");
        return
    }

    local text = args[1];
    local position = getPlayerPosition(playerId);

    foreach(adminId, admin in getAdmins()) {
        if(admin.seePw == true) {
            if(admin.seePwRange != null) {
                if(getPlayer(admin).distanceToPosition(position))
                    sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
            } else
                sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
        }

        if(admin.hasPlayerInPwTable(sendId) || admin.hasPlayerInPwTable(playerId))
            sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
    }

    addChatLog(playerId,text,"pw");
    getPlayer(playerId).lastPWId = sendId
    getPlayer(sendId).lastPWId = playerId
    sendMessageToPlayer(playerId, 118, 46, 212, ChatType.OOC+"Wiadomo�� do "+getPlayerRealName(playerId, sendId) + "(" + sendId + ")"+ " >> "+text);
    sendMessageToPlayer(sendId, 37, 138, 221, ChatType.OOC+"Wiadomo�� od "+getPlayerRealName(sendId, playerId) + "(" + playerId + ")"+ " >> "+text);
})

addCommand(["r", "re"], function(playerId, params) {
    local args = sscanf("s", params);
    if (!args)  {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /r wiadomo��");
        return;
    }

    local sendId = getPlayer(playerId).lastPWId;
    if(isPlayerConnected(sendId) == false) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Gracz nie jest po��czony.");
        return;
    }

    if (getPlayer(sendId).ignorepw)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Ten gracz ignoruje prywatne wiadomo�ci.");
        return
    }
    local text = args[0];
    local position = getPlayerPosition(playerId);

    foreach(adminId, admin in getAdmins()) {
        if(admin.seePw == true) {
            if(admin.seePwRange != null) {
                if(getPlayer(admin).distanceToPosition(position))
                    sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
            } else
                sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
        }

        if(admin.hasPlayerInPwTable(sendId) || admin.hasPlayerInPwTable(playerId))
            sendMessageToPlayer(adminId, 118, 46, 212, ChatType.OOC+"PW od "+getAccount(playerId).username+"("+playerId+") do "+getAccount(sendId).username+"("+sendId+") wiadomo��: "+text);
    }

    addChatLog(playerId,text,"pw");
    sendMessageToPlayer(playerId, 118, 46, 212, ChatType.OOC+"Wiadomo�� do "+getPlayerRealName(playerId, sendId) + "(" + sendId + ")"+ " >> "+text);
    sendMessageToPlayer(sendId, 37, 138, 221, ChatType.OOC+"Wiadomo�� od "+getPlayerRealName(sendId, playerId) + "(" + playerId + ")"+ " >> "+text);
})

addCommand("test", function(playerId, params) {
    local args = sscanf("d", params);
    if (!args)  {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /test <stat id>");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"0 - si�a, 1 - zr�czno��, 2 - inteligencja, 3 - kr�g magii");
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"4 - 1h, 5 - 2h, 6 - �uk, 7 - kusze, 8 - witalno��, 9 - mana");
        return;
    }

    local stat = args[0];
    if(stat < 0 || stat > 9)
        return;

    local max = 0, stastistic = "";
    switch(stat) {
        case 0: max = getPlayerStrength(playerId); stastistic = "si�a"; break;
        case 1: max = getPlayerDexterity(playerId); stastistic = "zr�czno��"; break;
        case 2: max = getPlayerInteligence(playerId); stastistic = "inteligencja"; break;
        case 3: max = getPlayerMagicLevel(playerId); stastistic = "kr�g magii"; break;
        case 4: max = getPlayerSkillWeapon(playerId,0); stastistic = "1h"; break;
        case 5: max = getPlayerSkillWeapon(playerId,1); stastistic = "2h"; break;
        case 6: max = getPlayerSkillWeapon(playerId,2); stastistic = "�uki"; break;
        case 7: max = getPlayerSkillWeapon(playerId,3); stastistic = "kusze"; break;
        case 8: max = getPlayerHealth(playerId); stastistic = "witalno��"; break;
        case 9: max = getPlayerMana(playerId); stastistic = "mana"; break;
    }

    local random = irand(max);
    local result = 0;
    if(max > 11){
            local y = 2;
            for (local i = 10; y <= max; ++y){
                local z = (y/2);
                if (max == (i+y))
                    result = (random+z)
                else if (result > max)
                    result = result - z;
        }
    local bonus = ((max/2)-5);

    local object = Config["ChatMethod"][ChatMethods.Action];
    distanceChat(playerId, ChatType.IC, object.distance, 0, 204, 255, "[PLAYER:"+playerId+"] rzuci� kostk� "+max+" z bonusem "+bonus+" na statystyk� ("+stastistic+"). Wynik:"+result);
    }
    if(max >= 0 && max <= 11){
        local bonus = 0;
        local object = Config["ChatMethod"][ChatMethods.Action];
        distanceChat(playerId, ChatType.IC, object.distance, 0, 204, 255, "[PLAYER:"+playerId+"] rzuci� kostk� "+max+" z bonusem "+bonus+" na statystyk� ("+stastistic+"). Wynik:"+random);
    }
});

addCommand("kostka", function(playerId, params) {
    local args = sscanf("dd", params);
    if (!args)  {
        sendMessageToPlayer(playerId, 152, 30, 30, ChatType.IC+"B��d sk�adni. Wpisz /kostka <ilo��_oczek> <ilo��_rzut�w>!");
        return;
    }

    local max_range = args[0];
    if(max_range <= 0 || max_range > 100)
        max_range = 1;

    local amount = args[1];
    if(amount <= 0 || amount > 100)
        amount = 6;

    local throws = "";
    for(local i = 0; i < amount; i++)
    {
        local throwBone = (rand() % max_range) + 1;
        throws = throws + throwBone + ", ";
    }

    throws = throws.slice(0, -2);
    local object = Config["ChatMethod"][ChatMethods.Action];
    distanceChat(playerId, ChatType.IC, object.distance, 0, 204, 255, "[PLAYER:"+playerId+"] rzuca ko�ci� "+throws+" o tylu �cianach: "+max_range);
});

addCommand("animacje", function(playerId, params) {
    local i = 0;
    local text = "";
    foreach(index, value in Config["AnimationCommands"]) {
        text += "/"+index+" ";
        i = i + 1;
        if(i == 4) {
            sendMessageToPlayer(playerId, 255, 255, 255, ChatType.OOC+""+text);
            i = 0;
            text = "";
        }
    }

    if(i > 0)
        sendMessageToPlayer(playerId, 255, 255, 255, ChatType.OOC+" "+text);
})
addCommand("fck", function(playerId, params) {
    if(params == "")
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /fck <powd>");
        return;
    }

    local player = getPlayer(playerId);
    
    
    if(player.ck())
    {
        addPlayerLog(playerId, "Na�o�y� sobie CK o tre�ci: " + params);
        sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("Gracz %s nalo�y� na sobie CK! Pow�d to: %s", getPlayerName(playerId), params));
        kick(playerId, "Twoja posta� zgin�a. Nie otrzymujesz dodatkowych PN.");
    }
})

addCommand("sprobuj", function(playerId, params) {
    if(params == "")
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /sprobuj <co chcesz wykonac> np. /sprobuj wejsc na drzewo");
        return;
    }
    local max = 100;
    local rand = irand(max);

    if(rand >= 50) {
        local object = Config["ChatMethod"][ChatMethods.Action];
        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] z powodzeniem spr�bowa� " + params);
    } else {
        local object = Config["ChatMethod"][ChatMethods.Action];
        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] z niepowodzeniem spr�bowa� " + params);
    }
})

addCommand("opis", function(playerId, params) {
    local args = sscanf("s", params);
    local description = args[0];
    if(description == "") {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.OOC+"Wpisz /opis <opis_postaci> ");
        return;
    }
    sendMessageToPlayer(playerId, 118, 46, 212, ChatType.OOC+"Ustawi�e� opis postaci: " + description);
    getPlayer(playerId).description = description;
    Query().update(Player.dbTable, ["description"], ["'" + description + "'"]).where(["id = " + getPlayer(playerId).rId]).execute();
    PlayerPacket(playerId).setPlayerDescription(playerId, description);
})

addCommand("usun", function(playerId, params) {
    sendMessageToPlayer(playerId, 118, 46, 212, ChatType.OOC+"Usun��e� opis postaci.");
    getPlayer(playerId).description = "Brak";
    Query().update(Player.dbTable, ["description"], ["'Brak'"]).where(["id = " + getPlayer(playerId).rId]).execute();
    PlayerPacket(playerId).setPlayerDescription(playerId, "Brak");
})
