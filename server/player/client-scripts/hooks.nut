
_setPlayerFatness <- setPlayerFatness;
_setPlayerScale <- setPlayerScale;

function setPlayerScale(playerId, x, y, z)
{
    if(playerId >= getMaxSlots()) {
        _setPlayerScale(playerId, x, y, z);
        return;
    }

    PlayerPacket().sendScale(playerId, x, y, z);
}

function setPlayerWalkingStyle(playerId, walkId)
{
    local rangedWeapon = getPlayerRangedWeapon(heroId);
    if(rangedWeapon != -1 && rangedWeapon != null && getItemScheme(Items.name(rangedWeapon).toupper()).flag == ITEMCAT_DRAGGABLE) {
        return;
    }

    PlayerPacket().sendWalkingStyle(playerId, walkId);
}

function setPlayerSpeech(playerId, value)
{
    if(playerId >= getMaxSlots())
        return;

    PlayerPacket().sendSpeech(playerId, value);
}

function setPlayerFatness(playerId, value)
{
    if(playerId >= getMaxSlots()) {
        _setPlayerFatness(playerId, value);
        return;
    }

    PlayerPacket().sendFatness(playerId, value);
}

function setPlayerStamina(playerId, value) {
    Hero.stamina = value;
}

function getPlayerStamina(playerId) {
    return Hero.stamina;
}

function setPlayerHunger(playerId, value) {
    // if(Hero.hunger == 100 && value >= 100){
    //     print("playAni")
    //     playAni(playerId,"S_KOTZKUEBEL_S1");
    //     value = 50;
    // }
    Hero.hunger = value;
}

function getPlayerHunger(playerId) {
    return Hero.hunger;
}

function setPlayerEndurance(playerId, value) {
    Hero.endurance = value;
}

function getPlayerEndurance(playerId) {
    return Hero.endurance;
}

function getPlayerRealName(playerId) {
    if(Hero.realNames[playerId] == "" || (getPlayer(playerId).masked && playerId != heroId)){
        return "Nieznajomy "+playerId;
    }

    return Hero.realNames[playerId];
}

function setPlayerRealName(playerId, value) {
    Hero.realNames[playerId] = value;
}

function setPlayerInteligence(playerId, value) {
    if(playerId != heroId)
        return;

    Hero.inteligence = value;
}

function getPlayerInteligence(playerId)
{
    if(playerId != heroId)
        return 0;

    return Hero.inteligence;
}

function setPlayerInvisible(playerId, value) {
    Hero.invisible = value;
}

function getPlayerInvisible(playerId) {
    return Hero.invisible;
}

function setPlayerMasked(playerId, value) {
    getPlayer(playerId).masked = value;
}

function isInteractionPossibleWithPlayer(playerId) {
    if(getPlayerWeaponMode(playerId) != 0)
        return false;

    if(getPlayerInstance(playerId) == "PC_HERO")
        return true;

    return false;
}

function getInventoryWeight() {
    if(Hero.inventory == null)
        return 0.0;

    return Hero.inventory.weight;
}

function getMaxInventoryWeight()
{
    return 50.0 + getPlayerStrength(heroId)/1.0 + BuffController.getExtraWeight() + checkIsPlayerWearingBag();
}

function checkIsPlayerWearingBag()
{
    local rangedWeapon = getPlayerRangedWeapon(heroId);
    if(rangedWeapon != -1 && rangedWeapon != null) {
        local instance = Items.name(rangedWeapon).toupper();
        if(getItemScheme(instance) == null)
            return 0;

        if(getItemScheme(instance).flag == ITEMCAT_BAG)
            return getItemScheme(instance).weight;
    }

    return 0;
}

function checkOverWeight()
{
    local weight = getInventoryWeight();
    local maxWeight = getMaxInventoryWeight();
    Hero.isOverweight = weight > maxWeight;
}

function getPositionDifference(position1, position2)
{
    if(position1 == null || position2 == null)
        return 400000;

    return getDistance3d(position1.x, position1.y, position1.z, position2.x, position2.y, position2.z)
}

/**
* Get eye area of bot
*
* @param range - distance in witch we wanna to create eye area
* @return position - table {x, y, z}
*/
function getPlayerEyeArea(rangeView)
{
    local rot = getPlayerAngle(heroId);
    local pos = getPlayerPosition(heroId);

    if(rot < 0)
        rot += (ceil(abs(rot)/360.0))*360;
    else
        rot -= (floor(rot/360))*360.0;

    if(rot%90==0)
    {
        if(rot == 0)		pos.z += rangeView;
        else if(rot == 90)	pos.x += rangeView;
        else if(rot == 180)	pos.z -= rangeView;
        else if(rot == 270)	pos.x -= rangeView;
    }
    else
    {
        local mrot = (floor(rot/90))*90;
        local a = cos((rot - mrot) * ( PI / 180 )) * rangeView;
        local b = sin((rot - mrot) * ( PI / 180 )) * rangeView;

        if(rot > 270)		{ pos.x -= a; pos.z += b;}
        else if(rot > 180)	{ pos.x -= b; pos.z -= a;}
        else if(rot > 90)	{ pos.x += a; pos.z -= b;}
        else if(rot > 0)	{ pos.x += b; pos.z += a;}
    }

    return pos;
}

/**
* Set random respawn after death by some natural causes
*/
function setRandomRespawn() {
    setTimer(function() {
        BlackScreen(3000);


        local predefinedSpawnPoints = [
            {name="silbach_1", x=-55916.9,y=-113.438,z=-118443},
            {name="silbach_2", x=-62659.5,y=332.5,z=-112693},
            {name="silbach_3", x=-62922.4,y=318.125,z=-107362},
            {name="silbach_4", x=-51143.7,y=1088.2,z=-103544},
        ];

        local heroPos = getPlayerPosition(heroId);
        local spawns = [];
        foreach(index,pos in predefinedSpawnPoints){
            local spawnDiff = getPositionDifference(heroPos,pos);
            spawns.push({x = pos.x, y=pos.y, z=pos.z, diff= spawnDiff})
        }
        local linq = Linq(spawns);
        local orderedSpawns = linq.OrderBy("diff").Take(2).ToArray();
        local newPos = orderedSpawns[irand(orderedSpawns.len() - 1)];

        setPlayerPosition(heroId, newPos.x,newPos.y,newPos.z)

        addPlayerNotification(heroId, "Obudzi�e� si� w innym miejscu.");
    }, 1500, 1);
}

/*
    GodMode functions
*/

function setGodMode(value)
{
    getPlayer(heroId).setGodMode(value);
}

function isGodModeEnabled()
{
    return getPlayer(heroId).isGodModeEnabled();
}

function setUnconscious(value) {
    if(Hero.unconscious != 0 && value == 0){
        setFreeze(false);
        playAni(heroId, "S_RUN");
        Hero.unconsciousDraw.visible = false;
    }else if(Hero.unconscious == 0 && value != 0) {
        playAni(heroId, "S_DEADB");
        Hero.unconsciousDraw.text = "Jeste� nieprzytomny: "+Hero.unconscious;
        Hero.unconsciousDraw.visible = true;
    }

    Hero.unconscious = value;
}

function getUnconscious() {
    return Hero.unconscious;
}

function setCarried(value) {
    setPlayerCollision(Hero.carried, true);
    playAni(heroId, "S_RUN");
    Hero.carried = value;
    setPlayerCollision(Hero.carried, false);
}

function getCarried() {
    return Hero.carried;
}

function setCarrying(value) {
    setPlayerCollision(Hero.carrying, true);
    Hero.carrying = value;
    setPlayerCollision(Hero.carrying, false);
}

function getCarrying() {
    return Hero.carrying;
}

addEventHandler ("onCommand", function (cmd, params) {
    foreach(index, value in Config["AnimationCommands"]) {
        if(cmd == index)
            playAni(heroId, value);
    }
});