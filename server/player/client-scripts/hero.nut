
Hero <- {
    realId = -1,

    loggIn = false,
    isNew = false,
    stamina = 100.0,
    hunger = 100.0,
    isAdmin = false,
    isOverweight = false,
    movingVob = false,

    unconscious = 0,
    unconsciousDraw = Draw(0,0,""),

    carried = -1,
    carrying = -1,

    inteligence = 0,
    endurance = 100,

    skills = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    equippedItems = [],

    personalCard = [],
    personalNotes = [],

    realNames = [],
    animations = {},

    shield = -1,
    helmet = -1,
    armor = -1,
    melee = -1,
    ranged = -1,

    spells = [-1, -1, -1, -1, -1, -1, -1, -1],
    rings = [-1, -1],
    belt = -1,
    amulet = -1,
    speech = -1,

    fractionId = -1,
    fractionRoleId = -1,

    goActive = false,

    invisible = false,
    inventory = null,

    goTo = function(isWalking, x, y, z, angle)
    {
        if(goActive != false)
            return;

        goActive = { x = x, y = y, z = z, angle = angle, anim = isWalking ? "S_WALKL" : "S_RUNL" };
    }
}

function resetHeroStatistics() {
    Hero = {
        realId = -1,

        loggIn = false,
        isNew = false,
        stamina = 100.0,
        hunger = 100.0,
        isAdmin = false,
        isOverweight = false,
        movingVob = false,

        unconsciousDraw = Draw(0,0,""),
        unconscious = 0,
        carried = -1,
        carrying = -1,

        inteligence = 0,
        endurance = 100,

        skills = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        equippedItems = [],

        personalCard = [],
        personalNotes = [],

        realNames = [],
        animations = {},

        shield = -1,
        helmet = -1,
        armor = -1,
        melee = -1,
        ranged = -1,

        spells = [-1, -1, -1, -1, -1, -1, -1, -1],
        rings = [-1, -1],
        belt = -1,
        amulet = -1,
        speech = -1,

        fractionId = -1,
        fractionRoleId = -1,

        goActive = false,

        invisible = false,
        inventory = null,

        goTo = function(isWalking, x, y, z, angle)
        {
            if(goActive != false)
                return;

            goActive = { x = x, y = y, z = z, angle = angle, anim = isWalking ? "S_WALKL" : "S_RUNL" };
        }
    }


    for(local i = 0; i < getMaxSlots(); i ++)
        Hero.realNames.append("");
}

SPRINT_MDS <- Mds.id("HUMANS_SPRINT2.MDS");
WOUNDED_MDS <- Mds.id("HUMANS_WOUNDED.MDS");
DRUNKEN_MDS <- Mds.id("HUMANS_DRUNKEN.MDS");

for(local i = 0; i < getMaxSlots(); i ++)
    Hero.realNames.append("");

local call = getTickCount();

addEventHandler("onRender", function() {
    if(call > getTickCount())
        return;

    local callEventNeed = false;

    local ringLeftHand = getPlayerRing(heroId, HAND_LEFT);
    local ringRightHand = getPlayerRing(heroId, HAND_RIGHT);

    if(Hero.rings[0] != ringLeftHand || Hero.rings[1] != ringRightHand)
    {
        callEventNeed = true;
        Hero.rings = [ringLeftHand, ringRightHand];
    }

    local belt = getPlayerBelt(heroId);
    if(belt != Hero.belt) {
        callEventNeed = true;
        Hero.belt = belt;
    }

    local armor = getPlayerArmor(heroId);
    if(armor != Hero.armor) {
        callEventNeed = true;
        Hero.armor = armor;
    }

    local melee = getPlayerMeleeWeapon(heroId);
    if(melee != Hero.melee) {
        callEventNeed = true;
        Hero.melee = melee;
    }

    local ranged = getPlayerRangedWeapon(heroId);
    if(ranged != Hero.ranged) {
        callEventNeed = true;
        Hero.armor = ranged;
    }

    local amulet = getPlayerAmulet(heroId);
    if(amulet != Hero.amulet) {
        callEventNeed = true;
        Hero.amulet = amulet;
    }

    local shield = getPlayerShield(heroId);
    if(shield != Hero.shield) {
        callEventNeed = true;
        Hero.shield = shield;
    }

    local helmet = getPlayerHelmet(heroId);
    if(helmet != Hero.helmet) {
        callEventNeed = true;
        Hero.helmet = helmet;
    }

    for(local i = 0; i <= 7; i ++)
    {
        local spell = getPlayerSpell(heroId, i);
        if(spell != Hero.spells[i]) {
            callEventNeed = true;
            Hero.spells[i] = spell;
        }
    }

    if(callEventNeed)
    {
        Hero.equippedItems = [ringLeftHand, ringRightHand, belt, armor, melee, ranged];

        foreach(spell in Hero.spells)
            Hero.equippedItems.push(spell);

        callEvent("onChangeEquippedItems");
    }

    checkOverWeight();

        if(Hero.unconscious != 0) {
        if(Hero.carried == -1) {
            if(!(getPlayerAni(heroId) == "T_VICTIM" || getPlayerAni(heroId) == "S_DEADB" || getPlayerAni(heroId) == "T_DEADB"))
                playAni(heroId, "S_DEADB");
        }

        if(getPlayerWeaponMode(heroId) != 0)
            setPlayerWeaponMode(heroId, 0);

        if(Hero.unconscious > 1680)
            Hero.unconsciousDraw.text = "Jeste� nieprzytomny: "+Hero.unconscious+" (Mo�esz zasn�� po up�ywie dw�ch minut)";
        else
            Hero.unconsciousDraw.text = "Jeste� nieprzytomny: "+Hero.unconscious+" (Naci�nij C aby zasn��, V aby wsta�)";

        if(getSetting("OtherCustomFont")) {
            Hero.unconsciousDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            Hero.unconsciousDraw.setScale(0.45,0.45);
        }

        Hero.unconsciousDraw.setPosition(8129 - Hero.unconsciousDraw.width - 50, 7600);
        Hero.unconsciousDraw.setColor(255, 255, 255);
        setFreeze(true);
    }

    call = getTickCount() + 500;
})

function getPlayerAttributeUpgradeObject(attributeId)
{
    local value = 0;
    switch(attributeId)
    {
        case PlayerAttributes.Str:
            value = getPlayerStrength(heroId);
        break;
        case PlayerAttributes.Dex:
            value = getPlayerDexterity(heroId);
        break;
        case PlayerAttributes.Int:
            value = getPlayerInteligence(heroId);
        break;
        case PlayerAttributes.MagicLvl:
            value = getPlayerMagicLevel(heroId);
        break;
        case PlayerAttributes.Mana:
            value = getPlayerMaxMana(heroId);
        break;
        case PlayerAttributes.Hp:
            value = getPlayerMaxHealth(heroId);
        break;
        case PlayerAttributes.OneH:
            value = getPlayerSkillWeapon(heroId, 0);
        break;
        case PlayerAttributes.TwoH:
            value = getPlayerSkillWeapon(heroId, 1);
        break;
        case PlayerAttributes.Bow:
            value = getPlayerSkillWeapon(heroId, 2);
        break;
        case PlayerAttributes.Cbow:
            value = getPlayerSkillWeapon(heroId, 3);
        break;
    }

    if(!(attributeId in Config["PlayerAttributeUpgrade"]))
        return null;

    local objs = Config["PlayerAttributeUpgrade"][attributeId];
    local obj = null;

    foreach(_item in objs)
    {
        if(_item.min > value)
            continue;

        obj = _item;
    }

    if(obj == null)
        return null;

    if("max" in obj)
    {
        if(value >= obj.max)
            return null;
    }

    local cost = 1;

    if("costLp" in obj)
        cost = obj.costLp;

    local valueAfter = 1;

    if("value" in obj)
        valueAfter = obj.value;

    return {cost = cost, attribute = attributeId, value = valueAfter, afterValue = value };
}

function getPlayerSkillUpgradeObject(skillId)
{
    local value = Hero.skills[skillId];

    if(!(skillId in Config["PlayerSkillUpgrade"]))
        return null;

    local objs = Config["PlayerSkillUpgrade"][skillId];
    local obj = null;

    foreach(_item in objs)
    {
        if(_item.min > value)
            continue;

        obj = _item;
    }

    if(obj == null)
        return null;

    if("max" in obj)
    {
        if(value >= obj.max)
            return null;
    }

    local cost = 1;

    if("costLp" in obj)
        cost = obj.costLp;

    local valueAfter = 1;

    if("value" in obj)
        valueAfter = obj.value;

    return {cost = cost, skill = skillId, value = valueAfter, afterValue = value };
}

function isPlayerBehindPlayer(playerId) {
    local pos = getPlayerPosition(playerId);
    local angle = getPlayerAngle(playerId) + 180;
    if(angle > 360)
        angle = angle - 360;

    local targetPosition = {x = pos.x, y = pos.y, z = pos.z}

    targetPosition.x = pos.x + (sin(angle * 3.14 / 180.0) * 160);
    targetPosition.z = pos.z + (cos(angle * 3.14 / 180.0) * 160);
    local heroAngle = getPlayerAngle(heroId);
    angle = getPlayerAngle(playerId);
    if(!(heroAngle >= angle - 30 && heroAngle <= angle + 30))
        return false;

    local heroPos = getPlayerPosition(heroId);
    return getDistance3d(heroPos.x, heroPos.y, heroPos.z, targetPosition.x, targetPosition.y, targetPosition.z) < 150;
}

function addPlayerNotification(playerId, notification) {
    Notification.add(notification);
}

function applyOverlay(mdsId) {
    if(mdsId in Hero.animations)
        Hero.animations[mdsId] = getTickCount();
    else
        Hero.animations[mdsId] <- getTickCount();
}

function removeOverlay(mdsId) {
    if(mdsId in Hero.animations)
        Hero.animations.rawdelete(mdsId);
}

function removeAllOverlay() {
    Hero.animations.clear();
    Hero.animations = {};
}

function isPlayerSiting(playerId) {
    local sittingAnims = [
        "t_Chair_Stand_2_S0",
        "s_Chair_S0",
        "t_Chair_S0_2_Stand",
        "t_Chair_S0_2_S1",
        "s_Chair_S1",
        "t_Chair_S1_2_S0",
        "R_Chair_Random_1",
        "R_Chair_Random_2",
        "R_Chair_Random_3",
        "R_Chair_Random_4",
        "t_Bench_Stand_2_S0",
        "s_Bench_S0",
        "t_Bench_S0_2_Stand",
        "t_Bench_S0_2_S1",
        "s_Bench_S1",
        "t_Bench_S1_2_S0",
        "R_Bench_Random_1",
        "R_Bench_Random_2",
        "R_Bench_Random_3",
        "R_Bench_Random_4",
        "t_Throne_Stand_2_S0",
        "s_Throne_S0",
        "t_Throne_S0_2_Stand",
        "t_Throne_S0_2_S1",
        "s_Throne_S1",
        "t_Throne_S1_2_S0",
    ];

    local pAnim = getPlayerAni(playerId);
    foreach(anim in sittingAnims)
        if(anim.toupper() == pAnim.toupper())
            return true;

    return false;
}

local lastAnim = null;

addEventHandler("onRender", function() {
    if(lastAnim != getPlayerAni(heroId))
    {
        lastAnim = getPlayerAni(heroId);
        callEvent("onPlayerChangeAnim", lastAnim);
    }

    if(Hero.goActive != false)
    {
        setPlayerPosition(heroId, Hero.goActive.x,Hero.goActive.y,Hero.goActive.z);
        setPlayerAngle(heroId, Hero.goActive.angle);
        Hero.goActive = false;
        // disableControls(true);
        // local position = getPlayerPosition(heroId);
        // local angle = getPlayerAngle(heroId);
        // if(getPlayerAni(heroId) != Hero.goActive.anim)
        //     playAni(heroId, Hero.goActive.anim);

        // print("ANI_PLAYER: " + getPlayerAni(heroId));
        // print("ANI_HERO: " + Hero.goActive.anim);
        // local _angle = getVectorAngle(position.x,position.z,Hero.goActive.x,Hero.goActive.z);
        // local angleDiff = abs(_angle - angle);

        // if(angleDiff > 5)
        //     setPlayerAngle(heroId, _angle);

        // if(getPositionDifference(position, Hero.goActive) < 100)
        // {
        //     disableControls(false);
        //     setPlayerPosition(heroId, Hero.goActive.x,Hero.goActive.y,Hero.goActive.z);
        //     setPlayerAngle(heroId, Hero.goActive.angle);
        //     stopAni(heroId);
        //     Hero.goActive = false;
        // }
    }

    if(Hero.carried != -1) {
        local carriedPos = getPlayerPosition(Hero.carried);
        carriedPos.x = carriedPos.x - (sin(getPlayerAngle(Hero.carried) * 3.14 / 180.0) * 40);
        carriedPos.z = carriedPos.z - (cos(getPlayerAngle(Hero.carried) * 3.14 / 180.0) * 40);

        disableControls(false);
        setPlayerPosition(heroId, carriedPos.x, carriedPos.y, carriedPos.z);
        setPlayerAngle(heroId, getPlayerAngle(Hero.carried))

        if(getPlayerAni(heroId) != "S_NOSZONY")
            playAni(heroId, "S_NOSZONY");
    }
});

local lastAnim = getPlayerAni(heroId);


local disabledBecouseDraggableItems = false, disableTimer = null, disableDraggableDropDraw = Draw(3000, 4000, "Nie mo�esz nie�� dw�ch ci�kich przedmiot�w jednocze�nie.");
addEventHandler ("onSecond", function () {
    if(getPlayerWeaponMode(heroId) != 0) {
        if(WOUNDED_MDS in Hero.animations || DRUNKEN_MDS in Hero.animations) {
            addPlayerNotification(heroId, "Jeste� ranny, nie mo�esz walczy�.")
            setPlayerWeaponMode(heroId, 0);
        } else {
            local rangedWeapon = getPlayerRangedWeapon(heroId);
            if(rangedWeapon != -1 && rangedWeapon != null) {
                if(getItemScheme(Items.name(rangedWeapon).toupper()).flag == ITEMCAT_DRAGGABLE) {
                    addPlayerNotification(heroId, "Masz przedmiot, kt�ry nie pozwala ci podnie�� broni.")
                    setPlayerWeaponMode(heroId, 0);
                    stopAni(heroId);
                }
            }
        }
    }

    local draggableOne = 0;
    if( Hero.inventory != null) {
        foreach(_item in Hero.inventory.getItems()) {
            if(_item.flag == ITEMCAT_DRAGGABLE)
                draggableOne += _item.amount;
        }
    }

    if(draggableOne >= 2) {
        if(disabledBecouseDraggableItems == false) {
            disabledBecouseDraggableItems = true;
            stopAni(heroId);
            disableDraggableDropDraw.visible = true;

            disableTimer = setTimer(function() {
                setFreeze(true);
            }, 50, 0)
        }
    }else {
        if(disabledBecouseDraggableItems) {
            killTimer(disableTimer);
            setFreeze(false);
            disableDraggableDropDraw.visible = false;
            disabledBecouseDraggableItems = false;
        }
    }
});

local overweightInfo = Draw(7300, 7200, "Jeste� przeci��ony");
overweightInfo.font = "FONT_OLD_20_WHITE_HI.TGA";
overweightInfo.setScale(0.4,0.4);
overweightInfo.setColor(230, 0, 0);

addEventHandler("onPlayerChangeAnim", function(anim) {
    if(Hero.isOverweight) {
        stopAni(heroId);
        overweightInfo.visible = true;
    }else
        if(overweightInfo.visible)
            overweightInfo.visible = false;
})

local tick = getTickCount(), spellId = -1;

addEventHandler("onPlayerSpellCast", function(playerId, _spellId) {
    if (heroId == playerId)
        spellId = _spellId;
});

addEventHandler ("onPlayerChangeAnim", function (anim) {
    if(tick > getTickCount())
        return;

    if(anim == "T_SUMSHOOT_2_STAND" || anim == "T_HEASHOOT_2_STAND" || anim == "T_SLESHOOT_2_STAND") {
        tick = getTickCount() + 3000;
        PlayerPacket().summonSpell(spellId, getPlayerFocus());
    }
});

addEventHandler ("onKey", function (key) {
    if (chatInputIsOpen())
        return

    if (isConsoleOpen())
        return

    if(ActiveGui != null)
        return;

    if(Hero.unconscious != 0 && Hero.unconscious < 1680) {
        if(key == KEY_C) {
            PlayerPacket().setUnconscious();
            BlackScreen(3000);
            local pos = getPlayerPosition(heroId);
            setPlayerPosition(heroId, -106925, -467.734, -108226);
        } else if(key == KEY_V) {
            if(Hero.unconscious < 1680) {
                PlayerPacket().setUnconscious();
                BlackScreen(3000);
            }
            else {
                addPlayerNotification(heroId, "Musisz odczeka� 2 minuty za nim b�dziesz m�g� wsta�.");
            }
        }
    }

    if(Hero.carrying != -1)
        if(key == KEY_SPACE)
            PlayerPacket().setCarrying();
});

_hasItem <- hasItem;

function hasItem(heroId, itemInstance) {
    return _hasItem(heroId, Items.id(itemInstance.toupper()));
}

function hasItemThatIsNotDestroyed(itemInstance) {
    itemInstance = itemInstance.toupper();
    foreach(_item in Hero.inventory.getItems()) {
        if(_item.instance == itemInstance)
            if(_item.resistance > 0) {
                return true;
            }
    }

    return false;
}