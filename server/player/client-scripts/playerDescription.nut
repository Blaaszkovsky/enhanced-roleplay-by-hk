// GUI Instantions
descriptionDraw <- GUI.Draw(anx(30), any(Resolution.y - 280), "")
//

descriptions <- array(getMaxSlots(), "Brak");

local function getScaledDescription(id)
{
	local text = "Opis: #"
	local width = 0
	local lines = 0

	width = textWidth(text)

	foreach(letter in descriptions[id])
	{
		if (width + letterWidth(letter.tochar()) > 8192 * 0.2 && letter.tochar() == " ")
		{
			text += "\n" + letter.tochar()
			width = 0
			lines += 1
		}
		else
		{
			width += letterWidth(letter.tochar())
			text += letter.tochar()
		}
	}

	text += "#"

	descriptionDraw.setPosition(anx(30), any(Resolution.y - 250 - lines * letterWidthPx("A")))
	return text
}

addEventHandler("onFocus", function(newId, oldId)
{
	if (newId > getMaxSlots())
	{
		if (descriptionDraw.getVisible())
			descriptionDraw.setVisible(false)

		return
	}
	else
		descriptionDraw.setVisible(true)

	if (newId == -1)
	{
		if (descriptions[heroId] == "Brak" || descriptions[heroId] == null)
			descriptionDraw.setText("")
		else
			descriptionDraw.setText(getScaledDescription(heroId))
	}
	else
	{
		if (descriptions[newId] == "Brak" || descriptions[heroId] == null)
			descriptionDraw.setText("")
		else
			descriptionDraw.setText(getScaledDescription(newId))
	}
})

addEventHandler("onPacket", function(packet)
{
	local identfier = packet.readUInt8();
	if(identfier != PacketReceivers)
		return;

	local typeId = packet.readUInt8();
	if(typeId != Packets.Player)
		return;

	typeId = packet.readUInt8();
	if(typeId != PlayerPackets.PlayerDescription)
		return;

	local id = packet.readUInt16()
	local text = packet.readString();

	descriptions[id] = text

	if (heroId == id)
	{
		descriptionDraw.setVisible(true)

		if (text == "Brak")
			descriptionDraw.setText("")
		else
			descriptionDraw.setText(getScaledDescription(heroId))
	}
})

addEventHandler("onOpenGUI", function() {
    descriptionDraw.setVisible(false)
})

addEventHandler("onCloseGUI", function() {
	descriptionDraw.setVisible(true)
})