
PlayerRecovery <- {
    activeInteractionMob = null,

    onSecond = function() {
        if(activeInteractionMob == null)
            return;

        PlayerPacket().recovery(activeInteractionMob);
    }

    onMobInteract = function (pointer, mobType, from, to) {
        if(from == 1) {
            activeInteractionMob = null
            return;
        }

        local visual = Mob(pointer).visual;
        foreach(recover in Config["PlayerRecovery"]) {
            if(recover.name == visual) {
                activeInteractionMob = recover.value;
                return;
            }
        }

        activeInteractionMob = null;
    }
}

addEventHandler("onMobInteract", PlayerRecovery.onMobInteract.bindenv(PlayerRecovery));
addEventHandler("onSecond", PlayerRecovery.onSecond.bindenv(PlayerRecovery))