
PlayerInteraction <- {
    bucket = {}
    playerId = -1

    onInit = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 125), any(Resolution.y/2 - 200), anx(250), any(380), "SG_BOX.TGA");
        result.playerName <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Nazwa", result.window);
        result.playerTrap <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Zwi��", result.window);
        result.playerUnTrap <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Rozwi��", result.window);
        result.playerTrade <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Handluj", result.window);
        result.playerHeal <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Ulecz", result.window);
        result.playerSearch <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Przeszukaj", result.window);
        result.playerDescription <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Opis", result.window);
        result.playerExit <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);
        result.playerSteal <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Okradnij", result.window);
        result.playerManage <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Zarz�dzaj (frakcja)", result.window);
        result.playerInvite <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Zapro� (frakcja)", result.window);
        result.playerTake <- GUI.Button(anx(50), any(0), anx(150), any(60), "SG_BUTTON.TGA", "Podnie�", result.window);

        result.setNameWindow <- GUI.Window(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 140), anx(400), any(220), "SG_BOX.TGA");
        result.nameInput <- GUI.Input(anx(50), any(40), anx(300), any(55), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2, result.setNameWindow);
        result.playerAccept <- GUI.Button(anx(100), any(120), anx(150), any(60), "SG_BUTTON.TGA", "Zatwierd�", result.setNameWindow);

        result.playerAccept.bind(EventType.Click, function(element) {
            local text = PlayerInteraction.bucket.nameInput.getText();
            if(text.len() < 3) {
                addPlayerNotification(heroId, "Zbyt kr�tka nazwa.");
                return;
            }

            PlayerPacket().setRealName(PlayerInteraction.playerId, text);
            addPlayerNotification(heroId, "Ustawiono nazw�.");
            PlayerInteraction.bucket.nameInput.setText("");
            PlayerInteraction.hide();
        });

        result.playerName.bind(EventType.Click, function(element) {
            PlayerInteraction.bucket.window.setVisible(false);
            PlayerInteraction.bucket.setNameWindow.setVisible(true);
        });

        result.playerUnTrap.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.UnTrap);
        });

        result.playerTrap.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Trap);
        });

        result.playerTrade.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Trade);
        });

        result.playerSearch.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Search);
        });

        result.playerDescription.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerPacket().sendPlayerDescription(playerId);
        });

        result.playerInvite.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Invite);
        });

        result.playerManage.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            FractionsInterface.show(playerId);
            PlayerPacket().getPlayerManageFractionPlayer(playerId);
        });

        result.playerHeal.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Heal);
        });

        result.playerTake.bind(EventType.Click, function(element) {
            local playerId = PlayerInteraction.playerId;
            PlayerInteraction.hide();
            PlayerAsk.start(playerId, PlayerAskType.Take);
        });

        result.playerExit.bind(EventType.Click, function(element) {
            PlayerInteraction.hide();
        });

        result.playerSteal.bind(EventType.Click, function(element) {
            PlayerInteraction.hide();
            PlayerStealInteraction.show(PlayerInteraction.playerId);
        });

        bucket = result;
    }
    onInteractPlayer = function(_playerId) {
        playerId = _playerId;
        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.InteractionPlayer;
        setPlayerWeaponMode(heroId, 0);

        bucket.playerInvite.setVisible(false);
        bucket.playerManage.setVisible(false);
        bucket.playerHeal.setVisible(false);
        bucket.playerSteal.setVisible(false);

        if(getPlayerAni(playerId) != "S_DEADB")
            bucket.playerTake.setVisible(false);

        if(System.Bound.isIn(_playerId)) {
            bucket.playerTrap.setVisible(false);
            bucket.playerUnTrap.setVisible(true);
        } else {
            bucket.playerTrap.setVisible(true);
            bucket.playerUnTrap.setVisible(false);
        }

        if(hasItem(heroId, "ITMI_NACPANY") == 0) {
            bucket.playerTrap.setVisible(false);
        }

        if(Hero.skills[PlayerSkill.Medic] != 0) {
            bucket.playerHeal.setVisible(true);
        }

        if(System.Bound.isIn(heroId)) {
            bucket.playerTrap.setVisible(false);
            bucket.playerUnTrap.setVisible(false);
        }

        bucket.playerTrade.setVisible(true);

        if(getPlayer(_playerId).masked)
            bucket.playerName.setVisible(false);

        if(Hero.skills[PlayerSkill.Thief] != 0)
            bucket.playerSteal.setVisible(true);

        if(!isPlayerBehindPlayer(_playerId) || isPlayerSiting(_playerId))
            bucket.playerSteal.setVisible(false);

        Camera.setBeforePlayer(playerId, 240);

        if((getPlayerHealth(playerId) == 0 || getPlayerHealth(heroId) == 0))
            hide();

        if(canPlayerManageFraction())
            bucket.playerManage.setVisible(true);
        else
            if(canPlayerInvite())
                bucket.playerInvite.setVisible(true);

        local y = 0;
        foreach(child in bucket.window.childs) {
            if(child.pointer.m_visible) {
                child.pointer.setPosition(anx(Resolution.x/2 - 125) + anx(50), any(Resolution.y/2 - 200) + any(20 + (y * 70)));
                y = y + 1;
            }
        }
    }
    hide = function() {
        bucket.window.setVisible(false);
        bucket.setNameWindow.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
    }
    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.InteractionPlayer)
            hide();
    }
}

addEventHandler("onInit", PlayerInteraction.onInit.bindenv(PlayerInteraction));
addEventHandler("onInteractPlayer", PlayerInteraction.onInteractPlayer.bindenv(PlayerInteraction));
addEventHandler("onForceCloseGUI", PlayerInteraction.onForceCloseGUI.bindenv(PlayerInteraction));
Bind.addKey(KEY_ESCAPE, PlayerInteraction.hide.bindenv(PlayerInteraction), PlayerGUI.InteractionPlayer)