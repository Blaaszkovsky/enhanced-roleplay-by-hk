
PlayerStealInteraction <- {
    targetId = -1,
    firstPosition = null,

    show = function(_targetId) {
        if (eggInstance != null)
            return

        BaseGUI.show();
        ActiveGui = PlayerGUI.Steal;
        firstPosition = getPlayerPosition(_targetId);

        local eggTimer = EggTimer(10000);
        eggTimer.onEnd = function(eleobjectment) {
            PlayerStealInteraction.check();
        }

        targetId = _targetId;
    }

    hide = function() {
        BaseGUI.hide();
        targetId = -1;
        ActiveGui = null;
        setFreeze(false);
        resetEggTimer();
        eggTimer = null;
    }

    check = function() {
        if(ActiveGui != PlayerGUI.Steal)
            return;

        local pos = getPlayerPosition(targetId);
        if((getDistance3d(pos.x, pos.y, pos.z, firstPosition.x, firstPosition.y, firstPosition.z) > 300 || !isPlayerBehindPlayer(targetId)) && Hero.skills[PlayerSkill.Thief] == 0) {
            hide();
            return;
        }

        PlayerPacket().stealFromUser(targetId);
    }

    load = function(items) {
        hide();
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Steal)
            hide();
    }
}

addEventHandler("onForceCloseGUI", PlayerStealInteraction.onForceCloseGUI.bindenv(PlayerStealInteraction));
Bind.addKey(KEY_ESCAPE, PlayerStealInteraction.hide.bindenv(PlayerStealInteraction), PlayerGUI.Steal);
