
FractionMenu <- {
    object = null,
    bucket = null,
    fractionId = -1,

    members = {},
    memberPermissions = {},
    permissions = {},

    canInvite = null,
    canTrade = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 350), any(Resolution.y/2 - 300), anx(700), any(600), "SG_BOX.TGA", null, false)

        result.permissions <- MyGUI.SelectList(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(70), anx(300), any(210))
        result.permissionList <- MyGUI.SelectList(anx(Resolution.x/2 - 350) + anx(350), any(Resolution.y/2 - 300) + any(70), anx(300), any(210))
        result.memberList <- MyGUI.SelectList(anx(Resolution.x/2 - 350) + anx(350), any(Resolution.y/2) + any(30), anx(300), any(240))
        result.operationList <- MyGUI.SelectList(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2) + any(30), anx(300), any(240))

        result.memberList.onClick = function(id) {
            FractionMenu.member(id);
        }

        result.operationList.onClick = function(id) {
            FractionMenu.operation(id);
        }

        result.invitePlayersList <- MyGUI.DropDown(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(70), anx(600), any(60), "Gracz", [], "Wybierz gracza..", null);
        result.invitePlayersSubmit <- GUI.Button(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(170), anx(300), any(60), "SG_BUTTON.TGA", "Zapro�");
        result.invitePlayersBack <- GUI.Button(anx(Resolution.x/2) + anx(40), any(Resolution.y/2 - 300) + any(170), anx(300), any(60), "SG_BUTTON.TGA", "Wr��");

        result.invitePlayersBack.bind(EventType.Click, function(element) {
            FractionMenu.backOperation();
        });

        result.invitePlayersSubmit.bind(EventType.Click, function(element) {
            local playerId = FractionMenu.bucket.invitePlayersList.getValue();
            FractionPacket().inviteMember(FractionMenu.fractionId, playerId);
            FractionMenu.hide();
        });

        result.memberTitle <- GUI.Draw(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(30), "");
        result.memberPermissionsList <- MyGUI.DropDown(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(70), anx(600), any(60), "Uprawnienia", [], "Wybierz uprawnienia...", null, true);
        result.memberSubmit <- GUI.Button(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(170), anx(300), any(60), "SG_BUTTON.TGA", "Aktualizuj");
        result.memberRemove <- GUI.Button(anx(Resolution.x/2) + anx(40), any(Resolution.y/2 - 300) + any(170), anx(300), any(60), "SG_BUTTON.TGA", "Usu�");
        result.memberBack <- GUI.Button(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(270), anx(300), any(60), "SG_BUTTON.TGA", "Wr��");

        result.memberBack.bind(EventType.Click, function(element) {
            FractionMenu.backMember();
        });

        result.memberRemove.bind(EventType.Click, function(element) {
            local playerId = element.attribute;
            FractionPacket().removeMember(FractionMenu.fractionId, playerId);
            FractionMenu.hide();
        });

        result.memberSubmit.bind(EventType.Click, function(element) {
            local playerId = element.attribute;
            local str = "";
            foreach(item in FractionMenu.bucket.memberPermissionsList.getValue())
                str = str + item + ",";

            str = str.slice(0, -1);

            FractionPacket().updateMember(FractionMenu.fractionId, playerId, str);
            FractionMenu.hide();
        });

        result.elements <- [];

        return result;
    }

    init = function()
    {
        bucket = prepareWindow();
    }

    show = function(_fractionId)
    {
        fractionId = _fractionId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Fraction;

        PlayerPacket().playerFractionInfo(fractionId);
        BlackScreen(1000);

        bucket.window.setVisible(true);
    }

    hide = function()
    {
        BaseGUI.hide();
        ActiveGui = null;

        bucket.permissions.setVisible(false);
        bucket.permissionList.setVisible(false);
        bucket.memberList.setVisible(false);
        bucket.operationList.setVisible(false);

        bucket.invitePlayersList.setVisible(false);
        bucket.invitePlayersSubmit.setVisible(false);
        bucket.invitePlayersBack.setVisible(false);

        bucket.memberPermissionsList.setVisible(false);
        bucket.memberTitle.setVisible(false);
        bucket.memberSubmit.setVisible(false);
        bucket.memberRemove.setVisible(false);
        bucket.memberBack.setVisible(false);   

        foreach(element in bucket.elements)
            element.setVisible(false);

        bucket.window.setVisible(false);
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Fraction)
            hide();   
    }

    load = function(obj)
    {
        object = obj;

        if(ActiveGui != PlayerGUI.Fraction)
            return;

        if(obj[0].type == PlayerGUIFraction.None)
            return;

        foreach(element in bucket.elements)
            element.destroy();

        bucket.permissions.clear();
        bucket.permissionList.clear();
        bucket.memberList.clear();
        bucket.operationList.clear();

        foreach(item in obj)
        {
            switch(item.type)
            {
                case PlayerGUIFraction.Permissions:
                    bucket.elements.append(GUI.Draw(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(30), "Twoje Permisje"))

                    item.value = split(item.value, ",");
                    foreach(element in item.value)
                        bucket.permissions.addOption(-1, element);

                    bucket.permissions.setVisible(true);
                break;
                case PlayerGUIFraction.MemberPermission:
                    bucket.elements.append(GUI.Draw(anx(Resolution.x/2 - 350) + anx(350), any(Resolution.y/2 - 300) + any(30), "Permisje"))
                    permissions.clear();

                    item.value = split(item.value, ",");
                    foreach(element in item.value) {
                        local args = sscanf("ds", element);
                        permissions[args[0]] <- args[1];
                        bucket.permissionList.addOption(args[0], args[1]);
                    }

                    bucket.permissionList.setVisible(true);
                break;
                case PlayerGUIFraction.MemberList:
                    bucket.elements.append(GUI.Draw(anx(Resolution.x/2 - 350) + anx(350), any(Resolution.y/2 - 10), "Cz�onkowie"))
                    memberPermissions.clear();
                    members.clear();
                    item.value = split(item.value, ",");
                    foreach(element in item.value) {
                        element = split(element, "-");
                        local args = sscanf("ds", element[0]);
                        bucket.memberList.addOption(args[0], args[1]);
                        members[args[0]] <- args[1];
                        memberPermissions[args[0]] <- [];
                        foreach(item in split(element[1],"&"))
                            memberPermissions[args[0]].append(item.tointeger());
                    }

                    bucket.memberList.setVisible(true);
                break;
                case PlayerGUIFraction.Invite:
                    bucket.elements.append(GUI.Draw(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 10), "Uprawnienia"))
                    bucket.operationList.addOption(1, "Zapraszanie");
                    bucket.operationList.setVisible(true);
                break;
                case PlayerGUIFraction.Trade:
                    bucket.elements.append(GUI.Draw(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 10), "Uprawnienia"))
                    bucket.operationList.addOption(2, "Stragan");
                    bucket.operationList.setVisible(true);
                break;
            }
        }

        foreach(element in bucket.elements)
        {
            element.setFont("FONT_OLD_20_WHITE_HI.TGA")
            element.setScale(0.5,0.5);
            element.setColor(210, 200, 190);
            element.setVisible(true);
        }
    }

    operation = function(idType)
    {
        switch(idType)
        {
            case 1:
                bucket.permissions.setVisible(false);
                bucket.permissionList.setVisible(false);
                bucket.memberList.setVisible(false);
                bucket.operationList.setVisible(false);

                foreach(element in bucket.elements)
                    element.setVisible(false);

                local content = [];
                for(local i = 0; i < getMaxSlots(); i ++)
                    if(getPlayerName(i) != null)
                        content.push({id = i, name = getPlayerName(i)});

                bucket.invitePlayersList.setContent(content);
                bucket.invitePlayersList.setVisible(true);
                bucket.invitePlayersSubmit.setVisible(true);
                bucket.invitePlayersBack.setVisible(true);
            break;
        }
    }

    backOperation = function()
    {
        bucket.permissions.setVisible(true);
        bucket.permissionList.setVisible(true);
        bucket.memberList.setVisible(true);
        bucket.operationList.setVisible(true);

        foreach(element in bucket.elements)
            element.setVisible(true);

        bucket.invitePlayersList.setVisible(false);
        bucket.invitePlayersSubmit.setVisible(false);
        bucket.invitePlayersBack.setVisible(false);
    }

    member = function (memberId) {
        bucket.permissions.setVisible(false);
        bucket.permissionList.setVisible(false);
        bucket.memberList.setVisible(false);
        bucket.operationList.setVisible(false);

        foreach(element in bucket.elements)
            element.setVisible(false);

        local content = [];

        foreach(permissionId, namePermission in permissions)
            content.push({id = permissionId.tointeger(), name = namePermission});

        bucket.memberTitle.setText(members[memberId]);
        bucket.memberPermissionsList.destroy();
        bucket.memberPermissionsList = MyGUI.DropDown(anx(Resolution.x/2 - 350) + anx(40), any(Resolution.y/2 - 300) + any(70), anx(600), any(60), "Uprawnienia", content, "Wybierz uprawnienia...", memberPermissions[memberId], true);
        bucket.memberPermissionsList.setVisible(true);
        bucket.memberSubmit.setVisible(true);
        bucket.memberSubmit.attribute = memberId;
        bucket.memberRemove.setVisible(true);
        bucket.memberRemove.attribute = memberId;
        bucket.memberBack.setVisible(true);       
    }

    backMember = function() {
        bucket.permissions.setVisible(true);
        bucket.permissionList.setVisible(true);
        bucket.memberList.setVisible(true);
        bucket.operationList.setVisible(true);

        foreach(element in bucket.elements)
            element.setVisible(true);

        bucket.memberPermissionsList.setVisible(false);
        bucket.memberTitle.setVisible(false);
        bucket.memberSubmit.setVisible(false);
        bucket.memberRemove.setVisible(false);
        bucket.memberBack.setVisible(false);        
    }
}

addEventHandler("onInit", FractionMenu.init.bindenv(FractionMenu));

Bind.addKey(KEY_ESCAPE, FractionMenu.hide.bindenv(FractionMenu), PlayerGUI.Fraction)
addEventHandler("onForceCloseGUI", FractionMenu.onForceCloseGUI.bindenv(FractionMenu));