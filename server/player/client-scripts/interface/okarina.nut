local sounds = [
    "OCARINA_INSERT01.WAV",
    "OCARINA_INSERT02.WAV",
    "OCARINA_INSERT03.WAV",
    "OCARINA_INSERT04.WAV",
    "OCARINA_INSERT05.WAV",
    "OCARINA_INSERT06.WAV",
    "OCARINA_INSERT07.WAV",
    "OCARINA_INSERT08.WAV",
    "OCARINA_INSERT09.WAV",
    "OCARINA_INSERT10.WAV",
    "OCARINA_INSERT11.WAV",
    "OCARINA_INSERT12.WAV",
];

local pck = getTickCount();

PlayerOkarina <- {
    bucket = null
    setLoop = false,
    setVolume = 5,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(100), "SG_BOX.TGA", null, false)
        result.buttonExit <- GUI.Button(anx(400), any(120), anx(200), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);

        result.draw <- GUI.Draw(anx(50), any(20), "1 - Z to klawisze aby odegra� d�wi�k.\nKwadratowe nawiasy, aby zmieni� tryby.", result.window);
        result.drawTwo <- GUI.Draw(anx(50), any(60), "G�o�no��: 5", result.window);

        result.buttonExit.bind(EventType.Click, function(element) {
            PlayerOkarina.hide();
        });

        return result;
    }

    playSound = function(playerId, soundId, looping, volume) {
        local obj = Sound3d(sounds[soundId]);
        obj.volume = (volume * 1.0).tofloat() * 0.1;
        obj.setTargetPlayer(playerId);
        obj.looping = false;
        obj.radius = 1500;
        obj.play();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Okarina;

        playAni(heroId, "T_FLUTENPC_S0_2_S1");
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onKey = function(key) {
        if(ActiveGui != PlayerGUI.Okarina)
            return;

        if(pck > getTickCount())
            return;

        pck = getTickCount() + 50;
        switch(key) {
            case KEY_1:
                PlayerPacket().playOkarina(0, setLoop, setVolume);
            break;
            case KEY_2:
                PlayerPacket().playOkarina(1, setLoop, setVolume);
            break;
            case KEY_3:
                PlayerPacket().playOkarina(2, setLoop, setVolume);
            break;
            case KEY_4:
                PlayerPacket().playOkarina(3, setLoop, setVolume);
            break;
            case KEY_5:
                PlayerPacket().playOkarina(4, setLoop, setVolume);
            break;
            case KEY_6:
                PlayerPacket().playOkarina(5, setLoop, setVolume);
            break;
            case KEY_7:
                PlayerPacket().playOkarina(6, setLoop, setVolume);
            break;
            case KEY_8:
                PlayerPacket().playOkarina(7, setLoop, setVolume);
            break;
            case KEY_9:
                PlayerPacket().playOkarina(8, setLoop, setVolume);
            break;
            case KEY_0:
                PlayerPacket().playOkarina(9, setLoop, setVolume);
            break;
            case KEY_MINUS:
                PlayerPacket().playOkarina(10, setLoop, setVolume);
            break;
            case KEY_EQUALS:
                PlayerPacket().playOkarina(11, setLoop, setVolume);
            break;					
            case KEY_LBRACKET:
                setVolume = setVolume - 1;
                if(setVolume < 1)
                    setVolume = 1;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
            case KEY_RBRACKET:
                setVolume = setVolume + 1;
                if(setVolume > 10)
                    setVolume = 10;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
        }
    }
}

addEventHandler("onKey", PlayerOkarina.onKey.bindenv(PlayerOkarina));

Bind.addKey(KEY_ESCAPE, PlayerOkarina.hide.bindenv(PlayerOkarina), PlayerGUI.Okarina)