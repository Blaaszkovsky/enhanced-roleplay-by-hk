
OtherPlayerDescription <- {
    bucket = null

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 - 300), anx(600), any(600), "SG_BOX.TGA", null, false)
        result.draw <- Draw(anx(Resolution.x/2 - 270), any(Resolution.y/2 - 270), "");

        return result;
    }

    force = function() {
        local focusId = getPlayerFocus();

        if (focusId > getMaxSlots())
            return;

        if(focusId == -1 || getPlayerWeaponMode(heroId) != 0)
            return;

        if(isInteractionPossibleWithPlayer(focusId) == false)
            return;

        PlayerPacket().sendPlayerDescription(focusId);
    }

    show = function(data) {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        bucket.draw.text = String.parseArray(data, "\n");
        bucket.draw.visible = true;

        if(getSetting("OtherCustomFont")) {
            bucket.draw.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.draw.setScale(0.45, 0.45);
        }

        BaseGUI.show();
        ActiveGui = PlayerGUI.Description;
    }

    hide = function() {
        bucket.window.setVisible(false);
        bucket.draw.visible = false;

        BaseGUI.hide();
        ActiveGui = null;
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Description)
            hide();
    }
}

Bind.addKey(KEY_ESCAPE, OtherPlayerDescription.hide.bindenv(OtherPlayerDescription), PlayerGUI.Description)
Bind.addKey("DESCRIPTION", OtherPlayerDescription.hide.bindenv(OtherPlayerDescription), PlayerGUI.Description)
Bind.addKey("DESCRIPTION", OtherPlayerDescription.force.bindenv(OtherPlayerDescription))

addEventHandler("onForceCloseGUI", OtherPlayerDescription.onForceCloseGUI.bindenv(OtherPlayerDescription));