local sounds = [
    "CONGA_INSERT01.WAV",
    "CONGA_INSERT02.WAV",
    "CONGA_INSERT03.WAV",
    "CONGA_INSERT04.WAV",
    "CONGA_INSERT05.WAV",
    "CONGA_INSERT06.WAV",
];

local pck = getTickCount();

PlayerConga <- {
    bucket = null
    setLoop = false,
    setVolume = 5,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(100), "SG_BOX.TGA", null, false)
        result.buttonExit <- GUI.Button(anx(400), any(120), anx(200), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);

        result.draw <- GUI.Draw(anx(50), any(20), "1 - Z to klawisze aby odegra� d�wi�k.\nKwadratowe nawiasy, aby zmieni� tryby.", result.window);
        result.drawTwo <- GUI.Draw(anx(50), any(60), "G�o�no��: 5", result.window);

        result.buttonExit.bind(EventType.Click, function(element) {
            PlayerConga.hide();
        });

        return result;
    }

    playSound = function(playerId, soundId, looping, volume) {
        local obj = Sound3d(sounds[soundId]);
        obj.volume = (volume * 1.0).tofloat() * 0.1;
        obj.setTargetPlayer(playerId);
        obj.looping = false;
        obj.radius = 1500;
        obj.play();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Conga;

        playAni(heroId, "T_PAUKE_STAND_2_S0");
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onKey = function(key) {
        if(ActiveGui != PlayerGUI.Conga)
            return;

        if(pck > getTickCount())
            return;

        pck = getTickCount() + 50;
        switch(key) {
            case KEY_1:
                PlayerPacket().playConga(0, setLoop, setVolume);
            break;
            case KEY_2:
                PlayerPacket().playConga(1, setLoop, setVolume);
            break;
            case KEY_3:
                PlayerPacket().playConga(2, setLoop, setVolume);
            break;
            case KEY_4:
                PlayerPacket().playConga(3, setLoop, setVolume);
            break;
            case KEY_5:
                PlayerPacket().playConga(4, setLoop, setVolume);
            break;
            case KEY_6:
                PlayerPacket().playConga(5, setLoop, setVolume);
            break;				
            case KEY_LBRACKET:
                setVolume = setVolume - 1;
                if(setVolume < 1)
                    setVolume = 1;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
            case KEY_RBRACKET:
                setVolume = setVolume + 1;
                if(setVolume > 10)
                    setVolume = 10;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
        }
    }
}

addEventHandler("onKey", PlayerConga.onKey.bindenv(PlayerConga));

Bind.addKey(KEY_ESCAPE, PlayerConga.hide.bindenv(PlayerConga), PlayerGUI.Conga)