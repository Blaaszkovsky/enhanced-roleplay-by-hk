local lastRender = getTickCount();

Inventory <- {
    bucket = null,
    bucketEq = null,
    openedObjInstance = null,
    openedObjId = null,

    slots = [],
    slide = 0,

    activeRow = -1,
    activeColumn = -1,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x - 450), any(Resolution.y/2 - 300), anx(430), any(600), "SG_EQ.TGA", null, false)
        result.weight <- GUI.Draw(anx(130), any(530), "", result.window);
        result.ores <- GUI.Draw(anx(130), any(560), "", result.window);
        result.drawInfo <- GUI.Draw(anx(30), any(640), "", result.window);
        result.draws <- [];

        return result;
    }

    renderQuote = function(text) {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        if(getSetting("OtherCustomFont")) {
            bucket.drawInfo.setFont("FONT_OLD_20_WHITE_HI.TGA")
            bucket.drawInfo.setScale(0.4,0.4);
        }

        bucket.drawInfo.setColor(160, 150, 140);
        bucket.drawInfo.setText(text);
    }

    init = function()
    {
        bucket = prepareWindow();

        if(getSetting("OtherCustomFont")) {
            bucket.weight.setFont("FONT_OLD_20_WHITE_HI.TGA")
            bucket.weight.setScale(0.6,0.6);

            bucket.ores.setFont("FONT_OLD_20_WHITE_HI.TGA")
            bucket.ores.setScale(0.6,0.6);
        }

        bucket.weight.setColor(160, 150, 140);
        bucket.ores.setColor(160, 150, 140);

        for(local y = 1; y <= Config["InventoryRaws"]; y ++)
            for(local x = 1; x <= Config["InventoryColumns"]; x ++)
                slots.append(InventorySlot(-1,x,y));

        bucket.drawInfo.setPosition(anx(Resolution.x/2 - 200), any(Resolution.y - 200));
    }

    refresh = function()
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        bucketEq = prepareInventory();
        bucket.window.setVisible(true);
        bucket.weight.setText("Waga: "+getInventoryWeight()+"/"+getMaxInventoryWeight());
        bucket.ores.setText("Bry�ki: " + _hasItem(heroId, Items.id("ITMI_NUGGET")));

        foreach(slot in slots)
            slot.setVisible(true);

        update();
    }

    prepareInventory = function()
    {
        local returnTab = [];
        local eq = Hero.inventory;

        foreach(itemId in eq.ids)
        {
            local item = getItem(itemId);

            if(item == null)
                continue;

            local itemObj = item.getScheme();

            returnTab.append( {
                itemId = itemId,
                instance = item.instance,
                flag = itemObj.flag,
                amount = item.amount,
            })
        }

        returnTab.sort(function(a, b) {
            if (a.flag > b.flag) return 1;
            if (a.flag < b.flag) return -1;
            return 0;
        });

        return returnTab;
    }

    show = function()
    {
        slide = 0

        activeRow = 0
        activeColumn = 0

        BaseGUI.show();
        ActiveGui = PlayerGUI.Inventory;

        setPlayerWeaponMode(heroId, 0);
        bucketEq = prepareInventory();
        bucket.window.setVisible(true);
        bucket.weight.setText("Waga: "+getInventoryWeight()+"/"+getMaxInventoryWeight());
        bucket.ores.setText("Z�oto: " + _hasItem(heroId, Items.id("ITMI_OLDCOIN")));

        foreach(slot in slots)
            slot.setVisible(true);

        Camera.setBeforePlayer(heroId, 300);

        foreach(_item in Hero.inventory.getItems()) {
            if(_hasItem(heroId, Items.id(_item.instance)) == 0) {
                giveItem(heroId, Items.id(_item.instance), _item.amount);
            }
        }

        update();
    }

    hide = function()
    {
        BaseGUI.hide();
        ActiveGui = null;

        bucket.window.setVisible(false);
        bucket.draws.clear();

        foreach(slot in slots)
            slot.setVisible(false);
    }

    update = function()
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        if(bucketEq == null)
            return;

        local index = 0;
        for(local y = 1; y <= Config["InventoryRaws"]; y ++) {
            for(local x = 1; x <= Config["InventoryColumns"]; x ++) {
                local item = index + (slide * Config["InventoryColumns"])
                item = bucketEq.len() > item ? bucketEq[item] : null;
                slots[index].update(item);

                if(activeColumn == (x - 1) && activeRow == (y - 1))
                    slots[index].activeSlot();

                index = index + 1;
            }
        }
    }

    onChangeEquippedItems = function() {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        update();
    }

    activeSlotDown = function () {
        foreach(slot in slots)
        {
            if((slot.x-1) == activeColumn && (slot.y-1) == activeRow)
            {
                slot.unactiveSlot();
                break;
            }
        }
    }

    use = function ()
    {
        if(lastRender > getTickCount()) {
            renderQuote("Poczekaj chwil..")
            return;
        }

        lastRender = getTickCount() + 500;

        local itemObj = getItem(openedObjId);
        if(itemObj == null)
            return;

        local itemObjScheme = itemObj.getScheme()

        if(openedObjInstance == "ITMI_LUTE") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerLute.show();
            return;
        }
		
        if(openedObjInstance == "ITMI_LUTE_PRISCILLA") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerLute.show();
            return;
        }

        if(openedObjInstance == "ITMI_LUTE_NORD") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerLute.show();
            return;
        }		

        if(openedObjInstance == "ITMI_KM_FLUTE") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerFlute.show();
            return;
        }

        if(openedObjInstance == "ITMI_CONGA") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerConga.show();
            return;
        }
		
        if(openedObjInstance == "ITMI_OCARINA") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerOkarina.show();
            return;
        }

        if(openedObjInstance == "ITMI_HARP") {
            useItemToState(heroId, Items.id(openedObjInstance), 1);
            hide();
            PlayerHarp.show();
            return;
        }		
		
        switch(itemObjScheme.flag)
        {
            case ITEMCAT_HERB: case ITEMCAT_FOOD: case ITEMCAT_POTION:
                useItem(heroId, Items.id(openedObjInstance));

                if(itemObjScheme.flag == ITEMCAT_FOOD)
                {
                    foreach(r in itemObjScheme.restore)
                    {
                        if(r.type == PlayerAttributes.Hunger && r.value > 0 && Hero.hunger == 100)
                        {
                            playAni(heroId,"S_KOTZKUEBEL_S1");
                        }
                    }
                }
            break;
            case ITEMCAT_USABLE:
                useItemToState(heroId, Items.id(openedObjInstance), 1);
                hide();
                return;
            break;

            case ITEMCAT_OTHER:
                return;
            break;
        }

        if(openedObjInstance != null) {
            PlayerPacket().itemUse(openedObjId);
            renderQuote("Nastpuje akcja uycia przedmiotu..");
        }

        setTimer(function() {
            Inventory.bucketEq = Inventory.prepareInventory();
            Inventory.update();
        }, 100, 3);
    }

    threwOne = function()
    {
        if(lastRender > getTickCount()) {
            renderQuote("Poczekaj chwil..")
            return;
        }

        lastRender = getTickCount() + 1500;

        if(openedObjId != null)
            PlayerPacket().throwItem(openedObjId, 1);

        playAni(heroId, "T_IDROP_2_STAND");

        setTimer(function() {
            Inventory.bucketEq = Inventory.prepareInventory();
            Inventory.update();
        }, 100, 3);
    }

    threw = function()
    {
        if(lastRender > getTickCount()) {
            renderQuote("Poczekaj chwil..")
            return;
        }

        lastRender = getTickCount() + 1500;

        if(openedObjId != null) {
            local item = getItem(openedObjId);
            PlayerPacket().throwItem(openedObjId, item.amount);
        }

        playAni(heroId, "T_IDROP_2_STAND");
        renderQuote("Nastpuje akcja wyrzucenia przedmiotu..");
    }

    down = function ()
    {
        activeSlotDown();

        activeRow = activeRow + 1;

        if(activeRow > (Config["InventoryRaws"] - 1)) {
            activeRow = Config["InventoryRaws"] - 1;
            slide = slide + 1;
        }

        update();
    }

    up = function ()
    {
        activeSlotDown();

        activeRow = activeRow - 1;

        if(activeRow < 0) {
            activeRow = 0;

            if(slide > 0)
                slide = slide - 1;
        }

        update();
    }

    left = function ()
    {
        activeSlotDown();

        activeColumn = activeColumn - 1;

        if(activeColumn <= 0)
            activeColumn = 0;

        update();
    }

    right = function ()
    {
        activeSlotDown();

        activeColumn = activeColumn + 1;

        if(activeColumn >= (Config["InventoryColumns"] - 1))
            activeColumn = Config["InventoryColumns"] - 1;

        update();
    }

    showItemDescription = function (itemObj)
    {
        openedObjInstance = itemObj.instance;
        openedObjId = itemObj.id;
        bucket.draws.clear();

        local pos = { x = anx(60), y = any(Resolution.y/2 - 300) };
        local drawTitle = Draw(pos.x + anx(30), pos.y + any(40), itemObj.name);
        if(getSetting("OtherCustomFont")) {
            drawTitle.font = "FONT_OLD_20_WHITE_HI.TGA";
            drawTitle.setScale(0.7,0.7);
        }
        drawTitle.setColor(230, 220, 210);
        drawTitle.visible = true;

        local drawDesc = Draw(pos.x + anx(30), pos.y + any(90), itemObj.getFullStringDescription()+"\nC - Wyrzu�(1x)\nX - Wyrzu�\nE - U�yj");
        if(getSetting("OtherCustomFont")) {
            drawDesc.font = "FONT_OLD_20_WHITE_HI.TGA";
            drawDesc.setScale(0.5,0.5);
        }
        drawDesc.setColor(210, 200, 190);
        drawDesc.visible = true;

        bucket.draws.append(drawDesc);
        bucket.draws.append(drawTitle);
    }

    hideItemDescription = function()
    {
        openedObjInstance = null;
        bucket.draws.clear();
    }

    onClick = function(element)
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        activeSlotDown();

        foreach(slot in slots)
        {
            if(slot.background == element)
            {
                activeColumn = slot.x - 1;
                activeRow = slot.y - 1;

                update();

                use();
                break;
            }
        }
    }

    onMouseUp = function(element, button)
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        if(button != MOUSE_RMB)
            return;

        activeSlotDown();

        foreach(slot in slots)
        {
            if(slot.background == element)
            {
                activeColumn = slot.x - 1;
                activeRow = slot.y - 1;

                update();
                break;
            }
        }

        use();
    }

    onMouseIn = function(element)
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        activeSlotDown();

        foreach(slot in slots)
        {
            if(slot.background.getPositionPx().x == element.getPositionPx().x && slot.background.getPositionPx().y == element.getPositionPx().y)
            {
                activeColumn = slot.x - 1;
                activeRow = slot.y - 1;
                activeSlotDown()

                update();
                break;
            }
        }
    }

    onMouseWheel = function(value)
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        activeSlotDown();

        activeRow = activeRow - value;

        if (value == 1)
        {
            if (activeRow < 0)
            {
                activeRow = 0

                if (slide > 0)
                    slide = slide - 1
            }
        }
        else
        {
            if(activeRow > (Config["InventoryRaws"] - 1)) {
                activeRow = Config["InventoryRaws"] - 1;
                slide = slide + 1;
            }
        }

        update();
    }
}

class InventorySlot
{
    background = null;
    highlight = null;
    resistanceback = null;

    render = null;
    itemId = -1
    draw = null;
    instance = null;

    x = -1
    y = -1

    constructor(itemId,x,y)
    {
        this.itemId = itemId;
        this.x = x;
        this.y = y;

        local startPos = {x = anx(Resolution.x - 390), y = any(Resolution.y/2 - 250) };
        background = GUI.Texture(startPos.x + anx(80) * (x - 1), startPos.y + any(80) * (y - 1), anx(70), any(70), "SG_BOX.TGA");
        background.setAlpha(200);

        highlight = GUI.Texture(startPos.x + anx(80) * (x - 1) - anx(3), startPos.y + any(80) * (y - 1) - any(3), anx(76), any(76), "INV_SLOT_HIGHLIGHTED.TGA");
        highlight.setColor(190,180,170);
        highlight.setAlpha(150);

        resistanceback = GUI.Texture(startPos.x + anx(80) * (x - 1), startPos.y + any(80) * (y - 1), anx(70), any(70), "WHITE.TGA");
        resistanceback.setColor(190,180,170);
        resistanceback.setAlpha(0);

        render = null;
        draw = null;
        instance = null;
    }

    function update(obj)
    {
        if(ActiveGui != PlayerGUI.Inventory)
            return;

        local pos = background.getPosition();
        local size = background.getSize();

        if(obj != null)
        {
            itemId = obj.itemId;
            render = ItemRender(pos.x,pos.y,size.width, size.height, obj.instance);
            render.visible = true;
            local item = getItem(itemId);

            if(item.equipped) {
                background.setColor(255,88,55);
                background.setAlpha(255);
            }else{
                background.setColor(255, 255, 255);
                background.setAlpha(100);
            }

            local maxResistance = item.getScheme().resistance;
            if(maxResistance != -1) {
                local percent = abs((item.resistance * 100)/maxResistance);
                if(percent > 75) {
                    resistanceback.setColor(0, 60, 50);
                    resistanceback.setAlpha(15);
                }else if(percent > 35) {
                    resistanceback.setColor(65, 55, 0);
                    resistanceback.setAlpha(15);
                }else {
                    resistanceback.setColor(255, 0, 0);
                    resistanceback.setAlpha(15);
                }
            }else{
                resistanceback.setAlpha(0);
            }

            render.top();
            instance = obj.instance;

            draw = Draw(pos.x + size.width - anx(30), pos.y + size.height - any(30), obj.amount);
            if(getSetting("OtherCustomFont")) {
                draw.font = "FONT_OLD_20_WHITE_HI.TGA";
                draw.setScale(0.5, 0.5);
            }
            draw.setColor(160,150,140);
            draw.visible = true;
            draw.top();
        }else{
            itemId = -1;
            background.setColor(255, 255, 255);
            instance = null;
            render = null;
            draw = null;
        }
    }

    function activeSlot()
    {
        if(instance != null)
            Inventory.showItemDescription(getItem(itemId));
        else
            Inventory.hideItemDescription();

        highlight.setVisible(true);
        background.top();
    }

    function unactiveSlot()
    {
        highlight.setVisible(false);
        background.top();
    }

    function setVisible(toggle)
    {
        background.setVisible(toggle);
        resistanceback.setVisible(toggle);
        highlight.setVisible(false);

        render = null;
        draw = null;
    }
}

addEventHandler("onInit", Inventory.init.bindenv(Inventory));
addEventHandler("onChangeEquippedItems", Inventory.onChangeEquippedItems.bindenv(Inventory));
addEventHandler("GUI.onClick", Inventory.onClick.bindenv(Inventory));
addEventHandler("GUI.onMouseUp", Inventory.onMouseUp.bindenv(Inventory));
addEventHandler("GUI.onMouseIn", Inventory.onMouseIn.bindenv(Inventory));
addEventHandler("onMouseWheel", Inventory.onMouseWheel.bindenv(Inventory));

Bind.addKey("EQUIPMENT", Inventory.show.bindenv(Inventory))
Bind.addKey(KEY_TAB, Inventory.show.bindenv(Inventory))
Bind.addKey(KEY_TAB, Inventory.hide.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey("EQUIPMENT", Inventory.hide.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_ESCAPE, Inventory.hide.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_X, Inventory.threw.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_C, Inventory.threwOne.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_E, Inventory.use.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_DOWN, Inventory.down.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_UP, Inventory.up.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_LEFT, Inventory.left.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_RIGHT, Inventory.right.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_S, Inventory.down.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_W, Inventory.up.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_A, Inventory.left.bindenv(Inventory), PlayerGUI.Inventory)
Bind.addKey(KEY_D, Inventory.right.bindenv(Inventory), PlayerGUI.Inventory)
