local lock = false;

enum CharacterAddOption
{
    Body,
    Skin,
    Head,
    Face,
    Size,
    Fatness,
    Walking
}

local pojebiemnieprzytymkodzie = null
local models = {}
local walks = {}

local ROWS_AMOUNT = 8
local CELLS_AMOUNT = 2

CharacterAdd <-
{
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        // result.windowStorySelector <- GUI.Window(anx(Resolution.x/2 - 210), any(Resolution.y/2 - 300), anx(420), any(530), "SG_BOX.TGA", null, false);
        // result.leftStorySelector <- GUI.Button(anx(-20), any(250), anx(40), any(40), "SG_L.TGA", "", result.windowStorySelector);
        // result.rightStorySelector <- GUI.Button(anx(400), any(250), anx(40), any(40), "SG_R.TGA", "", result.windowStorySelector);
        // result.acceptStorySelector <- GUI.Button(anx(110), any(480), anx(200), any(60), "SG_BUTTON.TGA", "Wybierz", result.windowStorySelector);
        // result.windowStorySelectorDraws <- [];

        // result.leftStorySelector.bind(EventType.Click, function(element) {CharacterAdd.showStorySelector(element.attribute);});
        // result.rightStorySelector.bind(EventType.Click, function(element) {CharacterAdd.showStorySelector(element.attribute);});
        // result.acceptStorySelector.bind(EventType.Click, function(element) {CharacterAdd.selectStory(result.windowStorySelectorDraws[2]); CharacterAdd.hide();});

        result.window <- GUI.Window(anx(40), any(Resolution.y/2 - 300), anx(420), any(560), "SG_BOX.TGA", null, false);

        result.nameInput <- GUI.Input(anx(60), any(40), anx(300), any(55), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa u�ytkownika", 2, result.window);

        result.bodyButton <- GUI.Button(anx(10), any(120), anx(200), any(60), "SG_BUTTON.TGA", "P�e�", result.window);
        result.skinButton <- GUI.Button(anx(210), any(120), anx(200), any(60), "SG_BUTTON.TGA", "Cia�o", result.window);
        result.headButton <- GUI.Button(anx(10), any(200), anx(200), any(60), "SG_BUTTON.TGA", "G�owa", result.window);
        result.faceButton <- GUI.Button(anx(210), any(200), anx(200), any(60), "SG_BUTTON.TGA", "Twarz", result.window);
        result.sizeButton <- GUI.Button(anx(10), any(280), anx(0), any(0), "SG_BUTTON.TGA", "", result.window);
        result.fatnessButton <- GUI.Button(anx(210), any(280), anx(200), any(60), "SG_BUTTON.TGA", "Grubo��", result.window);
        result.walkingButton <- GUI.Button(anx(10), any(280), anx(200), any(60), "SG_BUTTON.TGA", "Chodzenie", result.window);

        result.scrollBar <- GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, result.window);
        result.scrollBar.setMaximum(360);
        result.scrollBar.setSize(anx(500), any(30));
        result.scrollBar.setPosition(anx(Resolution.x/2 - 250), any(Resolution.y - 200));

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            setPlayerAngle(heroId, value);
        }, PlayerGUI.CharacterAdd);

        result.bodyButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Body);
        });

        result.skinButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Skin);
        });

        result.headButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Head);
        });

        result.faceButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Face);
        });

        result.sizeButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Size);
        });

        result.fatnessButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Fatness);
        });

        result.walkingButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Walking);
        });

        result.backButton <- GUI.Button(anx(10), any(440), anx(200), any(80), "SG_BUTTON.TGA", "Wr��", result.window);
        result.createButton <- GUI.Button(anx(210), any(440), anx(200), any(80), "SG_BUTTON.TGA", "Stw�rz", result.window);

        result.windowOperation <- GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "SG_BOX.TGA", null, false);
        result.operations <- [];
        result.pizdaprzemka <- []

        result.backButton.bind(EventType.Click, function(element) {
            CharacterAdd.hide();
            showCharacterList();
        });

        result.createButton.bind(EventType.Click, function(element) {
            if(CharacterAdd.bucket.nameInput.getText().len() > 2) {
                PlayerPacket().addCharacter(CharacterAdd.bucket.nameInput.getText());
            }
        });

        return result;
    },

    chooseOperation = function (typeId)
    {
        foreach(element in bucket.operations)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);

            element.destroy();
        }

        lock = false;
        stopAni(heroId);
        pojebiemnieprzytymkodzie = typeId
        models.clear()
        walks.clear()

        bucket.operations.clear();
        bucket.pizdaprzemka.clear()
        bucket.windowOperation.destroyAll();
        bucket.windowOperation = GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "SG_BOX.TGA", null, false);

        switch(typeId)
        {
            case CharacterAddOption.Body:
                local maleButton = GUI.Button(anx(50), any(200), anx(300), any(80), "SG_BUTTON.TGA", "M�czyzna", bucket.windowOperation);
                local femaleButton = GUI.Button(anx(50), any(300), anx(300), any(80), "SG_BUTTON.TGA", "Kobieta", bucket.windowOperation);

                maleButton.bind(EventType.Click, function(element) {
                    CharacterAdd.changeVisual(CharacterAddOption.Body, "Hum_Body_Naked0");
                });
                femaleButton.bind(EventType.Click, function(element) {
                    CharacterAdd.changeVisual(CharacterAddOption.Body, "Hum_Body_Babe0");
                });

                bucket.operations.append(maleButton);
                bucket.operations.append(femaleButton);
            break;
            case CharacterAddOption.Skin:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "HUM_BODY_NAKED_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        CharacterAdd.changeVisual(CharacterAddOption.Skin, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxSkin"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setFile("HUM_BODY_NAKED_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.CharacterAdd);
            break;
            case CharacterAddOption.Head:
            bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, bucket.windowOperation));
            local cellsAdded = 0

            local row = 0
            local cell = 0

            foreach(model in Config["headModels"])
            {
                if (!(row in models))
                    models[row] <- []

                if(cellsAdded < (ROWS_AMOUNT) * CELLS_AMOUNT)
                {
                    local button = GUI.Button(anx(cell == 0 ? 20 : 210), any(30 + row * 60), anx(170), any(50), "SYN_BUTTON", model.name, bucket.windowOperation);
                    button.attribute = model
                    bucket.pizdaprzemka.push(button);

                    bucket.pizdaprzemka.top().bind(EventType.Click, function(element)
                    {
                          CharacterAdd.changeVisual(VisualOption.Head, element.attribute.asc);
                    })
                }

                models[row].push({model = model})

                if (cell == CELLS_AMOUNT - 1)
                {
                    ++row
                    cell = 0

                    models[row] <- []
                }
                else
                    ++cell

                ++cellsAdded
            }

            local positionPx = bucket.windowOperation.getPositionPx()
            local sizePx = bucket.windowOperation.getSizePx()

            bucket.windowOperation.setPositionPx(positionPx.x - 40, positionPx.y)
            bucket.windowOperation.setSizePx(sizePx.width + 40, sizePx.height)

            positionPx = bucket.windowOperation.getPositionPx()
            sizePx = bucket.windowOperation.getSizePx()

            bucket.operations[0].setSizePx(30, sizePx.height - 45);
            bucket.operations[0].setPositionPx(positionPx.x + sizePx.width - bucket.operations[0].getSizePx().width - 20, positionPx.y);

            if (row > ROWS_AMOUNT - 1)
                bucket.operations[0].setMaximum(row - ROWS_AMOUNT + cellsAdded % 2)
            else
                bucket.operations[0].setMaximum(0)

            bucket.operations[0].setValue(0)

            break;
            case CharacterAddOption.Face:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "Hum_Head_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        CharacterAdd.changeVisual(CharacterAddOption.Face, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxFace"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setFile("Hum_Head_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.CharacterAdd);
            break;

            case CharacterAddOption.Size:
                local midgetSizeButton = GUI.Button(anx(20), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Krasnal", bucket.windowOperation);
                local dwarfSizeButton = GUI.Button(anx(210), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Ma�y", bucket.windowOperation);
                local smallSizeButton = GUI.Button(anx(20), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Niski", bucket.windowOperation);
                local normalSizeButton = GUI.Button(anx(210), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tallSizeButton = GUI.Button(anx(20), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local giantSizeButton = GUI.Button(anx(210), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Gigant", bucket.windowOperation);

                midgetSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Midget);});
                dwarfSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Dwarf);});
                smallSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Small);});
                normalSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Normal);});
                tallSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Tall);});
                giantSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Giant);});

                bucket.operations.append(midgetSizeButton);bucket.operations.append(dwarfSizeButton);bucket.operations.append(smallSizeButton);
                bucket.operations.append(normalSizeButton);bucket.operations.append(tallSizeButton);bucket.operations.append(giantSizeButton);
            break;

            case CharacterAddOption.Fatness:
                local thinFatnessButton = GUI.Button(anx(20), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Chudzielec", bucket.windowOperation);
                local skinnyFatnessButton = GUI.Button(anx(210), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Chudy", bucket.windowOperation);
                local normalFatnessButton = GUI.Button(anx(20), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local muscularFatnessButton = GUI.Button(anx(210), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Uminiony", bucket.windowOperation);
                local thickFatnessButton = GUI.Button(anx(20), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Pulchny", bucket.windowOperation);
                local fatFatnessButton = GUI.Button(anx(210), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Tusty", bucket.windowOperation);

                thinFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Thin);});
                skinnyFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Skinny);});
                normalFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Normal);});
                muscularFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Muscular);});
                thickFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Thick);});
                fatFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Fat);});

                bucket.operations.append(thinFatnessButton);bucket.operations.append(skinnyFatnessButton);bucket.operations.append(normalFatnessButton);
                bucket.operations.append(muscularFatnessButton);bucket.operations.append(thickFatnessButton);bucket.operations.append(fatFatnessButton);
            break;

            case CharacterAddOption.Walking:
                lock = getPlayerPosition(heroId);
                playAni(heroId, "S_WALKL");

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, bucket.windowOperation));
                local cellsAdded = 0

                local row = 0
                local cell = 0

                foreach(walk in Config["WalkStyles"])
                {
                    if (!(row in walks))
                        walks[row] <- []

                    if(cellsAdded < (ROWS_AMOUNT) * CELLS_AMOUNT)
                    {
                        local button = GUI.Button(anx(cell == 0 ? 20 : 210), any(30 + row * 60), anx(170), any(50), "SYN_BUTTON", walk.name, bucket.windowOperation);
                        button.attribute = walk
                        bucket.pizdaprzemka.push(button);

                        bucket.pizdaprzemka.top().bind(EventType.Click, function(element)
                        {
                              CharacterAdd.changeVisual(VisualOption.Walking, element.attribute.idx);
                        })
                    }

                    walks[row].push({walk = walk})

                    if (cell == CELLS_AMOUNT - 1)
                    {
                        ++row
                        cell = 0

                        walks[row] <- []
                    }
                    else
                        ++cell

                    ++cellsAdded
                }

                local positionPx = bucket.windowOperation.getPositionPx()
                local sizePx = bucket.windowOperation.getSizePx()

                bucket.windowOperation.setPositionPx(positionPx.x - 40, positionPx.y)
                bucket.windowOperation.setSizePx(sizePx.width + 40, sizePx.height)

                positionPx = bucket.windowOperation.getPositionPx()
                sizePx = bucket.windowOperation.getSizePx()

                bucket.operations[0].setSizePx(30, sizePx.height - 45);
                bucket.operations[0].setPositionPx(positionPx.x + sizePx.width - bucket.operations[0].getSizePx().width - 20, positionPx.y);

                if (row > ROWS_AMOUNT - 1)
                    bucket.operations[0].setMaximum(row - ROWS_AMOUNT + cellsAdded % 2)
                else
                    bucket.operations[0].setMaximum(0)

                bucket.operations[0].setValue(0)
            break;
        }

        bucket.windowOperation.setVisible(true);
    }

    changeVisual = function(type, value)
    {
        local getVisual = getPlayerVisual(heroId);
        switch(type)
        {
            case CharacterAddOption.Body:
                setPlayerVisual(heroId, value, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
            case CharacterAddOption.Skin:
                setPlayerVisual(heroId, getVisual.bodyModel, value, getVisual.headModel, getVisual.headTxt);
            break;
            case CharacterAddOption.Head:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, value, getVisual.headTxt);
            break;
            case CharacterAddOption.Face:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, value);
            break;
            case CharacterAddOption.Size:
                setPlayerSpeech(heroId, value);
            break;
            case CharacterAddOption.Fatness:
                setPlayerFatness(heroId, value);
            break;
            case CharacterAddOption.Walking:
                setPlayerWalkingStyle(heroId, value);
            break;
        }
        getVisual = getPlayerVisual(heroId);
        PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
    }

    destroyWindow = function ()
    {
        if (bucket == null)
            return

        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox || element instanceof GUI.ScrollBar)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);

        bucket.window.destroyAll();
        bucket.windowOperation.destroyAll();
    },

    start = function ()
    {
        BaseGUI.show();
        ActiveGui = PlayerGUI.CharacterAdd;

        local getVisual = getPlayerVisual(heroId);
        setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);

        Camera.setPosition(-53073.4,1312.03,-116573);
        Camera.setRotation(10, 359.275, 0);

        bucket = prepareWindow();
        bucket.window.setVisible(true);
        bucket.scrollBar.setVisible(true);
        bucket.scrollBar.setValue(getPlayerAngle(heroId).tointeger());
    },

    // showStorySelector = function(currentId = 0)
    // {
    //     bucket.windowStorySelector.setVisible(true);
    //     bucket.window.setVisible(false);
    //     bucket.scrollBar.setVisible(false);
    //     bucket.windowOperation.setVisible(false);

    //     bucket.leftStorySelector.attribute = currentId > 0 ? (currentId - 1) : 3;
    //     bucket.rightStorySelector.attribute = currentId < 3 ? (currentId + 1) : 0;

    //     local obj = Config["PlayerScenario"][currentId];
    //     local x = anx(Resolution.x/2 - 210), y = any(Resolution.y/2 - 300);

    //     bucket.windowStorySelectorDraws = [
    //         Draw(x + anx(30), y + any(30), obj.name),
    //         Draw(x + anx(25), y + any(90), obj.description),
    //         currentId
    //     ];

    //     bucket.windowStorySelectorDraws[0].font = "FONT_OLD_20_WHITE_HI.TGA";
    //     bucket.windowStorySelectorDraws[1].font = "FONT_OLD_20_WHITE_HI.TGA";
    //     bucket.windowStorySelectorDraws[0].setColor(200, 0, 0);
    //     bucket.windowStorySelectorDraws[1].setColor(235, 215, 187);
    //     bucket.windowStorySelectorDraws[1].setScale(0.5, 0.5);
    //     bucket.windowStorySelectorDraws[0].visible = true;
    //     bucket.windowStorySelectorDraws[1].visible = true;
    // }

    selectStory = function()
    {
        local obj = Config["PlayerScenario"];
        setPlayerPosition(heroId, obj.spawn.x, obj.spawn.y, obj.spawn.z);
        setPlayerAngle(heroId, obj.spawn.angle);
    }

    hide = function ()
    {
        BaseGUI.hide();
        ActiveGui = null;

        lock = false;
        stopAni(heroId);

        destroyWindow();
        bucket = null;
    }
}

addEventHandler("onRender", function() {
    if(lock != false)
    {
        if(getPlayerAni(heroId) != "S_WALKL")
            playAni(heroId, "S_WALKL");

        setPlayerPosition(heroId, lock.x, lock.y, lock.z);
    }
})

addEventHandler("GUI.onChange", function(self)
{
	if (!ActiveGui == PlayerGUI.CharacterAdd)
		return

    if (pojebiemnieprzytymkodzie == CharacterAddOption.Head)
    {
        if(self != CharacterAdd.bucket.operations[0])
            return

        local value = self.getValue().tointeger()
        local currentCell = 0

        for(local row = value, end = value + ROWS_AMOUNT; row < end; row++)
        {
            if (!(row in models))
                break

            local _models = models[row]
            local cellsAmount = _models.len()

            for(local cellIdx = 0; cellIdx < cellsAmount; cellIdx++)
            {
                if (!CharacterAdd.bucket.pizdaprzemka[currentCell].getVisible())
                    CharacterAdd.bucket.pizdaprzemka[currentCell].setVisible(true)

                CharacterAdd.bucket.pizdaprzemka[currentCell].setText(_models[cellIdx].model.name)
                CharacterAdd.bucket.pizdaprzemka[currentCell].attribute = _models[cellIdx].model

                CharacterAdd.bucket.pizdaprzemka[currentCell].bind(EventType.Click, function(element)
                {
                      CharacterAdd.changeVisual(VisualOption.Head, element.attribute.asc);
                })

                ++currentCell
            }

            if (cellsAmount == 1)
                CharacterAdd.bucket.pizdaprzemka[currentCell].setVisible(false)
        }
    }
    else if(pojebiemnieprzytymkodzie == CharacterAddOption.Walking)
    {
        if(self != CharacterAdd.bucket.operations[0])
            return

        local value = self.getValue().tointeger()
        local currentCell = 0

        for(local row = value, end = value + ROWS_AMOUNT; row < end; row++)
        {
            if (!(row in walks))
                break

            local _walks = walks[row]
            local cellsAmount = _walks.len()

            for(local cellIdx = 0; cellIdx < cellsAmount; cellIdx++)
            {
                if (!CharacterAdd.bucket.pizdaprzemka[currentCell].getVisible())
                    CharacterAdd.bucket.pizdaprzemka[currentCell].setVisible(true)

                CharacterAdd.bucket.pizdaprzemka[currentCell].setText(_walks[cellIdx].walk.name)
                CharacterAdd.bucket.pizdaprzemka[currentCell].attribute = _walks[cellIdx].walk

                CharacterAdd.bucket.pizdaprzemka[currentCell].bind(EventType.Click, function(element)
                {
                      CharacterAdd.changeVisual(VisualOption.Walking, element.attribute.idx);
                })

                ++currentCell
            }

            if (cellsAmount == 1)
                CharacterAdd.bucket.pizdaprzemka[currentCell].setVisible(false)
        }
    }
})

addEventHandler("onPlayerLoad", function() { if( ActiveGui == PlayerGUI.CharacterAdd) CharacterAdd.selectStory(); CharacterAdd.hide(); })
