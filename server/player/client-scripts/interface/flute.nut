local sounds = [
    "FLUTE_SONG_01.WAV",
    "FLUTE_SONG_02.WAV",
    "FLUTE_SONG_03.WAV",
    "FLUTE_SONG_04.WAV",
    "FLUTE_SONG_05.WAV",
    "FLUTE_SONG_06.WAV",
    "FLUTE_SONG_07.WAV",
    "FLUTE_SONG_08.WAV",
    "FLUTE_SONG_09.WAV",
    "FLUTE_SONG_10.WAV",
    "FLUTE_SONG_11.WAV",
    "FLUTE_SONG_12.WAV",
    "FLUTE_SONG_13.WAV",
    "FLUTE_SONG_14.WAV",
    "FLUTE_SONG_15.WAV",
    "FLUTE_SONG_16.WAV",
    "FLUTE_SONG_17.WAV",
    "FLUTE_SONG_18.WAV",
    "FLUTE_SONG_19.WAV",
    "FLUTE_SONG_20.WAV",
    "FLUTE_SONG_21.WAV",
];

local pck = getTickCount();

PlayerFlute <- {
    bucket = null
    setLoop = false,
    setVolume = 5,
    playerSounds = {},

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(100), "SG_BOX.TGA", null, false)
        result.buttonExit <- GUI.Button(anx(400), any(120), anx(200), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);

        result.draw <- GUI.Draw(anx(50), any(20), "1-9 oraz Q-P - klawisze aby odegra� piosenki.\nZ,X - aby zmieni� g�o�no��.", result.window);
        result.drawTwo <- GUI.Draw(anx(50), any(60), "G�o�no��: 5", result.window);

        result.buttonExit.bind(EventType.Click, function(element) {
            PlayerFlute.hide();
        });

        return result;
    }

    playSound = function(playerId, soundId, looping, volume) {
        if(!(playerId in playerSounds)) {
            playerSounds[playerId] <- Sound3d(sounds[soundId]);
            playerSounds[playerId].volume = (volume * 1.0).tofloat() * 0.1;
            playerSounds[playerId].setTargetPlayer(playerId);
            playerSounds[playerId].looping = false;
            playerSounds[playerId].radius = 1500;

        } else {
            if(playerSounds[playerId].isPlaying()) {
                playerSounds[playerId].stop();
            }
            playerSounds[playerId].file = sounds[soundId];
        }
        playerSounds[playerId].play();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Flute;

        playAni(heroId, "T_FLUTENPC_S0_2_S1");
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onKey = function(key) {
        if(ActiveGui != PlayerGUI.Flute)
            return;

        if(pck > getTickCount())
            return;

        pck = getTickCount() + 50;
        switch(key) {
            case KEY_1:
                PlayerPacket().playFlute(0, setLoop, setVolume);
            break;
            case KEY_2:
                PlayerPacket().playFlute(1, setLoop, setVolume);
            break;
            case KEY_3:
                PlayerPacket().playFlute(2, setLoop, setVolume);
            break;
            case KEY_4:
                PlayerPacket().playFlute(3, setLoop, setVolume);
            break;
            case KEY_5:
                PlayerPacket().playFlute(4, setLoop, setVolume);
            break;
            case KEY_6:
                PlayerPacket().playFlute(5, setLoop, setVolume);
            break;
            case KEY_7:
                PlayerPacket().playFlute(6, setLoop, setVolume);
            break;
            case KEY_8:
                PlayerPacket().playFlute(7, setLoop, setVolume);
            break;
            case KEY_9:
                PlayerPacket().playFlute(8, setLoop, setVolume);
            break;
            case KEY_0:
                PlayerPacket().playFlute(9, setLoop, setVolume);
            break;
            case KEY_Q:
                PlayerPacket().playFlute(10, setLoop, setVolume);
            break;
            case KEY_W:
                PlayerPacket().playFlute(11, setLoop, setVolume);
            break;
            case KEY_E:
                PlayerPacket().playFlute(12, setLoop, setVolume);
            break;
            case KEY_R:
                PlayerPacket().playFlute(13, setLoop, setVolume);
            break;
            case KEY_T:
                PlayerPacket().playFlute(14, setLoop, setVolume);
            break;
            case KEY_Y:
                PlayerPacket().playFlute(15, setLoop, setVolume);
            break;
            case KEY_U:
                PlayerPacket().playFlute(16, setLoop, setVolume);
            break;
            case KEY_I:
                PlayerPacket().playFlute(17, setLoop, setVolume);
            break;
            case KEY_O:
                PlayerPacket().playFlute(18, setLoop, setVolume);
            break;
            case KEY_P:
                PlayerPacket().playFlute(19, setLoop, setVolume);
            break;
            case KEY_Z:
                setVolume = setVolume - 1;
                if(setVolume < 1)
                    setVolume = 1;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
            case KEY_X:
                setVolume = setVolume + 1;
                if(setVolume > 10)
                    setVolume = 10;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
        }
    }
}

addEventHandler("onKey", PlayerFlute.onKey.bindenv(PlayerFlute));

Bind.addKey(KEY_ESCAPE, PlayerFlute.hide.bindenv(PlayerFlute), PlayerGUI.Flute)