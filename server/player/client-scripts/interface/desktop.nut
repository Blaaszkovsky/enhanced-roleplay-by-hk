
PlayerDesktop <- {
    healthRound = Texture(anx(30), any(Resolution.y - 190), anx(256), any(32), "SG_BAR_BACK_02.TGA")
    healthBack = Texture(0,0,0,0, "SG_BAR_BACK_02.TGA")
    health = Texture(anx(40), any(Resolution.y - 182), anx(236), any(15), "BAR_HEALTH.TGA")

    manaRound = Texture(anx(30), any(Resolution.y - 150), anx(256), any(32), "SG_BAR_BACK_02.TGA")
    manaBack = Texture(0,0,0,0, "SG_BAR_BACK_02.TGA")
    mana = Texture(anx(40), any(Resolution.y - 142), anx(236), any(15), "BAR_MANA.TGA")

    staminaRound = Texture(anx(30), any(Resolution.y - 110), anx(256), any(32), "SG_BAR_BACK_02.TGA")
    staminaBack = Texture(0,0,0,0, "SG_BAR_BACK_02.TGA")
    stamina = Texture(anx(40), any(Resolution.y - 102), anx(236), any(15), "BAR_MISC.TGA")

    hungerRound = Texture(anx(30), any(Resolution.y - 70), anx(256), any(32), "SG_BAR_BACK_02.TGA")
    hungerBack = Texture(0,0,0,0, "SG_BAR_BACK_02.TGA")
    hunger = Texture(anx(40), any(Resolution.y - 62), anx(236), any(15), "BAR_MISC.TGA")

    timeDraw = Draw(anx(Resolution.x - 200), any(Resolution.y - 40), ""),
    timeDrawOOC = Draw(anx(Resolution.x - 200), any(Resolution.y - 20), ""),
    nextStaminaCheck = null,
    lastStaminaCheck = null,

    invisibleDraw = Draw(anx(Resolution.x - 250), any(Resolution.y - 120), "Jestem niewidzialny"),

    open = function()
    {
        healthRound.visible = true;
        health.visible = true;
        healthBack.visible = true;

        manaRound.visible = true;
        mana.visible = true;
        manaBack.visible = true;

        staminaRound.visible = true;
        stamina.visible = true;
        staminaBack.visible = true;

        hungerRound.visible = true;
        hunger.visible = true;
        hungerBack.visible = true;

        BuffInterface.show();

        if(getSetting("OtherCustomFont")) {
            timeDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDraw.setScale(0.45,0.45);
        }

        timeDraw.setColor(160, 150, 140);
        timeDraw.visible = true;
        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);
        timeDraw.setPosition(8129 - timeDraw.width - 50, 7800);

        if(getSetting("OtherCustomFont")) {
            timeDrawOOC.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDrawOOC.setScale(0.45,0.45);
        }

        timeDrawOOC.setColor(160, 150, 140);
        timeDrawOOC.visible = true;
        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);
        timeDrawOOC.setPosition(8129 - timeDrawOOC.width - 50, 8000);

        invisibleDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
        invisibleDraw.setScale(0.4,0.4);
        invisibleDraw.setColor(255, 0, 0);
    }

    hide = function()
    {
        healthRound.visible = false;
        healthBack.visible = false;
        health.visible = false;

        manaRound.visible = false;
        mana.visible = false;
        manaBack.visible = false;

        staminaRound.visible = false;
        stamina.visible = false;
        staminaBack.visible = false;

        hungerRound.visible = false;
        hungerBack.visible = false;
        hunger.visible = false;

        timeDraw.visible = false;
        timeDrawOOC.visible = false;

        BuffInterface.hide();
    }

    onSettingsChange = function()
    {
        local opened = false;

        if(healthRound.visible)
            opened = true;

        timeDraw = Draw(anx(Resolution.x - 200), any(Resolution.y - 40), "");
        timeDrawOOC = Draw(anx(Resolution.x - 200), any(Resolution.y - 20), "");

        if(getSetting("OtherCustomFont")) {
            timeDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDraw.setScale(0.45,0.45);
        }

        timeDraw.setColor(160, 150, 140);
        timeDraw.visible = true;
        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);
        timeDraw.setPosition(8129 - timeDraw.width - 50, 7800);

        if(getSetting("OtherCustomFont")) {
            timeDrawOOC.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDrawOOC.setScale(0.45,0.45);
        }

        timeDrawOOC.setColor(160, 150, 140);
        timeDrawOOC.visible = true;
        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);
        timeDrawOOC.setPosition(8129 - timeDrawOOC.width - 50, 8000);

        hunger.setColor(15, 118, 15)

        if(opened)
            open();
    }

    render = function()
    {
        try {
            local hpPercent = abs((getPlayerHealth(heroId) * 100)/getPlayerMaxHealth(heroId));
            PlayerDesktop.health.setRect(0,0,anx((238 * hpPercent)/100),any(15));
            PlayerDesktop.health.setSize(anx((238 * hpPercent)/100),any(15));

            hpPercent = abs((getPlayerMana(heroId) * 100)/getPlayerMaxMana(heroId));
            PlayerDesktop.mana.setRect(0,0,anx((238 * hpPercent)/100),any(15));
            PlayerDesktop.mana.setSize(anx((238 * hpPercent)/100),any(15));

            hpPercent = abs((getPlayerStamina(heroId) * 100)/100.0);
            PlayerDesktop.stamina.setRect(0,0,anx((238 * hpPercent)/100),any(15));
            PlayerDesktop.stamina.setSize(anx((238 * hpPercent)/100),any(15));

            hpPercent = abs((getPlayerHunger(heroId) * 100)/100.0);
            PlayerDesktop.hunger.setRect(0,0,anx((238 * hpPercent)/100),any(15));
            PlayerDesktop.hunger.setSize(anx((238 * hpPercent)/100),any(15));
        }catch(error) {}

        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);

        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);

        local visibility = false
        if (getPlayerInvisible(heroId))
        {
            if(ActiveGui != PlayerGUI.Free)
                visibility = true
        }

        invisibleDraw.visible = visibility;

        if(nextStaminaCheck != null) {
            if(nextStaminaCheck == true) {
                if(isKeyPressed(getAccountKeyBind("SPRINT")))
                    return;

                PlayerPacket().useSprint();
                nextStaminaCheck = null;
            }else if(nextStaminaCheck < getTickCount()) {
                if(isKeyPressed(getAccountKeyBind("SPRINT")) == false)
                    return;

                PlayerPacket().useSprint();
                nextStaminaCheck = true;
            }
        }
    }

    onKey = function(key) {
        if(healthRound.visible == false)
            return;

        if(getTickCount() > lastStaminaCheck) {
            if(key == getAccountKeyBind("SPRINT")) {
                if(nextStaminaCheck == null) {
                    nextStaminaCheck = getTickCount() + 500;
                    lastStaminaCheck = getTickCount() + 2000;
                }
            }
        }
    }
}

addEventHandler("onOpenGUI", function() {
    PlayerDesktop.hide();
})

addEventHandler("onCloseGUI", function() {
    PlayerDesktop.open();
})

addEventHandler("onRender", function() {
    PlayerDesktop.render();
})

addEventHandler("onKey", PlayerDesktop.onKey.bindenv(PlayerDesktop));
addEventHandler("onSettingsChange", PlayerDesktop.onSettingsChange.bindenv(PlayerDesktop));
