
local acceptButton = GUI.Button(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 80), anx(140), any(60), "SG_BUTTON.TGA", "Akceptuj");
local rejectButton = GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 80), anx(140), any(60), "SG_BUTTON.TGA", "Rezygnuj");

acceptButton.bind(EventType.Click, function(element) {
    PlayerAsk.accept();
});

rejectButton.bind(EventType.Click, function(element) {
    PlayerAsk.reject();
});

PlayerAsk <- {
    askPlayerId = -1,
    askType = -1,
    askValue = false,

    window = null,
    timer = null,

    prepareWindow = function() {
        local result = {};
        if(askValue == false) {
            result.background <- Texture(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 100), anx(400), any(100), "SG_BOX.TGA");
            result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 60), "Oczekiwanie na reakcj�..");

            result.background.visible = true;
            result.draw.visible = true;
        } else {
            result.background <- Texture(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 100), anx(400), any(100), "SG_BOX.TGA");

            switch(askType) {
                case PlayerAskType.Trade:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " proponuje handel");
                break;
                case PlayerAskType.Trap:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce ci� zwi�za�");
                break;
                case PlayerAskType.UnTrap:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce ci� rozwi�za�");
                break;
                case PlayerAskType.Invite:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce �eby� do��czy� do jego frakcji.");
                break;
                case PlayerAskType.Heal:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce ci� uleczy�.");
                break;
                case PlayerAskType.Search:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce ci� przeszuka�.");
                break;
                case PlayerAskType.Take:
                    result.draw <- Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), getPlayerRealName(PlayerAsk.askPlayerId) + " chce ci� podnie��.");
                break;
            }

            result.background.visible = true;
            rejectButton.setVisible(true);
            acceptButton.setVisible(true);
            result.draw.visible = true;

            timer = setTimer(function() {
                PlayerAsk.close();
                PlayerAsk.reject();
            }, 10000, 1);
        }
        return result;
    }

    start = function(_askPlayerId, _askType) {
        askPlayerId = _askPlayerId;
        askType = _askType;
        askValue = false;

        PlayerPacket().sendAsk(askPlayerId, askType, "Rozpoczeto..");
        window = prepareWindow();
        BaseGUI.show();
    }

    run = function(_askPlayerId, _askType, _askValue) {
        if(ActiveGui != null) {
            PlayerPacket().rejectAsk();
            return;
        }

        askPlayerId = _askPlayerId;
        askType = _askType;
        askValue = _askValue;

        window = prepareWindow();

        BaseGUI.show();
        ActiveGui = PlayerGUI.Ask;
    }

    reject = function() {
        PlayerPacket().rejectAsk();
    }

    accept = function() {
        PlayerPacket().acceptAsk(PlayerAsk.askType);
    }

    onForceCloseGUI = function () {
        if(ActiveGui == PlayerGUI.Ask)
        {
            PlayerPacket().rejectAsk();
            close();
        }
    }

    close = function() {
        askPlayerId = -1;
        askType = -1;
        askValue = false;

        if(timer != null)
            killTimer(timer);

        timer = null;
        window = null;
        rejectButton.setVisible(false);
        acceptButton.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
    }
}

addEventHandler("onForceCloseGUI", PlayerAsk.onForceCloseGUI.bindenv(PlayerAsk));