local sounds = [
    "HARP_INSERT01.WAV",
    "HARP_INSERT02.WAV",
    "HARP_INSERT03.WAV",
    "HARP_INSERT04.WAV",
    "HARP_INSERT05.WAV",
    "HARP_INSERT06.WAV",
    "HARP_INSERT07.WAV",
    "HARP_INSERT08.WAV",
    "HARP_INSERT09.WAV",
    "HARP_INSERT10.WAV",
    "HARP_INSERT11.WAV",
    "HARP_INSERT12.WAV",
    "HARP_INSERT13.WAV",
    "HARP_INSERT14.WAV",
    "HARP_INSERT15.WAV",
    "HARP_INSERT16.WAV",
    "HARP_INSERT17.WAV",
    "HARP_INSERT18.WAV",
    "HARP_INSERT19.WAV",
    "HARP_INSERT20.WAV",
    "HARP_INSERT21.WAV",
    "HARP_INSERT22.WAV",
];

local pck = getTickCount();

PlayerHarp <- {
    bucket = null
    setLoop = false,
    setVolume = 5,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(100), "SG_BOX.TGA", null, false)
        result.buttonExit <- GUI.Button(anx(400), any(120), anx(200), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);

        result.draw <- GUI.Draw(anx(50), any(20), "1 - Z to klawisze aby odegra� d�wi�k.\nKwadratowe nawiasy, aby zmieni� tryby.", result.window);
        result.drawTwo <- GUI.Draw(anx(50), any(60), "G�o�no��: 5", result.window);

        result.buttonExit.bind(EventType.Click, function(element) {
            PlayerHarp.hide();
        });

        return result;
    }

    playSound = function(playerId, soundId, looping, volume) {
        local obj = Sound3d(sounds[soundId]);
        obj.volume = (volume * 1.0).tofloat() * 0.1;
        obj.setTargetPlayer(playerId);
        obj.looping = false;
        obj.radius = 1500;
        obj.play();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Harp;

        playAni(heroId, "T_DRUMSCHEIT_STAND_2_S0");
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onKey = function(key) {
        if(ActiveGui != PlayerGUI.Harp)
            return;

        if(pck > getTickCount())
            return;

        pck = getTickCount() + 50;
        switch(key) {
            case KEY_1:
                PlayerPacket().playHarp(0, setLoop, setVolume);
            break;
            case KEY_2:
                PlayerPacket().playHarp(1, setLoop, setVolume);
            break;
            case KEY_3:
                PlayerPacket().playHarp(2, setLoop, setVolume);
            break;
            case KEY_4:
                PlayerPacket().playHarp(3, setLoop, setVolume);
            break;
            case KEY_5:
                PlayerPacket().playHarp(4, setLoop, setVolume);
            break;
            case KEY_6:
                PlayerPacket().playHarp(5, setLoop, setVolume);
            break;
            case KEY_7:
                PlayerPacket().playHarp(6, setLoop, setVolume);
            break;
            case KEY_8:
                PlayerPacket().playHarp(7, setLoop, setVolume);
            break;
            case KEY_9:
                PlayerPacket().playHarp(8, setLoop, setVolume);
            break;
            case KEY_0:
                PlayerPacket().playHarp(9, setLoop, setVolume);
            break;
            case KEY_Q:
                PlayerPacket().playHarp(10, setLoop, setVolume);
            break;
            case KEY_W:
                PlayerPacket().playHarp(11, setLoop, setVolume);
            break;
            case KEY_E:
                PlayerPacket().playHarp(12, setLoop, setVolume);
            break;
            case KEY_R:
                PlayerPacket().playHarp(13, setLoop, setVolume);
            break;
            case KEY_T:
                PlayerPacket().playHarp(14, setLoop, setVolume);
            break;
            case KEY_Y:
                PlayerPacket().playHarp(15, setLoop, setVolume);
            break;
            case KEY_U:
                PlayerPacket().playHarp(16, setLoop, setVolume);
            break;
            case KEY_I:
                PlayerPacket().playHarp(17, setLoop, setVolume);
            break;
            case KEY_O:
                PlayerPacket().playHarp(18, setLoop, setVolume);
            break;
            case KEY_P:
                PlayerPacket().playHarp(19, setLoop, setVolume);
            break;	
            case KEY_A:
                PlayerPacket().playHarp(20, setLoop, setVolume);
            break;	
            case KEY_S:
                PlayerPacket().playHarp(21, setLoop, setVolume);
            break;				
            case KEY_LBRACKET:
                setVolume = setVolume - 1;
                if(setVolume < 1)
                    setVolume = 1;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
            case KEY_RBRACKET:
                setVolume = setVolume + 1;
                if(setVolume > 10)
                    setVolume = 10;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
        }
    }
}

addEventHandler("onKey", PlayerHarp.onKey.bindenv(PlayerHarp));

Bind.addKey(KEY_ESCAPE, PlayerHarp.hide.bindenv(PlayerHarp), PlayerGUI.Harp)