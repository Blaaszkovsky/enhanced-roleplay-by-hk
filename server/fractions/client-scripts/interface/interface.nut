
FractionsInterface <- {
    workingObj = null,
    bucket = null,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 200), anx(400), any(300), "SG_BOX.TGA");
        result.drawTitle <- GUI.Draw(anx(40), any(40), "", result.window);
        result.external <- [];
        result.drawTitle.setScale(0.5,0.5);
        result.drawTitle.setFont("FONT_OLD_20_WHITE_HI.TGA");

        return result;
    }

    show = function(playerId) {
        if(bucket == null)
            bucket = prepareWindow();

        BaseGUI.show();
        bucket.window.setVisible(true);
        bucket.drawTitle.setText(getPlayerRealName(playerId));
        ActiveGui = PlayerGUI.Fraction;
    }

    hide = function() {
        BaseGUI.hide();
        bucket.window.setVisible(false);
        ActiveGui = null;

        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();
    }

    setUp = function(obj) {
        workingObj = obj;
        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();

        if(obj.fractionId == -1) {
            bucket.external.push(GUI.Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), "Gracz nie nalezy do \n�adnej frakcji."));
            bucket.external.push(GUI.Button(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 80), anx(320), any(60), "SG_BUTTON.TGA", "Zapro�"));

            bucket.external[bucket.external.len()-1].bind(EventType.Click, function(element) {
                local playerId = FractionsInterface.workingObj.playerId;
                FractionsInterface.hide();
                PlayerAsk.start(playerId, PlayerAskType.Invite);
            });
        }else if(obj.fractionId == Hero.fractionId) {
            local ranksSelector = [];
            foreach(rank in getFraction(Hero.fractionId).ranks)
                ranksSelector.push({id = rank.id, name = rank.name});

            bucket.external.push(MyGUI.DropDown(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 100), anx(320), any(60), "Rola", ranksSelector, "Wybierz rang�..", obj.fractionRoleId));
            bucket.external.push(GUI.Button(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 10), anx(150), any(60), "SG_BOX.TGA", "Zapisz"));
            local deleteButton = bucket.external[bucket.external.len() - 1];
            deleteButton.bind(EventType.Click, function(element) {
                local playerId = FractionsInterface.workingObj.playerId;
                local valueOfBucket = FractionsInterface.bucket.external[0].getValue();
                FractionPacket().setFractionPlayer(playerId, Hero.fractionId, valueOfBucket);
                FractionsInterface.hide();
            });

            bucket.external.push(GUI.Button(anx(Resolution.x/2 + 10), any(Resolution.y/2 - 10), anx(150), any(60), "SG_BOX.TGA", "Usu�"));
            local saveButton = bucket.external[bucket.external.len() - 1];
            saveButton.bind(EventType.Click, function(element) {
                local playerId = FractionsInterface.workingObj.playerId;
                FractionPacket().setFractionPlayer(playerId, -1, -1);
                FractionsInterface.hide();
            });
        }else
            bucket.external.push(GUI.Draw(anx(Resolution.x/2 - 160), any(Resolution.y/2 - 140), "Gracz nie nale�y do twojej frakcji."));

        foreach(external in bucket.external)
            external.setVisible(true);
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Fraction)
            hide();
    }
}

addEventHandler("onForceCloseGUI", FractionsInterface.onForceCloseGUI.bindenv(FractionsInterface));
Bind.addKey(KEY_ESCAPE, FractionsInterface.hide.bindenv(FractionsInterface), PlayerGUI.Fraction);
