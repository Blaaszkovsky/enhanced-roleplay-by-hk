
class FractionRank {
    id = -1
    name = ""

    crafts = null
    factions = null

    canTrade = false
    canInvite = false
    canManage = false
    canManageBoard = false;
    canCraft = false

    constructor(_id, _name) {
        id = _id
        name = _name

        crafts = []
        factions = []

        canTrade = false
        canInvite = false
        canManage = false
        canManageBoard = false;
        canCraft = false

    }

    function full(value) {
        canTrade = value
        canInvite = value
        canManage = value
        canManageBoard = value
        canCraft = value

    }

    function addFaction(value) {
        factions.push(value.id)
    }

    function addCrafts(value) {
        foreach(name in value)
            crafts.push(name);
    }

    function updateAfterCraftsLoaded() {
        local allCrafts = getCrafts();
        local newCrafts = [];
        local fractions = getFractions();

        foreach(fraction in fractions) {
            foreach(__ in allCrafts) {
                if(__.fractionIds.find(fraction.id.tostring()) != null) {
                    newCrafts.push(__.id);
                    break;
                }
            }
        }

        crafts = newCrafts;
    }
}
