
class Fraction {
    static FractionTable = "fraction";

    id = -1
    name = ""

    ranks = null
    territories = null

    firstRank = -1;

    constructor() {
        id = -1
        name = ""

        ranks = {};
        territories = {};

        firstRank = -1;
    }

    function addRank(_id, _name) {
        ranks[_id] <- FractionRank(_id, _name);
        return ranks[_id];
    }

    function addTerritory(_id, _name, _area) {
        territories[_id] <- FractionTerritory(_id, _name, _area);
        return territories[_id];
    }

    function updateAfterCraftsLoaded() {
        foreach(rank in ranks)
            rank.updateAfterCraftsLoaded();
    }

    function getFirstRank() {
        return firstRank;
    }
}