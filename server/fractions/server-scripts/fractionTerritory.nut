
class FractionTerritory {
    id = -1
    name = null
    area = null

    constructor(_id, _name, _area) {
        id = _id;
        name = _name;
        area = _area;
    }
}