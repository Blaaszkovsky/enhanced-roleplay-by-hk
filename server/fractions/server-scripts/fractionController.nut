
class FractionController
{
    static fractions = {};

    static function createFraction(fractionId, name) {
        local obj = Fraction();
        obj.name = name;
        obj.id = fractionId;

        fractions[fractionId] <- obj;
        return obj;
    }

    static function updateAfterCraftsLoaded() {
        foreach(fraction in fractions)
            fraction.updateAfterCraftsLoaded();
    }

    static function getCraftObjectsForFractionMember(playerId) {
        local obj = getPlayer(playerId);
        local fraction = getFraction(obj.fractionId);
        if(fraction == null)
            return [];

        if(obj.fractionRoleId in fraction.ranks)
            return fraction.ranks[obj.fractionRoleId].crafts;
        else
            return [];
    }

    static function getPlayerFactions(playerId) {
        local obj = getPlayer(playerId);
        local fraction = getFraction(obj.fractionId);
        if(fraction == null)
            return [];

        if(obj.fractionRoleId in fraction.ranks)
            return fraction.ranks[obj.fractionRoleId].factions;
        else
            return [];
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ) {
            case FractionPackets.SetForPlayer:
                setPlayerFraction(packet.readInt16(), packet.readInt16(), packet.readInt16());
            break;
        }
    }
}

function getFraction(fractionId) {
    if(fractionId in FractionController.fractions)
        return FractionController.fractions[fractionId];;

    return null;
}

getFractions <- @() FractionController.fractions;
RegisterPacketReceiver(Packets.Fraction, FractionController.onPacket.bindenv(FractionController));
