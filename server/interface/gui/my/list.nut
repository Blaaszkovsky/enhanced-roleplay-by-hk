
local SelectLists = [];

class MyGUI.SelectList
{
	background = null
	list = null
	scroll = null
	search = null

	length = -1
	items = null

	onClick = null
	visible = false

	constructor(x,y,width,height,showbackground = true, showSearch = false)
	{
		this.background = GUI.Texture(x,y,width,height,"SG_BOX.TGA")

		if(showbackground == false)
			background.setAlpha(0);

		this.scroll = GUI.ScrollBar(x + width - anx(60), y + any(20), anx(30), height - any(40), "SG_SCROLL.TGA", "SG_BUTTON.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical)

		if(showSearch)
			this.search = GUI.Input(x + anx(20), y + any(20), width - anx(80), any(40), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Szukaj..", 2);
		else
			this.search = null;

		this.list = {}
		this.items = []

		this.visible = false
		this.length = abs((height - any(40))/any(50))

		this.onClick = function(id) {};

		local startYIndex = 20;
		if(showSearch) {
			startYIndex = startYIndex + 60;
			length = length - 1;
		}

		for(local i = 0; i < length; i ++)
			this.items.append(GUI.Button(x + anx(20), y + any(startYIndex) + any(50 * i), width - anx(80), any(40), "SG_BUTTON.TGA", ""));

		SelectLists.append(this);
	}

	function clear() {
		list.clear();
		scroll.setVisible(false);

		updateItems();
	}

	function addOption(id, name) {
		list[id] <- name;

		if(getOptionsLength() > length) {
			scroll.setVisible(true);
			scroll.setMaximum(getOptionsLength() - length);
		}else
			scroll.setVisible(false);

		if(visible)
			updateItems();
	}

	function removeOption(id) {
		if(id in list)
			list.rawdelete(id)

		if(getOptionsLength() > length) {
			scroll.setVisible(true);
			scroll.setMaximum(getOptionsLength() - length);
		}else
			scroll.setVisible(false);

		if(visible)
			updateItems();
	}

	function getOptionsLength() {
		local i = 0;
		foreach(_ in list)
			i ++;

		return i;
	}

	function setVisible(value) {
		visible = value;

		visible ? show() : hide();
	}

	function show() {
		background.setVisible(true);
		if(search)
			search.setVisible(true);

		if(getOptionsLength() > length) {
			scroll.setVisible(true);
			scroll.setMaximum(getOptionsLength() - length);
		}else
			scroll.setVisible(false);

		scroll.top();

		updateItems(0);
	}

	function hide() {
		scroll.setVisible(false);
		background.setVisible(false);
		if(search) {
			search.setVisible(false);
			search.setText("");
		}

		foreach(item in items)
			item.setVisible(false);
	}

	function render() {
		foreach(item in items) {
			if(item.getAlpha() != 255)
				item.setAlpha(item.getAlpha() + 15);
		}
	}

	function click(element) {
		foreach(item in items) {
			if(item == element) {
				this.onClick(element.attribute);
				break;
			}
		}
	}

	function updateItems(alpha = 0) {
		foreach(item in items)
			item.setVisible(false);

		local text = "";

		if(search != null)
		 	text = search.getText().toupper();

		if(scroll.visible)
		{
			local value = scroll.getValue();
			local i = 0;
			foreach(key,option in list) {
				if(text.len() > 0)
					if(option.toupper().find(text) == null)
						continue;

				if(i >= value && i < (value + length)) {
					items[(i - value)].setText(option);
					items[(i - value)].setVisible(true);
					items[(i - value)].attribute = key;
					items[(i - value)].setAlpha(alpha);
				}
				i++;
			}
		}
		else
		{
			local i = 0;
			foreach(key,option in list) {
				if(text.len() > 0)
					if(option.toupper().find(text) == null)
						continue;

				items[i].setText(option);
				items[i].setVisible(true);
				items[i].attribute = key;
				items[i].setAlpha(alpha);
				i++;
			}
		}
	}

	function onInputInsertLetter(inpt, letter) {
		if(inpt != search)
			return;

		local text = search.getText().toupper();
		updateItems();
	}

	function destroy() {
		background.destroy();
		scroll.destroy();

		foreach(item in items)
			item.destroy();

		foreach(index, qlist in SelectLists)
			if(qlist == this)
				SelectLists.remove(index);
	}
}

addEventHandler("GUI.onChange", function(element) {
	foreach(index, qlist in SelectLists)
		if(qlist.visible)
			if(qlist.scroll == element)
				qlist.updateItems();
});


addEventHandler("GUI.onClick", function(element) {
	foreach(index, qlist in SelectLists)
		if(qlist.visible)
			qlist.click(element);
});

local fps = getTickCount();

addEventHandler("onRender", function() {
	if(fps < getTickCount()) {
		foreach(index, qlist in SelectLists)
			if(qlist.visible)
				qlist.render();

		fps = getTickCount() + 50;
	}
});

addEventHandler("GUI.onInputInsertLetter", function(inpt, letter) {
	foreach(index, qlist in SelectLists)
		if(qlist.visible)
			qlist.onInputInsertLetter(inpt, letter);
});

addEventHandler("GUI.onInputRemoveLetter", function(inpt, letter) {
	foreach(index, qlist in SelectLists)
		if(qlist.visible)
			qlist.onInputInsertLetter(inpt, letter);
});

