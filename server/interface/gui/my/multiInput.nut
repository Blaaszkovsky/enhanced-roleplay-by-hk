local multiLinesInputs = [];

class MyGUI.MultiLineInput
{
    position = null
    width = -1

    lines = -1
    value = null

    active = false
    visible = false

    currentLine = -1
    pointer = -1

    constructor(x,y,width,linesLength)
    {
        this.position = {x = x, y = y}
        this.width = width

        this.value = []
        this.lines = []

        for(local i = 0; i < linesLength; i ++)
            this.lines.append(Draw(0,0,""));

        this.currentLine = -1

        this.pointer = Draw(0,0,">");
        this.pointer.setColor(200,0,0);

        this.active = false
        this.visible = false

        multiLinesInputs.append(this);
    }

    function setValue(content)
    {
        foreach(line in lines)
            line.text = "";

        value = content;

        foreach(index, line in content)
            lines[index].text = line;
    }

    function getValue()
    {
        local tab = [];

        foreach(index, line in lines) {
            tab.append(line.text);
        }
        return tab;
    }

    function setVisible(value)
    {
        visible = value
        visible ? show() : hide();
    }

    function show()
    {
        pointer.visible = true;
        currentLine = 0;

        foreach(index, line in lines) {
            line.visible = true;
            line.setPosition(position.x + 30, position.y + 175 * index);

            if(line.text.len() > 0)
                currentLine = index;
        }

        updateTextLine();
    }

    function hide()
    {
        active = false
        pointer.visible = false;

        foreach(line in lines)
            line.visible = false;
    }

    function updateTextLine() {
        pointer.setPosition(position.x - 50, position.y + 175 * currentLine)
    }

    function destroy() {
        multiLinesInputs.remove(multiLinesInputs.find(this));
    }

    function setColor(r,g,b) {
        foreach(line in lines)
            line.setColor(r,g,b);
    }

    function keyHandler(key) {
        if(!active)
            return;

        local letter = getKeyLetter(key);
        if(letter) {
            if(lines[currentLine].width > width) {
                if(currentLine < lines.len() - 1)
                    currentLine = currentLine + 1;
                else
                    return;
            }

            updateTextLine();
            lines[currentLine].text += letter;
        }

        if(key == KEY_BACK)
        {
            if(lines[currentLine].text.len() > 0)
                lines[currentLine].text = lines[currentLine].text.slice(0, lines[currentLine].text.len() - 1);
            else {
                if(currentLine == 0)
                    return;

                currentLine = currentLine - 1;
                updateTextLine();
            }
        }
    }

    static function onTimer() {
        foreach(input in multiLinesInputs)
            if(input.active)
                input.pointer.visible = !input.pointer.visible;
    }

    static function onKey(key) {
        if(isGUIInputActive == true)
            return;

        foreach(input in multiLinesInputs)
            input.keyHandler(key);
    }
}

setTimer(function() {
    MyGUI.MultiLineInput.onTimer();
}, 400, 0);

addEventHandler("onKey", MyGUI.MultiLineInput.onKey.bindenv(MyGUI.MultiLineInput));