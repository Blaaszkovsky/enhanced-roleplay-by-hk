
class WorldInterface.Texture
{
    position = null
    size = null
    color = null

    file = null
    rotation = 0
    alpha = 0

    distance = -1
    shrinkMode = false
    shrinkScale = null
    eyeMode = false

    element = null
    visible = false
    rendered = false

    constructor(x, y, z, width, height, _file)
    {
        position = {x = x, y = y, z = z}
        size = {width = width, height = height}
        color = {r = 255, g = 255, b = 255}

        file = _file
        rotation = 0
        alpha = 255

        distance = 3000
        shrinkMode = true
        shrinkScale = {x = 1.0, y = 1.0}
        eyeMode = true

        element = null
        visible = false
        rendered = false

        WorldInterface.list.append(this);
    }

    function remove() {
        local index = WorldInterface.render.find(this);
        if(index != null)
            WorldInterface.render.remove(index);

        local index = WorldInterface.list.find(this);
        if(index != null)
            WorldInterface.list.remove(index);
    }

    function update(pos) {
        if(visible == false) {
            if(element)
                element.visible = false;

            return;
        }

        local _distance = getDistance3d(pos.x, pos.y, pos.z, position.x, position.y, position.z);
        if(_distance <= distance)
        {
            local show = true;
            local camera = _Camera.project(position.x, position.y, position.z);
            if(camera == null)
                show = false;

            if(eyeMode && show)
            {
                local vector = getVectorAngle(position.x, position.z, pos.x, pos.z) + 500;
                vector = abs(vector - (getPlayerAngle(heroId) + 500));
                if(!(vector > 140 && vector < 220))
                    show = false;
            }

            if(shrinkScale == false)
                shrinkScale = {x = 1.0, y = 1.0}
            else
                shrinkScale = {x = 1.0 - (_distance/distance), y = 1.0 - (_distance/distance)}

            if(show) {
                element.setPositionPx(camera.x, camera.y);
                element.setSize(size.width * shrinkScale.x, size.height * shrinkScale.y)
                element.visible = true;
            }else
                element.visible = false;
        }else
            element.visible = false;
    }

    function render() {
        rendered = true;

        element = Texture(0,0,0,0,file);
        element.file = file;
        element.rotation = rotation;
        element.alpha = alpha;
        element.setColor(color.r, color.g, color.b);
    }

    function unrender() {
        rendered = false;

        element = null;
    }

    function setWorldPosition(x,y,z) {
        position.x = x;
        position.y = y;
        position.z = z;
    }
}