
WorldInterface <- {};

WorldInterface.list <- [];
WorldInterface.render <- [];

WorldInterface.check <- getTickCount();

WorldInterface.onRender <- function() {
    local position = getPlayerPosition(heroId);

    foreach(item in render)
        item.update(position);

    if(check < getTickCount())
    {
        foreach(item in list)
        {
            if(getPositionDifference(position, item.position) > 5000)
            {
                local index = render.find(item);
                if(index != null) {
                    item.unrender();
                    render.remove(index);
                }
            }
            else
            {
                local index = render.find(item);
                if(index == null) {
                    item.render();
                    render.push(item);
                }
            }
        }

        check = getTickCount() + 1000;
    }
}

addEventHandler("onRender", WorldInterface.onRender.bindenv(WorldInterface));
