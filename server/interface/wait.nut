 
WaitInterface <-
{
    background = GUI.Texture(0,0,8129,8129,"WHITE.TGA")
    draw = GUI.Draw(0,0,"")

    show = function (text) {
        WaitInterface.draw.setText(text);
        WaitInterface.background.setColor(0,0,0);
        WaitInterface.background.setAlpha(150);
        WaitInterface.background.setVisible(true);
        WaitInterface.draw.setVisible(true);
        WaitInterface.draw.setPosition(4100-WaitInterface.draw.getSize().width/2, 3900);
    },

    hide = function () {
        WaitInterface.background.setVisible(false);
        WaitInterface.draw.setVisible(false);        
    }
}