
eggInstance <- null;

class EggTimer
{
    onEnd = null;

    attribute = -1;
    externalAttribute = -1;

    lastCheck = -1;
    timeNow = 0;
    timeLeft = 0;

    texture = null;
    button = null;

    constructor(time)
    {
        attribute = -1;
        externalAttribute = -1;

        lastCheck = getTickCount();
        timeNow = 0;
        timeLeft = time;

        texture = Texture(anx(Resolution.x/2 - 256), any(Resolution.y/2 - 30), anx(512), any(64), "SG_SCROLL_HORIZONTAL.TGA");
        button = Texture(anx(Resolution.x/2 - 250), any(Resolution.y/2 - 26), anx(500), any(56), "WHITE.TGA");

        button.setColor(190, 160, 140);
        button.alpha = 100;

        updateBar();

        texture.visible = true;
        button.visible = true;

        onEnd = null;
        eggInstance = this;
    }

    function updateBar()
    {
        local percent = abs((timeNow * 100)/timeLeft);
        button.setSize(anx((500 * percent)/100), any(56));
    }

    function onRender()
    {
        local now = getTickCount();

        timeNow = timeNow + (now - lastCheck);

        updateBar();
        if(timeNow > timeLeft)
        {
            if(onEnd != null)
                onEnd(this);

            eggInstance = null;
            return;
        }

        lastCheck = now;
    }
}

addEventHandler("onDamage", function(struct) {
    if(eggInstance != null)
        eggInstance = null;
})

addEventHandler("onRender", function() {
    if(eggInstance != null)
        eggInstance.onRender();
})

function getEggTimer() {
    return eggInstance;
}

function resetEggTimer() {
    eggInstance = null;
}