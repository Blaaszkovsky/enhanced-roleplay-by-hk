local freeCamEnabled = false
local freeCamPaused = false

local freeCamMovementSpeed = 300.0 // meters per sec
local freeCamRotationSpeed = 100.0 // meters per sec

// hooks

local oldCursorTxt = getCursorTxt()
local _setCursorTxt = setCursorTxt
function setCursorTxt(txt)
{
	if (!freeCamEnabled || freeCamPaused)
		_setCursorTxt(txt)
	else
		oldCursorTxt = txt
}

function setCursorInFreeCamera(txt)
{
	_setCursorTxt(txt);
}

// public functions

isFreeCamEnabled <- @() freeCamEnabled
function enableFreeCam(toggle)
{
	_Camera.modeChangeEnabled = !toggle
	_Camera.movementEnabled = !toggle

	setFreeze(toggle)

	if (toggle)
	{
		oldCursorTxt = getCursorTxt()
		_setCursorTxt("MODIALPHA01.TGA")
	}
	else
		_setCursorTxt(oldCursorTxt)

	setCursorVisible(toggle)
	freeCamEnabled = toggle
}

isFreeCamPaused <- @() freeCamPaused
function pauseFreeCam(toggle)
{
	if (!freeCamEnabled)
		return

	freeCamPaused = toggle

	if (toggle)
		_setCursorTxt(oldCursorTxt)
	else
		_setCursorTxt("MODIALPHA01.TGA")
}

getFreeCamMovementSpeed <- @() freeCamMovementSpeed
function setFreeCamMovementSpeed(speed)
{
	freeCamMovementSpeed = speed
}

getFreeCamRotationSpeed <- @() freeCamRotationSpeed
function setFreeCamRotationSpeed(speed)
{
	freeCamRotationSpeed = speed
}

// locals

local deg2Rad = 0.0174533

local before = getTickCount()
local oldCursorPosition = getCursorPosition()

addEventHandler("onRender",function()
{
	local now = getTickCount()
	local cursorPosition = getCursorPosition()

	if (freeCamEnabled && !freeCamPaused && !chatInputIsOpen())
	{
		local deltaTime = (now - before) * 0.001

		local position = _Camera.getPosition()
		local rotation = _Camera.getRotation()

		local movementAcceleration = freeCamMovementSpeed * deltaTime
		local rotationAcceleration = freeCamRotationSpeed * deltaTime

		local vecForward = {
			x = cos(deg2Rad * rotation.x) * sin(deg2Rad * rotation.y),
			y = sin(deg2Rad * rotation.x),
			z = cos(deg2Rad * rotation.x) * cos(deg2Rad * rotation.y)
		}

		local vecRight = {
			x = vecForward.z,
			y = 0,
			z = -vecForward.x
		}

		local isForwardKeyPressed = isKeyPressed(KEY_UP) || isKeyPressed(KEY_W)
		local isBackwardKeyPressed = isKeyPressed(KEY_DOWN) || isKeyPressed(KEY_S)

		local isControlPressed = isKeyPressed(KEY_LCONTROL) || isKeyPressed(KEY_RCONTROL)

		if (isKeyPressed(KEY_LSHIFT) || isKeyPressed(KEY_RSHIFT))
		{
			movementAcceleration *= 3
			rotationAcceleration *= 1.5
		}

		if (isForwardKeyPressed && !isBackwardKeyPressed)
		{
			position.x += vecForward.x * movementAcceleration
			position.y -= vecForward.y * movementAcceleration
			position.z += vecForward.z * movementAcceleration
		}
		else if (isBackwardKeyPressed && !isForwardKeyPressed)
		{
			position.x -= vecForward.x * movementAcceleration
			position.y += vecForward.y * movementAcceleration
			position.z -= vecForward.z * movementAcceleration
		}

		if (isKeyPressed(KEY_LEFT) && !isKeyPressed(KEY_RIGHT))
			if (!isControlPressed)
				rotation.y -= rotationAcceleration
			else
				rotation.z -= rotationAcceleration
		else if (isKeyPressed(KEY_RIGHT) && !isKeyPressed(KEY_LEFT))
			if (!isControlPressed)
				rotation.y += rotationAcceleration
			else
				rotation.z += rotationAcceleration

		if (isKeyPressed(KEY_SPACE) && !isKeyPressed(KEY_X))
			position.y += movementAcceleration
		else if (isKeyPressed(KEY_X) && !isKeyPressed(KEY_SPACE))
			position.y -= movementAcceleration

		if (isKeyPressed(KEY_NEXT) && !isKeyPressed(KEY_PRIOR))
			rotation.x += rotationAcceleration
		else if (isKeyPressed(KEY_PRIOR) && !isKeyPressed(KEY_NEXT))
			rotation.x -= rotationAcceleration

		if (isKeyPressed(KEY_A) && !isKeyPressed(KEY_D))
		{
			position.x -= vecRight.x * movementAcceleration
			position.z -= vecRight.z * movementAcceleration
		}
		else if (isKeyPressed(KEY_D) && !isKeyPressed(KEY_A))
		{
			position.x += vecRight.x * movementAcceleration
			position.z += vecRight.z * movementAcceleration
		}

		if (oldCursorPosition.x != cursorPosition.x || oldCursorPosition.y != cursorPosition.y)
		{
			local cursorDeltaX = (cursorPosition.x - oldCursorPosition.x) / 8192.0
			local cursorDeltaY = (cursorPosition.y - oldCursorPosition.y) / 8192.0

			rotation.x += cursorDeltaY * freeCamRotationSpeed
			rotation.y += cursorDeltaX * freeCamRotationSpeed

			if (cursorPosition.x == 0)
				cursorPosition.x = 8192
			else if (cursorPosition.x  >= 8192)
				cursorPosition.x = 0

			if (cursorPosition.y == 0)
				cursorPosition.y = 8192
			else if (cursorPosition.y >= 8192)
				cursorPosition.y = 0

			setCursorPosition(cursorPosition.x, cursorPosition.y)
		}

		_Camera.setPosition(position.x, position.y, position.z)
		_Camera.setRotation(rotation.x, rotation.y, rotation.z)
	}


	before = now
	oldCursorPosition = cursorPosition
})

// addEventHandler ("onKey", function (_key) {
// 	if (freeCamEnabled && !freeCamPaused && !chatInputIsOpen())
// 	{
// 		local _camPos = _Camera.getPosition();
// 		local _camRot = _Camera.getRotation();
// 		if(_key == KEY_X) {
// 			local text = "{ x = "+_camPos.x + ", y = "+_camPos.y+", z = "+_camPos.z+"}";
// 			print(text);
// 		}
// 		if(_key == KEY_D) {
// 			local text = "path.createPoint({position = ["+_camPos.x + ","+_camPos.y+","+_camPos.z+"], rotation = ["+_camRot.x + ","+_camRot.y+","+_camRot.z+"], lerpSpeed = 300});";
// 			PlayerPacket().print(text);
// 		}
// 	}
// });


// // client-side
// addEventHandler("onCommand", function(cmd, params)
// {
//     // This command will toggle the free cam mode
// 	local pos = _Camera.getPosition();
// 	if (cmd == "freecam")
// 		setTimer(function(){enableFreeCam(!isFreeCamEnabled())}, 1000, 1);
// 	else if(cmd == "cpos")
// 		print(pos);
// })
