_Camera <- Camera;

class CameraManagment
{
    stopped = false;
    vobCamera = null;

    constructor()
    {
        stopped = false;
        vobCamera = Vob("CAMERA.3D");
    }

    function setPosition(x,y,z)
    {
        _Camera.setPosition(x,y,z);
    }

    function setRotation(x,y,z)
    {
        _Camera.setRotation(x,y,z)
    }

    function getPosition()
    {
        return _Camera.getPosition();
    }

    function getRotation()
    {
        return _Camera.getPosition();
    }

    function setMode(val)
    {
        _Camera.setMode(val);
    }

    function getMode()
    {
        return _Camera.getMode();
    }

    function setBeforePlayer(pid=heroId, distance = 120)
    {
        local pos = getPlayerPosition(pid);
        local angle = getPlayerAngle(pid);
        local x = pos.x, y = pos.y, z = pos.z;

        if(getPlayerAni(pid) == "S_BENCH_S1")
            angle = angle + 180;

        setBeforePos(distance, x, y, z, angle);
    }

    function setBeforePos(distance = 120,x = 0,y = 0,z = 0, angle=0)
    {
        _Camera.setPosition(x,y,z);
        _Camera.setRotation(0, angle, 0);

        x = x + (sin(angle * 3.14 / 180.0) * distance);
        z = z + (cos(angle * 3.14 / 180.0) * distance);

        _Camera.setPosition(x, y, z);
        _Camera.setRotation(0, angle + 180, 0);
    }

    function setBesidePlayer(pid=heroId, distance = 120)
    {
        local pos = getPlayerPosition(pid);
        local angle = getPlayerAngle(pid);
        local x = pos.x, y = pos.y, z = pos.z;

        setBesidePos(distance, x, y, z, angle);
    }

    function setBesidePos(distance = 120,x = 0,y = 0,z = 0, angle=0)
    {
        angle = angle + 70;

        _Camera.setPosition(x,y,z);
        _Camera.setRotation(0, angle, 0);

        x = x + (sin(angle * 3.14 / 180.0) * distance);
        z = z + (cos(angle * 3.14 / 180.0) * distance);

        _Camera.setPosition(x, y, z);
        _Camera.setRotation(0, angle + 180, 0);
    }

    function setBeforePosFromTop(distance, x, y, z)
    {
        setBeforePos(distance, x, y, z);
        local pos = _Camera.getPosition();
        local rot = _Camera.getRotation();
        _Camera.setPosition(pos.x, pos.y + 100 + (distance*0.1), pos.z);
        _Camera.setRotation(rot.x + 20, rot.y, rot.z);
    }

    function setTargetPlayer(pid=heroId)
    {
        _Camera.setTargetPlayer(pid)
        _Camera.modeChangeEnabled = false;
    }

    function setFreeze(val)
    {
        if(!val)
            setDefaultCamera();
        else {
            _Camera.movementEnabled = false
            _Camera.modeChangeEnabled = false
        }
    }

    function setDefaultCamera()
    {
        _Camera.setTargetPlayer(heroId)
        _Camera.movementEnabled = true
        _Camera.modeChangeEnabled = true
    }
}

Camera <- CameraManagment();