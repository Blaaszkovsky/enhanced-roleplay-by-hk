
local window = GUI.Window(anx(Resolution.x/2 - 270), any(Resolution.y/2 - 300), anx(540), any(340), "SG_BOX.TGA", null, false);
local netStats = GUI.Draw(anx(40), any(20), "", window);

setTimer(function () {
    if(ActiveGui == PlayerGUI.Net)
        updateNetMenu();
}, 500, 0);

function updateNetMenu()
{
    local stats = getNetworkStats();
    local text = [
        format("Ping: %i ms", getPlayerPing(heroId)),
        format("FPS: %i", getFpsRate()),
        format("Pakiety: %i", stats.packetReceived),
        format("Utracone pakiety: %i", stats.packetlossTotal),
        format("Utracone pakiety (ostatnia sekunda): %i", stats.packetlossLastSecond),
        format("Wiadomo�� do ponownego wys�ania: %i", stats.messagesInResendBuffer),
        format("Wiadomo�� do wys�ania: %i", stats.messageInSendBuffer),
        format("Bity do ponownego wys�ania: %i", stats.bytesInResendBuffer),
        format("Bity do wys�ania: %i", stats.bytesInSendBuffer),
    ];

    netStats.setText(String.parseArray(text));
}

function showNetMenu()
{
    BaseGUI.show();
    ActiveGui = PlayerGUI.Net;
    window.setVisible(true);
}

function hideNetMenu()
{
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

Bind.addKey(KEY_F6, showNetMenu);
Bind.addKey(KEY_F6, hideNetMenu, PlayerGUI.Net)
Bind.addKey(KEY_ESCAPE, hideNetMenu, PlayerGUI.Net)