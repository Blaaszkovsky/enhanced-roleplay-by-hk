

AnimationInterface <- {
    Buttons = [],

    FreeCamButton = GUI.Button(anx(Resolution.x/2 - 75), any(Resolution.y/2 - 50), anx(150), any(50), "SG_BUTTON.TGA", "Wolna Kamera"),
    BuildingButton = GUI.Button(anx(Resolution.x/2 - 75), any(Resolution.y - 70), anx(200), any(50), "SG_BUTTON.TGA", "Budowanie"),

    List = MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600), true, true),
    Animations = [
        {name = "Czytanie", anim = "T_MAPSEALEDKEY_STAND_2_S0"},
        {name = "Ogl�danie walki 1", anim = "T_WATCHFIGHT_YEAH_02"},
        {name = "Ogl�danie walki 2", anim = "T_WATCHFIGHT_YEAH_03"},
        {name = "Ogl�danie walki 3", anim = "T_WATCHFIGHT_OHNO_02"},
        {name = "Ogl�danie walki 4", anim = "T_WATCHFIGHT_OHNO_03"},
        {name = "Zdenerwowane rozgl�danie si�", anim = "T_SEARCH_SCARED"},
        {name = "Krzy� na drog�", anim = "T_PISSOFF"},
        {name = "�mianie si�", anim = "T_LAUGH"},
        {name = "D�ugie �mianie si�", anim = "T_LAUGH_LONG"},
        {name = "Kichni�cie", anim = "T_WHISTLE"},
        {name = "Wymiotowanie", anim = "T_STAND_2_VOMIT"},
        {name = "Opieranie si� o �cian�", anim = "T_STAND_2_LEAN"},
        {name = "Oburzenie", anim = "T_WOLFROAR"},
        {name = "Si�ganie do kieszeni", anim = "T_INN_STAND_2_S0"},
        {name = "R�ce z ty�u", anim = "T_STAND_2_MILSTAND"},
        {name = "R�ka na piersi", anim = "T_MILSTAND_2_MILJOIN"},
        {name = "Ogl�danie miecza", anim = "T_1HST0INSPECT"},
        {name = "Zm�czenie", anim = "T_STAND_2_BREATH"},
        {name = "Siadanie b�d�c zwi�zanym", anim = "T_STAND_2_MPRISON"},
        {name = "Oparcie o st�", anim = "T_STAND_2_TABLEPLAN"},
        {name = "Wyci�gni�cie skr�ta", anim = "T_JOINTSTRONG_STAND_2_S0"},
        {name = "Zamy�lenie", anim = "T_STAND_2_COUNTING"},
        {name = "Roz�o�enie r�k", anim = "T_STAND_2_KILLME"},
        {name = "Podrzucenie mieszkiem", anim = "T_SELLJOINT_STAND_2_S0"},
        {name = "�ebranie 1", anim = "T_LUTZSIT_STAND_2_S0"},
        {name = "Zwi�zane r�ce", anim = "T_STAND_2_LEADER"},
        {name = "Wzruszenie r�koma", anim = "T_SHRUG"},
        {name = "Dobicie", anim = "T_JON_KNEEL"},
        {name = "Szept na boku", anim = "T_WHISPER_START"},
        {name = "Patrzenie na przedmiot", anim = "T_FINGERSCOUNT_START"},
        {name = "Trzymanie si� za brzuch", anim = "T_STOMACHHURT_START"},
        {name = "Kl�kni�cie ze zwi�zanym r�koma", anim = "T_EXECUTIONKNEEL_STAND_2_S0"},
        {name = "Le�enie w trumnie", anim = "T_STAND_2_FANESLEEP"},
        {name = "Wiwat", anim = "T_ANGRYMOB_01"},
        {name = "Wiwat 1", anim = "T_ANGRYMOB_02"},
        {name = "K�adzenie si� spa�", anim = "T_BEDHIGH_START"},
        {name = "Pchanie", anim = "T_TRYOPENGATE"},
        {name = "Rozci�gni�cie ramienia", anim = "R_STRETCH_RANDOM_01"},
        {name = "Gest podcinania gard�a", anim = "T_THREAT_DEATH"},
        {name = "Strach", anim = "T_SHOCKED_LOOP"},
        {name = "Patrzenie w horyzont", anim = "T_STAND_2_LOOKOUT"},
        {name = "Pchanie 2", anim = "S_CANONPUSH_S1"},
        {name = "Zakrycie oczu", anim = "S_CRY"},
        {name = "Przykuc 1", anim = "S_SITSLAV_S1"},
        {name = "R�ka w kieszeni", anim = "S_WAITING"},
        {name = "Strach 2", anim = "S_SCAREDDIALOGUE"},
        {name = "Przeszukanie beczki", anim = "R_BARRELCONTAINER_KAJMA"},
        {name = "T-posing", anim = "T_POSE"},
        {name = "R�ka na biodrze", anim = "S_SNIFFQ303_S0"},
        {name = "Depresja(trzymanie si� za g�ow�)", anim = "T_RAZORFIGHT_SHORT"},
        {name = "Depresja(trzymanie si� za g�ow� i upadek na kolana)", anim = "T_RAZORFIGHT_LONG"},
        {name = "Depresyjne siadanie", anim = "T_STAND_2_TIREDSIT"},
        {name = "Depresyjne siadanie 1", anim = "T_STAND_2_DEPRESSIONSIT"},
        {name = "Taniec 1", anim = "S_DANCE_1"},
        {name = "Taniec 2", anim = "S_DANCE_2"},
        {name = "Taniec 3", anim = "S_DANCE_3"},
        {name = "Taniec 4", anim = "S_DANCE_4"},
        {name = "Taniec 5", anim = "S_CHIK"},
        {name = "Twerkowanie", anim = "S_TWERK"},
        {name = "Gangnam Style", anim = "S_GANGAM"},
        {name = "Cancan (taniec)", anim = "S_CANCAN"},
        {name = "Taniec 6", anim = "S_JAZZ"},
        {name = "Makarena", anim = "S_MACARENA"},
        {name = "Moonwalk", anim = "S_MOONWALK"},
        {name = "Taniec 7", anim = "S_SILLY_1"},
        {name = "Taniec 8", anim = "S_SILLY_2"},
        {name = "Taniec 9", anim = "S_TWIST"},
        {name = "YMCA", anim = "S_YMCA"},
        {name = "Fala (taniec)", anim = "S_WAVE"},
        {name = "Taniec 10", anim = "S_SWING_1"},
        {name = "Taniec 11", anim = "S_SWING_2"},
        {name = "Taniec 12", anim = "S_SWING_3"},
        {name = "Mdlenie", anim = "T_VICTIM"},
        {name = "Facepalm", anim = "T_FACEPALM"},
        {name = "Trz�sanie nogami", anim = "R_LEGSHAKE"},
        {name = "Kopni�cie obiektu", anim = "T_KICKOBJECT_S0_2_S1"},
        {name = "Rozgl�danie", anim = "T_SEARCH"},
        {name = "Upadek moralny", anim = "T_FALLDOWN_QUICK"},
        {name = "Egzekucja", anim = "T_EXECUTION_S0_2_S1"},
        {name = "Sniff", anim = "T_SNIFFQ303_S0_2_STAND"},
        {name = "Za�amanie nerwowe", anim = "T_Q405_MARVIN_ONKNEE"},
        {name = "Wymioty", anim = "R_VOMIT_RANDOM_01"},
    ]

    Tree = {
        "Taniec": {
            "Domy�lne": {
                "Taniec 1" : "S_DANCE_1",
                "Taniec 2" : "S_DANCE_2",
                "Taniec 3" : "S_DANCE_3",
                "Taniec 4" : "S_DANCE_4",
                "Taniec 5" : "S_SWING_1",
                "Taniec 6" : "S_SWING_2",
                "Taniec 7" : "S_SWING_3",
            },
            "Erotyczne": {
                "Twerk" : "S_TWERK",
                "Kobiecy taniec 2" : "T_DANCEK2",
                "Striptiz" : "T_STAND_2_STRIPPEN",
                "Taniec brzucha" : "T_STAND_2_BAUCHTANZ",
            },
            "Makarena" : "S_MACARENA",
            "MoonWalk" : "S_MOONWALK",
            "YMCA" : "S_YMCA",
            "Gangam Style" : "S_GANGAM",
        },
        "Mimika": {
            "Mrugni�cie" : ["R_EYESBLINK"],
            "Z�y" : ["S_ANGRY"],
            "Oczy zamkni�te" : ["S_EYESCLOSED"],
            "Przera�ony" : ["S_FRIGHTENED"],
            "Normalny" : ["S_NEUTRAL"],
            "Przyjazny" : ["S_FRIENDLY"],
        },
        "Czynno�ci" : {
            "Oddawanie czci" : "T_IDOL_S0_2_S1",
            "Uk�on" : "T_GREETNOV",
            "Przywitanie maga" : "T_JUBELN_04",
            "Dobicie" : "T_1HSFINISH",
            "Wygrana" : "T_WYGRANA",
            "Sceniczne" : {
                "Uk�on artysty 1" : "T_AMBIENT_VERBEUGEN_01",
                "Uk�on artysty 2" : "T_AMBIENT_VERBEUGEN_02",
                "Ogl�daj walk�" : "T_WATCHFIGHT_YEAH",
                "Kibicowanie" : "S_KIBICOWANIE",
                "Klaskanie" : {
                    "Domy�lne" : "T_STAND_2_CLAPHANDS",
                    "Powolne" : "T_JUBELN_05",
                    "Eskpresyjne" : "T_JUBELN_01",
                    "Wyrachowane" : "T_JUBELN_02",
                    "Dystynktowne" : "T_JUBELN_03",
                },
            },
            "Machanie" : {
                "Machaj" : "T_MACHANIE",
                "Pomachaj" : "T_AMBIENT_SCHIMPFEN",
                "Machaj2" : "T_MACHANIE2",
                "Machaj3" : "T_ARENAMACHANIE",
            }
            "Zbierz" : "T_PLUNDER",
            "Sikanie" : "T_STAND_2_PEE",
            "Pijackie" : {
                "Sikanie po pijaku" : "T_DRUNKENPEE",
                "Rzyganie" : "S_KOTZKUEBEL_S1",
            }
            "Pochodnia" : {
                "Roz�wietl teren" : "T_POSWIECENIE",
                "Roz�wietl teren 2" : "T_POSWIECENIE2",
                "Atak pochodni�" : "T_POCHODNIA",
            }
            "B�aganie" : {
               "Padni�cie do kolan" : "T_BETEN_STAND_2_S0",
               "B�aganie" : "T_BLAGANIE",
            }
        },
        "U�ycie" : {
            "Prace" : {
                "�cinaj" : "S_LUMBERJACK_S1",
                "Przybijaj" : "S_FLOORHAM_S1",
                "Naprawiaj" : "S_REPAIR_S1",
                "Kop w ziemii" : "S_DIGGER_S1",
                "Kop rud�" : "S_ORE_S1",
                "Zbieraj z ziemii" : "S_EINPFLANZEN_S1",
                "Alchemia" : "S_LAB_S1",
            }
            "Pal" : "T_JOINT_RANDOM_1",
            "Pij" : "T_POTION_RANDOM_1",
            "Jedz" : "T_MEAT_RANDOM_1",
            "Myj si�" : "T_STAND_2_WASH",
            "Czytaj notke" : "T_MAP_STAND_2_S0",
            "Czytaj pulpit" : "S_BOOK_S1",
            "Ostrz bro�" : "T_CARVE_STAND_2_S0",
            "Picie wody z rzeki" : "T_KNIEENTRINKEN_STAND_2_S0",
            "Granie na lutni" : "S_LUTE_S1",
        },
        "�wiczenia" : {
            "Salto" : "T_SALTO",
            "Trening" : "T_1HSFREE",
            "Rozgrzewka" : "T_ROZGRZEWKA",
            "Boxowanie" : "T_BOXOWANIE",
            "Gimnastyka" : {
                "Rozci�ganie" : "T_GYMNASTIK_01",
                "Pr�enie musku��w" : "T_GYMNASTIK_04",
                "Przysiady" : "T_GYMNASTIK_05",
                "Pompki" : {
                    "Pompki zap�tlone" : "S_POMPKI_S1",
                    "Zej�cie do pompek" : "T_POMPKI",
                 },
                "Szpagat" : "T_GYMNASTIK_08",
                "Przeci�ganie si�" : "T_GYMNASTIK_09",
            },
        },
        "Statyczne" : {
            "Siadanie" : {
                "Domy�lne" : "T_STAND_2_SIT",
                "Przy ognisku" : "T_STAND_2_PACO",
                "Wyluzowane" : "T_STAND_2_MELVIN",
                "Medytacja" : "T_STAND_2_BAAL",
                "Na �awce" : "S_BENCH_S1",
            },
            "Le�enie" : {
                "Na plecach" : "S_DEADB",
                "Na brzuchu" : "S_DEAD",
                "Na boku" : "T_STAND_2_SLEEPGROUND",
            },
            "Mumia" : "T_MUMIE_STAND",
            "Stra�nik 1" : "T_STAND_2_LGUARD",
            "Stra�nik 2" : "T_STAND_2_HGUARD",
            "Oparcie o �ciane" : "T_STAND_2_LEAN",
            "Kucni�cie" : "T_PAN_STAND_2_S0",
            "Modlitwa" : "T_STAND_2_PRAY",
            "Przykuc" : "T_STAND_2_PRZYKUC",
            "Oparcie si�" : "S_OPIERANIESIE",
        },
        "Magia" : {
            "Magia 1": "T_PRACTICEMAGIC",
            "Magia 2": "T_PRACTICEMAGIC2",
            "Magia 3": "T_PRACTICEMAGIC3",
            "Magia 4": "T_PRACTICEMAGIC4",
            "Magia 5": "T_PRACTICEMAGIC6",
            "Magia 6": "T_PRACTICEMAGIC7",
            "Energia": "T_ENERGIA",
            "Magicznakula": "T_KULAFULA",
            "Czar": "T_CZAR",
        },
        "R�ne" : {
            "Przegl�d miecza" : "T_1HSINSPECT",
            "Szale�stwo" : "S_CON_VICTIM",
            "Kopni�cie z nud�w" : "T_BORINGKICK",
            "Rozgl�danie si�" : {
                "Rozgl�danie si� 1" : "T_ZASIEBIE",
                "Rozgl�danie si� 2" : "T_ZASIEBIE2",
                "Rozgl�danie si� 3" : "T_ROZGLADANIE",
            }
            "Wypatrywanie" : "S_UMSEHEN_S1",
            "Wytarcie twarzy" : "T_POTION_RANDOM_2",
            "Stanie w miejscu" : {
                "�ebranie" : "T_STAND_2_BETTELN",
                "Stanie przy �cianie" : "T_STAND_2_ARMCROSSED_A",
                "Stanie pod latarni�" : "T_STANDTALK",
                "Stanie jak kopacz" : "T_STANDRIECHEN",
                "Gadanie" : "T_GADANIE",
            }
            "Salutowanie" : "T_SALUTOWANIE",
            "Strza�a amora" : "T_STRZELANIELUK",
        },
        "Reakcje" : {
            "Podstawowe" : {
                "Tak" : "T_YES",
                "Nie" : "T_NO",
                "Nie wiem" : "T_DONTKNOW",
                "Facepalm" : "T_IDIOT",
                "Dzieki" : "T_DZIEKUJE",
            }
            "No Nie!" : "T_NONIE",
            "Zapomnij o tym" : "T_FORGETIT",
            "Gro�ba" : {
                "Gro�ba 1" : "T_GETLOST",
                "Gro�ba 2" : "T_GETLOST2",
            }
            "Ho no tu!" : "T_COMEOVERHERE"
            "�miech" : {
                "Zwyk�y �miech" : "T_LAUGHING",
                "Intensywny �miech" : "T_LACHEN",
            }
            "Krzyk" : "T_OKRZYK",
            "Zastraszony" : "T_FRIGHTENED",
            "Przestraszony" : "T_STRACH",
            "Obola�y" : "T_WJAJA",
            "Mdlenie" : "T_VICTIM",
            "Zawiedziony" : "T_ZAWIEDZIONY",
        },
    }

    onInit = function() {
        BuildingButton.bind(EventType.Click, function(element) {
            AnimationInterface.Hide();
            BuildingBuilderInterface.show();
        });

        FreeCamButton.bind(EventType.Click, function(element) {
            AnimationInterface.Hide();
        });
    }

    Show = function () {
        BaseGUI.show();
        ActiveGui = PlayerGUI.Animation;
        AnimationInterface.ShowTreeFor(AnimationInterface.Tree);

        //FreeCamButton.setVisible(true);

        if(Hero.skills[PlayerSkill.Carpenter] > 2)
            BuildingButton.setVisible(true);
    }

    Hide = function () {
        BaseGUI.hide();
        ActiveGui = null;

        foreach(butt in AnimationInterface.Buttons)
            butt.button.setVisible(false);

        AnimationInterface.Buttons.clear();

        AnimationInterface.FreeCamButton.setVisible(false);
        AnimationInterface.BuildingButton.setVisible(false);
    }

    ShowList = function () {
        BaseGUI.show();
        ActiveGui = PlayerGUI.AnimationList;

        AnimationInterface.List.clear();
        AnimationInterface.List.setVisible(true);

        foreach(animationId, animationName in AnimationInterface.Animations) {
            AnimationInterface.List.addOption(animationName.anim, animationName.name);
        }

        AnimationInterface.List.onClick = function(id) {
            playAni(heroId, id);
        }
    }

    HideList = function () {
        BaseGUI.hide();
        ActiveGui = null;
        AnimationInterface.List.setVisible(false);
    }

    ShowTreeFor = function(table)
    {
        local degreePerButton = 360/table.len();
        local showedButtons = 0;

        foreach(name, value in table)
        {
            local positionButton = {x = 3596, y = 3596};
            local rotation = degreePerButton * showedButtons;

            positionButton.x = positionButton.x + (sin(rotation * 3.14 / 180.0) * 1600);
            positionButton.y = positionButton.y + (cos(rotation * 3.14 / 180.0) * 1600);

            local button = GUI.Button(positionButton.x, positionButton.y, anx(200), any(70), "SG_INPUT", name);
            button.setVisible(true);

            AnimationInterface.Buttons.append({button = button, option = value});
            showedButtons = showedButtons + 1;
        }
    },

    ClickOption = function(optionButton)
    {
        foreach(option in AnimationInterface.Buttons)
        {
            if(option.button == optionButton)
            {
                if(typeof option.option == "table")
                {
                    foreach(butt in AnimationInterface.Buttons)
                        butt.button.setVisible(false);

                    AnimationInterface.Buttons.clear();
                    AnimationInterface.ShowTreeFor(option.option);
                }else if(typeof option.option == "array")
                {
                    playFaceAni(heroId, option.option[0]);
                    AnimationInterface.Hide();
                }else if(typeof option.option == "string")
                {
                    playAni(heroId, option.option);
                    AnimationInterface.Hide();
                }
                break;
            }
        }
    }
}

addEventHandler("onInit", AnimationInterface.onInit.bindenv(AnimationInterface));

Bind.addKey(KEY_ESCAPE, AnimationInterface.Hide, PlayerGUI.Animation)
Bind.addKey(KEY_ESCAPE, AnimationInterface.HideList, PlayerGUI.AnimationList)

addEventHandler("onMouseClick", function(btn) {
    if(ActiveGui != null)
        return;

    if (chatInputIsOpen())
		return

	if (isConsoleOpen())
		return

    if(getPlayerWeaponMode(heroId) != WEAPONMODE_NONE)
        return;

    if(btn == MOUSE_RMB)
        AnimationInterface.Show();
})


addEventHandler("GUI.onClick", function(self)
{
    if(ActiveGui != PlayerGUI.Animation)
        return;

    AnimationInterface.ClickOption(self);
})

keyBinds <- {}
local current = null

addEventHandler("GUI.onMouseIn", function(element) {
    foreach(option in AnimationInterface.List.items)
    {
        if (option == element)
            current = element
    }
})

addEventHandler("GUI.onMouseOut", function(element) {
    foreach(option in AnimationInterface.List.items)
    {
        if (option == element)
            current = null
    }
})

addEventHandler("onKey", function(key)
{
    if (ActiveGui == null)
    {
        if ((key in keyBinds))
            playAni(heroId, keyBinds[key]);

        return
    }

    if (ActiveGui != PlayerGUI.AnimationList)
        return

    switch(key)
    {
        case KEY_NUMPAD0:
        case KEY_NUMPAD1:
        case KEY_NUMPAD2:
        case KEY_NUMPAD3:
        case KEY_NUMPAD4:
        case KEY_NUMPAD5:
        case KEY_NUMPAD6:
        case KEY_NUMPAD7:
        case KEY_NUMPAD8:
        case KEY_NUMPAD9:
        {
            if (!(key in keyBinds))
            {
                foreach(option in AnimationInterface.List.items)
                {
                    if (option == current)
                    keyBinds[key] <- option.attribute
                }
            }
            else
            {
                foreach(option in AnimationInterface.List.items)
                {
                    if (option == current)
                    {
                        if (keyBinds[key] == option.attribute)
                        {
                            local packet = ExtendedPacket(Packets.Player);
                            packet.writeUInt8(PlayerPackets.AnimationBindDelete)
                            packet.writeUInt16(key);
                            packet.send(RELIABLE)
                            delete keyBinds[key]

                            return
                        }

                        keyBinds[key] = option.attribute
                    }
                }
            }

            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.AnimationBind)
            packet.writeUInt16(key);
            packet.writeString(keyBinds[key])
            packet.send(RELIABLE)

            break
        }
    }
})

addEventHandler("GUI.onClick", function(self)
{
    if(ActiveGui != PlayerGUI.Animation)
        return;

    AnimationInterface.ClickOption(self);
})