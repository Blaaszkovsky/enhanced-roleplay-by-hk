local lock = false;

enum VisualOption
{
    Body,
    Skin,
    Head,
    Face,
    Size,
    Fatness,
    Walking
}

local pojebiemnieprzytymkodzie = null
local models = {}
local walks = {}

local ROWS_AMOUNT = 8
local CELLS_AMOUNT = 2

VisualChange <- {

    bucket = null,

    prepareWindow = function () {
        local result = {};

        result.window <- GUI.Window(anx(40), any(Resolution.y/2 - 300), anx(420), any(350), "SG_BOX.TGA", null, false);

        result.bodyButton <- GUI.Button(anx(10), any(50), anx(200), any(60), "SG_BUTTON.TGA", "P�e�", result.window);
        result.skinButton <- GUI.Button(anx(210), any(50), anx(200), any(60), "SG_BUTTON.TGA", "Cia�o", result.window);
        result.headButton <- GUI.Button(anx(10), any(130), anx(200), any(60), "SG_BUTTON.TGA", "G�owa", result.window);
        result.faceButton <- GUI.Button(anx(210), any(130), anx(200), any(60), "SG_BUTTON.TGA", "Twarz", result.window);
        result.walkingButton <- GUI.Button(anx(10), any(210), anx(200), any(60), "SG_BUTTON.TGA", "Chodzenie", result.window);
        result.sizeButton <- GUI.Button(anx(10), any(0), anx(0), any(0), "SG_BUTTON.TGA", "", result.window);
        result.fatnessButton <- GUI.Button(anx(210), any(210), anx(200), any(60), "SG_BUTTON.TGA", "Grubo��", result.window);

        result.scrollBar <- GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, result.window);
        result.scrollBar.setMaximum(360);
        result.scrollBar.setSize(anx(500), any(30));
        result.scrollBar.setPosition(anx(Resolution.x/2 - 250), any(Resolution.y - 200));

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            setPlayerAngle(heroId, value);
        }, PlayerGUI.Visual);

        result.bodyButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Body);
        });

        result.skinButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Skin);
        });

        result.headButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Head);
        });

        result.faceButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Face);
        });

        result.sizeButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Size);
        });

        result.fatnessButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Fatness);
        });

        result.walkingButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Walking);
        });

        result.windowOperation <- GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "SG_BOX.TGA", null, false);
        result.operations <- [];
        result.pizdaprzemka <- []

        return result;
    }

    chooseOperation = function (typeId)
    {
        foreach(element in bucket.operations)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);

            element.destroy();
        }

        lock = false;
        stopAni(heroId);

        pojebiemnieprzytymkodzie = typeId
        models.clear()
        walks.clear()

        bucket.operations.clear();
        bucket.pizdaprzemka.clear()
        bucket.windowOperation.destroyAll();
        bucket.windowOperation = GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "SG_BOX.TGA", null, false);

        switch(typeId)
        {
            case VisualOption.Body:
                local maleButton = GUI.Button(anx(50), any(200), anx(300), any(80), "SG_BUTTON.TGA", "Mczyzna", bucket.windowOperation);
                local femaleButton = GUI.Button(anx(50), any(300), anx(300), any(80), "SG_BUTTON.TGA", "Kobieta", bucket.windowOperation);

                maleButton.bind(EventType.Click, function(element) {
                    VisualChange.changeVisual(VisualOption.Body, "Hum_Body_Naked0");
                });
                femaleButton.bind(EventType.Click, function(element) {
                    VisualChange.changeVisual(VisualOption.Body, "Hum_Body_Babe0");
                });

                bucket.operations.append(maleButton);
                bucket.operations.append(femaleButton);
            break;
            case VisualOption.Skin:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "HUM_BODY_NAKED_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        VisualChange.changeVisual(VisualOption.Skin, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxSkin"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            VisualChange.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            VisualChange.bucket.operations[((left * 2) + top + left)].setFile("HUM_BODY_NAKED_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.Visual);
            break;
            case VisualOption.Head:

            bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(25), any(30), "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, bucket.windowOperation));
            local cellsAdded = 0

            local row = 0
            local cell = 0

            foreach(model in Config["headModels"])
            {
                if (!(row in models))
                    models[row] <- []

                if(cellsAdded < (ROWS_AMOUNT) * CELLS_AMOUNT)
                {
                    local button = GUI.Button(anx(cell == 0 ? 20 : 210), any(30 + row * 60), anx(170), any(50), "SG_BUTTON", model.name, bucket.windowOperation);
                    button.attribute = model
                    bucket.pizdaprzemka.push(button);

                    bucket.pizdaprzemka.top().bind(EventType.Click, function(element)
                    {
                          VisualChange.changeVisual(VisualOption.Head, element.attribute.asc);
                    })
                }

                models[row].push({model = model})

                if (cell == CELLS_AMOUNT - 1)
                {
                    ++row
                    cell = 0

                    models[row] <- []
                }
                else
                    ++cell

                ++cellsAdded
            }

            local positionPx = bucket.windowOperation.getPositionPx()
            local sizePx = bucket.windowOperation.getSizePx()

            bucket.windowOperation.setPositionPx(positionPx.x - 40, positionPx.y)
            bucket.windowOperation.setSizePx(sizePx.width + 40, sizePx.height)

            positionPx = bucket.windowOperation.getPositionPx()
            sizePx = bucket.windowOperation.getSizePx()

            bucket.operations[0].setSizePx(30, sizePx.height - 45);
            bucket.operations[0].setPositionPx(positionPx.x + sizePx.width - bucket.operations[0].getSizePx().width - 20, positionPx.y);

            if (row > ROWS_AMOUNT - 1)
                bucket.operations[0].setMaximum(row - ROWS_AMOUNT + cellsAdded % 2)
            else
                bucket.operations[0].setMaximum(0)

            bucket.operations[0].setValue(0)

            break;

            case VisualOption.Face:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "Hum_Head_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        VisualChange.changeVisual(VisualOption.Face, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "SG_SCROLL_HORIZONTAL.TGA", "SG_INDICATOR.TGA", "SG_L.TGA", "SG_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxFace"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            VisualChange.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            VisualChange.bucket.operations[((left * 2) + top + left)].setFile("Hum_Head_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.Visual);
            break;

            case VisualOption.Size:
                local midgetSizeButton = GUI.Button(anx(20), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Krasnal", bucket.windowOperation);
                local dwarfSizeButton = GUI.Button(anx(210), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Ma�y", bucket.windowOperation);
                local smallSizeButton = GUI.Button(anx(20), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Niski", bucket.windowOperation);
                local normalSizeButton = GUI.Button(anx(210), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tallSizeButton = GUI.Button(anx(20), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local giantSizeButton = GUI.Button(anx(210), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Gigant", bucket.windowOperation);

                midgetSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Midget);});
                dwarfSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Dwarf);});
                smallSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Small);});
                normalSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Normal);});
                tallSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Tall);});
                giantSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Giant);});

                bucket.operations.append(midgetSizeButton);bucket.operations.append(dwarfSizeButton);bucket.operations.append(smallSizeButton);
                bucket.operations.append(normalSizeButton);bucket.operations.append(tallSizeButton);bucket.operations.append(giantSizeButton);
            break;

            case VisualOption.Fatness:
                local thinFatnessButton = GUI.Button(anx(20), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Chudzielec", bucket.windowOperation);
                local skinnyFatnessButton = GUI.Button(anx(210), any(150), anx(170), any(80), "SG_BUTTON.TGA", "Chudy", bucket.windowOperation);
                local normalFatnessButton = GUI.Button(anx(20), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local muscularFatnessButton = GUI.Button(anx(210), any(250), anx(170), any(80), "SG_BUTTON.TGA", "Uminiony", bucket.windowOperation);
                local thickFatnessButton = GUI.Button(anx(20), any(350), anx(170), any(80), "SG_BUTTON.TGA", "Pulchny", bucket.windowOperation);
                local fatFatnessButton = GUI.Button(anx(210), any(350), anx(170), any(80), "SG_BUTTON.TGA", "T�usty", bucket.windowOperation);

                thinFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Thin);});
                skinnyFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Skinny);});
                normalFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Normal);});
                muscularFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Muscular);});
                thickFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Thick);});
                fatFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Fat);});

                bucket.operations.append(thinFatnessButton);bucket.operations.append(skinnyFatnessButton);bucket.operations.append(normalFatnessButton);
                bucket.operations.append(muscularFatnessButton);bucket.operations.append(thickFatnessButton);bucket.operations.append(fatFatnessButton);
            break;

            case VisualOption.Walking:
                lock = getPlayerPosition(heroId);
                playAni(heroId, "S_WALKL");

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(25), any(30), "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, bucket.windowOperation));
                local cellsAdded = 0

                local row = 0
                local cell = 0

                foreach(walk in Config["WalkStyles"])
                {
                    if (!(row in walks))
                        walks[row] <- []

                    if(cellsAdded < (ROWS_AMOUNT) * CELLS_AMOUNT)
                    {
                        local button = GUI.Button(anx(cell == 0 ? 20 : 210), any(30 + row * 60), anx(170), any(50), "SG_BUTTON", walk.name, bucket.windowOperation);
                        button.attribute = walk
                        bucket.pizdaprzemka.push(button);

                        bucket.pizdaprzemka.top().bind(EventType.Click, function(element)
                        {
                              VisualChange.changeVisual(VisualOption.Walking, element.attribute.idx);
                        })
                    }

                    walks[row].push({walk = walk})

                    if (cell == CELLS_AMOUNT - 1)
                    {
                        ++row
                        cell = 0

                        walks[row] <- []
                    }
                    else
                        ++cell

                    ++cellsAdded
                }

                local positionPx = bucket.windowOperation.getPositionPx()
                local sizePx = bucket.windowOperation.getSizePx()

                bucket.windowOperation.setPositionPx(positionPx.x - 40, positionPx.y)
                bucket.windowOperation.setSizePx(sizePx.width + 40, sizePx.height)

                positionPx = bucket.windowOperation.getPositionPx()
                sizePx = bucket.windowOperation.getSizePx()

                bucket.operations[0].setSizePx(30, sizePx.height - 45);
                bucket.operations[0].setPositionPx(positionPx.x + sizePx.width - bucket.operations[0].getSizePx().width - 20, positionPx.y);

                if (row > ROWS_AMOUNT - 1)
                    bucket.operations[0].setMaximum(row - ROWS_AMOUNT + cellsAdded % 2)
                else
                    bucket.operations[0].setMaximum(0)

                bucket.operations[0].setValue(0)

            break;
        }

        bucket.windowOperation.setVisible(true);
    }

    changeVisual = function(type, value)
    {
        local getVisual = getPlayerVisual(heroId);
        switch(type)
        {
            case VisualOption.Body:
                setPlayerVisual(heroId, value, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
            case VisualOption.Skin:
                setPlayerVisual(heroId, getVisual.bodyModel, value, getVisual.headModel, getVisual.headTxt);
            break;
            case VisualOption.Head:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, value, getVisual.headTxt);
            break;
            case VisualOption.Face:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, value);
            break;
            case VisualOption.Size:
                setPlayerHeight(heroId, value);
            break;
            case VisualOption.Fatness:
                setPlayerFatness(heroId, value);
            break;
            case VisualOption.Walking:
                setPlayerWalkingStyle(heroId, value);
            break;
        }
        getVisual = getPlayerVisual(heroId);
        PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
    }

    destroyWindow = function ()
    {
        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox || element instanceof GUI.ScrollBar)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);

        bucket.window.destroyAll();
        bucket.windowOperation.destroyAll();
    },

    start = function () {
        BaseGUI.show();
        ActiveGui = PlayerGUI.Visual;

        bucket = prepareWindow();
        bucket.window.setVisible(true);
        bucket.scrollBar.setVisible(true);
        bucket.scrollBar.setValue(getPlayerAngle(heroId).tointeger());
    },

    hide = function () {
        BaseGUI.hide();
        ActiveGui = null;

        lock = false;
        stopAni(heroId);

        destroyWindow();
        bucket = null;
    }
}


addEventHandler("onRender", function() {
    if(lock != false)
    {
        if(getPlayerAni(heroId) != "S_WALKL")
            playAni(heroId, "S_WALKL");

        setPlayerPosition(heroId, lock.x, lock.y, lock.z);
    }
})

addEventHandler("GUI.onChange", function(self)
{
	if (!ActiveGui == PlayerGUI.Visual)
		return

    if (pojebiemnieprzytymkodzie == VisualOption.Head)
    {
        if(self != VisualChange.bucket.operations[0])
            return

        local value = self.getValue().tointeger()
        local currentCell = 0

        for(local row = value, end = value + ROWS_AMOUNT; row < end; row++)
        {
            if (!(row in models))
                break

            local _models = models[row]
            local cellsAmount = _models.len()

            for(local cellIdx = 0; cellIdx < cellsAmount; cellIdx++)
            {
                if (!VisualChange.bucket.pizdaprzemka[currentCell].getVisible())
                    VisualChange.bucket.pizdaprzemka[currentCell].setVisible(true)

                VisualChange.bucket.pizdaprzemka[currentCell].setText(_models[cellIdx].model.name)
                VisualChange.bucket.pizdaprzemka[currentCell].attribute = _models[cellIdx].model

                VisualChange.bucket.pizdaprzemka[currentCell].bind(EventType.Click, function(element)
                {
                      VisualChange.changeVisual(VisualOption.Head, element.attribute.asc);
                })

                ++currentCell
            }

            if (cellsAmount == 1)
                VisualChange.bucket.pizdaprzemka[currentCell].setVisible(false)
        }
    }
    else if(pojebiemnieprzytymkodzie == VisualOption.Walking)
    {
        if(self != VisualChange.bucket.operations[0])
            return

        local value = self.getValue().tointeger()
        local currentCell = 0

        for(local row = value, end = value + ROWS_AMOUNT; row < end; row++)
        {
            if (!(row in walks))
                break

            local _walks = walks[row]
            local cellsAmount = _walks.len()

            for(local cellIdx = 0; cellIdx < cellsAmount; ++cellIdx)
            {
                if (!VisualChange.bucket.pizdaprzemka[currentCell].getVisible())
                    VisualChange.bucket.pizdaprzemka[currentCell].setVisible(true)

                VisualChange.bucket.pizdaprzemka[currentCell].setText(_walks[cellIdx].walk.name)
                VisualChange.bucket.pizdaprzemka[currentCell].attribute = _walks[cellIdx].walk

                VisualChange.bucket.pizdaprzemka[currentCell].bind(EventType.Click, function(element)
                {
                      VisualChange.changeVisual(VisualOption.Walking, element.attribute.idx);
                })

                ++currentCell
            }

            if (cellsAmount == 1)
                VisualChange.bucket.pizdaprzemka[currentCell].setVisible(false)
        }
    }
})


addEventHandler("onForceCloseGUI", function() {
    if(ActiveGui == PlayerGUI.Visual)
        VisualChange.hide();
});
Bind.addKey(KEY_ESCAPE, VisualChange.hide.bindenv(VisualChange), PlayerGUI.Visual)
