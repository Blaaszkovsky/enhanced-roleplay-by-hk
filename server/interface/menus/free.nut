

function showFreeMenu(params = null) {
    BaseGUI.show();
    ActiveGui = PlayerGUI.Free;

    for(local i = 0; i<200; i++)
        disableKey(i, false);

    Camera.setFreeze(false);
    setFreeze(false)
    setCursorVisible(false)

    VobController.toggleDraws(false)
}

function hideFreeMenu() {
    BaseGUI.hide();
    ActiveGui = null;

    VobController.toggleDraws(true)
}

Bind.addKey(KEY_F7, showFreeMenu)
Bind.addKey(KEY_F7, hideFreeMenu, PlayerGUI.Free)
Bind.addKey(KEY_ESCAPE, hideFreeMenu, PlayerGUI.Free)

addCommand("hud", showFreeMenu);

