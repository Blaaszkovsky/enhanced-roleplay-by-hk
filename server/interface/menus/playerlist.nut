
PlayersList <-
{
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 - 350), anx(610), any(760), "SG_BOX.TGA", null, false);
        result.scrollBar <- GUI.ScrollBar(anx(570), any(50), anx(30), any(660), "SG_SCROLL.TGA", "SG_INDICATOR.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, result.window);
        result.buttons <- [];

        local visiblePlayers = 0;
        foreach(player in getPlayers())
        {
            if(visiblePlayers > 11)
                break;

            if(isPlayerStreamed(player.id))
            {
                result.buttons.append({
                    idbutton = GUI.Button(anx(50), any(visiblePlayers * 50 + 50), anx(40), any(45), "SG_BUTTON.TGA", player.id, result.window),
                    button = GUI.Button(anx(100), any(visiblePlayers * 50 + 50), anx(420), any(45), "SG_BUTTON.TGA", getPlayerRealName(player.id), result.window),
                });
                local rgbColor = getPlayerColor(player.id);
                result.buttons[result.buttons.len()-1].button.draw.setColor(rgbColor.r, rgbColor.g, rgbColor.b);
                visiblePlayers = visiblePlayers + 1;
            }else{
                result.buttons.append({
                    idbutton = GUI.Button(anx(50), any(visiblePlayers * 50 + 50), anx(40), any(45), "SG_BUTTON.TGA", player.id, result.window),
                    button = GUI.Button(anx(100), any(visiblePlayers * 50 + 50), anx(420), any(45), "SG_BUTTON.TGA", "Brak", result.window),
                });
                visiblePlayers = visiblePlayers + 1;
            }
        }

        result.searchInput <- GUI.Input(anx(50), any(660), anx(300), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Szukaj..", 2, result.window),
        result.searchButton <- GUI.Button(anx(350), any(660), anx(150), any(45), "SG_BUTTON.TGA", "Szukaj", result.window),
        result.searchResetButton <- GUI.Button(anx(500), any(660), anx(50), any(45), "SG_BUTTON.TGA", "X", result.window),

        result.inputResult <- GUI.Input(anx(Resolution.x/2 - 250), any(Resolution.y/2 + 350), anx(510), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "...", 2);

        result.searchButton.bind(EventType.Click, function(element) {
            PlayersList.search();
        });

        result.searchResetButton.bind(EventType.Click, function(element) {
            PlayersList.bucket.inputResult.setVisible(false);
        });

        result.scrollBar.setMaximum(getMaxSlots() - 12);

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            PlayersList.change(value);
        }, PlayerGUI.PlayerList);

        return result;
    },

    search = function()
    {
        local value = bucket.searchInput.getText();
        if(value != null && value != " ") {
            for(local i = 0; i < getMaxSlots(); i++) {
                if(isPlayerStreamed(i)) {
                    local name = getPlayerRealName(i);
                    if(name.find(value) != null)
                    {
                        bucket.inputResult.setText(name + " ("+i+")");
                        bucket.inputResult.setVisible(true);
                        return;
                    }
                }
            }
        }
        bucket.inputResult.setVisible(false);
    },

    change = function(value)
    {
        if(type(value) == "string" || type(value) == "object")
            return;

        local visiblePlayers = 0;
        for(local _index = 0; _index < getMaxSlots(); _index++)
        {
            if(_index < value)
                continue;

            if(visiblePlayers > 11)
                break;

            if(isPlayerCreated(_index))
            {
                bucket.buttons[visiblePlayers].idbutton.setText(_index);
                bucket.buttons[visiblePlayers].button.setText(getPlayerRealName(_index));
                local rgbColor = getPlayerColor(_index);
                bucket.buttons[visiblePlayers].button.draw.setColor(rgbColor.r, rgbColor.g, rgbColor.b);
                visiblePlayers = visiblePlayers + 1;
            }else{
                bucket.buttons[visiblePlayers].idbutton.setText(_index);
                bucket.buttons[visiblePlayers].button.setText("Brak");
                bucket.buttons[visiblePlayers].button.draw.setColor(255, 255, 255);
                visiblePlayers = visiblePlayers + 1;
            }
        }
    }

    destroyWindow = function ()
    {
        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);
        bucket.window.destroyAll();
        bucket.inputResult.destroy();
    },

    start = function ()
    {
        BaseGUI.show();
        ActiveGui = PlayerGUI.PlayerList;

        bucket = prepareWindow();
        bucket.window.setVisible(true);

        PlayersList.change(bucket.scrollBar.getValue());
    },

    hide = function ()
    {
        BaseGUI.hide();
        ActiveGui = null;

        destroyWindow();
        bucket = null;
    }

    onForceCloseGUI = function()
    {
        if (ActiveGui == PlayerGUI.PlayerList)
            hide()
    }
}

Bind.addKey(KEY_F5, function() {
    PlayersList.start();
})

Bind.addKey(KEY_F5, function() {
    PlayersList.hide();
},PlayerGUI.PlayerList)

Bind.addKey(KEY_ESCAPE, function() {
    PlayersList.hide();
},PlayerGUI.PlayerList)

addEventHandler("onForceCloseGUI", PlayersList.onForceCloseGUI.bindenv(PlayersList));