
local result = {};

result.window <- GUI.Window(anx(Resolution.x/2 - 600), any(Resolution.y/2 - 400), anx(1200), any(740), "SG_BOX.TGA", null, false);
result.exitButton <- GUI.Button(anx(Resolution.x), any(Resolution.y/2) - 100, anx(50), any(50), "SG_CHECKBOX.TGA", "X", result.window);
result.keySettings <- GUI.Button(anx(50), any(Resolution.y/2 + 100), anx(200), any(50), "SG_BUTTON.TGA", "Klawisze", result.window);
result.keySettings.bind(EventType.Click, function(element) {
    hideMenuSettings();
    AccountKeySettings.show();
});

result.fields <- {};

function showMenuSettings()
{
    result.window.setVisible(true);

    foreach(field in result.fields) {
        field.setVisible(true);

        if(field instanceof GUI.Draw)
            field.setScale(0.8, 0.8);
    }

    BaseGUI.show();
    ActiveGui = PlayerGUI.Settings;
}

function hideMenuSettings()
{
    foreach(field in result.fields)
        field.setVisible(false);

    BaseGUI.hide();
    ActiveGui = null;
    result.window.setVisible(false);

    foreach(name, field in result.fields)
    {
        if(field instanceof GUI.Range)
            Config["Settings"][name].value = field.getValue();
        else if(field instanceof GUI.CheckBox)
            Config["Settings"][name].value = field.m_checked;
        else if(field instanceof GUI.Input)
            Config["Settings"][name].value = field.getText();
        else if(field instanceof MyGUI.DropDown)
            Config["Settings"][name].value = field.getValue();
    }

    local tab = [];
    foreach(settingName, setting in Config["Settings"])
        tab.push({name = settingName, value = setting.value.tostring()})

    Account.Packet.settingWrite(tab);
    callEvent("onSettingsChange");

    try {
        local resol = split(Config["Settings"]["Resolution"].value, ",");
        if(Resolution.x != resol[0] || Resolution.y != resol[1] || Resolution.bpp != resol[2])
            setResolution(resol[0].tointeger(), resol[1].tointeger(), resol[2].tointeger());
    }catch(error){}
}

Bind.addKey(KEY_ESCAPE, hideMenuSettings, PlayerGUI.Settings)

addEventHandler ("onInit", function () {
    foreach(settingName, settingValue in Config["Settings"])
        Config["Settings"][settingName].value <- Config["Settings"][settingName].defined;
});

addEventHandler("onAccountReceiveSettingData", function(data) {
    local tab = [];

    foreach(_resolution in getAvailableResolutions())
    {
        tab.append({
            id = format("%d,%d,%d", _resolution.x, _resolution.y, _resolution.bpp)
            name = format("%dx%d %d", _resolution.x, _resolution.y, _resolution.bpp)
        })
    }

    Config["Settings"]["Resolution"].data = tab;
    Config["Settings"]["Resolution"].defined = format("%d,%d,%d", Resolution.x, Resolution.y, Resolution.bpp)

    local y = 40;
    local x = false;
        

    foreach(settingName, settingValue in Config["Settings"])
    {
        local value = null;

        Config["Settings"][settingName].value = data[settingName];
        local xRad = x ? 2200 : 0;


        switch(settingValue.type)
        {
            case SettingsFormType.Checkbox:
                result.fields[settingName] <- GUI.CheckBox(anx(100) + xRad, any(y + 10), anx(40), any(40), "SG_CHECKBOX.TGA", "SG_CHECKBOX_CHECKED.TGA", result.window);
                result.fields[settingName].setChecked(settingValue.value);

                result.fields[settingName+"_name"] <- GUI.Draw(450 + anx(50) + xRad, any(y + 20), settingValue.name, result.window);
                if("description" in settingValue)
                    result.fields[settingName+"_description"] <- GUI.Draw(450 + anx(50) + xRad, any(y + 50), settingValue.description, result.window);
            break;
            case SettingsFormType.Input:
                result.fields[settingName] <- GUI.Input(anx(100) + xRad, any(y), anx(350), any(60), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "", 2, result.window);
                result.fields[settingName].setText(settingValue.value);
                result.fields[settingName+"_name"] <- GUI.Draw(anx(100) + xRad, any(y - 20), settingValue.name, result.window);
                if("description" in settingValue)
                    result.fields[settingName+"_description"] <- GUI.Draw(100 + xRad, any(y + 60), settingValue.description, result.window);
            break;
            case SettingsFormType.Select:
                result.fields[settingName] <- MyGUI.DropDown(anx(460) + xRad, any(y + 400), anx(350), any(60), settingValue.name, settingValue.data, settingValue.name, settingValue.value);
                if("description" in settingValue)
                    result.fields[settingName+"_description"] <- GUI.Draw(anx(100) + xRad, any(y + 900), settingValue.description, result.window);

                result.fields[settingName].setScale(0.8, 0.8);
            break;
            case SettingsFormType.Range:
                result.fields[settingName] <- GUI.Range(anx(100) + xRad, any(y + 20), anx(350), any(40), "SG_HORIZONTAL.TGA", "SG_BUTTON.TGA", Orientation.Horizontal, result.window);
                result.fields[settingName].setValue(settingValue.value);
                result.fields[settingName].setMinimum(settingValue.min);
                result.fields[settingName].setMaximum(settingValue.max);

                if (settingName == "SoundtrackVolume")
                    result.fields[settingName].setStep(0.1)

                result.fields[settingName+"_name"] <- GUI.Draw(anx(100) + xRad, any(y), settingValue.name + " - "+settingValue.value, result.window);
                if("description" in settingValue)
                    result.fields[settingName+"_description"] <- GUI.Draw(anx(100) + xRad, any(y + 60), settingValue.description, result.window);
            break;
        }

        if(x == true)
            y = y + 80;

        x = !x;
    }

    callEvent("onSettingsChange");
});

addEventHandler("GUI.onChange", function(element) {
    if(ActiveGui != PlayerGUI.Settings)
        return;

    foreach(name, field in result.fields)
    {
        if(field == element && field instanceof GUI.Range)
            result.fields[name+"_name"].setText(Config["Settings"][name].name+" - "+element.getValue());
    }
})

local bindExit = Bind.addClick(result.exitButton, function() {
    hideMenuSettings();
}, PlayerGUI.Settings);

function getSetting(name) {
    return Config["Settings"][name].value;
}