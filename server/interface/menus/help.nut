

local window = GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 400), anx(800), any(740), "SG_BOX.TGA", null, false);

local mainHelp = GUI.Button(anx(10), any(10), anx(250), any(60), "SG_BUTTON.TGA", "Og�lne", window);
local keysHelp = GUI.Button(anx(270), any(10), anx(250), any(60), "SG_BUTTON.TGA", "Klawisze", window);
local mechanicHelp = GUI.Button(anx(530), any(10), anx(250), any(60), "SG_BUTTON.TGA", "Mechanika", window);

mainHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(0);
});

keysHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(1);
});

mechanicHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(2);
});

local drawHelp = GUI.Draw(anx(30), any(90), "", window);
local exitButton = GUI.Button(anx(775), any(-25), anx(50), any(50), "SG_CHECKBOX.TGA", "X", window);
local bindExit = Bind.addClick(exitButton, function() {
    hideHelpMenu();
}, PlayerGUI.HelpMenu);

function changeHelpMenu(id) {
    local text = "[#FF0000]Og�lne informacje\n[#FFFFFF]Pasek staminy odnawia si� \n";
    text += "6 razy na dzie� - 3 razy przez ��ko, 3 razy przez jedzenie.\n";
    text += "To Ty tworzysz swoj� posta� i histori� Wioski Silbach,\n";
    text += "graj, bierz udzia� w eventach i ciesz si� z \n";
    text += "dobrze przygotowanej fabularnie gry.\n";
    text += "Co jaki� czas wszyscy otrzymuj� kolejn� pul�\n";
    text += "punkt�w nauki, aby rozwija� swoje postaci.!\n";
    text += "[#FF0000]Podstawowe komendy w grze\n[#FFFFFF]\n";
    text += "[#FF0000]/test[#FFFFFF] - kostkowanie na statystyki postaci\n";
    text += "[#FF0000]/kostka[#FFFFFF] - podstawowe rzucanie ko�ci�\n";
    text += "[#FF0000]/report[#FFFFFF] - zg�oszenie incydentu administracji\n";
    text += "[#FF0000]/pw[#FFFFFF] - wiadomo�� prywatna do gracza\n";
    text += "[#FF0000]/draw[#FFFFFF] - poproszenie administracji o umieszczenie drawu\n";
    text += "[#FF0000]/sprobuj[#FFFFFF] - wylosowanie powodzenia lub nie w akcji.\n";
    text += "[#FF0000]/opis tekst[#FFFFFF] - ustaw opis, ktory beda widziec inni gracze.\n";
    text += "[#FF0000]/usun[#FFFFFF] - usuwa opis gracza.\n";
    text += "Wi�cej informacji znajdziesz na naszym discordzie: discord.gg/cUBwpqQnmj\n";



    switch(id) {
        case 1:
            text = "[#FF0000]Klawisze\n[#FFFFFF]Dost�pne klawisze i ich spos�b dzia�ania:\n";
            text += "[#FF0000]ESCAPE[#FFFFFF] - menu gry\n";
            text += "[#FF0000]B[#FFFFFF] - statystyki\n";
            text += "[#FF0000]N[#FFFFFF] - notatki\n";
            text += "[#FF0000]I/TAB[#FFFFFF] - ekwipunek\n";
            text += "[#FF0000]X[#FFFFFF] - skradanie\n";
            text += "[#FF0000]LSHIFT + myszka[#FFFFFF] - 30x handel/skrzynia\n";
            text += "[#FF0000]F5[#FFFFFF] - lista graczy\n";
            text += "[#FF0000]F6[#FFFFFF] - sie�\n";
            text += "[#FF0000]F7[#FFFFFF] - brak UI\n";
            text += "[#FF0000]UP[#FFFFFF] - Nast�pny blok chatu\n";
            text += "[#FF0000]NUMPAD 1/2/3[#FFFFFF] - <x> blok chatu\n";
            text += "[#FF0000]LWIN[#FFFFFF] - sprint\n";
            text += "[#FF0000]G/J/K/L[#FFFFFF] - interakcja z obiektem\n";
            text += "[#FF0000]LCTRL[#FFFFFF] - interakcja z graczem\n";
        break;
        case 2:
            text = "[#FF0000]Mechanika -[#FFFFFF]Kr�tki spis najwa�niejszych mechanik:\n";
            text += "[#FF0000]Craft[#FFFFFF] - dost�pny pod wieloma r�nymi obiektami.\n";
            text += "jak kowad�o, ku�nia, sto�y i wiele innych.\n";
            text += "[#FF0000]Wytrzyma�o��[#FFFFFF] - dost�pna pod dwoma wariantami.\n";
            text += "Jeden wariant obs�uguje prace i crafting. \nOdnawia si� ona 3 razy do 100% w nowym dniu gry.\n";
            text += "Sprint, kt�ry ondawia si� ca�y czas - po prostu musisz troch� odpocz�� wcze�niej.\n";
            text += "[#FF0000]Prace[#FFFFFF] - prace na serwerze to g��wne 4 ga��zie, kt�re \n";
            text += "s� najbardziej rozpowszechnione. Czyli pracowanie \n w kopalni i uzyskiwanie rud r�nych metali. \n";
            text += "Praca jako drwal i �cinanie drzew. \nPraca jako rybak i �owienie ryb oraz praca jako rolnik.\n";
            text += "Musisz pami�ta�, �e niekt�re ro�liny mo�emy posadzi� tylko na dobrym gruncie. (jako�� gruntu) \n";
            text += "[#FF0000]Chat/komunikacja[#FFFFFF] - na serwerze mamy kilka typ�w chat�w.\n";
            text += "- Chat IC, OOC i globalny. To jest dost�pne do wgl�du dla ka�dego gracza.\n";
            text += "Na chacie IC mo�emy bez u�ycia komend u�ywa� ME,DO dzi�ki # i <>.\n";
            text += "Ponad to mo�emy napisa� wi�cej ni� jedn� linijk� tekstu.\n";
            text += "Wi�cej informacji znajdziesz na naszym discordzie!\n";
        break;
    }

    drawHelp.setText(text);
}

function showHelpMenu() {
    BaseGUI.show();
    ActiveGui = PlayerGUI.HelpMenu;
    window.setVisible(true);
    changeHelpMenu(0);
}

function hideHelpMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

Bind.addKey("HELP", showHelpMenu)
Bind.addKey("HELP", hideHelpMenu, PlayerGUI.HelpMenu)
Bind.addKey(KEY_ESCAPE, hideHelpMenu, PlayerGUI.HelpMenu)