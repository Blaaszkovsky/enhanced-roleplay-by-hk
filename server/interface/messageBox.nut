
MessageBox <-
{
    draw = Draw(0,0,""),
    timer = null,

    show = function(text, y = 4000)
    {
        MessageBox.draw.text = text;
        MessageBox.draw.setPosition(4129-draw.width/2, y);
        MessageBox.draw.visible = true;

        if(MessageBox.timer != null)
            killTimer(MessageBox.timer);

        MessageBox.timer = setTimer(function() {
            MessageBox.draw.visible = false;
        }, 5000, 1);
    }

    hide = function()
    {
        if(MessageBox.timer != null)
            killTimer(MessageBox.timer);

        MessageBox.draw.visible = false;
    }
}