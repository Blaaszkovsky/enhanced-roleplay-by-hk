
local screens = [];

class BlackScreen
{
    texture = null

    leftTime = 0
    lastTime = 0

    constructor(executeTime)
    {
        texture = Texture(0,0,anx(Resolution.x),any(Resolution.y),"BLACK.TGA");
        texture.alpha = 160;
        texture.visible = true;

        if(executeTime < 500)
            executeTime = 501;

        leftTime = executeTime;
        lastTime = getTickCount();

        screens.push(this);
    }

    function render() {
        local diff = getTickCount() - lastTime;

        lastTime = getTickCount();
        leftTime = leftTime - diff;

        if(leftTime < 250)
            texture.alpha = texture.alpha - diff;
        else {
            if(texture.alpha < 255) {
                if((texture.alpha + diff * 10) > 255)
                    texture.alpha = 255;
                else
                    texture.alpha = texture.alpha + diff * 10;
            }
        }

        texture.top();

        if(leftTime <= 0)
            screens.remove(screens.find(this))
    }
}

addEventHandler("onRender", function() {
    foreach(screen in screens)
        screen.render();
})