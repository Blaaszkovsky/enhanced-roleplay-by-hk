class Building {
    static BuildingsTable = "building";

    id = -1;

    author = -1;
    date = "";

    position = null;
    rotation = null;

    targetPosition = null;
    targetRotation = null;

    progress = -1;
    scheme = -1;

    constructor(id) {
        this.id = id;

        this.author = -1;
        this.date = "";

        this.position = {x = 0, y = 0, z = 0};
        this.rotation = {x = 0, y = 0, z = 0};

        this.targetPosition = {x = 0, y = 0, z = 0};
        this.targetRotation = {x = 0, y = 0, z = 0};

        this.progress = -1;
        this.scheme = -1;
    }

    function getScheme() {
        return Config["Buildings"][scheme];
    }
}