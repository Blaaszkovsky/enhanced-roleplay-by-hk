class BuildingController {
    static buildings = {};

    static function onInit() {
        local buildingsInDatabase = Query().select().from(Building.BuildingsTable).all();

        foreach(obj in buildingsInDatabase)
        {
            local building = Building(obj.id);

            building.progress = obj.progress.tointeger();
            building.scheme = obj.scheme;
            building.author = obj.author;
            building.date = obj.date;

            building.position = {x = obj.x.tofloat(), y = obj.y.tofloat(), z = obj.z.tofloat()}
            building.rotation = {x = obj.rotX.tofloat(), y = obj.rotY.tofloat(), z = obj.rotZ.tofloat()}

            building.targetPosition = {x = obj.targetX.tofloat(), y = obj.targetY.tofloat(), z = obj.targetZ.tofloat()}
            building.targetRotation = {x = obj.targetRotX.tofloat(), y = obj.targetRotY.tofloat(), z = obj.targetRotZ.tofloat()}

            buildings[building.id] <- building;
        }

        print("We have " + buildings.len() + " existing buildings.");
    }

    static function onPlayerJoin(playerId) {
        foreach(building in buildings)
            BuildingPacket(building).create(playerId);
    }

    static function completeBuilding(id) {
        if(!(id in buildings))
            return;

        local obj = getBuilding(id);
        local vobInfo = obj.getScheme().vobInfo;

        Query().insertInto(GameVob.VobTable, ["animationId", "animationTime", "triggerId", "triggerType", "triggerAnimation", "triggerTime", "hpMode", "world", "hp", "hpMax", "movable", "alpha", "dynamic", "weight", "visual", "name",
            "positionX", "positionY", "positionZ", "rotationX", "rotationY", "rotationZ","targetPositionX", "targetPositionY",
            "targetPositionZ", "targetRotationX", "targetRotationY", "targetRotationZ", "items", "guid", "complexity", "type", "createdBy"],
        [
            0, 0, 0, 0, 0, 1000, vobInfo.hpMode, "'"+getServerWorld()+"'", vobInfo.hp, vobInfo.hp, vobInfo.movable, 1.0, 0, 0, "'"+obj.getScheme().model+"'", "'"+obj.author+"-"+obj.scheme+"'",
            obj.position.x, obj.position.y, obj.position.z, obj.rotation.x, obj.rotation.y, obj.rotation.z, obj.targetPosition.x, obj.targetPosition.y, obj.targetPosition.z, obj.targetRotation.x, obj.targetRotation.y, obj.targetRotation.z,
            "''", "''", 3, vobInfo.type, "'"+obj.author+"'"
        ]).execute();

        local vob = Query().select().from(GameVob.VobTable).where(["createdBy = '"+obj.author+"'"]).orderBy("id DESC").one();
        if(vob == null)
            return;

        local newVob = GameVob(vob["id"].tointeger())

        newVob.trigger = vob.triggerId.tointeger();
        newVob.triggerType = vob.triggerType.tointeger();
        newVob.triggerAnimation = vob.triggerAnimation.tointeger();
        newVob.triggerTime = vob.triggerTime.tointeger();

        newVob.hpMode = vob.hpMode.tointeger();
        newVob.hp = vob.hp.tointeger();
        newVob.hpMax = vob.hpMax.tointeger();
        newVob.opened = vob.opened.tointeger();

        newVob.movable = vob.movable.tointeger();

        newVob.alpha = vob.alpha.tofloat();
        newVob.dynamic = vob.dynamic.tointeger() == 1 ? true : false;
        newVob.visual = vob.visual;
        newVob.name = vob.name;

        newVob.type = vob.type.tointeger();
        newVob.weight = vob.weight.tofloat();
        newVob.guid = vob.guid;
        newVob.complexity = vob.complexity;
        newVob.createdBy = vob.createdBy;

        if(newVob.type == VobType.Container) {
            newVob.inventory = item.Controller.createBucket(ItemRelationType.Vob, newVob.id);
            newVob.inventory.maxWeight = newVob.weight;
        }

        newVob.position = {x = vob.positionX.tofloat(), y = vob.positionY.tofloat(), z = vob.positionZ.tofloat()}
        newVob.rotation = {x = vob.rotationX.tofloat(), y = vob.rotationY.tofloat(), z = vob.rotationZ.tofloat()}

        newVob.targetPosition = {x = vob.targetPositionX.tofloat(), y = vob.targetPositionY.tofloat(), z = vob.targetPositionZ.tofloat()}
        newVob.targetRotation = {x = vob.targetRotationX.tofloat(), y = vob.targetRotationY.tofloat(), z = vob.targetRotationZ.tofloat()}

        VobController.vobs[newVob.id] <- newVob;

        vob.animationId = vob.animationId.tointeger();
        if(vob.animationId != 0)
            System.Effect.add(newVob.id, SystemEffectTarget.Vob, vob.animationId, vob.animationTime.tointeger());

        VobPacket(newVob).sendCreateToAll();

        Query().deleteFrom(Building.BuildingsTable).where(["id = "+id]).execute();
        BuildingPacket(obj).deleteBuilding();
        buildings.rawdelete(id);
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BuildingPackets.Create:
                local carpenterSkill = getPlayerSkill(playerId, PlayerSkill.Carpenter);
                local scheme = packet.readString();

                if(!(scheme in Config["Buildings"]))
                    return;

                local obj = Config["Buildings"][scheme];
                if(obj.skill > carpenterSkill) {
                    addPlayerNotification(playerId, "Zbyt niski poziom umiej�tno�ci.");
                    return;
                }

                foreach(item in obj.materials) {
                    if(hasPlayerItem(playerId, item[0]) < item[1]) {
                        addPlayerNotification(playerId, "Nie masz potrzebnych przedmiot�w.");
                        return;
                    }
                }

                local dateObject = date();
                local dateFormat = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;
                local world = getPlayerWorld(playerId);
                local playerObject = getPlayer(playerId);

                Query().insertInto(Building.BuildingsTable, ["scheme", "author", "date", "world", "x", "y", "z", "rotX", "rotY", "rotZ", "targetX", "targetY", "targetZ", "targetRotX", "targetRotY", "targetRotZ"], [
                    "'"+scheme+"'","'"+playerObject.name+"'", "'"+dateFormat+"'", "'"+world+"'", packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readFloat()
                ]).execute();

                local bobj = Query().select().from(Building.BuildingsTable).where(["date = '"+dateFormat+"'"]).orderBy("id DESC").one();
                if(bobj == null){
                    addPlayerNotification(playerId, "Error w trakcie robienia budynku.");
                    throw "Error w trakcie robienia budynku.";
                }

                addPlayerNotification(playerId, "Utworzono budynek.");
                foreach(item in obj.materials)
                    removeItem(playerId, item[0], item[1]);

                local building = Building(bobj.id);

                building.progress = bobj.progress.tointeger();
                building.scheme = bobj.scheme;
                building.author = bobj.author;
                building.date = bobj.date;

                building.position = {x = bobj.x.tofloat(), y = bobj.y.tofloat(), z = bobj.z.tofloat()}
                building.rotation = {x = bobj.rotX.tofloat(), y = bobj.rotY.tofloat(), z = bobj.rotZ.tofloat()}

                building.targetPosition = {x = bobj.targetX.tofloat(), y = bobj.targetY.tofloat(), z = bobj.targetZ.tofloat()}
                building.targetRotation = {x = bobj.targetRotX.tofloat(), y = bobj.targetRotY.tofloat(), z = bobj.targetRotZ.tofloat()}

                buildings[building.id] <- building;

                BuildingPacket(building).create();
            break;
            case BuildingPackets.Build:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                local obj = getBuilding(id);

                if(getPlayerStamina(playerId) < 5.0) {
                    addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
                    return;
                }

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.0);
                obj.progress = obj.progress + 1;
                if(obj.progress >= obj.getScheme().work)
                    completeBuilding(obj.id);
                else
                    BuildingPacket(obj).build();
            break;
            case BuildingPackets.Hit:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                local obj = getBuilding(id);

                if(getPlayerStamina(playerId) < 5.0) {
                    addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
                    return;
                }

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.0);
                obj.progress = obj.progress - 1;
                if(obj.progress <= 0) {
                    BuildingPacket(obj).deleteBuilding();
                    buildings.rawdelete(id);
                }else
                    BuildingPacket(obj).hit();
            break;
            case BuildingPackets.Delete:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                Query().deleteFrom(Building.BuildingsTable).where(["id = "+id]).execute();
                BuildingPacket(buildings[id]).deleteBuilding();
                buildings.rawdelete(id);
            break;
        }
    }
}

getBuilding <- @(id) BuildingController.buildings[id];
getBuildings <- @() BuildingController.buildings;

addEventHandler("onInit", BuildingController.onInit.bindenv(BuildingController));
addEventHandler("onPlayerJoin", BuildingController.onPlayerJoin.bindenv(BuildingController));
RegisterPacketReceiver(Packets.Building, BuildingController.onPacket.bindenv(BuildingController));
