
class BuildingPacket
{
    packet = null;
    object = null;

    constructor(bObject)
    {
        packet = ExtendedPacket(Packets.Building);
        object = bObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(BuildingPackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.author);
        packet.writeString(object.date);
        packet.writeString(object.scheme);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeInt16(object.progress);

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function build() {
        packet.writeUInt8(BuildingPackets.Build)
        packet.writeInt32(object.id);
        packet.writeInt16(object.progress);
        packet.sendToAll(RELIABLE);
    }

    function hit() {
        packet.writeUInt8(BuildingPackets.Hit)
        packet.writeInt32(object.id);
        packet.writeInt16(object.progress);
        packet.sendToAll(RELIABLE);
    }

    function deleteBuilding() {
        packet.writeUInt8(BuildingPackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}