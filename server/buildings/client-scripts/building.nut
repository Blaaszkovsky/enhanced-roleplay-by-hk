class Building {
    id = -1;

    author = -1;
    date = "";

    position = -1;
    rotation = -1;

    done = false;
    progress = -1;
    scheme = -1;

    vobs = null;
    observer = null;
    draw = null;

    constructor(id) {
        this.id = id;

        this.author = -1;
        this.date = "";

        this.position = {x = 0, y = 0, z = 0};
        this.rotation = {x = 0, y = 0, z = 0};

        this.done = false;
        this.progress = -1;
        this.scheme = "";

        this.vobs = [];
        this.draw = null;
        this.observer = null;
    }

    function create() {
        foreach(construct in getScheme().construction) {
            local vob = Vob("ADDON_MISC_WOODPILLAR_M01.3DS");
            vob.cdDynamic = true;
            vob.cdStatic = true;
            vob.setPosition(position.x + construct[0], position.y + construct[1], position.z + construct[2])
            vob.setRotation(rotation.x + construct[3], rotation.y + construct[4], rotation.z + construct[5])
            vob.addToWorld();
            vobs.push(vob);
        }

        draw = WorldInterface.Draw3d(position.x,position.y + 109.40,position.z);
        draw.addLine("Budowanie: "+progress+"/"+getScheme().work);
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        draw.distance = 800;
        draw.setScale(0.4, 0.4);
        draw.visible = true;

        observer = InteractionTree.addDistanceObserver(position.x,position.y + 109.40,position.z, 300, "Budowla "+scheme, function() {BuildingController.interactWithBuilding(); });
    }

    function updateDrawText() {
        draw.setLineText(0, "Budowanie: "+progress+"/"+getScheme().work);
    }

    function hit(newProgress) {
        progress = newProgress;
        updateDrawText();
        draw.setLineColor(0, 200, 71, 38);
    }

    function build(newProgress) {
        progress = newProgress;
        updateDrawText();
        draw.setLineColor(0, 0, 231, 68);
    }

    function remove() {
        InteractionTree.removeDistanceObserver(observer);
        draw.remove();
    }

    function checkDistance(x,y,z) {
        return getDistance3d(position.x, position.y, position.z, x, y, z);
    }

    function getScheme() {
        return Config["Buildings"][scheme];
    }
}