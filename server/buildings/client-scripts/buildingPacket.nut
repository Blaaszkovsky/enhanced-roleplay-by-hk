
class BuildingPacket
{
    packet = null;
    object = null;

    constructor(bObject)
    {
        packet = ExtendedPacket(Packets.Building);
        object = bObject;
    }

    function build() {
        packet.writeUInt8(BuildingPackets.Build)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function hit() {
        packet.writeUInt8(BuildingPackets.Hit)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function deleteBuilding() {
        packet.writeUInt8(BuildingPackets.Delete)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }
}