class BuildingController {
    static buildings = {};

    static function interactWithBuilding() {
        local pos = getPlayerPosition(heroId);
        local possibleBuilding = getNearestBuilding(pos.x,pos.y,pos.z);

        if(possibleBuilding == null)
            return;

        BuildingInterface.show(possibleBuilding.id);
    }

    static function getNearestBuilding(posx, posy, posz) {
        local closestBuilding = null;
        foreach(building in buildings) {
            if(building.checkDistance(posx, posy, posz) < 300) {
                closestBuilding = building;
                break;
            }
        }

        return closestBuilding;
    }

    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BuildingPackets.Build:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                buildings[id].build(packet.readInt16());
            break;
            case BuildingPackets.Create:
                local building = Building(packet.readInt32());

                building.author = packet.readString();
                building.date = packet.readString();
                building.scheme = packet.readString();

                building.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                building.rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}

                building.progress = packet.readInt16();
                building.create();

                buildings[building.id] <- building;
            break;
            case BuildingPackets.Hit:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                buildings[id].hit(packet.readInt16());
            break;
            case BuildingPackets.Delete:
                local id = packet.readInt32();
                if(!(id in buildings))
                    return;

                buildings[id].remove();
                buildings.rawdelete(id);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Building, BuildingController.onPacket.bindenv(BuildingController));

getBuilding <- @(id) BuildingController.buildings[id];
getBuildings <- @() BuildingController.buildings;
