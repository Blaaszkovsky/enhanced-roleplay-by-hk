
BuildingBuilderInterface <- {
    bucket = null,
    eggTimer = null,
    activeKeys = [],
    vob = null,
    secondaryVob = null,
    lock = false

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(50), any(Resolution.y/2 - 300), anx(400), any(560), "SG_BOX.TGA", null, false);
        result.scrollbarList <- GUI.ScrollBar(anx(360), any(50), anx(30), any(480), "SG_SCROLL.TGA", "SG_BUTTON.TGA", "SG_U.TGA", "SG_O.TGA", Orientation.Vertical, result.window);
        result.listContent <- [];

        result.extraDraws <- [];
        result.makeButton <- GUI.Button(anx(Resolution.x - 300), any(Resolution.y/2 + 200), anx(200), any(60), "SG_BUTTON.TGA", "Enter - Utw�rz");
        
        result.bindForScrollbar <- Bind.onChange(result.scrollbarList, function(value) {
            BuildingBuilderInterface.refreshList();
        }, PlayerGUI.BuildingBuilder);

        return result;
    }

    show = function()
    {
        if(bucket == null)
            bucket = prepareWindow();

        BaseGUI.show();
        ActiveGui = PlayerGUI.BuildingBuilder;
        bucket.window.setVisible(true);
        refreshList();
    }

    hide = function()
    {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
        bucket.makeButton.setVisible(false);

        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();
        bucket.extraDraws = [];
        activeKeys = [];

        playAni(heroId,"S_RUN");
        resetEggTimer();

        eggTimer = null;
        vob = null;
        secondaryVob = null;
        lock = false;
    }

    refreshList = function()
    {
        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();

        local visibleVobs = 0;
        local _index = -1;
        local vobs = Config["Buildings"];
        local value = bucket.scrollbarList.getValue();
        value = value * 2;

        foreach(scheme,object in vobs) {
            _index = _index + 1;

            if(_index < value)
                continue;

            if(visibleVobs >= 6)
                break;

            bucket.listContent.push(BuildingBuilderInterface.Object(object, scheme, visibleVobs));
            visibleVobs = visibleVobs + 1;
        }

        foreach(vobItem in bucket.listContent)
            vobItem.setVisible(true);
    }

    preview = function(scheme) {
        local pos = getPlayerPosition(heroId);
        local angle = getPlayerAngle(heroId);
        vob = Vob(Config["Buildings"][scheme].model);
        vob.setPosition(pos.x + 75.0, pos.y, pos.z + 37.50);
        vob.setRotation(0,angle,0);
        vob.cdDynamic = false;
        vob.cdStatic = false;
        vob.addToWorld();
        bucket.makeButton.setVisible(true);
        bucket.makeButton.attribute = scheme;

        bucket.extraDraws = [];
        bucket.extraDraws.push(Draw(0,0,scheme));
        local obj = Config["Buildings"][scheme];
        bucket.extraDraws.push(Draw(0,0,"Praca: "+obj.work));
        bucket.extraDraws.push(Draw(0,0,"Umiej�tno��: "+obj.skill));
        foreach(material in obj.materials)
            bucket.extraDraws.push(Draw(0,0,getItemName(material[0]) + " - "+material[1]));

        foreach(index, draw in bucket.extraDraws) {
            if(getSetting("OtherCustomFont")) {
                draw.font = "FONT_OLD_20_WHITE_HI.TGA";
                draw.setScale(0.45, 0.45);
            }

            draw.setPosition(anx(Resolution.x - 300), any(Resolution.y/2 - (400 - ((bucket.extraDraws[0].heightPx + 10) * index))));
            draw.visible = true;
        }
    }

    build = function(){
         local pos = getPlayerPosition(heroId);
        if(getPositionDifference(pos, vob.getPosition()) > 1500) {
            hide();
            return;
        }

        local pos = vob.getPosition();
        local rot = vob.getRotation();

        local obj = Config["Buildings"][bucket.makeButton.attribute];
        if(obj.vobInfo.type == VobType.Door) {
            if(secondaryVob == null) {
                secondaryVob = Vob(obj.model);
                secondaryVob.setPosition(pos.x, pos.y, pos.z);
                secondaryVob.setRotation(rot.x, rot.y, rot.z);
                secondaryVob.cdDynamic = false;
                secondaryVob.cdStatic = false;
                secondaryVob.addToWorld();

                vob.setPosition(pos.x + 30, pos.y, pos.z + 30);
                bucket.extraDraws.push(Draw(0,0,"Ustawiasz otwart� form� obiektu."));
                bucket.extraDraws[bucket.extraDraws.len() -1].setPosition(anx(Resolution.x/2 - 100), any(Resolution.y/2 + 200));
                bucket.extraDraws[bucket.extraDraws.len() -1].visible = true;
            } else {
                local opos = secondaryVob.getPosition();
                local orot = secondaryVob.getRotation();

                local packet = ExtendedPacket(Packets.Building);
                packet.writeUInt8(BuildingPackets.Create);
                packet.writeString(bucket.makeButton.attribute);
                packet.writeFloat(opos.x);
                packet.writeFloat(opos.y);
                packet.writeFloat(opos.z);
                packet.writeFloat(orot.x);
                packet.writeFloat(orot.y);
                packet.writeFloat(orot.z);
                packet.writeFloat(pos.x);
                packet.writeFloat(pos.y);
                packet.writeFloat(pos.z);
                packet.writeFloat(rot.x);
                packet.writeFloat(rot.y);
                packet.writeFloat(rot.z);
                packet.send(RELIABLE);

                BuildingBuilderInterface.eggTimer = EggTimer(5000);
                lock = true;
                playAni(heroId, "S_REPAIR_S1");
                BuildingBuilderInterface.bucket.window.setVisible(false);
                BuildingBuilderInterface.eggTimer.onEnd = function(eleobjectment) {
                    BuildingBuilderInterface.hide();
                }
            }
        }else {
            local packet = ExtendedPacket(Packets.Building);
            packet.writeUInt8(BuildingPackets.Create);
            packet.writeString(bucket.makeButton.attribute);
            packet.writeFloat(pos.x);
            packet.writeFloat(pos.y);
            packet.writeFloat(pos.z);
            packet.writeFloat(rot.x);
            packet.writeFloat(rot.y);
            packet.writeFloat(rot.z);
            packet.writeFloat(pos.x);
            packet.writeFloat(pos.y);
            packet.writeFloat(pos.z);
            packet.writeFloat(rot.x);
            packet.writeFloat(rot.y);
            packet.writeFloat(rot.z);
            packet.send(RELIABLE);

            BuildingBuilderInterface.eggTimer = EggTimer(5000);
            lock = true;
            playAni(heroId, "S_REPAIR_S1");
            BuildingBuilderInterface.bucket.window.setVisible(false);
            BuildingBuilderInterface.eggTimer.onEnd = function(eleobjectment) {
                BuildingBuilderInterface.hide();
            }
        }
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.BuildingBuilder)
            hide();
    }

    onKey = function(key)
    {
        if(lock){
            return;
        }
        switch(key) {
            case KEY_NUMPAD5: case KEY_NUMPAD8: case KEY_NUMPAD4: case KEY_NUMPAD6: case KEY_NUMPAD7: case KEY_NUMPAD9: case KEY_E: case KEY_Q: case KEY_DOWN: case KEY_UP: case KEY_LEFT: case KEY_RIGHT: case KEY_A: case KEY_D: case KEY_W: case KEY_S:
                activeKeys.append(key);
            break;
            case KEY_RETURN:
               build();
            break;
        }
    }

    onClick = function(object) {
        if(ActiveGui != PlayerGUI.BuildingBuilder)
            return;

        foreach(item in bucket.listContent) {
            if(item.background == object) {
                preview(item.background.attribute);
                return;
            }
        }
    }

    onRender = function() {
        local speed = 1;

        if(isKeyPressed(KEY_LSHIFT))
            speed = 10;

        if(vob == null)
            activeKeys = [];

        foreach(index, key in activeKeys)
        {
            if(!isKeyPressed(key))
            {
                activeKeys.remove(index);
                continue;
            }

            local position = vob.getPosition();
            local rotation = vob.getRotation();

            switch(key)
            {
                case KEY_DOWN: case KEY_S:
                    position.x = position.x - (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z - (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_UP: case KEY_W:
                    position.x = position.x + (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_LEFT: case KEY_A:
                    position.x = position.x + (sin(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_RIGHT: case KEY_D:
                    position.x = position.x + (sin(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_Q:
                    position.y = position.y + speed;
                break;
                case KEY_E:
                    position.y = position.y - speed;
                break;
                case KEY_NUMPAD4:
                    rotation.y = rotation.y + speed;
                break;
                case KEY_NUMPAD6:
                    rotation.y = rotation.y - speed;
                break;
                case KEY_NUMPAD5:
                    rotation.x = rotation.x + speed;
                break;
                case KEY_NUMPAD8:
                    rotation.x = rotation.x - speed;
                break;
                case KEY_NUMPAD7:
                    rotation.z = rotation.z + speed;
                break;
                case KEY_NUMPAD9:
                    rotation.z = rotation.z - speed;
                break;
            }

            vob.setPosition(position.x, position.y, position.z);
            vob.setRotation(rotation.x, rotation.y, rotation.z);
        }
    }
}

addEventHandler("onForceCloseGUI", BuildingBuilderInterface.onForceCloseGUI.bindenv(BuildingBuilderInterface));
addEventHandler("GUI.onClick", BuildingBuilderInterface.onClick.bindenv(BuildingBuilderInterface));
addEventHandler("onKey", function(key) {
    if(ActiveGui == PlayerGUI.BuildingBuilder)
        BuildingBuilderInterface.onKey(key);
})
addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.BuildingBuilder)
        BuildingBuilderInterface.onRender();
})
Bind.addKey(KEY_ESCAPE, BuildingBuilderInterface.hide.bindenv(BuildingBuilderInterface), PlayerGUI.BuildingBuilder)

class BuildingBuilderInterface.Object {
    index = null
    background = null;
    render = null;
    name = null;

    constructor(object, scheme, index) {
        local pos = {x = 0, y = 0}
        for(local i = 0; i < index; i ++) {
            pos.x = pos.x + 1;
            if(pos.x >= 2) {
                pos.x = 0
                pos.y = pos.y + 1;
            }
        }

        background = GUI.Button(anx(50) + anx(pos.x * 175), any(Resolution.y/2 - 300) + any(175 * pos.y), anx(175), any(175), "SG_BUTTON.TGA", "");
        background.attribute = scheme
        local backgroundPosition = background.getPosition();
        render = ItemRender(backgroundPosition.x + anx(25), backgroundPosition.y + any(10), anx(150), any(150), "");
        render.visual = object.model;

        name = Draw(backgroundPosition.x + 20, backgroundPosition.y + any(150), scheme);
        if(name.text.len() > 15) {
            name.text = name.text.slice(0, 15) + "...";
        }

        name.font = "FONT_OLD_20_WHITE_HI.TGA";
        name.setScale(0.5, 0.5);
        name.setColor(255, 248, 196);
    }

    function setVisible(value) {
        background.setVisible(value);
        render.visible = value;
        name.visible = value;
    }

    function remove() {
        background.setVisible(false);
        background.destroy();
        name = null;
        render = null;
    }
}