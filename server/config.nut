Config <- {};

// General

Config["MaxStamineRecoveredByInteraction"] <- 600
Config["MaxStamineRecoveredByFood"] <- 300

Config["MinuteSeconds"] <- 5;

// Chat

Config["ChatAnimation"] <- true;
Config["ChatHistorySize"] <- 100;
Config["ChatDefaultLines"] <- 10;
Config["ChatScreen"] <- 70;
Config["ChatLineMultiplier"] <- 1.0;

Config["ChatLabel"] <- {}
Config["ChatLabel"][ChatType.OOC] <- "OOC"
Config["ChatLabel"][ChatType.ALL] <- "ALL"
Config["ChatLabel"][ChatType.IC] <- "IC"
Config["ChatLabel"][ChatType.REPORT] <- "REP"
Config["ChatLabel"][ChatType.ADMIN] <- "ADM"

Config["ChatMethod"] <- {}
Config["ChatMethod"][ChatMethods.Normal] <- {
	distance = 800,
	r = 255,
	g = 255,
	b = 255
}
Config["ChatMethod"][ChatMethods.Global] <- {
	distance = -1,
	r = 200,
	g = 150,
	b = 150
}
Config["ChatMethod"][ChatMethods.OutOfCharacter] <- {
	distance = 800,
	r = 0,
	g = 200,
	b = 255
}
Config["ChatMethod"][ChatMethods.Whisper] <- {
	distance = 155,
	r = 150,
	g = 150,
	b = 160
}
Config["ChatMethod"][ChatMethods.Shout] <- {
	distance = 3000,
	r = 250,
	g = 150,
	b = 150
}
Config["ChatMethod"][ChatMethods.Enviroment] <- {
	distance = 800,
	r = 125,
	g = 200,
	b = 125
}
Config["ChatMethod"][ChatMethods.Action] <- {
	distance = 800,
	r = 100,
	g = 150,
	b = 200
}
Config["ChatMethod"][ChatMethods.Mumble] <- {
	distance = 400,
	r = 193,
	g = 190,
	b = 190
}
Config["ChatMethod"][ChatMethods.LoudTalk] <- {
	distance = 1500,
	r = 250,
	g = 150,
	b = 150
}

Config["AdminConfirms"] <- {
	// "userName": "password",
}

Config["WalkStyles"] <- [{
		name = "Normalny",
		idx = PlayerWalk.Normal
	},
	{
		name = "Zm�czony",
		idx = PlayerWalk.Tired
	},
	{
		name = "Kobieta",
		idx = PlayerWalk.Woman
	},
	{
		name = "Wojsko",
		idx = PlayerWalk.Army
	},
	{
		name = "Wyluzowany",
		idx = PlayerWalk.Lazy
	},
	{
		name = "Arogancki",
		idx = PlayerWalk.Arrogant
	},
	{
		name = "Mag",
		idx = PlayerWalk.Mage
	},
	{
		name = "Z�odziej",
		idx = PlayerWalk.Pockets
	},
	{
		name = "Zm�czony v2",
		idx = PlayerWalk.OldTired
	},
	{
		name = "Arogancki v2",
		idx = PlayerWalk.OldArrogant
	},
	{
		name = "Mag v2",
		idx = PlayerWalk.OldMage
	},
	{
		name = "Wyluzowany v2",
		idx = PlayerWalk.OldLazy
	},
	{
		name = "Ben",
		idx = PlayerWalk.HurtBen
	},
	{
		name = "Leader",
		idx = PlayerWalk.Leader
	},
	{
		name = "Seller",
		idx = PlayerWalk.Seller
	},
	{
		name = "Stomachhurt",
		idx = PlayerWalk.Stomachhurt
	},
	{
		name = "Waitress",
		idx = PlayerWalk.Waitress
	},
	{
		name = "Crate",
		idx = PlayerWalk.Crate
	},
	{
		name = "Poisoned",
		idx = PlayerWalk.Poisoned
	},
	{
		name = "Hurt",
		idx = PlayerWalk.Hurt
	},
	{
		name = "Pocketwalk",
		idx = PlayerWalk.PocketWalk
	},
]

Config["headModels"] <- [{
		name = "Tusty �ysy",
		asc = "Hum_Head_FatBald"
	},
	{
		name = "Wojowniczy",
		asc = "Hum_Head_Fighter"
	},
	{
		name = "Kucyk",
		asc = "Hum_Head_Pony"
	},
	{
		name = "Bez kucyka",
		asc = "HUM_HEAD_WITHOUTPONY"
	},
	{
		name = "Bez kucyka alt",
		asc = "HUM_HEAD_MARVIN"
	},
	{
		name = "�ysy",
		asc = "Hum_Head_Bald"
	},
	{
		name = "Z�odziej",
		asc = "Hum_Head_Thief"
	},
	{
		name = "Z�o�liwy",
		asc = "Hum_Head_Psionic"
	},
	{
		name = "Z�o�liwy z kucykiem",
		asc = "HUM_HEAD_PSIONIC_PONY"
	},
	{
		name = "Zm�czony",
		asc = "ASC_HUM_HEAD_TIRED"
	},
	{
		name = "Wysoki",
		asc = "ASC_HUM_HEAD_TALL"
	},
	{
		name = "Gruby",
		asc = "ASC_HUM_HEAD_FAT"
	},
	{
		name = "Z�y",
		asc = "ASC_HUM_HEAD_ANGRY"
	},
	{
		name = "Broda du�a",
		asc = "HUM_HEAD_BrodaDuza"
	},
	{
		name = "Broda ma�a",
		asc = "HUM_HEAD_BrodaMala"
	},
	{
		name = "Broda �rednia",
		asc = "HUM_HEAD_BrodaSrednia"
	},
	{
		name = "Broda + w�s",
		asc = "HUM_HEAD_BrodaWas"
	},
	{
		name = "Broda alter",
		asc = "HUM_HEAD_BEARD2"
	},
	{
		name = "Broda + kucyk",
		asc = "HUM_HEAD_PONYBEARD"
	},
	{
		name = "Boki wygolone",
		asc = "HUM_HEAD_SIDEBURNS"
	},
	{
		name = "Broda altern",
		asc = "HUM_HEAD_BEARD4"
	},
	{
		name = "Flail",
		asc = "HUM_HEAD_FLAILNEU"
	},
	{
		name = "Flex",
		asc = "HUM_HEAD_FLEXNEU"
	},
	{
		name = "Lutter",
		asc = "HUM_HEAD_LUTTERNEU"
	},
	{
		name = "Pfeiffer",
		asc = "HUM_HEAD_PFEIFFERNEU"
	},
	{
		name = "Pymonte",
		asc = "HUM_HEAD_PYMONTENEU"
	},
	{
		name = "Thomas",
		asc = "HUM_HEAD_THOMASNEU"
	},
	{
		name = "Unicorn",
		asc = "HUM_HEAD_UNICORNNEU"
	},
	{
		name = "Nerd",
		asc = "ASC_HUM_HEAD_NERD"
	},
	{
		name = "Dredy",
		asc = "ASC_HUM_HEAD_DRED"
	},
	{
		name = "Maga",
		asc = "ASC_HUM_HEAD_MAGE"
	},
	{
		name = "Monk",
		asc = "ASC_HUM_HEAD_MONK"
	},
	{
		name = "Segmenty",
		asc = "ASC_HUM_HEAD_SEGMENTS"
	},
	{
		name = "Nordycki 1",
		asc = "ASC_HUM_HEAD_NORD"
	},
	{
		name = "Nordycki 2",
		asc = "ASC_HUM_HEAD_NORD2"
	},
	{
		name = "Gruby irokez",
		asc = "ASC_HUM_HEAD_FATIROKEZ"
	},
	{
		name = "Gruby + w�s",
		asc = "ASC_HUM_HEAD_FATMUSTACHE"
	},
	{
		name = "Gruby + broda",
		asc = "ASC_HUM_HEAD_FATBEARD"
	},
	{
		name = "Pirat",
		asc = "ASC_HUM_HEAD_PIRATE"
	},
	{
		name = "Pirat2",
		asc = "ASC_HUM_HEAD_PIRATE2"
	},
	{
		name = "D�ugie w�osy",
		asc = "HUM_HEAD_LONGHAIR"
	},
	{
		name = "Kobieta 1",
		asc = "HUM_HEAD_BABE12"
	},
	{
		name = "Kobieta 2",
		asc = "HUM_HEAD_BABE1"
	},
	{
		name = "Kobieta 3",
		asc = "HUM_HEAD_BABE2"
	},
	{
		name = "Kobieta 4",
		asc = "HUM_HEAD_BABE3"
	},
	{
		name = "Kobieta 5",
		asc = "HUM_HEAD_BABE4"
	},
	{
		name = "Kobieta 7",
		asc = "HUM_HEAD_BABE6"
	},
	{
		name = "Kobieta 8",
		asc = "HUM_HEAD_BABE7"
	},
	{
		name = "Kobieta 9",
		asc = "HUM_HEAD_BABE8"
	},
	{
		name = "Kobieta 10",
		asc = "HUM_HEAD_BABEHAIR"
	},
	{
		name = "Kobieta 11",
		asc = "HUM_HEAD_BABE"
	},
	{
		name = "Kobieta 12",
		asc = "HUM_HEAD_IVY"
	},
]

Config["DynamicBindings"] <- {
	"SPRINT": {
		defined = 219,
		writable = true,
		label = "Sprint",
	},
	"EQUIPMENT": {
		defined = 23,
		writable = true,
		label = "Ekwipunek",
	},
	"EQUIPMENT_ADDITIONAL": {
		defined = 15,
		writable = false,
		label = "Ewkipunek gwarant."
	},
	"DESCRIPTION": {
		defined = 50,
		writable = true,
		label = "Opis targetu",
	},
	"STATISTICS": {
		defined = 48,
		writable = true,
		label = "Statystyki",
	},
	"NOTES": {
		defined = 49,
		writable = true,
		label = "Notatki",
	},
	"POSSIBLE_ONE": {
		defined = 34,
		writable = true,
		label = "Klawisz funkcyjny 1",
	},
	"POSSIBLE_TWO": {
		defined = 36,
		writable = true,
		label = "Klawisz funkcyjny 2",
	},
	"POSSIBLE_THREE": {
		defined = 37,
		writable = true,
		label = "Klawisz funkcyjny 3",
	},
	"POSSIBLE_FOUR": {
		defined = 38,
		writable = true,
		label = "Klawisz funkcyjny 4",
	},
	"CALENDAR": {
		defined = 24,
		writable = true,
		label = "Kalendarz",
	},
	"HELP": {
		defined = 35,
		writable = true,
		label = "Pomoc",
	},
}

// Forest

Config["ForestSystem"] <- {}

Config["ForestSystem"]["Dab"] <- {
	time = 300,
	vob = [
		[0, "OW_LOB_BUSH_V2.3DS"],
		[225, "OW_LOB_BUSH_V7.3DS"],
		[225, "NW_NATURE_BIGTREE_356P.3DS"],
	],
	outcome = [{
			chance = 80,
			skill = 0,
			instance = "ITCR_SDRP_DROP_16",
			amount = 1
		},
		{
			chance = 80,
			skill = 0,
			instance = "ITCR_SDRP_DROP_14",
			amount = 1
		},
		{
			chance = 90,
			skill = 1,
			instance = "ITCR_SDRP_DROP_16",
			amount = 1
		},
		{
			chance = 90,
			skill = 1,
			instance = "ITCR_SDRP_DROP_14",
			amount = 1
		},
		{
			chance = 70,
			skill = 2,
			instance = "ITCR_SDRP_DROP_16",
			amount = 1
		},
		{
			chance = 80,
			skill = 2,
			instance = "ITCR_SDRP_DROP_14",
			amount = 2
		},
		{
			chance = 80,
			skill = 3,
			instance = "ITCR_SDRP_DROP_16",
			amount = 1
		},
		{
			chance = 80,
			skill = 3,
			instance = "ITCR_SDRP_DROP_14",
			amount = 3
		},
		{
			chance = 80,
			skill = 4,
			instance = "ITCR_SDRP_DROP_14",
			amount = 4
		},
		{
			chance = 80,
			skill = 4,
			instance = "ITCR_SDRP_DROP_16",
			amount = 2
		},
		{
			chance = 1,
			skill = 4,
			instance = "ITCR_SDRP_DROP_86",
			amount = 1
		},
		{
			chance = 1,
			skill = 4,
			instance = "ITCR_SDRP_DROP_86",
			amount = 2
		},
		{
			chance = 0.5,
			skill = 4,
			instance = "ITCR_SDRP_INGR_33",
			amount = 1
		},
		{
			chance = 0.5,
			skill = 4,
			instance = "ITCR_SDRP_INGR_33",
			amount = 2
		},
		{
			chance = 80,
			skill = 5,
			instance = "ITCR_SDRP_DROP_14",
			amount = 5
		},
		{
			chance = 80,
			skill = 5,
			instance = "ITCR_SDRP_DROP_16",
			amount = 3
		},
		{
			chance = 1,
			skill = 5,
			instance = "ITCR_SDRP_DROP_86",
			amount = 2
		},
		{
			chance = 1,
			skill = 5,
			instance = "ITCR_SDRP_DROP_86",
			amount = 4
		},
		{
			chance = 1,
			skill = 5,
			instance = "ITCR_SDRP_INGR_33",
			amount = 1
		},
		{
			chance = 1,
			skill = 5,
			instance = "ITCR_SDRP_INGR_33",
			amount = 2
		},
	]
}

Config["FishingSystem"] <- {}

Config["FishingSystem"]["Rybak T0"] <- {
	refresh = 600,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 20,
			skill = 0,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�
		{
			chance = 45,
			skill = 0,
			instance = "itfo_SDRP_food_30",
			amount = 0
		}, //miss
		{
			chance = 35,
			skill = 0,
			instance = "ITPL_WEED",
			amount = 1
		}, //chwast
		{
			chance = 40,
			skill = 1,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 25,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�
		{
			chance = 25,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania
		{
			chance = 1,
			skill = 1,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia
		//T2
		{
			chance = 23,
			skill = 2,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�
		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot
		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik
		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk
		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin
		{
			chance = 1,
			skill = 2,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia
		{
			chance = 1,
			skill = 2,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia
		{
			chance = 1,
			skill = 2,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia
		//T3
		{
			chance = 13,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�
		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik
		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 3,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 5,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia
	]
}

Config["FishingSystem"]["Morskie T1"] <- {
	refresh = 600,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 40,
			skill = 1,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 25,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 25,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 3,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 1,
			skill = 1,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		//T2
		{
			chance = 23,
			skill = 2,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 13,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania


		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 3,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 5,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia


	]
}
//WORK HERE
Config["FishingSystem"]["Rzeczne T1"] <- {
	refresh = 600,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 33,
			skill = 1,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 20,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 20,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 20,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 2,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 2,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 2,
			skill = 1,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 1,
			skill = 1,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		//T2
		{
			chance = 19,
			skill = 2,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 1,
			skill = 2,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		//T3
		{
			chance = 11,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 1,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 6,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

	]
}

Config["FishingSystem"]["Morskie T2"] <- {
	refresh = 600,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 23,
			skill = 2,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 18,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 12,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 1,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		//T3
		{
			chance = 13,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 3,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 5,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia
	]
}

Config["FishingSystem"]["Rzeczne T2"] <- {
	refresh = 600,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 19,
			skill = 2,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 14,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 10,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 2,
			skill = 2,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 1,
			skill = 2,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 2,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		//T3
		{
			chance = 11,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 1,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 6,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia
	]
}

Config["FishingSystem"]["Morskie T3"] <- {
	refresh = 720,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 13,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_63",
			amount = 1
		}, //�led�

		{
			chance = 13,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_71",
			amount = 1
		}, //szprot

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 10,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 3,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_70",
			amount = 1
		}, //Morszczuk

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_31",
			amount = 1
		}, //Makrela

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_69",
			amount = 1
		}, //Pirania

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_28",
			amount = 1
		}, //Miecznik

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_33",
			amount = 1
		}, //Tu�czyk

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_72",
			amount = 1
		}, //Rekin

		{
			chance = 5,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia
	]
}

Config["FishingSystem"]["Rzeczne T3"] <- {
	refresh = 720,
	slots = 10,
	vob = "FISCHEN_1.ASC",
	outcome = [{
			chance = 11,
			skill = 3,
			instance = "itfo_SDRP_food_30",
			amount = 1
		}, //p�o�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_29",
			amount = 1
		}, //kara�

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_67",
			amount = 1
		}, //karp

		{
			chance = 11,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_26",
			amount = 1
		}, //szczupak

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 8,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 6,
			skill = 3,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 1,
			skill = 3,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 3,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		//T4
		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_27",
			amount = 1
		}, //Sum

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_74",
			amount = 1
		}, //Pstr�g

		{
			chance = 18,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_47",
			amount = 1
		}, //�aba

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 13,
			skill = 4,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 2,
			skill = 4,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 4,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia

		//T5
		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_68",
			amount = 1
		}, //Z�ota Rybka

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_73",
			amount = 1
		}, //Barrakuda

		{
			chance = 30,
			skill = 5,
			instance = "ITFO_SDRP_FOOD_32",
			amount = 1
		}, //Seriola Olbrzymia

		{
			chance = 6,
			skill = 5,
			instance = "ITMI_ADDON_WHITEPEARL",
			amount = 1
		}, //per�a

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_GoldFisch",
			amount = 1
		}, //Ci�sza rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItFo_HalvorFish_Mis",
			amount = 1
		}, //Dziwnie wygl�daj�ca rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_LockpickFisch",
			amount = 1
		}, //Lekka rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_ErzFisch",
			amount = 1
		}, //Kulista rybcia

		{
			chance = 1,
			skill = 5,
			instance = "ItSe_RingFisch",
			amount = 1
		}, //Ma�a rybcia
	]
}
//Tutaj g�rnik
//jak jest amount = 0 to szansa na niewydropienie rudy
//szanse si� ��cz� tierowo na danym z�o�u czyli np wszystkie skill = 5 to wszystkie szanse si� dodaj� i rozk�adaj� na dany itemek
Config["MineSystem"] <- {}

Config["MineSystem"]["Zloze magicznej rudy"] <- {
	refresh = 180,
	slots = 5,
	vob = "ORE_GROUND.ASC",
	outcome = [{
			chance = 50, //szansa na drop
			skill = 5, //tier g�rnika
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 1 //ilo��
		},
		{
			chance = 50, //szansa na drop
			skill = 5,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 4,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 3,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze nasyconej magicznej rudy"] <- {
	refresh = 180,
	slots = 5,
	vob = "ORE_GROUND_MAGERZ.ASC",
	outcome = [{
			chance = 1, //szansa na drop
			skill = 5,
			instance = "ITMI_NUGGET", //bry�ka magicznej rudy
			amount = 1
		},
		{
			chance = 49, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 2
		},

		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 1
		},

		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},

		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 1
		},

		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_61", //Zanieczyszczona ruda
			amount = 0
		},
	]
}
/*
Config["MineSystem"]["Zloze czerwonej rudy"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_REDERZ.ASC",
    outcome = [
        {chance = 80, skill = 5, instance = "ITCR_SDRP_DROP_74", amount = 3},
        {chance = 40, skill = 5, instance = "ITCR_SDRP_DROP_74", amount = 2},
        {chance = 20, skill = 5, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 10, skill = 5, instance = "ITCR_SDRP_DROP_74", amount = 0},

        {chance = 40, skill = 4, instance = "ITCR_SDRP_DROP_74", amount = 2},
        {chance = 20, skill = 4, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 10, skill = 4, instance = "ITCR_SDRP_DROP_74", amount = 0},

        {chance = 20, skill = 3, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_SDRP_DROP_74", amount = 0},

        {chance = 10, skill = 2, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 40, skill = 2, instance = "ITCR_SDRP_DROP_74", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 80, skill = 1, instance = "ITCR_SDRP_DROP_74", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_SDRP_DROP_74", amount = 1},
        {chance = 160, skill = 1, instance = "ITCR_SDRP_DROP_74", amount = 0},
  ]
}
*/
Config["MineSystem"]["Zloze czarnej rudy"] <- {
	refresh = 180,
	slots = 20,
	vob = "ORE_GROUND_BLACKERZ.ASC",
	outcome = [{
			chance = 1, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 1
		},
		{
			chance = 99, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_74", //Bry�ka czarnej rudy
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze smoly"] <- {
	refresh = 5000,
	slots = 20,
	vob = "ORE_GROUND_KOHLE.ASC",
	outcome = [{
			chance = 20, //szansa na drop
			skill = 5,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 3
		},
		{
			chance = 30, //szansa na drop
			skill = 5,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 2
		},
		{
			chance = 50, //szansa na drop
			skill = 5,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 2
		},
		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 3,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 3,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 0
		},
		{
			chance = 50, //szansa na drop
			skill = 2,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 2,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 0
		},
		{
			chance = 50, //szansa na drop
			skill = 1,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 1,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 0
		},

		{
			chance = 50, //szansa na drop
			skill = 0,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 1
		},
		{
			chance = 50, //szansa na drop
			skill = 0,
			instance = "ITMI_PITCH_02", //Smo�a
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze wegla"] <- {
	refresh = 180,
	slots = 5,
	vob = "ORE_GROUND_KOHLE.ASC",
	outcome = [{
			chance = 50, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 5
		},
		{
			chance = 30, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 4
		},
		{
			chance = 10, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 3
		},
		{
			chance = 10, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 2
		},

		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 4
		},
		{
			chance = 30, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 3
		},
		{
			chance = 10, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 2
		},
		{
			chance = 10, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 1
		},

		{
			chance = 50, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 3
		},
		{
			chance = 25, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 2
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 2
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 1
		},

		{
			chance = 20, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_60", //Bry�ka w�gla
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze akwamarynu"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_AQUAMARIN.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 1
		},
		{
			chance = 75, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},

		{
			chance = 20, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},
		{
			chance = 15, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 1
		},
		{
			chance = 85, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_55", //Nieoszlifowany akwamaryn
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze amethystu"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_AMETHYST.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 1
		},
		{
			chance = 75, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},

		{
			chance = 20, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},
		{
			chance = 15, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 1
		},
		{
			chance = 85, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_57", //Nieoszlifowany ametyst
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze zelaza"] <- {
	refresh = 180,
	slots = 5,
	vob = "ORE_GROUNDIRON.ASC",
	outcome = [{
			chance = 30, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 5
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 4
		},
		{
			chance = 25,
			skill = 5,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 3
		},
		{
			chance = 20, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 2
		},

		{
			chance = 40, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 4
		},
		{
			chance = 30, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 3
		},
		{
			chance = 30, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 2
		},

		{
			chance = 40, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 3
		},
		{
			chance = 20, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 2
		},
		{
			chance = 20, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 2
		},

		{
			chance = 40, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 2
		},
		{
			chance = 60, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 1
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 1
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_13", //bry�ka �elaza
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze rubinu"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_RUBIN.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 1
		},
		{
			chance = 75, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},

		{
			chance = 20, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},
		{
			chance = 15, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 1
		},
		{
			chance = 85, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_53", //Nieoszlifowany rubin
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze topasu"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_TOPAS.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 1
		},
		{
			chance = 75, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},

		{
			chance = 20, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},
		{
			chance = 15, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 1
		},
		{
			chance = 85, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_56", //Nieoszlifowany topaz
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze szmaragdu"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_SMARAGD.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 1
		},
		{
			chance = 75, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},

		{
			chance = 20, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},
		{
			chance = 15, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 1
		},
		{
			chance = 85, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},


		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_54", //Nieoszlifowany szmaragd
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze srebra"] <- {
	refresh = 180,
	slots = 20,
	vob = "ORE_GROUND_SILVER.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 5
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 4
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 3
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 2
		},

		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 4
		},
		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 2
		},
		{
			chance = 1, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 1
		},

		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 2
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 1
		},

		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_52", //Samorodek srebra
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze zlota"] <- {
	refresh = 130,
	slots = 10,
	vob = "ORE_GROUND_GOLD.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 5
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 3
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 2
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 4
		},

		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 4
		},
		{
			chance = 33,
			skill = 4,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 2
		},

		{
			chance = 1, //szansa na drop
			skill = 4,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 1
		},

		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 2
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 1
		},

		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 0
		},
		{
			chance = 100, //szansa na drop
			skill = 0,
			instance = "ItMi_GoldNugget_Addon", //bry�ka z�ota
			amount = 0
		},

		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
	]
}
Config["MineSystem"]["Zloze siarki"] <- {
	refresh = 180,
	slots = 20,
	vob = "ORE_GROUNDSIAR.ASC",
	outcome = [{
			chance = 50, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 5
		},
		{
			chance = 30, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 4
		},
		{
			chance = 10, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 3
		},
		{
			chance = 10, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 2
		},

		{
			chance = 50, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 4
		},
		{
			chance = 30, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 3
		},
		{
			chance = 10, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 2
		},
		{
			chance = 10, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 1
		},

		{
			chance = 50, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 3
		},
		{
			chance = 25, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 2
		},
		{
			chance = 25, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 1
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 2
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 1
		},

		{
			chance = 20, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_58", //Bry�ka siarki
			amount = 0
		},
	]
}
Config["MineSystem"]["Zloze soli"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUNDSOL.ASC",
	outcome = [{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 5
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 4
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 3
		},
		{
			chance = 25, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 2
		},

		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 4
		},
		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 2
		},

		{
			chance = 33, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 1
		},

		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 3
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 2
		},
		{
			chance = 33, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 1
		},
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 0
		},

		{
			chance = 100, //szansa na drop
			skill = 2,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 2
		},

		{
			chance = 100, //szansa na drop
			skill = 1,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 1
		},

		{
			chance = 20, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 1
		},
		{
			chance = 80, //szansa na drop
			skill = 0,
			instance = "ITCR_SDRP_DROP_59", //bry�ka soli
			amount = 0
		},
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_53",
			amount = 1
		}, //rubin
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_54",
			amount = 1
		}, //szmaragd
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_55",
			amount = 1
		}, //akwamaryn
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_56",
			amount = 1
		}, //topaz
		{
			chance = 1, //szansa na drop
			skill = 3,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
		{
			chance = 2, //szansa na drop
			skill = 4,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
		{
			chance = 2, //szansa na drop
			skill = 5,
			instance = "ITCR_SDRP_DROP_57",
			amount = 1
		}, //ametyst
	]
}
Config["MineSystem"]["Zloze kamienia"] <- {
	refresh = 180,
	slots = 10,
	vob = "ORE_GROUND_STONE.ASC",
	outcome = [{
			chance = 50,
			skill = 5,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 5
		},
		{
			chance = 30,
			skill = 5,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 4
		},
		{
			chance = 10,
			skill = 5,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 3
		},
		{
			chance = 10,
			skill = 5,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 2
		},

		{
			chance = 50,
			skill = 4,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 4
		},
		{
			chance = 30,
			skill = 4,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 3
		},
		{
			chance = 10,
			skill = 4,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 2
		},
		{
			chance = 10,
			skill = 4,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 1
		},

		{
			chance = 50,
			skill = 3,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 3
		},
		{
			chance = 25,
			skill = 3,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 2
		},
		{
			chance = 25,
			skill = 3,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 1
		},
		{
			chance = 100,
			skill = 2,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 2
		},
		{
			chance = 100,
			skill = 1,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 1
		},
		{
			chance = 30,
			skill = 0,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 1
		},
		{
			chance = 70,
			skill = 0,
			instance = "ITCR_SDRP_DROP_85", //kamie� d
			amount = 0
		},
	]
}

Config["CornerSystem"] <- [{
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "GS_BIALAMYRTANA",
		position = {
			x = -108098,
			y = -898.281,
			z = -108173,
			angle = 293.059,
		}
		money = {
			min = 10,
			max = 15
		}
	},
	{
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "GS_URNALESTERA",
		position = {
			x = -115953,
			y = -1908.67,
			z = -109137,
			angle = 85.0355,
		}
		money = {
			min = 15,
			max = 20
		}
	},
	{
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "GS_WRZASK",
		position = {
			x = -111023,
			y = -1235.23,
			z = -100019,
			angle = 156.14,
		}
		money = {
			min = 5,
			max = 10
		}
	}, {
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "GS_WRZASK",
		position = {
			x = -111712,
			y = -628.594,
			z = -113585,
			angle = 342.482,
		}
		money = {
			min = 5,
			max = 10
		}
	}, {
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "ITMI_JOINT",
		position = {
			x = -111926,
			y = -1052.11,
			z = -109625,
			angle = 345.295,
		}
		money = {
			min = 8,
			max = 13
		}
	}, {
		name = "Podejrzany Typ",
		animation = "S_LGUARD",
		itemToTrade = "ITMI_JOINT",
		position = {
			x = -114299,
			y = -1588.44,
			z = -105298,
			angle = 269.438,
		}
		money = {
			min = 8,
			max = 13
		}
	}
]

// Player settings configuration

Config["PlayerSkins"] <- [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 75];
Config["PlayerFaces"] <- [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499];

Config["PlayerMaxSkin"] <- 87;
Config["PlayerMaxFace"] <- 2167;

Config["InventoryRaws"] <- 6;
Config["InventoryColumns"] <- 4;

// GUI Pointer

Config["NoCharactersInfo"] <- "Witaj na serwerze!\nTo serwer w kt�rym poczujesz\nco to prawdziwa rozgrywka roleplay.\nZapraszamy do utworzenia postaci.";
Config["HelpInfo"] <- "[#FF0000]Pomoc\n[#FFFFFF]Je�li jeste� nowy\n i chcia�by� dowiedzie� si� \njak dobrze zacz�� \nrozgrywk� na serwerze.\nOdwied� nasz� stron�\nhistoriakolonii.pl\noraz discorda, gdzie\nznajdziesz poradniki, kt�re pomog�\nCi wej�� w ten �wiat.";
Config["FAQ"] <- [
	"Jest� bogiem?",
	"> U�wiadom to sobie",
]

// FractionPermissionType

Config["FractionPermissionType"] <- {};

Config["FractionPermissionType"][FractionPermissionType.Invite] <- "Zapraszanie";
Config["FractionPermissionType"][FractionPermissionType.Leadership] <- "Zarz�dzanie";
Config["FractionPermissionType"][FractionPermissionType.Crafting] <- "Crafting";
Config["FractionPermissionType"][FractionPermissionType.Working] <- "Praca";
Config["FractionPermissionType"][FractionPermissionType.Trade] <- "Handel";
Config["FractionPermissionType"][FractionPermissionType.Faction] <- "Frakcja NPC";

// Interactions

Config["Interactions"] <- {};

Config["Interactions"][InteractionVob.Barbq] <- "BARBQ_SCAV.MDS"
Config["Interactions"][InteractionVob.Anvil] <- "BSANVIL_OC.MDS"
Config["Interactions"][InteractionVob.Mill] <- "MOERS_MESH.ASC"
Config["Interactions"][InteractionVob.Mill2] <- "MOERS_MESH2.ASC"
Config["Interactions"][InteractionVob.Cooker] <- "STOVE_NW_CITY_01.ASC"
Config["Interactions"][InteractionVob.Armourer] <- "ARMORSTAND_PLATTNER.ASC"
Config["Interactions"][InteractionVob.NewCampfire] <- "CAMPFIRE_NORMAL.ASC"
Config["Interactions"][InteractionVob.Alchemy] <- "LATI_G1.MDS"
Config["Interactions"][InteractionVob.RuneTable] <- "RMAKER_1.MDS"
Config["Interactions"][InteractionVob.Pot] <- "CAULDRON_OC2.ASC"
Config["Interactions"][InteractionVob.Bake] <- "BAKE_MESH.MDS"
Config["Interactions"][InteractionVob.Tanning] <- "SKINLAB_PLACE.MDS"
Config["Interactions"][InteractionVob.Sewing] <- "WEBSTUHL_USE.MDS"
Config["Interactions"][InteractionVob.ArrowTable] <- "TISCHLER_MESH.MDS"
Config["Interactions"][InteractionVob.BowTable] <- "BOWSDW_WBENCH.MDS"
Config["Interactions"][InteractionVob.Writing] <- "BUCHSCHREIBEN_01.ASC"
Config["Interactions"][InteractionVob.Cutting] <- "WOODCHOPPIN_NORMAL.MDS"
Config["Interactions"][InteractionVob.RuneMaker] <- "RMELTER_1.ASC"
Config["Interactions"][InteractionVob.BlastFurnace] <- "BSMELTER_1.MDS"
Config["Interactions"][InteractionVob.Furnace] <- "BSFIRE_OC.MDS"
Config["Interactions"][InteractionVob.Herbal] <- "LAB_PSI3.ASC"
Config["Interactions"][InteractionVob.Sworder] <- "BSANVIL_OC2.MDS"
Config["Interactions"][InteractionVob.Juicer] <- "HERB_PSI.MDS"
Config["Interactions"][InteractionVob.Alchemy2] <- "LAB_PSI.ASC"
Config["Interactions"][InteractionVob.FSlaughter] <- "FISCHGRILLEN_FINAL.MDS"
Config["Interactions"][InteractionVob.Carpentry] <- "SAEGEN_3.ASC"
Config["Interactions"][InteractionVob.Sharper] <- "BSSHARP_OC.MDS"
Config["Interactions"][InteractionVob.Cooker2] <- "PAN_OC.MDS"
Config["Interactions"][InteractionVob.Slaught] <- "FISCHSCHNEIDEN_RZEZNIK.ASC"
Config["Interactions"][InteractionVob.MedLab] <- "MOERS_LEKARZ.ASC"
Config["Interactions"][InteractionVob.WaterBucket] <- "BSCOOL_OC.MDS"
Config["Interactions"][InteractionVob.Fischsneiden] <- "FISCHSCHNEIDEN_1.ASC"
Config["Interactions"][InteractionVob.Baumsaege] <- "BAUMSAEGE_1.ASC"
Config["Interactions"][InteractionVob.Stomper] <- "BSFIRE_PROJEKTO_AUSSEN_VAR1.MDS"
Config["Interactions"][InteractionVob.Jeweler] <- "JEWELER_TABLE.ASC"

Config["InteractionsCraft"] <- [InteractionVob.Cooker, InteractionVob.Armourer, InteractionVob.Barbq, InteractionVob.Anvil, InteractionVob.NewCampfire, InteractionVob.Alchemy, InteractionVob.RuneTable, InteractionVob.Mill, InteractionVob.Mill2, InteractionVob.Pot, InteractionVob.Bake, InteractionVob.Tanning, InteractionVob.Sewing, InteractionVob.ArrowTable, InteractionVob.BowTable, InteractionVob.Writing, InteractionVob.Cutting, InteractionVob.RuneMaker, InteractionVob.BlastFurnace, InteractionVob.Herbal, InteractionVob.Sworder, InteractionVob.Juicer, InteractionVob.Alchemy2, InteractionVob.FSlaughter, InteractionVob.Carpentry, InteractionVob.Sharper, InteractionVob.Cooker2, InteractionVob.Slaught, InteractionVob.Furnace, InteractionVob.MedLab, InteractionVob.WaterBucket, InteractionVob.Fischsneiden, InteractionVob.Baumsaege, InteractionVob.Stomper, InteractionVob.Jeweler]

// Craft

Config["CraftAnimations"] <- {};
Config["CraftAnimations"]["NONE"] <- "Nie przypisuj";
Config["CraftAnimations"]["S_SIT"] <- "Siadanie";
Config["CraftAnimations"]["S_LGUARD"] <- "R�ce za�o�one";

Config["CraftCaller"] <- {};
Config["CraftCaller"][CraftCaller.Barbq] <- "Ognisko";
Config["CraftCaller"][CraftCaller.BarbqFraction] <- "Ognisko + frakcja";
Config["CraftCaller"][CraftCaller.Anvil] <- "Kowad�o";
Config["CraftCaller"][CraftCaller.AnvilFraction] <- "Kowad�o + frakcja";
Config["CraftCaller"][CraftCaller.Mill] <- "Mo�dzierz";
Config["CraftCaller"][CraftCaller.Mill2] <- "Mo�dzierz2";
Config["CraftCaller"][CraftCaller.MillFraction] <- "Mo�dzierz + frakcja";
Config["CraftCaller"][CraftCaller.Mill2Fraction] <- "Mo�dzierz2 + frakcja";
Config["CraftCaller"][CraftCaller.Alchemy] <- "St� alchemiczny";
Config["CraftCaller"][CraftCaller.AlchemyFraction] <- "St� alchemiczny + frakcja";
Config["CraftCaller"][CraftCaller.RuneTable] <- "St� runiczny";
Config["CraftCaller"][CraftCaller.RuneTableFraction] <- "St� runiczny + frakcja";
Config["CraftCaller"][CraftCaller.Cooker] <- "Kuchenka";
Config["CraftCaller"][CraftCaller.CookerFraction] <- "Kuchenka + frakcja";
Config["CraftCaller"][CraftCaller.Cooker2] <- "Patelnia";
Config["CraftCaller"][CraftCaller.Cooker2Fraction] <- "Patelnia + frakcja";
Config["CraftCaller"][CraftCaller.Armourer] <- "St� p�atnerza";
Config["CraftCaller"][CraftCaller.ArmourerFraction] <- "St� p�atnerza + frakcja";
Config["CraftCaller"][CraftCaller.Pot] <- "Koci�";
Config["CraftCaller"][CraftCaller.PotFraction] <- "Koci� + frakcja";
Config["CraftCaller"][CraftCaller.Bake] <- "Piec";
Config["CraftCaller"][CraftCaller.BakeFraction] <- "Piec + frakcja";
Config["CraftCaller"][CraftCaller.Tanning] <- "Stojak do garbowania";
Config["CraftCaller"][CraftCaller.TanningFraction] <- "Stojak do garbowania + frakcja";
Config["CraftCaller"][CraftCaller.Sewing] <- "St� do szycia";
Config["CraftCaller"][CraftCaller.SewingFraction] <- "St� do szycia + frakcja";
Config["CraftCaller"][CraftCaller.ArrowTable] <- "St� do strza�";
Config["CraftCaller"][CraftCaller.ArrowTableFraction] <- "St� do strza� + frakcja";
Config["CraftCaller"][CraftCaller.BowTable] <- "St� �uczarski";
Config["CraftCaller"][CraftCaller.BowTableFraction] <- "St� �uczarski + frakcja";
Config["CraftCaller"][CraftCaller.Writing] <- "Pulpit do pisania";
Config["CraftCaller"][CraftCaller.WritingFraction] <- "Pulpit do pisania + frakcja";
Config["CraftCaller"][CraftCaller.Cutting] <- "Pieniek drwala";
Config["CraftCaller"][CraftCaller.CuttingFraction] <- "Pieniek drwala + frakcja";
Config["CraftCaller"][CraftCaller.RuneMaker] <- "Piec runiczny";
Config["CraftCaller"][CraftCaller.RuneMakerFraction] <- "Piec runiczny + frakcja";
Config["CraftCaller"][CraftCaller.BlastFurnace] <- "Piec hutniczy";
Config["CraftCaller"][CraftCaller.BlastFurnaceFraction] <- "Piec hutniczy + frakcja";
Config["CraftCaller"][CraftCaller.Furnace] <- "Piec do stali";
Config["CraftCaller"][CraftCaller.FurnaceFraction] <- "Piec do stali + frakcja";
Config["CraftCaller"][CraftCaller.Herbal] <- "St� zielarza";
Config["CraftCaller"][CraftCaller.HerbalFraction] <- "St� zielarza + frakcja";
Config["CraftCaller"][CraftCaller.Sworder] <- "St� miecznika";
Config["CraftCaller"][CraftCaller.SworderFraction] <- "St� miecznika + frakcja";
Config["CraftCaller"][CraftCaller.Sugarer] <- "St� cukiernika";
Config["CraftCaller"][CraftCaller.SugarerFraction] <- "St� cukiernika + frakcja";
Config["CraftCaller"][CraftCaller.Juicer] <- "St� do soku";
Config["CraftCaller"][CraftCaller.JuicerFraction] <- "St� do soku + frakcja";
Config["CraftCaller"][CraftCaller.Alchemy2] <- "St� gorzelniczny";
Config["CraftCaller"][CraftCaller.Alchemy2Fraction] <- "St� gorzelniczny + frakcja";
Config["CraftCaller"][CraftCaller.FSlaughter] <- "St� do siekania ryby";
Config["CraftCaller"][CraftCaller.FSlaughterFraction] <- "St� do siekania ryby + frakcja";
Config["CraftCaller"][CraftCaller.Carpentry] <- "St� do pi�owania desek";
Config["CraftCaller"][CraftCaller.CarpentryFraction] <- "St� do pi�owania desek + frakcja";
Config["CraftCaller"][CraftCaller.Sharper] <- "Ko�o szlifierskie";
Config["CraftCaller"][CraftCaller.SharperFraction] <- "Ko�o szlifierskie + frakcja";
Config["CraftCaller"][CraftCaller.Slaught] <- "St� rze�nika";
Config["CraftCaller"][CraftCaller.SlaughtFraction] <- "St� rze�nika + frakcja";
Config["CraftCaller"][CraftCaller.MedLab] <- "St� medyka";
Config["CraftCaller"][CraftCaller.MedLabFraction] <- "St� medyka + frakcja";
Config["CraftCaller"][CraftCaller.WaterBucket] <- "Wiadro z wod�";
Config["CraftCaller"][CraftCaller.WaterBucketFraction] <- "Wiadro z wod� + frakcja";
Config["CraftCaller"][CraftCaller.Fischsneiden] <- "St� do filetowania";
Config["CraftCaller"][CraftCaller.FischsneidenFraction] <- "St� do filetowania + frakcja";
Config["CraftCaller"][CraftCaller.NewCampfire] <- "Ognisko z patykiem";
Config["CraftCaller"][CraftCaller.NewCampfireFraction] <- "Ognisko z patykiem + frakcja";
Config["CraftCaller"][CraftCaller.Baumsaege] <- "K�oda";
Config["CraftCaller"][CraftCaller.BaumsaegeFraction] <- "K�oda + frakcja";
Config["CraftCaller"][CraftCaller.Stomper] <- "Przerabiarka";
Config["CraftCaller"][CraftCaller.StomperFraction] <- "Przerabiarka + frakcja";
Config["CraftCaller"][CraftCaller.Jeweler] <- "St� jubilera";
Config["CraftCaller"][CraftCaller.JewelerFraction] <- "St� jubilera + frakcja";

Config["CraftCallerVobRelation"] <- {};

Config["CraftCallerVobRelation"][CraftCaller.Barbq] <- [InteractionVob.Barbq, false];
Config["CraftCallerVobRelation"][CraftCaller.BarbqFraction] <- [InteractionVob.Barbq, true];
Config["CraftCallerVobRelation"][CraftCaller.Anvil] <- [InteractionVob.Anvil, false];
Config["CraftCallerVobRelation"][CraftCaller.AnvilFraction] <- [InteractionVob.Anvil, true];
Config["CraftCallerVobRelation"][CraftCaller.Mill] <- [InteractionVob.Mill, false];
Config["CraftCallerVobRelation"][CraftCaller.Mill2] <- [InteractionVob.Mill2, false];
Config["CraftCallerVobRelation"][CraftCaller.MillFraction] <- [InteractionVob.Mill, true];
Config["CraftCallerVobRelation"][CraftCaller.Mill2Fraction] <- [InteractionVob.Mill2, true];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy] <- [InteractionVob.Alchemy, false];
Config["CraftCallerVobRelation"][CraftCaller.AlchemyFraction] <- [InteractionVob.Alchemy, true];
Config["CraftCallerVobRelation"][CraftCaller.RuneTable] <- [InteractionVob.RuneTable, false];
Config["CraftCallerVobRelation"][CraftCaller.RuneTableFraction] <- [InteractionVob.RuneTable, true];
Config["CraftCallerVobRelation"][CraftCaller.Cooker] <- [InteractionVob.Cooker, false];
Config["CraftCallerVobRelation"][CraftCaller.CookerFraction] <- [InteractionVob.Cooker, true];
Config["CraftCallerVobRelation"][CraftCaller.Cooker2] <- [InteractionVob.Cooker2, false];
Config["CraftCallerVobRelation"][CraftCaller.Cooker2Fraction] <- [InteractionVob.Cooker2, true];
Config["CraftCallerVobRelation"][CraftCaller.Armourer] <- [InteractionVob.Armourer, false];
Config["CraftCallerVobRelation"][CraftCaller.ArmourerFraction] <- [InteractionVob.Armourer, true];
Config["CraftCallerVobRelation"][CraftCaller.Pot] <- [InteractionVob.Pot, false];
Config["CraftCallerVobRelation"][CraftCaller.PotFraction] <- [InteractionVob.Pot, true];
Config["CraftCallerVobRelation"][CraftCaller.Bake] <- [InteractionVob.Bake, false];
Config["CraftCallerVobRelation"][CraftCaller.BakeFraction] <- [InteractionVob.Bake, true];
Config["CraftCallerVobRelation"][CraftCaller.Tanning] <- [InteractionVob.Tanning, false];
Config["CraftCallerVobRelation"][CraftCaller.TanningFraction] <- [InteractionVob.Tanning, true];
Config["CraftCallerVobRelation"][CraftCaller.Sewing] <- [InteractionVob.Sewing, false];
Config["CraftCallerVobRelation"][CraftCaller.SewingFraction] <- [InteractionVob.Sewing, true];
Config["CraftCallerVobRelation"][CraftCaller.ArrowTable] <- [InteractionVob.ArrowTable, false];
Config["CraftCallerVobRelation"][CraftCaller.ArrowTableFraction] <- [InteractionVob.ArrowTable, true];
Config["CraftCallerVobRelation"][CraftCaller.BowTable] <- [InteractionVob.BowTable, false];
Config["CraftCallerVobRelation"][CraftCaller.BowTableFraction] <- [InteractionVob.BowTable, true];
Config["CraftCallerVobRelation"][CraftCaller.Writing] <- [InteractionVob.Writing, false];
Config["CraftCallerVobRelation"][CraftCaller.WritingFraction] <- [InteractionVob.Writing, true];
Config["CraftCallerVobRelation"][CraftCaller.Cutting] <- [InteractionVob.Cutting, false];
Config["CraftCallerVobRelation"][CraftCaller.CuttingFraction] <- [InteractionVob.Cutting, true];
Config["CraftCallerVobRelation"][CraftCaller.RuneMaker] <- [InteractionVob.RuneMaker, false];
Config["CraftCallerVobRelation"][CraftCaller.RuneMakerFraction] <- [InteractionVob.RuneMaker, true];
Config["CraftCallerVobRelation"][CraftCaller.BlastFurnace] <- [InteractionVob.BlastFurnace, false];
Config["CraftCallerVobRelation"][CraftCaller.BlastFurnaceFraction] <- [InteractionVob.BlastFurnace, true];
Config["CraftCallerVobRelation"][CraftCaller.Furnace] <- [InteractionVob.Furnace, false];
Config["CraftCallerVobRelation"][CraftCaller.FurnaceFraction] <- [InteractionVob.Furnace, true];
Config["CraftCallerVobRelation"][CraftCaller.Herbal] <- [InteractionVob.Herbal, false];
Config["CraftCallerVobRelation"][CraftCaller.HerbalFraction] <- [InteractionVob.Herbal, true];
Config["CraftCallerVobRelation"][CraftCaller.Sworder] <- [InteractionVob.Sworder, false];
Config["CraftCallerVobRelation"][CraftCaller.SworderFraction] <- [InteractionVob.Sworder, true];
Config["CraftCallerVobRelation"][CraftCaller.Sugarer] <- [InteractionVob.Sugarer, false];
Config["CraftCallerVobRelation"][CraftCaller.SugarerFraction] <- [InteractionVob.Sugarer, true];
Config["CraftCallerVobRelation"][CraftCaller.Juicer] <- [InteractionVob.Juicer, false];
Config["CraftCallerVobRelation"][CraftCaller.JuicerFraction] <- [InteractionVob.Juicer, true];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy2] <- [InteractionVob.Alchemy2, false];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy2Fraction] <- [InteractionVob.Alchemy2, true];
Config["CraftCallerVobRelation"][CraftCaller.FSlaughter] <- [InteractionVob.FSlaughter, false];
Config["CraftCallerVobRelation"][CraftCaller.FSlaughterFraction] <- [InteractionVob.FSlaughter, true];
Config["CraftCallerVobRelation"][CraftCaller.Carpentry] <- [InteractionVob.Carpentry, false];
Config["CraftCallerVobRelation"][CraftCaller.CarpentryFraction] <- [InteractionVob.Carpentry, true];
Config["CraftCallerVobRelation"][CraftCaller.Sharper] <- [InteractionVob.Sharper, false];
Config["CraftCallerVobRelation"][CraftCaller.SharperFraction] <- [InteractionVob.Sharper, true];
Config["CraftCallerVobRelation"][CraftCaller.Slaught] <- [InteractionVob.Slaught, false];
Config["CraftCallerVobRelation"][CraftCaller.SlaughtFraction] <- [InteractionVob.Slaught, true];
Config["CraftCallerVobRelation"][CraftCaller.MedLab] <- [InteractionVob.MedLab, false];
Config["CraftCallerVobRelation"][CraftCaller.MedLabFraction] <- [InteractionVob.MedLab, true];
Config["CraftCallerVobRelation"][CraftCaller.WaterBucket] <- [InteractionVob.WaterBucket, false];
Config["CraftCallerVobRelation"][CraftCaller.WaterBucketFraction] <- [InteractionVob.WaterBucket, true];
Config["CraftCallerVobRelation"][CraftCaller.Fischsneiden] <- [InteractionVob.Fischsneiden, false];
Config["CraftCallerVobRelation"][CraftCaller.FischsneidenFraction] <- [InteractionVob.Fischsneiden, true];
Config["CraftCallerVobRelation"][CraftCaller.NewCampfire] <- [InteractionVob.NewCampfire, false];
Config["CraftCallerVobRelation"][CraftCaller.NewCampfireFraction] <- [InteractionVob.NewCampfire, true];
Config["CraftCallerVobRelation"][CraftCaller.Baumsaege] <- [InteractionVob.Baumsaege, false];
Config["CraftCallerVobRelation"][CraftCaller.BaumsaegeFraction] <- [InteractionVob.Baumsaege, true];
Config["CraftCallerVobRelation"][CraftCaller.Stomper] <- [InteractionVob.Stomper, false];
Config["CraftCallerVobRelation"][CraftCaller.StomperFraction] <- [InteractionVob.Stomper, true];
Config["CraftCallerVobRelation"][CraftCaller.Jeweler] <- [InteractionVob.Jeweler, false];
Config["CraftCallerVobRelation"][CraftCaller.JewelerFraction] <- [InteractionVob.Jeweler, true];

// Player const to values

Config["MaxDailyLearnPoints"] <- 2;
Config["DailyGivenPoints"] <- 3;
Config["HourlyGivenNuggets"] <- 2;
Config["MaxDailyNuggets"] <- 3;

Config["PlayerFatness"] <- {}
Config["PlayerFatness"][PlayerFatness.Thin] <- 0.44
Config["PlayerFatness"][PlayerFatness.Skinny] <- 0.18
Config["PlayerFatness"][PlayerFatness.Normal] <- 1.00
Config["PlayerFatness"][PlayerFatness.Muscular] <- 1.28
Config["PlayerFatness"][PlayerFatness.Thick] <- 1.50
Config["PlayerFatness"][PlayerFatness.Fat] <- 1.72

Config["PlayerWalk"] <- {}
Config["PlayerWalk"][PlayerWalk.Normal] <- "";
Config["PlayerWalk"][PlayerWalk.Tired] <- "HUMANS_TIRED.MDS";
Config["PlayerWalk"][PlayerWalk.Woman] <- "HUMANS_BABE.MDS";
Config["PlayerWalk"][PlayerWalk.Army] <- "HUMANS_MILITIA.MDS";
Config["PlayerWalk"][PlayerWalk.Lazy] <- "HUMANS_RELAXED.MDS";
Config["PlayerWalk"][PlayerWalk.Arrogant] <- "HUMANS_ARROGANCE.MDS";
Config["PlayerWalk"][PlayerWalk.Mage] <- "HUMANS_MAGE.MDS";
Config["PlayerWalk"][PlayerWalk.Pockets] <- "HUMANS_KIESZENIE.MDS";
Config["PlayerWalk"][PlayerWalk.OldTired] <- "HUMANS_G1TIRED.MDS";
Config["PlayerWalk"][PlayerWalk.OldArrogant] <- "HUMANS_G1ARROGANCE.MDS";
Config["PlayerWalk"][PlayerWalk.OldMage] <- "HUMANS_G1MAGE.MDS";
Config["PlayerWalk"][PlayerWalk.OldLazy] <- "HUMANS_G1RELAXED.MDS";
Config["PlayerWalk"][PlayerWalk.HurtBen] <- "HUMANS_HURT_BEN.MDS";
Config["PlayerWalk"][PlayerWalk.Leader] <- "HUMANS_LEADER.MDS";
Config["PlayerWalk"][PlayerWalk.Seller] <- "HUMANS_SELLER.MDS";
Config["PlayerWalk"][PlayerWalk.Stomachhurt] <- "HUMANS_STOMACHHURT.MDS";
Config["PlayerWalk"][PlayerWalk.Waitress] <- "HUMANS_WAITRESS.MDS";
Config["PlayerWalk"][PlayerWalk.Crate] <- "HUMANS_CRATE.MDS";
Config["PlayerWalk"][PlayerWalk.Poisoned] <- "HUMANS_POISONED.MDS";
Config["PlayerWalk"][PlayerWalk.Hurt] <- "HUMANS_HURT.MDS";
Config["PlayerWalk"][PlayerWalk.PocketWalk] <- "HUMANS_POCKETWALK.MDS";

Config["PlayerStatus"] <- {}
Config["PlayerStatus"][PlayerStatus.Active] <- "Aktywna";
Config["PlayerStatus"][PlayerStatus.Killed] <- "Zabita";
Config["PlayerStatus"][PlayerStatus.Deleted] <- "Usuni�ta";

Config["PlayerSkill"] <- {}
Config["PlayerSkill"][PlayerSkill.Blacksmith] <- "Kowal";
Config["PlayerSkill"][PlayerSkill.Runemaker] <- "Zaklinacz";
Config["PlayerSkill"][PlayerSkill.Armourer] <- "P�atnerz";
Config["PlayerSkill"][PlayerSkill.Alchemy] <- "Alchemik";
Config["PlayerSkill"][PlayerSkill.Cook] <- "Kucharz";
Config["PlayerSkill"][PlayerSkill.Fisherman] <- "Rybak";
Config["PlayerSkill"][PlayerSkill.Miner] <- "G�rnik";
Config["PlayerSkill"][PlayerSkill.Lumberjack] <- "Drwal";
Config["PlayerSkill"][PlayerSkill.Herbalist] <- "Ziel./Farm.";
//Config["PlayerSkill"][PlayerSkill.Farmer] <- "Nierob";
Config["PlayerSkill"][PlayerSkill.Hunter] <- "My�liwy";
Config["PlayerSkill"][PlayerSkill.Butcher] <- "Rze�nik";
Config["PlayerSkill"][PlayerSkill.Carpenter] <- "Stolarz";
Config["PlayerSkill"][PlayerSkill.Fletcher] <- "�uczarz";
Config["PlayerSkill"][PlayerSkill.Medic] <- "Medyk";
Config["PlayerSkill"][PlayerSkill.Tailor] <- "Krawiec";
Config["PlayerSkill"][PlayerSkill.Thief] <- "Z�odziej";
Config["PlayerSkill"][PlayerSkill.Jeweler] <- "Jubiler";

Config["PlayerAttributes"] <- {}
Config["PlayerAttributes"][PlayerAttributes.Str] <- "Si�a";
Config["PlayerAttributes"][PlayerAttributes.Dex] <- "Zr�czno��";
Config["PlayerAttributes"][PlayerAttributes.Int] <- "Inteligencja";
Config["PlayerAttributes"][PlayerAttributes.Hp] <- "Witalno��";
Config["PlayerAttributes"][PlayerAttributes.Mana] <- "Mana";
Config["PlayerAttributes"][PlayerAttributes.OneH] <- "Bro� 1h";
Config["PlayerAttributes"][PlayerAttributes.TwoH] <- "Bro� 2h";
Config["PlayerAttributes"][PlayerAttributes.Bow] <- "�uk";
Config["PlayerAttributes"][PlayerAttributes.Cbow] <- "Kusza";
Config["PlayerAttributes"][PlayerAttributes.MagicLvl] <- "Kr�g magii";
Config["PlayerAttributes"][PlayerAttributes.Stamina] <- "Wytrzyma�o��";
Config["PlayerAttributes"][PlayerAttributes.LearnPoints] <- "Punkty nauki";
Config["PlayerAttributes"][PlayerAttributes.Hunger] <- "Najedzenie";

Config["PlayerSkillValue"] <- ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

Config["PlayerSkillUpgrade"] <- {};
Config["PlayerSkillUpgrade"][PlayerSkill.Blacksmith] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 70
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 160
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
Config["PlayerSkillUpgrade"][PlayerSkill.Armourer] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 70
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
Config["PlayerSkillUpgrade"][PlayerSkill.Alchemy] <- [{
		min = 0,
		costLp = 10
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
/*
Config["PlayerSkillUpgrade"][PlayerSkill.Enchanter] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
*/
Config["PlayerSkillUpgrade"][PlayerSkill.Cook] <- [{
		min = 0,
		costLp = 10
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
Config["PlayerSkillUpgrade"][PlayerSkill.Miner] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 160
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
Config["PlayerSkillUpgrade"][PlayerSkill.Lumberjack] <- [{
		min = 0,
		costLp = 10
	},
	{
		min = 1,
		costLp = 70
	},
	{
		min = 2,
		costLp = 120
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];
Config["PlayerSkillUpgrade"][PlayerSkill.Herbalist] <- [{
		min = 0,
		costLp = 10
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		max = 3,
		costLp = 100
	},
]
//Config["PlayerSkillUpgrade"][PlayerSkill.Farmer] <- [{
//		min = 0,
//		costLp = 1000
//	},
//	{
//		min = 1,
//		costLp = 1000
//	},
//	{
//		min = 2,
//		max = 3,
//		costLp = 1000
//	},
//];
Config["PlayerSkillUpgrade"][PlayerSkill.Hunter] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 50
	},
	{
		min = 2,
		costLp = 80
	},
	{
		min = 3,
		costLp = 160
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Thief] <- [
	//{ min = 0, costLp = 999 },
	{
		min = 1,
		max = 2,
		costLp = 100
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Fisherman] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},

];

Config["PlayerSkillUpgrade"][PlayerSkill.Tailor] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Butcher] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Carpenter] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Fletcher] <- [{
		min = 0,
		costLp = 15
	},
	{
		min = 1,
		costLp = 70
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];

Config["PlayerSkillUpgrade"][PlayerSkill.Medic] <- [{
		min = 0,
		costLp = 10
	},
	{
		min = 1,
		costLp = 60
	},
	{
		min = 2,
		costLp = 100
	},
	{
		min = 3,
		costLp = 150
	},
	{
		min = 4,
		max = 5,
		costLp = 200
	},
];


Config["PlayerAttributeUpgrade"] <- {};
Config["PlayerAttributeUpgrade"][PlayerAttributes.Str] <- [{
		min = 10,
		costLp = 3,
		value = 1
	},
	{
		min = 15,
		costLp = 4,
		value = 1
	},
	{
		min = 30,
		costLp = 5,
		value = 1
	},
	{
		min = 45,
		costLp = 6,
		value = 1
	},
	{
		min = 60,
		costLp = 7,
		value = 1
	},
	{
		min = 75,
		costLp = 8,
		value = 1
	},
	{
		min = 90,
		costLp = 9,
		value = 1
	},
	{
		min = 100,
		costLp = 10,
		value = 1
	},
	{
		min = 110,
		costLp = 11,
		value = 1
	},
	{
		min = 120,
		costLp = 12,
		value = 1
	},
	{
		min = 130,
		costLp = 13,
		value = 1
	},
	{
		min = 140,
		max = 150,
		costLp = 14,
		value = 1
	},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Dex] <- [{
		min = 10,
		costLp = 3,
		value = 1
	},
	{
		min = 15,
		costLp = 4,
		value = 1
	},
	{
		min = 30,
		costLp = 5,
		value = 1
	},
	{
		min = 45,
		costLp = 6,
		value = 1
	},
	{
		min = 60,
		costLp = 7,
		value = 1
	},
	{
		min = 75,
		costLp = 8,
		value = 1
	},
	{
		min = 90,
		costLp = 9,
		value = 1
	},
	{
		min = 100,
		costLp = 10,
		value = 1
	},
	{
		min = 110,
		costLp = 11,
		value = 1
	},
	{
		min = 120,
		costLp = 12,
		value = 1
	},
	{
		min = 130,
		costLp = 13,
		value = 1
	},
	{
		min = 140,
		max = 150,
		costLp = 14,
		value = 1
	},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Int] <- [{
		min = 10,
		costLp = 3,
		value = 1
	},
	{
		min = 15,
		costLp = 4,
		value = 1
	},
	{
		min = 30,
		costLp = 5,
		value = 1
	},
	{
		min = 45,
		costLp = 6,
		value = 1
	},
	{
		min = 60,
		costLp = 7,
		value = 1
	},
	{
		min = 75,
		costLp = 8,
		value = 1
	},
	{
		min = 90,
		max = 200,
		costLp = 9,
		value = 1
	},
];
// Config["PlayerAttributeUpgrade"][PlayerAttributes.MagicLvl] <- [{
// 		min = 1,
// 		costLp = 60
// 	},
// 	{
// 		min = 2,
// 		max = 3,
// 		costLp = 120
// 	},
// ];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Hp] <- [{
		min = 100,
		costLp = 3,
		value = 10
	},
	{
		min = 120,
		costLp = 4,
		value = 10
	},
	{
		min = 140,
		costLp = 5,
		value = 10
	},
	{
		min = 160,
		costLp = 6,
		value = 10
	},
	{
		min = 180,
		costLp = 7,
		value = 10
	},
	{
		min = 200,
		costLp = 8,
		value = 10
	},
	{
		min = 220,
		costLp = 9,
		value = 10
	},
	{
		min = 240,
		costLp = 10,
		value = 10
	},
	{
		min = 260,
		costLp = 11,
		value = 10
	},
	{
		min = 280,
		costLp = 12,
		value = 10
	},
	{
		min = 400,
		max = 500,
		costLp = 15,
		value = 10
	},

];
// Config["PlayerAttributeUpgrade"][PlayerAttributes.Mana] <- [
//     { min = 0, costLp = 7, value = 5 },
//     { min = 35, costLp = 8, value = 5 },
//     { min = 80, costLp = 12, value = 10 },
//     { min = 200, max = 500, costLp = 14, value = 10 },
// ];
Config["PlayerAttributeUpgrade"][PlayerAttributes.OneH] <- [{
		min = 0,
		costLp = 2,
		value = 1
	},
	{
		min = 15,
		costLp = 3,
		value = 1
	},
	{
		min = 30,
		costLp = 4,
		value = 1
	},
	{
		min = 45,
		costLp = 5,
		value = 1
	},
	{
		min = 60,
		costLp = 6,
		value = 1
	},
	{
		min = 75,
		costLp = 7,
		value = 1
	},
	{
		min = 90,
		max = 100,
		costLp = 8,
		value = 1
	},


];
Config["PlayerAttributeUpgrade"][PlayerAttributes.TwoH] <- [{
		min = 0,
		costLp = 2,
		value = 1
	},
	{
		min = 15,
		costLp = 3,
		value = 1
	},
	{
		min = 30,
		costLp = 4,
		value = 1
	},
	{
		min = 45,
		costLp = 5,
		value = 1
	},
	{
		min = 60,
		costLp = 6,
		value = 1
	},
	{
		min = 75,
		costLp = 7,
		value = 1
	},
	{
		min = 90,
		max = 100,
		costLp = 8,
		value = 1
	},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Bow] <- [{
		min = 0,
		costLp = 2,
		value = 1
	},
	{
		min = 15,
		costLp = 3,
		value = 1
	},
	{
		min = 30,
		costLp = 4,
		value = 1
	},
	{
		min = 45,
		costLp = 5,
		value = 1
	},
	{
		min = 60,
		costLp = 6,
		value = 1
	},
	{
		min = 75,
		costLp = 7,
		value = 1
	},
	{
		min = 90,
		max = 100,
		costLp = 8,
		value = 1
	},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Cbow] <- [{
		min = 0,
		costLp = 2,
		value = 1
	},
	{
		min = 15,
		costLp = 3,
		value = 1
	},
	{
		min = 30,
		costLp = 4,
		value = 1
	},
	{
		min = 60,
		costLp = 6,
		value = 1
	},
	{
		min = 75,
		costLp = 7,
		value = 1
	},
	{
		min = 90,
		max = 100,
		costLp = 8,
		value = 1
	},
];
Config["PlayerSkillStaminaCost"] <- {}
Config["PlayerSkillStaminaCost"][PlayerSkill.Lumberjack] <- [{
		tier = 0,
		cost = 50
	},
	{
		tier = 1,
		cost = 25
	},
	{
		tier = 2,
		cost = 25
	},
	{
		tier = 3,
		cost = 20
	},
	{
		tier = 4,
		cost = 20
	},
	{
		tier = 5,
		cost = 20
	},
];

Config["PlayerScenario"] <- {
	spawn = {
		x = -88970.9,
		y = 617.57,
		z = -118394,
		angle = 349
	},
};

// Settings

Config["Settings"] <- {
	"ChatAnimation": {
		name = "Animacja chatu",
		type = SettingsFormType.Checkbox,
		defined = true
	},
	"DialogCustomFont": {
		name = "Customowy font dialog�w z npc",
		type = SettingsFormType.Checkbox,
		defined = true
	},
	"ChatLines": {
		name = "Linijki chatu",
		type = SettingsFormType.Range,
		min = 5,
		max = 30,
		defined = 10,
	},
	"OtherCustomFont": {
		name = "Customowy font w GUI",
		type = SettingsFormType.Checkbox,
		defined = true
	},
	"NamesRound": {
		name = "Wy�wietlanie nick�w w pobli�u",
		type = SettingsFormType.Checkbox,
		defined = true
	}
	"ChatPercentOnScreen": {
		name = "Szeroko�� chatu",
		type = SettingsFormType.Range,
		min = 50,
		max = 100,
		defined = 70
	},

	"InterfaceScale": {
		name = "Skala interfejsu",
		type = SettingsFormType.Range,
		min = 1,
		max = 10,
		defined = 5
	},
	"DialogAutoContinue": {
		name = "Automatyczne przewijanie dialogu",
		description = "Czas po jakim automatycznie dialog przesuwa si� dalej.",
		type = SettingsFormType.Range,
		min = 5,
		max = 30,
		defined = 15,
	}
	"InterfaceType": {
		name = "Tryb interfejsu",
		type = SettingsFormType.Select,
		data = [{
				id = "Minimal",
				name = "Minimalny"
			},
			{
				id = "Optimal",
				name = "Optymalny"
			},
			{
				id = "Maximum",
				name = "Ci�g�a widoczno��"
			},
		],
		defined = "Optimal"
	},
	"Resolution": {
		name = "Rozdzielczo�� ekranu",
		type = SettingsFormType.Select,
		data = [],
		defined = 0,
	},
	"SoundtrackVolume": {
		name = "G�osno�� soundtracku",
		type = SettingsFormType.Range,
		min = 0.0,
		max = 1.0,
		defined = 1.0
	},
};

//Wypalona gleba - polna gleba
//Gleba niskiej jako�ci - lesna gleba
//Zwyk�a gleba - gorska gleba
//Bogata w minera�y gleba - bagienna gleba
//Idealna gleba - gleba przy wodzie

Config["GroundLevel"] <- ["Wypalona gleba", "Gleba niskiej jako�ci", "Zwyk�a gleba", "Bogata w minera�y gleba", "Idealna gleba"]

Config["GroundHerbSystem"] <- {};
Config["GroundHerbSystem"]["Wszedzie"] <- [{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_01" // Serafis
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_10" // Ziele lecznicze
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SPEED_HERB_01" // Z�bate ziele
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_11" // Korze� leczniczy
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_MANA_HERB_01" // Ognista pokrzywa
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Las"] <- [{
		cooldown = 7200, // sekundy
		instance = "ITPL_FORESTBERRY" // Le�na jagoda
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_14" // Twardzie�
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_09" // Ro�lina lecznicza
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_05" // Psianka
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_18" // Piekielnik
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_19" // Gorzki chleb
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_MUSHROOM_01" // ciemny grzyb
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_MUSHROOM_02" // mi�so kopacza
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_61" // Amanita
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_62" // Craterellus
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_63" // T�usty kret
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_64" // Du�y muchomor
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_65" // Pieprznik
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Jaskinia"] <- [{
	cooldown = 7200, // sekundy
	instance = "ITPL_MUSHROOM_02" // mi�so kopacza
}, ]
Config["GroundHerbSystem"]["Bagna"] <- [{
		cooldown = 7200, // sekundy
		instance = "ITPL_SWAMPHERB" // Bagienne ziele
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Orkowe"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_07" // Orkowe ziele
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Trolowe"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_SAGITTA_HERB_MIS" // S�oneczny aloes
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Polne"] <- [{
		cooldown = 7200, // sekundy
		instance = "ITPL_BLUEPLANT" // Niebieski bez
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_PLANEBERRY" // Polna jagoda
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["RzadkieWszedzie"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_MANA_HERB_02" // Ogniste ziele
	},
	{
		cooldown = 8000, // sekundy
		instance = "ITPL_MANA_HERB_03" // Ognisty korze�
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["RzadkieJaskinia"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_66" // Trolli czort
	},
	{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_67" // Harpii szpon
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_SDRP_HERB_68" // Magiczny grzyb
	},
]
Config["GroundHerbSystem"]["RzadkieJawne"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_02" // Velais
	},
	{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_06" // Kocianka
	},
	{
		cooldown = 8000, // sekundy
		instance = "ITPL_DEX_HERB_01" // Goblinie jagody
	},
	{
		cooldown = 8000, // sekundy
		instance = "ITPL_STRENGTH_HERB_01" // Smoczy korze�
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Gory"] <- [{
		cooldown = 8000, // sekundy
		instance = "ITPL_SDRP_HERB_03" // G�rski mech
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Cmentarz"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_SDRP_HERB_04" // Mech nagrobny
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Rzepa"] <- [{
		cooldown = 7200, // sekundy
		instance = "ITPL_BEET" // Rzepa
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["BardzoRzadkie1"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_SDRP_HERB_08" // Li�� d�bu
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["BardzoRzadkie2"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_SDRP_HERB_12" // Krucze ziele
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["BardzoRzadkie3"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_SDRP_HERB_13" // Czarne ziele
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["BardzoRzadkie4"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_SDRP_HERB_15" // Dragrot
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["BardzoRzadkie5"] <- [{
		cooldown = 9500, // sekundy
		instance = "ITPL_PERM_HERB" // Szczaw kr�lewski
	},
	{
		cooldown = 7200, // sekundy
		instance = "ITPL_WEED" // Chwasty
	}
]
Config["GroundHerbSystem"]["Jablka"] <- [{
	cooldown = 7200, // sekundy
	instance = "ITFO_SDRP_FOOD_11" // jablko
}, ]
Config["GroundHerbSystem"]["Woda morska"] <- [{
	cooldown = 7200, // sekundy
	instance = "ITCR_SDRP_INGR_26" // woda morska
}, ]




Config["HerbSystem"] <- {};
Config["HerbSystem"]["Rumianek"] <- [{
		name = "Nasiono Rumianku",
		time = 60, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_SDRP_HERB_20",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�y Rumianek",
		time = 90, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�y Rumianek",
		time = 120, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Rumianek",
		time = 150, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_SWAMPHERB",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["Ziemniak"] <- [{
		name = "Nasiono Ziemniaka",
		time = 60, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_SDRP_HERB_23",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�y Ziemniak",
		time = 90, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�y Ziemniak",
		time = 120, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Ziemniak",
		time = 150, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITFO_GS_Ziemniaki",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Bawe�na"] <- [{
		name = "Nasiono Bawe�ny",
		time = 60, // sekundy
		difficulty = 1, // 0 - 10
		instance = "itpl_SDRP_herb_50",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�a Bawe�na",
		time = 90, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�a Bawe�na",
		time = 120, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Bawe�na",
		time = 150, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITFO_GS_Bawelna",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Pokrzywa"] <- [{
		name = "Nasiono Pokrzywy",
		time = 60, // sekundy
		difficulty = 1, // 0 - 10
		instance = "itpl_SDRP_herb_69",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�a Pokrzywa",
		time = 90, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�a Pokrzywa",
		time = 120, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Pokrzywa",
		time = 150, // sekundy
		difficulty = 1, // 0 - 10
		instance = "ITPL_MANA_HERB_01",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["Wilczy mak"] <- [{
		name = "Nasiono Wilczy mak",
		time = 60, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_35",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�y Wilczy mak",
		time = 90, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�y Wilczy mak",
		time = 120, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Wilczy mak",
		time = 150, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_10",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["Winogron"] <- [{
		name = "Pestka winogrona",
		time = 60, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_74",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�e winogrono",
		time = 90, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�e winogrono",
		time = 120, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Winogron",
		time = 150, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itfo_SDRP_food_05",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]


Config["HerbSystem"]["Marchewka"] <- [{
		name = "Nasiono marchwi",
		time = 60, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_48",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�a marchew",
		time = 90, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�a marchew",
		time = 120, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Marchew",
		time = 150, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITFO_SDRP_KM_10",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Zbo�a"] <- [{
		name = "Nasiono Zbo�a",
		time = 60, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_22",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�e Zbo�e",
		time = 90, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�e Zbo�e",
		time = 120, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Zbo�e",
		time = 150, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITFO_GS_Zboze",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Mi�ta"] <- [{
		name = "Nasiono Mi�ty",
		time = 60, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_SDRP_HERB_30",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�a Mi�ta",
		time = 90, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�a Mi�ta",
		time = 120, // sekundy
		difficulty = 2, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Mi�ta",
		time = 150, // sekundy
		difficulty = 2, // 0 - 10
		instance = "itpl_SDRP_herb_09",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["�ywokost"] <- [{
		name = "Nasiono �ywokostu",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_37",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�y �ywokost",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�y �ywokost",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "�ywokost",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_01",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["Pio�un"] <- [{
		name = "Nasiono Pio�unu",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_28",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Niedojrza�y Pio�un",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 3,
	},
	{
		name = "Niedojrza�y Pio�un",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 3,
	},
	{
		name = "Pio�un",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_PLANEBERRY",
		amount = 1,
		staminaCost = 6,
		ground = 3,
	}
]

Config["HerbSystem"]["Kapusta"] <- [{
		name = "Nasiono Kapusty",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_21",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�a Kapusta",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�a Kapusta",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Kapusta",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITFO_GS_Kapusta",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Burak"] <- [{
		name = "Nasiono Buraka",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_49",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�y Burak",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�y Burak",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Burak",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITFO_SDRP_KM_69",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Jagoda"] <- [{
		name = "Nasiono Jagody",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_75",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�a Jagoda",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�a Jagoda",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Jagoda",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itfo_SDRP_food_07",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

Config["HerbSystem"]["Malina"] <- [{
		name = "Nasiono Maliny",
		time = 60, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itpl_SDRP_herb_77",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Niedojrza�a Malina",
		time = 90, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 2,
		ground = 0,
	},
	{
		name = "Niedojrza�a Malina",
		time = 120, // sekundy
		difficulty = 3, // 0 - 10
		instance = "ITPL_WEED",
		amount = 1,
		staminaCost = 3,
		ground = 0,
	},
	{
		name = "Malina",
		time = 150, // sekundy
		difficulty = 3, // 0 - 10
		instance = "itfo_SDRP_food_08",
		amount = 1,
		staminaCost = 6,
		ground = 0,
	}
]

// Vobs

Config["VobType"] <- {};
Config["VobType"][VobType.Static] <- "Statyczny";
Config["VobType"][VobType.StaticName] <- "Statyczny + nazwa";
Config["VobType"][VobType.Interaction] <- "Interakcja";
Config["VobType"][VobType.Door] <- "Ruchomy";
Config["VobType"][VobType.Switch] <- "Prze��cznik";
Config["VobType"][VobType.Container] <- "Magazyn";

Config["VobHealth"] <- {};
Config["VobHealth"][VobHealth.Static] <- "Statyczny";
Config["VobHealth"][VobHealth.Vulnerable] <- "Do zniszczenia";

Config["VobMovable"] <- {};
Config["VobMovable"][VobMovable.Static] <- "Statyczny";
Config["VobMovable"][VobMovable.Active] <- "Do przemieszczenia";

Config["VobTriggerType"] <- {};
Config["VobTriggerType"][VobTriggerType.Self] <- "Brak";
Config["VobTriggerType"][VobTriggerType.Vob] <- "Inny Vob";

Config["VobTriggerAnimation"] <- {};
Config["VobTriggerAnimation"][VobTriggerAnimation.Door] <- "Drzwi";
Config["VobTriggerAnimation"][VobTriggerAnimation.None] <- "�adna";

Config["DamageLabel"] <- {};

Config["DamageLabel"][DAMAGE_BLUNT] <- "Obuch";
Config["DamageLabel"][DAMAGE_POINT] <- "Strza�a";
Config["DamageLabel"][DAMAGE_EDGE] <- "Ostrze";
Config["DamageLabel"][DAMAGE_MAGIC] <- "Magia";
Config["DamageLabel"][DAMAGE_FIRE] <- "Ogie�";
Config["DamageLabel"][DAMAGE_FLY] <- "Powietrze";
Config["DamageLabel"][DAMAGE_FALL] <- "Upadek";
Config["DamageLabel"][DAMAGE_BARRIER] <- "Bariera";
Config["DamageLabel"][DAMAGE_UNKNOWN] <- "Nieznane";

Config["PlayerHeal"] <- {
	"ITCR_SDRP_MED_20": 30000,
	"ITCR_SDRP_MED_21": 30000,
	"ITCR_SDRP_MED_22": 30000,
	"ITCR_SDRP_MED_23": 30000,
	"ITCR_SDRP_MED_24": 30000,
	"ITCR_SDRP_MED_12": 30000,
	"ITSC_LIGHTHEAL": 30000,
	"ITSC_MEDIUMHEAL": 30000,
	"ITSC_FULLHEAL": 30000,
};

Config["AnimationCommands"] <- {
	"taniec1": "T_DANCE_09",
	"taniec2": "T_DANCE_08",
	"taniec3": "T_DANCE_07",
	"taniec4": "T_DANCE_06",
	"taniec5": "T_DANCE_05",
	"taniec6": "T_DANCE_04",
	"taniec7": "T_DANCE_03",
	"ktaniec1": "T_DANCEK1",
	"ktaniec 2": "T_DANCEK2",
	"striptiz": "T_STAND_2_STRIPPEN",
	"tbrzucha": "T_STAND_2_BAUCHTANZ",
	"tludowy": "T_FALK_DANCE",
	"wariat": "S_FIRE_VICTIM",
	"mrugnij": ["R_EYESBLINK"],
	"zly": ["S_ANGRY"],
	"oczy": ["S_EYESCLOSED"],
	"przestraszony": ["S_FRIGHTENED"],
	"normalny": ["S_NEUTRAL"],
	"przyjazny": ["S_FRIENDLY"],
	"czczenie": "T_IDOL_S0_2_S1",
	"uklon": "T_GREETNOV",
	"przywitaniemag": "T_JUBELN_04",
	"artysta1": "T_AMBIENT_VERBEUGEN_01",
	"artysta2": "T_AMBIENT_VERBEUGEN_02",
	"arena": "T_WATCHFIGHT_YEAH",
	"kibicowanie": "S_KIBICOWANIE",
	"kdomyslne": "T_STAND_2_CLAPHANDS",
	"kpowolne": "T_JUBELN_05",
	"keskpresyjne": "T_JUBELN_01",
	"kwyrachowane": "T_JUBELN_02",
	"kdystynkt": "T_JUBELN_03",
	"machaj": "T_MACHANIE",
	"machaj2": "T_MACHANIE2",
	"machaj3": "T_ARENAMACHANIE",
	"pomachaj": "T_AMBIENT_SCHIMPFEN",
	"zbierz": "T_PLUNDER",
	"sikanie": "T_STAND_2_PEE",
	"sikaniepijak": "T_DRUNKENPEE",
	"zyganie": "S_KOTZKUEBEL_S1",
	"pochodnia1": "T_POSWIECENIE",
	"pochodnia2": "T_POSWIECENIE2",
	"pochodnia3": "T_POCHODNIA",
	"nakolana": "T_BETEN_STAND_2_S0",
	"blaganie": "T_BLAGANIE",
	"scinaj": "S_LUMBERJACK_S1",
	"przybijaj": "S_FLOORHAM_S1",
	"naprawiaj": "S_REPAIR_S1",
	"kopziemie": "S_DIGGER_S1",
	"koprude": "S_ORE_S1",
	"zbieraj": "S_EINPFLANZEN_S1",
	"dobij": "T_1HSFINISH",
	"wygrana": "T_WYGRANA",
	"alchemia": "S_LAB_S1",
	"pal": "T_JOINT_RANDOM_1",
	"pij": "T_POTION_RANDOM_1",
	"jedz": "T_MEAT_RANDOM_1",
	"myj": "T_STAND_2_WASH",
	"czytajn": "T_MAP_STAND_2_S0",
	"czytajp": "S_BOOK_S1",
	"ostrz": "T_CARVE_STAND_2_S0",
	"pijzrzeki": "T_KNIEENTRINKEN_STAND_2_S0",
	"salto": "T_SALTO",
	"lutnia": "S_LUTE_S1",
	"rog": "T_HORN_S0_2_S1",
	"trening": "T_1HSFREE",
	"rozgrzewka": "T_ROZGRZEWKA",
	"box": "T_BOXOWANIE",
	"rozciaganie": "T_GYMNASTIK_01",
	"miesniak": "T_GYMNASTIK_04",
	"przysiady": "T_GYMNASTIK_05",
	"pompki": "S_POMPKI_S1",
	"dopompek": "T_POMPKI",
	"szpagat": "T_GYMNASTIK_08",
	"przeciaganie": "T_GYMNASTIK_09",
	"siad": "T_STAND_2_SIT",
	"siado": "T_STAND_2_PACO",
	"siadw": "T_STAND_2_MELVIN",
	"siadm": "T_STAND_2_BAAL",
	"lawka": "S_BENCH_S1",
	"lez": "S_DEADB",
	"lezbrzuch": "S_DEAD",
	"spij": "T_STAND_2_SLEEPGROUND",
	"mumia": "T_MUMIE_STAND",
	"straznik1": "T_STAND_2_LGUARD",
	"straznik2": "T_STAND_2_HGUARD",
	"salut": "T_SALUTOWANIE",
	"amor": "T_STRZELANIELUK",
	"przykuc": "T_STAND_2_PRZYKUC",
	"oparcie": "T_STAND_2_LEAN",
	"oparcie2": "S_OPIERANIESIE",
	"kucniecie": "T_PAN_STAND_2_S0",
	"modlitwa": "T_STAND_2_PRAY",
	"magia1": "T_PRACTICEMAGIC",
	"magia2": "T_PRACTICEMAGIC2",
	"magia3": "T_PRACTICEMAGIC3",
	"magia4": "T_PRACTICEMAGIC4",
	"magia5": "T_PRACTICEMAGIC6",
	"magia6": "T_PRACTICEMAGIC7",
	"energia": "T_ENERGIA",
	"magicznakula": "T_KULAFULA",
	"czar": "T_CZAR",
	"przeglad": "T_1HSINSPECT",
	"szalenstwo": "S_CON_VICTIM",
	"nudy": "T_BORINGKICK",
	"rozgladanie1": "T_ZASIEBIE",
	"rozgladanie2": "T_ZASIEBIE2",
	"rozgladanie3": "T_ROZGLADANIE",
	"wypatrywanie": "S_UMSEHEN_S1",
	"wytarcie": "T_POTION_RANDOM_2",
	"zebranie": "T_STAND_2_BETTELN",
	"skrzyzowane": "T_STAND_2_ARMCROSSED_A",
	"latarnia": "T_STANDTALK",
	"kopacz": "T_STANDRIECHEN",
	"gadanie": "T_GADANIE",
	"tak": "T_YES",
	"nie": "T_NO",
	"niewiem": "T_DONTKNOW",
	"facepalm": "T_IDIOT"
	"dzieki": "T_DZIEKUJE",
	"nonie": "T_NONIE",
	"zapomnij": "T_FORGETIT",
	"grozba1": "T_GETLOST",
	"grozba2": "T_GETLOST2",
	"ho": "T_COMEOVERHERE",
	"smiech1": "T_LAUGHING",
	"smiech2": "T_LACHEN",
	"krzyk": "T_OKRZYK",
	"zastraszony": "T_FRIGHTENED",
	"zawiedziony": "T_ZAWIEDZIONY",
	"strach": "T_STRACH",
	"obolaly": "T_WJAJA",
	"mdlenie": "T_VICTIM"
	"kucie": "S_BSANVIL_S1"
	"ostrzenie": "S_BSSHARP_S1"
	"schladzaniemiecza": "S_BSCOOL_S1"
	"grzaniepreta": "S_BSFIRE_S1"
};

Config["stealableItems"] <- [
	"ITMI_OLDCOIN",
]

Config["NotificationBoard"] <- [
	//archolos
	{
		x = -87686.1,
		y = 714.176,
		z = -120379,
		angle = 152.212,
		corners = [{
				x = -87828.4,
				y = 761.276,
				z = -120359
			},
			{
				x = -87824.1,
				y = 629.058,
				z = -120355
			},
			{
				x = -87595.4,
				y = 761.316,
				z = -120416
			},
			{
				x = -87583.8,
				y = 637.547,
				z = -120408
			},
		],
		size = [5, 6],
		fractionIds = [0, 1, 2, 4, 5, 6],
		camera = {
			x = -87686.1,
			y = 714.176,
			z = -120379,
			rotx = 152.212
		},
	},
	{
		x = -95076.9,
		y = 244.128,
		z = -117234,
		angle = 152.212,
		corners = [{
				x = -95088.8,
				y = 293.319,
				z = -117342
			},
			{
				x = -95088.8,
				y = 173.206,
				z = -117343
			},
			{
				x = -95058.8,
				y = 296.487,
				z = -117199
			},
			{
				x = -95058.8,
				y = 173.693,
				z = -117111
			},
		],
		size = [5, 6],
		fractionIds = [0, 1, 2, 4, 5, 6],
		camera = {
			x = -94855.8,
			y = 311.22,
			z = -117199,
			rotx = 152.212
		},
	},
	{
		x = -109080,
		y = -550.366,
		z = -111879,
		angle = 152.212,
		corners = [{
				x = -109291,
				y = -555.122,
				z = -111656
			},
			{
				x = -109291,
				y = 666.625,
				z = -111656
			},
			{
				x = -109068,
				y = -551.467,
				z = -111618
			},
			{
				x = -109068,
				y = -657.649,
				z = -111618
			},
		],
		size = [5, 6],
		fractionIds = [0, 1, 2, 4, 5, 6],
		camera = {
			x = -109080.8,
			y = -550.366,
			z = -111879,
			rotx = 152.212
		},
	},
]

Config["Questions"] <- [{
		id = 1,
		question = "Kt�rego z czat�w powiniene� u�y�, by napisa� co� jako twoja posta�?",
		answers = [
			["Czatu ALL", false],
			["Czatu OOC", false],
			["Czatu IC", true],
			["Czatu RP", false],
		]
	},
	{
		id = 2,
		question = "Kt�rego z czat�w powiniene� u�y�, by spyta� si� kolegi o prac� domow� do szko�y?",
		answers = [
			["Czatu ALL", false],
			["Czatu OOC", true],
			["Czatu IC", false],
			["Czatu RP", false],
		]
	},
	{
		id = 3,
		question = "Kt�rego z czat�w powiniene� u�y�, by wyrazi� niesmak do matki innego gracza?",
		answers = [
			["Czatu IC, nie zapominaj�c r�wnie� o ojcu", false],
			["�adnego. Je�li ju� chc� co� wyja�ni�, to we w�asnym zakresie lub poprzez ticketa \nze skarg� do administracji", true],
			["Czatu OOC, lecz powinienem wyrazi� niesmak do osoby gracza, a nie jego matki", false],
			["Nie u�yje do tego czatu w grze, lecz oficjalnego Discorda HK", false],
		]
	},
	{
		id = 4,
		question = "Kolega w prywatnej wiadomo�ci wys�a� Ci informacje na temat planowanej zasadzce na twoj� posta�. \nCo powiniene� z t� informacj� zrobi�?",
		answers = [
			["Wykorzystam t� wiadomo��, by uciec. Tylko przed tym odegram, �e dosta�em \ngo��bia pocztowego", false],
			["Jest to MetaGaming, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", true],
			["Jest to PowerGaming, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", false],
			["Jest to CombatLog, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", false],
		]
	},
	{
		id = 5,
		question = "Co to jest MG, czyli MetaGaming?",
		answers = [
			["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", true],
			["Zmuszenie kogo� do akcji RP, narzucanie komu� danej gry, akcj� RP. Np. /me rzuca w \nniego no�em, tak by zgin�� itp. Absolutnie zakazane", false],
			["Wychodzenie z roli, b��dne odegranie akcji RP", false],
			["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
		]
	},
	{
		id = 6,
		question = "Nowy gracz zacz�� Ci� uderza� broni� obuchow� bez odegrania. Jest to \nprzyk�ad jakiego� poj�cia RP?",
		answers = [
			["Spawn Kill", false],
			["Combat Gaming (CG)", false],
			["Deathmatch (DM)", false],
			["Random Deathmatch (RDM)", true],
		]
	},
	{
		id = 7,
		question = "Czy skoczenie do stogu siana z klifu sprawia, �e twoja posta� prze�yje?",
		answers = [
			["Tak, je�li napisz� KP assasyna", false],
			["Tak, je�li mam lekki pancerz, inaczej nie", false],
			["Zale�y od wysoko�ci, ale z regu�y nie. Moja posta� to zwyk�y cz�owiek, a nie bohater gry akcji", true],
			["Zale�y czy dobrze wykonam fiko�a na /me, to nie takie trudne", false],
		]
	},
	{
		id = 8,
		question = "Chcesz kogo� zaatakowa�, jakie warunki musisz spe�ni�?",
		answers = [
			["Atakujemy si�, gdy obie strony wyci�gn� bro�", false],
			["Napisz� narracj� na me i/lub do. Zaatakuj�, gdy druga strona te� wyci�gnie bro�.\nNie dotyczy akcji dynamicznych, np. zasadzek", true],
			["W ka�dej sytuacji atakuje od razu, bo to akcja dynamiczna", false],
			["Napisz� narracj� na me i/lub do. Zaatakuj�, gdy druga strona te� wyci�gnie bro�", false],
		]
	},
	{
		id = 9,
		question = "Co to jest PG, czyli PowerGaming?",
		answers = [
			["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
			["Zmuszenie kogo� do akcji RP, narzucanie komu� danej gry, akcj� RP. Np. /me rzuca w niego no�em,\n tak by zgin�� itp. Absolutnie zakazane", true],
			["Wychodzenie z roli, b��dne odegranie akcji RP", false],
			["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z akcj� pr�ba zemsty.", false],
		]
	},
	{
		id = 10,
		question = "Co to jest BW, czyli Brutally Wounded?",
		answers = [
			["omdlenie, czy odniesienie powa�nych ran uniemo�liwiaj�cych poruszanie si� (pasek HP schodzi do 0)", true],
			["Atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. \nJest to umy�lne atakowanie innych os�b bez powodu IC", false],
			["Losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie� dotrze�. \nAbsolutnie zakazane", false],
			["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty.", false],
		]
	},
	{
		id = 11,
		question = "Co to jest CK, czyli Character Kill?",
		answers = [
			["Niewymuszone u�miercenie postaci", true],
			["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, \nza pozwoleniem administracji.", false],
			["Atakowanie gracza", false],
			["Nijaczenie postaci", false],
		]
	},
	{
		id = 12,
		question = "Co to jest FCK, czyli Forced Character Kill?",
		answers = [
			["Niewymuszone u�miercenie postaci", false],
			["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, \nza pozwoleniem administracji  ", true],
			["Atakowanie gracza", false],
			["Nijaczenie postaci", false],
		]
	},
	{
		id = 13,
		question = "Co to jest DM, czyli DeathMatch?",
		answers = [
			["Losowe atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. Jest \nto umy�lne atakowanie innych os�b z zamiarem wbicia BW, bez powodu IC Absolutnie zakazane", false],
			["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, za \npozwoleniem administracji", false],
			["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", true],
			["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty", false],
		]
	},
	{
		id = 14,
		question = "Co to jest RDM, czyli Random DeathMatch?",
		answers = [
			["Losowe atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. Jest \nto umy�lne atakowanie innych os�b z zamiarem wbicia BW, bez powodu IC Absolutnie zakazane", true],
			["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, za \npozwoleniem administracji", false],
			["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", false],
			["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty", false],
		]
	},
	{
		id = 15,
		question = "Co to jest CN, czyli Celebrity Name?",
		answers = [
			["Nazwanie swojej postaci Exodus lub Szakal. Absolutnie zakazane", false],
			["Nazwanie swojej postaci niezgodnie z uniwersum Gothic, szczeg�lnie imieniem \nosoby znanej. Np. Justin Bieber. Absolutnie zakazane", true],
			["Zaklepanie imienia postaci na serwerze", false],
			["Tzw. leakowanie danych osobowych innego gracza. Absolutnie zakazane, mo�e \nsi� sko�czy� s�dem", false],
		]
	},
	{
		id = 16,
		question = "Co to jest BH, czyli Bunny Hop?",
		answers = [
			["U�ywanie nazwy jakiego� zwierz�cia jako imi� postaci. Np. Kr�lik, zaj�c", false],
			["U�ywanie kamery trzecioosobowej do akcji RP. Wychylanie zza rogu itp., \nby by� niewidocznym. Absolutnie zakazane", false],
			["Nienaturalne u�ywanie skakania, szczeg�lnie skakanie po dachach itp., \nby szybciej dotrze� w dane miejsce. Absolutnie zakazane", true],
			["Omdlenie, czy odniesienie powa�nych ran uniemo�liwiaj�cych poruszanie \nsi� (pasek HP schodzi do 0)", false],
		]
	},
	{
		id = 17,
		question = "Czy w ci�kim pancerzu mo�esz wykonywa� czynno�ci takie jak \nwspinanie si� po g�rach, czy p�ywanie?",
		answers = [
			["Tak", false],
			["Nie, mog� tylko p�ywa�", false],
			["Tak, lecz wi��e si� to z konsekwencjami IC", true],
			["Nie, mog� tylko chodzi� po g�rach", false],
		]
	},
	{
		id = 18,
		question = "Co to jest CL, czyli Combat Log?",
		answers = [
			["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", true],
			["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", false],
			["Jest to umy�lne atakowanie innych os�b z zamiarem wbicia BW, \nbez powodu IC. Absolutnie zakazane", false],
			["Logi ka�dego uderzenia innego gracza, dost�pne tylko dla administracji", false],
		]
	},
	{
		id = 19,
		question = "Co to jest RK, czyli Revenge Kill?",
		answers = [
			["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
			["Wychodzenie z roli, b��dne odegranie akcji RP", false],
			["�mier� postaci", false],
			["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna\n z akcj� pr�ba zemsty. Absolutnie zakazane", true],
		]
	},
	{
		id = 20,
		question = "Co to jest Cam Spying?",
		answers = [
			["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu\n kamery bez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", true],
			["Wychodzenie z roli, b��dne odegranie akcji RP", false],
			["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", false],
			["Odgrywanie kamer w Gothicu. Absolutnie zakazane", false],
		]
	},
	{
		id = 21,
		question = "Co to jest Fail RP?",
		answers = [
			["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
			["losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie�\n dotrze�. \nAbsolutnie zakazane", false],
			["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
			["Wychodzenie z roli, b��dne odegranie akcji RP", true],
		]
	},
	{
		id = 22,
		question = "Co to jest Scouting?",
		answers = [
			["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu \nkamery bez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", false],
			["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
			["losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie� dotrze�. \nAbsolutnie zakazane", false],
			["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", true],
		]
	},
	{
		id = 0,
		question = "Co to jest Ghost Town (Miasto Duch�w)?",
		answers = [
			["B��dne zak�adanie przez ludzi, �e miejsca fabularne zaludnione, takie jak miasta \npomimo braku graczy s� puste", true],
			["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
			["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu kamery \nbez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", false],
			["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", false],
		]
	}
];

Config["Calendar"] <- {};
Config["Calendar"].yearDays <- 305;
Config["Calendar"]["Seasons"] <- ["�niwa Innosa", "Ciemno�� Beliara", "R�wnowaga Adannosa"]
Config["Calendar"]["�niwa Innosa"] <- {
	months = [30, 31, 30, 31, 30],
	buffToHerbGrowth = 10,
	buffToMineGrowth = 0,
	buffToForestGrowth = 5,
	buffToFishGrowth = 0,
};
Config["Calendar"]["Ciemno�� Beliara"] <- {
	months = [31, 30, 31],
	buffToHerbGrowth = 0,
	buffToMineGrowth = 10,
	buffToForestGrowth = 0,
	buffToFishGrowth = 0,
};
Config["Calendar"]["R�wnowaga Adannosa"] <- {
	months = [30, 31],
	buffToHerbGrowth = 10,
	buffToMineGrowth = 0,
	buffToForestGrowth = 10,
	buffToFishGrowth = 10,
};

Config["PlayerRecovery"] <- [{
		name = "CHAIR_1_NC.ASC",
		value = 0.020
	},
	{
		name = "BENCH_1_NC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_1_PC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_3_PC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_1_OC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_2_OC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_3_OC.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_NW_NORMAL_01.ASC",
		value = 0.020
	},
	{
		name = "CHAIR_NW_EDEL_01.ASC",
		value = 0.020
	},
	{
		name = "BENCH_1_NC.ASC",
		value = 0.020
	},
	{
		name = "BENCH_1_OC.ASC",
		value = 0.020
	},
	{
		name = "BENCH_2_OC.ASC",
		value = 0.020
	},
	{
		name = "BENCH_NW_OW_01.ASC",
		value = 0.020
	},
	{
		name = "BENCH_NW_CITY_01.ASC",
		value = 0.020
	},
	{
		name = "BENCH_NW_CITY_02.ASC",
		value = 0.020
	},
	{
		name = "BENCH_NW_CITY_01.ASC",
		value = 0.020
	},
	{
		name = "BENCH_NW_CITY_01.ASC",
		value = 0.020
	},
	{
		name = "BENCH_3_OC.ASC",
		value = 0.020
	},
	{
		name = "BENCH_THRONE.ASC",
		value = 0.050
	},
	{
		name = "THRONENEW_RICH.ASC",
		value = 0.050
	},
	{
		name = "BEDHIGH_PSI.ASC",
		value = 0.1
	},
	{
		name = "THRONE_NW_CITY_01.ASC",
		value = 0.050
	},
	{
		name = "BEDHIGH_PC.ASC",
		value = 0.1
	},
	{
		name = "BEDHIGH_1_OC.ASC",
		value = 0.15
	},
	{
		name = "BEDHIGH_3_OC.ASC",
		value = 0.25
	},
	{
		name = "BEDHIGH_4_OC.ASC",
		value = 0.25
	},
	{
		name = "SMOKE_WATERPIPE.MDS",
		value = 0.2
	},
	{
		name = "BEDHIGH_2_OC.ASC",
		value = 0.2
	},
	{
		name = "BATHTUB_WOODEN_BIGGER.ASC",
		value = 0.15
	},
	{
		name = "BEDHIGH_NW_NORMAL_01.ASC",
		value = 0.3
	},
	{
		name = "BEDHIGH_NW_EDEL_01.ASC",
		value = 0.4
	},
	{
		name = "BEDHIGH_NW_MASTER_01.ASC",
		value = 0.4
	},
	{
		name = "BEDHIGH_LOVE_OC.ASC",
		value = 0.35
	},
];

// Config["Barrier"] <- [
[57939.2, 1280.28, ],
[55954.4, 5421.51, ],
[52856.8, 10047, ],
[49451.9, 14908.2, ],
[44199.8, 20513.3, ],
[37684.2, 26271.2, ],
[30434, 31462.4, ],
[25573.6, 32692.7, ],
[21248.3, 35176.1, ],
[19450.7, 35205, ],
[16263.1, 32799.6, ],
[10755.6, 34744.4, ],
[9736.9, 37990.5, ],
[8218.6, 38393.1, ],
[4065, 39018.4, ],
[839.9, 39079.3, ],
[-9312.9, 38694.2, ],
[-19258.3, 40991.4, ],

[-28570.3, 47572, ],
//[ -29684.1, 40535.7, ],
//[ -42060.9, 41084, ],

//[ -39313.7, 36558.8, ],
[-49319.6, 31970.2, ],
[-54137.3, 26761.7, ],
[-62089.3, 21598.1, ],
[-66193.7, 12999.2, ],
[-66132.3, 6204, ],
[-63855.2, -5700.8, ],
[-59385.1, -10081.5, ],
[-56013.8, -22393.4, ],
[-47250.3, -28502, ],
[-37136.5, -38319.2, ],
[-24664.7, -46687.9, ],
[-7860.6, -48966.6, ],
[4876.6, -49691, ],
[23147.8, -47875.1, ],
[48722.3, -39488.8, ],
[55902.4, -31909.8, ],
[61238.6, -23412.8, ],
[60230.1, -6641.9, ],
//];

Config["TwistedVobs"] <- [
	"CHESTSMALL_NW_POOR_LOCKED.MDS",
	"CHESTBIG_NW_RICH_LOCKED.MDS",
	"CHESTSMALL_OCCHESTSMALL.MDS",
	"CHESTBIG_NW_NORMAL_LOCKED.MDS",
	"CHESTSMALL_OCCHESTSMALLLOCKED.MDS",
	"CHESTBIG_OCCHESTLARGELOCKED.MDS",
	"CHESTSMALL_NW_POOR_OPEN.MDS",
	"CHESTBIG_OCCHESTMEDIUM.MDS",
	"CHESTSMALL_OCCRATESMALLLOCKED.MDS",
	"CHESTBIG_OCCRATELARGELOCKED.MDS",
	"CHESTBIG_NW_RICH_OPEN.MDS",
	"CHESTBIG_NW_RICH_LOCKED.ASC",
];

Config["CraftVobRelation"] <- {
	"BOOK_BLUE.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BOOK_NW_CITY_CUPBOARD_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_CUPBOARD_RICH_01.3DS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_CUPBOARD_RICH_02.3DS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"THRONENEW_RICH.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"THRONE_BIG.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"THRONE_NW_CITY_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_PSI.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_PC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_1_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_3_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_4_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_2_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_NW_NORMAL_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_NW_EDEL_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BEDHIGH_NW_MASTER_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHESTSMALL_NW_POOR_LOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 200
	}
	"CHESTBIG_NW_NORMAL_LOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLRLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 600
	}
	"CHESTBIG_NW_RICH_LOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLRLRLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 800
	}
	"CHESTSMALL_OCCHESTSMALLLOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 350
	}
	"CHESTBIG_OCCHESTLARGELOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 400
	}
	"CHESTSMALL_OCCRATESMALLLOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 100
	}
	"CHESTBIG_OCCRATELARGELOCKED.MDS": {
		canBeInteractedWith = true,
		lockable = true,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 250
	}
	"NW_CITY_CART_01.3DS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 100
	}
	"KM_VOB_BARRELCART.3DS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 100
	}
	"THRONE_NW_CITY_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 100
	}
	"THRONENEW_RICH.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRLR",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 100
	}
	"LADDER_2.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"LADDER_3.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"LADDER_4.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"SMOKE_WATERPIPE.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_THRONE.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_3_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_1_NC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_1_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_2_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_NW_OW_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_NW_CITY_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BENCH_NW_CITY_02.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"INNOS_NW_MISC_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"INNOS_ADANOS_KM.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}

	"CHAIR_2_NC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_1_NC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_1_PC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_3_PC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_1_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_2_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_3_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_NW_NORMAL_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CHAIR_NW_EDEL_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_MISC_FIELDGRAVE_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_MISC_GRAVESTONE_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_MISC_GRAVESTONE_02.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_CUPBOARD_POOR_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 40.50
	}
	"NW_CITY_CUPBOARD_POOR_02.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 40.50
	}
	"NW_CITY_TABLE_PEASANT_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_TABLE_NORMAL_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"DT_TABLE_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_TABLE_RICH_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_TABLE_RICH_02.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"OC_TABLE_V2.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"OM_ORETABLE.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"PC_LOB_TABLE1.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"PC_LOB_TABLE2.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSANVIL_OC.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSCOOL_OC.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSSHARP_OC.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BARBQ_SCAV.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CAULDRON_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"STOVE_NW_CITY_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"PAN_OC.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"LAB_PSI.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"LATI_G1.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"ARMORSTAND_PLATTNER.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSMELTER_1.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSFIRE_OC.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSMELTER_1.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"WOODCHOPPIN_NORMAL.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"MOERS_MESH.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BOWSDW_WBENCH.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"TISCHLER_MESH.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"WEBSTUHL_USE.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"SKINLAB_PLACE.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"MOERS_LEKARZ.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"SAEGEN_3.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"FISCHGRILLEN_FINAL.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"HERB_PSI.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSANVIL_OC2.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"LAB_PSI3.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BUCHSCHREIBEN_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"HERB_NW_MISC_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"NW_CITY_MAP_WAR_CLOSED_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"TRINKFASSS_MESH.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 30
	}
	"OC_BARREL_V1.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 30
	}
	"NW_HARBOUR_CRATE_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = true,
		weight = 80
	}
	"NW_CITY_DECO_SHADOWHEAD_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}

	"HK_VOB_BEAR_HEAD.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}

	"NW_CITY_DECO_TROLLHEAD_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}
	"NW_CITY_DECO_SWORDFISH_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}
	"NW_CITY_DECO_WOLFHEAD_01.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}
	"OC_DECORATE_V2.3DS": {
		canBeInteractedWith = false,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 30
	}
	"BEDHIGH_LOVE_OC.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"FISCHSCHNEIDEN_RZEZNIK.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BAKE_MESH.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"RMELTER_1.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"RMAKER_1.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"CAULDRON_OC2.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BAUMSAEGE_1.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"STOVE_PO_OUTDOOR_01.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"STOMPER_OM.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BATHTUB_WOODEN_BIGGER.ASC": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
	"BSFIRE_PROJEKTO_AUSSEN_VAR1.MDS": {
		canBeInteractedWith = true,
		lockable = false,
		combination = "RLRL",
		destroyable = false,
		health = 10,
		canbechest = false,
		weight = 40.50
	}
}

Config["BedVobs"] <- [
	"BEDHIGH_PSI.ASC",
	"BEDHIGH_PC.ASC",
	"BEDHIGH_1_OC.ASC",
	"BEDHIGH_3_OC.ASC",
	"BEDHIGH_4_OC.ASC",
	"BEDHIGH_2_OC.ASC",
	"BEDHIGH_NW_NORMAL_01.ASC",
	"BEDHIGH_NW_EDEL_01.ASC",
	"BEDHIGH_NW_MASTER_01.ASC",
	"BEDHIGH_LOVE_OC.ASC"
]


Config["Rats"] <- {
	active = false, // is active module
	cooldown = 1800, // seconds after new rat will apear
	slots = 4, // max rats on server
	positions = [ // positions
		//[-3181.72,-671.719,3590.55],
		//[142.266,-1091.72,3074.3],
		//[-1303.52,-900.938,-6650.55],
		// [1026.17,-771.844,4843.44],
		// [1576.64,-671.797,4705.78],
		// [1524.06,-671.797,5561.56],
		// [-38768.7,44.1406,-5216.48],
		//[-7890,-441.797,-25.2344],
		// [1718.36,-371.797,-6669.53],
		// [-2780.7,-531.797,4560.7],
	],
	items = [
		"ITCR_SDRP_DROP_37",
		"ITCR_SDRP_DROP_64",
		"ITCR_SDRP_DROP_38",
		"ITCR_SDRP_DROP_71",
		"ITCR_SDRP_DROP_36",
		"ITCR_SDRP_DROP_69",
		"ITCR_SDRP_DROP_81",
		"ITCR_SDRP_DROP_31",
		"ITCR_SDRP_DROP_66",
		"ITCR_SDRP_DROP_51",
		"ITCR_SDRP_DROP_45",
		"ITCR_SDRP_DROP_42",
		"ITCR_SDRP_DROP_33",
		"ITCR_SDRP_DROP_30",
		"ITCR_SDRP_DROP_49",
		"ITCR_SDRP_DROP_65",
		"ITCR_SDRP_DROP_63",
		"ITCR_SDRP_DROP_68",
		"ITCR_SDRP_DROP_39",
		"ITCR_SDRP_DROP_70",
		"ITCR_SDRP_DROP_43",
		"ITCR_SDRP_DROP_47",
		"ITCR_SDRP_DROP_40",
		"ITCR_SDRP_DROP_83",
		"ITCR_SDRP_DROP_84",
		"ITCR_SDRP_DROP_44",
		"ITCR_SDRP_DROP_08",
		"ITCR_SDRP_DROP_22",
		"ITCR_SDRP_DROP_77",
		"ITCR_SDRP_DROP_78",
		"ITCR_SDRP_DROP_06",
		"ITCR_SDRP_DROP_24",
		"ITCR_SDRP_DROP_41",
		"ITCR_SDRP_DROP_07"
	],
}




Config["PriceSchemes"] <- []
/*
enum BuffType
{
    None,
    Custom,
    Attribute,
        attributeId = PlayerAttribute.Str
    Skill,
        skillId = PlayerSkill.Alchemy
    HealthReduce,
    HealthRegeneration,
    ManaReduce,
    ManaRegeneration,
    EnduranceRegeneration,
    StaminaRegeneration,
    WeightLimit,
    DamageBonus,
    DamageReduce,
    ProtectionBonus,
    ProtectionReduce,
    Mds,
}

*/
Config["Buffs"] <- {


	"Adanos1": { //Na czas 30 min zwi�ksza Man� +100
		name = "Adanos1",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Mana,
		value = 100,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 1800,
	},

	"Adanos2": { //Na czas 30 min zwi�ksza stamin� +25
		name = "Adanos2",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Stamina,
		value = 25,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 1800,
	},

	"Innos1": { //Na czas 30 min zwi�ksza hp +50
		name = "Innos1",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Hp,
		value = 50,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 1800,
	},

	"Innos2": { //Na czas 30 min zwi�ksza si�� +5
		name = "Innos2",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Str,
		value = 5,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 1800,
	},

	"Innos3": { //Na czas 30 min zwi�ksza zr�czno�� +5
		name = "Innos3",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Dex,
		value = 5,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 1800,
	},

	"Wol": { //TIER 3 - zwi�ksza udzwig o 30 na 10 minut
		name = "Wol",
		type = BuffType.WeightLimit,
		value = 30,
		interval = 0,
		icon = "BAG2_EQ_ICON_R.TGA",
		time = 600,
	},

	"Gornik": { //TIER 3/TIER 4 - zwi�ksza lvl g�rnika o jeden
		name = "Gornik",
		type = BuffType.Skill,
		skillId = PlayerSkill.Miner,
		value = 1,
		interval = 0,
		icon = "PICK_EQ_ICON_R.TGA",
		time = 300,
	},

	"Rybak": { //TIER 3/TIER 4 - zwi�ksza lvl rybaka o jeden
		name = "Rybak",
		type = BuffType.Skill,
		skillId = PlayerSkill.Fisherman,
		value = 1,
		interval = 0,
		icon = "PICK_EQ_ICON_R.TGA",
		time = 300,
	},

	"Drwal": { //TIER 3/TIER 4 - zwi�ksza lvl drwala o jeden
		name = "Drwal",
		type = BuffType.Skill,
		skillId = PlayerSkill.Lumberjack,
		value = 1,
		interval = 0,
		icon = "PICK_EQ_ICON_R.TGA",
		time = 300,
	},

	"Mag": { //TIER 3/TIER 4 - zwi�ksza lvl kr�gu magii o jeden
		name = "Mag",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.MagicLvl,
		value = 1,
		interval = 0,
		icon = "PICK_EQ_ICON_R.TGA",
		time = 300,
	},

	"Biegacz": { //TIER 2 - regeneruje 4 biegania
		name = "Biegacz",
		type = BuffType.EnduranceRegeneration,
		value = 20,
		interval = 1,
		icon = "PLAYER_EQ_ICON_R.TGA",
		time = 30,
	},

	"Laknienie": { //TIER 2 - regeneruje 2 stamy co 30 sekund na 10 minut czyli 40 staminy
		name = "Laknienie",
		type = BuffType.StaminaRegeneration,
		value = 2,
		interval = 30,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 600,
	},

	"Laknienie2": { //TIER 2 - regeneruje 3 stamy co 30 sekund na 10 minut czyli 60 staminy
		name = "Laknienie2",
		type = BuffType.StaminaRegeneration,
		value = 3,
		interval = 30,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 600,
	},

	"Stama2": { //TIER 2 - regeneruje 2 stamy
		name = "Stama2",
		type = BuffType.StaminaRegeneration,
		value = 0.4,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama3": { //TIER 2 - regeneruje 3 stamy
		name = "Stama3",
		type = BuffType.StaminaRegeneration,
		value = 0.6,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama4": { //TIER 2 - regeneruje 4 stamy
		name = "Stama4",
		type = BuffType.StaminaRegeneration,
		value = 0.8,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama5": { //TIER 2 - regeneruje 5 stamy
		name = "Stama5",
		type = BuffType.StaminaRegeneration,
		value = 1,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama6": { //TIER 2 - regeneruje 6 stamy
		name = "Stama6",
		type = BuffType.StaminaRegeneration,
		value = 1.2,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama7": { //TIER 2 - regeneruje 7 stamy
		name = "Stama7",
		type = BuffType.StaminaRegeneration,
		value = 1.4,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama8": { //TIER 2/TIER 3 - regeneruje 8 stamy
		name = "Stama8",
		type = BuffType.StaminaRegeneration,
		value = 1.6,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama9": { //TIER 2/TIER 3 - regeneruje 9 stamy
		name = "Stama9",
		type = BuffType.StaminaRegeneration,
		value = 1.8,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama10": { //- regeneruje 10 stamy
		name = "Stama10",
		type = BuffType.StaminaRegeneration,
		value = 2,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},


	"Stama12": { //TIER 3 - regeneruje 12 stamy
		name = "Stama12",
		type = BuffType.StaminaRegeneration,
		value = 2.4,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama15": { //TIER 3/TIER 4 - regeneruje 15 stamy
		name = "Stama15",
		type = BuffType.StaminaRegeneration,
		value = 3,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama20": { //TIER 4 - regeneruje 20 stamy
		name = "Stama20",
		type = BuffType.StaminaRegeneration,
		value = 4,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama30": { //TIER 5 - regeneruje 30 stamy
		name = "Stama30",
		type = BuffType.StaminaRegeneration,
		value = 6,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama35": { //TIER 5 - regeneruje 35 stamy
		name = "Stama35",
		type = BuffType.StaminaRegeneration,
		value = 7,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Stama50": { //TIER 5 - regeneruje 50 stamy
		name = "Stama50",
		type = BuffType.StaminaRegeneration,
		value = 10,
		interval = 10,
		icon = "FOOD_EQ_ICON_R.TGA",
		time = 50,
	},

	"Rybahp": { //Z DROPU - redukuje 5 hp
		name = "Rybahp",
		type = BuffType.HealthReduce,
		value = 1,
		interval = 20,
		icon = "WIND_EQ_ICON_R.TGA",
		time = 100,
	},

	"Rybamana": { //Z DROPU - redukuje 5 many
		name = "Rybamana",
		type = BuffType.ManaReduce,
		value = 1,
		interval = 20,
		icon = "LIGHTNING_ICONS.TGA",
		time = 100,
	},

	"Srybahp": { //Z DROPU - regeneruje 15 hp
		name = "Srybahp",
		type = BuffType.HealthRegeneration,
		value = 3,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 50,
	},

	"Herbatka": { //Z DROPU - regeneruje 35 hp
		name = "Herbatka",
		type = BuffType.HealthRegeneration,
		value = 5,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 70,
	},

	"Herbatka2": { //Z DROPU - regeneruje 75 hp
		name = "Herbatka2",
		type = BuffType.HealthRegeneration,
		value = 5,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 150,
	},

	"Herbatka3": { //Z DROPU - regeneruje 150 hp
		name = "Herbatka3",
		type = BuffType.HealthRegeneration,
		value = 15,
		interval = 20,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 200,
	},

	"Srybamana": { //Z DROPU - regeneruje 15 many
		name = "Srybamana",
		type = BuffType.ManaRegeneration,
		value = 3,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 50,
	},

	"Nieprzetworzonehp": { //Z DROPU - redukuje 10 hp
		name = "Nieprzetworzonehp",
		type = BuffType.HealthReduce,
		value = 1,
		interval = 10,
		icon = "WIND_EQ_ICON_R.TGA",
		time = 100,
	},

	"Nieprzetworzonehp": { //Z DROPU - redukuje 10 hp
		name = "Nieprzetworzonehp",
		type = BuffType.HealthReduce,
		value = 1,
		interval = 10,
		icon = "WIND_EQ_ICON_R.TGA",
		time = 100,
	},


	"1HMieczBoost": { //Z DROPU - redukuje 10 many
		name = "1HMieczBoost",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.OneH,
		value = 10,
		interval = 0,
		icon = "LIGHTNING_ICONS.TGA",
		time = 600,
	},

	"2HMieczBoost": { //Z DROPU - redukuje 10 many
		name = "2HMieczBoost",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.TwoH,
		value = 10,
		interval = 0,
		icon = "LIGHTNING_ICONS.TGA",
		time = 600,
	},

	"Surowehp": { //Z DROPU - redukuje 20 hp
		name = "Surowehp",
		type = BuffType.HealthReduce,
		value = 2,
		interval = 10,
		icon = "WIND_EQ_ICON_R.TGA",
		time = 100,
	},

	"Surowemana": { //Z DROPU - redukuje 20 many
		name = "Surowemana",
		type = BuffType.ManaReduce,
		value = 2,
		interval = 10,
		icon = "LIGHTNING_ICONS.TGA",
		time = 100,
	},

	"Zgnilehp": { //Z DROPU - redukuje 40 hp
		name = "Zgnilehp",
		type = BuffType.HealthReduce,
		value = 4,
		interval = 10,
		icon = "WIND_EQ_ICON_R.TGA",
		time = 100,
	},

	"Zgnilemana": { //Z DROPU - redukuje 40 many
		name = "Zgnilemana",
		type = BuffType.ManaReduce,
		value = 4,
		interval = 10,
		icon = "LIGHTNING_ICONS.TGA",
		time = 100,
	},

	"Roslinahp": { //TIER 0 - regeneruje 3 hp
		name = "Roslinahp",
		type = BuffType.HealthRegeneration,
		value = 1,
		interval = 20,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 60,
	},

	"Jablkohp": { //TIER 0 - regeneruje 5 hp
		name = "Jablkohp",
		type = BuffType.HealthRegeneration,
		value = 1,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 50,
	},
	"Zupahp": { //TIER 0 - regeneruje 7 hp
		name = "Zupahp",
		type = BuffType.HealthRegeneration,
		value = 1,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 70,
	},

	"Pieczonehp": { //TIER 0 - regeneruje 18 hp
		name = "Pieczonehp",
		type = BuffType.HealthRegeneration,
		value = 3,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 60,
	},

	"Potrawahp": { //TIER 1 - regeneruje 25 hp
		name = "Potrawahp",
		type = BuffType.HealthRegeneration,
		value = 5,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 50,
	},

	"Daniehp": { //TIER 2 - regeneruje 50 hp
		name = "Daniehp",
		type = BuffType.HealthRegeneration,
		value = 5,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg75hp": { //TIER 2 - regeneruje 75 hp
		name = "Reg75hp",
		type = BuffType.HealthRegeneration,
		value = 7.5,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg100hp": { //TIER 2 - regeneruje 100 hp
		name = "Reg100hp",
		type = BuffType.HealthRegeneration,
		value = 10,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg150hp": { //TIER 2 - regeneruje 150 hp
		name = "Reg150hp",
		type = BuffType.HealthRegeneration,
		value = 15,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg200hp": { //TIER 3 - regeneruje 200 hp
		name = "Reg200hp",
		type = BuffType.HealthRegeneration,
		value = 20,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg300hp": { //TIER 4 - regeneruje 300 hp
		name = "Reg300hp",
		type = BuffType.HealthRegeneration,
		value = 30,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg400hp": { //TIER 5 - regeneruje 400 hp
		name = "Reg400hp",
		type = BuffType.HealthRegeneration,
		value = 40,
		interval = 10,
		icon = "PLUS_EQ_ICON_R.TGA",
		time = 100,
	},

	"Posilekmaxhp": { //TIER 3 - dodaje 15 max hp
		name = "Posilekmaxhp",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Hp,
		value = 15,
		interval = 0,
		icon = "HP_EQ_ICON_R.TGA",
		time = 120,
	},

	"Strawamaxhp": { //TIER 4 - dodaje 30 max hp
		name = "Strawamaxhp",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Hp,
		value = 30,
		interval = 0,
		icon = "HP_EQ_ICON_R.TGA",
		time = 120,
	},

	"Krewhp": { //TIER 4 - dodaje 30 max hp
		name = "Krewhp",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Hp,
		value = 30,
		interval = 0,
		icon = "HP_EQ_ICON_R.TGA",
		time = 900,
	},

	"Chemolmana": { //TIER 0 - regeneruje 10 many
		name = "Chemolmana",
		type = BuffType.ManaRegeneration,
		value = 1,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Skretmana": { //TIER 1 - regeneruje 30 many
		name = "Skretmana",
		type = BuffType.ManaRegeneration,
		value = 3,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Jointmana": { //TIER 2 - regeneruje 50 many
		name = "Jointmana",
		type = BuffType.ManaRegeneration,
		value = 5,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg75m": { //Z DROPU - regeneruje 75 many
		name = "Reg75m",
		type = BuffType.ManaRegeneration,
		value = 7.5,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg100m": { //Z DROPU - regeneruje 100 many
		name = "Reg100m",
		type = BuffType.ManaRegeneration,
		value = 10,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg150m": { //Z DROPU - regeneruje 150 many
		name = "Reg150m",
		type = BuffType.ManaRegeneration,
		value = 15,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg200m": { //Z DROPU - regeneruje 200 many
		name = "Reg200m",
		type = BuffType.ManaRegeneration,
		value = 20,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Reg250m": { //Z DROPU - regeneruje 250 many
		name = "Reg250m",
		type = BuffType.ManaRegeneration,
		value = 25,
		interval = 10,
		icon = "HAND2_EQ_ICON_R.TGA",
		time = 100,
	},

	"Ganjamaxmana": { //TIER 3 - dodaje 30 max many
		name = "Ganjamaxmana",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Mana,
		value = 30,
		interval = 0,
		icon = "EYE_EQ_ICON_R.TGA",
		time = 120,
	},

	"Ziolomaxmana": { //TIER 4 - dodaje 50 max many
		name = "Ziolomaxmana",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Mana,
		value = 50,
		interval = 0,
		icon = "EYE_EQ_ICON_R.TGA",
		time = 120,
	},

	"Krewmana": { //TIER 4 - dodaje 50 max many
		name = "Krewmana",
		type = BuffType.Attribute,
		attributeId = PlayerAttributes.Mana,
		value = 50,
		interval = 0,
		icon = "EYE_EQ_ICON_R.TGA",
		time = 900,
	},

	"Sprint": { //TIER 3 - umo�liwia sprint na 1 minut�
		name = "Sprint",
		type = BuffType.Mds,
		mds = "HUMANS_SPRINT.MDS",
		value = 0,
		interval = 0,
		icon = "BOOTS_EQ_ICON_R.TGA",
		time = 60,
	},

	"Sprint2": { //TIER 4 - umo�liwia sprint na 2 minuty
		name = "Sprint2",
		type = BuffType.Mds,
		mds = "HUMANS_SPRINT.MDS",
		value = 0,
		interval = 0,
		icon = "BOOTS_EQ_ICON_R.TGA",
		time = 120,
	},

	"Akrobatyka": { //TIER 5 - umo�liwia akrobatyk� na 3 minuty
		name = "Akrobatyka",
		type = BuffType.Mds,
		mds = "HUMANS_ACROBATIC.MDS",
		value = 0,
		interval = 0,
		icon = "BOOTS_EQ_ICON_R.TGA",
		time = 160,
	},

	"Akrobatyka2": { //TIER 5 - umo�liwia akrobatyk� na 2 minuty
		name = "Akrobatyka2",
		type = BuffType.Mds,
		mds = "HUMANS_ACROBATIC.MDS",
		value = 0,
		interval = 0,
		icon = "BOOTS_EQ_ICON_R.TGA",
		time = 120,
	},

	"Miod": { //ALKOHOL - Miod
		name = "Miod",
		type = BuffType.Mds,
		mds = "HUMANS_DRUNKEN.MDS",
		value = 0,
		interval = 0,
		stack = true,
		icon = "ALCHEMY_EQ_ICON_R.TGA",
		time = 15,
	},

	"Piwo": { //ALKOHOL - piwo
		name = "Piwo",
		type = BuffType.Mds,
		mds = "HUMANS_DRUNKEN.MDS",
		value = 0,
		interval = 0,
		stack = true,
		icon = "ALCHEMY_EQ_ICON_R.TGA",
		time = 120,
	},

	"Wino": { //ALKOHOL - wino marki wino
		name = "Wino",
		type = BuffType.Mds,
		mds = "HUMANS_DRUNKEN.MDS",
		value = 0,
		interval = 0,
		stack = true,
		icon = "ALCHEMY_EQ_ICON_R.TGA",
		time = 240,
	},

	"Gorzalka": { //ALKOHOL - wodka
		name = "Gorzalka",
		type = BuffType.Mds,
		mds = "HUMANS_DRUNKEN.MDS",
		value = 0,
		interval = 0,
		stack = true,
		icon = "ALCHEMY_EQ_ICON_R.TGA",
		time = 360,
	},

	"Samogon": { //ALKOHOL - bimber/spirytus
		name = "Samogon",
		type = BuffType.Mds,
		mds = "HUMANS_DRUNKEN.MDS",
		value = 0,
		interval = 0,
		stack = true,
		icon = "ALCHEMY_EQ_ICON_R.TGA",
		time = 480,
	},

	"Lewitacja": { //TIER 3 - Lewitacja
		name = "Lewitacja",
		type = BuffType.Mds,
		mds = "HUMANS_LEWITACJA.MDS",
		value = 0,
		interval = 0,
		icon = "NATURE_EQ_ICON_R.TGA",
		time = 30,
	},

	"Lewitacja2": { //TIER 5 - Lewitacja2
		name = "Lewitacja2",
		type = BuffType.Mds,
		mds = "HUMANS_LEWITACJA.MDS",
		value = 0,
		interval = 0,
		icon = "NATURE_EQ_ICON_R.TGA",
		time = 60,
	},

	"Zatrucie": { //TIER 3 - redukuje 1000 hp
		name = "Zatrucie",
		type = BuffType.HealthReduce,
		value = 100,
		interval = 120,
		icon = "POTION2_EQ_ICON_R.TGA",
		time = 3600,
	},
	"Brzuch180": { //Brzuch bola� od pizzy :(
		name = "Brzuch180",
		type = BuffType.Mds,
		mds = "HUMANS_HURT.MDS",
		value = 0,
		interval = 0,
		icon = "SHOULDER_EQ_ICON_R.TGA",
		time = 180,
	},


};

Config["Buildings"] <- {
	"Biedne drzwi": {
		skill = 3,
		model = "DOOR_NW_POOR_01.MDS",
		work = 3,
		vobInfo = {
			type = VobType.Door,
			hpMode = VobHealth.Vulnerable,
			hp = 40,
			movable = VobMovable.Static,
		},
		materials = [
			["ITCR_SDRP_DROP_15", 30],
			["ITUS_SDRP_TOOL_35", 3],
			["ITCR_SDRP_INGR_18", 1],
		],
		construction = [
			[-83.50, 130.70, -1.30, 0, 0, 0],
			[50.78, 130.70, -1.0, 0, 0, 0],
			[-20.58, 286.70, -4.50, -88, 92, 0],
		]
	},
	"Drzwi": {
		skill = 3,
		model = "DOOR_NW_RICH_01.MDS",
		work = 80,
		vobInfo = {
			type = VobType.Door,
			hpMode = VobHealth.Vulnerable,
			hp = 120,
			movable = VobMovable.Static,
		},
		materials = [
			["ITCR_SDRP_DROP_15", 45],
			["ITUS_SDRP_TOOL_35", 6],
			["ITCR_SDRP_INGR_18", 2],
		],
		construction = [
			[-83.50, 130.70, -1.30, 0, 0, 0],
			[50.78, 130.70, -1.0, 0, 0, 0],
			[-20.58, 286.70, -4.50, -88, 92, 0],
		]
	},
	"Mocne Drzwi": {
		skill = 4,
		model = "DOOR_WOODEN.MDS",
		work = 90,
		vobInfo = {
			type = VobType.Door,
			hpMode = VobHealth.Vulnerable,
			hp = 160,
			movable = VobMovable.Static,
		},
		materials = [
			["ITCR_SDRP_DROP_15", 60],
			["ITUS_SDRP_TOOL_35", 12],
			["ITCR_SDRP_INGR_18", 2],
		],
		construction = [
			[-83.50, 130.70, -1.30, 0, 0, 0],
			[50.78, 130.70, -1.0, 0, 0, 0],
			[-20.58, 286.70, -4.50, -88, 92, 0],
		]
	},
	"Wrota": {
		skill = 5,
		model = "DOOR_NW_DRAGONISLE_02.MDS",
		work = 120,
		vobInfo = {
			type = VobType.Door,
			hpMode = VobHealth.Static,
			hp = 2000,
			movable = VobMovable.Static,
		},
		materials = [
			["ITCR_SDRP_DROP_15", 90],
			["ITUS_SDRP_TOOL_35", 24],
			["ITCR_SDRP_INGR_18", 7],
		],
		construction = [
			[-83.50, 130.70, -1.30, 0, 0, 0],
			[50.78, 130.70, -1.0, 0, 0, 0],
			[-20.58, 286.70, -4.50, -88, 92, 0],
		]
	},
	"Sciana": {
		skill = 3,
		model = "EVT_NC_MAINGATE01.3DS",
		work = 55,
		vobInfo = {
			type = VobType.Static,
			hpMode = VobHealth.Vulnerable,
			hp = 150,
			movable = VobMovable.Static,
		},
		materials = [
			["ITCR_SDRP_DROP_15", 20],
			["ITUS_SDRP_TOOL_35", 5],
		],
		construction = [
			[-200.50, -30.70, -30.30, 0, 0, 0],
			[0.78, -30.70, -21.0, 0, 0, 0],
			[200.58, -30.70, 24.50, 0, 0, 0],
		]
	},
}