
class ExtendedPacket extends Packet
{
    constructor(typeId)
    {
        base.constructor();

        writeUInt8(PacketReceivers);
        writeUInt8(typeId);
    }

    function send(pid, order = RELIABLE)
    {
        base.send(pid, order);
    }

    function sendToAllPlayers(order = RELIABLE)
    {
        foreach(_player in Player.all())
            if(_player.inGame)
                base.send(_player.id, order);
    }

    function sendInRangeFromPosition(position, distance, order = RELIABLE) {
        foreach(_player in getPlayers())
            if(_player.distanceToPosition(position) < distance)
                base.send(_player.id, order);
    }

    function sendToPlayersInState(stateId, order = RELIABLE)
    {
        foreach(_player in Player.all())
            if(_player.inGame)
                if(_player.stateId == stateId)
                    base.send(_player.id, order);
    }
}