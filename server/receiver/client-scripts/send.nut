
class ExtendedPacket extends Packet
{
    constructor(typeId)
    {
        base.constructor();

        writeUInt8(PacketReceivers);
        writeUInt8(typeId);
    }

    function send(order = RELIABLE_ORDERED)
    {
        base.send(order);
    }
}