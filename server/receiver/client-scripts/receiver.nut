
class RegisterPacketReceiver
{
    static objects = [];

    typeId = -1;
    functionToReceive = null;

    constructor(typeId, functionToReceive)
    {
        this.typeId = typeId;
        this.functionToReceive = functionToReceive;

        RegisterPacketReceiver.objects.append(this);
    }

    static function onPacket(packet)
    {
        local packetIdentity = packet.readUInt8();
        if(packetIdentity != PacketReceivers)
            return;

        local idRequest = packet.readUInt8();
        for (local i = RegisterPacketReceiver.objects.len() - 1; i >= 0; --i)
		{
            local obj = RegisterPacketReceiver.objects[i];
            if(idRequest == obj.typeId)
            {
                obj.functionToReceive(packet);
                return;
            }
        }
    }
}

addEventHandler("onPacket", RegisterPacketReceiver.onPacket)
