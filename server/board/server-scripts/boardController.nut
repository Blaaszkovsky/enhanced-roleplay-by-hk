
class BoardController
{
    static boards = {};

    static function onInit() {
        local boardInDatabase = Query().select().from(Board.BoardTable).all();
        local boardFractionIds = [];
        foreach(boardId, boardPosition in Config["NotificationBoard"]) {
            if(boardPosition.fractionIds)
                boardFractionIds = boardPosition.fractionIds
            boards[boardId] <- Board(boardId, boardPosition, boardPosition.size, boardFractionIds);
        }

        foreach(boardNotificationObject in boardInDatabase)
        {
            local newNotificationBoard = BoardNotification(boardNotificationObject.boardId.tointeger())

            newNotificationBoard.id = boardNotificationObject.id.tointeger();
            newNotificationBoard.world = boardNotificationObject.world;
            newNotificationBoard.user = boardNotificationObject.userName;
            newNotificationBoard.text = split(boardNotificationObject.text,"&");
            newNotificationBoard.position = {x = boardNotificationObject.x.tointeger(), y = boardNotificationObject.y.tointeger()}
            newNotificationBoard.expiredAt = boardNotificationObject.expiredAt;

            boards[boardNotificationObject.boardId].addNotification(newNotificationBoard);
        }

        print("We have " + boards.len() + " registered boards.");
    }

    static function onPlayerLoad(playerId) {


        foreach(boardId, board in boards)
            BoardPacket().sendBoard(playerId, board);
    }

    static function deleteExpiredBoards() {
        local boardInDatabase = Query().select().from(Board.BoardTable).all();
        local dateObject = date();
        local currentDate = dateObject.year+"-"+ dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;
        foreach(boardNotificationObject in boardInDatabase) {
            if(boardNotificationObject.expiredAt <= currentDate) {
                Query().deleteFrom(Board.BoardTable).where(["id = "+boardNotificationObject.id]).execute();
                BoardPacket().remove(boardNotificationObject.boardId, boardNotificationObject.id);
            }
        }
    }

    static function onPacket(playerId, packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BoardPackets.Delete:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local notificationId = packet.readInt16();

                local fractionObj = getFractions();
                local canManageBoard = false;

                if(getPlayer(playerId).fractionId != -1) {
                    local fractionPlayerRank = fractionObj[getPlayer(playerId).fractionId].ranks[getPlayer(playerId).fractionRoleId];
                    canManageBoard = fractionPlayerRank.canManageBoard;
                }

                if(boards[boardId].notifications[notificationId].user == getAccount(playerId).username) {
                    local object = Config["ChatMethod"][ChatMethods.Action];
                    distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] zerwa� og�oszenie z tablicy");
                    boards[boardId].removeNotification(notificationId);
                    BoardPacket().remove(boardId, notificationId);
                    Query().deleteFrom(Board.BoardTable).where(["id = "+notificationId]).execute();
                    addPlayerLog(playerId, "Zerwa� og�oszenie z tablicy ID: " + notificationId);
                }
                else if(canManageBoard) {
                    local object = Config["ChatMethod"][ChatMethods.Action];
                    distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] zerwa� og�oszenie z tablicy");
                    boards[boardId].removeNotification(notificationId);
                    BoardPacket().remove(boardId, notificationId);
                    Query().deleteFrom(Board.BoardTable).where(["id = "+notificationId]).execute();
                } else if(getAdmin(playerId).role < PlayerRole.Helper) {
                    local object = Config["ChatMethod"][ChatMethods.Action];
                    distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] zerwa� og�oszenie z tablicy");
                    boards[boardId].removeNotification(notificationId);
                    BoardPacket().remove(boardId, notificationId);
                    Query().deleteFrom(Board.BoardTable).where(["id = "+notificationId]).execute();
                } else {
                    addPlayerNotification(playerId, "Nie mo�esz zerwa� cudzego og�oszenia!");
                    return
                }


            break;
            case BoardPackets.Create:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local emptySlot = null;
                local emptySlot = boards[boardId].getEmptySlot();
                if(emptySlot == null) {
                    addPlayerNotification(playerId, "Nie ma wolnych miejsc.");
                    return;
                }

                local listStrings = [];
                // Tutaj olewamy X,Y bo klient mo�e nas wprowadzi� w b��d
                //TODO: Usun�� paczk� od klienta, kt�ra odpowiada za wysy�anie X,Y
                local dummy = packet.readInt16();
                local dummy2 = packet.readInt16();
                local positionX = emptySlot.x;
                local positionY = emptySlot.y;
                local listLen = packet.readInt16();
                local expiredAt = packet.readString();
                for(local i = 0; i < listLen; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    listStrings.append(str);
                }

                local newNotificationBoard = BoardNotification(boardId);


                newNotificationBoard.world = getPlayerWorld(playerId);
                newNotificationBoard.user = getAccount(playerId).username;
                newNotificationBoard.position = emptySlot;
                newNotificationBoard.text = listStrings;
                newNotificationBoard.expiredAt = expiredAt;

                local queryResult = Query().insertInto(Board.BoardTable, ["text", "boardId", "userName", "world", "x", "y", "expiredAt"
                ], [
                    "'"+String.parseArray(listStrings, "&")+"'", boardId, "'"+getAccount(playerId).username+"'", "'"+newNotificationBoard.world+"'", positionX, positionY, "'" + newNotificationBoard.expiredAt + "'"
                ]).execute();

                if(queryResult == false)
                {
                    // Jak ju� wejdziemy tutaj to raczej co� z baz� jest nie tak
                    addPlayerNotification(playerId, "Bl�d podczas dodawania og�oszenia");
                    return;
                }

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] zawiesi� og�oszenie na tablicy");

                local idBoard = DB.queryGet("SELECT id FROM "+Board.BoardTable+" ORDER BY id DESC LIMIT 1")["id"];

                newNotificationBoard.id = idBoard;

                boards[boardId].addNotification(newNotificationBoard);
                BoardPacket().sendNotification(newNotificationBoard);
                addPlayerLog(playerId, "Doda� og�oszenie na tablice ID: " + idBoard);

            break;
        }
    }
}

addEventHandler("onInit", BoardController.onInit.bindenv(BoardController));
addEventHandler("onPlayerLoad", BoardController.onPlayerLoad.bindenv(BoardController))
addEventHandler("onMinute", BoardController.deleteExpiredBoards.bindenv(BoardController))
RegisterPacketReceiver(Packets.Board, BoardController.onPacket.bindenv(BoardController));

getBoard <- @(id) BoardController.boards[id];
getBoards <- @() BoardController.boards;

