
class Board
{
    static BoardTable = "board";

    id = -1
    notifications = null;
    size = null;
    position = null;
    fractionIds = null;

    constructor(id, position, size, fractionIds)
    {
        this.id = id;
        this.notifications = {};
        this.size = size;
        this.position = position;
        this.fractionIds = fractionIds;
    }

    function getEmptySlot() {
        local emptySlot = null;
        for(local x = 0; x < size[0]; x++) {
            for(local y = 0; y < size[1]; y++) {
                if(hasIndexFilled(x,y) == false) {
                    emptySlot = {x = x, y = y};
                    return emptySlot;
                }
            }
        }
        return emptySlot;
    }

    function hasIndexFilled(x,y) {
        foreach(note in notifications)
            if(note.position.x == x && note.position.y == y)
                return true;

        return false;
    }

    function addNotification(notification) {
        notifications[notification.id] <- notification;
    }

    function removeNotification(index) {
        notifications.rawdelete(index);
    }
}

class BoardNotification
{
    id = -1
    boardId = -1

    text = null
    user = null
    world = null
    position = null
    expiredAt = null;

    constructor(boardId)
    {
        this.id = -1;
        this.boardId = boardId;

        this.position = {x = 0, y = 0}
        this.user = "";
        this.text = [];
        this.world = "";
        this.expiredAt = "";
    }

    function getTextAsString() {
        local str = "";
        foreach(tet in text)
            str += tet;

        return str;
    }

    function getTitle() {
        local newText = getTextAsString();
        if(newText.len() > 10)
            return user + " - " + newText.slice(0, 10);

        return user + " - " + newText;
    }
}