
BoardInterfaceCreate <- {
    boardId = -1,
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 280), any(Resolution.y/2 - 340), anx(560), any(700), "LETTERS.TGA", null, false);
        result.multiline <- MyGUI.MultiLineInput(anx(Resolution.x/2 - 240), any(Resolution.y/2 - 220), anx(460), 18);
        result.multiline.setColor(32,44,32);
        result.header <- GUI.Draw(anx(Resolution.x/2 - 240), any(Resolution.y/2 - 270), "Dodaj notatk�");
        result.header.setFont("FONT_OLD_20_WHITE_HI.TGA");
        result.header.setScale(0.6, 0.6);
        result.header.setColor(32,44,32);

        result.addButton <- GUI.Button(anx(150), any(740), anx(200), any(60), "SG_BUTTON.TGA", "Dodaj", result.window);

        result.addButton.bind(EventType.Click, function(element) {
            local boardId = BoardInterfaceCreate.boardId;
            BoardInterfaceCreate.hide();
            local dateObject = date();
            BoardPacket().create(boardId, BoardInterfaceCreate.bucket.multiline.getValue(), {x = 0, y = 0}, dateObject.year+"-"+(dateObject.month + 1) +"-"+(dateObject.day+1)+" "+dateObject.hour+":"+dateObject.min);
        });

        return result;
    }

    show = function(_boardId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        boardId = _boardId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.BoardCreate;

        bucket.multiline.active = true;

        bucket.window.setVisible(true);
        bucket.multiline.setVisible(true);
        bucket.multiline.setValue(array(18, ""));
        bucket.header.setVisible(true);
    }

    hide = function()
    {
        bucket.window.setVisible(false);
        bucket.multiline.setVisible(false);
        bucket.header.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.BoardCreate)
            hide();
    }
}

addEventHandler("onForceCloseGUI", BoardInterfaceCreate.onForceCloseGUI.bindenv(BoardInterfaceCreate));
Bind.addKey(KEY_ESCAPE, BoardInterfaceCreate.hide.bindenv(BoardInterfaceCreate), PlayerGUI.BoardCreate)