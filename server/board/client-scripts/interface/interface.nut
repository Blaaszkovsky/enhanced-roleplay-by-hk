
BoardInterface <- {
    boardId = -1
    noteId = -1,
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(0, 0, 0, 0, "SG_BOX.TGA", null, false);
        result.windowSecond <- GUI.Window(anx(Resolution.x/2 - 580), any(Resolution.y/2 - 340), anx(560), any(700), "LETTERS.TGA", null, false);
        result.drawText <- Draw(anx(Resolution.x/2 - 540), any(Resolution.y/2 - 220),"");
        result.drawText.setColor(42,52,55);
        result.notes <- [];
        result.timeLeft <- GUI.Draw(anx(255), any(740),"", result.windowSecond);

        result.addButton <- GUI.Button(0, 0, anx(400), any(60), "SG_BUTTON.TGA", "Dodaj now� notatk�", result.window);
        result.addButton.bind(EventType.Click, function(element) {
            local boardId = BoardInterface.boardId;
            BoardInterface.hide();
            BoardInterfaceCreate.show(boardId);
        });

        result.removeButton <- GUI.Button(0, any(720), anx(200), any(60), "SG_BUTTON.TGA", "Usu�", result.windowSecond);
        result.removeButton.bind(EventType.Click, function(element) {
            local boardId = BoardInterface.boardId;
            BoardPacket().remove(BoardInterface.boardId, BoardInterface.noteId)
        });

        return result;
    }

    show = function(_boardId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        boardId = _boardId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Board;

        playAni(heroId,"T_MAP_STAND_2_S0");

        local board = getBoard(boardId);
        Camera.setPosition(board.camera.x, board.camera.y, board.camera.z);
        Camera.setRotation(0, board.camera.rotx, 0);

        bucket.window.setVisible(true);

        refresh();
    }

    hide = function()
    {
        bucket.window.setVisible(false);
        bucket.windowSecond.setVisible(false);

        bucket.drawText.text = "";
        bucket.drawText.visible = false;

        bucket.timeLeft.setText("");
        bucket.timeLeft.setVisible(false);

        boardId = -1;
        noteId = -1;

        foreach(note in bucket.notes)
        {
            note.button.setVisible(false);
            note.button.destroy();
        }

        bucket.notes.clear();
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    showDescription = function(_noteId) {
        noteId = _noteId;
        local note = getBoard(boardId).notifications[noteId];
        bucket.windowSecond.setVisible(true);
        bucket.drawText.text = note.getLineText();
        bucket.drawText.visible = true;

        bucket.timeLeft.setText("Wyga�nie: " + note.expiredAt);
        bucket.timeLeft.setVisible(true);
    }

    refresh = function() {
        foreach(note in bucket.notes)
        {
            note.button.setVisible(false);
            note.button.destroy();
        }

        bucket.notes.clear();

        local board = getBoard(boardId);

        local baseSize = bucket.window.getSize();
        local basePosition = bucket.window.getPosition();

        switch(boardId)
        {
            case 0:
                bucket.window.setSize(2048, 4092); baseSize = { width = 2048, height = 4092};
                bucket.window.setPosition(4592, 2024); basePosition = {x = 4592, y = 2024};
            break;
            case 1:
                bucket.window.setSize(3076, 3076); baseSize = { width = 3076, height = 3076};
                bucket.window.setPosition(4192, 2524); basePosition = {x = 4192, y = 2524};
            break;
            default:
                bucket.window.setSize(3076, 3076); baseSize = { width = 3076, height = 3076};
                bucket.window.setPosition(4192, 2524); basePosition = {x = 4192, y = 2524};
            break;
        }

        baseSize.width = baseSize.width - 100;
        baseSize.height = baseSize.height - 100;
        basePosition.x = basePosition.x + 50;
        basePosition.y = basePosition.y + 50;

        bucket.addButton.setPosition(basePosition.x, basePosition.y - any(100));

        local chunkSize = [abs(baseSize.width/board.size[0]), abs(baseSize.height/board.size[1])]

        foreach(notification in board.notifications)
        {
            local button = GUI.Button(
                basePosition.x + chunkSize[0] * notification.position.x,
                basePosition.y + chunkSize[1] * notification.position.y,
                chunkSize[0],
                chunkSize[1],
                "LETTERS.TGA"
            );

            local draw = Draw(basePosition.x + chunkSize[0] * notification.position.x + 25, basePosition.y + chunkSize[1] * notification.position.y + 25, notification.getLineText());
            draw.setScale(0.175, 0.175);
            draw.setColor(25, 31, 21);

            button.attribute = notification.id;
            button.setVisible(true);
            draw.visible = true;
            bucket.notes.append({ button = button, draw = draw });
        }
    }

    onBoardNotificationAdd = function(_boardId, boardNotificationId)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        if(boardId != _boardId)
            return;

        refresh();
    }

    onBoardNotificationRemove = function(_boardId, boardNotificationId)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        if(boardId != _boardId)
            return;

        refresh();
    }

    onClick = function(element)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        foreach(note in bucket.notes)
        {
            if(note.button instanceof GUI.Button)
            {
                if(note.button == element)
                {
                    showDescription(note.button.attribute);
                    return;
                }
            }
        }
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Board)
            hide();
    }
}

addEventHandler("onForceCloseGUI", BoardInterface.onForceCloseGUI.bindenv(BoardInterface));
addEventHandler("onBoardNotificationAdd", BoardInterface.onBoardNotificationAdd.bindenv(BoardInterface));
addEventHandler("onBoardNotificationRemove", BoardInterface.onBoardNotificationRemove.bindenv(BoardInterface));
addEventHandler("GUI.onClick", BoardInterface.onClick.bindenv(BoardInterface));

Bind.addKey(KEY_ESCAPE, BoardInterface.hide.bindenv(BoardInterface), PlayerGUI.Board)