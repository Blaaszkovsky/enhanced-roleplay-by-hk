local draws = {}

addEventHandler("onInit", function() {
    foreach(boardId, boardPosition in Config["NotificationBoard"]) {

        local draw = Draw3d(boardPosition.x, boardPosition.y, boardPosition.z)

        draw.insertText("Na tablicy nie ma og�osze�")

        draw.distance = 700
        draw.visible = true

        draws[boardId] <- draw
    }
})


addEventHandler("onBoardNotificationAdd", function(boardId, notificationBoardId) {
    local board = getBoard(boardId);

    draws[boardId].setLineText(0, "Na tablicy wisi " + board.notifications.len() + " og�osze�")
});

addEventHandler("onBoardNotificationRemove", function(boardId, notificationBoardId) {
    local board = getBoard(boardId);
    local notificationsLen = board.notifications.len()

    if (ActiveGui == PlayerGUI.Board)
    {
        if (BoardInterface.noteId == notificationBoardId)
            BoardInterface.hide();
    }

    if (notificationsLen != 0)
        draws[boardId].setLineText(0, "Na tablicy wisi " + notificationsLen + " og�osze�")
    else
        draws[boardId].setLineText(0, "Na tablicy nie ma og�osze�")
});