
class Board
{
    static BoardTable = "board";

    id = -1

    notifications = null;
    position = null;
    shadows = null;
    corners = null;
    camera = null;
    size = null;

    constructor(id, x, y, z, angle, corners, size, camera)
    {
        this.id = id;

        this.notifications = {};
        this.shadows = [];
        this.corners = corners;
        this.size = size;
        this.camera = camera;
        this.position = {x = x, y = y, z = z, angle = angle}
    }

    function addNotification(notification) {
        notification.boardId = id;
        notifications[notification.id] <- notification;
    }
}

class BoardNotification
{
    id = -1
    text = ""
    position = null
    expiredAt = "";

    boardId = -1

    constructor(id, text, position, expiredAt)
    {
        this.id = id;
        this.text = text
        this.position = position
        this.expiredAt = expiredAt
    }

    function getLineText() {
        return String.parseArray(text, "\n");
    }
}
