
class BoardController
{
    static boards = {};

    static function onInit() {
        foreach(boardId, boardPosition in Config["NotificationBoard"]) {
            boards[boardId] <- Board(boardId, boardPosition.x, boardPosition.y, boardPosition.z, boardPosition.angle, boardPosition.corners, boardPosition.size, boardPosition.camera);
            InteractionTree.addDistanceObserver(boardPosition.x, boardPosition.y, boardPosition.z, 400, "Rzu� okiem na tablic�", function() {BoardController.interactWithBoard(); });
        }
    }

    static function interactWithBoard() {
        local pos = getPlayerPosition(heroId);
        foreach(board in boards) {
            if(getPositionDifference(pos, board.position) < 400) {
                BoardInterface.show(board.id);
                break;
            }
        }
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BoardPackets.List:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                boards[boardId].notifications.clear();

                local notificationsLen = packet.readInt16();
                for(local i = 0; i < notificationsLen; i ++) {
                    local notificationBoardId = packet.readInt16();
                    local positionX = packet.readInt16();
                    local positionY = packet.readInt16();
                    local textLen = packet.readInt16();
                    local text = [];
                    local expiredAt = packet.readString();

                    for(local i = 0; i < textLen; i++) {
                        text.append(packet.readString());
                    }

                    local notification = BoardNotification(notificationBoardId, text, {x = positionX, y = positionY}, expiredAt)
                    boards[boardId].addNotification(notification);

                    callEvent("onBoardNotificationAdd", boardId, notificationBoardId);
                }
            break;
            case BoardPackets.Delete:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local notificationBoardId = packet.readInt16();
                boards[boardId].notifications.rawdelete(notificationBoardId);
                callEvent("onBoardNotificationRemove", boardId, notificationBoardId);
            break;
            case BoardPackets.Create:
                local notificationId = packet.readInt16();
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local positionX = packet.readInt16();
                local positionY = packet.readInt16();
                local textLen = packet.readInt16();
                local text = [];
                local expiredAt = packet.readString();


                for(local i = 0; i < textLen; i++) {
                    text.append(packet.readString());
                }

                local notification = BoardNotification(notificationId, text, {x = positionX, y = positionY}, expiredAt)
                boards[boardId].addNotification(notification);

                callEvent("onBoardNotificationAdd", boardId, notificationId);
            break;
        }
    }
}

addEventHandler("onInit", BoardController.onInit.bindenv(BoardController));
RegisterPacketReceiver(Packets.Board, BoardController.onPacket.bindenv(BoardController));

getBoard <- @(id) BoardController.boards[id];
getBoards <- @() BoardController.boards;

