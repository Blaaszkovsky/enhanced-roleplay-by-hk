
// enum BoardPackets
// {
//     Create,
//     Delete,
//     List,
//     Show,
// }

class BoardPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Board);
    }

    function getList(boardId) {
        packet.writeUInt8(BoardPackets.List)
        packet.writeInt16(boardId);
        packet.send(RELIABLE);
    }

    function remove(boardId, boardNotificationId) {
        packet.writeUInt8(BoardPackets.Delete)
        packet.writeInt16(boardId);
        packet.writeInt16(boardNotificationId);
        packet.send(RELIABLE);
    }

    function create(boardId, list, position, expiredAt) {
        packet.writeUInt8(BoardPackets.Create)
        packet.writeInt16(boardId);
        packet.writeInt16(position.x);
        packet.writeInt16(position.y);
        packet.writeInt16(list.len());
        packet.writeString(expiredAt);
        foreach(_item in list)
            packet.writeString(_item);

        packet.send(RELIABLE);
    }
}