

enum GarbageCollectorType {
    Player,
    Vob,
    ItemGround,
}

GarbageCollector <- {
    list = {},

    add = function(type, identfier, secondsleft) {
        list[type+"_"+identfier] <- secondsleft;
    }

    remove = function(index) {
        local args = split(index, "_");
        local type = args[0].tointeger();
        local identfier = args[1].tointeger();

        switch(type) {
            case GarbageCollectorType.ItemGround:
                try {
                    ItemsGround.destroy(identfier);
                    ItemsWorldRegister.destroyColleration(identfier);
                }catch(error){}
            break;
        }

        list.rawdelete(index);
        onSecond();
    }

    onSecond = function() {
        foreach(index, item in list) {
            if(list[index] <= 0) {
                remove(index);
                break;
            }

            list[index] = list[index] - 1;
        }
    }
}

addEventHandler ("onSecond", GarbageCollector.onSecond.bindenv(GarbageCollector));