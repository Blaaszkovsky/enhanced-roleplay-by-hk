_print <- print;

function print(...)
{
    foreach(arg in vargv)
    {
        switch(typeof(arg))
        {
            case "array": case "table":
                foreach(index, value in arg) {
                    local typeOfValue = typeof value;
                    if(typeof(value) == "integer" || typeof(value) == "float" || typeof(value) == "string" )
                        _print(index + ": " + value);
                    else {
                        _print("Index: "+index);
                        print(value);
                    }
                }
            break;
            case "instance":
                foreach (key, value in arg.getclass()) {
                    if (typeof value != "function") {
                        if(typeof (arg[key]) == "integer" || typeof (arg[key]) == "float" || typeof (arg[key]) == "string" )
                            _print(key + ": " + arg[key]);
                        else {
                            _print(key);
                            _print(arg[key]);
                        }
                    }
                }
            break;
            default:
                _print(arg);
            break;
        }
    }
}