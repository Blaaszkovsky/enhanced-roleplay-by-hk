_date <- date;

function date(time = 0)
{
    local d = _date();
    if(time > 0){
        d = _date(time);
    }
    d.month = d.month + 1;
    return d; 
}