
function irand(max) 
{
    // Generate a pseudo-random integer between 0 and max
    local roll = (1.0 * rand() / RAND_MAX) * (max + 1);
    return roll.tointeger();
}

function getMin (value1, value2){ return value1 < value2 ? value1 : value2;}
function getMax (value1, value2){ return value1 > value2 ? value1 : value2;}
function getBetween(value, min, max){ return value >= min ? value <= max ? value : max : min;}