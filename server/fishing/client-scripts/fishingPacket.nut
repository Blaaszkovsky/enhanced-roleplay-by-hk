
class FishingPacket
{
    packet = null;
    object = null;

    constructor(fishingObject)
    {
        packet = ExtendedPacket(Packets.Fishing);
        object = fishingObject;
    }

    function deleteFishing() {
        packet.writeUInt8(FishingPackets.Delete)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function takeFishing() {
        packet.writeUInt8(FishingPackets.Slots)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }
}