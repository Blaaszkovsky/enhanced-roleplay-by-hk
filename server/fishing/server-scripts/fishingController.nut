
class FishingController
{
    static fishings = {};

    static function onTryToHarvestFishing(playerId, id) {
        if(!(id != fishings))
            return;


        local obj = fishings[id];
        local scheme = obj.getScheme();

        local pObject = getPlayer(playerId);

        local playerSkillStamina = 0;

        if(obj.slots <= 0) {
            addPlayerNotification(playerId, "Tu niema nic!");
            return;
        }

        if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 0) {
            playerSkillStamina = 25; 
        } else if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 1) {
            playerSkillStamina = 20;
        } else if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 2) {
            playerSkillStamina = 15;
        } else if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 3) {
            playerSkillStamina = 10;
        } else if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 4) {
            playerSkillStamina = 5;
        } else if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == 5) {
            playerSkillStamina = 5;
        }

        
        if(getPlayerStamina(playerId) < playerSkillStamina) {
            addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
            return;
        }

        setPlayerStamina(playerId, getPlayerStamina(playerId) - playerSkillStamina);


        obj.slots -= 1;
        local outcome = scheme.outcome;

        local possibilitiesArray = [];
        foreach(index, _item in outcome) {
            if(getPlayerSkill(playerId, PlayerSkill.Fisherman) == _item.skill && _item.chance != -1) {
                for(local i = 0; i < _item.chance; i ++) {
                    possibilitiesArray.push({instance = _item.instance, amount = _item.amount});
                }
            }
        }

        local givedItem = false;
        if(possibilitiesArray.len() != 0) {
            local _item = possibilitiesArray[irand(possibilitiesArray.len() - 1)];
            giveItem(playerId, _item.instance, _item.amount);
            if(_item.amount == 0)
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Nie uda�o si� nic zlowi�.");
            else
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Uda�o si� uzyska� "+getItemScheme(_item.instance).name+" w ilo�ci "+_item.amount+".");

            addPlayerLog(playerId, "Udalo sie uzyskac z rybolostwa "+_item.instance+" w ilosci "+_item.amount);
            givedItem = true;
        }

        if(givedItem == false) {
            foreach(_item in outcome) {
                if(_item.chance == -1) {
                    giveItem(playerId, _item.instance, _item.amount);
                    sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Uda�o si� uzyska� "+getItemScheme(_item.instance).name+" w ilo�ci "+_item.amount+".");
                    addPlayerLog(playerId, "Udalo sie uzyskac z rybolostwa "+_item.instance+" w ilosci "+_item.amount);
                    givedItem = true;
                    break;
                }
            }
        }

        foreach(_item in pObject.inventory.getItems()) {
            if(_item.instance == "ITMW_SDRP_2H_OTH_01") {
                if(_item.resistance > 0) {
                    _item.resistance = _item.resistance - 1;
                    _item.sendUpdate(playerId);
                    break;
                }
            }
        }

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Fishing);
        packet.writeUInt8(FishingPackets.Update);
        packet.writeInt16(1);
        packet.writeInt32(obj.id);
        packet.writeInt16(obj.slots);
        packet.sendToAll(RELIABLE);
    }

    static function onInit() {
        local fishingsInDatabase = Query().select().from(Fishing.FishingTable).all();

        foreach(fishingsObject in fishingsInDatabase)
        {
            local newFishing = Fishing(fishingsObject.id.tointeger())

            newFishing.scheme = fishingsObject.scheme;
            newFishing.slots = fishingsObject.slots.tointeger();
            newFishing.rotation = {x = fishingsObject.rotationX.tofloat(), y = fishingsObject.rotationY.tofloat(), z = fishingsObject.rotationZ.tofloat()}
            newFishing.position = {x = fishingsObject.x.tofloat(), y = fishingsObject.y.tofloat(), z = fishingsObject.z.tofloat()}
            newFishing.world = fishingsObject.world

            fishings[newFishing.id] <- newFishing;
        }

        print("We have " + fishings.len() + " existing fishings.");
    }

    static function onPlayerJoin(playerId) {
        foreach(fishing in fishings)
            FishingPacket(fishing).create(playerId);
    }

    static function onSecond() {
        local idsOfFishings = [];

        foreach(fishingId, fishing in fishings)
        {
            if(fishing.onSecond())
                idsOfFishings.push({id = fishingId, slots = fishing.slots});
        }

        if(idsOfFishings.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Fishing);
        packet.writeUInt8(FishingPackets.Update);
        packet.writeInt16(idsOfFishings.len());
        foreach(obj in idsOfFishings) {
            packet.writeInt32(obj.id);
            packet.writeInt16(obj.slots);
        }
        packet.sendToAll(RELIABLE);
    }

    function getNearFishing(playerId) {
        foreach(fishingId, fishing in fishings)
            if(getPositionDifference(pos, fishing.position) < 500)
                return fishingId;

        return null;
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case FishingPackets.Slots:
                onTryToHarvestFishing(playerId, packet.readInt32());
            break;
            case FishingPackets.Create:
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();

                Query().insertInto(Fishing.FishingTable, ["x", "y", "z", "rotationX", "rotationY", "rotationZ", "slots", "scheme"], [
                    positionX, positionY, positionZ, rotationX, rotationY, rotationZ, Config["FishingSystem"][scheme].slots, "'"+scheme+"'"
                ]).execute();

                local fishingsObject = Query().select().from(Fishing.FishingTable).orderBy("id DESC").one();
                if(fishingsObject.scheme != scheme)
                    return;

                local newFishing = Fishing(fishingsObject.id.tointeger())

                newFishing.scheme = fishingsObject.scheme;
                newFishing.slots = fishingsObject.slots.tointeger();
                newFishing.rotation = {x = fishingsObject.rotationX.tofloat(), y = fishingsObject.rotationY.tofloat(), z = fishingsObject.rotationZ.tofloat()}
                newFishing.position = {x = fishingsObject.x.tofloat(), y = fishingsObject.y.tofloat(), z = fishingsObject.z.tofloat()}
                newFishing.world = getPlayerWorld(playerId)

                fishings[newFishing.id] <- newFishing;
                FishingPacket(newFishing).create(-1);
            break;
            case FishingPackets.Delete:
                local fishingId = packet.readInt16();
                if(!(fishingId in fishings))
                    return;

                FishingPacket(fishings[fishingId]).deleteFishing();
                fishings.rawdelete(fishingId);
                Query().deleteFrom(Fishing.FishingTable).where(["id = "+fishingId]).execute();
            break;
        }
    }
}

addEventHandler("onInit", FishingController.onInit.bindenv(FishingController));
addEventHandler("onPlayerJoin", FishingController.onPlayerJoin.bindenv(FishingController))
addEventHandler("onSecond", FishingController.onSecond.bindenv(FishingController))
RegisterPacketReceiver(Packets.Fishing, FishingController.onPacket.bindenv(FishingController));
