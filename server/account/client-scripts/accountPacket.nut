
Account.Packet <- {
    kick = function(reason) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.Kick);
        packet.writeString(reason);
        packet.send(RELIABLE);
    }

    loggIn = function(objectToSend) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.LoggIn);
        packet.writeString(objectToSend.username);
        packet.writeString(objectToSend.password);
        packet.send(RELIABLE);
    }

    register = function(username, password) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.Register);
        packet.writeString(username);
        packet.writeString(password);
        packet.send(RELIABLE);
    }

    code = function(code) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.CodeConfirmation);
        packet.writeString(code);
        packet.send(RELIABLE);
    }

    requestPassword = function() {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.PasswordRequest);
        packet.send(RELIABLE);
    }

    password = function(code) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.PasswordConfirm);
        packet.writeString(code);
        packet.send(RELIABLE);
    }

    rerollPassword = function(password) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.PasswordReroll);
        packet.writeString(password);
        packet.send(RELIABLE);
    }

    whiteList = function(code) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.WhiteList);
        packet.writeString(code);
        packet.send(RELIABLE);
    }

    logoutAction = function() {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.Logout);
        packet.send(RELIABLE);
    }

    settingWrite = function(tab) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.Setting);
        packet.writeInt16(tab.len())
        foreach(item in tab) {
            packet.writeString(item.name);
            packet.writeString(item.value);
        }
        packet.send(RELIABLE);
    }

    settingKeyWrite = function(tab) {
        local packet = Packet();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
        packet.writeUInt8(AccountPackets.SettingKey);
        packet.writeInt16(tab.len())
        foreach(name, value in tab) {
            packet.writeString(name);
            packet.writeString(value.tostring());
        }
        packet.send(RELIABLE);
    }
}