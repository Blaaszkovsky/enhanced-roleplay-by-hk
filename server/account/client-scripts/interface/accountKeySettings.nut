keyLabelTab <- {};
keyLabelTab[KEY_1] <- "1";
keyLabelTab[KEY_2] <- "2";
keyLabelTab[KEY_3] <- "3";
keyLabelTab[KEY_4] <- "4";
keyLabelTab[KEY_5] <- "5";
keyLabelTab[KEY_6] <- "6";
keyLabelTab[KEY_7] <- "7";
keyLabelTab[KEY_8] <- "8";
keyLabelTab[KEY_9] <- "9";
keyLabelTab[KEY_0] <- "0";
keyLabelTab[KEY_TAB] <- "TAB";
keyLabelTab[KEY_Q] <- "Q";
keyLabelTab[KEY_W] <- "W";
keyLabelTab[KEY_E] <- "E";
keyLabelTab[KEY_R] <- "R";
keyLabelTab[KEY_T] <- "T";
keyLabelTab[KEY_Y] <- "Y";
keyLabelTab[KEY_U] <- "U";
keyLabelTab[KEY_I] <- "I";
keyLabelTab[KEY_O] <- "O";
keyLabelTab[KEY_P] <- "P";
keyLabelTab[KEY_A] <- "A";
keyLabelTab[KEY_S] <- "S";
keyLabelTab[KEY_D] <- "D";
keyLabelTab[KEY_F] <- "F";
keyLabelTab[KEY_G] <- "G";
keyLabelTab[KEY_H] <- "H";
keyLabelTab[KEY_J] <- "J";
keyLabelTab[KEY_K] <- "K";
keyLabelTab[KEY_L] <- "L";
keyLabelTab[KEY_Z] <- "Z";
keyLabelTab[KEY_X] <- "X";
keyLabelTab[KEY_C] <- "C";
keyLabelTab[KEY_V] <- "V";
keyLabelTab[KEY_B] <- "B";
keyLabelTab[KEY_N] <- "N";
keyLabelTab[KEY_M] <- "M";
keyLabelTab[KEY_RSHIFT] <- "R_SHIFT";
keyLabelTab[KEY_LSHIFT] <- "L_SHIFT";
keyLabelTab[KEY_RCONTROL] <- "R_CTRL";
keyLabelTab[KEY_LCONTROL] <- "L_CTRL";
keyLabelTab[KEY_RMENU] <- "R_ALT";
keyLabelTab[KEY_LMENU] <- "L_ALT";
keyLabelTab[KEY_LWIN] <- "L_WIN";
keyLabelTab[KEY_RWIN] <- "R_WIN";
keyLabelTab[KEY_COMMA] <- ",";
keyLabelTab[KEY_PERIOD] <- ".";
keyLabelTab[KEY_SLASH] <- "/";
keyLabelTab[KEY_SEMICOLON] <- ";";
keyLabelTab[KEY_LBRACKET] <- "[";
keyLabelTab[KEY_RBRACKET] <- "]";
keyLabelTab[KEY_MINUS] <- "-";
keyLabelTab[KEY_EQUALS] <- "=";


AccountKeySettings <- {
    bucket = null
    selectedItem = null,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 350), any(Resolution.y/2 - 400), anx(700), any(800), "SG_BOX.TGA");

        result.buttons <- {};

        local x = 0, y = 0;
        foreach(_itemName, _item in Config["DynamicBindings"]) {
            result.buttons[_itemName] <- GUI.Button(anx(25 + x * 350), any(25 + y * 80), anx(300), any(60), "SG_BUTTON.TGA", "", result.window);
            result.buttons[_itemName].attribute = _itemName;

            if(_item.writable) {
                result.buttons[_itemName].bind(EventType.Click, function(element) {
                    AccountKeySettings.showKeyboard(element.attribute);
                });
            }

            x = x + 1;
            if(x > 1) {
                y = y + 1;
                x = 0;
            }
        }

        result.keyboard <- GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y/2 + 100), anx(800), any(310), "BLACK.TGA");

        result.keyboardKeys <- {};
        result.keyboardKeys[KEY_1] <- GUI.Button(anx(70), any(10), anx(60), any(45), "SG_BUTTON.TGA", "1", result.keyboard);
        result.keyboardKeys[KEY_2] <- GUI.Button(anx(130), any(10), anx(60), any(45), "SG_BUTTON.TGA", "2", result.keyboard);
        result.keyboardKeys[KEY_3] <- GUI.Button(anx(200), any(10), anx(60), any(45), "SG_BUTTON.TGA", "3", result.keyboard);
        result.keyboardKeys[KEY_4] <- GUI.Button(anx(270), any(10), anx(60), any(45), "SG_BUTTON.TGA", "4", result.keyboard);
        result.keyboardKeys[KEY_5] <- GUI.Button(anx(340), any(10), anx(60), any(45), "SG_BUTTON.TGA", "5", result.keyboard);
        result.keyboardKeys[KEY_6] <- GUI.Button(anx(410), any(10), anx(60), any(45), "SG_BUTTON.TGA", "6", result.keyboard);
        result.keyboardKeys[KEY_7] <- GUI.Button(anx(480), any(10), anx(60), any(45), "SG_BUTTON.TGA", "7", result.keyboard);
        result.keyboardKeys[KEY_8] <- GUI.Button(anx(550), any(10), anx(60), any(45), "SG_BUTTON.TGA", "8", result.keyboard);
        result.keyboardKeys[KEY_9] <- GUI.Button(anx(620), any(10), anx(60), any(45), "SG_BUTTON.TGA", "9", result.keyboard);
        result.keyboardKeys[KEY_0] <- GUI.Button(anx(690), any(10), anx(60), any(45), "SG_BUTTON.TGA", "0", result.keyboard);

        result.keyboardKeys[KEY_TAB] <- GUI.Button(anx(10), any(70), anx(70), any(45), "SG_BUTTON.TGA", "TAB", result.keyboard);
        result.keyboardKeys[KEY_Q] <- GUI.Button(anx(90), any(70), anx(60), any(45), "SG_BUTTON.TGA", "Q", result.keyboard);
        result.keyboardKeys[KEY_W] <- GUI.Button(anx(160), any(70), anx(60), any(45), "SG_BUTTON.TGA", "W", result.keyboard);
        result.keyboardKeys[KEY_E] <- GUI.Button(anx(230), any(70), anx(60), any(45), "SG_BUTTON.TGA", "E", result.keyboard);
        result.keyboardKeys[KEY_R] <- GUI.Button(anx(300), any(70), anx(60), any(45), "SG_BUTTON.TGA", "R", result.keyboard);
        result.keyboardKeys[KEY_T] <- GUI.Button(anx(370), any(70), anx(60), any(45), "SG_BUTTON.TGA", "T", result.keyboard);
        result.keyboardKeys[KEY_Y] <- GUI.Button(anx(440), any(70), anx(60), any(45), "SG_BUTTON.TGA", "Y", result.keyboard);
        result.keyboardKeys[KEY_U] <- GUI.Button(anx(510), any(70), anx(60), any(45), "SG_BUTTON.TGA", "U", result.keyboard);
        result.keyboardKeys[KEY_I] <- GUI.Button(anx(580), any(70), anx(60), any(45), "SG_BUTTON.TGA", "I", result.keyboard);
        result.keyboardKeys[KEY_O] <- GUI.Button(anx(650), any(70), anx(60), any(45), "SG_BUTTON.TGA", "O", result.keyboard);
        result.keyboardKeys[KEY_P] <- GUI.Button(anx(720), any(70), anx(60), any(45), "SG_BUTTON.TGA", "P", result.keyboard);

        result.keyboardKeys[KEY_A] <- GUI.Button(anx(110), any(130), anx(60), any(45), "SG_BUTTON.TGA", "A", result.keyboard);
        result.keyboardKeys[KEY_S] <- GUI.Button(anx(180), any(130), anx(60), any(45), "SG_BUTTON.TGA", "S", result.keyboard);
        result.keyboardKeys[KEY_D] <- GUI.Button(anx(250), any(130), anx(60), any(45), "SG_BUTTON.TGA", "D", result.keyboard);
        result.keyboardKeys[KEY_F] <- GUI.Button(anx(320), any(130), anx(60), any(45), "SG_BUTTON.TGA", "F", result.keyboard);
        result.keyboardKeys[KEY_G] <- GUI.Button(anx(390), any(130), anx(60), any(45), "SG_BUTTON.TGA", "G", result.keyboard);
        result.keyboardKeys[KEY_H] <- GUI.Button(anx(460), any(130), anx(60), any(45), "SG_BUTTON.TGA", "H", result.keyboard);
        result.keyboardKeys[KEY_J] <- GUI.Button(anx(530), any(130), anx(60), any(45), "SG_BUTTON.TGA", "J", result.keyboard);
        result.keyboardKeys[KEY_K] <- GUI.Button(anx(600), any(130), anx(60), any(45), "SG_BUTTON.TGA", "K", result.keyboard);
        result.keyboardKeys[KEY_L] <- GUI.Button(anx(670), any(130), anx(60), any(45), "SG_BUTTON.TGA", "L", result.keyboard);

        result.keyboardKeys[KEY_LSHIFT] <- GUI.Button(anx(10), any(190), anx(110), any(45), "SG_BUTTON.TGA", "L_SHIFT", result.keyboard);
        result.keyboardKeys[KEY_Z] <- GUI.Button(anx(130), any(190), anx(60), any(45), "SG_BUTTON.TGA", "Z", result.keyboard);
        result.keyboardKeys[KEY_X] <- GUI.Button(anx(200), any(190), anx(60), any(45), "SG_BUTTON.TGA", "X", result.keyboard);
        result.keyboardKeys[KEY_C] <- GUI.Button(anx(270), any(190), anx(60), any(45), "SG_BUTTON.TGA", "C", result.keyboard);
        result.keyboardKeys[KEY_V] <- GUI.Button(anx(340), any(190), anx(60), any(45), "SG_BUTTON.TGA", "V", result.keyboard);
        result.keyboardKeys[KEY_B] <- GUI.Button(anx(410), any(190), anx(60), any(45), "SG_BUTTON.TGA", "B", result.keyboard);
        result.keyboardKeys[KEY_N] <- GUI.Button(anx(480), any(190), anx(60), any(45), "SG_BUTTON.TGA", "N", result.keyboard);
        result.keyboardKeys[KEY_M] <- GUI.Button(anx(550), any(190), anx(60), any(45), "SG_BUTTON.TGA", "M", result.keyboard);
        result.keyboardKeys[KEY_RSHIFT] <- GUI.Button(anx(620), any(190), anx(110), any(45), "SG_BUTTON.TGA", "R_SHIFT", result.keyboard);

        result.keyboardKeys[KEY_LCONTROL] <- GUI.Button(anx(10), any(250), anx(100), any(45), "SG_BUTTON.TGA", "L_CTRL", result.keyboard);
        result.keyboardKeys[KEY_LMENU] <- GUI.Button(anx(120), any(250), anx(100), any(45), "SG_BUTTON.TGA", "L_ALT", result.keyboard);
        result.keyboardKeys[KEY_LWIN] <- GUI.Button(anx(230), any(250), anx(100), any(45), "SG_BUTTON.TGA", "L_WIN", result.keyboard);
        result.keyboardKeys[KEY_RWIN] <- GUI.Button(anx(440), any(250), anx(100), any(45), "SG_BUTTON.TGA", "R_WIN", result.keyboard);
        result.keyboardKeys[KEY_RMENU] <- GUI.Button(anx(550), any(250), anx(100), any(45), "SG_BUTTON.TGA", "R_ALT", result.keyboard);
        result.keyboardKeys[KEY_RCONTROL] <- GUI.Button(anx(660), any(250), anx(100), any(45), "SG_BUTTON.TGA", "R_CTRL", result.keyboard);

        foreach(_keyId, button in result.keyboardKeys) {
            button.attribute = _keyId;
            button.bind(EventType.Click, function(element) {
                AccountKeySettings.selectKeyboard(AccountKeySettings.selectedItem, element.attribute);
            });
        }

        return result;
    }

    showKeyboard = function(_item) {
        bucket.keyboard.setVisible(true);
        selectedItem = _item;

        foreach(_keyId, _button in bucket.keyboardKeys) {
            if(isKeyBindAccountTaken(_keyId))
                _button.setColor(200,0,0);
            else
                _button.setColor(255,255,255);
        }
    }

    selectKeyboard = function(_item, _value) {
        setAccountKeyBind(_item, _value);
        bucket.keyboard.setVisible(false);
        refresh();
    }

    refresh = function() {
        foreach(_acc, _button in bucket.buttons) {
            _button.setText(Config["DynamicBindings"][_acc].label + " - " + keyLabelTab[Account.keyBinds[_acc]]);
        }
    }

    show = function() {
        if(ActiveGui != null)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.KeySelector;
        refresh();
    }

    hide = function() {
        selectedItem = null;
        bucket.keyboard.setVisible(false);
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        Account.Packet.settingKeyWrite(Account.keyBinds);
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.KeySelector)
            hide();
    }
}

Bind.addKey(KEY_ESCAPE, AccountKeySettings.hide.bindenv(AccountKeySettings), PlayerGUI.KeySelector)
addEventHandler("onForceCloseGUI", AccountKeySettings.onForceCloseGUI.bindenv(AccountKeySettings));
