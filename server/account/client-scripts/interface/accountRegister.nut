
local window = GUI.Window(any(Resolution.x/2 - 550), any(Resolution.y/2 - 300), anx(424), any(510), "SG_BOX.TGA", null, false);
local logo = GUI.Button(anx(-40), any(-230), anx(512), any(256), "SG_LOGO.TGA", "", window);

local registerButton = GUI.Button(anx(42), any(330), anx(340), any(50), "SG_BUTTON.TGA", "Rejestracja", window);
local goLoggInButton = GUI.Button(anx(42), any(400), anx(340), any(50), "SG_BUTTON.TGA", "Wr��", window);

local username = GUI.Input(anx(42), any(70), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa u�ytkownika", 2, window);
local password = GUI.Input(anx(42), any(140), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Has�o", 2, window);
local passwordRepeat = GUI.Input(anx(42), any(210), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Powt�rz Has�o", 2, window);

local draws = [
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

registerButton.bind(EventType.Click, function(element) {
    sendRegister();
});

goLoggInButton.bind(EventType.Click, function(element) {
    hideOutRegisterMenu();
    showOutTemporaryLoggInMenu();
});

function sendRegister() {
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({password = password, passwordRepeat = passwordRepeat, username = username});

    if(uiResult.password != uiResult.passwordRepeat) {
        showRegisterConfirmationError("Podane has�a si� nie zgadzaj�.");
        return;
    }

    if(uiResult.username.len() > 32) {
        showRegisterConfirmationError("Nazwa gracza zbyt d�uga.");
        return;
    }

    if(uiResult.password.len() < 4) {
        showRegisterConfirmationError("Has�o zbyt kr�tkie.");
        return;
    }

    if(uiResult.password.len() > 32) {
        showRegisterConfirmationError("Has�o zbyt d�ugie.");
        return;
    }

    if(uiResult.username.len() < 4) {
        showRegisterConfirmationError("Nazwa konta zbyt kr�tka.");
        return;
    }

    if(uiResult.password.len() > 3 && uiResult.username.len() > 3) {
        Loading.show();
        Account.Packet.register(uiResult.username, uiResult.password);
    }else
        showRegisterConfirmationError("Has�o i nazwa min. 4 znaki.")
}

function showRegisterConfirmationError(message) {
    draws[0].setText(message);
}

function showRegisterMenu() {
    ActiveGui = PlayerGUI.Register;
    window.setVisible(true);
}

function hideRegisterMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

function hideOutRegisterMenu() {
    window.setVisible(false);
}