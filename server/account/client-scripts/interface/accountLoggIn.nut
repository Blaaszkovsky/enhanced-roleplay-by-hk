
local window = GUI.Window(any(Resolution.x/2 - 550), any(Resolution.y/2 - 250), anx(424), any(562), "SG_BOX.TGA", null, false);
local logo = GUI.Button(anx(-40), any(-230), anx(512), any(256), "SG_LOGO.TGA", "", window);

local loggInButton = GUI.Button(anx(42), any(290), anx(340), any(50), "SG_BUTTON.TGA", "Zaloguj", window);
local exitGameButton = GUI.Button(anx(42), any(360), anx(340), any(50), "SG_BUTTON.TGA", "Wyjd�", window);
local registerButton = GUI.Button(anx(42), any(450), anx(340), any(50), "SG_BUTTON.TGA", "Rejestracja", window);

local path = CameraPath()

path.createPoint({position = [-55871.8,184.508,-118890], rotation = [3.15186,1073.08,0], lerpSpeed = 300});
path.createPoint({position = [-55933,107.407,-116080], rotation = [-2.5,1087.92,0], lerpSpeed = 300});
path.createPoint({position = [-57792.3,515.5,-113435], rotation = [-6.01562,1025.53,0], lerpSpeed = 300});
path.createPoint({position = [-57249.8,510.494,-111084], rotation = [0.185547,1152.82,0], lerpSpeed = 300});
path.createPoint({position = [-54225.2,490.002,-109538], rotation = [-1.1084,1089.12,0], lerpSpeed = 300});
path.createPoint({position = [-53274.1,676.023,-107056], rotation = [-6.47949,1133.35,0], lerpSpeed = 300});
path.createPoint({position = [-50595.8,935.951,-107311], rotation = [1.19873,1208.44,0], lerpSpeed = 300});
path.createPoint({position = [-51304.6,1197.9,-103949], rotation = [-11.4844,1152.93,0], lerpSpeed = 300});
path.createPoint({position = [-55426.7,606.082,-106373], rotation = [2.77344,1005.53,0], lerpSpeed = 300});
path.createPoint({position = [-59666.8,1113.33,-104825], rotation = [9.16992,892.039,0], lerpSpeed = 300});
path.createPoint({position = [-62762,519.026,-107214], rotation = [2.49268,861.839,0], lerpSpeed = 300});
path.createPoint({position = [-60865.6,529.86,-110516], rotation = [1.01562,897.349,0], lerpSpeed = 300});
path.createPoint({position = [-63616.3,888.536,-113029], rotation = [12.124,793.443,0], lerpSpeed = 300});

loggInButton.bind(EventType.Click, function(element) {
    sendLoggIn();
});

exitGameButton.bind(EventType.Click, function(element) {
    exitGame();
});

registerButton.bind(EventType.Click, function(element) {
    hideOutTemporaryLoggInMenu();
    showRegisterMenu();
});


local remember = GUI.CheckBox(anx(320), any(220), anx(35), any(40), "SG_CHECKBOX.TGA", "SG_CHECKBOX_CHECKED.TGA", window);
local password = GUI.Input(anx(42), any(140), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Has�o", 2, window);
local username = GUI.Input(anx(42), any(70), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa u�ytkownika", 2, window);
local draws = [
    GUI.Draw(anx(50), any(230), "Zapami�taj has�o:", window),
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

function sendLoggIn()
{
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({password=password,username=username,remember=remember});

    if(uiResult.password.len() > 3 && uiResult.username.len() > 3) {
        Loading.show();
        Account.Packet.loggIn(uiResult);
        if(uiResult.remember) {
            if("LocalStorage" in getroottable()) {
                LocalStorage.setItem("password", uiResult.password);
                LocalStorage.setItem("username", uiResult.username);
            }
        }
    }else
        showLoggInError("Has�o i nazwa min. 4 znaki.")
}

function showLoggInError(value)
{
    local positionDraw = draws[1].getPosition();
    draws[1].setText(value);
    draws[1].setPosition(anx(700), positionDraw.y);
}

function showOutTemporaryLoggInMenu() {
    window.setVisible(true);
    ActiveGui = PlayerGUI.LoggIn;
}

function showLoggInMenu()
{
    if(path == null) {
        path = CameraPath()

        path.createPoint({position = [-55871.8,184.508,-118890], rotation = [3.15186,1073.08,0], lerpSpeed = 300});
        path.createPoint({position = [-55933,107.407,-116080], rotation = [-2.5,1087.92,0], lerpSpeed = 300});
        path.createPoint({position = [-57792.3,515.5,-113435], rotation = [-6.01562,1025.53,0], lerpSpeed = 300});
        path.createPoint({position = [-57249.8,510.494,-111084], rotation = [0.185547,1152.82,0], lerpSpeed = 300});
        path.createPoint({position = [-54225.2,490.002,-109538], rotation = [-1.1084,1089.12,0], lerpSpeed = 300});
        path.createPoint({position = [-53274.1,676.023,-107056], rotation = [-6.47949,1133.35,0], lerpSpeed = 300});
        path.createPoint({position = [-50595.8,935.951,-107311], rotation = [1.19873,1208.44,0], lerpSpeed = 300});
        path.createPoint({position = [-51304.6,1197.9,-103949], rotation = [-11.4844,1152.93,0], lerpSpeed = 300});
        path.createPoint({position = [-55426.7,606.082,-106373], rotation = [2.77344,1005.53,0], lerpSpeed = 300});
        path.createPoint({position = [-59666.8,1113.33,-104825], rotation = [9.16992,892.039,0], lerpSpeed = 300});
        path.createPoint({position = [-62762,519.026,-107214], rotation = [2.49268,861.839,0], lerpSpeed = 300});
        path.createPoint({position = [-60865.6,529.86,-110516], rotation = [1.01562,897.349,0], lerpSpeed = 300});
        path.createPoint({position = [-63616.3,888.536,-113029], rotation = [12.124,793.443,0], lerpSpeed = 300});
    }

    path.start();
    clearMultiplayerMessages();
    enable_DamageAnims(false);
    enable_NicknameId(true);

    if("LocalStorage" in getroottable()) {
        local pass = LocalStorage.getItem("password")
        if(pass != null) {
            password.setText(pass);
            remember.setChecked(true);
        }

        local user = LocalStorage.getItem("username")
        if(user != null)
            username.setText(user);
    }

    BaseGUI.show();
    ActiveGui = PlayerGUI.LoggIn;

    window.setVisible(true);
}

function hideOutTemporaryLoggInMenu() {
    window.setVisible(false);
}

local function onStopCamera(obj)
{
    if(path == null)
        return;

    if(ActiveGui != PlayerGUI.LoggIn)
        return;

    path.start();
}

local function hideLoggIn()
{
    BaseGUI.hide();
    ActiveGui = null;
    path.stop();

    window.setVisible(false);
}

function hideCameraPath() {
    path = null;
    deleteActiveCameraPath();
}

addEventHandler("CameraPath.onStop", onStopCamera)