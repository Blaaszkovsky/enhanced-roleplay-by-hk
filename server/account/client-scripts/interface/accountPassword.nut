
local window = GUI.Window(any(Resolution.x/2 - 550), any(Resolution.y/2 - 250), anx(424), any(562), "SG_BOX.TGA", null, false);
// local logo = GUI.Button(anx(-20), any(-176), anx(424), any(212), "GS_LOGO.TGA", "", window);

local resetPasswordButton = GUI.Button(anx(42), any(350), anx(340), any(50), "SG_BUTTON.TGA", "Potwierd�", window);

GUI.Draw(anx(42), any(70), "Wpisz kod, kt�ry dosta�e�\nna podany wcze�niem e-mail.", window);
local code = GUI.Input(anx(42), any(140), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Kod", 2, window);
local password = GUI.Input(anx(42), any(210), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Has�o", 2, window);
local passwordRepeat = GUI.Input(anx(42), any(280), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Powt�rz Has�o", 2, window);

local draws = [
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

resetPasswordButton.bind(EventType.Click, function(element) {
    sendResetPassword();
});

function sendResetPassword() {
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({code = code, password = password, passwordRepeat = passwordRepeat});

    if(uiResult.password != uiResult.passwordRepeat) {
        showPasswordError("Has�o powt�rzone nie zgadza si� z potem has�o.");
        return;
    }

    if(uiResult.password.len() < 4) {
        showPasswordError("Has�o zbyt kr�tkie.");
        return;
    }

    if(uiResult.password.len() > 32) {
        showPasswordError("Has�o zbyt d�ugie.");
        return;
    }

    Loading.show();
    Account.Packet.password(uiResult.code, uiResult.password);
    hideOutPasswordMenu();
    showOutTemporaryLoggInMenu();
}

function showPasswordError(message) {
    draws[0].setText(message);
}

function showPasswordMenu() {
    ActiveGui = PlayerGUI.Password;
    window.setVisible(true);
}

function hidePasswordMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

function hideOutPasswordMenu() {
    window.setVisible(false);
}