
local window = GUI.Window(anx(80), any(Resolution.y/2 - 250), anx(424), any(562), "SG_BOX.TGA", null, false);
// local logo = GUI.Button(anx(-20), any(-176), anx(424), any(212), "GS_LOGO.TGA", "", window);

local goCodeButton = GUI.Button(anx(42), any(210), anx(340), any(50), "SG_BUTTON.TGA", "Potwierd�", window);

GUI.Draw(anx(42), any(70), "Wpisz tutaj kod weryfikacyjny\notrzymany od administracji.", window);
local code = GUI.Input(anx(42), any(140), anx(340), any(45), "SG_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Kod", 2, window);

local draws = [
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

goCodeButton.bind(EventType.Click, function(element) {
    sendCodeAcceptation();
});

function sendCodeAcceptation() {
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({code = code});

    if(uiResult.code.len() != 6) {
        showRegisterConfirmationError("Kod ma 6 znak�w.");
        return;
    }

    Loading.show();
    Account.Packet.code(uiResult.code);
}

function showCodeConfirmationError() {
    draws[0].setText("B��dny kod aktywacyjny.");
}

function showCodeMenu() {
    ActiveGui = PlayerGUI.Code;
    window.setVisible(true);
}

function hideCodeMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

function hideOutCodeMenu() {
    window.setVisible(false);
}