
Account.Controller <- {
    onPacket = function(packet)
    {
        local identfier = packet.readUInt8();
        if(identfier != PacketAccount)
            return;

        local typeId = packet.readUInt8();
        if(typeId != Packets.Account)
            return;

        local typ = packet.readUInt8();

        switch(typ)
        {
            case AccountPackets.Register:
                Loading.hide();
                showRegisterConfirmationError(packet.readString());
            break;
            case AccountPackets.LoggIn:
                Loading.hide();
                showLoggInError(packet.readString());
            break;
            case AccountPackets.PasswordReroll:
                Loading.hide();
                showPasswordRerollError();
            break;
            case AccountPackets.CodeConfirmation:
                Loading.hide();

                if(ActiveGui == PlayerGUI.Code)
                    showCodeConfirmationError();
                else {
                    if(ActiveGui == PlayerGUI.LoggIn)
                        hideOutTemporaryLoggInMenu();
                    else if(ActiveGui == PlayerGUI.Register)
                        hideOutRegisterMenu();

                    showCodeMenu();
                }
            break;
            case AccountPackets.Accept:
                Account.id = packet.readInt16();
                Account.loggIn = true;
                Loading.hide();

                local showPaswordReroll = packet.readBool();

                if(ActiveGui == PlayerGUI.LoggIn)
                    hideOutTemporaryLoggInMenu();
                else if(ActiveGui == PlayerGUI.Register)
                    hideOutRegisterMenu();
                else if(ActiveGui == PlayerGUI.Code)
                    hideOutCodeMenu();

                if(showPaswordReroll)
                    showPasswordRerollMenu();
                else {
                    hideCameraPath();
                    showCharacterList();
                }
            break;
            case AccountPackets.Logout:
                Loading.hide();
                clearInventory();
                clearCharacters();
                resetHeroStatistics();
                showLoggInMenu();
            break;
            case AccountPackets.Skins:
                local message = packet.readString();
                Account.extraSkins = {"SKIN": [], "FACE": []};

                if(message.len() == 0)
                    return;

                message = split(message, ",");
                foreach(item in message) {
                    local args = sscanf("dd", item);
                    if (args) {
                        local id = args[0];
                        if(id == 0)
                            id = "FACE";
                        else
                            id = "SKIN";

                        Account.extraSkins[id].push(args[1])
                    }
                }
            break;
            case AccountPackets.Setting:
                local settingLen = packet.readInt16();
                local data = {};

                for(local i = 0; i < settingLen; i++) {
                    local settingName = packet.readString();
                    local settingValue = packet.readString();

                    switch(settingName) {
                        case "Resolution":
                            settingValue = settingValue.tostring();
                        break;
                        case "ChatAnimation":
                            settingValue = settingValue == "true" ? true : false;
                        break;
                        case "DialogCustomFont":
                            settingValue = settingValue == "true" ? true : false;
                        break;
                        case "OtherCustomFont":
                            settingValue = settingValue == "true" ? true : false;
                        break;
                        case "ChatPercentOnScreen":
                            settingValue = settingValue.tointeger();
                        break;
                        case "ChatLines":
                            settingValue = settingValue.tointeger();
                        break;
                        case "InterfaceScale":
                            settingValue = settingValue.tointeger();
                        break;
                        case "DialogAutoContinue":
                            settingValue = settingValue.tointeger();
                        break;
                        case "NamesRound":
                            settingValue = settingValue == "true" ? true : false;
                        break
                        case "SoundtrackVolume":
                            settingValue = settingValue.tofloat();
                        break;
                    }

                    data[settingName] <- settingValue;
                }

                callEvent("onAccountReceiveSettingData", data);
            break;
            case AccountPackets.SettingKey:
                local settingLen = packet.readInt16();

                for(local i = 0; i < settingLen; i++) {
                    local settingName = packet.readString();
                    local settingValue = packet.readInt16();

                    Account.keyBinds[settingName] = settingValue;
                }
            break;
        }
    }

    onCommand = function(command, params) {
        switch(command) {
            case "extra_face":
                local args = sscanf("d", params);
                if (!args)
                {
                    Chat.addMessage(ChatType.OOC, 255, 255, 255, "/extra_face <id dodatkowej twarzy>");
                    foreach(id, faceId in Account.extraSkins["FACE"]) {
                        Chat.addMessage(ChatType.OOC, 255, 255, 255, "Id: "+id+" - twarz nr."+faceId);
                    }
                    return;
                }

                local getVisual = getPlayerVisual(heroId);
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, Account.extraSkins["FACE"][args[0]]);
                getVisual = getPlayerVisual(heroId);
                PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
            case "extra_skin":
                local args = sscanf("d", params);
                if (!args)
                {
                    Chat.addMessage(ChatType.OOC, 255, 255, 255, "/extra_skin <id dodatkowej sk�ry>");
                    foreach(id, skinId in Account.extraSkins["SKIN"]) {
                        Chat.addMessage(ChatType.OOC, 255, 255, 255, "Id: "+id+" - sk�ra nr."+skinId);
                    }
                    return;
                }

                local getVisual = getPlayerVisual(heroId);
                setPlayerVisual(heroId, getVisual.bodyModel, Account.extraSkins["SKIN"][args[0]], getVisual.headModel, getVisual.headTxt);
                getVisual = getPlayerVisual(heroId);
                PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
        }
    }
}

addEventHandler("onPacket", function(packet) { Account.Controller.onPacket(packet); });
addEventHandler("onCommand", function(command, params) { Account.Controller.onCommand(command, params); });