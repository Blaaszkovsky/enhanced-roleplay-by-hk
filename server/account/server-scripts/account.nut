
class Account
{
    static dbTable = "account";
    static dbWhiteListTable = "accountwhitelist";
    static saltPassword = "someSaltPassword";

    rId = -1;
    id = -1;

    username = "";
    password = "";

    createdAt = "";
    updatedAt = "";

    confirmationCode = "";
    isConfirmed = false;
    playerUID = "";
    resetCode = "";

    nextPnToCharacter = 0

    isNeedPasswordReroll = false
    isNeedAcceptPolicy = false
    packetManager = null;

    constructor(id)
    {
        this.rId = -1;
        this.id = id;

        this.username = "";
        this.password = "";

        this.createdAt = "";
        this.updatedAt = "";

        this.confirmationCode = "";
        this.isConfirmed = true;
        this.playerUID = "";
        this.resetCode = "";
        this.isNeedPasswordReroll = false;
        this.isNeedAcceptPolicy = false;

        this.nextPnToCharacter = 0;

        this.packetManager = AccountPacket(id);
    }

    function save() {
        local dateObject = date();
        updatedAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        username = mysql_escape_string(username);
        if(username == null)
            username = "";

        password = mysql_escape_string(password);
        if(password == null)
            password = "";


        confirmationCode = mysql_escape_string(confirmationCode);
        if(confirmationCode == null)
            confirmationCode = "";

        playerUID = mysql_escape_string(playerUID);
        if(playerUID == null)
            playerUID = "";

        DB.queryGet("UPDATE "+Account.dbTable+" SET online = 0 WHERE id = "+rId+";");

        Query().update(Account.dbTable, [
            "username",
            "password",
            "serial",
            "ip",
            "isConfirmed",
            "createdAt",
            "updatedAt",
            "nextPnToCharacter",
            "isNeedPasswordReroll",
            "isNeedAcceptPolicy",
        ], [
            "'"+username+"'",
            "'"+password+"'",
            "'"+getPlayerSerial(id)+"'",
            "'"+getPlayerIP(id)+"'",
            isConfirmed ? 1 : 0,
            "'"+createdAt+"'",
            "'"+updatedAt+"'",
            nextPnToCharacter,
            isNeedPasswordReroll ? 1 : 0,
            isNeedAcceptPolicy ? 1 : 0,
		]).where(["id = "+rId]).execute();
    }

    function register() {
        local dateObject = date();
        createdAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        username = mysql_escape_string(username);
        if(username == null)
            username = "";

        password = mysql_escape_string(password);
        if(password == null)
            password = "";

        playerUID = mysql_escape_string(playerUID);
        if(playerUID == null)
            playerUID = "";

        confirmationCode = mysql_escape_string(confirmationCode);
        if(confirmationCode == null)
            confirmationCode = "";

        Query().insertInto(Account.dbTable, [
            "username",
            "password",
            "confirmationCode",
            "resetCode",
            "serial",
            "playerUID",
            "ip",
            "isConfirmed",
            "createdAt",
            "updatedAt",
            "nextPnToCharacter",
            "isNeedAcceptPolicy"
        ], [
            "'"+username+"'",
            "'"+password+"'",
            "'"+confirmationCode+"'",
            "''",
            "'"+getPlayerSerial(id)+"'",
            "'"+getPlayerUID(id)+"'",
            "'"+getPlayerIP(id)+"'",
            1,
            "'"+createdAt+"'",
            "'"+createdAt+"'",
            0,
            1
		]).execute();
    }

    function load() {
        local object = Query().select().from(Account.dbTable).where(["username = '"+username+"'", "password = '"+password+"'"]).one();

        rId = object["id"];
        username = object["username"];
        password = object["password"];
        createdAt = object["createdAt"];
        updatedAt = object["updatedAt"];
        resetCode = object["resetCode"];
        playerUID = object["playerUID"];
        confirmationCode = object["confirmationCode"];
        nextPnToCharacter = object["nextPnToCharacter"].tointeger();
        isConfirmed = object["isConfirmed"].tointeger() == 1 ? true : false;
        isNeedPasswordReroll = object["isNeedPasswordReroll"].tointeger() == 1 ? true : false;
        isNeedAcceptPolicy = object["isNeedAcceptPolicy"].tointeger() == 1 ? true : false;

        packetManager.sendSkins(object["skinsIds"]);

        DB.queryGet("UPDATE "+Account.dbTable+" SET online = 1 WHERE id = "+rId+";");
    }


    function clear()
    {

        if(rId != -1)
            save();

        rId = -1;

        username = "";
        password = "";

        createdAt = "";
        updatedAt = "";

        isConfirmed = false;

        confirmationCode = "";
        resetCode = "";
        playerUID = "";

        nextPnToCharacter = 0;
        isNeedPasswordReroll = false;
        isNeedAcceptPolicy = false;



    }

    static function setNextPnToCharacter(id, value) {
        local obj = getAccount(id);

        obj.nextPnToCharacter = value;
        obj.save();

        addPlayerLog(id, "Dodaj do konta dodatkowe PN po CK. " + value);
    }

    static function getNextPnToCharacter(id) {
        local obj = getAccount(id);
        if(obj.nextPnToCharacter == 0)
            return 0;

        local receivePN = obj.nextPnToCharacter;

        obj.nextPnToCharacter = 0;
        obj.save();

        addPlayerLog(id, "Tworzy posta� z dodatkowym PN " + receivePN);

        return receivePN;
    }

    static function findByUsername(username) {
		username = mysql_escape_string(username);

        return Query().select().from(Account.dbTable).where(["username = '"+username+"'"]).one();
    }

    static function findBySerial(serial) {
		serial = mysql_escape_string(serial);

        return Query().select().from(Account.dbTable).where(["serial = '"+serial+"'"]).one();
    }
    static function findByPlayerUID(playerUID, username) {
		playerUID = mysql_escape_string(playerUID);

        return Query().select().from(Account.dbTable).where(["playerUID = '"+playerUID+"'", "username = '"+username+"'"]).one();
    }

    static function findByPlayerUIDAlt(playerUID, username) {
		playerUID = mysql_escape_string(playerUID);

        return Query().select().from(Account.dbTable).where(["playerUIDAlt = '"+playerUID+"'", "username = '"+username+"'"]).one()
    }

    static function findWhiteList(code) {
		code = mysql_escape_string(code);

        return Query().select().from(Account.dbWhiteListTable).where(["code = '"+code+"' AND active = 1"]).one();
    }

    static function prepareHash(pass) {
        local preparedPassword = md5(pass);
        return "_"+md5(Account.saltPassword + preparedPassword);
    }
}