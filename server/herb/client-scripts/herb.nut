
class Herb
{
    id = -1
    position = null
    rotation = -1

    scheme = -1
    status = -1

    stamina = -1
    growth = -1

    vob = null
    ground = null
    draw = null

    observer = null

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        stamina = 0
        rotation = 0
        status = 0
        growth = 0

        vob = Vob("SKULL.3DS");
        ground = Vob("KOPA.3DS");
        draw = null;

        observer = null;
    }

    function addToWorld(id)
    {
        draw = WorldInterface.Draw3d(position.x,position.y + 109.40,position.z);
        draw.addLine("Ro�lina ("+id+") "+status+"/"+getScheme().len());
        draw.addLine("Wzrost: "+growth+"/"+getScheme()[status].time);
        draw.addLine("Kondycja: "+stamina+"%");
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        draw.distance = 800;
        draw.setScale(0.6, 0.6);
        draw.visible = true;
        updateDrawText();

        local info = Daedalus.instance(getScheme()[status].instance)
        vob = Vob(info.visual);

        ground.cdDynamic = false;
        ground.cdStatic = false;

        ground.setPosition(position.x, position.y, position.z);
        vob.setPosition(position.x, position.y + 20,position.z);

        vob.setRotation(0,rotation,0);
        ground.setRotation(0,rotation,0);

        ground.cdDynamic = true;
        ground.cdStatic = true;

        vob.addToWorld();
        ground.addToWorld();
        observer = InteractionTree.addDistanceObserver(position.x,position.y + 109.40,position.z, 150, "Praca nad ro�lin� ("+id+")", function() {HerbController.interactWithHerb(id); });
    }

    function updateDrawText() {
        if((status+1) == getScheme().len()) {
            draw.setLineText(0, "Ro�lina uros�a ("+id+")");
            draw.setLineText(1, "");
            draw.setLineText(2, "");
        } else {
            draw.setLineText(0, "Ro�lina ("+id+") "+(status+1)+"/"+getScheme().len());
            draw.setLineText(1, "Wzrost: "+growth+"/"+getScheme()[status].time);
            draw.setLineText(2, "Kondycja: "+stamina+"%");
        }

        if(stamina >= 75)
            draw.setLineColor(2, 0, 171, 68);
        else if(stamina < 75 && stamina > 35)
            draw.setLineColor(2, 171, 142, 0);
        else
            draw.setLineColor(2, 163, 5, 34);
    }

    function update(_growth, _stamina, _status) {
        if(status != _status)
        {
            status = _status;
            local info = Daedalus.instance(getScheme()[status].instance)
            vob.visual = info.visual;
        }

        stamina = _stamina;
        growth = _growth;
        updateDrawText();
    }

    function getScheme() {
        return Config["HerbSystem"][scheme];
    }

    function remove()
    {
        InteractionTree.removeDistanceObserver(observer);
        draw.remove();
    }
}