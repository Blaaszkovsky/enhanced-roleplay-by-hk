
class HerbPacket
{
    packet = null;
    object = null;

    constructor(herbObject)
    {
        packet = ExtendedPacket(Packets.Herb);
        object = herbObject;
    }

    function updateStamina() {
        packet.writeUInt8(HerbPackets.Stamina)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function deleteHerb() {
        packet.writeUInt8(HerbPackets.Delete)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function takeHerb() {
        packet.writeUInt8(HerbPackets.Growth)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }
}