
class HerbController
{
    static herbs = {};

    static function interactWithGround()
    {
        local pos = getPlayerPosition(heroId);
        local possibleLevelGround = Ground.getByPosition(pos.x,pos.y,pos.z);

        if(possibleLevelGround == null)
            return;

        if(hasItemThatIsNotDestroyed("ITMW_SDRP_2H_OTH_07") == false) {
            addPlayerNotification(heroId, "Nie masz �opaty.");
            return;
        }

        if(!(getPlayerMeleeWeapon(heroId) == Items.id("ITMW_SDRP_2H_OTH_07") && getPlayerWeaponMode(heroId) == WEAPONMODE_2HS)) {
            addPlayerNotification(heroId, "Wyci�gnij �opat�.");
            return;
        }

        if(getNearHerb(pos) == null)
            HerbInterfaceCreate.show(possibleLevelGround);
        else
            addPlayerNotification(heroId, "Zbyt blisko inna ro�lina.");
    }

    static function interactWithHerb(id) {
        local pos = getPlayerPosition(heroId);
        // local herbId = getNearHerb(pos);
        local herbId = id;
        if(herbId == null)
            return;

        HerbInterface.show(herbId);
    }

    static function getNearHerb(pos)
    {
        foreach(herbId, herb in herbs)
            if(getPositionDifference(pos, herb.position) < 200)
                return herbId;

        return null;
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case HerbPackets.Create:
                local id = packet.readInt32();
                local scheme = packet.readString();
                local status = packet.readInt16();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotation = packet.readInt16();
                local growth = packet.readInt16();
                local stamina = packet.readInt16();

                local obj = Herb(id);
                obj.scheme = scheme;
                obj.status = status;
                obj.position = {x = positionX, y = positionY, z = positionZ};
                obj.rotation = rotation;
                obj.growth = growth;
                obj.stamina = stamina;
                obj.addToWorld(id);

                herbs[id] <- obj;
            break;
            case HerbPackets.Delete:
                local id = packet.readInt32();

                if(!(id in herbs))
                    return;

                getHerb(id).remove();
                herbs.rawdelete(id);
            break;
            case HerbPackets.Update:
                local length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    herbs[packet.readInt32()].update(packet.readInt16(),packet.readInt16(),packet.readInt16())
            break;
        }
    }
}

getHerb <- @(id) HerbController.herbs[id];
getHerbs <- @() HerbController.herbs;

RegisterPacketReceiver(Packets.Herb, HerbController.onPacket.bindenv(HerbController));
