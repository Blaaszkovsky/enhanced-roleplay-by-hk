//0 - Wypalona gleba - polna gleba
//1 - Gleba niskiej jako�ci - lesna gleba
//2 - Zwyk�a gleba - gorska gleba
//3 - Bogata w minera�y gleba - bagienna gleba
//4 - Idealna gleba - gleba przy wodzie
//[x,y,z,ground] - przyk�ad dodania nowej gleby [-85840.6,516.431,-122572, 0], dodaje wypalon� glebe

Ground <- {};
Ground.list <- [
    [
        [-85896.7,638.672,-123453, 0],
        [-85969.3,631.953,-122393, 0],
        [-85498.2,579.922,-123087, 0],
        [-95966,196.016,-116695, 3],
        [-93304.1,198.984,-120463, 3],
        [-110569,537.891,-125742, 3],
        [-77454.8,-2553.44,-115438, 3],
        [24051.4,-2649.77,-13973.3, 0],

    ],
]

addEventHandler("onInit", function() {
    foreach(index, tree in Ground.list)
        foreach(item in tree)
            InteractionTree.addDistanceObserver(item[0], item[1], item[2], 600, "Kopanie w ziemii.", function() {HerbController.interactWithGround(); });
})

Ground.getByPosition <- function(x,y,z) {
    foreach(index, tree in Ground.list)
        foreach(item in tree)
            if(getDistance2d(item[0], item[2], x, z) < 600)
                return item[3];

    return null;
}