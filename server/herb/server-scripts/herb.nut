
class Herb
{
    static HerbTable = "herb";

    id = -1
    position = null

    scheme = -1
    status = -1
    stamina = -1
    growth = -1
    rotation = -1

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        stamina = 0
        rotation = 0
        status = 0
        growth = 0
    }

    function onSecond() {
        local chance = irand(100);
        local needUpdate = false;

        if(getScheme().len() == (status + 1))
            return false;

        local needGrowth = true;
        if(stamina < 75)
        {
            local chanceToGrowth = stamina + 25;
            if(chanceToGrowth < chance)
                needGrowth = false;
        }

        if(stamina < 0) {
            if(chance < 25)
                stamina = stamina - 1;
            needUpdate = true;
        } else {
            if(chance < 25)
                stamina = stamina - 1;
    
            if(needGrowth)
                growth = growth + 1;
    
            if(growth >= getScheme()[status].time)
            {
                growth = 0;
                status = status + 1;
                needUpdate = true;
            }
    
            if(growth % 10 == 0)
                needUpdate = true;
        }


        if(needUpdate)
            update();

        return needGrowth;
    }

    function update() {
        DB.queryGet("UPDATE "+ Herb.HerbTable +" SET growth = "+growth+", status = "+status+", stamina = "+stamina+" WHERE id = "+id);
    }

    function getScheme() {
        return Config["HerbSystem"][scheme];
    }
}