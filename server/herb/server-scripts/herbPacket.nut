
class HerbPacket
{
    packet = null;
    object = null;

    constructor(herbObject)
    {
        packet = ExtendedPacket(Packets.Herb);
        object = herbObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(HerbPackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.scheme);
        packet.writeInt16(object.status);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeInt16(object.rotation);
        packet.writeInt16(object.growth);
        packet.writeInt16(object.stamina);

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function deleteHerb() {
        packet.writeUInt8(HerbPackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}