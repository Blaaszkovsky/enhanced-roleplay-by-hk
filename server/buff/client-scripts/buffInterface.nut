
BuffInterface <- {
    icons = {},

    show = function() {
        foreach(icon in icons)
            icon.visible = true;
    }

    hide = function() {
        foreach(icon in icons)
            icon.visible = false;
    }

    reorder = function() {
        local startPosition = Resolution.x/2 - ((icons.len() * 44)/2);
        foreach(icon in icons) {
            icon.setPosition(anx(startPosition), any(Resolution.y - 60));
            startPosition = startPosition + 44;
        }
    }

    onAdd = function(object) {
        BuffInterface.icons[object.scheme] <- Texture(anx(0), anx(0), anx(34), any(34), object.getScheme().icon);
        BuffInterface.reorder();

        if(ActiveGui == null)
            BuffInterface.icons[object.scheme].visible = true;
    }

    onRemove = function(object) {
        BuffInterface.icons.rawdelete(object.scheme);
        BuffInterface.reorder();
    }
}