
class Buff
{
    id = -1
    interval = -1

    scheme = ""
    counter = -1

    constructor(id, scheme, counter, interval) {
        this.id = id
        this.scheme = scheme
        this.counter = counter
        this.interval = interval
    }

    function enable() {
    }

    function disable() {

    }

    function getScheme() {
        return Config["Buffs"][scheme];
    }

    function getType() {
        return getScheme().type;
    }
}