class BuffPacket {
    packet = null;
    obj = null

    constructor(obj) {
        this.obj = obj
        this.packet = ExtendedPacket(Packets.Buff);
    }

    function add() {
        packet.writeUInt8(BuffPackets.Add)
        packet.writeInt32(obj.id);
        packet.writeString(obj.scheme);
        packet.writeInt16(obj.counter);
        packet.writeInt16(obj.interval);
        packet.send(obj.playerId, RELIABLE);
    }

    function remove() {
        packet.writeUInt8(BuffPackets.Remove)
        packet.writeInt32(obj.id);
        packet.send(obj.playerId, RELIABLE);
    }
}