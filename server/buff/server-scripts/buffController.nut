
class BuffController
{
    static buffs = {}

    static function resetBuffsForPlayer(playerId) {
        local rId = getPlayer(playerId).rId;
        Query().deleteFrom(Buff.BuffTable).where(["playerId = "+rId]).execute();

        if(!(playerId in buffs))
            return;

        for(local i = (buffs[playerId].len() - 1); i >= 0; i--) {
            buffs[playerId][i].disable();
            buffs[playerId][i].packetRemove();
        }

        buffs[playerId].clear();
    }

    static function recreateBuffForId(playerId) {
        buffs[playerId] <- [];
    }

    static function givePlayerBuff(playerId, schemeName) {
        if(!(schemeName in Config["Buffs"]))
            return;

        local sxm = Config["Buffs"][schemeName];
        local buffExist = false;

        foreach(buff in buffs[playerId]) {
            if(buff.scheme == schemeName) {
                local needToStack = false;
                if("stack" in sxm)
                    if(sxm.stack == true)
                        needToStack = true;

                if(needToStack)
                    buff.counter = buff.counter + sxm.time;
                else
                    buff.counter = sxm.time;

                buffExist = true;
                break;
            }
        }

        if(buffExist == false) {
            Query().insertInto(Buff.BuffTable, ["playerId", "scheme", "counter", "intervalsec"], [getPlayer(playerId).rId, "'"+schemeName+"'", sxm.time, 0]).execute();

            local buffObject = Query().select().from(Buff.BuffTable).where(["playerId = "+getPlayer(playerId).rId]).orderBy("id DESC").one();
            local loadedBuff = Buff(
                buffObject.id.tointeger(),
                playerId,
                buffObject.scheme,
                buffObject.counter.tointeger(),
                buffObject.intervalsec.tointeger()
            );

            loadedBuff.enable();
            loadedBuff.packetAdd();
            buffs[playerId].push(loadedBuff);
        }
    }

    static function removePlayerBuff(playerId, schemeName) {
        if(!(schemeName in Config["Buffs"]))
            return;

        local sxm = Config["Buffs"][schemeName];
        local buffExist = false;

        foreach(_index, buff in buffs[playerId]) {
            if(buff.scheme == schemeName) {
                buff.disable();
                buff.packetRemove();
                buffs[playerId].remove(_index);
            }
        }
    }

    static function onSecond() {
        foreach(playerId, buffsPlayer in buffs) {
            if(buffsPlayer.len() == 0)
                continue;

            for(local i = (buffsPlayer.len() - 1); i >= 0; i--) {
                if(buffsPlayer[i].counter <= 0) {
                    buffsPlayer[i].disable();
                    buffsPlayer[i].packetRemove();
                    buffsPlayer.remove(i);
                }else {
                    buffsPlayer[i].onSecond();
                }
            }
        }
    }

    static function onPlayerDisconnect(playerId) {
        local rId = getPlayer(playerId).rId;
        if(rId == -1)
            return;

        Query().deleteFrom(Buff.BuffTable).where(["playerId = "+rId]).execute();

        if(!(playerId in buffs))
            return;

        if(buffs[playerId].len() > 0) {
            local arr = [];
            foreach(buff in buffs[playerId]) {
                buff.disable();
                arr.append([rId, "'"+buff.scheme+"'", buff.counter, buff.interval]);
            }

            Query().insertInto(Buff.BuffTable, ["playerId", "scheme", "counter", "intervalsec"], arr).execute();
        }

        buffs.rawdelete(playerId);
    }

    static function onPlayerLoad(playerId) {
        local buffsForPlayerInDatabase = Query().select().from(Buff.BuffTable).where(["playerId = "+getPlayer(playerId).rId]).all();

        buffs[playerId] <- [];

        foreach(buffObject in buffsForPlayerInDatabase)
        {
            if(buffObject.counter.tointeger() <= 0)
                continue;

            local loadedBuff = Buff(
                buffObject.id.tointeger(),
                playerId,
                buffObject.scheme,
                buffObject.counter.tointeger(),
                buffObject.intervalsec.tointeger()
            );

            loadedBuff.enable();
            loadedBuff.packetAdd();
            buffs[playerId].push(loadedBuff);
        }
    }
}

addEventHandler ("onPlayerLoad", BuffController.onPlayerLoad.bindenv(BuffController));
addEventHandler ("onSecond", BuffController.onSecond.bindenv(BuffController));

