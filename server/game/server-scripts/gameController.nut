
class GameController
{
    static nextGameId = -1;
    static games = {};

    static function createGame(type) {
        nextGameId = nextGameId + 1;

        local id = nextGameId;

        switch(type) {
            case GameType.Dice:
                DiceGame(id, type);
            break;
        }
    }

    static function onPlayerJoin(playerId) {
        foreach(game in games)
            game.sendStart(playerId);
    }

    static function onSecond() {
        foreach(game in games)
            game.onSecond();
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case GamePackets.Move:
                local id = packet.readInt32();
                if(!(id in games))
                    return;

                games[id].move(playerId, packet.readString());
            break;
            case GamePackets.End:
                local id = packet.readInt32();
                if(!(id in games))
                    return;

                games[id].end(playerId);
                games.rawdelete(id);
            break;
            case GamePackets.Message:
                local id = packet.readInt32();
                if(!(id in games))
                    return;

                games[id].receiveMessage(playerId, packet.readString());
            break;
        }
    }
}

addEventHandler("onPlayerJoin", GameController.onPlayerJoin.bindenv(GameController))
addEventHandler("onSecond", GameController.onSecond.bindenv(GameController))
RegisterPacketReceiver(Packets.Game, GameController.onPacket.bindenv(GameController));
