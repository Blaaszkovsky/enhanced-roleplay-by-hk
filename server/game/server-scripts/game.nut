
class Game
{
    id = -1
    statusId = -1
    type = null

    players = null
    events = null

    constructor(_id, _type) {
        this.id = _id;
        this.type = _type;
        this.statusId = GameStatus.Start;

        this.players = [];
        this.events = {};
    }

    function end(playerId) {
        onEnd(playerId);

        GamePacket(this).deleteGame();
    }
}

