
enum GamePackets
{
    Create,
    Move,
    End,
    Message,
    Delete,
}

enum GameStatus
{
    Start,
    InGame,
    Results,
}

enum GameType
{
    Dice,
    Snake,
}