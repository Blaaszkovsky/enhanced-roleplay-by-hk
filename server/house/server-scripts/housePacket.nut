class HousePacket
{
    object = -1;
    packet = null;

    constructor(houseObject)
    {
        object = houseObject;
        packet = ExtendedPacket(Packets.House);
    }

    function sendCreate(playerId = -1) {
        packet.writeUInt8(HousePackets.Create)
        packet.writeInt16(object.id);
        packet.writeInt16(object.owner);
        packet.writeString(object.world);
        packet.writeString(object.name);
        packet.writeFloat(object.minY);
        packet.writeFloat(object.maxY);
        packet.writeInt16(object.positions.len());

        foreach(pos in object.positions) {
            packet.writeFloat(pos.x);
            packet.writeFloat(pos.z);
        }

        packet.writeInt16(object.doors.len());

        foreach(door in object.doors) {
            packet.writeInt16(door.id);
            local state = getCurrentState();
            packet.writeString(door.visual);
            packet.writeFloat(state.position.x);
            packet.writeFloat(state.position.y);
            packet.writeFloat(state.position.z);
            packet.writeFloat(state.rotation.x);
            packet.writeFloat(state.rotation.y);
            packet.writeFloat(state.rotation.z);
        }

        if(playerId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(playerId);
    }

    function remove() {
        packet.writeUInt8(HousePackets.Remove)
        packet.writeInt16(object.id);
        packet.sendToAll(RELIABLE);
    }

    function owner() {
        packet.writeUInt8(HousePackets.Owner)
        packet.writeInt16(object.id);
        packet.writeInt16(object.owner);
        packet.sendToAll(RELIABLE);       
    }

    function addDoor() {
        packet.writeUInt8(HousePackets.AddDoor)
        packet.writeInt16(object.houseId);
        packet.writeInt16(object.id);
        packet.writeString(object.visual);
        local state = getCurrentState();
        packet.writeFloat(state.position.x);
        packet.writeFloat(state.position.y);
        packet.writeFloat(state.position.z);
        packet.writeFloat(state.rotation.x);
        packet.writeFloat(state.rotation.y);
        packet.writeFloat(state.rotation.z);
        packet.sendToAll(RELIABLE);       
    }

    function removeDoor() {
        packet.writeUInt8(HousePackets.RemoveDoor)
        packet.writeInt16(object.houseId);
        packet.writeInt16(object.id);
        packet.sendToAll(RELIABLE);       
    }
}