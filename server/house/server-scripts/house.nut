
class House
{
    static HouseTable = "house";
    static HouseDoorTable = "housedoor";

    id = -1

    owner = -1
    minY = -1
    maxY = -1
    world = ""
    name = ""

    doors = null
    positions = null
    area = null

    constructor(id)
    {
        this.id = id

        this.owner = -1
        this.minY = -1
        this.maxY = -1
        this.world = ""
        this.name = ""

        this.doors = []
        this.positions = []
        this.area = null
    }

    function setPosition(arrayWithPositions)
    {
        positions = arrayWithPositions

        area = Area({
            points = positions
            world = world
        })
    }

    function getPositionLabel()
    {
        local str = "";
        foreach(position in positions)
            str += position.x + " " + position.z + "&";

        if(str.len() > 0)
            str = str.slice(0, -1);
        
        return str;
    }

    function save()
    {
        Query().update(House.HouseTable, ["name", "position", "minY", "maxY", "world", "ownerId"], ["'"+name+"'", "'"+getPositionLabel()+"'", minY, maxY, "'"+world+"'", owner]).where(["id = "+id]).execute();
        Query().deleteFrom(House.HouseDoorTable).where(["houseId = "+id]).execute();

        if(doors.len() > 0)
        {
            local arr = [];
            foreach(item in doors)
                arr.append([id, item.position.x, item.position.y, item.position.z, item.rotation.x, item.rotation.y, item.rotation.z, item.targetPosition.x, item.targetPosition.y, item.targetPosition.z, item.targetRotation.x, item.targetRotation.y, item.targetRotation.z, item.keyId, "'"+item.visual+"'"]);

            Query().insertInto(House.HouseDoorTable, ["houseId", "positionX", "positionY", "positionZ", "rotationX", "rotationY", "rotationZ", "targetPositionX", "targetPositionY", "targetPositionZ", "targetRotationX", "targetRotationY", "targetRotationZ", "keyId", "visual"], arr).execute();
        }
    }
}

class HouseDoor
{
    id = -1

    houseId = -1
    keyId = -1
    visual = ""

    position = null
    rotation = null

    targetPosition = null
    targetRotation = null

    state = -1

    constructor(id, houseId)
    {
        this.id = id
        this.houseId = houseId
        this.keyId = -1
        this.visual = ""
    
        this.position = {x = 0, y = 0, z = 0}
        this.rotation = {x = 0, y = 0, z = 0}
    
        this.targetPosition = {x = 0, y = 0, z = 0}
        this.targetRotation = {x = 0, y = 0, z = 0}

        this.state = HouseDoorState.Closed;
    }

    function getCurrentState()
    {
        switch(state)
        {
            case HouseDoorState.Closed:
                return {
                    position = position,
                    rotation = rotation
                }
            break;
            case HouseDoorState.Opened:
                return {
                    position = targetPosition,
                    rotation = targetRotation
                }
            break;
        }
    }
}