
class HouseController
{
    static houses = {};

    static function onInit() {
        local housesInDatabase = Query().select().from(House.HouseTable).all();

        foreach(house in housesInDatabase)
        {
            local newHouse = House(house["id"].tointeger())

            newHouse.owner = house.ownerId.tointeger();
            newHouse.world = house.world;
            newHouse.name = house.name;
            newHouse.minY = house.minY.tofloat();
            newHouse.maxY = house.maxY.tofloat();

            if(house.positions.len() > 0)
            {
                house.positions = split(house.positions,"&");
                local positions = [];
                foreach(item in house.positions)
                {
                    local args = sscanf("ff", item);
                    positions.append({
                        x = args[0],
                        z = args[1],
                    })
                }
                newHouse.setPosition(positions);
            }

            local doors = Query().select().from(House.HouseDoorTable).where(["houseId = "+newHouse.id]).all();

            foreach(doorObj in doors)
            {
                local door = HouseDoor(doorObj.id.tointeger(), newHouse.id);
                door.position = {x = doorObj.positionX.tofloat(),y = doorObj.positionY.tofloat(),z = doorObj.positionZ.tofloat()}
                door.rotation = {x = doorObj.rotationX.tofloat(),y = doorObj.rotationY.tofloat(),z = doorObj.rotationZ.tofloat()}
                door.targetPosition = {x = doorObj.targetPositionX.tofloat(),y = doorObj.targetPositionY.tofloat(),z = doorObj.targetPositionZ.tofloat()}
                door.targetRotation = {x = doorObj.targetRotationX.tofloat(),y = doorObj.targetRotationY.tofloat(),z = doorObj.targetRotationZ.tofloat()}
                door.keyId = doorObj.keyId.tointeger();
                door.visual = doorObj.visual;

                newHouse.doors.append(door);
            }

            houses[newHouse.id] <- newHouse;
        }

        print("We have " + houses.len() + " registered houses.");
    }

    static function onPlayerJoin(playerId)
    {
        foreach(house in houses)
            HousePacket(house).sendCreate(playerId);
    }

    static function onPacket(playerId, packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case HousePackets.Create:
                local name = packet.readString();
                local owner = packet.readInt16();
                local world = getPlayerWorld(playerId);
                local minY = packet.readFloat();
                local maxY = packet.readFloat();
                local positions = packet.readString();

                Query().insertInto(House.HouseTable, ["name", "ownerId", "world", "minY", "maxY", "positions"], ["'"+name+"'", owner, "'"+world+"'", minY, maxY, "'"+positions+"'"]).execute();

                local house = Query().select().from(House.HouseTable).where(["name = '"+name+"'"]).orderBy("id DESC").one();
                if(house == null)
                    return;

                local newHouse = House(house["id"].tointeger())

                newHouse.owner = house.ownerId.tointeger();
                newHouse.world = house.world;
                newHouse.name = house.name;
                newHouse.minY = house.minY.tofloat();
                newHouse.maxY = house.maxY.tofloat();

                if(house.positions.len() > 0)
                {
                    house.positions = split(house.positions,"&");
                    local positions = [];
                    foreach(item in house.positions)
                    {
                        local args = sscanf("ff", item);
                        positions.append({
                            x = args[0],
                            z = args[1],
                        })
                    }
                    newHouse.setPosition(positions);
                }

                houses[newHouse.id] <- newHouse;
                HousePacket(newHouse).sendCreate();
            break;
            case HousePackets.Remove:
                local houseId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                Query().deleteFrom(House.HouseTable).where(["id = "+houseId]).execute();
                HousePacket(houses[houseId]).remove();
                houses.rawdelete(houseId);
            break;
            case HousePackets.Owner:
                local houseId = packet.readInt16();
                local ownerId = packet.readInt16();
                if(!(houseId in houses))
                    return;
                    
                houses[houseId].owner = ownerId;
                houses[houseId].save();
                HousePacket(houses[houseId]).owner();
            break;
            case HousePackets.AddDoor:
                local houseId = packet.readInt16();
                local doorId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                foreach(door in houses[houseId].doors)
                    if(door.id == doorId)
                        return;

                local door = HouseDoor(doorId, houseId);
                door.visual = packet.readString();
                door.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                door.rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                door.createObject();
                houses[houseId].doors.append(door);
                houses[houseId].save();
                HousePacket(door).addDoor();
            break;
            case HousePackets.RemoveDoor:
                local houseId = packet.readInt16();
                local doorId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                foreach(index, door in houses[houseId].doors) {
                    if(door.id == doorId) {
                        HousePacket(door).removeDoor();
                        houses[houseId].doors.remove(index);
                        houses[houseId].save();
                        return;
                    }
                }
            break;
        }
    }
}

RegisterPacketReceiver(Packets.House, HouseController.onPacket.bindenv(HouseController));
addEventHandler("onInit", HouseController.onInit.bindenv(HouseController));
addEventHandler("onPlayerJoin", HouseController.onPlayerJoin.bindenv(HouseController));