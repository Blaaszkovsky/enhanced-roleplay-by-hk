
class ForestController
{
    static forests = {};

    static function onTryToHarvestForest(playerId, id) {
        if(!(id != forests))
            return;

        local obj = forests[id];
        local scheme = obj.getScheme();

        if(obj.growth < scheme.time) {
            addPlayerNotification(playerId, "Drzewo nie uros�o.");
            return;
        }

        if(getPlayerStamina(playerId) < 25.0) {
            addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
            return;
        }

        foreach(tier, stamina in Config["PlayerSkillStaminaCost"][PlayerSkill.Lumberjack]) {
            if(getPlayerSkill(playerId, PlayerSkill.Lumberjack) == tier) {
                setPlayerStamina(playerId, getPlayerStamina(playerId) - stamina.cost);
            }
        }

        obj.growth = 1;
        local outcome = scheme.outcome;

        local possibilitiesArray = [];
        foreach(index, item in outcome) {
            if(getPlayerSkill(playerId, PlayerSkill.Lumberjack) == item.skill && item.chance != -1) {
                for(local i = 0; i < item.chance; i ++) {
                    possibilitiesArray.push({instance = item.instance, amount = item.amount});
                }
            }
        }

        local givedItem = false;
        if(possibilitiesArray.len() != 0) {
            local item = possibilitiesArray[irand(possibilitiesArray.len() - 1)];
            giveItem(playerId, item.instance, item.amount);
            if(item.amount == 0)
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Nie uda�o si� zci��.");
            else
                sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Uda�o si� uzyska� "+getItemName(item.instance)+" w ilo�ci "+item.amount+".");

            addPlayerLog(playerId, "Udalo sie uzyskac z drwalstwa "+item.instance+" w ilosci "+item.amount);
            givedItem = true;
        }

        if(givedItem == false) {
            foreach(item in outcome) {
                if(item.chance == -1) {
                    giveItem(playerId, item.instance, item.amount);
                    sendMessageToPlayer(playerId, 100, 150, 200, ChatType.IC+"Uda�o si� uzyska� "+getItemName(item.instance)+" w ilo�ci "+item.amount+".");
                    givedItem = true;
                    break;
                }
            }
        }

        foreach(_item in getPlayer(playerId).inventory.getItems()) {
            if(_item.instance == "ITMW_SDRP_1H_AXE_04") {
                if(_item.resistance > 0) {
                    _item.resistance = _item.resistance - 1;
                    _item.sendUpdate(playerId);
                    break;
                }
            }
        }

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Forest);
        packet.writeUInt8(ForestPackets.Update);
        packet.writeInt16(1);
        packet.writeInt32(obj.id);
        packet.writeInt16(obj.growth);
        packet.sendToAll(RELIABLE);
    }

    static function onInit() {
        local forestsInDatabase = Query().select().from(Forest.ForestTable).all();

        foreach(forestsObject in forestsInDatabase)
        {
            local newForest = Forest(forestsObject.id.tointeger())

            newForest.scheme = forestsObject.scheme;
            newForest.growth = forestsObject.growth.tointeger();
            newForest.rotation = {x = forestsObject.rotationX.tofloat(), y = forestsObject.rotationY.tofloat(), z = forestsObject.rotationZ.tofloat()}
            newForest.position = {x = forestsObject.x.tofloat(), y = forestsObject.y.tofloat(), z = forestsObject.z.tofloat()}
            newForest.world = forestsObject.world

            forests[newForest.id] <- newForest;
        }

        print("We have " + forests.len() + " growing forests.");
    }

    static function onPlayerJoin(playerId) {
        foreach(forest in forests)
            ForestPacket(forest).create(playerId);
    }

    static function onSecond() {
        local idsOfForests = [];

        foreach(forestId, forest in forests)
        {
            if(forest.onSecond())
                idsOfForests.push({id = forestId, growth = forest.growth});
        }

        if(idsOfForests.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Forest);
        packet.writeUInt8(ForestPackets.Update);
        packet.writeInt16(idsOfForests.len());
        foreach(obj in idsOfForests) {
            packet.writeInt32(obj.id);
            packet.writeInt16(obj.growth);
        }
        packet.sendToAll(RELIABLE);
    }

    function getNearForest(playerId) {
        foreach(forestId, forest in forests)
            if(getPositionDifference(pos, forest.position) < 300)
                return forestId;

        return null;
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case ForestPackets.Growth:
                onTryToHarvestForest(playerId, packet.readInt32());
            break;
            case ForestPackets.Create:
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();

                Query().insertInto(Forest.ForestTable, ["x", "y", "z", "rotationX", "rotationY", "rotationZ", "growth", "scheme", "world"], [
                    positionX, positionY, positionZ, rotationX, rotationY, rotationZ, Config["ForestSystem"][scheme].time, "'"+scheme+"'", "'" + getPlayerWorld(playerId)+"'"
                ]).execute();

                local forestsObject = Query().select().from(Forest.ForestTable).orderBy("id DESC").one();
                if(forestsObject.scheme != scheme)
                    return;

                local newForest = Forest(forestsObject.id.tointeger())

                newForest.scheme = forestsObject.scheme;
                newForest.growth = forestsObject.growth.tointeger();
                newForest.rotation = {x = forestsObject.rotationX.tofloat(), y = forestsObject.rotationY.tofloat(), z = forestsObject.rotationZ.tofloat()}
                newForest.position = {x = forestsObject.x.tofloat(), y = forestsObject.y.tofloat(), z = forestsObject.z.tofloat()}
                newForest.world = getPlayerWorld(playerId)

                forests[newForest.id] <- newForest;
                ForestPacket(newForest).create(-1);
            break;
            case ForestPackets.Delete:
                local forestId = packet.readInt16();
                if(!(forestId in forests))
                    return;

                ForestPacket(forests[forestId]).deleteForest();
                forests.rawdelete(forestId);
                Query().deleteFrom(Forest.ForestTable).where(["id = "+forestId]).execute();
            break;
        }
    }
}

addEventHandler("onInit", ForestController.onInit.bindenv(ForestController));
addEventHandler("onPlayerJoin", ForestController.onPlayerJoin.bindenv(ForestController))
addEventHandler("onSecond", ForestController.onSecond.bindenv(ForestController))
RegisterPacketReceiver(Packets.Forest, ForestController.onPacket.bindenv(ForestController));
