
class Forest
{
    id = -1
    position = null
    rotation = -1

    scheme = -1
    growth = -1

    draw = null
    observer = null
    vob = null

    world = ""

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        rotation = {
            x = 0,
            y = 0,
            z = 0
        }
        growth = 0

        observer = null
        draw = null
        vob = null

        world = ""
    }

    function addToWorld() {
        draw = WorldInterface.Draw3d(position.x,position.y,position.z);
        draw.addLine("Drzewo");
        draw.addLine("Wzrost: "+growth+"/"+getScheme().time);
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        draw.distance = 600;
        draw.setScale(0.6, 0.6);
        draw.visible = true;
        updateDrawText();

        vob = Vob(getScheme().vob[0][1]);
        vob.setPosition(position.x,position.y,position.z);
        vob.setRotation(rotation.x,rotation.y,rotation.z);
        vob.cdDynamic = true;
        vob.cdStatic = true;
        vob.addToWorld();
        vob.floor();
        update(growth);

        observer = InteractionTree.addDistanceObserver(position.x,position.y + 109.40,position.z, 500, "Zetnij drzewo.", function() {ForestController.interactWithForest(); });
    }

    function updateDrawText() {
        draw.setLineText(1, "Wzrost: "+growth+"/"+getScheme().time);
    }

    function update(_growth) {
        growth = _growth;

        local instance = null;
        foreach(vob in getScheme().vob) {
            if(_growth >= vob[0])
                instance = vob[1];
        }

        vob.floor();
        vob.visual = instance;
        updateDrawText();
    }

    function remove()
    {
        InteractionTree.removeDistanceObserver(observer);
        draw.remove();
    }

    function getScheme() {
        return Config["ForestSystem"][scheme];
    }

    static function onWorldChange(world)
    {
        foreach(forest in ForestController.forests)
        {
            if (forest.world != world)
            {
                forest.remove()
                forest.vob.removeFromWorld()
            }
        }
    }

    static function onWorldEnter(world)
    {
        foreach(forest in ForestController.forests)
        {
            if (forest.world == world)
                forest.addToWorld()
        }
    }
}

addEventHandler("onWorldEnter", Forest.onWorldEnter.bindenv(Forest));
addEventHandler("onWorldChange", Forest.onWorldChange.bindenv(Forest));