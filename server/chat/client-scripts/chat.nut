
Chat <- {

    activeContainer = -1,

    lines = []
    containers = {}

    input = null
    scroll = null

    active = false
    visible = false

    lineHeight = any(30),
    lineLength = 10,

    lastMessage = "",

    toggle = function () {
        visible ? show() : hide();
    }

    show = function () {
        visible = true
        scroll.show()

        foreach(line in lines)
            line.show()

        foreach(container in containers)
            container.showBar();

        containers[activeContainer].open();
    }

    hide = function () {
        if(scroll == null)
            return

        visible = false

        scroll.hide()

        foreach(line in lines)
            line.hide()

        foreach(container in containers)
            container.hideBar();
    }

    openInput = function() {
        input.show();
        active = true;
        _Camera.modeChangeEnabled = false;
        _Camera.movementEnabled = false;
        isGUIInputActive = true;

        disableControls(true);
        setCursorVisible(true);

        for(local i = 0; i<200; i++)
            disableKey(i, true);

        setFreeze(true);
    }

    closeInput = function() {
        input.hide();
        active = false;
        _Camera.modeChangeEnabled = true;
        _Camera.movementEnabled = true;
        isGUIInputActive = false;

        // If you try to open gui via command closeInput will start after showing cursor/freezing player it may cause problems with proper working of BaseGUI.show()
        if(ActiveGui == null) {
            disableControls(false);
            setCursorVisible(false);
            setFreeze(false);

            if(Hero.unconscious <= 0) {
                setCursorVisible(false);
                setFreeze(false);
            }

            for(local i = 0; i<200; i++)
                disableKey(i, false);

            disableKey(KEY_1, false);
            disableKey(KEY_2, false);
            disableKey(KEY_3, false);
            disableKey(KEY_4, false);
            disableKey(KEY_5, false);
            disableKey(KEY_6, false);
            disableKey(KEY_7, false);
            disableKey(KEY_8, false);
            disableKey(KEY_9, false);
            disableKey(KEY_0, false);
            disableKey(KEY_BACK, true)
            disableKey(KEY_L, true)
        }
    }

    addContainer = function (idContainer) {
        containers[idContainer] <- ChatContainer(idContainer);
    }

    removeContainer = function (idContainer) {
        containers.rawdelete(idContainer);
    }

    changeContainer = function (idContainer) {
        if(!(idContainer in containers))
            idContainer = ChatType.IC;

        containers[activeContainer].close();

        activeContainer = idContainer;
        containers[activeContainer].open();
    }

    switchContainer = function(idContainer) {
        containers[activeContainer].close();

        activeContainer = idContainer;
        containers[activeContainer].open();
    }

    addMessage = function(idContainer, r, g, b, text) {
        if(idContainer in containers)
            containers[idContainer].addMessage(text, r, g, b);
    }

    keyHandler = function(key) {
        input.keyHandler(key);


        switch (key)
        {
            case KEY_RETURN:
            local textChat = input.getText();

            if(textChat.len() == 0) {
                Chat.closeInput();
                chatInputClose();
                return;
            }

            textChat = chatInputGetText()

            local isCommand = textChat.slice(0, 1) == "/"

            if(!isCommand)
                textChat = Chat.activeContainer+textChat;
            else
                textChat = "/" + textChat.slice(1, textChat.len())

            local textArray = split(textChat,"\n")
            local formatted = ""

            foreach(text in textArray)
                formatted += text

            lastMessage = formatted.slice(isCommand ? 0 : 1, formatted.len());
            chatInputSetText(formatted);
            chatInputSend();

            chatInputClose();
            Chat.closeInput();
            break;

            case KEY_ESCAPE:
                chatInputSetText("");
                cancelEvent();
            case KEY_TILDE:
                chatInputClose();
                Chat.closeInput();
			break;

            case KEY_DOWN:
                Chat.scroll.move(1);

                Chat.containers[activeContainer].refresh();
			break;

            case KEY_UP:
                input.clear();
                local len = lastMessage.len();
                for(local i = 0; i < len; i ++) {
                    input.addLetter(lastMessage.slice(0 + i, i + 1));
                }

                local text = ""

                for(local i = 0; i < input.lines.len(); ++i)
                {
                    if (input.lines[i] != null && input.lines[i].text.len() != 0)
                    {
                        text += input.lines[i].text

                        if ((i + 1 in input.lines) && input.lines[i + 1].text.len() != 0)
                            text += "\n"
                    }
                }

                chatInputSetText(text)
                Chat.scroll.move(-1);
                Chat.containers[activeContainer].refresh();
            break;

            default:
                playGesticulation(heroId);
			break;
        }
    }
}

function chatInputIsOpen() {
    return Chat.active;
}