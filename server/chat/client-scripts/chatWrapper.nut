const REFORMAT_NAME = 1;
const REFORMAT_ME = 2;
const REFORMAT_DO = 3;

function chatTextFormat(text, r, g, b, containerId) {
    text = chatTextReformat(text, REFORMAT_NAME);

    local newText = [];
    local widthScreen = (8129 * Config["ChatScreen"])/100;

    local draw = Draw(0,0,"");
    draw.setScale(Config["ChatLineMultiplier"], Config["ChatLineMultiplier"]);

    local activeColor = {r = r, g = g, b = b};
    local registeredColors = [];

    local activeMeCurveFormat = false;
    local activeMeHashtagFormat = false;

    local methodAction = Config["ChatMethod"][ChatMethods.Action];
    local methodEnviroment = Config["ChatMethod"][ChatMethods.Enviroment];

    registeredColors.append({start = 0, r = activeColor.r, g = activeColor.g, b = activeColor.b});

    for(local i = 0; i < text.len(); i++) {
        local letter = String.charAt(i, text);

        if((draw.width - chatTextFormaters(draw.text) * Config["ChatLineMultiplier"]) > widthScreen) {
            newText.append({ text = draw.text, color = clone registeredColors });

            draw.text = "";

            registeredColors.clear();
            registeredColors.append({start = 0, r = activeColor.r, g = activeColor.g, b = activeColor.b});
        }

        draw.text += letter;

        if(containerId == ChatType.IC)
        {
            if(letter == "#")
            {
                if(activeMeHashtagFormat == false)
                    registeredColors.append({start = draw.text.len() - 1, r = methodAction.r, g = methodAction.g, b = methodAction.b});
                else
                    registeredColors.append({start = draw.text.len(), r = r, g = g, b = b});

                activeColor = registeredColors[registeredColors.len() - 1];
                activeMeHashtagFormat = !activeMeHashtagFormat;
            }else if(letter == "<")
            {
                registeredColors.append({start = draw.text.len() - 1, r = methodEnviroment.r, g = methodEnviroment.g, b = methodEnviroment.b});
                activeColor = registeredColors[registeredColors.len() - 1];
            }else if(letter == ">")
            {
                registeredColors.append({start = draw.text.len(), r = r, g = g, b = b});
                activeColor = registeredColors[registeredColors.len() - 1];
            }
        }
    }

    newText.append({ text = draw.text, color = registeredColors });

    return newText;
}

function chatTextFormaters(text)
{
    local tInt = 0;

    do {
        if(text.find("[COLOR") == null);
            return tInt * 50;

        tInt = tInt + 1;
        text = text.slice(text.find("[COLOR") + 6, 0);
    }while(true == true)
}

function chatTextReformat(text, type, r = -1, g = -1, b = -1)
{
    switch(type)
    {
        case REFORMAT_NAME:
            do {
                local cutPositions = {start = 0, end = 0};
                local findNameTag = text.find("[PLAYER:");
                if(findNameTag == null)
                    break;

                cutPositions.start = findNameTag;
                findNameTag = text.slice(findNameTag, text.len());

                local closingBracket = findNameTag.find("]");
                cutPositions.end = cutPositions.start + (closingBracket + 1);

                local cuttedPart = text.slice(cutPositions.start, cutPositions.end);
                cuttedPart = split(cuttedPart.slice(1, -1),":");
                cuttedPart = getPlayerRealName(cuttedPart[1].tointeger());

                text = format("%s%s%s",text.slice(0, cutPositions.start), cuttedPart, text.slice(cutPositions.end, text.len()));
            }while(true == true)
        break;

        case REFORMAT_ME:
            local method = Config["ChatMethod"][ChatMethods.Action];


        break;

        case REFORMAT_DO:
            local method = Config["ChatMethod"][ChatMethods.Enviroment];

            local finded = false;
            do {
                if(String.find(text, "#") == false)
                    break;

                if(finded)
                    text = String.replace(text, "#", "AOLT;[COLOR:"+format("%d,%d,%d", r, g, b)+"]")
                else
                    text = String.replace(text, "#", "[COLOR:"+format("%d,%d,%d", method.r, method.g, method.b)+"]AOLT;")

                finded = !finded;
            }while(true == true)

            text = String.replaceAll(text, "AOLT;", "#");
        break;
    }

    return text;
}
