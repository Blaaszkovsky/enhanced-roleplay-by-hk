Chat.init <- function() {
    for(local i = 0; i < Config["ChatDefaultLines"]; i++)
        lines.append(ChatLine(Config["ChatScreen"]))

    setKeyLayout(0);

    lineLength = Config["ChatDefaultLines"];

    scroll = ChatScroll(Config["ChatDefaultLines"]);
    input = ChatInput(Config["ChatScreen"]);

    addContainer(ChatType.IC);
    addContainer(ChatType.OOC);
    addContainer(ChatType.ALL);

    activeContainer = ChatType.IC;
}

addEventHandler("onInit", Chat.init.bindenv(Chat));

Chat.settingsChange <- function() {
    Config["ChatAnimation"] = getSetting("ChatAnimation");
    Config["ChatScreen"] = getSetting("ChatPercentOnScreen");
    Config["ChatDefaultLines"] = getSetting("ChatLines");
    Config["ChatLineMultiplier"] = 0.5 + getSetting("InterfaceScale") * 0.1;

    lineHeight = any(30 * Config["ChatLineMultiplier"])
    lineLength = Config["ChatDefaultLines"];
    lines.clear();

    for(local i = 0; i < Config["ChatDefaultLines"]; i++)
        lines.append(ChatLine(Config["ChatScreen"]))

    if(scroll != null)
    {
        scroll.setChatLines(lineLength);

        input.setWidthOfScreen(Config["ChatScreen"]);
        input.setWidthOfScreen(getSetting("ChatPercentOnScreen"));
    }

    if(visible)
        containers[activeContainer].refresh();
}

addEventHandler("onSettingsChange", Chat.settingsChange.bindenv(Chat));

Chat.onMessage <- function(pid, r, g, b, message) {
    local containerId = String.firstChar(message).tointeger();
    Chat.addMessage(containerId, r, g, b, message.slice(1));
}

addEventHandler("onPlayerMessage", Chat.onMessage.bindenv(Chat));

Chat.key <- function(key) {
    if(ActiveGui != null)
        return;

    if(Chat.active) {
        Chat.keyHandler(key);
        return;
    }

    switch(key)
    {
        case KEY_T:
            if (!isConsoleOpen() && Chat.visible)
            {
                Chat.openInput();
                chatInputOpen()
                local pos = input.lines[0].getPosition()
                chatInputSetPosition(-33, pos.y)
            }
        break;
        case KEY_PRIOR:
        case KEY_V:
            local m = Chat.activeContainer + 1;
            Chat.changeContainer(m);
        break;
    }
}

addEventHandler("onKey", Chat.key.bindenv(Chat));

local fps = getTickCount();

Chat.render <- function() {
    if(ActiveGui != null)
        return;

    if(fps > getTickCount())
        return;

    if(active)
        Chat.input.render();

    foreach(line in Chat.lines)
        line.render();

    fps = getTickCount() + 50;
}

addEventHandler("onRender", Chat.render.bindenv(Chat));

Chat.onPaste <- function(string) {
	if (!Chat.visible)
		return;

    if(ActiveGui != null)
        return;

    Chat.input.removeLastLetter();

    if(string.len() > 60)
        string = string.slice(0, 60);

    local len = string.len();
    for(local i = 0; i < len; i ++) {
        Chat.input.addLetter(string.slice(0 + i, i + 1));
    }
}

addEventHandler("onPaste", Chat.onPaste.bindenv(Chat));

Chat.mouseWheel <- function(direction) {
	if (!Chat.visible)
		return;

    if(ActiveGui != null)
        return;

    if(Chat.scroll == null)
        return;

    if(!Chat.scroll.visible)
        return;

    if(!Chat.active)
        return;

    Chat.scroll.move(direction);
    Chat.containers[activeContainer].refresh();
}

addEventHandler("onMouseWheel", Chat.mouseWheel.bindenv(Chat));
