

class ChatLine
{
    draws = -1
    widthScreen = 0
    counter = 0

    constructor(width) {
        draws = [];
        widthScreen = width;
        counter = 0;
    }

    function setVisible(value) {
        value ? show() : hide()
    }

    function clear() {
        foreach(draw in draws)
            draw.text = ""
    }

    function apply(index, bucketObject, isNew = false) {
        draws.clear();
        counter = 0;

        local leftPadding = anx(15);

        foreach(tInd, color in bucketObject.color)
        {
            local text = bucketObject.text;

            if((tInd + 1) < bucketObject.color.len())
                text = bucketObject.text.slice(color.start, bucketObject.color[tInd + 1].start)
            else
                text = bucketObject.text.slice(color.start);

            local draw = Draw(leftPadding, any(70) + index * Chat.lineHeight, text);
            draw.visible = Chat.visible;
            draw.setColor(color.r, color.g, color.b);
            draw.setScale(Config["ChatLineMultiplier"], Config["ChatLineMultiplier"]);
            draws.append(draw);

            leftPadding = leftPadding + draw.width;
        }

        if(isNew == true && Config["ChatAnimation"]) {
            counter = Config["ChatLineMultiplier"]/0.1;

            foreach(draw in draws)
                draw.setScale(Config["ChatLineMultiplier"],0.0);
        }
    }

    function show() {
        foreach(draw in draws)
            draw.visible = true
    }

    function hide() {
        foreach(draw in draws)
            draw.visible = false
    }

    function render() {
        if(counter == 0)
            return;
    
        counter = counter - 1;

        foreach(draw in draws)
        {
            local scale = draw.getScale();
            draw.setScale(Config["ChatLineMultiplier"], scale.height + 0.1);

            if(counter == 0)
                draw.setScale(Config["ChatLineMultiplier"], Config["ChatLineMultiplier"]);
        }
    }
}