
class ChatPlayer
{
    id = -1;

    expiration = null;

    blockChatOOC = false;
    blockChat = false;

    constructor(id) {
        this.id = id;

        this.expiration = null;

        this.blockChat = false;
        this.blockChatOOC = false;
    }

    function checkExpiration() {
        if(expiration < time())
        {
            blockChat = false;
            blockChatOOC = false;
        }
    }
}

addEventHandler("onPlayerLoad", function(id) {
    sendMessageToPlayer(id, 255, 255, 255, ChatType.IC + "Witam ponownie na serwerze!");
})