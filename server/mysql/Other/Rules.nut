
ORM.Rules <- { 
    "string" : function(value, instances) {
        return typeof(value) == "string";
    }  
    "int" : function(value, instances) {
        return typeof(value) == "int";
    }
    "min" : function(value, instances) {
        return value.len() >= instances[0];
    }
    "max" : function(value, instances) {
        return value.len() <= instances[0];
    }
    "boolean" : function(value, instances) {
        return typeof(value) == "bool";
    }
    "boolean" : function(value, instances) {
        return typeof(value) == "float";
    }
    "date" : function(value, instances) {
        return value.len() == 10;
    }
    "range" : function(value, instances) {
        return value in instances[0];
    }
    "require" : function(value, instances) {
        return value != null && value != "";
    }
}