
/**
 * Class to used for Migrations operations
 *
 * @type {Class}
 */
class Migration {

    static migrationTable = "migration";

    /**
    * Contains variables with names
    *
    * @type {String}
    */
    static nameMigration = UNKNOWN;

    /**
    * Contains functions to run down / up function
    *
    * @type {String}
    */
    static runUp = UNKNOWN;
    static runDown = UNKNOWN;

    constructor()
    {
        if (this.nameMigration == UNKNOWN) 
            throw "Wrong migration configuration";

        local allTab = DB.getDatabaseSchema();  
        local notFind = true;
        foreach(all in allTab){
            foreach(v, k in all){
                if(k == this.migrationTable){
                    notFind = false;
                }
            }
        }
        if(notFind) {
            local q = Query().createTable(this.migrationTable, [
                ["id int NOT NULL AUTO_INCREMENT PRIMARY KEY"],
                ["name varchar(255) NOT NULL"],
                ["data DATE"],
            ]).execute();
        }
    }

    function run() {
        
        if(this.runUp == UNKNOWN)
            throw "Not configure up.";
        
        local findQuery = Query().select().from(this.migrationTable).where(["name = '"+this.nameMigration+"'"]).one();
        if(findQuery)
            return;

        this.runUp();
        Query().insertInto(this.migrationTable, ["name", "data"], ["'"+this.nameMigration+"'", "'"+mDate.get().str+"'"]).execute();    
    }


    function down() {
        
        if(this.runDown == UNKNOWN)
            throw "Not configure up.";
        
        local findQuery = Query().select().from(this.migrationTable).where(["name = '"+this.nameMigration+"'"]).one();
        if(!findQuery)
            return;
            
        this.runDown();    
        Query().deleteFrom(this.migrationTable).where(["name = '"+this.nameMigration+"'"]).execute();
    }
}

/*
local mig = Migration();
mig.run();
mig.down(); 

// Installation
class TestMigration extends Migration
{
    // need to use this to create row in table 'migrations'
    static nameMigration = "PlayerMigration";

    // Create run up function base on builder
    function runUp() {
        Query().createTable("character",[
            ["id int NOT NULL AUTO_INCREMENT PRIMARY KEY"],
            ["name varchar(255) NOT NULL"],
            ["password varchar(255) NOT NULL"],
            ["liczba int"],
            ["data DATE"],
        ]).execute(); 
	}

    // Create run down function
	function runDown() {
		Query().dropTable("character").execute();
	}
}
//Use run up
TestMigration().run();

//Use run down 
TestMigration().down();
*/