class ORM.Field.String extends ORM.Field {
    static type = "varchar";
    static size = 256;
}