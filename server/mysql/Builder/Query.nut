
class Query {

    /**
    * Variable contains query itself as string
    *
    * @type {string}
    */
    query = null;

    /**
    * Variable storage is query contain select function
    *
    * @type {bool}
    */
    _useSelect = false;

    /**
    * Variable storage is query contain from function
    *
    * @type {bool}
    */
    _useFrom = false;

    /**
    * Storage handler for db connection
    *
    * @type {object}
    */
    dbHandler = null;

    constructor(_dbHandler = DB){
        query = "";
        //
        _useSelect = false;
        _useFrom = false;

        dbHandler = _dbHandler;
    }

    /**
    * Function create select instruction for query
    *
    * @type {string}
    */
	function select(command = "*") {
        //set variable to mark as used select
        _useSelect = true;

		query += SelectQuery.combine(command);
        return this;
	}

    /**
    * Function executing our query and get result in bool value
    *
    * @type {bool}
    */
    function execute() {
        if(_useSelect == false)
            throw "Query dosen't contain 'select or update or insert' function";
        if(_useFrom == false && _useSelect == true)
            throw "Query dosen't contain 'from' function";

        return dbHandler.query(query) ? true : false;
    }

    /**
    * Function return only one record we wanna find
    *
    * @type {array}
    */
    function one()
    {
        if(_useSelect == false)
            throw "Query dosen't contain 'select or update or insert' function";
        if(_useFrom == false && _useSelect == true)
            throw "Query dosen't contain 'from' function";

        local result = query += " LIMIT 1;";
        local record = dbHandler.queryGet(result);
        return record;
    }

    /**
    * Function return only one record we wanna find
    *
    * @type {array}
    */
    function all()
    {
        if(_useSelect == false)
            throw "Query dosen't contain 'select or update or insert' function";
        if(_useFrom == false && _useSelect == true)
            throw "Query dosen't contain 'from' function";

        local result = query += ";";
        local record = dbHandler.queryGetAll(result);
        return record;
    }

    /**
    * Function create order structure in sql format from array or string
    *
    * @type {array}
    */
    function orderBy(command)
    {
        query += OrderQuery.combine(command);
        return this;
    }

    /**
    * Function create delete query from scratch
    *
    * @type {array}
    */
    function deleteFrom(tab) {
        _useSelect = true;
        _useFrom = true;

        query = "DELETE FROM "+tab;
        return this;
    }

    /**
    * Function create insert query from scratch
    *
    * @type {array}
    */
    function insertInto(tab, tabOne, tabTwo) {
        _useSelect = true;
        _useFrom = true;

        query += InsertQuery.combine(tab, tabOne, tabTwo);
        return this;
    }

    /**
    * Function create update query from scratch
    *
    * @type {array}
    */
    function update(tab, tabOne, tabTwo) {
        _useSelect = true;
        _useFrom = true;

        if(tabOne.len() != tabTwo.len())
            throw "Query dosen't contain proper created both tab."

        query += UpdateQuery.combine(tab, tabOne, tabTwo);
        return this;
    }

    /**
    * Function create create table statement
    *
    * @type {array}
    */
    function createTable(tableName, fields) {
        _useSelect = true;
        _useFrom = true;

        query = CreateTableQuery.combine(tableName, fields);
        return this;
    }

    /**
    * Function drop table
    *
    * @type {array}
    */
    function dropTable(tableName) {
         _useSelect = true;
        _useFrom = true;

        query = "DROP TABLE "+tableName;
        return this;
    }

    /**
    * Function create primary key
    *
    * @type {array}
    */
    function primaryKey(tableName, fieldName) {
        _useSelect = true;
        _useFrom = true;

        query = PrimaryKeyQuery.combine(tableName, fieldName);
        return this;
    }

    /**
    * Function create where in sql format from array or string
    *
    * @type {array}
    */
    function where(command) {
		query += WhereQuery.combine(command);
        return this;
    }


    /**
    * Function create 'from' in sql format from array or string
    *
    * @type {array}
    */
    function from(command) {
        //set variable to mark as used from
        _useFrom = true;
		query += FromQuery.combine(command);
        return this;
    }


    /**
    * Function returns query in string format
    *
    * @type {string}
    */
    function getSQL() {
        return query;
    }
}


/*
local query = Query();
query.getSQL @return {string};
query.execute @return {bool};
query.all @return {array};
query.one @return {array};
query.where @return {obj};
query.from @return {obj};
query.update @return {obj};
query.orderBy @return {obj};
query.createTable @return {obj};
query.insertInto @return {obj};
query.deleteFrom @return {obj};
query.primaryKey @return {obj};
query.dropTable @return {obj};

Query().select().from("player").where(["id = 1", "name = 'Quarchodron'"]).one();
Query().deleteFrom("player").where(["name = 'Jacek'"]).execute();
Query().insertInto("player", ["name", "password"], ["'Jacek'", "'Warnenczyk'"]).execute();
Query().update("player", ["name", "password"], ["'Jacek'","'Warnpozmianie'"]).where(["name = 'Jacek'"]).execute();
*/