class FromQuery {
    static function combine(command) {
        if(typeof command == "array")
            return combineArray(command);

        local query = " FROM "+command;
        return query;
    }

    static function combineArray(command)
    {
        local query = " FROM ";
        foreach(arr in command)
            query += arr+",";

        query = query.slice(0, -1);
        return query; 
    }
}