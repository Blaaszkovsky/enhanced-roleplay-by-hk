class PrimaryKeyQuery {
    static function combine(tableName, fieldName) {
        local query = "ALTER TABLE "+tableName+" ADD PRIMARY KEY ("+fieldName+")"
        return query;
    }
}