class OrderQuery {
    static function combine(command) {
        if(typeof command == "array")
            return combineArray(command);

        local query = " ORDER BY "+command;
        return query;
    }

    static function combineArray(command)
    {
        local query = " ORDER BY ";
        foreach(arr in command)
            query += arr+",";

        query = query.slice(0, -1);
        return query; 
    }
}