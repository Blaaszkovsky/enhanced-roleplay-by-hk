
class Connector {

	hostname = "";
	database = "";
	user = "";
	password = "";
	port = -1;

	handler = null;
	retry = null;

	constructor(_hostname, _user, _password, _database, _port)
	{
		hostname = _hostname;
		database = _database;
		user = _user;
		password = _password;
		port = _port;

		retry = null;
		handler = mysql_connect(_hostname, _user, _password, _database, _port);

		if (!handler)
			throw "MySQL doesn't get connection. Server closed.";

		mysql_option_reconnect(true);
	}

	function check() {
		if(handler == null) {
			retryConnection();
			throw "MySQL doesn't get connection. ";
		}

		else if(mysql_ping(handler) == false) {
			retryConnection();
			throw "MySQL doesn't get connection. ";
		}
	}

	function getDatabaseSchema()
	{
		check();
		local query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '"+database+"'";
		return queryGetAll(query);
	}

	function query(command) {
		// print(command);
		check();
		local mysql_result = mysql_query(handler, command);
		if(mysql_result == false)
		{
			print(" - SQL-ERROR : " + mysql_errno(handler));
			print(" - SQL-ERROR : " + mysql_error(handler));
			local myfile = io.file("errors-database.txt", "a");
			myfile.write(time()+"\n");
			myfile.write(command+"\n");
			myfile.close();
			return false;
		}
		return true;
	}

	function queryGet(command)
	{
		//print(command);
		check();
		local result;
		local mysql_result = mysql_query(handler, command);

		if(mysql_result == false)
		{
			print(" - SQL-ERROR : " + mysql_errno(handler));
			print(" - SQL-ERROR : " + mysql_error(handler));
			local myfile = io.file("errors-database.txt", "a");
			myfile.write(time()+"\n");
			myfile.write(command+"\n");
			myfile.close();
			return [];
		}

		if(mysql_result == null)
			return [];

		if(mysql_num_rows(mysql_result) > 0)
		{
			result = mysql_fetch_assoc(mysql_result);
		}

		mysql_free_result(mysql_result);
		return result;
	}

	function queryGetAll(command)
	{
		//print(command);
		check();
		local result = [];
		local mysql_result = mysql_query(handler, command);

		if(mysql_result == false)
		{
			print(" - SQL-ERROR : " + mysql_errno(handler));
			print(" - SQL-ERROR : " + mysql_error(handler));
		}

		if(mysql_result == null)
			return [];

		local count = mysql_num_rows(mysql_result);
		for(local i=0; i<count; i++)
		{
			result.push(mysql_fetch_assoc(mysql_result));
		}

		mysql_free_result(mysql_result);

		return result;
	}

	function retryConnection() {
		// retry operation
		if(retry == null) {
			retry = RetryConnector(this);

			if(handler)
				if(mysql_ping(handler) == true)
					retry = null
		}else
			retry.run();
	}
}

class RetryConnector
{
	// Connector class object
	parent = null

	counter = -1

	constructor(parent)
	{
		this.parent = parent;
		this.counter = 0

		run();
	}

	function run() {
		counter = counter + 1;

		if(counter == 5)
		{
			for(local i = 0; i < getMaxSlots(); i ++)
				kick(i, "Server error.");

			print("Server closed the connection. NO MYSQL CONNECTION.")
			exit();
			return;
		}

		parent.handler = mysql_connect(parent.hostname, parent.user, parent.password, parent.database, parent.port)
	}
}

// Active connection used in ORM
DB <- Connector("localhost", "user", "somePassword", "someDatabase", 3306);


/*
local db = Connector(host, user, pass, db, port);
db.queryGetAll @return {array}
db.queryGet @return {array}
db.getDatabaseSchema @return {array}
db.query @return {bool}
db.check @return {bool}

example
DB <- Connector("localhost", "root", "", "gothic", 3306);
DB.query("CREATE TABLE `test` (person int(11));");
*/