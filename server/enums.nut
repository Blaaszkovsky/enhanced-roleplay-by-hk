/*************************************************************************************************************************
Events
**************************************************************************************************************************/

addEvent("onAccountRegister");
addEvent("onAccountLoad");
addEvent("onAccountReceiveSettingData");

addEvent("onNpcSpawn");

addEvent("onPlayerLoad");
addEvent("onPlayerUnLoad");
addEvent("onPlayerRegister");
addEvent("onChangeEquippedItems");
addEvent("onPlayerChangeInventory");
addEvent("onPlayerChangeCell");
addEvent("onPlayerGetEffect");
addEvent("onPlayerChangeAnim");
addEvent("onPlayerLostEffect");
addEvent("onPlayerSpellSummon");
addEvent("onPlayerLeftOnGround");
addEvent("onPlayerTakeFromGround");

addEvent("onCreateCraft");
addEvent("onRemoveCraft");

addEvent("onInteractNpc");
addEvent("onInteractPlayer");
addEvent("onInteractMob");
addEvent("onInteractArea");
addEvent("onInteractLostNpc");
addEvent("onInteractLostPlayer");
addEvent("onInteractLostMob");
addEvent("onInteractLostArea");

addEvent("onOpenGUI");
addEvent("onCloseGUI");
addEvent("onForceCloseGUI");

addEvent("onFractionCreate");
addEvent("onFractionRemove");
addEvent("onFractionAddMember");
addEvent("onFractionRemoveMember");
addEvent("onFractionTerritories");
addEvent("onFractionMembers");
addEvent("onFractionPermissions");

addEvent("onSettingsChange");

addEvent("onForestInteract");
addEvent("onForestAdd");
addEvent("onForestRemove");

addEvent("onFishingAdd");
addEvent("onFishingRemove");

addEvent("onMineAdd");
addEvent("onMineRemove");

addEvent("onVobInteract");
addEvent("onVobLostInteract");
addEvent("onVobAdd");
addEvent("onVobRemove");
addEvent("onVobCreate");
addEvent("onVobDestroy");

addEvent("onHouseAdd");
addEvent("onHouseRemove");
addEvent("onHouseDoorInteract");

addEvent("onBoardNotificationAdd");
addEvent("onBoardNotificationRemove");

addEvent("onSecond");
addEvent("onMinute");
addEvent("onTenSeconds");
addEvent("onFiveSeconds");


/*************************************************************************************************************************
Craft
**************************************************************************************************************************/

enum CraftPackets
{
    Use,
    Send,
    Call,
    Remove,
    Update,
    AddItems,
    RemoveItems,
    AddReward,
    RemoveReward,
}

enum CraftCaller
{
    Barbq,
    BarbqFraction,
    Anvil,
    AnvilFraction,
    Mill,
    Mill2,
    MillFraction,
    Mill2Fraction,
    Alchemy,
    AlchemyFraction,
    RuneTable,
    RuneTableFraction,
    Cooker,
    CookerFraction,
    Cooker2,
    Cooker2Fraction,
    Armourer,
    ArmourerFraction,
    Pot,
    PotFraction,
    Bake,
    BakeFraction,
    Tanning,
    TanningFraction,
    Sewing,
    SewingFraction,
    ArrowTable,
    ArrowTableFraction,
    BowTable,
    BowTableFraction,
    Writing,
    WritingFraction,
    Cutting,
    CuttingFraction,
    RuneMaker,
    RuneMakerFraction,
    BlastFurnace,
    BlastFurnaceFraction,
    Furnace,
    FurnaceFraction,
    Herbal,
    HerbalFraction,
    Sworder,
    SworderFraction,
    Juicer,
    JuicerFraction,
    Sugarer,
    SugarerFraction,
    Slaught,
    SlaughtFraction,
    Alchemy2,
    Alchemy2Fraction,
    FSlaughter,
    FSlaughterFraction,
    Carpentry,
    CarpentryFraction,
    Sharper,
    SharperFraction,
    MedLab,
    MedLabFraction,
    WaterBucket,
    WaterBucketFraction,
    Fischsneiden,
    FischsneidenFraction,
    NewCampfire,
    NewCampfireFraction,
    Baumsaege,
    BaumsaegeFraction,
    Stomper,
    StomperFraction,
	Jeweler,
	JewelerFraction,
}


/*************************************************************************************************************************
Vobs
**************************************************************************************************************************/

enum VobType
{
    Static,
    StaticName,
    Interaction,
    Container,
    Door,
    Movable,
    Switch,
}

enum VobHealth
{
    Static,
    Vulnerable
}

enum VobMovable
{
    Static,
    Active,
}

enum VobPackets
{
    Create,
    Rebuild,
    Remove,
    Position,
    Rotation,
    Movable,
    Health,
    Operation,
    DoorInteraction,
    Complexity,
    NewGuid,
    ChangeMovable
}

enum VobTriggerType
{
    Self,
    Vob
}

enum VobTriggerAnimation
{
    Door,
    None,
}

/*******************************f******************************************************************************************
Interactions
**************************************************************************************************************************/

enum InteractionVob
{
    Barbq,
    Anvil,
    Alchemy,
    RuneTable,
    Cooker,
    Cooker2,
    Mill,
    Mill2,
    Armourer,
    Sit,
    Bed,
    Fishbar,
    NewCampfire,
    Pot,
    Bake,
    Tanning,
    Sewing,
    ArrowTable,
    BowTable,
    Writing,
    Cutting,
    RuneMaker,
    BlastFurnace,
    Furnace,
    Herbal,
    Sworder,
    Juicer,
    Sugarer,
    Slaught,
    Alchemy2,
    FSlaughter,
    Carpentry,
    Sharper,
    MedLab,
    WaterBucket,
    Fischsneiden,
    Baumsaege,
    Stomper,
	Jeweler
}

/*************************************************************************************************************************
Items
**************************************************************************************************************************/

const ITEMCAT_ARMOR = 1;
const ITEMCAT_ONEH = 2;
const ITEMCAT_TWOH = 3;
const ITEMCAT_BOW = 4;
const ITEMCAT_CBOW = 5;
const ITEMCAT_RUNE = 6;
const ITEMCAT_SCROLL = 7;
const ITEMCAT_POTION = 8;
const ITEMCAT_FOOD = 9;
const ITEMCAT_HERB = 10;
const ITEMCAT_RING = 11;
const ITEMCAT_BELT = 12;
const ITEMCAT_BOOK = 13;
const ITEMCAT_SHIELD = 14;
const ITEMCAT_TORCH = 15;
const ITEMCAT_BAG = 16;
const ITEMCAT_DRAGGABLE = 17;
const ITEMCAT_OTHER = 18;
const ITEMCAT_MASK = 19;
const ITEMCAT_HELMET = 20;
const ITEMCAT_USABLE = 21;

const DAMAGE_BLUNT = 0;
const DAMAGE_POINT = 1;
const DAMAGE_EDGE = 2;
const DAMAGE_MAGIC = 3;
const DAMAGE_FIRE = 4;
const DAMAGE_FLY = 5;
const DAMAGE_FALL = 6;
const DAMAGE_BARRIER = 7;
const DAMAGE_UNKNOWN = 8;


/*************************************************************************************************************************
Chat
**************************************************************************************************************************/

enum ChatType
{
    IC,
    OOC,
    ALL,
    REPORT,
    ADMIN
}

enum ChatMethods
{
    Normal,
    OutOfCharacter // ooc
    Global, // g
    Whisper, // sz
    Shout, // k
    Enviroment, // do
    Action, // me
    Mumble, // c
    LoudTalk, //g
}


/*************************************************************************************************************************
Board enums / const
**************************************************************************************************************************/

enum BoardPackets
{
    Create,
    Delete,
    List,
}

/*************************************************************************************************************************
Buff enums / const
**************************************************************************************************************************/

enum BuffPackets
{
    Add,
    Remove,
}

enum BuffType
{
    None,
    Custom,
    Attribute,
    Skill,
    HealthReduce,
    HealthRegeneration,
    ManaReduce,
    ManaRegeneration,
    EnduranceRegeneration,
    StaminaRegeneration,
    WeightLimit,
    DamageBonus,
    DamageReduce,
    ProtectionBonus,
    ProtectionReduce,
    Mds,
}

/*************************************************************************************************************************
Building enums / const
**************************************************************************************************************************/

enum BuildingPackets
{
    Create,
    Delete,
    Build,
    Hit,
}

/*************************************************************************************************************************
Draw enums / const
**************************************************************************************************************************/

enum DrawPackets
{
    Create,
    Delete,
}

/*************************************************************************************************************************
Forest enums / const
**************************************************************************************************************************/

enum ForestPackets
{
    Create,
    Update,
    Growth,
    Delete,
}

/*************************************************************************************************************************
Mine enums / const
**************************************************************************************************************************/

enum MinePackets
{
    Create,
    Update,
    Slots,
    Delete,
}

/*************************************************************************************************************************
Fishing enums / const
**************************************************************************************************************************/

enum FishingPackets
{
    Create,
    Update,
    Slots,
    Delete,
}

/*************************************************************************************************************************
Herb enums / const
**************************************************************************************************************************/

enum HerbPackets
{
    Create,
    Update,
    Stamina,
    Growth,
    Delete,
}

/*************************************************************************************************************************
Admin enums / const
**************************************************************************************************************************/

enum AdminPackets
{
    Role,
    ExitGame,
    PlayerRegister,
    PlayerUnRegister,
    GetFractionMembers,
    AddFractionMember,
    RemoveFractionMember,
    UpdateFractionMember,
    CreateBot,
    UpdateBot,
    RemoveBot,
    GetCrafts,
    GetCraft,
    GetPlayers,
    UpdatePlayer,
    KickPlayer,
    BanPlayer,
    TeleportPlayer,
    GetBots,
    ItemRegister,
    Inivisibility,
    GetAssets,
    AddAsset,
    DeleteAsset,
    Question,
    GetItems,
    UpdateItem,
    DeleteItem,
    ConfirmRole,
    SaveToLog
}


/*************************************************************************************************************************
Trade enums / const
**************************************************************************************************************************/

enum TradePackets
{
    Create,
    Accept,
    AddItem,
    RemoveItem,
    End,
}

/*************************************************************************************************************************
Fraction enums / const
**************************************************************************************************************************/

enum FractionPackets
{
    Send,
    Remove,
    MemberReset,
    MemberAdd,
    MemberRemove,
    SendTerritories,
    SendMembers,
    SendPermissions,
    InviteMember,
    RemoveMember,
    UpdateMember,
    SetForPlayer
}

enum FractionPermissionType
{
    Invite,
    Leadership,
    Crafting,
    Working,
    Trade,
    Faction,
}

/*************************************************************************************************************************
Player enums / const
**************************************************************************************************************************/

enum AccountPackets
{
    Register,
    Accept,
    LoggIn,
    CodeConfirmation,
    PasswordRequest,
    PasswordConfirm,
    PasswordReroll,
    AcceptPolicy,
    WhiteList,
    Logout,
    Skins,
    Kick,
    Setting,
    SettingKey,
}

enum PlayerRole {
    User,
    Patreon,
    Builder,
    Narrator,
    Helper,
    Support,
    SupportPlus,
    GameMaster,
    GameMasterPlus,
    Mod,
    Admin
}

enum PlayerAskType
{
    Trade,
    Trap,
    UnTrap,
    Invite,
    Heal,
    Search,
    Take
}

enum PlayerStatus
{
    Active,
    Killed,
    Deleted,
}

enum PlayerMuteOption
{
    IC,
    OOC,
    ALL,
}

enum PlayerPackets
{
    //Character
    CharacterAccept,
    CharacterList,
    CharacterAdd,
    CharacterPreview,

    DuplicatingKey,
    UseSprint,
    SpellSummon,
    FractionManage,
    ClearInventory,
    Team,
    UseItem,
    OneTimeEffect,
    ThrowItem,
    QuoteItem,
    Notification,
    PositionRearange,
    Lute,
    Flute,
    Okarina,
    Conga,	
    Harp,	
    Following,
    StopFollowing,
    Ask,
    RejectAsk,
    AcceptAsk,
    RandomRespawn,
    ApplyAnimation,
    RemoveAnimation,
    RemoveAllAnimation,
    Recovery,
    StealFromUser,
    StartSearch,
    RunSearch,
    EndSearch,

    //Statistics
    NextLevelExperience,
    Experience,
    Skill,
    Inteligence,
    LearnPoints,
    Scale,
    Level,
    Unconscious,
    Carried,
    Carrying,
    WalkingStyle,
    Visual,
    Speech,
    Fatness,
    Endurance,
    Stamina,
    PersonalCard,
    RealName,
    PersonalNotes,
    Inventory,
    Upgrade,
    SkillUpgrade,
    Fraction,
    FractionInfo,
    FractionSetup,
    FractionRemove,
    GodMode,

    //Methods
    PlayerDescription,
    GetDescription,
    Hunger,
    Invisible,
    Masked,
    Print,
    AnimationBind,
    AnimationBindDelete,
}

enum PlayerFatness
{
    Thin,
    Skinny,
    Normal,
    Muscular,
    Thick,
    Fat,
}

enum PlayerSkill
{
    Blacksmith,
    Armourer,
    Alchemy,
    Cook,
    Fisherman,
    Miner,
    Lumberjack,
    Herbalist,
//    Farmer,
	Jeweler,
    Hunter,
    Butcher,
    Carpenter,
    Fletcher,
    Medic,
    Tailor,
    Thief,
    Runemaker,
}

enum PlayerScenario
{
    Castaway,
    Prisoner,
    Traveler,
    Amnesion
}

enum PlayerSpeech
{
    Midget,
    Dwarf,
    Small,
    Normal,
    Tall,
    Giant,
}

enum PlayerWalk
{
    Normal,
    Tired,
    Woman,
    Army,
    Lazy,
    Arrogant,
    Mage,
    Pockets,
    OldTired,
    OldArrogant,
    OldMage,
    OldLazy,
    HurtBen,
    Leader,
    Seller,
    Stomachhurt,
    Waitress,
    Crate,
    Poisoned,
    Hurt,
    PocketWalk,
}

enum PlayerGUI
{
    LoggIn,
    Register,
    Ask,
    Code,
    Password,
    VobOpener,
    PasswordRequest,
    PasswordReroll,
    Lobby,
    Building,
    BuildingBuilder,
    Free,
    InteractionPlayer,
    CharacterList,
    CharacterAdd,
    PlayerList,
    Fraction,
    Duplicating,
    MenuGame,
    KeySelector,
    HelpMenu,
    Statistics,
    Craft,
    CraftList,
    Settings,
    BotDialog,
    VobInteraction,
    Calendar,
    Trade,
    Board,
    BoardCreate,
    BoardDetails,
    Animation,
    Map,
    Visual,
    Notes,
    Inventory,
    Steal,
    Questions,
    Search,
    Anim,
    Net,
    TeamPick,
    Herb,
    Forest,
    ThiefVob,
    AnimationList,
    Fishing,
    ActionDraws,
    HerbCreate,
    Lute,
    Flute,
    Okarina,
    Conga,	
    Harp,	
    Description,
    ServerBuilder,
    ServerBuilderDraws,
    ServerBuilderVobs,
    ServerBuilderHouse,
    ServerBuilderPlayer,
    ServerBuilderWork,
    ServerBuilderAssets,
    ServerBuilderBots,
    ServerBuilderItems,
    ServerBuilderFishings,
    ServerBuilderForests,
    ServerBuilderMines,
    ServerBuilderFractions,
}

enum PlayerGUIFraction
{
    None,
    Permissions,
    FractionInfo,
    MemberList,
    MemberPermission,
    Invite,
    Trade,
}

enum PlayerAttributes
{
    Str,
    Dex,
    Int,
    Hp,
    Mana,
    OneH,
    TwoH,
    Bow,
    Cbow,
    MagicLvl,
    Stamina,
    LearnPoints,
    Hunger
}

/*************************************************************************************************************************
Note enums / const
**************************************************************************************************************************/

enum NotePackets
{
    Get,
    Create,
    Drop,
    Destroy,
}

enum NoteStatus
{
    Equipment,
    Ground,
}

/*************************************************************************************************************************
ActionDraw enums / const
**************************************************************************************************************************/

enum ActionDrawPackets
{
    Create,
    Delete,
    Activate,
}

/*************************************************************************************************************************
Packets
**************************************************************************************************************************/

enum PacketDataType
{
    Int8,
    Int16,
    Int32,
    Float
    String,
    Bool,
    Array,
    ArrayIteration,
    ArrayEnd,
    Table,
    TableIteration,
    TableEnd,
    Object,
    ObjectIteration,
    ObjectEnd,
    Error,
    Null,
}

enum Packets
{
    Account,
    Player,
    Admin,
    Fraction,
    Craft,
    Work,
    Draw,
    Vob,
    House,
    Board,
    Herb,
    Forest,
    Mine,
    Note,
    Fishing,
    Trade,
    ActionDraw,
    AntyCheat,
    Game,
    Buff,
    Building,
}

enum PacketInteractions
{
    Mob
}

enum SettingsFormType
{
    Checkbox,
    Input,
    Select,
    Range
}

const PacketAccount = 27;
const PacketReceivers = 9;
const PacketBots = 8;
const PacketInteraction = 10;