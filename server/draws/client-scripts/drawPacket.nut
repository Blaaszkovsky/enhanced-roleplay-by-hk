
class DrawPacket
{
    packet = null;

    constructor()
    {
        this.packet = ExtendedPacket(Packets.Draw);
    }

    function create(id, distance, r, g, b, x, y, z, world, text) {
        packet.writeUInt8(DrawPackets.Create)
        packet.writeInt16(id);
        packet.writeInt16(distance);
        packet.writeInt16(r);
        packet.writeInt16(g);
        packet.writeInt16(b);
        packet.writeFloat(x);
        packet.writeFloat(y);
        packet.writeFloat(z);
        packet.writeString(world);
        packet.writeInt16(text.len());

        foreach(_text in text)
            packet.writeString(_text);

        packet.send();
    }

    function remove(drawId) {
        packet.writeUInt8(DrawPackets.Delete)
        packet.writeInt16(drawId);

        packet.send();
    }
}