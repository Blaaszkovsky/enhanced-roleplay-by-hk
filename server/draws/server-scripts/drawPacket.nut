
class DrawPacket
{
    draw = null;
    packet = null;

    constructor(draw)
    {
        this.draw = draw;
        this.packet = ExtendedPacket(Packets.Draw);
    }

    function create(playerId) {
        packet.writeUInt8(DrawPackets.Create)
        packet.writeInt16(draw.id);
        packet.writeInt16(draw.distance);
        packet.writeInt16(draw.color.r);
        packet.writeInt16(draw.color.g);
        packet.writeInt16(draw.color.b);
        packet.writeFloat(draw.position.x);
        packet.writeFloat(draw.position.y);
        packet.writeFloat(draw.position.z);
        packet.writeString(draw.world);
        packet.writeInt16(draw.text.len());

        foreach(_text in draw.text)
            packet.writeString(_text);

        packet.send(playerId);
    }

    function remove(playerId) {
        packet.writeUInt8(DrawPackets.Delete)
        packet.writeInt16(draw.id);

        packet.send(playerId);
    }
}