
class Craft
{
    static CraftTable = "craft";
    static CraftItemsTable = "craftitems";
    static CraftRewardsTable = "craftreward";

    id = -1

    name = ""
    items = null
    rewards = null

    createdAt = ""
    createdBy = ""

    animation = ""
    caller = -1

    staminaCost = -1
    skillRequired = -1
    skillValue = -1
    fractionIds = -1

    constructor(craftId)
    {
        id = craftId

        name = ""

        items = {}
        rewards = {}

        createdAt = ""
        createdBy = ""

        animation = ""
        caller = -1

        staminaCost = -1
        skillRequired = []
        skillValue = []
        fractionIds = []
    }

    function getVisual() {
        foreach(v,_ in rewards)
            return v;
    }

    function isVob() {
        local name = getVisual();
        if(name.find("ASC") != null || name.find("MDS") != null || name.find("3DS") != null)
            return true;

        return false;
    }

    function use(playerId)
    {
        local pObj = getPlayer(playerId);

        if(pObj.stamina < staminaCost) {
            CraftPacket().use(playerId, "Brak wytrzyma�o�ci.");
            return false;
        }

        local lenghtOfSkills = skillRequired.len();
        for(local i = 0; i < lenghtOfSkills; i++) {
            if(getPlayerSkill(playerId, skillRequired[i]) < skillValue[i]) {
                CraftPacket().use(playerId, "Brak umiej�tno�ci.");
                return false;
            }
        }

        foreach(instance, amount in items) {

            instance = instance.toupper();

            if  (getItemScheme(instance) == null)
            {
                CraftPacket().use(playerId, "Tymczasowo niedost�pne");
                return false
            }

            if(hasPlayerItem(playerId, instance) < amount) {
                CraftPacket().use(playerId, "Brak "+instance+" w ilo�ci "+amount);
                return false;
            }
        }

        if (!isVob())
        {
            foreach(instance, amount in rewards)
            {
                instance = instance.toupper();

                if  (getItemScheme(instance) == null)
                {
                    CraftPacket().use(playerId, "Tymczasowo niedost�pne");
                    return false
                }
            }
        }

        setPlayerStamina(playerId, getPlayerStamina(playerId) - staminaCost);

        foreach(instance, amount in items)
            removeItem(playerId, instance.toupper(), amount);

        if(isVob()) {
            local obj = {
                lockable = false,
                combination = "RLRL",
                destroyable = false,
                health = 10,
                canbechest = false,
                canBeInteractedWith = false,
                weight = 40.50
            }

            if(getVisual() in Config["CraftVobRelation"])
                obj = Config["CraftVobRelation"][getVisual()];

            local guid = "";
            if(obj.lockable)
                guid = "KLUCZ_"+String.makeid(12);

            local hpMode = 0;
            if(obj.destroyable == true)
                hpMode = VobHealth.Vulnerable;

            local vobType = VobType.Static;
            if(obj.canBeInteractedWith)
                vobType = VobType.Interaction;

            if(obj.canbechest)
                vobType = VobType.Container;

            local combination = 1 + getPlayerSkill(playerId, PlayerSkill.Carpenter);
            if(combination > 6)
                combination = 6;

            local pos = getPlayerPosition(playerId);

            Query().insertInto(GameVob.VobTable, ["animationId", "animationTime", "triggerId", "triggerType", "triggerAnimation", "triggerTime", "hpMode", "world", "hp", "hpMax", "movable", "alpha", "dynamic", "weight", "visual", "name",
                "positionX", "positionY", "positionZ", "rotationX", "rotationY", "rotationZ","targetPositionX", "targetPositionY",
                "targetPositionZ", "targetRotationX", "targetRotationY", "targetRotationZ", "items", "guid", "complexity", "type", "createdBy"],
            [
                0, 0, 0, 0, 0, 0, hpMode, "'"+getPlayerWorld(playerId)+"'", obj.health, obj.health,
                VobMovable.Active, 1.0, false, obj.weight, "'"+getVisual()+"'", "''", pos.x + 100, pos.y, pos.z + 50, 0, 0, 0,
                0,0,0,0,0,0, "''", "'"+guid+"'", "'"+combination+"'", vobType, "'"+getAccount(playerId).username+"'"
            ]).execute();

            local vob = Query().select().from(GameVob.VobTable).where(["visual = '"+getVisual()+"'"]).orderBy("id DESC").one();
            if(vob == null)
                return;

            if(guid != "") {
                local _item = createItem("ITKE_SDRP_DOOR_01", 1);
                _item.guid = guid;
                _item.save();
                pObj.inventory.addItem(_item.id);
            }

            local newVob = GameVob(vob["id"].tointeger())

            newVob.trigger = vob.triggerId.tointeger();
            newVob.triggerType = vob.triggerType.tointeger();
            newVob.triggerAnimation = vob.triggerAnimation.tointeger();
            newVob.triggerTime = vob.triggerTime.tointeger();
            newVob.world = getPlayerWorld(playerId);

            newVob.hpMode = vob.hpMode.tointeger();
            newVob.hp = vob.hp.tointeger();
            newVob.hpMax = vob.hpMax.tointeger();
            newVob.opened = vob.opened.tointeger();

            newVob.movable = vob.movable.tointeger();

            newVob.alpha = vob.alpha.tofloat();
            newVob.dynamic = vob.dynamic.tointeger() == 1 ? true : false;
            newVob.visual = vob.visual;
            newVob.name = vob.name;

            newVob.type = vob.type.tointeger();
            newVob.weight = vob.weight.tofloat();
            newVob.guid = vob.guid;
            newVob.complexity = vob.complexity.tointeger();
            newVob.createdBy = vob.createdBy;
            if(newVob.type == VobType.Container) {
                newVob.inventory = item.Controller.createBucket(ItemRelationType.Vob, newVob.id);
                newVob.inventory.maxWeight = newVob.weight;
            }

            newVob.position = {x = vob.positionX.tofloat(), y = vob.positionY.tofloat(), z = vob.positionZ.tofloat()}
            newVob.rotation = {x = vob.rotationX.tofloat(), y = vob.rotationY.tofloat(), z = vob.rotationZ.tofloat()}

            newVob.targetPosition = {x = vob.targetPositionX.tofloat(), y = vob.targetPositionY.tofloat(), z = vob.targetPositionZ.tofloat()}
            newVob.targetRotation = {x = vob.targetRotationX.tofloat(), y = vob.targetRotationY.tofloat(), z = vob.targetRotationZ.tofloat()}

            VobController.vobs[newVob.id] <- newVob;

            vob.animationId = vob.animationId.tointeger();
            if(vob.animationId != 0)
                System.Effect.add(newVob.id, SystemEffectTarget.Vob, vob.animationId, vob.animationTime.tointeger());

            VobPacket(newVob).sendCreateToAll();
        }else{
            foreach(instance, amount in rewards)
                giveItem(playerId, instance.toupper(), amount);
        }

        addPlayerLog(playerId, "Wykonal craft o nazwie " + name);

        CraftPacket().use(playerId, "Uda�o si� stworzy� przedmiot.");
    }
}