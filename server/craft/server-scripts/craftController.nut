
class CraftController
{
    static crafts = {};

    static function onInit() {
        local craftsInDatabase = Query().select().from(Craft.CraftTable).all();

        foreach(craft in craftsInDatabase)
        {
            local newCraft = Craft(craft["id"].tointeger())
            newCraft.name = craft["name"];
            newCraft.caller = craft["caller"].tointeger();
            newCraft.animation = craft["animation"];
            newCraft.staminaCost = craft["staminaCost"].tofloat();
            newCraft.skillRequired = split(craft["skillRequired"], ",")
            newCraft.skillValue = split(craft["skillValue"], ",")
            newCraft.fractionIds = split(craft["fractionIds"], ",")

            for(local i = 0; i < newCraft.skillRequired.len(); i ++) {
                newCraft.skillRequired[i] = newCraft.skillRequired[i].tointeger();
                newCraft.skillValue[i] = newCraft.skillValue[i].tointeger();
            }

            newCraft.createdBy = craft["createdBy"];
            newCraft.createdAt = craft["createdAt"];

            local craftItems = Query().select().from(Craft.CraftItemsTable).where(["craftId = "+newCraft.id+""]).all();

            foreach(craftItem in craftItems)
                newCraft.items[craftItem["instance"]] <- craftItem["amount"].tointeger();

            local craftRewards = Query().select().from(Craft.CraftRewardsTable).where(["craftId = "+newCraft.id+""]).all();

            foreach(craftReward in craftRewards)
                newCraft.rewards[craftReward["instance"]] <- craftReward["amount"].tointeger();


            crafts[newCraft.id] <- newCraft;
        }

        FractionController.updateAfterCraftsLoaded();

        print("We have " + crafts.len() + " registered crafts.");
    }

    static function newCraft(playerId, name, animation, caller, staminaCost, skillRequired, skillValue) {
        local dateObject = date();
        local dateFormat = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        Query().insertInto(Craft.CraftTable, ["createdBy", "createdAt", "name", "animation", "caller", "staminaCost", "skillRequired", "skillValue"
        ], [
            "'"+getPlayerName(playerId)+"'", "'"+dateFormat+"'", "'"+name+"'", "'"+animation+"'", caller, staminaCost, "'"+skillRequired+"'", "'"+skillValue+"'"
        ]).execute();

        local item = Query().select().from(Craft.CraftTable).orderBy("id DESC").one();

        local newCraft = Craft(item["id"].tointeger())
        newCraft.name = item["name"];
        newCraft.caller = item["caller"].tointeger();
        newCraft.animation = item["animation"];
        newCraft.staminaCost = item["staminaCost"].tofloat();
        newCraft.skillRequired = split(item["skillRequired"], ",")
        newCraft.skillValue = split(item["skillValue"], ",")

        for(local i = 0; i < newCraft.skillRequired.len(); i ++) {
            newCraft.skillRequired[i] = newCraft.skillRequired[i].tointeger();
            newCraft.skillValue[i] = newCraft.skillValue[i].tointeger();
        }

        newCraft.createdBy = item["createdBy"];
        newCraft.createdAt = item["createdAt"];
        crafts[newCraft.id] <- newCraft;

        CraftPacket().sendCraft(playerId, newCraft);

        callEvent("onCreateCraft", newCraft.id);
    }

    static function sendAll(playerId) {
        foreach(obj in crafts)
            CraftPacket().sendCraft(playerId, obj);
    }

    static function send(playerId,craftId) {
        if(craftId in crafts)
            CraftPacket().sendCraft(playerId, crafts[craftId]);
    }

    static function getRelatedCrafts(playerId, interactionId) {
        local relatedCrafts = [];
        local playerObject = getPlayer(playerId);
        local craftFractionsIds = FractionController.getCraftObjectsForFractionMember(playerId);

        foreach(craft in crafts) {
            local objectCraft = [999,999];
            try {
                objectCraft = Config["CraftCallerVobRelation"][craft.caller];
            }catch(error){}

            if(objectCraft[0] == interactionId) {
                if(objectCraft[1] == true) {
                    if(craft.fractionIds.find(getPlayer(playerId).fractionId.tostring()) != null)
                        relatedCrafts.append(craft);
                } else
                    relatedCrafts.append(craft);
            }
        }

        return relatedCrafts;
    }

    static function onInteractMob(playerId, interactionId) {
        local craftsRelated = getRelatedCrafts(playerId, interactionId);

        foreach(obj in craftsRelated)
            CraftPacket().sendCraft(playerId, obj);
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case CraftPackets.Use:
                local craftId = packet.readInt32();
                local craftObj = getCraft(craftId);

                craftObj.use(playerId);
            break;
            case CraftPackets.Call:

            break;
            case CraftPackets.Remove:
                local craftId = packet.readInt32();

                if(!(craftId in crafts))
                    return;

                crafts.rawdelete(craftId);

                callEvent("onRemoveCraft", craftId);
            break;
            case CraftPackets.Send:
                local name = packet.readString();
                local animation = packet.readString();
                local caller = packet.readInt16();
                local staminaCost = packet.readFloat();
                local skillRequired = packet.readInt16();
                local skillValue = packet.readInt16();

                newCraft(playerId, name, animation, caller, staminaCost, skillRequired, skillValue)
            break;
            case CraftPackets.Update:
                local craftId = packet.readInt32();
                local name = packet.readString();
                local animation = packet.readString();
                local caller = packet.readInt16();
                local staminaCost = packet.readFloat();
                local skillRequired = packet.readInt16();
                local skillValue = packet.readInt16();

                if(!(craftId in crafts))
                    return;

                local obj = crafts[craftId];
                obj.name = name;
                obj.animation = animation;
                obj.caller = caller;
                obj.staminaCost = staminaCost;
                obj.skillRequired = [skillRequired];
                obj.skillValue = [skillValue];
            break;
            case CraftPackets.AddItems:
                local craftId = packet.readInt32();
                local instance = packet.readString();
                local amount = packet.readInt16();

                if(!(craftId in crafts))
                    return;

                local obj = crafts[craftId];
                if(instance in obj.items)
                    obj.items[instance] = obj.items[instance] + amount;
                else
                    obj.items[instance] <- amount;
            break;
            case CraftPackets.RemoveItems:
                local craftId = packet.readInt32();
                local instance = packet.readString();
                local amount = packet.readInt16();

                if(!(craftId in crafts))
                    return;

                local obj = crafts[craftId];
                if(instance in obj.items)
                    if(obj.items[instance] == amount)
                        obj.items.rawdelete(instance);
            break;
            case CraftPackets.AddReward:
                local craftId = packet.readInt32();
                local instance = packet.readString();
                local amount = packet.readInt16();

                if(!(craftId in crafts))
                    return;

                local obj = crafts[craftId];
                if(instance in obj.rewards)
                    obj.rewards[instance] = obj.rewards[instance] + amount;
                else
                    obj.rewards[instance] <- amount;
            break;
            case CraftPackets.RemoveReward:
                local craftId = packet.readInt32();
                local instance = packet.readString();
                local amount = packet.readInt16();

                if(!(craftId in crafts))
                    return;

                local obj = crafts[craftId];
                if(instance in obj.rewards)
                    if(obj.rewards[instance] == amount)
                        obj.rewards.rawdelete(instance);
            break;
        }
    }
}

addEventHandler("onInit", CraftController.onInit.bindenv(CraftController))
addEventHandler("onInteractMob", CraftController.onInteractMob.bindenv(CraftController))
RegisterPacketReceiver(Packets.Craft, CraftController.onPacket.bindenv(CraftController));

getCraft <- @(id) CraftController.crafts[id];
getCrafts <- @() CraftController.crafts;
