

class Craft
{
    id = -1

    name = ""
    items = null
    rewards = null

    createdAt = ""
    createdBy = ""

    animation = ""
    caller = -1

    staminaCost = -1
    skillRequired = -1
    skillValue = -1

    constructor(craftId)
    {
        this.id = craftId

        this.name = ""

        this.items = {}
        this.rewards = {}

        this.createdAt = ""
        this.createdBy = ""

        this.animation = ""
        this.caller = -1

        this.staminaCost = -1
        this.skillRequired = []
        this.skillValue = []
    }

    function getSkillValueTotal() {
        local v = 0;
        foreach(value in skillValue)
            v += value;

        return v;
    }

    function use()
    {
        if(Hero.stamina < staminaCost)
            return false;

        for(local i = 0; i < skillRequired.len(); i++) {
            if(Hero.skills[skillRequired[i]] < skillValue[i])
                return false;
        }

        foreach(instance, amount in items) {
            if(getItemScheme(instance.toupper()) == null)
            {
                return false
            }

            if(hasItem(heroId, instance.toupper()) < amount)
                return false;
        }

         foreach(instance, amount in rewards)
        {
            if(getItemScheme(instance.toupper()) == null)
            {
                return false
            }
        }

        return true;
    }

    function check() {
        return use();
    }

    function getInfo() {
        local info = "";
        info += "Użycie wytrzymałości: "+staminaCost+"\n";
        for(local i = 0; i < skillRequired.len(); i++) {
            info += "Umiejętność: "+Config["PlayerSkill"][skillRequired[i]]+"\n";
            info += "Poziom umiejętności: "+skillValue[i]+"\n";
        }

        return info;
    }

    function getVisual() {
        foreach(v,_ in rewards)
            return v;
    }

    function isVob() {
        local name = getVisual();
        if(name.find("ASC") != null || name.find("MDS") != null || name.find("3DS") != null)
            return true;

        return false;
    }

    function getName() {
        if(name.len() > 16)
            return name.slice(0, 16)+"..";

        return name;
    }

    function getLabel() {
        if(isVob())
            return name;

        if(rewards.len() == 1) {
            foreach(v, k in rewards) {
                if(k == 1)
                    return getItemScheme(v).name;
                else
                    return getItemScheme(v).name+" ("+k+")";
            }
        } else {
            local label = "";
            foreach(v, k in rewards) {
                if(k == 1)
                    label += getItemScheme(v).name +"\n";
                else
                    label += getItemScheme(v).name+" ("+k+")" +"\n";
            }
            return label;
        }
    }

    function getItemsLabel() {
        local label = "";
        foreach(v, k in items) {
            if(k == 1)
                label += getItemScheme(v).name +"\n";
            else
                label += getItemScheme(v).name+" ("+k+")" +"\n";
        }
        return label;
    }

    function getDescription() {
        if(isVob()) {
            foreach(v, k in rewards)
                return "Vob "+v;
        } else {
            foreach(v, k in rewards)
                return getItemScheme(v).getFullStringDescriptionWithName();
        }
    }
}