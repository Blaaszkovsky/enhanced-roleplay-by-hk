
class CraftController
{
    static crafts = {};

    static function reset() {
        crafts.clear();
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case CraftPackets.Use:
                local text = packet.readString();

                if(ActiveGui == PlayerGUI.Craft) {
                    CraftInterface.craftPreview.info[4].text = text;
                    CraftInterface.craftPreview.info[4].setPosition(anx(Resolution.x/2) - CraftInterface.craftPreview.info[4].width/2, any(Resolution.y/2 + 300))
                }
            break;
            case CraftPackets.Send:
                local craftObj = Craft(packet.readInt32());
                craftObj.name = packet.readString();

                local length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    craftObj.items[packet.readString()] <- packet.readInt16();

                length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    craftObj.rewards[packet.readString()] <- packet.readInt16();

                craftObj.createdAt = packet.readString();
                craftObj.createdBy = packet.readString();
                craftObj.animation = packet.readString();
                craftObj.caller = packet.readInt16();
                craftObj.staminaCost = packet.readFloat();
                local lenght = packet.readInt16();
                for(local i = 0; i < lenght; i ++) {
                    craftObj.skillRequired.push(packet.readInt16());
                    craftObj.skillValue.push(packet.readInt16());
                }
                crafts[craftObj.id] <- craftObj;
                callEvent("onCreateCraft", craftObj.id);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Craft, CraftController.onPacket.bindenv(CraftController));

function getCraft(craftId) {
    if(craftId in CraftController.crafts)
        return CraftController.crafts[craftId];

    return null;
}
getCrafts <- @() CraftController.crafts;
