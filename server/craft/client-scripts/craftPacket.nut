
class CraftPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Craft);
    }

    function use(craftId)
    {
        packet.writeUInt8(CraftPackets.Use)
        packet.writeInt32(craftId);
        packet.send();
    }

    function send(name, animation, caller, staminaCost, skillRequired, skillValue)
    {
        packet.writeUInt8(CraftPackets.Send)
        packet.writeString(name);
        packet.writeString(animation);
        packet.writeInt16(caller);
        packet.writeFloat(staminaCost);
        packet.writeInt16(skillRequired);
        packet.writeInt16(skillValue);
        packet.send();
    }

    function call(craftId)
    {
        packet.writeUInt8(CraftPackets.Call)
        packet.writeInt32(craftId);
        packet.send();
    }

    function remove(craftId)
    {
        packet.writeUInt8(CraftPackets.Remove)
        packet.writeInt32(craftId);
        packet.send();
    }

    function update(craftId, name, animation, caller, staminaCost, skillRequired, skillValue)
    {
        packet.writeUInt8(CraftPackets.Update)
        packet.writeInt32(craftId);
        packet.writeString(name);
        packet.writeString(animation);
        packet.writeInt16(caller);
        packet.writeFloat(staminaCost);
        packet.writeInt16(skillRequired);
        packet.writeInt16(skillValue);
        packet.send();
    }

    function addItem(craftId, instance, amount)
    {
        packet.writeUInt8(CraftPackets.AddItems)
        packet.writeInt32(craftId);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send();
    }

    function removeItem(craftId, instance, amount)
    {
        packet.writeUInt8(CraftPackets.RemoveItems)
        packet.writeInt32(craftId);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send();
    }

    function addReward(craftId, instance, amount)
    {
        packet.writeUInt8(CraftPackets.AddReward)
        packet.writeInt32(craftId);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send();
    }

    function removeReward(craftId, instance, amount)
    {
        packet.writeUInt8(CraftPackets.RemoveReward)
        packet.writeInt32(craftId);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send();
    }
}

