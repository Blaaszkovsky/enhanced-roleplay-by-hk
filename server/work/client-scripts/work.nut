
class Work
{
    id = -1

    name = "";

    staminaCost = -1.0;
    staminaCostMultiplier = -1;

    createdAt = "";
    createdBy = "";

    seconds = -1;
    secondsMultiplier = -1;

    skillRequired = -1;
    skillValue = -1;

    animation = "";
    caller = -1;

    constructor(workId)
    {
        id = workId;

        name = "";

        staminaCost = 0.0;
        staminaCostMultiplier = 100;

        createdAt = "";
        createdBy = "";

        seconds = 60;
        secondsMultiplier = 100;

        skillRequired = 1;
        skillValue = 1;

        animation = "NONE";
        caller = -1;
    }
}