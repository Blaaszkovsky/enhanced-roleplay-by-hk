
class Note {
    id = -1
    text = ""
    name = ""
    renderInstance = ""

    constructor() {
        id = -1
        text = [];
        name = ""
        renderInstance = ""
    }

    function getRandomRender() {
        local index = irand(5);
        switch(index) {
            case 0: renderInstance = "ITWR_ADDON_BOOKLP2"; break;
            case 1: renderInstance = "ITWR_ADDON_BOOKXP250"; break;
            case 2: renderInstance = "ITWR_ADDON_FRANCISABRECHNUNG_MIS"; break;
            case 3: renderInstance = "ITWR_ADDON_SUMMONANCIENTGHOST"; break;
            case 4: renderInstance = "ITWR_ADDON_BOOKLP2"; break;
            case 5: renderInstance = "ITWR_ASTRONOMY_MIS"; break;
        }
    }
}
