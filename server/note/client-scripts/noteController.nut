class NoteController {
    static notes = {};

    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case NotePackets.Get:
                local id = packet.readInt32();
                local name = packet.readString();
                local length = packet.readInt16();
                local texts = [];
                for(local i = 0; i < length; i ++)
                    texts.push(packet.readString());

                if(id in notes)
                    notes.rawdelete(id);

                notes[id] <- Note();
                notes[id].id = id;
                notes[id].name = name;
                notes[id].text = texts;
                notes[id].getRandomRender();

                if(ActiveGui == PlayerGUI.Notes)
                    NoteInterface.change(0);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Note, NoteController.onPacket.bindenv(NoteController));