
class NotePacket {
    packet = null;

    constructor() {
        packet = ExtendedPacket(Packets.Note);
    }

    function save(name, text) {
        packet.writeUInt8(NotePackets.Create)
        packet.writeString(name);
        packet.writeInt16(text.len());
        foreach(item in text)
            packet.writeString(item);
        packet.send(RELIABLE);
    }

    function get() {
        packet.writeUInt8(NotePackets.Get)
        packet.send(RELIABLE);
    }

    function drop(noteId) {
        packet.writeUInt8(NotePackets.Drop)
        packet.writeInt32(noteId);
        packet.send(RELIABLE);
    }

    function destroy(noteId) {
        packet.writeUInt8(NotePackets.Destroy)
        packet.writeInt32(noteId);
        packet.send(RELIABLE);
    }
}