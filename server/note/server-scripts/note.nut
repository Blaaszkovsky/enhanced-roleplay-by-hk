
class Note {
    static NoteTable = "note";

    id = -1
    text = ""
    name = ""

    status = -1
    item = null

    ownerId = -1;
    position = null;
    world = null;

    constructor() {
        id = -1
        text = [];
        name = ""

        status = NoteStatus.Equipment;
        item = null;

        ownerId = -1;
        position = {x = 0, y = 0, z = 0};
        world = getServerWorld();
    }

    function placeOnGround(playerId) {
        local pos = getPlayerPosition(playerId);
        local angle = getPlayerAngle(playerId);

        pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 150);
        pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 150);

        status = NoteStatus.Ground;
        item = ItemsGround.spawn(Items.id("ITWR_ADDON_BOOKLP2"), 1, pos.x + 100, pos.y, pos.z, getPlayerWorld(playerId));

        position = {x = pos.x + 100, y = pos.y, z = pos.z}
        world = getPlayerWorld(playerId);

        update();
    }

    function setOnEquipment(playerId) {
        ownerId = getPlayer(playerId).rId;
        status = NoteStatus.Equipment;
        item = null;

        update();
    }

    function create() {
        Query().insertInto(Note.NoteTable, ["playerId", "name", "world", "x", "y", "z", "status", "text"], [
            ownerId, "'"+name+"'", "'"+world+"'", position.x, position.y, position.z, status, "'"+getTextMySQLFormat()+"'"
        ]).execute();

        local obj = Query().select().from(Note.NoteTable).where(["playerId = "+ownerId]).orderBy("id DESC").one();
        if(obj == null)
            throw "Error w trakcie robienia note.";

        id = obj["id"].tointeger();
    }

    function destroy() {
        if(item != null)
            ItemsGround.destroy(item.id);

        Query().deleteFrom(Note.NoteTable).where(["id = "+id]).execute();
    }

    function update() {
        Query().update(Note.NoteTable, ["name", "x", "y", "z", "world", "status"], [name, position.x, position.y, position.z, world, status]).where(["id = "+id]).execute();
    }

    function getTextMySQLFormat() {
        return String.parseArray(text, "&");
    }
}