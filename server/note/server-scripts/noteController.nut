class NoteController {
    static notes = {};

    static function onInit() {
        local notesInDatabase = Query().select().from(Note.NoteTable).all();

        foreach(noteObject in notesInDatabase)
        {
            local newNote = Note();

            newNote.id = noteObject.id.tointeger();

            newNote.name = noteObject.name;
            newNote.ownerId = noteObject.playerId.tointeger();
            newNote.status = noteObject.status.tointeger();
            newNote.world = noteObject.world;
            newNote.text = split(noteObject.text,"&");
            newNote.position = {x = noteObject.x.tofloat(), y = noteObject.y.tofloat(), z = noteObject.z.tofloat()}

            notes[newNote.id] <- newNote;
        }

        print("We have " + notes.len() + " existing notes.");
    }

    static function addNote(playerId, name, texts) {
        if(hasPlayerItem(playerId, "ITUS_SDRP_TOOL_39") == 0)
            return;

        removeItem(playerId, "ITUS_SDRP_TOOL_39", 1);

        for(local i = 0; i < texts.len(); i ++) {
            local str = mysql_escape_string(texts[i]);
            if(str == null)
                str = "";

            texts[i] = str;
        }

        local newNote = Note();
        newNote.setOnEquipment(playerId);
        newNote.name = name;
        newNote.text = texts;
        newNote.create();
        notes[newNote.id] <- newNote;

        local object = Config["ChatMethod"][ChatMethods.Action];
        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] napisa� ksi��k�.");
    }

    static function onPlayerTakeItem(playerId, item) {
        foreach(note in notes) {
            if(note.item == null)
                continue;

            if(note.item.id == item.id) {
                note.setOnEquipment(playerId);
                return true;
            }
        }

        return false;
    }

    static function getPlayerNotes(playerId) {
        playerId = getPlayer(playerId).rId;
        local notesToAdjust = [];
        foreach(note in notes)
            if(note.ownerId == playerId && note.status == NoteStatus.Equipment)
                notesToAdjust.push(note)

        return notesToAdjust;
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case NotePackets.Get:
                local notes = getPlayerNotes(playerId);
                foreach(note in notes)
                    NotePacket().get(playerId, note.id, note.name, note.text)
            break;
            case NotePackets.Create:
                local name = packet.readString();
                local textLen = packet.readInt16(), texts = [];
                for(local i = 0; i < textLen; i ++)
                    texts.push(packet.readString());

                addNote(playerId, name, texts);
            break;
            case NotePackets.Drop:
                local noteId = packet.readInt32();
                if(!(noteId in notes))
                    return;

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] upu�ci� ksi��k�.");
                local note = notes[noteId];
                note.placeOnGround(playerId);
            break;
            case NotePackets.Destroy:
                local noteId = packet.readInt32();
                if(!(noteId in notes))
                    return;

                local note = notes[noteId];
                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] niszczy ksi��k�.");
                note.destroy();
                notes.rawdelete(noteId);
            break;
        }
    }
}

addEventHandler("onInit", NoteController.onInit.bindenv(NoteController));
RegisterPacketReceiver(Packets.Note, NoteController.onPacket.bindenv(NoteController));