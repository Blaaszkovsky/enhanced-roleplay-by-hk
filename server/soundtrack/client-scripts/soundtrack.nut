local soundtrackSound = Sound("03.WAV")
soundtrackSound.volume = 0.2
local canPlay = true
disableMusicSystem(false);

local dailySounds =
[
    "03.WAV",
    "05.WAV",
    "18.WAV",
    "22.WAV",
    "28.WAV",
    "30.WAV",
    "31.WAV",
]

local nightSounds =
[
    "10.WAV",
    "15.WAV",
    "19.WAV",
    "20.WAV",
    "21.WAV",
    "41.WAV",
    "44.WAV",
]

local currentSound = rand() % dailySounds.len()
local typeSound = null

local function clampCurrent(soundsArray, currValue)
{
    if (soundsArray.len() - 1 > currValue)
        return currValue + 1
    else
        return 0
}

addEventHandler("onInit", function()
{
	setMusicVolume(0.0);
    disableMusicSystem(false);

})

setTimer(function()
{
    if (!canPlay)
        return

    if (!soundtrackSound.isPlaying())
    {
        local clamped = 0

        if (getTime().hour >= 8 && getTime().hour <= 21)
        {
            clamped = clampCurrent(dailySounds, currentSound)
            typeSound = dailySounds
        }
        else
        {
            clamped = clampCurrent(nightSounds, currentSound)
            typeSound = nightSounds
        }

        soundtrackSound.file = typeSound[clamped]
        currentSound = clamped
        soundtrackSound.play()
		soundtrackSound.looping = false
    }
}, 10000, 0)

addEventHandler("onSettingsChange", function()
{
    soundtrackSound.volume = Config["Settings"]["SoundtrackVolume"].value

    if (soundtrackSound.volume == 0.0)
        canPlay = false
    else
        canPlay = true
})