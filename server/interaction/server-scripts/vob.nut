
class VobInteraction
{
    static function onPacket(playerId, packet) {
        local packetId = packet.readUInt8();

        if(packetId != PacketInteraction)
            return;

        switch(packet.readUInt8())
        {
            case PacketInteractions.Mob:
                callEvent("onInteractMob", playerId, packet.readInt16());
            break;
        }
    }
}

addEventHandler("onPacket", VobInteraction.onPacket.bindenv(VobInteraction)); 