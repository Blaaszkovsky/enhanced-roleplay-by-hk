
InteractionTree <- {};

InteractionTree.list <- [];
InteractionTree.active <- [];

InteractionTree.possibleRenders <- {};
InteractionTree.possibleKeys <- ["POSSIBLE_ONE", "POSSIBLE_TWO", "POSSIBLE_THREE", "POSSIBLE_FOUR"];

foreach(index, value in InteractionTree.possibleKeys) {
    InteractionTree.active.append(null);

    InteractionTree.possibleRenders[index] <- {
        background = Texture(0,0,anx(200),any(40),"SG_VERTICAL.TGA"),
        draw = Draw(0,0,""),
    };
}

InteractionTree.add <- function(label, observer) {
    local index = -1, iCounter = 0;
    foreach(treeIndex, treeValue in InteractionTree.active) {
        if(treeValue == null) {
            index = treeIndex;
            InteractionTree.active[treeIndex] = { label = label, observer = observer };
            break;
        }
        iCounter = iCounter + 1;
    }

    if(index == -1)
        return;

    local object = InteractionTree.possibleRenders[index];

    object.draw.text = "["+keyLabelTab[getAccountKeyBind(InteractionTree.possibleKeys[iCounter])]+"] - "+label;
    object.draw.font = "FONT_OLD_20_WHITE_HI.TGA";
    object.draw.setScale(0.35, 0.35);
    // set out position
    object.background.setPosition(anx(Resolution.x - 100) - object.draw.width, any(Resolution.y - 250 + 50 * iCounter))
    object.draw.setPosition(anx(Resolution.x - 75) - object.draw.width, any(Resolution.y - 250 + 50 * iCounter + 11))
    object.background.setSize(object.draw.width + anx(50), any(35));
    // set visible of objects
    object.background.visible = true;
    object.draw.visible = true;
}

InteractionTree.remove <- function(label) {
    local index = -1, iCounter = 0;
    foreach(treeIndex, treeValue in InteractionTree.active) {
        if(treeValue == null)
            continue;

        if(treeValue.label == label) {
            index = treeIndex;
            InteractionTree.active[treeIndex] = null;
            break;
        }
        iCounter = iCounter + 1;
    }

    if(index == -1)
        return;

    local object = InteractionTree.possibleRenders[index];

    object.background.visible = false;
    object.draw.visible = false;
}


InteractionTree.checkObservers <- function() {
    local heroPosition = getPlayerPosition(heroId);
    foreach(item in InteractionTree.list)
    {
        local difference = getPositionDifference(item.position, heroPosition);
        if(difference < item.distance) {
            if(item.active == false) {
                InteractionTree.add(item.label, item.observer);
                item.active = true;
            }
        }
        else {
            if(item.active == true) {
                InteractionTree.remove(item.label);
                item.active = false;
            }
        }
    }
}

local lastTick = null;
addEventHandler("onRender", function() {
    if(ActiveGui != null)
        return;

    if(getTickCount() > lastTick) {
        InteractionTree.checkObservers();
        lastTick = getTickCount() + 100;
    }
});

addEventHandler("onKey", function(key) {
    if (chatInputIsOpen())
        return

    if (isConsoleOpen())
        return

    if(ActiveGui != null)
        return;

    foreach(treeIndex, treeValue in InteractionTree.active) {
        if(treeValue == null)
            continue;

        if(getAccountKeyBind(InteractionTree.possibleKeys[treeIndex]) == key)
            treeValue.observer();
    }
})

addEventHandler("onCloseGUI", function() {
    foreach(treeIndex, treeValue in InteractionTree.active) {
        if(treeValue == null)
            continue;

        local object = InteractionTree.possibleRenders[treeIndex];

        object.background.visible = true;
        object.draw.visible = true;
    }
})

addEventHandler("onOpenGUI", function() {
    foreach(treeIndex, treeValue in InteractionTree.active) {
        if(treeValue == null)
            continue;

        local object = InteractionTree.possibleRenders[treeIndex];

        object.background.visible = false;
        object.draw.visible = false;
    }
})

InteractionTree.removeDistanceObserver <- function(object)
{
    local findObserver = InteractionTree.list.find(object);
    if(findObserver != null) {
        InteractionTree.remove(object.label);
        InteractionTree.list.remove(findObserver);
    }
}

class InteractionTree.addDistanceObserver
{
    position = null
    distance = -1

    label = ""
    observer = null

    active = false

    constructor(x,y,z,distance,label,observer)
    {
        this.position = {x = x, y = y, z = z}
        this.distance = distance

        this.label = label
        this.observer = observer
        this.active = false

        InteractionTree.list.push(this);
    }
}