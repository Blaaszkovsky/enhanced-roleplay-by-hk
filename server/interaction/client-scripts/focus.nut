local focusId = -1;

class FocusInteraction
{
    static function onFocus(newFocus, oldFocus) {
        focusId = -1;

        hideInteractionLabel();

        if(ActiveGui != null)
            return;

        if(getPlayerWeaponMode(heroId) != 0)
            return false;

        if(newFocus == -1)
        {
            hideInteractionLabel();
            return false;
        }
        else if(newFocus >= getMaxSlots())
        {
            local bot = getBots()[getBotElements()[newFocus]];
            if(bot.isInteractionPossible() == false)
                return false;
        }
        else
        {
            if(isInteractionPossibleWithPlayer(newFocus) == false)
                return false;
        }

        focusId = newFocus;

        showInteractionLabel();
    }

    static function onAcceptFocus() {
        hideInteractionLabel();

        local pos = getPlayerPosition(heroId);
        local posFocus = getPlayerPosition(focusId);

        if(getDistance3d(pos.x, pos.y, pos.z, posFocus.x, posFocus.y, posFocus.z) > 350)
            return;

        if(focusId >= getMaxSlots())
            callEvent("onInteractNpc",getBotElements()[focusId]);
        else
            callEvent("onInteractPlayer",focusId);
    }
}

addEventHandler("onFocus", FocusInteraction.onFocus.bindenv(FocusInteraction));