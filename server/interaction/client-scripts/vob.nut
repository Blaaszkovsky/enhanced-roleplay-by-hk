
class VobInteraction
{
    static function getInteractionFromModel(modelName) {
        modelName = modelName.toupper();
        foreach(index, model in Config["Interactions"])
        {
            if(typeof(model) == "array")
            {
                if(model.find(modelName) != null)
                    return index;
            }else
                if(model == modelName)
                    return index;
        }

        return -1;
    }

    static function onMobInteract(pointer, mobType, from, to) {

        if(from == 1)
        {
            callEvent("onInteractLostMob");
        }
        else
        {
            local interactionId = VobInteraction.getInteractionFromModel(Mob(pointer).visual);
            callEvent("onInteractMob", interactionId);
            print(Mob(pointer).visual);

            if(interactionId == -1)
            {
                foreach(vob in getVobs())
                {
                    if (vob.world != getWorld())
                        continue

                    if(vob.object.ptr == pointer)
                        callEvent("onVobInteract", vob.id);
                }

                foreach(house in getHouses())
                    foreach(door in house.doors)
                    {
                        if (house.world != getWorld())
                            continue

                        if(door.element.ptr == pointer)
                            callEvent("onHouseDoorInteract", house.id, door.id);
                    }

                foreach(_id, mine in getMines())
                {
                    if (mine.world != getWorld())
                        continue

                    if(mine.vob.ptr == pointer)
                        MineController.interactWithMine(_id);
                }

                foreach(_id, fishing in getFishings())
                {
                    if (fishing.world != getWorld())
                        continue

                    if(fishing.vob.ptr == pointer)
                        FishingController.interactWithFishing(_id);
                }
            }else{
                local packet = Packet();
                packet.writeUInt8(PacketInteraction);
                packet.writeUInt8(PacketInteractions.Mob);
                packet.writeInt16(interactionId);
                packet.send(RELIABLE);
            }
        }
    }
}

addEventHandler("onMobInteract", VobInteraction.onMobInteract.bindenv(VobInteraction));