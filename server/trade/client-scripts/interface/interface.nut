
TradeInterface <- {
    tradeWindow = null,
    tradeObject = null,
    alredyAccepted = false,

    prepareTradeWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 200), any(Resolution.y - 150), anx(300), any(100), "SG_BOX.TGA", null, false);

        result.buttonAccept <- GUI.Button(anx(20), any(20), anx(130), any(60), "SG_BUTTON.TGA", "Akceptuj", result.window);
        result.buttonExit <- GUI.Button(anx(150), any(20), anx(130), any(60), "SG_BUTTON.TGA", "Wyjd�", result.window);

        result.buttonAccept.bind(EventType.Click, function(element) {
            TradeInterface.accept();
        });

        result.buttonExit.bind(EventType.Click, function(element) {
            TradeInterface.exit();
        });

        return result;
    }

    getAccepted = function() {
        tradeObject.offerTwo.ui.setBlock(true);
        tradeObject.offerOne.ui.setBlock(true);
        Hero.inventory.ui.setBlock(true);

        tradeWindow.buttonAccept.setColor(3, 45, 58);
    }

    accept = function() {
        if(alredyAccepted)
            return;

        alredyAccepted = true;
        tradeObject.packetManager.acceptTrade();
    }

    exit = function() {
        tradeObject.packetManager.end();
    }

    show = function(_tradeObject) {
        BaseGUI.show();
        ActiveGui = PlayerGUI.Trade;

        tradeObject = _tradeObject;

        if(tradeObject.playerOneId == heroId) {
            tradeObject.offerOne.ui = item.Bucket.UI(tradeObject.offerOne);
            tradeObject.offerOne.ui.setConnection(Hero.inventory);
            tradeObject.offerOne.ui.setTitle("Twoja oferta");
            tradeObject.offerOne.ui.setVisible(true, ItemGUISide.Left, ItemGUIPosition.Top, ItemGUISize.Half);

            tradeObject.offerTwo.ui = item.Bucket.UI(tradeObject.offerTwo);
            tradeObject.offerTwo.ui.setTitle("Oferta "+getPlayerRealName(tradeObject.playerTwoId));
            tradeObject.offerTwo.ui.setVisible(true, ItemGUISide.Left, ItemGUIPosition.Bottom, ItemGUISize.Half);

            Hero.inventory.ui.setTitle("Ekwipunek");
            Hero.inventory.ui.setConnection(tradeObject.offerOne);
            Hero.inventory.ui.setVisible(true, ItemGUISide.Right);
        }else{
            tradeObject.offerOne.ui = item.Bucket.UI(tradeObject.offerOne);
            tradeObject.offerOne.ui.setTitle("Oferta "+getPlayerRealName(tradeObject.playerOneId));
            tradeObject.offerOne.ui.setVisible(true, ItemGUISide.Left, ItemGUIPosition.Bottom, ItemGUISize.Half);

            tradeObject.offerTwo.ui = item.Bucket.UI(tradeObject.offerTwo);
            tradeObject.offerTwo.ui.setConnection(Hero.inventory);
            tradeObject.offerTwo.ui.setTitle("Twoja oferta");
            tradeObject.offerTwo.ui.setVisible(true, ItemGUISide.Left, ItemGUIPosition.Top, ItemGUISize.Half);

            Hero.inventory.ui.setTitle("Ekwipunek");
            Hero.inventory.ui.setConnection(tradeObject.offerTwo);
            Hero.inventory.ui.setVisible(true, ItemGUISide.Right);
        }

        if(tradeWindow == null)
            tradeWindow = prepareTradeWindow();

        alredyAccepted = false;
        setPlayerWeaponMode(heroId, 0);

        tradeWindow.window.setVisible(true);
    }

    hide = function() {
        BaseGUI.hide();
        ActiveGui = null;

        Hero.inventory.ui.setBlock(false);
        Hero.inventory.ui.setVisible(false);

        tradeObject.offerOne.ui.setVisible(false);
        tradeObject.offerOne.ui = null

        tradeObject.offerTwo.ui.setVisible(false);
        tradeObject.offerTwo.ui = null

        tradeObject = null;
        alredyAccepted = false;
        tradeWindow.window.setVisible(false);
    }
}