
class TradePacket
{
    packet = null;
    obj = null;

    constructor(_obj)
    {
        obj = _obj;
        packet = ExtendedPacket(Packets.Trade);
    }

    function reset() {
        packet.reset();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Trade);
    }

    function acceptTrade() {
        packet.writeUInt8(TradePackets.Accept)
        packet.writeInt16(obj.id);
        packet.send();

        reset();
    }

    function end() {
        packet.writeUInt8(TradePackets.End)
        packet.writeInt16(obj.id);
        packet.send();

        reset();
    }
}