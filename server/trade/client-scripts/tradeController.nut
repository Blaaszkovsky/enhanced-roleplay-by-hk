
class TradeController
{
    static trades = {};

    static function endTrade(id) {
        TradeInterface.hide();
        local trade = trades[id];
        item.Controller.buckets.rawdelete(trade.offerOne.id);
        item.Controller.buckets.rawdelete(trade.offerTwo.id);
        trades.rawdelete(id);
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case TradePackets.Create:
                local id = packet.readInt16();
                local playerOneId = packet.readInt16();
                local playerTwoId = packet.readInt16();
                local offerOneId = packet.readUInt16();
                local offerTwoId = packet.readUInt16();

                local trade = Trade(id, playerOneId, playerTwoId);

                trade.offerOne = item.Controller.createBucket(offerOneId);
                trade.offerTwo = item.Controller.createBucket(offerTwoId);

                trade.setUpInterface();

                trades[id] <- trade;
            break;
            case TradePackets.Accept:
                local id = packet.readInt16();
                if(id in trades)
                    trades[id].acceptTrade();
            break;
            case TradePackets.End:
                local id = packet.readInt16();
                if(id in trades)
                    endTrade(id);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Trade, TradeController.onPacket.bindenv(TradeController));


