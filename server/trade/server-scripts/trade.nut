
class Trade
{
    id = -1;

    playerTwoId = -1;
    playerOneId = -1;
    offerOne = null
    offerTwo = null

    accepted = false;
    packetManager = null;

    constructor(identification, playerIdOne, playerIdTwo)
    {
        id = identification;

        playerTwoId = playerIdTwo;
        playerOneId = playerIdOne;
        offerTwo = null;
        offerOne = null;

        accepted = false;
        packetManager = TradePacket(this);
    }

    function acceptTrade() {
        if(accepted == true)
            TradeController.success(id);
        else{
            accepted = true;
            offerOne.block = true;
            offerTwo.block = true;
            packetManager.acceptTrade();
        }
    }

    function endTrade() {
        packetManager.end();

        foreach(itemId in offerOne.ids)
            getPlayer(playerOneId).inventory.addItem(itemId);

        foreach(itemId in offerTwo.ids)
            getPlayer(playerTwoId).inventory.addItem(itemId);

        item.Controller.removeBucket(offerOne.id);
        item.Controller.removeBucket(offerTwo.id);
    }
}
